/**
 * Package where console application interface for CellDesigner-SBGNML
 * transformation is implemented.
 */
package lcsb.mapviewer.converter.model.sbgnml.console;