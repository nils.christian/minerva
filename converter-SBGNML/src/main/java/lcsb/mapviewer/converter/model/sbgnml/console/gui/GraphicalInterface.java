package lcsb.mapviewer.converter.model.sbgnml.console.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbgnml.console.SbgnmlConsoleConverter;

/**
 * Graphical User Interface for SBGN-ML, CellDesigner converter.
 **/
public class GraphicalInterface {

	/**
	 * Path of the input file.
	 */
	private String		 szInputFilePath		 = null;

	/**
	 * Path of the output file.
	 */
	private String		 szOutputFilePath		 = null;

	/**
	 * Path of the log file.
	 */
	private String		 szLogsFilePath			 = null;

	/**
	 * Should the conversion go from CellDesigner to SBGN-ML file.
	 */
	private boolean		 cellDesigner2Sbgnml = false;

	/**
	 * Input text with path to input file.
	 */
	private JTextField jtfOpenFilePath;

	/**
	 * Input text with path to output file.
	 */
	private JTextField jtfSaveFilePath;

	/**
	 * Input text with path to log file.
	 */
	private JTextField jtfLogFilePath;

	/**
	 * Button used for opening files.
	 */
	private JButton		 btnOpenInput;

	/**
	 * Button used for saving converted files.
	 */
	private JButton		 btnSaveOutput;

	/**
	 * Button used for saving log file.
	 */
	private JButton		 btnLogsFile;

	/**
	 * Button for staring conversion.
	 */
	private JButton		 btnParse;

	/**
	 * Button for closing application.
	 */
	private JButton		 btnClose;

	/**
	 * Label with copyrights.
	 */
	private JLabel		 fundingLabel;

	/**
	 * Launch the application.
	 *
	 * @param args
	 */
	public void execute() {
		try {
			final JFrame frame = new JFrame("SBGN-ML to SBML Converter");

			JPanel panel = new JPanel();

			panel.setLayout(null);

			// CHECKSTYLE:OFF
			jtfOpenFilePath = new JTextField(70);
			jtfSaveFilePath = new JTextField(70);
			jtfLogFilePath = new JTextField(70);

			jtfOpenFilePath.setBounds(265, 5, 600, 28);
			jtfSaveFilePath.setBounds(265, 55, 600, 28);
			jtfLogFilePath.setBounds(265, 105, 600, 28);

			jtfLogFilePath.setEditable(false);
			jtfOpenFilePath.setEditable(false);
			jtfSaveFilePath.setEditable(false);

			btnOpenInput = new JButton();
			btnOpenInput.setBounds(10, 5, 218, 28);
			btnOpenInput.setText("Open Input File");
			btnOpenInput.addActionListener(createOpenButtonListener());

			btnSaveOutput = new JButton();
			btnSaveOutput.setBounds(10, 55, 218, 28);
			btnSaveOutput.setText("Save output file");
			btnSaveOutput.addActionListener(createSaveButtonListener());

			btnLogsFile = new JButton();
			btnLogsFile.setBounds(10, 105, 218, 28);
			btnLogsFile.setText("Save Model consistency logs");
			btnLogsFile.addActionListener(createSaveLogsListener());

			btnParse = new JButton();
			btnParse.setBounds(10, 205, 218, 28);
			btnParse.setText("Convert");
			btnParse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (null == szInputFilePath) {
						JOptionPane.showMessageDialog(
								frame, "Error: No path for the input file! Press " + btnOpenInput.getText() + " button to load input file!", "No input file!",
								JOptionPane.ERROR_MESSAGE);
					}

					else if (null == szOutputFilePath) {
						JOptionPane.showMessageDialog(
								frame, "Error: No path for the output file! Press " + btnSaveOutput.getText() + " button to save output file!", "No output file path!",
								JOptionPane.ERROR_MESSAGE);
					}

					else {
						if (null == szLogsFilePath) {
							int reply = JOptionPane.showConfirmDialog(
									frame, "Warning: No path for the model consistency check logs file! Do you want to add it now?", "No model consistency check file path!",
									JOptionPane.YES_NO_OPTION);

							if (reply == JOptionPane.YES_OPTION) {
								JFileChooser fileChooser = new JFileChooser();
								fileChooser.setDialogTitle("Specify a file to save Logs");

								int userSelection = fileChooser.showSaveDialog(null);

								if (userSelection == JFileChooser.APPROVE_OPTION) {
									szLogsFilePath = fileChooser.getSelectedFile().getAbsolutePath();
									int iPathSize = szLogsFilePath.length();
									String strExtension = szLogsFilePath.substring(iPathSize - 4, iPathSize);
									if (!strExtension.toLowerCase().equals(".txt")) {
										szLogsFilePath = szLogsFilePath.concat(".txt");
									}

									jtfLogFilePath.setText(szLogsFilePath);
								}
							}
						}

						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						System.setOut(new PrintStream(baos));

						try {
							SbgnmlConsoleConverter.convert(szInputFilePath, szOutputFilePath, cellDesigner2Sbgnml);
						} catch (InvalidInputDataExecption e1) {
							JOptionPane.showMessageDialog(frame, "Problems, when converting the file:\n" + e1.getMessage(), "Converter problem!", JOptionPane.ERROR_MESSAGE);
							return;
						}

						System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
						if (null != szLogsFilePath) {
							PrintWriter logsFile = null;
							try {
								logsFile = new PrintWriter(szLogsFilePath);
							} catch (FileNotFoundException ex) {
								JOptionPane.showMessageDialog(frame, "Error: Couldn't open the logs file ", "Problems with opening logs file!", JOptionPane.ERROR_MESSAGE);
							}
							logsFile.println(baos.toString());
							logsFile.close();

							JOptionPane.showMessageDialog(
									frame, "Parsing finished: output file available at: " + szOutputFilePath
											+ "\nIf anything went wrong, logs with all warnings can be found at: " + szLogsFilePath);
						} else {
							JOptionPane.showMessageDialog(frame, "Parsing finished: output file available at: " + szOutputFilePath);
						}
					}
				}
			});

			btnClose = new JButton();
			btnClose.setBounds(520, 205, 147, 28);
			btnClose.setText("Close");
			btnClose.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					frame.dispose();
				}
			});

			fundingLabel = new JLabel("@Copyright: EISBM - eTRIKS");
			fundingLabel.setBounds(700, 205, 300, 28);

			panel.add(btnOpenInput);
			panel.add(btnSaveOutput);
			panel.add(btnLogsFile);
			panel.add(btnParse);
			panel.add(btnClose);

			panel.add(jtfOpenFilePath);
			panel.add(jtfSaveFilePath);
			panel.add(jtfLogFilePath);

			frame.add(panel);
			frame.setSize(900, 280);
			frame.setLocationRelativeTo(null);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);

			// CHECKSTYLE:ON
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates listener to open dialog for saving logs.
	 * 
	 * @return listener resposinble to open dialog for saving log file
	 */
	public ActionListener createSaveLogsListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Specify a file to save Logs");

				int userSelection = fileChooser.showSaveDialog(null);

				if (userSelection == JFileChooser.APPROVE_OPTION) {
					szLogsFilePath = fileChooser.getSelectedFile().getAbsolutePath();
					if (!szLogsFilePath.toLowerCase().endsWith(".txt")) {
						szLogsFilePath = szLogsFilePath.concat(".txt");
					}

					jtfLogFilePath.setText(szLogsFilePath);
				}
			}
		};
	}

	/**
	 * Creates listener to open dialog for saving output.
	 * 
	 * @return listener resposinble to open dialog for saving output
	 */
	public ActionListener createSaveButtonListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Specify a file to save");
				fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON Files", "json"));
				fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("SBML Files", "xml"));

				int userSelection = fileChooser.showSaveDialog(null);

				if (userSelection == JFileChooser.APPROVE_OPTION) {
					szOutputFilePath = fileChooser.getSelectedFile().getAbsolutePath();
					jtfSaveFilePath.setText(szOutputFilePath);
				}
			}
		};
	}

	/**
	 * Creates listener to open dialog for opening input file.
	 * 
	 * @return listener resposinble to open dialog for input file
	 */
	public ActionListener createOpenButtonListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Open a file to parse");
				fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON Files", "json"));
				fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("SBML Files", "xml"));
				int userSelection = fileChooser.showOpenDialog(null);
				if (userSelection == JFileChooser.APPROVE_OPTION) {
					szInputFilePath = fileChooser.getSelectedFile().getAbsolutePath();
					cellDesigner2Sbgnml = szInputFilePath.endsWith(".xml");
					jtfOpenFilePath.setText(szInputFilePath);
					szOutputFilePath = szInputFilePath.substring(0, szInputFilePath.lastIndexOf('.'));
					if (cellDesigner2Sbgnml) {
						szOutputFilePath = szOutputFilePath.concat("_converted.sbgn");
					} else {
						szOutputFilePath = szOutputFilePath.concat("_converted.xml");
					}
					jtfSaveFilePath.setText(szOutputFilePath);
					szLogsFilePath = szInputFilePath.substring(0, szInputFilePath.lastIndexOf('.')).concat("_logs.txt");
					jtfLogFilePath.setText(szLogsFilePath);
				}
			}
		};
	}
}
