/**
 * 
 */
package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBException;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;

import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Sbgn;

/**
 * @author Michał Kuźma
 *
 */
public class SbgnmlXmlConverter implements IConverter {

	@Override
	public Model createModel(ConverterParams params) throws InvalidInputDataExecption {
		SbgnmlXmlParser parser = new SbgnmlXmlParser();
		return parser.createModel(params);
	}

	@Override
	public InputStream exportModelToInputStream(Model model) throws InconsistentModelException, ConverterException {
		SbgnmlXmlExporter exporter = new SbgnmlXmlExporter();
		Sbgn exportedData = exporter.toSbgnml(model);
		File tempFile;
		try {
			tempFile = File.createTempFile("sbgnmlExportTempFile", ".tmp");
			SbgnUtil.writeToFile(exportedData, tempFile);
			InputStream result = new FileInputStream(tempFile);
			return result;
		} catch (IOException e) {
			throw new ConverterException(e);
		} catch (JAXBException e) {
			throw new ConverterException(e);
		}
	}

	@Override
	public File exportModelToFile(Model model, String filePath) throws InconsistentModelException, ConverterException {
		SbgnmlXmlExporter exporter = new SbgnmlXmlExporter();
		Sbgn exportedData = exporter.toSbgnml(model);
		File exportedFile = new File(filePath);
		if (exportedFile.getParentFile() != null) {
			exportedFile.getParentFile().mkdirs();
		}
		try {
			SbgnUtil.writeToFile(exportedData, exportedFile);
		} catch (JAXBException e) {
			throw new ConverterException(e);
		}
		return exportedFile;
	}

	@Override
	public String getCommonName() {
		return "SBGN-ML";
	}

	@Override
	public MimeType getMimeType() {
		return MimeType.TEXT;
	}

	@Override
	public String getFileExtension() {
		return "sbgn";
	}

}
