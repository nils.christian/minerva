package lcsb.mapviewer.converter.model.sbgnml.console;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

/**
 * This class is used to parse and store input data for {@link SbgnmlConsoleConverter}
 * .
 * 
 * @author Michał Kuźma
 *
 */
public class SbgnmlRunOptions {

	/**
	 * Default class logger.
	 */
	private final Logger logger = Logger.getLogger(SbgnmlRunOptions.class.getName());

	/**
	 * What is the abbreviation of help option.
	 */
	private static final String	HELP_OPTION	= "h";
	

	/**
	 * What is the abbreviation of output file option.
	 */
	private static final String	OUTPUT_OPTION = "o";
	

	/**
	 * What is the abbreviation of input file option.
	 */
	private static final String	INPUT_OPTION = "i";
	
	/**
	 * What is the abbreviation of reversed converter mode option (CellDesigner to SBGN-ML).
	 */
	private static final String REVERSED_OPTION = "r";
	
	/**
	 * Object with available input options.
	 */
	private Options							options;

	/**
	 * What should be the command line to run the program.
	 */
	private String commandLineRun = null;

	/**
	 * Was the input valid?
	 */
	private boolean validInput = false;

	/**
	 * Data parsed from input.
	 */
	private CommandLine commandLine;
	
	/**
	 * Default constructor that parses data.
	 * 
	 * @param args
	 *          parameters used to run the program
	 */
	public SbgnmlRunOptions(String[] args) {
		options = new Options();
		options.addOption(createOption("input", INPUT_OPTION, true, "input-file", "input file"));
		options.addOption(createOption("output", OUTPUT_OPTION, true, "output-file", "converted output file"));
		OptionBuilder.isRequired(false);
		options.addOption(REVERSED_OPTION, false, "convert from CellDesigner to SBGN-ML");
		options.addOption(HELP_OPTION, false, "print this help menu");

		commandLineRun = "java -jar " + new java.io.File(SbgnmlConsoleConverter.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName()
				+ " [options]";

		CommandLineParser parser = new BasicParser();

		try {
			validInput = true;
			commandLine = parser.parse(options, args);
			if (getInputFilename() == null || getOutputFilename() == null
					|| (isReversedOption() && (!getInputFilename().endsWith(".xml") || !getOutputFilename().endsWith(".sbgn")))
					|| (!isReversedOption() && (!getInputFilename().endsWith(".sbgn") || !getOutputFilename().endsWith(".xml")))) {
				validInput = false;
			}
		} catch (ParseException e) {
			validInput = false;
			logger.info(e.getMessage());
		}

	}
	
	/**
	 * Creates new {@link Option} object.
	 * 
	 * @param optionName
	 *          full name of the option
	 * @param optionAbbreviation
	 *          abbreviation of the option
	 * @param hasArgument
	 *          has the option argument
	 * @param argumentName
	 *          name of the argument
	 * @param description
	 *          description of the option
	 * @return {@link Option} created from input values
	 */
	private Option createOption(String optionName, String optionAbbreviation, boolean hasArgument, String argumentName, String description) {
		OptionBuilder.hasArg(hasArgument);
		if (hasArgument) {
			OptionBuilder.withArgName(argumentName);
		}
		OptionBuilder.withDescription(description);
		OptionBuilder.withLongOpt(optionName);
		return OptionBuilder.create(optionAbbreviation);
	}
	
	/**
	 * @return the options
	 * @see #options
	 */
	public Options getOptions() {
		return options;
	}

	/**
	 * @param options
	 *          the options to set
	 * @see #options
	 */
	public void setOptions(Options options) {
		this.options = options;
	}

	/**
	 * @return the commandLineRun
	 * @see #commandLineRun
	 */
	public String getCommandLineRun() {
		return commandLineRun;
	}

	/**
	 * @param commandLineRun
	 *          the commandLineRun to set
	 * @see #commandLineRun
	 */
	public void setCommandLineRun(String commandLineRun) {
		this.commandLineRun = commandLineRun;
	}

	/**
	 * @return the validInput
	 * @see #validInput
	 */
	public boolean isValidInput() {
		return validInput;
	}

	/**
	 * @param validInput
	 *          the validInput to set
	 * @see #validInput
	 */
	public void setValidInput(boolean validInput) {
		this.validInput = validInput;
	}

	/**
	 * @return the commandLine
	 * @see #commandLine
	 */
	public CommandLine getCommandLine() {
		return commandLine;
	}

	/**
	 * @param commandLine
	 *          the commandLine to set
	 * @see #commandLine
	 */
	public void setCommandLine(CommandLine commandLine) {
		this.commandLine = commandLine;
	}
	
	/**
	 * Prints help to the console how to used the program (what parameters to put
	 * and what are they used for).
	 */
	public void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(commandLineRun, options);
	}
	
	/**
	 * Checks if {@link #HELP_OPTION} was selected in the input.
	 * 
	 * @return <code>true</code> if {@link #HELP_OPTION} was selected,
	 *         <code>false</code> otherwise
	 */
	public boolean isHelpOption() {
		return commandLine.hasOption(HELP_OPTION);
	}
	
	/**
	 * Checks if {@link #REVERSED} was selected in the input.
	 * 
	 * @return <code>true</code> if {@link #REVERSED_OPTION} was selected,
	 *         <code>false</code> otherwise
	 */
	public boolean isReversedOption() {
		return commandLine.hasOption(REVERSED_OPTION);
	}

	/**
	 * Returns name of the input file.
	 * 
	 * @return name of the input file
	 */
	public String getInputFilename() {
		return commandLine.getOptionValue(INPUT_OPTION);
	}

	/**
	 * Returns name of the output file.
	 * 
	 * @return name of the output file
	 */
	public String getOutputFilename() {
		return commandLine.getOptionValue(OUTPUT_OPTION);
	}
	
	/**
	 * Returns true if no options were given when executing application.
	 * @return true if no options were given when executing application
	 */
	public boolean noOptionsGiven() {
		return commandLine.getOptions().length == 0;
	}
}
