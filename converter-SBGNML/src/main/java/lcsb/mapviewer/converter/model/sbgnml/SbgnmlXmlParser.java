package lcsb.mapviewer.converter.model.sbgnml;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.sbgn.ArcClazz;
import org.sbgn.GlyphClazz;
import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Arc.End;
import org.sbgn.bindings.Arc.Next;
import org.sbgn.bindings.Arc.Start;
import org.sbgn.bindings.Bbox;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Map;
import org.sbgn.bindings.Port;
import org.sbgn.bindings.Sbgn;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.bioEntity.element.species.SpeciesConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.converter.model.sbgnml.structures.Process;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.SplitOperator;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;

/**
 * This class is a parser for SBGN-ML files.
 * 
 * @author Michał Kuźma
 *
 */
public class SbgnmlXmlParser {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(SbgnmlXmlParser.class.getName());

  /**
   * List of all processes to be parsed.
   */
  private List<Process> processes = new ArrayList<Process>();

  /**
   * List of all logic operator glyphs parsed so far.
   */
  private List<Glyph> logicOperators = new ArrayList<Glyph>();

  /**
   * List of all logic arcs parsed so far.
   */
  private List<Arc> logicArcs = new ArrayList<Arc>();

  /**
   * Default margin for container name.
   */
  private static final double CONTAINER_NAME_MARGIN = 5.0;

  /**
   * Part of height of the line used to cross degraded circle that goes behind
   * this circle.
   */
  private static final int CROSS_LINE_EXTENDED_LENGTH = 7;

  /**
   * Default color for parsed compartments.
   */
  private static final Color COMPARTMENT_COLOR = new Color(0.5f, 0.5f, 1.0f);

  /**
   * Method used to create a model from SBGN-ML file.
   * 
   * @param params
   *          parameters needed for parsing file and creating a model
   * @return valid model parsed from the file
   * @throws InvalidInputDataExecption
   *           thrown when input file is invalid
   */
  public Model createModel(ConverterParams params) throws InvalidInputDataExecption {

    Model model = new ModelFullIndexed(null);
    model.setIdModel(params.getFilename());

    // Input file
    File inputSbgnmlFile = inputStreamToFile(params.getInputStream());

    Sbgn sbgnData;
    // Check if input file has valid SBGN-ML data
    try {
      if (!SbgnUtil.isValid(inputSbgnmlFile)) {
        throw new InvalidInputDataExecption("Given input is not a valid SBGN-ML file.");
      }

      // Read data from file
      sbgnData = SbgnUtil.readFromFile(inputSbgnmlFile);
    } catch (InvalidInputDataExecption ex) {
      throw ex;
    } catch (Exception ex) {
      throw new InvalidInputDataExecption("Unable to read given file.", ex);
    }

    // Extract map with all the glyphs and arcs
    Map map = sbgnData.getMap();

    adjustSizeOfElements(map);

    setModelSize(map, model);

    // Iterate over every glyph
    for (Glyph g : map.getGlyph()) {
      parseGlyph(g, model);
    }

    // Iterate over every arc in the map
    for (Arc a : map.getArc()) {
      parseArc(a, model);
    }

    // Parse all processes created before
    for (Process p : processes) {
      try {
        parseProcess(p, model);
      } catch (Exception e) {
        logger.warn("Unable to parse the process: " + p.getCentralPoint().getId());
      }
    }

    return model;
  }

  /**
   * Writes input stream into a File.
   * 
   * @param inputStream
   *          Input stream to be written to File
   * @return File handle
   */
  private File inputStreamToFile(InputStream inputStream) {
    File tempFile = null;
    try {
      tempFile = File.createTempFile("stream2file", ".tmp");
      tempFile.deleteOnExit();
      try (FileOutputStream out = new FileOutputStream(tempFile)) {
        IOUtils.copy(inputStream, out);
      }
    } catch (Exception ex) {
      logger.debug("Unable to create temporary file.");
      return null;
    }
    return tempFile;
  }

  /**
   * Method used to adjust the size of elements that have their size represented
   * different in the SBGN-ML file. Adjusted elements: Source and Sink / Degraded
   * elements, Multimers.
   * 
   * @param map
   *          Map parsed from SBGN-ML file
   */
  private void adjustSizeOfElements(Map map) {
    for (Glyph g : map.getGlyph()) {
      GlyphClazz glyphClazz = GlyphClazz.fromClazz(g.getClazz());
      switch (glyphClazz) {
      case SOURCE_AND_SINK:
        g.getBbox().setH(g.getBbox().getH() + 2 * CROSS_LINE_EXTENDED_LENGTH);
        g.getBbox().setW(g.getBbox().getW() + 2 * CROSS_LINE_EXTENDED_LENGTH);
        g.getBbox().setX(g.getBbox().getX() - CROSS_LINE_EXTENDED_LENGTH);
        g.getBbox().setY(g.getBbox().getY() - CROSS_LINE_EXTENDED_LENGTH);
        break;
      case COMPLEX_MULTIMER:
      case MACROMOLECULE_MULTIMER:
      case NUCLEIC_ACID_FEATURE_MULTIMER:
      case SIMPLE_CHEMICAL_MULTIMER:
        int cardinality;
        try {
          cardinality = getMultimerCardinality(g);
        } catch (Exception ex) {
          cardinality = 2;
          logger.warn(ex.getMessage() + " Set the default value of 2.");
        }
        g.getBbox().setW(g.getBbox().getW() + (cardinality - 1) * SpeciesConverter.HOMODIMER_OFFSET);
        g.getBbox().setH(g.getBbox().getH() + (cardinality - 1) * SpeciesConverter.HOMODIMER_OFFSET);
        break;
      default:
        break;
      }
    }
  }

  /**
   * Method used to compute height and width of the model.
   * 
   * @param map
   *          Map parsed from SBGN-ML file
   * @param model
   *          Model to be updated
   */
  private void setModelSize(Map map, Model model) {
    double minX = Double.MAX_VALUE;
    double minY = Double.MAX_VALUE;
    double maxX = 0;
    double maxY = 0;

    // Iterate over every glyph
    for (Glyph g : map.getGlyph()) {
      if (GlyphClazz.fromClazz(g.getClazz()).equals(GlyphClazz.ANNOTATION)
          || GlyphClazz.fromClazz(g.getClazz()).equals(GlyphClazz.TAG)) {
        continue;
      }
      Bbox bbox = g.getBbox();
      if (bbox != null) {
        if (bbox.getX() < minX) {
          minX = bbox.getX();
        }
        if (bbox.getY() < minY) {
          minY = bbox.getY();
        }
        if (bbox.getX() + bbox.getW() > maxX) {
          maxX = bbox.getX() + bbox.getW();
        }
        if (bbox.getY() + bbox.getH() > maxY) {
          maxY = bbox.getY() + bbox.getH();
        }
      }
    }

    // Iterate over every arc
    for (Arc a : map.getArc()) {
      // Since submaps are not yet supported, the arcs connecting them or
      // tags cannot be used when computing width and height of the model.
      boolean connectsTagOrSubmap = false;
      if (a.getSource() instanceof Glyph) {
        Glyph sourceGlyph = (Glyph) a.getSource();
        if (GlyphClazz.fromClazz(sourceGlyph.getClazz()).equals(GlyphClazz.TAG)
            || GlyphClazz.fromClazz(sourceGlyph.getClazz()).equals(GlyphClazz.SUBMAP)) {
          connectsTagOrSubmap = true;
        }
      }
      if (a.getTarget() instanceof Glyph) {
        Glyph targetGlyph = (Glyph) a.getTarget();
        if (GlyphClazz.fromClazz(targetGlyph.getClazz()).equals(GlyphClazz.TAG)
            || GlyphClazz.fromClazz(targetGlyph.getClazz()).equals(GlyphClazz.SUBMAP)) {
          connectsTagOrSubmap = true;
        }
      }
      // If the arc is not connecting a submap or a tag, check it's points
      if (!connectsTagOrSubmap) {
        Start aStart = a.getStart();
        if (aStart.getX() < minX) {
          minX = aStart.getX();
        }
        if (aStart.getX() > maxX) {
          maxX = aStart.getX();
        }
        if (aStart.getY() < minY) {
          minY = aStart.getY();
        }
        if (aStart.getY() > maxY) {
          maxY = aStart.getY();
        }
        End aEnd = a.getEnd();
        if (aEnd.getX() < minX) {
          minX = aEnd.getX();
        }
        if (aEnd.getX() > maxX) {
          maxX = aEnd.getX();
        }
        if (aEnd.getY() < minY) {
          minY = aEnd.getY();
        }
        if (aEnd.getY() > maxY) {
          maxY = aEnd.getY();
        }
        for (Next aNext : a.getNext()) {
          if (aNext.getX() < minX) {
            minX = aNext.getX();
          }
          if (aNext.getX() > maxX) {
            maxX = aNext.getX();
          }
          if (aNext.getY() < minY) {
            minY = aNext.getY();
          }
          if (aNext.getY() > maxY) {
            maxY = aNext.getY();
          }
        }
      }
    }

    model.setWidth(maxX + minX);
    model.setHeight(maxY + minY);
  }

  /**
   * Method used to parse a single glyph.
   * 
   * @param g
   *          glyph to be parsed
   * @param model
   *          model to be updated
   * @throws Exception
   */
  private void parseGlyph(Glyph g, Model model) {
    Species newSpecies = null;
    switch (GlyphClazz.fromClazz(g.getClazz())) {
    case AND:
    case NOT:
    case OR:
      logicOperators.add(g);
      break;
    case COMPARTMENT:
      parseCompartment(g, model);
      break;
    case COMPLEX:
      parseComplex(g, null, true, model);
      break;
    case COMPLEX_MULTIMER:
      parseComplex(g, null, false, model);
      break;
    case MACROMOLECULE:
      newSpecies = new GenericProtein(g.getId());
      parseSpecies(g, newSpecies, true, model);
      break;
    case MACROMOLECULE_MULTIMER:
      newSpecies = new GenericProtein(g.getId());
      parseSpecies(g, newSpecies, false, model);
      break;
    case NUCLEIC_ACID_FEATURE:
      if (isRNA(g)) {
        newSpecies = new Rna(g.getId());
      } else {
        newSpecies = new Gene(g.getId());
      }
      parseSpecies(g, newSpecies, true, model);
      break;
    case NUCLEIC_ACID_FEATURE_MULTIMER:
      if (isRNA(g)) {
        newSpecies = new Rna(g.getId());
      } else {
        newSpecies = new Gene(g.getId());
      }
      parseSpecies(g, newSpecies, false, model);
      break;
    case PERTURBING_AGENT:
      newSpecies = new Phenotype(g.getId());
      parseSpecies(g, newSpecies, true, model);
      break;
    case PHENOTYPE:
      newSpecies = new Phenotype(g.getId());
      parseSpecies(g, newSpecies, true, model);
      break;
    case ASSOCIATION:
    case DISSOCIATION:
    case OMITTED_PROCESS:
    case PROCESS:
    case UNCERTAIN_PROCESS:
      processes.add(new Process(g));
      break;
    case SIMPLE_CHEMICAL:
      newSpecies = new SimpleMolecule(g.getId());
      parseSpecies(g, newSpecies, true, model);
      break;
    case SIMPLE_CHEMICAL_MULTIMER:
      newSpecies = new SimpleMolecule(g.getId());
      parseSpecies(g, newSpecies, false, model);
      break;
    case SOURCE_AND_SINK:
      newSpecies = new Degraded(g.getId());
      parseSpecies(g, newSpecies, true, model);
      break;
    case TAG:
    case SUBMAP:
      logger.warn("Submaps are not supported. Glyph: " + g.getId() + " has not been parsed.");
    case UNSPECIFIED_ENTITY:
      newSpecies = new Unknown(g.getId());
      parseSpecies(g, newSpecies, true, model);
      break;
    default:
      logger.warn("The glyph " + g.getId() + " of class " + g.getClazz()
          + " has not been parsed, since it is invalid for SBGN PD map.");
      break;
    }
  }

  /**
   * Method used to parse a single arc.
   * 
   * @param a
   *          arc to be parsed
   * @param model
   *          model to be updated
   */
  private void parseArc(Arc a, Model model) {
    switch (ArcClazz.fromClazz(a.getClazz())) {
    case CONSUMPTION:
      Port arcTargetPort = (Port) a.getTarget();
      for (Process p : processes) {
        if (p.getCentralPoint().getPort().contains(arcTargetPort)) {
          p.addReagentArc(a);
          break;
        }
      }
      break;
    case PRODUCTION:
      Port arcSourcePort = (Port) a.getSource();
      for (Process p : processes) {
        if (p.getCentralPoint().getPort().contains(arcSourcePort)) {
          p.addProductArc(a);
          break;
        }
      }
      break;
    case EQUIVALENCE_ARC:
      logger.warn("Submaps are not supported. Equivalence arc: " + a.getId() + " has not been parsed.");
      break;
    case LOGIC_ARC:
      logicArcs.add(a);
      break;
    case CATALYSIS:
    case INHIBITION:
    case MODULATION:
    case NECESSARY_STIMULATION:
    case STIMULATION:
      Glyph targetGlyph = (Glyph) a.getTarget();
      if (GlyphClazz.fromClazz(targetGlyph.getClazz()).equals(GlyphClazz.PHENOTYPE)) {
        if (!(a.getSource() instanceof Glyph)) {
          logger.warn("Invalid phenotype arc: " + a.getId());
          break;
        }
        try {
          parsePhenotypeArc(a, model);
        } catch (Exception ex) {
          logger.warn(ex.getMessage());
        }
        break;
      }
      if (a.getSource() instanceof Glyph) {
        Glyph sourceGlyph = (Glyph) a.getSource();
        if (GlyphClazz.fromClazz(sourceGlyph.getClazz()).equals(GlyphClazz.PHENOTYPE)
            && (model.getElementByElementId(targetGlyph.getId()) instanceof Species)) {
          try {
            parsePhenotypeArc(a, model);
          } catch (InvalidArgumentException ex) {
            logger.warn("The arc " + a.getId() + " of class " + a.getClazz() + " is not a valid reduced notation arc.");
          }
          break;
        }
      }
      for (Process p : processes) {
        if (p.getCentralPoint().getId().equals(targetGlyph.getId())) {
          p.addInfluenceArc(a);
          break;
        }
      }
      break;
    default:
      logger.warn("The arc " + a.getId() + " of class " + a.getClazz()
          + " has not been parsed, since it is invalid for SBGN PD map.");
    }
  }

  /**
   * Method used to parse arc going to or from a phenotype.
   * 
   * @param a
   *          arc to be parsed
   * @param model
   *          model to be updated
   */
  private void parsePhenotypeArc(Arc a, Model model) {
    Reaction reaction;

    switch (ArcClazz.fromClazz(a.getClazz())) {

    case INHIBITION:
      reaction = new NegativeInfluenceReaction();
      break;
    case MODULATION:
      reaction = new ReducedModulationReaction();
      break;
    case NECESSARY_STIMULATION:
      reaction = new ReducedTriggerReaction();
      break;
    case STIMULATION:
      reaction = new ReducedPhysicalStimulationReaction();
      break;
    default:
      throw new InvalidArgumentException();
    }

    reaction.setIdReaction(a.getId());
    reaction.setModel(model);

    Reactant reactant = new Reactant();
    reactant.setReaction(reaction);
    Glyph source = (Glyph) a.getSource();
    reactant.setElement(model.getElementByElementId(source.getId()));
    List<Point2D> reactantPointList = new ArrayList<>();
    reactantPointList.add(new Point2D.Double(a.getStart().getX(), a.getStart().getY()));
    reactantPointList.add(new Point2D.Double(a.getStart().getX(), a.getStart().getY()));
    PolylineData reactantLine = new PolylineData(reactantPointList);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);
    atd.setArrowLineType(LineType.SOLID);
    reactantLine.setEndAtd(atd);
    reactant.setLine(reactantLine);
    reaction.addReactant(reactant);

    Product product = new Product();
    product.setReaction(reaction);
    Glyph target = (Glyph) a.getTarget();
    product.setElement(model.getElementByElementId(target.getId()));
    List<Point2D> productPointList = getLinePoints(a);
    PolylineData productLine = parseLine(a, productPointList);
    product.setLine(productLine);
    reaction.addProduct(product);

    model.addReaction(reaction);
  }

  /**
   * Method used to parse a single species.
   * 
   * @param g
   *          SBGN-ML glyph representing the species
   * @param newSpecies
   *          species to be parsed
   * @param isHomodimer
   *          true if parsed species is not multimer
   * @param model
   *          model to be updated
   */
  private void parseSpecies(Glyph g, Species newSpecies, boolean isHomodimer, Model model) {
    newSpecies.setModel(model);
    // If Glyph has label with a name, set it as Species name. If not, set
    // Id as name.
    if (g.getLabel() != null) {
      newSpecies.setName(g.getLabel().getText());
    } else {
      newSpecies.setName(g.getId());
    }
    // Add species to parent complex if there is one
    if (newSpecies.getComplex() != null) {
      newSpecies.getComplex().addSpecies(newSpecies);
    }
    // If the species is a multimer, set it's cardinality.
    if (!isHomodimer) {
      try {
        newSpecies.setHomodimer(getMultimerCardinality(g));
      } catch (Exception ex) {
        newSpecies.setHomodimer(2);
        logger.warn(ex.getMessage() + " Set the default value of 2.");
      }
    }

    List<Glyph> children = g.getGlyph();
    for (Glyph child : children) {
      if (GlyphClazz.fromClazz(child.getClazz()).equals(GlyphClazz.STATE_VARIABLE)) {
        if (child.getState() == null || child.getState().getVariable() != null) {
          try {
            parseStateVariable(child, g, newSpecies);
          } catch (Exception ex) {
            logger.warn(ex.getMessage());
          }
        } else {
          String structuralState = child.getState().getValue();
          if (newSpecies instanceof Protein) {
            Protein protein = (Protein) newSpecies;
            protein.setStructuralState(structuralState);
          } else if (newSpecies instanceof Complex) {
            Complex complex = (Complex) newSpecies;
            complex.setStructuralState(structuralState);
          }
        }
      }
    }

    parseSpecies(g, newSpecies, model);

    adjustModificationCoordinates(newSpecies);

  }

  /**
   * {@link ModificationResidue} in element might have slightly off coordinates
   * (due to different symbol shapes). For that we need to align them to match our
   * model.
   * 
   * @param species
   */
  protected void adjustModificationCoordinates(Species species) {
    if (species instanceof Protein) {
      Protein protein = (Protein) species;
      if (protein.getModificationResidues().size() > 0) {
        CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, true);
        for (ModificationResidue mr : protein.getModificationResidues()) {
          double angle = converter.getAngleForPoint(species, mr.getPosition());
          mr.setPosition(converter.getResidueCoordinates(species, angle));
        }
      }

    }
  }

  /**
   * Method used to create a new alias from SBGN-ML glyph.
   * 
   * @param glyph
   *          SBGN-ML glyph representing the alias
   * @param species
   *          species of the alias
   * @param model
   *          model to be updated
   */
  private void parseSpecies(Glyph glyph, Species species, Model model) {
    species.setHeight(new Double(glyph.getBbox().getH()));
    species.setWidth(new Double(glyph.getBbox().getW()));
    species.setX(new Double(glyph.getBbox().getX()));
    species.setY(new Double(glyph.getBbox().getY()));

    Compartment parentCompartment = null;
    if (glyph.getCompartmentRef() != null) {
      Glyph compartmentGlyph = (Glyph) glyph.getCompartmentRef();
      parentCompartment = model.getElementByElementId(compartmentGlyph.getId());
    } else if (species.getComplex() == null) {
      parentCompartment = findParentCompartment(species, model);
    }
    if (parentCompartment != null) {
      species.setCompartment(parentCompartment);
      parentCompartment.addElement(species);
    }

    // Parse units of information
    for (Glyph child : glyph.getGlyph()) {
      if (GlyphClazz.fromClazz(child.getClazz()).equals(GlyphClazz.UNIT_OF_INFORMATION)) {
        parseUnitOfInformation(child, species);
      }
    }

    model.addElement(species);
  }

  /**
   * Finds a compartment where element should be located (base on the
   * coordinates).
   * 
   * @param child
   *          {@link Element} for which we want to find compartment
   * @param model
   *          {@link Model} where we look for a compartment
   * @return parent {@link Compartment}
   */
  private Compartment findParentCompartment(Element child, Model model) {
    Compartment nullParent = new Compartment("null");
    nullParent.setWidth(Double.MAX_VALUE);
    nullParent.setHeight(Double.MAX_VALUE);
    nullParent.setX(0.0);
    nullParent.setY(0.0);
    Compartment parent = nullParent;
    for (Compartment potentialParent : model.getCompartments()) {
      if (potentialParent.contains(child)) {
        if (parent.getSize() > potentialParent.getSize()) {
          parent = potentialParent;
        }
      }
    }
    if (parent != nullParent) {
      return parent;
    } else {
      return null;
    }
  }

  /**
   * Method used to retrieve multimer cardinality from a multimer glyph.
   * 
   * @param g
   *          multimer glyph
   * @return multimer cardinality
   * @throws Exception
   *           Exception is thrown if no proper unit of information with
   *           cardinality was found
   */
  private int getMultimerCardinality(Glyph g) throws Exception {
    int multimerCardinality = 0;
    // Check all the children nodes looking for unit of information with
    // cardinality
    List<Glyph> children = g.getGlyph();
    for (Glyph child : children) {
      if (GlyphClazz.fromClazz(child.getClazz()) == GlyphClazz.UNIT_OF_INFORMATION) {
        String[] splitLabel = child.getLabel().getText().split(":");
        multimerCardinality = Integer.parseInt(splitLabel[1]);
      }
    }
    // If no unit of information was found, or the cardinality is invalid,
    // raise exception
    if (multimerCardinality <= 0) {
      throw new Exception(
          "No proper unit of information with multimer cardinality was found." + " Glyph: " + g.getId());
    }

    return multimerCardinality;
  }

  /**
   * Method used to decide if Nucleic-acid feature should be translated to RNA.
   * 
   * @param g
   *          Nucleic-acid feature glyph
   * @return true if input is RNA
   */
  private boolean isRNA(Glyph g) {
    boolean rna = false;
    // Check all the children nodes looking for unit of information
    List<Glyph> children = g.getGlyph();
    for (Glyph child : children) {
      if (GlyphClazz.fromClazz(child.getClazz()) == GlyphClazz.UNIT_OF_INFORMATION) {
        if (child.getLabel().getText().toLowerCase().contains("rna")) {
          rna = true;
        }
      }
    }

    return rna;
  }

  /**
   * Method used to parse state variable.
   * 
   * @param unitOfInformationGlyph
   *          unit of information glyph from sbgn-ml file
   * @param speciesGlyph
   *          glyph that the unit of information considers
   * @param newSpecies
   *          species that the unit of information considers
   * @throws Exception
   *           Exception is thrown if state variable is parsed for species other
   *           than Protein
   */
  private void parseStateVariable(Glyph unitOfInformationGlyph, Glyph speciesGlyph, Species newSpecies)
      throws Exception {
    if (!(newSpecies instanceof Protein)) {
      throw new Exception("Only macromolecule elements can have state variables.");
    }
    Protein protein = (Protein) newSpecies;
    Residue mr = new Residue();

    mr.setSpecies(protein);
    mr.setIdModificationResidue(unitOfInformationGlyph.getId());
    if (unitOfInformationGlyph.getState() != null) {
      // If State variable consists of value and variable
      mr.setName(unitOfInformationGlyph.getState().getVariable());
      for (ModificationState ms : ModificationState.values()) {
        if (ms.getAbbreviation().equals(unitOfInformationGlyph.getState().getValue())) {
          mr.setState(ms);
        }
      }
    }

    // Compute the angle from coordinates and dimensions
    double x = unitOfInformationGlyph.getBbox().getX() + unitOfInformationGlyph.getBbox().getW() / 2;
    double y = unitOfInformationGlyph.getBbox().getY() + unitOfInformationGlyph.getBbox().getH() / 2;

    mr.setPosition(new Point2D.Double(x, y));

    protein.addModificationResidue(mr);
  }

  /**
   * Method used for parsing units of information.
   * 
   * @param unitOfInformationGlyph
   *          unit of information glyph from sbgn-ml file
   * @param alias
   *          alias that the unit of information concerns
   */
  private void parseUnitOfInformation(Glyph unitOfInformationGlyph, Species alias) {
    String unitOfInformationText = unitOfInformationGlyph.getLabel().getText();
    if (unitOfInformationText.contains(":") && !unitOfInformationText.startsWith("N:")) {
      String unitOfInformationPrefix = unitOfInformationText.substring(0, unitOfInformationText.indexOf(':'));
      String unitOfInformationSuffix = unitOfInformationText.substring(unitOfInformationText.indexOf(':') + 1);
      alias.setStatePrefix(unitOfInformationPrefix);
      alias.setStateLabel(unitOfInformationSuffix);
    } else if (!unitOfInformationText.startsWith("N:")) {
      alias.setStateLabel(unitOfInformationText);
      alias.setStatePrefix("free input");
    }
  }

  /**
   * Method used for parsing complex species.
   * 
   * @param complexGlyph
   *          complex species glyph from sbgn-ml file
   * @param parentComplexSpecies
   *          parent complex species
   * @param isHomodimer
   *          set if the complex is a homodimer
   * @param model
   *          model to update with the parsed complex species
   */
  private void parseComplex(Glyph complexGlyph, Complex parentComplexSpecies, boolean isHomodimer, Model model) {
    Complex complexSpecies = new Complex(complexGlyph.getId());
    if (parentComplexSpecies != null) {
      complexSpecies.setComplex(parentComplexSpecies);
    }
    parseSpecies(complexGlyph, complexSpecies, isHomodimer, model);

    Complex complexAlias = model.getElementByElementId(complexGlyph.getId());
    for (Glyph child : complexGlyph.getGlyph()) {
      Species newSpecies;
      switch (GlyphClazz.fromClazz(child.getClazz())) {
      case UNSPECIFIED_ENTITY:
        newSpecies = new Unknown(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, true, model);
        break;
      case SIMPLE_CHEMICAL:
        newSpecies = new SimpleMolecule(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, true, model);
        break;
      case MACROMOLECULE:
        newSpecies = new GenericProtein(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, true, model);
        break;
      case NUCLEIC_ACID_FEATURE:
        newSpecies = new Gene(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, true, model);
        break;
      case SIMPLE_CHEMICAL_MULTIMER:
        newSpecies = new SimpleMolecule(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, false, model);
        break;
      case MACROMOLECULE_MULTIMER:
        newSpecies = new GenericProtein(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, false, model);
        break;
      case NUCLEIC_ACID_FEATURE_MULTIMER:
        newSpecies = new Gene(child.getId());
        newSpecies.setComplex(complexSpecies);
        parseSpecies(child, newSpecies, false, model);
        break;
      case COMPLEX:
        parseComplex(child, complexSpecies, true, model);
        break;
      case COMPLEX_MULTIMER:
        parseComplex(child, complexSpecies, false, model);
        break;
      default:
        break;
      }
      Species newAlias = model.getElementByElementId(child.getId());
      if (newAlias != null) {
        newAlias.setComplex(complexAlias);
        complexAlias.addSpecies((Species) newAlias);
      }
    }
  }

  /**
   * Method used to compute end point for modifier line.
   * 
   * @param a
   *          SBGN-ML modifier arc
   * @param reaction
   *          reaction to which the arc points
   * @return end point for modifier line
   */
  private Point2D getModifierEndPoint(Arc a, Reaction reaction) {
    ModifierTypeUtils utils = new ModifierTypeUtils();
    Point2D result;

    Line2D centerLine = reaction.getCenterLine();
    double dx = centerLine.getX2() - centerLine.getX1();
    double dy = centerLine.getY2() - centerLine.getY1();
    double centerLineAngle = Math.atan2(dy, dx);

    Point2D centerPoint = new Point2D.Double(centerLine.getX1() + dx / 2, centerLine.getY1() + dy / 2);

    // Retrieve second last point from the arc
    Point2D secondLast;
    if (a.getNext().isEmpty()) {
      secondLast = new Point2D.Double(a.getStart().getX(), a.getStart().getY());
    } else {
      Next temp = a.getNext().get(a.getNext().size() - 1);
      secondLast = new Point2D.Double(temp.getX(), temp.getY());
    }

    double dx2 = secondLast.getX() - centerPoint.getX();
    double dy2 = secondLast.getY() - centerPoint.getY();
    double modifierAngle = Math.atan2(dy2, dx2);

    double finalAngle = modifierAngle - centerLineAngle;
    while (finalAngle < -Math.PI) {
      finalAngle += 2 * Math.PI;
    }
    while (finalAngle > Math.PI) {
      finalAngle -= 2 * Math.PI;
    }
    String lineConnectionType = null;

    // CHECKSTYLE:OFF
    if (finalAngle < -Math.PI / 3 * 2) {
      lineConnectionType = "0,4";
    } else if (finalAngle < -Math.PI / 3) {
      lineConnectionType = "0,2";
    } else if (finalAngle < 0) {
      lineConnectionType = "0,5";
    } else if (finalAngle < Math.PI / 3) {
      lineConnectionType = "0,7";
    } else if (finalAngle < Math.PI / 3 * 2) {
      lineConnectionType = "0,3";
    } else {
      lineConnectionType = "0,6";
    }
    // CHECKSTYLE:ON

    result = utils.getAnchorPointOnReactionRect(reaction, lineConnectionType);

    return result;
  }

  /**
   * Method used to parse line points from SBGN-ML arc.
   * 
   * @param a
   *          SBGN-ML arc
   * @return list of line points
   */
  private List<Point2D> getLinePoints(Arc a) {
    List<Point2D> pointList = new ArrayList<>();
    Point2D startPoint = new Point2D.Double(a.getStart().getX(), a.getStart().getY());
    Point2D endPoint = new Point2D.Double(a.getEnd().getX(), a.getEnd().getY());
    pointList.add(startPoint);
    for (Next nextPoint : a.getNext()) {
      pointList.add(new Point2D.Double(nextPoint.getX(), nextPoint.getY()));
    }
    pointList.add(endPoint);
    return pointList;
  }

  /**
   * Method used for parsing lines from sbgn-ml arcs.
   * 
   * @param a
   *          SBGN-ML arc
   * @param pointList
   *          list of points for the line
   * @return line parsed from SBGN-ML arc
   */
  private PolylineData parseLine(Arc a, List<Point2D> pointList) {
    PolylineData line = new PolylineData(pointList);
    ArrowTypeData atd = new ArrowTypeData();

    switch (ArcClazz.fromClazz(a.getClazz())) {
    case CATALYSIS:
      atd = ModifierType.CATALYSIS.getAtd();
      break;
    case CONSUMPTION:
      atd.setArrowType(ArrowType.NONE);
      break;
    case INHIBITION:
      atd = ModifierType.INHIBITION.getAtd();
      break;
    case MODULATION:
      atd = ModifierType.MODULATION_STRING.getAtd();
      break;
    case NECESSARY_STIMULATION:
      atd = ModifierType.TRIGGER_STRING.getAtd();
      break;
    case PRODUCTION:
      atd.setArrowType(ArrowType.FULL);
      break;
    case STIMULATION:
      atd = ModifierType.PHYSICAL_STIMULATION.getAtd();
      break;
    case LOGIC_ARC:
      atd.setArrowType(ArrowType.NONE);
      break;
    default:
      throw new InvalidArgumentException("Wrong arc class.");
    }
    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd.copy());
    return line;
  }

  /**
   * Returns {@link AndOperator} for the reaction's reagents port.
   * 
   * @param p
   *          process of the reaction
   * @return operator for the reaction port
   */
  private AndOperator getReactionPortAndOperator(Process p) {
    AndOperator andOperator = new AndOperator();
    Glyph centralPoint = p.getCentralPoint();
    Double centralPointX = new Double(centralPoint.getBbox().getX() + centralPoint.getBbox().getW() / 2);
    Double centralPointY = new Double(centralPoint.getBbox().getY() + centralPoint.getBbox().getH() / 2);
    Point2D centerOfReactionPoint = new Point2D.Double(centralPointX, centralPointY);
    Point2D portPoint;
    if (p.getReagentArcs().size() > 1) {
      portPoint = new Point2D.Double(p.getReagentArcs().get(0).getEnd().getX(),
          p.getReagentArcs().get(0).getEnd().getY());
    } else {
      portPoint = new Point2D.Double(p.getRevReagentArcs().get(0).getStart().getX(),
          p.getRevReagentArcs().get(0).getStart().getY());
    }
    PolylineData line = new PolylineData(portPoint, centerOfReactionPoint);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);
    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd);
    andOperator.setLine(line);
    return andOperator;
  }

  /**
   * Returns {@link SplitOperator} for the reaction's products port.
   * 
   * @param p
   *          process of the reaction
   * @return operator for the reaction port
   */
  private SplitOperator getReactionPortSplitOperator(Process p) {
    SplitOperator splitOperator = new SplitOperator();
    Glyph centralPoint = p.getCentralPoint();
    Double centralPointX = new Double(centralPoint.getBbox().getX() + centralPoint.getBbox().getW() / 2);
    Double centralPointY = new Double(centralPoint.getBbox().getY() + centralPoint.getBbox().getH() / 2);
    Point2D centerOfReactionPoint = new Point2D.Double(centralPointX, centralPointY);
    Point2D portPoint;
    if (!p.isReversible()) {
      portPoint = new Point2D.Double(p.getProductArcs().get(0).getStart().getX(),
          p.getProductArcs().get(0).getStart().getY());
    } else {
      portPoint = new Point2D.Double(p.getRevProductArcs().get(0).getStart().getX(),
          p.getRevProductArcs().get(0).getStart().getY());
    }
    PolylineData line = new PolylineData(centerOfReactionPoint, portPoint);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);
    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd);
    splitOperator.setLine(line);
    return splitOperator;
  }

  /**
   * Returns instance of {@link Modifier} based on given {@link ArcClazz}.
   * 
   * @param ac
   *          {@link ArcClazz} defining the result
   * @return {@link Modifier} of class adequate to given ac
   * @throws Exception
   *           thrown when no adequate Modifier has been found
   */
  private Modifier getModifierFromArcClazz(ArcClazz ac) throws Exception {
    switch (ac) {
    case CATALYSIS:
      return new Catalysis();
    case INHIBITION:
      return new Inhibition();
    case MODULATION:
      return new Modulation();
    case NECESSARY_STIMULATION:
      return new Trigger();
    case STIMULATION:
      return new PhysicalStimulation();
    default:
      logger.warn("Modifier arc of invalid class.");
      throw new Exception("Wrong arc class.");
    }
  }

  /**
   * Returns center point for given process.
   * 
   * @param p
   *          completely parsed process to compute center point from
   * @return center point for given process
   */
  private Point2D getCenterPointFromProcess(Process p) {
    Double centralPointX = new Double(p.getCentralPoint().getBbox().getX() + p.getCentralPoint().getBbox().getW() / 2);
    Double centralPointY = new Double(p.getCentralPoint().getBbox().getY() + p.getCentralPoint().getBbox().getH() / 2);
    Point2D centerOfReactionPoint = new Point2D.Double(centralPointX, centralPointY);
    return centerOfReactionPoint;
  }

  /**
   * Returns proper Reaction object based on given glyph clazz.
   * 
   * @param glyphClazz
   *          clazz of the process glyph
   * @return Reaction object based on given glyph clazz
   */
  private Reaction getReactionFromProcessGlyphClazz(String glyphClazz) {
    switch (GlyphClazz.fromClazz(glyphClazz)) {
    case ASSOCIATION:
    case DISSOCIATION:
    case PROCESS:
      return new StateTransitionReaction();
    case OMITTED_PROCESS:
      return new KnownTransitionOmittedReaction();
    case UNCERTAIN_PROCESS:
      return new UnknownTransitionReaction();
    default:
      throw new InvalidArgumentException();

    }
  }

  /**
   * Method used for parsing processes into reactions.
   * 
   * @param p
   *          process to be parsed
   * @param model
   *          model to be updated with the parsed reaction
   * @throws Exception
   *           thrown when the process was invalid
   */
  private void parseProcess(Process p, Model model) throws Exception {
    if (p.getProductArcs().isEmpty()) {
      throw new Exception(p.getCentralPoint().getId() + ": The process must have at least one outgoing arc.");
    }
    p.setProductsPort((Port) p.getProductArcs().get(0).getSource());
    for (Arc productArc : p.getProductArcs()) {
      if (!((Port) productArc.getSource()).equals(p.getProductsPort())) {
        p.setReversible(true);
        p.setReagentsPort((Port) productArc.getSource());
      }
    }
    if (p.getReagentsPort() == null && !p.getReagentArcs().isEmpty()) {
      p.setReagentsPort((Port) p.getReagentArcs().get(0).getTarget());
    }

    if ((p.getReagentArcs().isEmpty() && !p.isReversible()) || (p.getRevReagentArcs().isEmpty() && p.isReversible())) {
      throw new Exception(p.getCentralPoint().getId() + ": The process must have at least one incoming arc.");
    }

    Reaction reaction;
    if (p.getCentralPoint() == null) {
      throw new Exception("Process has no central point.");
    }
    reaction = getReactionFromProcessGlyphClazz(p.getCentralPoint().getClazz());
    reaction.setIdReaction(p.getCentralPoint().getId());
    reaction.setModel(model);

    reaction.setReversible(p.isReversible());

    // If there are multiple inputs, add "AND" operator
    AndOperator andOperator = null;
    if (p.getReagentArcs().size() > 1 || p.getRevReagentArcs().size() > 1) {
      andOperator = getReactionPortAndOperator(p);
      andOperator.setReaction(reaction);
      reaction.addNode(andOperator);
    }

    // If there are multiple outputs, add Split operator
    SplitOperator splitOperator = null;
    if ((p.getProductArcs().size() > 1 && !p.isReversible())
        || (p.isReversible() && p.getRevProductArcs().size() > 1)) {
      splitOperator = getReactionPortSplitOperator(p);
      splitOperator.setReaction(reaction);
      reaction.addNode(splitOperator);
    }

    for (Arc a : p.getReagentArcs()) {
      Reactant reactant = new Reactant();
      reactant.setReaction(reaction);
      Glyph source = (Glyph) a.getSource();
      reactant.setElement(model.getElementByElementId(source.getId()));
      List<Point2D> pointList = getLinePoints(a);
      if (p.getReagentArcs().size() == 1) {
        pointList.add(getCenterPointFromProcess(p));
      } else {
        pointList.add(pointList.get(pointList.size() - 1));
      }
      PolylineData line = parseLine(a, pointList);
      reactant.setLine(line);
      if (andOperator != null) {
        andOperator.addInput(reactant);
      }

      reaction.addReactant(reactant);
    }

    for (Arc a : p.getProductArcs()) {
      if (((Port) a.getSource()).equals(p.getProductsPort())) {
        Product product = new Product();
        product.setReaction(reaction);
        Glyph target = (Glyph) a.getTarget();
        product.setElement(model.getElementByElementId(target.getId()));
        List<Point2D> pointList = getLinePoints(a);
        if (p.getRevProductArcs().size() >= 1) {
          pointList.add(0, getCenterPointFromProcess(p));
        } else {
          pointList.add(0, pointList.get(0));
        }
        PolylineData line = parseLine(a, pointList);
        product.setLine(line);
        if (splitOperator != null) {
          splitOperator.addOutput(product);
        }

        reaction.addProduct(product);
      } else {
        Reactant reactant = new Reactant();
        reactant.setReaction(reaction);
        Glyph source = (Glyph) a.getTarget();
        reactant.setElement(model.getElementByElementId(source.getId()));
        List<Point2D> pointList = getLinePoints(a);
        if (p.getRevReagentArcs().size() <= 1) {
          pointList.add(0, getCenterPointFromProcess(p));
        } else {
          pointList.add(0, pointList.get(0));
        }
        PolylineData line = parseLine(a, pointList);
        line = line.reverse();
        reactant.setLine(line);
        if (andOperator != null) {
          andOperator.addInput(reactant);
        }

        reaction.addReactant(reactant);
      }
    }
    for (Arc a : p.getModifierArcs()) {
      Modifier modifier = null;
      try {
        modifier = getModifierFromArcClazz(ArcClazz.fromClazz(a.getClazz()));
      } catch (Exception ex) {
        logger.warn("Unable to add modifier");
        continue;
      }
      if (a.getSource() instanceof Glyph) {
        Glyph sourceGlyph = (Glyph) a.getSource();
        Species modifierAlias = (Species) model.getElementByElementId(sourceGlyph.getId());
        modifier.setElement(modifierAlias);
        List<Point2D> pointList = getLinePoints(a);
        pointList.remove(pointList.size() - 1);
        pointList.add(getModifierEndPoint(a, reaction));
        PolylineData line = parseLine(a, pointList);
        modifier.setLine(line);

        reaction.addModifier(modifier);
      } else if (a.getSource() instanceof Port) {
        // Logic operator
        try {
          parseLogicOperator(a, reaction, ArcClazz.fromClazz(a.getClazz()), null, model);
        } catch (Exception ex) {
          logger.warn(ex.getMessage());
        }
      }

    }

    model.addReaction(reaction);
  }

  /**
   * Returns {@link ArrowTypeData} based on given {@link ArcClazz}.
   * 
   * @param ac
   *          input arc class
   * @return ArrowTypeData based on input arrow class
   * @throws Exception
   *           thrown when invalid arc class was given on input
   */
  private ArrowTypeData getAtdFromArcClazz(ArcClazz ac) throws Exception {
    ArrowTypeData atd = new ArrowTypeData();
    switch (ac) {
    case CATALYSIS:
      atd = ModifierType.CATALYSIS.getAtd();
      break;
    case CONSUMPTION:
      atd.setArrowType(ArrowType.NONE);
      break;
    case INHIBITION:
      atd = ModifierType.INHIBITION.getAtd();
      break;
    case MODULATION:
      atd = ModifierType.MODULATION_STRING.getAtd();
      break;
    case NECESSARY_STIMULATION:
      atd = ModifierType.TRIGGER_STRING.getAtd();
      break;
    case PRODUCTION:
      atd.setArrowType(ArrowType.FULL);
      break;
    case STIMULATION:
      atd = ModifierType.PHYSICAL_STIMULATION.getAtd();
      break;
    case LOGIC_ARC:
      atd.setArrowType(ArrowType.NONE);
      break;
    default:
      throw new Exception("Wrong arc class.");
    }
    return atd.copy();
  }

  /**
   * Method used for parsing logic operators.
   * 
   * @param arc
   *          arc with a starting point in the logic operator
   * @param reaction
   *          reaction that the logic operator is a part of
   * @param modifierClass
   *          determines type of source species
   * @param targetOperator
   *          target logic operator
   * @param model
   *          model of the map
   * @throws Exception
   *           thrown when parsed logic operator is invalid
   */
  private void parseLogicOperator(Arc arc, Reaction reaction, ArcClazz modifierClass, NodeOperator targetOperator,
      Model model) throws Exception {
    Port operatorPort = (Port) arc.getSource();
    Glyph logicOperator = null;
    for (Glyph lo : logicOperators) {
      if (lo.getPort().contains(operatorPort)) {
        logicOperator = lo;
      }
    }
    if (logicOperator == null) {
      throw new Exception("Missing logic operator for logic arc: " + arc.getId());
    }

    // LogicOperator is valid for CellDesigner only if it has exactly 2
    // inputs of Species
    final Glyph tempLogicOperator = logicOperator;
    boolean isCellDesignerValidLogicOperator = logicArcs.stream()
        .filter(a -> tempLogicOperator.getPort().contains(a.getTarget()) && !(a.getSource() instanceof Port))
        .count() == 2;
    if (!isCellDesignerValidLogicOperator) {
      throw new Exception("Parsed operator is not valid for CellDesigner: " + logicOperator.getId());
    }

    NodeOperator operator;
    switch (GlyphClazz.fromClazz(logicOperator.getClazz())) {
    case AND:
      operator = new AndOperator();
      break;
    case OR:
      operator = new OrOperator();
      break;
    case NOT:
      logger.warn(
          "NOT gates are not implemented in the platform. Glyph: " + logicOperator.getId() + " has not been parsed.");
      return;
    default:
      throw new Exception("Wrong logic operator class.");
    }

    // Parse line from arc and operator glyph
    Point2D operatorCenterPoint = new Point2D.Double(
        logicOperator.getBbox().getX() + logicOperator.getBbox().getW() / 2,
        logicOperator.getBbox().getY() + logicOperator.getBbox().getH() / 2);

    List<Point2D> linePoints = getLinePoints(arc);
    new ArrayList<Point2D>();

    if (targetOperator == null) {
      linePoints.remove(linePoints.size() - 1);
      linePoints.add(getModifierEndPoint(arc, reaction));
    }

    // Check if operator port is in the line from center point of the
    // operator. If so, remove that redundant point.
    double dx, dy;
    dx = linePoints.get(0).getX() - operatorCenterPoint.getX();
    dy = linePoints.get(0).getY() - operatorCenterPoint.getY();
    double dx2, dy2;
    if (arc.getNext().isEmpty()) {
      dx2 = linePoints.get(linePoints.size() - 1).getX() - linePoints.get(0).getX();
      dy2 = linePoints.get(linePoints.size() - 1).getY() - linePoints.get(0).getY();
    } else {
      dx2 = arc.getNext().get(0).getX() - linePoints.get(0).getX();
      dy2 = arc.getNext().get(0).getY() - linePoints.get(0).getY();
    }
    DoubleComparator doubleComparator = new DoubleComparator();
    if (doubleComparator.compare(dy / dx, dy2 / dx2) == 0) {
      linePoints.remove(0);
    }
    linePoints.add(0, operatorCenterPoint);

    PolylineData line = new PolylineData(linePoints);
    ArrowTypeData atd = getAtdFromArcClazz(ArcClazz.fromClazz(arc.getClazz()));

    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd);

    operator.setLine(line);
    operator.setReaction(reaction);

    if (targetOperator != null) {
      operator.addOutput(targetOperator);
      targetOperator.addInput(operator);
    }

    for (Arc logicArc : logicArcs) {
      if (logicOperator.getPort().contains(logicArc.getTarget())) {
        if (logicArc.getSource() instanceof Port) {
          // The arc has source in logic operator
          logger.warn("Logic operators trees are not compatible with CellDesigner. Therefore they are not supported.");
          continue;
          // parseLogicOperator(logicArc, reaction, modifierClass,
          // operator, model);
        } else {
          Modifier modifier;

          switch (modifierClass) {
          case CATALYSIS:
            modifier = new Catalysis();
            break;
          case INHIBITION:
            modifier = new Inhibition();
            break;
          case MODULATION:
            modifier = new Modulation();
            break;
          case NECESSARY_STIMULATION:
            modifier = new Trigger();
            break;
          case STIMULATION:
            modifier = new PhysicalStimulation();
            break;
          default:
            throw new Exception("Wrong arc class.");
          }

          Glyph sourceGlyph = (Glyph) logicArc.getSource();
          Species modifierAlias = (Species) model.getElementByElementId(sourceGlyph.getId());
          modifier.setElement(modifierAlias);
          List<Point2D> pointList = getLinePoints(logicArc);
          pointList.add(operatorCenterPoint);
          PolylineData newLine = parseLine(logicArc, pointList);
          modifier.setLine(newLine);

          operator.addInput(modifier);

          reaction.addModifier(modifier);
        }
      }
    }
    reaction.addNode(operator);
  }

  /**
   * Method used for parsing compartments.
   * 
   * @param glyph
   *          compartment glyph from sbgn-ml file
   * @param model
   *          model to be updated with the parsed compartment
   */
  private void parseCompartment(Glyph glyph, Model model) {

    Compartment compartment = new SquareCompartment(glyph.getId());
    if (glyph.getLabel() != null) {
      compartment.setName(glyph.getLabel().getText());
    }
    compartment.setModel(model);
    compartment.setHeight(new Double(glyph.getBbox().getH()));
    compartment.setWidth(new Double(glyph.getBbox().getW()));
    compartment.setX(new Double(glyph.getBbox().getX()));
    compartment.setY(new Double(glyph.getBbox().getY()));
    compartment.setThickness(1.0);
    compartment.setColor(COMPARTMENT_COLOR);

    if (glyph.getLabel() != null && glyph.getLabel().getBbox() != null) {
      compartment.setNamePoint(glyph.getLabel().getBbox().getX(), glyph.getLabel().getBbox().getY());
    } else {
      compartment.setNamePoint(compartment.getX() + compartment.getThickness() + CONTAINER_NAME_MARGIN,
          compartment.getY() + compartment.getThickness() + CONTAINER_NAME_MARGIN);
    }
    Compartment parent = findParentCompartment(compartment, model);
    if (parent != null) {
      compartment.setCompartment(parent);
      parent.addElement(compartment);
    }

    model.addElement(compartment);
  }
}
