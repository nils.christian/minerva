package lcsb.mapviewer.converter.model.sbgnml.console;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter;
import lcsb.mapviewer.converter.model.sbgnml.console.gui.GraphicalInterface;
import lcsb.mapviewer.model.map.model.Model;

/**
 * This class is entry point for console tool to convert data from CellDesigner
 * file to SBGN-ML format and back.
 * 
 * @author Michał Kuźma
 *
 */
public class SbgnmlConsoleConverter {

	/**
	 * Default class logger.
	 */
	private final Logger		 logger	= Logger.getLogger(SbgnmlConsoleConverter.class.getName());

	/**
	 * Options from the input.
	 */
	private SbgnmlRunOptions inputOptions;

	/**
	 * Default constructor.
	 * 
	 * @param args
	 *          parameters with which the program was run
	 */
	public SbgnmlConsoleConverter(String[] args) {
		inputOptions = new SbgnmlRunOptions(args);
	}

	/**
	 * Main entry point to the program.
	 * 
	 * @param args
	 *          parameters with which the program was run
	 */
	public static void main(String[] args) {
		SbgnmlConsoleConverter main = new SbgnmlConsoleConverter(args);
		try {
			main.run();
		} catch (Exception e) {
			main.logger.error(e, e);
		}
	}

	/**
	 * Method used to convert data from one file format to another.
	 * 
	 * @param inputFilename
	 *          path to the input file
	 * @param outputFilename
	 *          path to the output file
	 * @param reversed
	 *          true if conversion should be done from CellDesigner to SBGN-ML
	 *          file
	 * @throws InvalidInputDataExecption
	 *           thrown when there is a problem with conversion
	 */
	public static void convert(String inputFilename, String outputFilename, boolean reversed) throws InvalidInputDataExecption {
		IConverter parser;
		IConverter exporter;
		if (reversed) {
			parser = new CellDesignerXmlParser();
			exporter = new SbgnmlXmlConverter();
		} else {
			parser = new SbgnmlXmlConverter();
			exporter = new CellDesignerXmlParser();
		}

		Model model;
		try {
			model = parser.createModel(new ConverterParams().filename(inputFilename));
		} catch (FileNotFoundException e) {
			throw new InvalidInputDataExecption("Problem with input file: " + inputFilename, e);
		}

		try {
			exporter.exportModelToFile(model, outputFilename);
		} catch (Exception e) {
			throw new InvalidInputDataExecption("Problem with output file: " + outputFilename, e);
		}
	}

	/**
	 * This method transform {@link #inputOptions input data}.
	 * 
	 * @throws InvalidInputDataExecption
	 *           thrown when the input data are invalid
	 */
	private void run() throws InvalidInputDataExecption {
		if (inputOptions.noOptionsGiven()) {
			GraphicalInterface gui = new GraphicalInterface();
			gui.execute();
		} else {
			if (!inputOptions.isValidInput() || inputOptions.isHelpOption()) {
				if (!inputOptions.isValidInput()) {
					logger.warn("Invalid input data");
				}
				inputOptions.printHelp();
			} else {
				logger.debug("Running...");

				convert(inputOptions.getInputFilename(), inputOptions.getOutputFilename(), inputOptions.isReversedOption());
			}
		}
	}
}
