package lcsb.mapviewer.converter.model.sbgnml;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.sbgn.ArcClazz;
import org.sbgn.GlyphClazz;
import org.sbgn.Language;
import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Arc.End;
import org.sbgn.bindings.Arc.Next;
import org.sbgn.bindings.Arc.Start;
import org.sbgn.bindings.Bbox;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Label;
import org.sbgn.bindings.Map;
import org.sbgn.bindings.Port;
import org.sbgn.bindings.Sbgn;

import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.graphics.bioEntity.reaction.ReactionConverter;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.AssociationOperator;
import lcsb.mapviewer.model.map.reaction.DissociationOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.SplitOperator;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Class used to export SBGN-ML files from the model.
 * 
 * @author Michał Kuźma
 *
 */
public class SbgnmlXmlExporter {

  /**
   * Helps in providing human readable identifiers of elements for logging.
   */
  private ElementUtils eu = new ElementUtils();

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(SbgnmlXmlExporter.class.getName());

  /**
   * Counter of the arcs parsed so far, used in generating arc's id.
   */
  private int arcCounter;

  /**
   * Set of all the operators parsed so far, used in generating operator's id.
   */
  private Set<NodeOperator> parsedOperators;

  /**
   * Map of operator IDs used when parsing arcs targeting an operator.
   */
  private java.util.Map<NodeOperator, String> operatorIds;

  /**
   * Map of all glyphs and ports generated so far with id as key.
   */
  private java.util.Map<String, Object> sourceTargetMap;

  /**
   * Side margin for units of information.
   */
  private static final double UNIT_OF_INFORMATION_MARGIN = 10.0;

  /**
   * Height of generated units of information.
   */
  private static final double UNIT_OF_INFORMATION_HEIGHT = 12.0;

  /**
   * Height and width of generated operators.
   */
  private static final double OPERATOR_SIZE = 40.0;

  /**
   * Distance between operator circle and port point.
   */
  private static final double OPERATOR_PORT_DISTANCE = 20.0;

  /**
   * Distance between process glyph and port point.
   */
  private static final double PROCESS_PORT_DISTANCE = 10.0;

  /**
   * Length of random alphabetic string added in the begining of ID, if it is a
   * number. SBGN-ML doesn't accept numbers as ID.
   */
  private static final int ID_RANDOM_STRING_LENGTH = 5;

  /**
   * Transforms model into SBGN-ML xml.
   * 
   * @param model
   *          model that should be transformed
   * @return SBGM-ML xml string for the model
   */
  public Sbgn toSbgnml(Model model) {
    // Reset global variables
    arcCounter = 0;
    parsedOperators = new HashSet<>();
    operatorIds = new HashMap<>();
    sourceTargetMap = new HashMap<>();

    Map map = new Map();
    map.setLanguage(Language.PD.getName());
    Sbgn sbgnData = new Sbgn();

    List<Species> aliases = model.getSpeciesList();

    for (Species a : aliases) {
      if (a.getComplex() == null) {
        Glyph newGlyph = aliasToGlyph(a);
        map.getGlyph().add(newGlyph);
      }
    }

    for (Reaction reaction : model.getReactions()) {
      try {
        map.getGlyph().add(getProcessGlyphFromReaction(reaction));
      } catch (InvalidArgumentException ex) {
        // Reduced notation
        try {
          map.getArc().add(getArcFromReducedReaction(reaction));
        } catch (InvalidArgumentException e) {
          logger.warn("Invalid arc type."
              + " Reduced notation reaction found of type that is not compliant with SBGN-ML format." + " Reaction: "
              + reaction.getIdReaction());
        }
        continue;
      }
      map.getArc().addAll(getArcsFromReaction(reaction, map.getGlyph()));
    }

    sbgnData.setMap(map);

    return sbgnData;
  }

  /**
   * Creates new glyph element with all parameters from given alias.
   * 
   * @param alias
   *          alias with all parameters for the glyph
   * @return newly created glyph
   */
  private Glyph aliasToGlyph(Element alias) {
    Glyph newGlyph = new Glyph();
    boolean idIsANumber = true;
    try {
      Integer.parseInt(alias.getElementId().substring(0, 1));
    } catch (NumberFormatException e) {
      idIsANumber = false;
    }
    if (idIsANumber) {
      newGlyph.setId(RandomStringUtils.randomAlphabetic(ID_RANDOM_STRING_LENGTH).concat(alias.getElementId()));
    } else {
      newGlyph.setId(alias.getElementId());
    }
    newGlyph.setClazz(getGlyphClazzFromElement(alias).getClazz());
    newGlyph.setLabel(getGlyphLabelFromAlias(alias));

    Bbox bbox = new Bbox();
    bbox.setX(alias.getX().floatValue());
    bbox.setY(alias.getY().floatValue());
    bbox.setW(alias.getWidth().floatValue());
    bbox.setH(alias.getHeight().floatValue());
    newGlyph.setBbox(bbox);

    if (GlyphClazz.fromClazz(newGlyph.getClazz()).equals(GlyphClazz.MACROMOLECULE)
        || GlyphClazz.fromClazz(newGlyph.getClazz()).equals(GlyphClazz.MACROMOLECULE_MULTIMER)) {
      Protein protein = (Protein) alias;
      for (ModificationResidue mr : protein.getModificationResidues()) {
        Glyph stateVariableGlyph = parseStateVariable(mr, newGlyph);
        stateVariableGlyph.setId(newGlyph.getId().concat("-").concat(stateVariableGlyph.getId()));
        newGlyph.getGlyph().add(stateVariableGlyph);
      }
    }

    Glyph unitOfInformationGlyph = getUnitOfInformationGlyph(alias);
    if (unitOfInformationGlyph != null) {
      newGlyph.getGlyph().add(unitOfInformationGlyph);
    }

    if (alias instanceof Complex) {
      Complex complexAlias = (Complex) alias;
      for (Species a : complexAlias.getElements()) {
        Glyph childGlyph = aliasToGlyph(a);
        newGlyph.getGlyph().add(childGlyph);
      }
    }

    sourceTargetMap.put(newGlyph.getId(), newGlyph);
    return newGlyph;
  }

  /**
   * Creates new glyph element with all parameters from given operator.
   * 
   * @param operator
   *          operator with all parameters for the glyph
   * @return newly created glyph
   */
  private Glyph operatorToGlyph(NodeOperator operator) {
    Glyph newGlyph = new Glyph();
    newGlyph.setId("operator".concat(Integer.toString(parsedOperators.size())));
    parsedOperators.add(operator);
    operatorIds.put(operator, newGlyph.getId());
    if (operator instanceof AndOperator) {
      newGlyph.setClazz(GlyphClazz.AND.getClazz());
    } else if (operator instanceof OrOperator) {
      newGlyph.setClazz(GlyphClazz.OR.getClazz());
    }

    Bbox bbox = new Bbox();
    bbox.setX((float) (operator.getLine().getPoints().get(0).getX() - OPERATOR_SIZE / 2));
    bbox.setY((float) (operator.getLine().getPoints().get(0).getY() - OPERATOR_SIZE / 2));
    bbox.setW((float) OPERATOR_SIZE);
    bbox.setH((float) OPERATOR_SIZE);
    newGlyph.setBbox(bbox);

    Port port2 = new Port();
    port2.setId(newGlyph.getId().concat(".2"));
    Point2D centerPoint = operator.getLine().getPoints().get(0);
    Point2D nextPoint = operator.getLine().getPoints().get(1);
    double dx = nextPoint.getX() - centerPoint.getX();
    double dy = nextPoint.getY() - centerPoint.getY();
    double portToCenterDistance = OPERATOR_SIZE / 2 + OPERATOR_PORT_DISTANCE;

    double dx2, dy2;
    if (dx != 0) {
      dx2 = Math.sqrt(Math.pow(portToCenterDistance, 2) / (1 + Math.pow(dy / dx, 2)));
      dy2 = dx2 * dy / dx;
    } else {
      dx2 = 0;
      if (dy > 0) {
        dy2 = portToCenterDistance;
      } else {
        dy2 = -portToCenterDistance;
      }
    }

    port2.setX((float) (centerPoint.getX() + dx2));
    port2.setY((float) (centerPoint.getY() + dy2));
    sourceTargetMap.put(port2.getId(), port2);
    newGlyph.getPort().add(port2);

    Port port1 = new Port();
    port1.setId(newGlyph.getId().concat(".1"));
    Point2D portPoint1 = new Point2D.Double(2 * centerPoint.getX() - port2.getX(),
        2 * centerPoint.getY() - port2.getY());
    port1.setX((float) portPoint1.getX());
    port1.setY((float) portPoint1.getY());
    sourceTargetMap.put(port1.getId(), port1);
    newGlyph.getPort().add(port1);

    sourceTargetMap.put(newGlyph.getId(), newGlyph);

    return newGlyph;
  }

  /**
   * Returns GlyphClazz adequate for given element, based on it's class.
   * 
   * @param element
   *          element to extract GlyphCLazz from
   * @return GlyphClazz adequate for given alias
   */
  private GlyphClazz getGlyphClazzFromElement(Element element) {
    if (element instanceof Protein) {
      Protein protein = (Protein) element;
      if (protein.getHomodimer() == 1) {
        return GlyphClazz.MACROMOLECULE;
      } else {
        return GlyphClazz.MACROMOLECULE_MULTIMER;
      }
    }

    if (element instanceof SimpleMolecule) {
      SimpleMolecule simpleMolecule = (SimpleMolecule) element;
      if (simpleMolecule.getHomodimer() == 1) {
        return GlyphClazz.SIMPLE_CHEMICAL;
      } else {
        return GlyphClazz.SIMPLE_CHEMICAL_MULTIMER;
      }
    }

    if (element instanceof Ion) {
      Ion ion = (Ion) element;
      if (ion.getHomodimer() == 1) {
        return GlyphClazz.SIMPLE_CHEMICAL;
      } else {
        return GlyphClazz.SIMPLE_CHEMICAL_MULTIMER;
      }
    }

    if (element instanceof Gene) {
      Gene gene = (Gene) element;
      if (gene.getHomodimer() == 1) {
        return GlyphClazz.NUCLEIC_ACID_FEATURE;
      } else {
        return GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER;
      }
    }

    if (element instanceof Rna) {
      Rna rna = (Rna) element;
      if (rna.getHomodimer() == 1) {
        return GlyphClazz.NUCLEIC_ACID_FEATURE;
      } else {
        return GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER;
      }
    }

    if (element instanceof AntisenseRna) {
      AntisenseRna rna = (AntisenseRna) element;
      if (rna.getHomodimer() == 1) {
        return GlyphClazz.NUCLEIC_ACID_FEATURE;
      } else {
        return GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER;
      }
    }

    if (element instanceof Complex) {
      Complex complexSpecies = (Complex) element;
      if (complexSpecies.getHomodimer() == 1) {
        return GlyphClazz.COMPLEX;
      } else {
        return GlyphClazz.COMPLEX_MULTIMER;
      }
    }

    if (element instanceof Degraded) {
      return GlyphClazz.SOURCE_AND_SINK;
    }

    if (element instanceof Phenotype) {
      return GlyphClazz.PHENOTYPE;
    }

    if (element instanceof Compartment) {
      return GlyphClazz.COMPARTMENT;
    }
    if (element instanceof Unknown) {
      return GlyphClazz.UNSPECIFIED_ENTITY;
    }

    logger.warn(eu.getElementTag(element)
        + "Element type is not supported by SBGN-ML format. Unspecified Entity type assigned.");
    return GlyphClazz.UNSPECIFIED_ENTITY;
  }

  /**
   * Returns label for a glyph with name extracted from given element.
   * 
   * @param element
   *          element with the name
   * @return label for a glyph
   */
  private Label getGlyphLabelFromAlias(Element element) {
    Label label = new Label();
    label.setText(element.getName());
    return label;
  }

  /**
   * Returns state variable glyph parsed from {@link ModificationResidue}.
   * 
   * @param mr
   *          {@link ModificationResidue} to be parsed
   * @param parentGlyph
   *          parent glyph for the state variable
   * @return state variable glyph
   */
  private Glyph parseStateVariable(ModificationResidue mr, Glyph parentGlyph) {
    Glyph glyph = new Glyph();
    glyph.setId(mr.getIdModificationResidue());
    glyph.setClazz(GlyphClazz.STATE_VARIABLE.getClazz());

    if (mr instanceof AbstractSiteModification) {
      AbstractSiteModification modification = (AbstractSiteModification) mr;
      if (modification.getState() != null) {
        Glyph.State state = new Glyph.State();
        state.setValue(modification.getState().getAbbreviation());
        state.setVariable(mr.getName());
        glyph.setState(state);
      }
    }

    Bbox bbox = new Bbox();

    final float filledWidth = 70.0f;
    final float filledHeight = 28.0f;
    final float emptyWidth = 20.0f;
    final float emptyHeight = 22.0f;
    if (glyph.getState() != null) {
      bbox.setH(filledHeight);
      bbox.setW(filledWidth);
    } else {
      bbox.setH(emptyHeight);
      bbox.setW(emptyWidth);
    }

    bbox.setX((float) mr.getPosition().getX());
    bbox.setY((float) mr.getPosition().getY());

    glyph.setBbox(bbox);

    return glyph;
  }

  /**
   * Returns glyph with unit of information extracted from the alias or null if no
   * unit of information was found.
   * 
   * @param alias
   *          input alias
   * @return glyph with unit of information extracted from the alias or null if no
   *         unit of information was found
   */
  private Glyph getUnitOfInformationGlyph(Element alias) {
    Glyph uoiGlyph = null;

    String uoiText = "";
    if (alias instanceof Species) {
      Species species = (Species) alias;
      int homodir = species.getHomodimer();
      if (homodir > 1) {
        uoiText = "N:".concat(Integer.toString(homodir));
      }
    }

    if (alias instanceof TruncatedProtein) {
      if (!uoiText.equals("")) {
        uoiText = uoiText.concat("; ");
      }
      uoiText = uoiText.concat("ct:truncatedProtein");
    }

    if (alias instanceof Species) {
      Species speciesAlias = (Species) alias;
      if ((speciesAlias.getStateLabel() != null) && (speciesAlias.getStatePrefix() != null)) {
        if (!uoiText.equals("")) {
          uoiText = uoiText.concat("; ");
        }
        if (!speciesAlias.getStatePrefix().equals("free input")) {
          uoiText = uoiText.concat(speciesAlias.getStatePrefix()).concat(":");
        }
        uoiText = uoiText.concat(speciesAlias.getStateLabel());
      }
    }

    if (!uoiText.contains("ct:")) {
      if (alias instanceof Rna) {
        if (!uoiText.equals("")) {
          uoiText = uoiText.concat("; ");
        }
        uoiText = uoiText.concat("ct:RNA");
      }
      if (alias instanceof AntisenseRna) {
        if (!uoiText.equals("")) {
          uoiText = uoiText.concat("; ");
        }
        uoiText = uoiText.concat("ct:antisenseRNA");
      }
      if (alias instanceof Gene) {
        if (!uoiText.equals("")) {
          uoiText = uoiText.concat("; ");
        }
        uoiText = uoiText.concat("ct:gene");
      }
    }

    if (!uoiText.equals("")) {
      uoiGlyph = new Glyph();
      uoiGlyph.setClazz(GlyphClazz.UNIT_OF_INFORMATION.getClazz());
      uoiGlyph.setId(alias.getElementId().concat("uoi"));

      Label label = new Label();
      label.setText(uoiText);
      uoiGlyph.setLabel(label);

      Bbox bbox = new Bbox();
      bbox.setX((float) (alias.getX() + UNIT_OF_INFORMATION_MARGIN));
      bbox.setY((float) (alias.getY() - UNIT_OF_INFORMATION_HEIGHT / 2));
      bbox.setH((float) UNIT_OF_INFORMATION_HEIGHT);
      bbox.setW((float) (alias.getWidth() - 2 * UNIT_OF_INFORMATION_MARGIN));
      uoiGlyph.setBbox(bbox);
    }

    return uoiGlyph;
  }

  /**
   * Returns process glyph created based on reaction's center point.
   * 
   * @param reaction
   *          reaction to be parsed
   * @return process glyph for given reaction
   */
  private Glyph getProcessGlyphFromReaction(Reaction reaction) {
    Glyph processGlyph = new Glyph();
    processGlyph.setId(reaction.getIdReaction());
    processGlyph.setClazz(getGlyphClazzFromReaction(reaction).getClazz());
    Bbox bbox = new Bbox();

    Line2D line = reaction.getCenterLine();
    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double pointX = (startPoint.getX() + endPoint.getX()) / 2;
    double pointY = (startPoint.getY() + endPoint.getY()) / 2;
    Point2D pointNW = new Point2D.Double(pointX - ReactionConverter.RECT_SIZE / 2,
        pointY - ReactionConverter.RECT_SIZE / 2);
    bbox.setX((float) pointNW.getX());
    bbox.setY((float) pointNW.getY());
    bbox.setH((float) ReactionConverter.RECT_SIZE);
    bbox.setW((float) ReactionConverter.RECT_SIZE);

    processGlyph.setBbox(bbox);

    Port reactantPort = new Port();
    reactantPort.setId(reaction.getIdReaction().concat(".1"));
    sourceTargetMap.put(reactantPort.getId(), reactantPort);

    Port productPort = new Port();
    productPort.setId(reaction.getIdReaction().concat(".2"));
    sourceTargetMap.put(productPort.getId(), productPort);

    // Set glyph orientation and ports' coordinates.
    double reactantDxAverage = 0.0;
    double reactantDyAverage = 0.0;
    double productDxAverage = 0.0;
    double productDyAverage = 0.0;

    for (Reactant r : reaction.getReactants()) {
      Point2D reactantPoint;
      List<Point2D> reactantArcPoints = r.getLine().getPoints();
      if (reaction instanceof HeterodimerAssociationReaction) {
        reactantPoint = reactantArcPoints.get(reactantArcPoints.size() - 1);
      } else {
        reactantPoint = reactantArcPoints.get(reactantArcPoints.size() - 2);
      }
      reactantDxAverage += reactantPoint.getX() - pointX;
      reactantDyAverage += reactantPoint.getY() - pointY;
    }
    reactantDxAverage /= reaction.getReactants().size();
    reactantDyAverage /= reaction.getReactants().size();

    for (Product p : reaction.getProducts()) {
      List<Point2D> productArcPoints = p.getLine().getPoints();
      Point2D productPoint;
      if (reaction instanceof DissociationReaction) {
        productPoint = productArcPoints.get(0);
      } else {
        productPoint = productArcPoints.get(1);
      }
      productDxAverage += productPoint.getX() - pointX;
      productDyAverage += productPoint.getY() - pointY;
    }
    productDxAverage /= reaction.getProducts().size();
    productDyAverage /= reaction.getProducts().size();

    boolean horizontalOrientation;
    if ((reactantDxAverage * productDxAverage < 0) && (reactantDyAverage * productDyAverage < 0)) {
      horizontalOrientation = (Math.abs(productDxAverage - reactantDxAverage) > Math
          .abs(productDyAverage - reactantDyAverage));
    } else {
      horizontalOrientation = (reactantDxAverage * productDxAverage < 0);
    }
    if (horizontalOrientation) {
      processGlyph.setOrientation("horizontal");
      reactantPort.setY((float) pointY);
      productPort.setY((float) pointY);
      if (reactantDxAverage < 0) {
        reactantPort.setX((float) (pointX - PROCESS_PORT_DISTANCE));
        productPort.setX((float) (pointX + PROCESS_PORT_DISTANCE));
      } else {
        reactantPort.setX((float) (pointX + PROCESS_PORT_DISTANCE));
        productPort.setX((float) (pointX - PROCESS_PORT_DISTANCE));
      }
    } else {
      reactantPort.setX((float) pointX);
      productPort.setX((float) pointX);
      if (reactantDyAverage < 0) {
        reactantPort.setY((float) (pointY - PROCESS_PORT_DISTANCE));
        productPort.setY((float) (pointY + PROCESS_PORT_DISTANCE));
      } else {
        reactantPort.setY((float) (pointY + PROCESS_PORT_DISTANCE));
        productPort.setY((float) (pointY - PROCESS_PORT_DISTANCE));
      }
      processGlyph.setOrientation("vertical");
    }

    processGlyph.getPort().add(reactantPort);
    processGlyph.getPort().add(productPort);

    sourceTargetMap.put(processGlyph.getId(), processGlyph);

    return processGlyph;
  }

  /**
   * Returns {@link GlyphClazz} appropriate to given reaction.
   * 
   * @param reaction
   *          {@link Reaction} to extract {@link GlyphClazz} from
   * @return {@link GlyphClazz} appropriate to given reaction
   */
  private GlyphClazz getGlyphClazzFromReaction(Reaction reaction) {
    if (reaction instanceof StateTransitionReaction) {
      return GlyphClazz.PROCESS;
    }
    if (reaction instanceof HeterodimerAssociationReaction) {
      return GlyphClazz.ASSOCIATION;
    }
    if (reaction instanceof DissociationReaction) {
      return GlyphClazz.DISSOCIATION;
    }
    if (reaction instanceof KnownTransitionOmittedReaction) {
      return GlyphClazz.OMITTED_PROCESS;
    }
    if (reaction instanceof UnknownTransitionReaction) {
      return GlyphClazz.UNCERTAIN_PROCESS;
    }
    throw new InvalidArgumentException();
  }

  /**
   * Returns arc extracted from given reduced notation reaction.
   * 
   * @param reaction
   *          reduced notation reaction
   * @return arc extracted from given reduced notation reaction
   */
  private Arc getArcFromReducedReaction(Reaction reaction) {
    if ((reaction.getReactants().size() != 1) || (reaction.getProducts().size() != 1)) {
      throw new InvalidArgumentException();
    }
    Arc arc = new Arc();
    arc.setId(reaction.getIdReaction());

    if (reaction instanceof NegativeInfluenceReaction) {
      arc.setClazz(ArcClazz.INHIBITION.getClazz());
    } else if (reaction instanceof ReducedModulationReaction) {
      arc.setClazz(ArcClazz.MODULATION.getClazz());
    } else if (reaction instanceof ReducedTriggerReaction) {
      arc.setClazz(ArcClazz.NECESSARY_STIMULATION.getClazz());
    } else if (reaction instanceof ReducedPhysicalStimulationReaction) {
      arc.setClazz(ArcClazz.STIMULATION.getClazz());
    } else {
      throw new InvalidArgumentException();
    }

    if (reaction.getProducts().get(0).getElement() instanceof Phenotype) {
      logger.warn("Found Phenotype being a reactant in process. That is discouraged");
    }

    arc.setSource(sourceTargetMap.get(reaction.getReactants().get(0).getElement().getElementId()));
    arc.setTarget(sourceTargetMap.get(reaction.getProducts().get(0).getElement().getElementId()));

    List<Point2D> pointList = reaction.getReactants().get(0).getLine().getPoints();
    pointList.addAll(reaction.getProducts().get(0).getLine().getPoints());

    removeRedundantPoints(pointList);

    Start start = new Start();
    start.setX((float) pointList.get(0).getX());
    start.setY((float) pointList.get(0).getY());
    arc.setStart(start);

    End end = new End();
    end.setX((float) pointList.get(pointList.size() - 1).getX());
    end.setY((float) pointList.get(pointList.size() - 1).getY());
    arc.setEnd(end);

    for (int i = 1; i < pointList.size() - 1; i++) {
      Point2D nextPoint = pointList.get(i);
      Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    return arc;
  }

  /**
   * Removes redundant points from the list.
   * 
   * @param pointList
   *          list of points to be fixed
   */
  private void removeRedundantPoints(List<Point2D> pointList) {
    boolean allDone = false;
    while (!allDone) {
      allDone = true;
      for (int i = 1; i < pointList.size() - 1; i++) {
        double dx1 = pointList.get(i).getX() - pointList.get(i - 1).getX();
        double dy1 = pointList.get(i).getY() - pointList.get(i - 1).getY();
        double dx2 = pointList.get(i + 1).getX() - pointList.get(i).getX();
        double dy2 = pointList.get(i + 1).getY() - pointList.get(i).getY();

        DoubleComparator doubleComparator = new DoubleComparator();
        if (((doubleComparator.compare(dy1 / dx1, dy2 / dx2) == 0)
            || (pointList.get(i - 1).getY() == pointList.get(i).getY())
                && (pointList.get(i).getY() == pointList.get(i + 1).getY()))
            && between(pointList.get(i).getX(), pointList.get(i - 1).getX(), pointList.get(i + 1).getX())) {
          pointList.remove(i);
          allDone = false;
        }
      }
    }
  }

  /**
   * Checks if argument x is between s1 and s2.
   * 
   * @param x
   *          number to checked if it is in the middle
   * @param s1
   *          one side argument
   * @param s2
   *          second side argument
   * @return true if x is between s1 and s2
   */
  private boolean between(double x, double s1, double s2) {
    if ((x >= s1) && (x <= s2)) {
      return true;
    }
    if ((x <= s1) && (x >= s2)) {
      return true;
    }
    return false;
  }

  /**
   * Returns set of all arcs used in the reaction.
   * 
   * @param reaction
   *          the reaction to extract arcs from
   * @param glyphList
   *          list of all glyphs in the map; used only for parsing operators
   * @return set of all arcs used in the reaction
   */
  private List<Arc> getArcsFromReaction(Reaction reaction, List<Glyph> glyphList) {
    List<Arc> arcList = new ArrayList<>();

    // Parse all nodes except NodeOperators
    for (AbstractNode node : reaction.getNodes().stream().filter(n -> !(n instanceof NodeOperator))
        .collect(Collectors.toList())) {
      try {
        arcList.add(getArcFromNode(node, glyphList));
      } catch (InvalidArgumentException ex) {
        logger.warn("Node skipped in export process, since it is not compliant with SBGN-ML format: "
            + node.getClass().getName());
        continue;
      }
    }

    // Now parse NodeOperators
    for (AbstractNode node : reaction.getOperators().stream().filter(o -> {
      if (o.getInputs().stream().filter(i -> i instanceof Reactant).count() > 0) {
        return false;
      }
      if (o instanceof SplitOperator || o instanceof AssociationOperator || o instanceof DissociationOperator) {
        return false;
      }
      return true;
    }).collect(Collectors.toList())) {
      try {
        arcList.add(getArcFromNode(node, glyphList));
      } catch (InvalidArgumentException ex) {
        logger.warn("Node skipped in export process, since it is not compliant with SBGN-ML format: "
            + node.getClass().getName());
        continue;
      }
    }

    return arcList;

  }

  /**
   * Returns arc for given node.
   * 
   * @param node
   *          node to parse arc from
   * @param glyphList
   *          list of all glyphs in the map; used only for parsing operators
   * @return SBGN-ML arc for given node
   */
  private Arc getArcFromNode(AbstractNode node, List<Glyph> glyphList) {
    Arc arc = new Arc();
    arc.setId("arc".concat(Integer.toString(arcCounter)));
    arcCounter += 1;

    boolean logicArc = false;
    if (node instanceof Modifier) {
      for (NodeOperator operator : node.getReaction().getOperators()) {
        if (operator.getInputs().contains(node)) {
          logicArc = true;
        }
      }
    }
    if (logicArc) {
      arc.setClazz(ArcClazz.LOGIC_ARC.getClazz());
    } else {
      arc.setClazz(getArcClazzFromNode(node));
    }

    if (node instanceof Reactant) {
      arc.setSource(sourceTargetMap.get(((Reactant) node).getElement().getElementId()));
      arc.setTarget(sourceTargetMap.get(node.getReaction().getIdReaction().concat(".1")));
    } else if (node instanceof Product) {
      arc.setSource(sourceTargetMap.get(node.getReaction().getIdReaction().concat(".2")));
      arc.setTarget(sourceTargetMap.get(((Product) node).getElement().getElementId()));
    } else if (node instanceof Modifier) {
      arc.setSource(sourceTargetMap.get(((Modifier) node).getElement().getElementId()));
      if (!node.getLine().getEndAtd().getArrowType().equals(ArrowType.NONE)) {
        arc.setTarget(sourceTargetMap.get(node.getReaction().getIdReaction()));
      } else {
        for (NodeOperator operator : node.getReaction().getOperators()) {
          if (operator.getInputs().contains(node)) {
            if (!parsedOperators.contains(operator)) {
              Glyph newOperator = operatorToGlyph(operator);
              glyphList.add(newOperator);
            }
            arc.setTarget(sourceTargetMap.get(operatorIds.get(operator).concat(".1")));
          }
        }
      }
    } else if (node instanceof NodeOperator) {
      if ((node instanceof DissociationOperator) || (node instanceof AssociationOperator)) {
        throw new InvalidArgumentException();
      }
      arc.setSource(sourceTargetMap.get(operatorIds.get(node).concat(".2")));
      if (!node.getLine().getEndAtd().getArrowType().equals(ArrowType.NONE)) {
        arc.setTarget(sourceTargetMap.get(node.getReaction().getIdReaction()));
      } else {
        for (NodeOperator operator : node.getReaction().getOperators()) {
          if (operator.getInputs().contains(node)) {
            if (!parsedOperators.contains(operator)) {
              Glyph newOperator = operatorToGlyph(operator);
              glyphList.add(newOperator);
            }
            arc.setTarget(sourceTargetMap.get(operatorIds.get(operator).concat(".1")));
          }
        }
      }
    }

    List<Point2D> arcPoints = node.getLine().getPoints();
    Start start = new Start();
    if ((node instanceof Product) || (node instanceof NodeOperator)) {
      Port sourcePort = (Port) arc.getSource();
      start.setX(sourcePort.getX());
      start.setY(sourcePort.getY());
    } else {
      start.setX((float) arcPoints.get(0).getX());
      start.setY((float) arcPoints.get(0).getY());
    }
    arc.setStart(start);

    End end = new End();
    if ((node instanceof Reactant) || ((node instanceof Modifier) && (arc.getTarget() instanceof Port))) {
      Port targetPort = (Port) arc.getTarget();
      end.setX(targetPort.getX());
      end.setY(targetPort.getY());
    } else {
      Point2D lastPoint = arcPoints.get(arcPoints.size() - 1);
      end.setX((float) lastPoint.getX());
      end.setY((float) lastPoint.getY());
    }
    arc.setEnd(end);

    if ((node instanceof Product) && (node.getReaction() instanceof DissociationReaction)) {
      Point2D nextPoint = arcPoints.get(0);
      Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    DoubleComparator doubleComparator = new DoubleComparator();
    for (int i = 1; i < arcPoints.size() - 1; i++) {
      Point2D nextPoint = arcPoints.get(i);
      if ((doubleComparator.compare(nextPoint.getX(), new Double(start.getX())) == 0)
          && (doubleComparator.compare(nextPoint.getY(), new Double(start.getY())) == 0)) {
        arc.getNext().clear();
        continue;
      }
      if ((doubleComparator.compare(nextPoint.getX(), new Double(end.getX())) == 0)
          && (doubleComparator.compare(nextPoint.getY(), new Double(end.getY())) == 0)) {
        break;
      }
      Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    if ((node instanceof Reactant) && (node.getReaction() instanceof HeterodimerAssociationReaction)) {
      Point2D nextPoint = arcPoints.get(arcPoints.size() - 1);
      Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    return arc;
  }

  /**
   * Returns {@link ArcClazz} for given node.
   * 
   * @param node
   *          Node to extract {@link ArcClazz} from
   * @return {@link ArcClazz} for given node
   */
  private String getArcClazzFromNode(AbstractNode node) {
    if (node instanceof Reactant) {
      return ArcClazz.CONSUMPTION.getClazz();
    }
    if (node instanceof Product) {
      return ArcClazz.PRODUCTION.getClazz();
    }
    if (node instanceof Catalysis) {
      return ArcClazz.CATALYSIS.getClazz();
    }
    if (node instanceof Inhibition) {
      return ArcClazz.INHIBITION.getClazz();
    }
    if (node instanceof Modulation) {
      return ArcClazz.MODULATION.getClazz();
    }
    if (node instanceof Trigger) {
      return ArcClazz.NECESSARY_STIMULATION.getClazz();
    }
    if (node instanceof PhysicalStimulation) {
      return ArcClazz.STIMULATION.getClazz();
    }
    if (node instanceof NodeOperator) {
      ArrowType arrowType = node.getLine().getEndAtd().getArrowType();
      switch (arrowType) {
      case BLANK:
        return ArcClazz.STIMULATION.getClazz();
      case BLANK_CROSSBAR:
        return ArcClazz.NECESSARY_STIMULATION.getClazz();
      case CIRCLE:
        return ArcClazz.CATALYSIS.getClazz();
      case CROSSBAR:
        return ArcClazz.INHIBITION.getClazz();
      case DIAMOND:
        return ArcClazz.MODULATION.getClazz();
      case NONE:
        return ArcClazz.LOGIC_ARC.getClazz();
      default:
        throw new InvalidArgumentException();

      }
    }
    throw new InvalidArgumentException();
  }
}
