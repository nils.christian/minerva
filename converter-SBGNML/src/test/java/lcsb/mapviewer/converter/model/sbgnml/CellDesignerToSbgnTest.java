package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;

public class CellDesignerToSbgnTest {
	Logger logger = Logger.getLogger(CellDesignerToSbgnTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSample() throws Exception {
		try {
			IConverter converter = new CellDesignerXmlParser();
			IConverter converter2 = new SbgnmlXmlConverter();

			Model model;
			model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/sample.xml"));

			String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
			converter2.exportModelToFile(model, output);
			new File(output).delete();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSample2() throws Exception {
		try {
			IConverter converter = new CellDesignerXmlParser();
			IConverter converter2 = new SbgnmlXmlConverter();

			Model model;
			model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/bubbles.xml"));

			String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
			converter2.exportModelToFile(model, output);
			new File(output).delete();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
