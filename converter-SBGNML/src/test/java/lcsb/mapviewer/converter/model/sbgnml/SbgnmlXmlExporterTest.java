package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class SbgnmlXmlExporterTest {

	Logger				 logger	= Logger.getLogger(SbgnmlXmlExporterTest.class.getName());

	private String testName;

	public SbgnmlXmlExporterTest(String testName) {
		this.testName = testName;
	}

	@Parameters(name = "{index} : {0}")
	public static Collection<Object[]> data() throws IOException {
		Collection<Object[]> data = new ArrayList<Object[]>();
		Files.walk(Paths.get("testFiles/sbgnmlParserTestFiles/sbgnmlFiles")).forEach(fPath -> {
			if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".sbgn")) {
				String tName = fPath.getFileName().toString().substring(0, fPath.getFileName().toString().indexOf(".sbgn"));
				data.add(new Object[] { tName });
			}
		});
		return data;
	}

	private void parseAndExport(String testName) throws Exception {
		IConverter converter = new SbgnmlXmlConverter();

		Model model = converter.createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/".concat(testName).concat(".sbgn")));

		converter.exportModelToFile(model, File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath());
	}

	/*
	 * @Test public void simpleTest() throws JAXBException {
	 * parseAndExport("simpleTest"); }
	 * 
	 * @Test public void VANTEDdiagram() throws JAXBException {
	 * parseAndExport("VANTEDdiagram"); }
	 */

	@Test
	public void test() throws Exception {
		try {
			parseAndExport(testName);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
