package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.field.Residue;

public class SbgnmlXmlParserTest2 {

  Logger logger = Logger.getLogger(SbgnmlXmlParserTest2.class.getName());

  @Test
  public void testAdjustModificationCoordinates() throws Exception {
    SbgnmlXmlParser parser = new SbgnmlXmlParser();
    GenericProtein protein = new GenericProtein("id");
    protein.setWidth(50);
    protein.setHeight(30);
    protein.setX(200);
    protein.setX(300);
    Residue mr = new Residue();
    Point2D position = new Point2D.Double(100, 20);
    mr.setPosition(position);
    protein.addModificationResidue(mr);
    parser.adjustModificationCoordinates(protein);

    assertTrue(mr.getPosition().distance(position) > Configuration.EPSILON);

  }

}
