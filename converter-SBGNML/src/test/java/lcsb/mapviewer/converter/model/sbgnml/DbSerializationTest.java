package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.ModelDao;

@Transactional(value = "txManager")
@ContextConfiguration(locations = { "/applicationContext-persist.xml", //
    "/test-applicationContext.xml", //
    "/dataSource.xml", //

})
@RunWith(SpringJUnit4ClassRunner.class)
public class DbSerializationTest {
  @Autowired
  ModelDao modelDao;
  @Autowired
  DbUtils dbUtils;

  Logger logger = Logger.getLogger(DbSerializationTest.class.getName());

  private void makeDbSerializationTest(String filePath) throws Exception {
    SbgnmlXmlParser parser = new SbgnmlXmlParser();

    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/".concat(filePath)));

    modelDao.add(model);
    modelDao.flush();
    modelDao.commit();
    modelDao.evict(model);
    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
    modelDao.delete(model2);
  }

  @Before
  public void setUp() throws Exception {
    // we use custom threads because in layoutservice there is commit method
    // called, and because of that hibernate session injected by spring
    // cannot
    // commit at the end of the test case

    dbUtils.createSessionForCurrentThread();

  }

  @After
  public void tearDown() throws Exception {
    // close session
    dbUtils.closeSessionForCurrentThread();
  }

  @Test
  public void VANTEDdiagramTest() throws Exception {
    makeDbSerializationTest("VANTEDdiagram.sbgn");
  }

  @Test
  public void activatedStat1AlphaTest() throws Exception {
    makeDbSerializationTest("activated_stat1alpha_induction_of_the_irf1_gene.sbgn");
  }

  @Test
  public void adhTest() throws Exception {
    makeDbSerializationTest("adh.sbgn");
  }

  @Test
  public void cloneMarkerTest() throws Exception {
    makeDbSerializationTest("clone-marker.sbgn");
  }

  @Test
  public void glycolysisTest() throws Exception {
    makeDbSerializationTest("glycolysis.sbgn");
  }

  @Test
  public void insulinLikeGrowthFactorSignalingTest() throws Exception {
    makeDbSerializationTest("insulin-like_growth_factor_signaling.sbgn");
  }

  @Test
  public void neuronalMuscleSignallingTest() throws Exception {
    makeDbSerializationTest("neuronal_muscle_signalling.sbgn");
  }

  @Test
  public void phenotypeTest() throws Exception {
    makeDbSerializationTest("phenotypeTest.sbgn");
  }

}
