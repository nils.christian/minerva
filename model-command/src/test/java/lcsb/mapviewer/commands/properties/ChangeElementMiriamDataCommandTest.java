package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementMiriamDataCommandTest extends CommandTestFunctions {

	Model model;

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testWithoutChange() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
			MiriamData md2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
			alias.addMiriamData(md);
			ChangeElementMiriamDataCommand command = new ChangeElementMiriamDataCommand(model, alias, md2, md);
			command.execute();
			assertTrue(alias.getMiriamData().contains(md2));
			assertEquals(1, alias.getMiriamData().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidChange() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
			MiriamData md2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA2");
			alias.addMiriamData(md);
			alias.addMiriamData(md2);
			ChangeElementMiriamDataCommand command = new ChangeElementMiriamDataCommand(model, alias, md2, md);
			try {
				command.execute();
				fail("Exception expected");
			} catch (CommandExecutionException e) {
				assertTrue(alias.getMiriamData().contains(md));
				assertTrue(alias.getMiriamData().contains(md2));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	

	@Test
	public void testChange() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
			MiriamData md2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA2");
			MiriamData md3 = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
			alias.addMiriamData(md);
			ChangeElementMiriamDataCommand command = new ChangeElementMiriamDataCommand(model, alias, md2, md);
			command.execute();
			assertTrue(alias.getMiriamData().contains(md2));
			assertFalse(alias.getMiriamData().contains(md3));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
}
