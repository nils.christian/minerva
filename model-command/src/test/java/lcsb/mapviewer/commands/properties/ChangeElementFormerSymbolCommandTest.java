package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementFormerSymbolCommandTest extends CommandTestFunctions {

	Model					 model;

	ModelComparator modelComparator = new ModelComparator();

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testChangeToTheSame() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementFormerSymbolCommand command = new ChangeElementFormerSymbolCommand(model, alias, "f_symbol","f_symbol");
			assertTrue(alias.getFormerSymbols().contains("f_symbol"));
			command.execute();
			assertTrue(alias.getFormerSymbols().contains("f_symbol"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
