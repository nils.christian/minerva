package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementNameCommandTest extends CommandTestFunctions {
	
	Model model;
	
	ModelComparator modelComparator= new ModelComparator();

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangeAliasName() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementNameCommand command = new ChangeElementNameCommand(model, alias, "test");
			assertFalse(alias.getName().equalsIgnoreCase("test"));
			command.execute();
			assertTrue(alias.getName().equalsIgnoreCase("test"));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUndo() throws Exception {
		try {
			Model model2 = createSimpleModel();
			Element alias = model.getElementByElementId("alias_id");

			// models should be equal before move
			assertEquals(0, modelComparator.compare(model, model2));

			ChangeElementNameCommand command = new ChangeElementNameCommand(model, alias, "test");
			command.execute();

			// after move models should be different
			assertTrue(0 != modelComparator.compare(model, model2));

			// undo command
			command.undo();

			// after undo they should be the same again
			assertEquals(0, modelComparator.compare(model, model2));

			command.redo();

			// after redo they should be different again
			assertTrue(0 != modelComparator.compare(model, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
