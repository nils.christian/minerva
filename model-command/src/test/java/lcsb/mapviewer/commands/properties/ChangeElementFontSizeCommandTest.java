package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementFontSizeCommandTest extends CommandTestFunctions{
	
	Model model;
	
	ModelComparator modelComparator= new ModelComparator();

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangeAliasFontSize() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementFontSizeCommand command = new ChangeElementFontSizeCommand(model, alias, 15.0);
			assertFalse(alias.getFontSize().equals(15.0));
			command.execute();
			assertTrue(alias.getFontSize().equals(15.0));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUndo() throws Exception {
		try {
			Model model2 = createSimpleModel();
			Element alias = model.getElementByElementId("alias_id");

			// models should be equal before move
			assertEquals(0, modelComparator.compare(model, model2));

			ChangeElementFontSizeCommand command = new ChangeElementFontSizeCommand(model, alias, 15.0);
			command.execute();

			// after move models should be different
			assertTrue(0 != modelComparator.compare(model, model2));

			// undo command
			command.undo();

			// after undo they should be the same again
			assertEquals(0, modelComparator.compare(model, model2));

			command.redo();

			// after redo they should be different again
			assertTrue(0 != modelComparator.compare(model, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
