package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementFormulaCommandTest extends CommandTestFunctions{
	
	Model model;
	
	ModelComparator modelComparator= new ModelComparator();

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangeAliasFormula() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementFormulaCommand command = new ChangeElementFormulaCommand(model, alias, "test");
			assertFalse("test".equals(alias.getFormula()));
			command.execute();
			assertTrue(alias.getFormula().equalsIgnoreCase("test"));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUndo() throws Exception {
		try {
			Model model2 = createSimpleModel();
			Element alias = model.getElementByElementId("alias_id");

			// models should be equal before move
			assertEquals(0, modelComparator.compare(model, model2));

			ChangeElementFormulaCommand command = new ChangeElementFormulaCommand(model, alias, "test");
			command.execute();

			// after move models should be different
			assertTrue(0 != modelComparator.compare(model, model2));

			// undo command
			command.undo();

			// after undo they should be the same again
			assertEquals(0, modelComparator.compare(model, model2));

			command.redo();

			// after redo they should be different again
			assertTrue(0 != modelComparator.compare(model, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
