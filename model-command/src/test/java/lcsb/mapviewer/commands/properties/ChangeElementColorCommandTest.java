package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;

import java.awt.Color;

import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementColorCommandTest extends CommandTestFunctions {

	Model					 model;

	ModelComparator modelComparator = new ModelComparator();

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangeAliasColor() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementColorCommand command = new ChangeElementColorCommand(model, alias, Color.PINK);
			assertFalse(alias.getColor().equals(Color.PINK));
			command.execute();
			assertTrue(alias.getColor().equals(Color.PINK));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUndo() throws Exception {
		try {
			Model model2 = createSimpleModel();
			Element alias = model.getElementByElementId("alias_id");

			// models should be equal before move
			assertEquals(0, modelComparator.compare(model, model2));

			ChangeElementColorCommand command = new ChangeElementColorCommand(model, alias, Color.PINK);
			command.execute();

			// after move models should be different
			assertTrue(0 != modelComparator.compare(model, model2));

			// undo command
			command.undo();

			// after undo they should be the same again
			assertEquals(0, modelComparator.compare(model, model2));

			command.redo();

			// after redo they should be different again
			assertTrue(0 != modelComparator.compare(model, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
