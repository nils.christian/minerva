package lcsb.mapviewer.commands.properties;

import static org.junit.Assert.*;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ChangeElementSynonymCommandTest extends CommandTestFunctions {

	Model					 model;

	ModelComparator modelComparator = new ModelComparator();

	@Before
	public void setUp() throws Exception {
		model = createSimpleModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangeAliasSynonym() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementSynonymCommand command = new ChangeElementSynonymCommand(model, alias, "test","synonym");
			assertFalse(alias.getSynonyms().contains("test"));
			command.execute();
			assertTrue(alias.getSynonyms().contains("test"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testChangeToTheSame() throws Exception {
		try {
			Element alias = model.getElementByElementId("alias_id");
			ChangeElementSynonymCommand command = new ChangeElementSynonymCommand(model, alias, "synonym","synonym");
			assertTrue(alias.getSynonyms().contains("synonym"));
			command.execute();
			assertTrue(alias.getSynonyms().contains("synonym"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUndo() throws Exception {
		try {
			Model model2 = createSimpleModel();
			Element alias = model.getElementByElementId("alias_id");

			// models should be equal before move
			assertEquals(0, modelComparator.compare(model, model2));

			ChangeElementSynonymCommand command = new ChangeElementSynonymCommand(model, alias, "test", "synonym");
			command.execute();

			// after move models should be different
			assertTrue(0 != modelComparator.compare(model, model2));

			// undo command
			command.undo();

			// after undo they should be the same again
			assertEquals(0, modelComparator.compare(model, model2));

			command.redo();

			// after redo they should be different again
			assertTrue(0 != modelComparator.compare(model, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
