package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class CopyCommandTest extends CommandTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCopyModel() {
    try {
      Model model = getModelForFile("testFiles/sample.xml", false);
      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));
    } catch (Exception e) {
      e.printStackTrace();
      fail("exception occurred");
    }
  }

  @Test
  public void testCopyModelWithKinetics() throws Exception {
    try {
      Model model = getModelForFile("testFiles/kinetics_with_compartment.xml", false);
      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));
      for (Reaction reaction : copy.getReactions()) {
        if (reaction.getKinetics() != null) {
          for (Element element : reaction.getKinetics().getElements()) {
            assertTrue("Element in the copy doesn't belong to copy", copy.getElements().contains(element));
          }
          for (SbmlFunction function : reaction.getKinetics().getFunctions()) {
            assertTrue("Function in the copy doesn't belong to copy", copy.getFunctions().contains(function));
          }
          for (SbmlParameter parameter : reaction.getKinetics().getParameters()) {
            if (parameter.getParameterId().equals("k2")) {
              assertTrue("Global parameter in the function copy doesn't belong to copy",
                  copy.getParameters().contains(parameter));
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyCustomModel() {
    try {
      Model model = new ModelFullIndexed(null);

      GenericProtein protein = new GenericProtein("A");
      protein.setNotes(null);

      model.addElement(protein);

      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));
    } catch (Exception e) {
      e.printStackTrace();
      fail("exception occurred");
    }
  }

  @Test
  public void testCopyCustomModel2() {
    try {
      Model model = new ModelFullIndexed(null);

      Complex complexAlias = new Complex("id2");
      GenericProtein protein = new GenericProtein("A");
      protein.setNotes(null);
      complexAlias.addSpecies(protein);
      model.addElement(protein);
      model.addElement(complexAlias);

      GenericProtein alias = new GenericProtein("B");
      alias.setNotes(null);

      complexAlias.addSpecies(alias);
      model.addElement(alias);

      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModel3() throws Exception {
    try {
      Model model = getModelForFile("testFiles/complex_with_state.xml", true);

      Model copy = new CopyCommand(model).execute();

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(copy);

      InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
      Model copy2 = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));
      ModelComparator comparator = new ModelComparator();

      // check if after conversion to xml everything works
      assertEquals(0, comparator.compare(copy, copy2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModel4() throws Exception {
    try {
      Model model = getModelForFile("testFiles/problematic_description.xml", true);

      Model copy = new CopyCommand(model).execute();

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(copy);

      InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
      Model copy2 = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));
      ModelComparator comparator = new ModelComparator();

      // check if after conversion to xml everything works
      assertEquals(0, comparator.compare(copy, copy2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModelWithArtifitialAliases() throws Exception {
    try {
      Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);
      new CreateHierarchyCommand(model, 2, 2).execute();

      Model copy = new CopyCommand(model).execute();

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(copy);

      InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
      Model copy2 = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));
      ModelComparator comparator = new ModelComparator();

      new CreateHierarchyCommand(copy2, 2, 2).execute();

      // check if after conversion to xml everything works
      assertEquals(0, comparator.compare(copy, copy2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModelWithSubmodels() throws Exception {
    try {
      Model model = getModel();
      Model model2 = getModel();
      model2.setNotes("ASDSA");
      model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopySubmodel() throws Exception {
    try {
      Model model = getModel();
      Model model2 = getModel();
      model2.setNotes("ASDSA");
      model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS, "name a"));

      Model model3 = getModel();
      model3.setNotes("ASDSA");
      model.addSubmodelConnection(new ModelSubmodelConnection(model3, SubmodelType.DOWNSTREAM_TARGETS, "name b"));
      Element alias = model2.getElementByElementId("a_id");
      alias.setSubmodel(new ElementSubmodelConnection(model3, SubmodelType.DOWNSTREAM_TARGETS, "name c"));
      Model copy = new CopyCommand(model2).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model2, copy));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModelWithSubmodels2() throws Exception {
    try {
      Model model = getModel();
      Model model2 = getModel();
      model2.setNotes("ASDSA2");

      model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
      model.getElementByElementId("a_id")
          .setSubmodel(new ElementSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModelWithName() throws Exception {
    try {
      Model model = getModel();
      model.setName("ASDSA2");

      Model copy = new CopyCommand(model).execute();

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, copy));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyModelWithSubmodelsThrowException() throws Exception {
    try {
      Model model = getModel();
      Model model2 = getModel();
      model2.setNotes("ASDSA2");

      model.getElementByElementId("a_id")
          .setSubmodel(new ElementSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
      try {
        new CopyCommand(model).execute();
        fail("Exception expected");
      } catch (InvalidArgumentException e) {
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);
    model.setNotes("Some description");

    GenericProtein alias = new GenericProtein("a_id");
    alias.setName("ad");
    model.addElement(alias);

    Layer layer = new Layer();
    layer.setName("layer name");
    model.addLayer(layer);

    model.addReaction(new Reaction());
    return model;
  }

  @Test
  public void testCopyModelReaction() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);

      Compartment c1 = new SquareCompartment("c1");
      Compartment c2 = new SquareCompartment("c2");
      c1.setVisibilityLevel("2");
      c2.setVisibilityLevel("3");

      model.addElement(c1);
      model.addElement(c2);

      GenericProtein s1 = new GenericProtein("s1");
      s1.setCompartment(c1);
      model.addElement(s1);

      GenericProtein s2 = new GenericProtein("s2");
      s2.setCompartment(c2);
      model.addElement(s2);

      StateTransitionReaction reaction = new StateTransitionReaction();
      Reactant reactant = new Reactant(s1);
      reactant.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10)));
      reaction.addReactant(reactant);
      Product product = new Product(s2);
      product.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(120, 10)));
      reaction.addProduct(product);
      reaction.setVisibilityLevel("4");

      model.addReaction(reaction);

      assertTrue(s1.equals(reaction.getReactants().get(0).getElement()));
      assertTrue(s2.equals(reaction.getProducts().get(0).getElement()));

      Model model2 = new CopyCommand(model).execute();
      Reaction reaction2 = model2.getReactions().iterator().next();

      assertTrue(s1.equals(reaction.getReactants().get(0).getElement()));
      assertTrue(s2.equals(reaction.getProducts().get(0).getElement()));

      assertFalse(s1.equals(reaction2.getReactants().get(0).getElement()));
      assertFalse(s2.equals(reaction2.getProducts().get(0).getElement()));

      assertNotNull(reaction2.getReactants().get(0).getElement().getCompartment());
      assertNotNull(reaction2.getProducts().get(0).getElement().getCompartment());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
