package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.awt.geom.Path2D;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SubModelCommandTest extends CommandTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSubmodel1() throws Exception {
		try {
			Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

			Path2D polygon = new Path2D.Double();
			polygon.moveTo(0, 0);
			polygon.lineTo(0, 100);
			polygon.lineTo(100, 100);
			polygon.lineTo(100, 0);
			polygon.closePath();

			Model copy = new SubModelCommand(model, polygon).execute();

			assertEquals(2, copy.getElements().size());
			assertEquals(0, copy.getReactions().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSubmodel2() throws Exception {
		try {
			Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

			Path2D polygon = new Path2D.Double();
			polygon.moveTo(50, 50);
			polygon.lineTo(350, 50);
			polygon.lineTo(350, 200);
			polygon.lineTo(50, 200);
			polygon.closePath();

			Model copy = new SubModelCommand(model, polygon).execute();

			assertEquals(9, copy.getElements().size());
			assertEquals(1, copy.getReactions().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSubmodel3() throws Exception {
		try {
			Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

			Path2D polygon = new Path2D.Double();
			polygon.moveTo(0, 200);
			polygon.lineTo(350, 200);
			polygon.lineTo(350, 400);
			polygon.lineTo(0, 400);
			polygon.closePath();

			Model copy = new SubModelCommand(model, polygon).execute();

			double dx = 10;
			double dy = -10;

			new MoveCommand(copy, dx, dy).execute();

			assertEquals(model.getElementByElementId("sa3").getCenterX(), copy.getElementByElementId("sa3").getCenterX() - dx, EPSILON);
			assertEquals(model.getElementByElementId("sa3").getCenterY(), copy.getElementByElementId("sa3").getCenterY() - dy, EPSILON);

			assertEquals(model.getReactionByReactionId("re3").getLines().get(0).getX2(), copy.getReactionByReactionId("re3").getLines().get(0).getX2() - dx, EPSILON);
			assertEquals(model.getReactionByReactionId("re3").getLines().get(0).getY2(), copy.getReactionByReactionId("re3").getLines().get(0).getY2() - dy, EPSILON);

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(copy);

			InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model copy2 = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));
			ModelComparator comparator = new ModelComparator();

			// check if after conversion to xml everything works
			assertEquals(0, comparator.compare(copy, copy2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSubmodelWithoutCompartments() throws Exception {
		try {
			Model model = getModelForFile("testFiles/compartments.xml", true);

			Path2D polygon = new Path2D.Double();
			polygon.moveTo(0, 10);
			polygon.lineTo(10, 10);
			polygon.lineTo(10, 0);
			polygon.lineTo(0, 0);
			polygon.closePath();

			Model copy = new SubModelCommand(model, polygon).execute();

			// we should cut off some of compartmets
			assertFalse(model.getLayers().iterator().next().getTexts().size() == copy.getLayers().iterator().next().getTexts().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSubmodelWithoutCompartments2() throws Exception {
		try {
			Model model = getModelForFile("testFiles/problematic/cutting_without_compartment.xml", true);

			Path2D polygon = new Path2D.Double();
			polygon.moveTo(0, 0);
			polygon.lineTo(0, 500);
			polygon.lineTo(300, 500);
			polygon.lineTo(300, 0);
			polygon.closePath();

			Model copy = new SubModelCommand(model, polygon).execute();

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xmlString = parser.toXml(copy);

			InputStream stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));

			ModelComparator mc = new ModelComparator();
			assertEquals(0, mc.compare(copy, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
