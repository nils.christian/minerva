package lcsb.mapviewer.commands.layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

public class ApplySimpleLayoutModelCommandTest {

  private int counter;

  @Test
  public void testEstimateLayoutMinPoint() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    protein.setX(5);
    protein.setY(10);
    protein.setWidth(50);
    protein.setHeight(100);
    model.addElement(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, new ArrayList<>());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertTrue(minPoint.getX() > protein.getBorder().getMaxX() || minPoint.getY() > protein.getBorder().getMaxY());
  }

  @Test
  public void testEstimateLayoutMinPointIgnoringElements() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    protein.setX(5);
    protein.setY(10);
    protein.setWidth(50);
    protein.setHeight(100);
    model.addElement(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, model.getBioEntities());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertEquals(0.0, minPoint.getX(), Configuration.EPSILON);
    assertEquals(0.0, minPoint.getY(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateLayoutMinPointWithDataWithoutLayout() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    model.addElement(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, model.getBioEntities());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertEquals(0.0, minPoint.getX(), Configuration.EPSILON);
    assertEquals(0.0, minPoint.getY(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateLayoutMinPointWithEmptyMap() {
    Model model = new ModelFullIndexed(null);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, model.getBioEntities());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertEquals(0.0, minPoint.getX(), Configuration.EPSILON);
    assertEquals(0.0, minPoint.getY(), Configuration.EPSILON);
  }

  @Test
  public void testModifyModelSize() {
    Model model = new ModelFullIndexed(null);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);
    Point2D minPoint = new Point2D.Double(2, 3);
    Dimension2D dimension = new DoubleDimension(4, 5);
    layoutModelCommand.modifyModelSize(model, minPoint, dimension);
    assertEquals(6.0, model.getWidth(), Configuration.EPSILON);
    assertEquals(8.0, model.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateDimension() {
    Model model = new ModelFullIndexed(null);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);

    Dimension2D dimension = layoutModelCommand.estimateDimension(new ArrayList<>());

    assertEquals(0.0, dimension.getWidth(), Configuration.EPSILON);
    assertEquals(0.0, dimension.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateDimensionWithElements() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    List<BioEntity> bioEntities = new ArrayList<>();
    bioEntities.add(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);

    Dimension2D dimension = layoutModelCommand.estimateDimension(bioEntities);

    assertTrue(dimension.getWidth() > 0.0);
    assertTrue(dimension.getHeight() > 0.0);
  }

  @Test
  public void testModifyComplexWithSimpleChildren() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Complex complex = createComplex();
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    complex.addSpecies(p1);
    complex.addSpecies(p2);

    layoutModelCommand.modifyComplexLocation(complex, 100, 100);

    assertFalse(p1.contains(p2));
    assertTrue(complex.contains(p1));
    assertTrue(complex.contains(p2));
  }

  private GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("" + counter++);
    result.setWidth((Double) null);
    result.setHeight((Double) null);
    result.setX((Double) null);
    result.setY((Double) null);
    return result;
  }

  private Complex createComplex() {
    Complex result = new Complex("" + counter++);
    result.setWidth((Double) null);
    result.setHeight((Double) null);
    result.setX((Double) null);
    result.setY((Double) null);
    return result;
  }

  @Test
  public void testModifyComplexWithSubcomplexes() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Complex complex = createComplex();
    Complex complex2 = createComplex();
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    complex.addSpecies(p1);
    complex2.addSpecies(p2);
    complex.addSpecies(complex2);

    layoutModelCommand.modifyComplexLocation(complex, 100, 100);

    assertFalse(p1.contains(p2));
    assertTrue(complex.contains(p1));
    assertTrue(complex.contains(p2));
    assertTrue(complex.contains(complex2));
    assertTrue(complex2.contains(p2));
    assertFalse(complex2.contains(p1));
  }

  @Test
  public void testModifySpeciesLocation() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    Set<Species> elements = new HashSet<>();
    elements.add(p1);
    elements.add(p2);

    layoutModelCommand.modifySpeciesListLocation(elements, new Point2D.Double(100, 100), new DoubleDimension(200, 200));

    assertTrue(p1.getX() >= 100);
    assertTrue(p1.getY() >= 100);
    assertTrue(p1.getBorder().getMaxX() <= 300);
    assertTrue(p1.getBorder().getMaxY() <= 300);
    assertTrue(p2.getX() >= 100);
    assertTrue(p2.getY() >= 100);
    assertTrue(p2.getBorder().getMaxX() <= 300);
    assertTrue(p2.getBorder().getMaxY() <= 300);

  }

  @Test
  public void testModifyElementLocation() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    Compartment p2 = new SquareCompartment("comp-id");
    Set<Element> elements = new HashSet<>();
    elements.add(p1);
    elements.add(p2);
    p2.addElement(p1);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(100, 100),
        new DoubleDimension(200, 200));

    assertTrue(p1.getX() >= 100);
    assertTrue(p1.getY() >= 100);
    assertTrue(p1.getBorder().getMaxX() <= 300);
    assertTrue(p1.getBorder().getMaxY() <= 300);
    assertTrue(p2.getX() >= 100);
    assertTrue(p2.getY() >= 100);
    assertTrue(p2.getBorder().getMaxX() <= 300);
    assertTrue(p2.getBorder().getMaxY() <= 300);

    assertTrue(p2.contains(p1));
  }

  @Test
  public void testModifyElementLocationInsideCompartmentInsideStaticCompartment() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein protein = createProtein();
    Compartment staticCompartment = new SquareCompartment("comp-id");
    staticCompartment.setX(10);
    staticCompartment.setY(20);
    staticCompartment.setWidth(200);
    staticCompartment.setHeight(200);

    Compartment compartmentToLayout = new SquareCompartment("comp-id2");
    compartmentToLayout.addElement(protein);

    staticCompartment.addElement(compartmentToLayout);

    Set<Element> elements = new HashSet<>();
    elements.add(protein);
    elements.add(compartmentToLayout);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(1000, 1000),
        new DoubleDimension(2000, 2000));

    assertFalse(protein.getX() >= 1000);
    assertFalse(protein.getY() >= 1000);

    assertTrue(staticCompartment.contains(protein));

    assertFalse(compartmentToLayout.getX() >= 1000);
    assertFalse(compartmentToLayout.getY() >= 1000);

    assertTrue(staticCompartment.contains(compartmentToLayout));
  }

}
