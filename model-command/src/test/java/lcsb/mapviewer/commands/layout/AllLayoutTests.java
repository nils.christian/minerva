package lcsb.mapviewer.commands.layout;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ApplySimpleLayoutModelCommandTest.class })
public class AllLayoutTests {

}
