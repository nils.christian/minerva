package lcsb.mapviewer.commands;

import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Creates a new instance of the model with data limited to region described by
 * polygon.
 * 
 * @author Piotr Gawron
 * 
 */
public class SubModelCommand extends NewModelCommand {
	
	/**
	 * Polygon that limits the area for the new model.
	 * 
	 */
	private Path2D polygon;

	/**
	 * Default constructor.
	 * 
	 * @param model
	 *          original {@link NewModelCommand#model}
	 * @param polygon
	 *          #polygon that limits the area for the new model
	 */
	public SubModelCommand(Model model, Path2D polygon) {
		super(model);
		this.polygon = polygon;
	}

	@Override
	public Model execute() {
		CopyCommand copy = new CopyCommand(getModel());
		Model result = copy.execute();

		Set<Element> aliasNotToRemove = new HashSet<Element>();

		for (Element alias : result.getElements()) {
			if (polygon.intersects(alias.getBorder())) {
				aliasNotToRemove.add(alias);
			}
		}
		boolean added = false;
		do {
			Set<Element> iterativeAliasNotToRemove = new HashSet<>();
			for (Element alias : aliasNotToRemove) {
				Element parent = alias.getCompartment();
				if (parent != null) {
					if (!aliasNotToRemove.contains(parent)) {
						iterativeAliasNotToRemove.add(parent);
					}
				}
				if (alias instanceof Species) {
					parent = ((Species) alias).getComplex();
					if (parent != null) {
						if (!aliasNotToRemove.contains(parent)) {
							iterativeAliasNotToRemove.add(parent);
						}
					}
				}
			}

			added = iterativeAliasNotToRemove.size() != 0;
			aliasNotToRemove.addAll(iterativeAliasNotToRemove);
		} while (added);

		List<Element> aliasToRemove = new ArrayList<Element>();
		for (Element alias : result.getElements()) {
			if (!(polygon.intersects(alias.getBorder()))) {
				boolean remove = true;
				if (alias instanceof Species) {
					remove = ((Species) alias).getComplex() == null;
				}
				if (aliasNotToRemove.contains(alias)) {
					remove = false;
				}
				if (remove) {
					aliasToRemove.add(alias);
					if (alias instanceof Complex) {
						List<Species> aliases = ((Complex) alias).getAllChildren();
						aliasToRemove.addAll(aliases);
					}
				}
			}

		}
		for (Element alias : aliasToRemove) {
			result.removeElement(alias);
		}

		List<Reaction> reactionsToRemove = new ArrayList<Reaction>();
		for (Reaction reaction : result.getReactions()) {
			boolean remove = false;
			for (AbstractNode node : reaction.getNodes()) {
				if (node instanceof ReactionNode) {
					if (!result.getElements().contains(((ReactionNode) node).getElement())) {
						remove = true;
						break;
					}
				}
			}
			if (remove) {
				reactionsToRemove.add(reaction);
			}
		}
		for (Reaction reaction : reactionsToRemove) {
			result.removeReaction(reaction);
		}

		for (Layer layer : result.getLayers()) {
			List<LayerText> textToRemove = new ArrayList<LayerText>();
			for (LayerText text : layer.getTexts()) {
				if (!(polygon.intersects(text.getBorder()))) {
					textToRemove.add(text);
				}
			}
			for (LayerText text : textToRemove) {
				layer.removeLayerText(text);
			}
		}

		return result;
	}

}
