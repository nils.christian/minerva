package lcsb.mapviewer.commands.layout;

import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.SplitOperator;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

public class ApplySimpleLayoutModelCommand extends ApplyLayoutModelCommand {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ApplySimpleLayoutModelCommand.class);

  public static final double COMPARTMENT_BORDER = 10;
  private static final double BORDER_FROM_EXISTING_ELEMENTS = 10;
  private static final int SPECIES_WIDTH = 90;
  private static final int SPECIES_HEIGHT = 30;
  private static final double COMPLEX_PADDING = 5;

  public ApplySimpleLayoutModelCommand(Model model, Collection<BioEntity> bioEntities, Double minX, Double minY,
      Double maxX, Double maxY, boolean addReactionPrefixes) {
    super(model, bioEntities, minX, minY, maxX, maxY, addReactionPrefixes);
  }

  public ApplySimpleLayoutModelCommand(Model model, Collection<BioEntity> bioEntities) {
    this(model, bioEntities, null, null, null, null, false);
  }

  public ApplySimpleLayoutModelCommand(Model model, Collection<BioEntity> bioEntities, boolean addReactionPrefixes) {
    this(model, bioEntities, null, null, null, null, addReactionPrefixes);
  }

  public ApplySimpleLayoutModelCommand(Model model) {
    this(model, null);
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    Set<Model> models = new HashSet<>();
    models.add(this.getModel());
    models.addAll(this.getModel().getSubmodels());
    for (Model model : models) {
      Collection<BioEntity> bioEntites = new HashSet<>();
      for (BioEntity bioEntity : this.getBioEntities()) {
        if (bioEntity.getModel() == model) {
          bioEntites.add(bioEntity);
        }
      }
      if (bioEntites.size() > 0) {
        Dimension2D dimension = null;
        Point2D minPoint = null;
        if (model == this.getModel()) {
          dimension = getStartDimension();
          minPoint = getStartPoint();
        }
        createLayout(model, bioEntites, null, minPoint, dimension);
      }
    }
  }

  private void createLayout(Model model, Collection<BioEntity> bioEntites, Compartment compartment, Point2D minPoint,
      Dimension2D dimension) {
    if (dimension == null) {
      dimension = estimateDimension(bioEntites);
    }
    if (minPoint == null) {
      minPoint = estimateLayoutMinPoint(model);
    }
    modifyModelSize(model, minPoint, dimension);
    Collection<Element> elements = new HashSet<>();
    Collection<Reaction> reactions = new HashSet<>();
    for (BioEntity bioEntity : bioEntites) {
      if (bioEntity instanceof Element) {
        elements.add((Element) bioEntity);
      } else if (bioEntity instanceof Reaction) {
        reactions.add((Reaction) bioEntity);
      } else {
        throw new InvalidArgumentException("Unknown class type: " + bioEntity);
      }
    }

    modifyElementLocation(elements, null, minPoint, dimension);
    modifyReactionLocation(reactions);

  }

  protected void modifyElementLocation(Collection<Element> elements, Compartment parent, Point2D minPoint,
      Dimension2D dimension) {
    Set<Compartment> compartments = new HashSet<>();
    Set<Species> elementToAlign = new HashSet<>();
    Map<Compartment, Set<Element>> elementsByStaticCompartment = new HashMap<>();
    for (Element element : elements) {
      if (element.getWidth() == null || element.getWidth() == 0) {
        element.setWidth(SPECIES_WIDTH);
        element.setHeight(SPECIES_HEIGHT);

      }
      if (element.getCompartment() == null || element.getCompartment() == parent) {
        if (element instanceof Compartment) {
          compartments.add((Compartment) element);
        } else if (element instanceof Species) {
          elementToAlign.add((Species) element);
        } else {
          throw new InvalidArgumentException("Unknown element type: " + element.getClass());
        }
      } else {
        Set<Element> elementsSet = elementsByStaticCompartment.get(element.getCompartment());
        if (elementsSet == null) {
          elementsSet = new HashSet<>();
          elementsByStaticCompartment.put(element.getCompartment(), elementsSet);
        }
        elementsSet.add(element);
      }
    }

    if (compartments.size() == 1 && elementToAlign.size() == 0) {
      Compartment compartment = compartments.iterator().next();
      compartment.setX(minPoint.getX() + COMPARTMENT_BORDER);
      compartment.setY(minPoint.getY() + COMPARTMENT_BORDER);
      compartment.setWidth(dimension.getWidth() - COMPARTMENT_BORDER * 2);
      compartment.setHeight(dimension.getHeight() - COMPARTMENT_BORDER * 2);
      compartment.setNamePoint(minPoint.getX() + COMPARTMENT_BORDER * 2, minPoint.getY() + COMPARTMENT_BORDER * 2);
      Point2D point = new Point2D.Double(minPoint.getX() + COMPARTMENT_BORDER, minPoint.getY() + COMPARTMENT_BORDER);
      Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth() - COMPARTMENT_BORDER * 2,
          dimension.getHeight() - COMPARTMENT_BORDER * 2);
      modifyElementLocation(compartment.getElements(), compartment, point, recursiveDimension);
    } else if (compartments.size() == 0) {
      modifySpeciesListLocation(elementToAlign, minPoint, dimension);
    } else {
      Collection<Element> firstHalf = new HashSet<>();
      Collection<Element> secondHalf = new HashSet<>();

      int firstHalfCount = 0;
      int secondHalfCount = 0;

      for (Compartment compartment : compartments) {
        int compSize = compartment.getAllSubElements().size() + 1;
        if (firstHalfCount < secondHalfCount) {
          firstHalf.add(compartment);
          firstHalfCount += compSize;
        } else {
          secondHalf.add(compartment);
          secondHalfCount += compSize;
        }
      }
      if (firstHalfCount < secondHalfCount) {
        firstHalf.addAll(elementToAlign);
        firstHalfCount += elementToAlign.size();
      } else {
        secondHalf.addAll(elementToAlign);
        secondHalfCount += elementToAlign.size();
      }
      if (dimension.getWidth() > dimension.getHeight()) {
        Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth() / 2, dimension.getHeight());
        modifyElementLocation(firstHalf, parent, minPoint, recursiveDimension);
        Point2D point = new Point2D.Double(minPoint.getX() + dimension.getWidth() / 2, minPoint.getY());
        modifyElementLocation(secondHalf, parent, point, recursiveDimension);
      } else {
        Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth(), dimension.getHeight() / 2);
        modifyElementLocation(firstHalf, parent, minPoint, recursiveDimension);
        Point2D point = new Point2D.Double(minPoint.getX(), minPoint.getY() + dimension.getHeight() / 2);
        modifyElementLocation(secondHalf, parent, point, recursiveDimension);
      }
    }
    List<Compartment> compartmentWithElementsToRearrange = new ArrayList<>();
    compartmentWithElementsToRearrange.addAll(elementsByStaticCompartment.keySet());
    Collections.sort(compartmentWithElementsToRearrange, Element.SIZE_COMPARATOR);
    for (Compartment compartment : compartmentWithElementsToRearrange) {
      Point2D point = new Point2D.Double(compartment.getX(), compartment.getY());
      Dimension2D compDimension = new DoubleDimension(compartment.getWidth(), compartment.getHeight());
      modifyElementLocation(elementsByStaticCompartment.get(compartment), compartment, point, compDimension);
    }

  }

  private void modifyReactionLocation(Collection<Reaction> reactions) {
    for (Reaction reaction : reactions) {
      modifyReaction(reaction);
    }
  }

  private void modifyReaction(Reaction reaction) {
    for (AbstractNode node : reaction.getOperators()) {
      reaction.removeNode(node);
    }

    Element productElement = reaction.getProducts().get(0).getElement();
    Element reactantElement = reaction.getReactants().get(0).getElement();

    Point2D middle = getMiddlePoint(productElement.getCenter(), reactantElement.getCenter());

    // for self reactions
    if (productElement.equals(reactantElement)) {
      middle.setLocation(middle.getX(), middle.getY() + 50);
    }

    modifyProducts(reaction, middle);
    modifyReactants(reaction, middle);
    modifyModifiers(reaction, middle);

    if (super.isAddReactionPrefixes()) {
      super.getModel().removeReaction(reaction);
      reaction.setIdReaction("reaction_glyph_" + getNextId() + "__" + reaction.getIdReaction());
      super.getModel().addReaction(reaction);
    }
  }

  private Point2D.Double getMiddlePoint(Point2D middle, Point2D productPoint) {
    return new Point2D.Double((productPoint.getX() + middle.getX()) / 2, (productPoint.getY() + middle.getY()) / 2);
  }

  private void modifyReactants(Reaction reaction, Point2D middle) {
    AndOperator operator = null;
    if (reaction.getReactants().size() > 1) {
      operator = new AndOperator();
      Point2D productPoint = reaction.getReactants().get(0).getElement().getCenter();
      Point2D operatorPoint = getMiddlePoint(middle, productPoint);
      operator.setLine(new PolylineData(middle, operatorPoint));
      reaction.addNode(operator);
      middle = operatorPoint;
    }
    for (Reactant reactant : reaction.getReactants()) {
      PolylineData line = new PolylineData();
      line.addPoint(reactant.getElement().getCenter());
      line.addPoint(middle);
      line.addPoint(middle);
      if (reaction.isReversible()) {
        line.getBeginAtd().setArrowType(ArrowType.FULL);
      }
      reactant.setLine(line);
      if (operator != null) {
        operator.addInput(reactant);
      }
    }
  }

  private void modifyModifiers(Reaction reaction, Point2D middle) {
    for (Modifier modifier : reaction.getModifiers()) {
      PolylineData line = new PolylineData(middle, modifier.getElement().getCenter());
      modifier.setLine(line);
      new ModifierTypeUtils().updateLineEndPoint(modifier);
    }
  }

  private void modifyProducts(Reaction reaction, Point2D middle) {
    SplitOperator operator = null;
    if (reaction.getProducts().size() > 1) {
      operator = new SplitOperator();
      Point2D productPoint = reaction.getProducts().get(0).getElement().getCenter();
      Point2D operatorPoint = getMiddlePoint(middle, productPoint);
      operator.setLine(new PolylineData(middle, operatorPoint));
      reaction.addNode(operator);
      middle = operatorPoint;
    }
    for (Product product : reaction.getProducts()) {
      PolylineData line = new PolylineData(middle, product.getElement().getCenter());
      line.getEndAtd().setArrowType(ArrowType.FULL);
      product.setLine(line);
      if (operator != null) {
        operator.addOutput(product);
      }
    }

  }

  protected void modifySpeciesListLocation(Set<Species> speciesList, Point2D minPoint, Dimension2D dimension) {
    double middleX = dimension.getWidth() / 2 + minPoint.getX();
    double middleY = dimension.getHeight() / 2 + minPoint.getY();

    double radiusX = Math.min(dimension.getWidth() / 3, dimension.getWidth() - SPECIES_WIDTH / 2);
    if (radiusX < 1) {
      radiusX = 1;
    }
    double radiusY = Math.min(dimension.getHeight() / 3, dimension.getHeight() - SPECIES_HEIGHT / 2);
    if (radiusY < 1) {
      radiusY = 1;
    }
    double radius = Math.min(radiusX, radiusY);

    double size = speciesList.size();
    double index = 0;

    for (Species element : speciesList) {
      if (element.getComplex() == null) {
        double angle = index / size * 2 * Math.PI;
        double elementCenterX = middleX + Math.sin(angle) * radius;
        double elementCenterY = middleY + Math.cos(angle) * radius;
        if (element instanceof Complex) {
          modifyComplexLocation((Complex) element, elementCenterX, elementCenterY);
        } else {
          element.setWidth(SPECIES_WIDTH);
          element.setHeight(SPECIES_HEIGHT);
        }
        element.setX(elementCenterX - SPECIES_WIDTH / 2);
        element.setY(elementCenterY - SPECIES_HEIGHT / 2);
        index++;
      }
    }

  }

  protected void modifyModelSize(Model model, Point2D minPoint, Dimension2D dimension) {
    double width = 0;
    double height = 0;
    if (model.getWidth() != null) {
      width = model.getWidth();
    }
    if (model.getHeight() != null) {
      height = model.getHeight();
    }
    width = Math.max(width, minPoint.getX() + dimension.getWidth());
    height = Math.max(height, minPoint.getY() + dimension.getHeight());
    model.setWidth(width);
    model.setHeight(height);
  }

  protected Point2D estimateLayoutMinPoint(Model model) {
    double minX = 0.0;
    double minY = 0.0;
    for (Element element : model.getElements()) {
      if (!getBioEntities().contains(element)) {
        if (element.getWidth() > 0 && element.getHeight() > 0) {
          minY = Math.max(minY, element.getBorder().getMaxY() + BORDER_FROM_EXISTING_ELEMENTS);
        }
      }
    }
    return new Point2D.Double(minX, minY);
  }

  protected Dimension2D estimateDimension(Collection<BioEntity> bioEntites) {
    double suggestedSize = Math.max(100 * bioEntites.size() / 4,
        Math.sqrt((SPECIES_WIDTH + 10) * (SPECIES_HEIGHT + 10) * bioEntites.size()));
    if (bioEntites.size() > 0) {
      suggestedSize = Math.max(suggestedSize, 2 * (SPECIES_WIDTH + 10));
    }
    return new DoubleDimension(suggestedSize, suggestedSize);
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();

  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();
  }

  protected void modifyComplexLocation(Complex complex, double centerX, double centerY) {
    if (complex.getWidth() == null || complex.getHeight() == null) {
      computeComplexSize(complex);
    }
    double childWidth = SPECIES_WIDTH;
    double childHeight = SPECIES_WIDTH;
    for (Species species : complex.getElements()) {
      childWidth = Math.max(childWidth, species.getWidth());
      childHeight = Math.max(childHeight, species.getHeight());
    }
    int rowSize = (int) Math.ceil(Math.sqrt(complex.getElements().size()));

    complex.setX(centerX - complex.getWidth() / 2);
    complex.setY(centerY - complex.getHeight() / 2);

    int x = 0;
    int y = 0;

    for (Species species : complex.getElements()) {
      species.setX(complex.getX() + (COMPLEX_PADDING + childWidth) * x);
      species.setY(complex.getY() + (COMPLEX_PADDING + childHeight) * y);
      x++;
      if (x == rowSize) {
        x = 0;
        y++;
      }
      if (species instanceof Complex) {
        modifyComplexLocation((Complex) species, species.getCenterX(), species.getCenterY());
      }
    }

  }

  private void computeComplexSize(Complex complex) {
    double childWidth = SPECIES_WIDTH;
    double childHeight = SPECIES_HEIGHT;
    for (Species species : complex.getElements()) {
      if (species instanceof Complex) {
        computeComplexSize((Complex) species);
      } else {
        species.setWidth(SPECIES_WIDTH);
        species.setHeight(SPECIES_HEIGHT);
      }
      childWidth = Math.max(childWidth, species.getWidth());
      childHeight = Math.max(childHeight, species.getHeight());
    }
    int rowSize = (int) Math.ceil(Math.sqrt(complex.getElements().size()));
    int rowCount = 0;
    if (rowSize == 0) {
      rowSize = 1;
      rowCount = 1;
    } else {
      rowCount = (complex.getElements().size() + rowSize - 1) / rowSize;
    }
    complex.setWidth((childWidth + COMPLEX_PADDING) * rowSize + COMPLEX_PADDING);
    complex.setHeight((childHeight + COMPLEX_PADDING) * rowCount + COMPLEX_PADDING);
  }

}
