package lcsb.mapviewer.commands;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;

/**
 * This {@link ModelCommand command} class allows to transform model into
 * multilevel (nested) component structure. Some artificial compartments will
 * appear. All compartments have information about children compartments. All
 * objects have information about parents.
 * 
 * 
 */
public class SetFixedHierarchyLevelCommand extends ModelCommand {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger = Logger.getLogger(SetFixedHierarchyLevelCommand.class);
	private Integer				level;

	public SetFixedHierarchyLevelCommand(Model model, Integer level) {
		super(model);
		this.level = level;
	}

	@Override
	protected void undoImplementation() {
		throw new NotImplementedException();
	}

	@Override
	protected void executeImplementation() {
		if (!ModelCommandStatus.CREATED.equals(getStatus()) && !ModelCommandStatus.UNDONE.equals(getStatus())) {
			throw new InvalidStateException("To execute command, the command must be in CREATED or UNDONE state. " + getStatus() + " found.");
		}
		if (level != null) {
			SemanticZoomLevelMatcher matcher = new SemanticZoomLevelMatcher();
			Model output = getModel();
			for (BioEntity bioEntity : output.getBioEntities()) {
				if (matcher.isVisible(level, bioEntity.getVisibilityLevel())) {
					bioEntity.setVisibilityLevel("0");
				} else {
					bioEntity.setVisibilityLevel(Integer.MAX_VALUE + "");
				}
			}
		}

		setStatus(ModelCommandStatus.EXECUTED);
	}

	@Override
	protected void redoImplementation() {
		throw new NotImplementedException();
	}

}
