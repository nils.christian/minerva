package lcsb.mapviewer.commands.properties;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.ModelCommandStatus;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes single
 * property of the element connected to {@link Element}.
 * 
 * @param <T>
 *          class of property to edit
 * 
 * @author Piotr Gawron
 *
 */
public abstract class ChangeElementPropertyCommand<T> extends ModelCommand {

	/**
	 * New element property value.
	 */
	private T		 newValue;

	/**
	 * Old element property value.
	 */
	private T		 oldValue;

	/**
	 * {@link Element} for which we will change the property.
	 */
	private Element alias;

	/**
	 * Default constructor.
	 * 
	 * @param model
	 *          model to move
	 * 
	 * @param alias
	 *          alias to be changed
	 * 
	 * @param newValue
	 *          new value of the element property
	 */
	public ChangeElementPropertyCommand(Model model, Element alias, T newValue) {
		super(model);
		this.alias = alias;
		this.newValue = newValue;
	}

	@Override
	protected void undoImplementation() throws CommandExecutionException {
		T tmp = newValue;
		newValue = oldValue;
		executeImplementation();
		newValue = tmp;
		oldValue = null;
		setStatus(ModelCommandStatus.UNDONE);
	}

	@Override
	protected void redoImplementation() throws CommandExecutionException {
		executeImplementation();
		setStatus(ModelCommandStatus.EXECUTED);
	}

	/**
	 * @return the alias
	 * @see #alias
	 */
	protected Element getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *          the alias to set
	 * @see #alias
	 */
	protected void setAlias(Element alias) {
		this.alias = alias;
	}

	/**
	 * @return the oldValue
	 * @see #oldValue
	 */
	protected Object getOldValue() {
		return oldValue;
	}

	/**
	 * @param oldValue
	 *          the oldValue to set
	 * @see #oldValue
	 */
	protected void setOldValue(T oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * @return the newValue
	 * @see #newValue
	 */
	protected Object getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue
	 *          the newValue to set
	 * @see #newValue
	 */
	protected void setNewValue(T newValue) {
		this.newValue = newValue;
	}
}
