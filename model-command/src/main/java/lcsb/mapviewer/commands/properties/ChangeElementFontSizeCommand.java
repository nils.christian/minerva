package lcsb.mapviewer.commands.properties;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

import org.apache.log4j.Logger;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes
 * {@link Element#fontSize}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementFontSizeCommand extends ChangeElementPropertyCommand<Double> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(ChangeElementFontSizeCommand.class);

	/**
	 * Default constructor.
	 * 
	 * @param model
	 *          {@link lcsb.mapviewer.commands.ModelCommand#model}
	 * @param alias
	 *          {@link ChangeElementPropertyCommand#alias}
	 * @param newName
	 *          new {@link Element#fontSize} value
	 */
	public ChangeElementFontSizeCommand(Model model, Element alias, Double newName) {
		super(model, alias, newName);
	}

	@Override
	protected void executeImplementation() {

		// abbreviation is not visualized, so we don't need to report any
		// visualization changes
		// includeInAffectedRegion(getAlias());

		setOldValue(getAlias().getFontSize());
		getAlias().setFontSize((Double) getNewValue());

		// abbreviation is not visualized, so we don't need to report any
		// visualization changes
		// includeInAffectedRegion(getAlias());
	}
}
