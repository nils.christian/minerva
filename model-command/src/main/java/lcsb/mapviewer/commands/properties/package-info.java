/**
 * This package contains {@link lcsb.mapviewer.commands.ModelCommand commands}
 * that change properies of {@link java.lang.reflect.AnnotatedElement elements }
 * or {@link lcsb.mapviewer.model.map.species.Element aliases}.
 * 
 */
package lcsb.mapviewer.commands.properties;

