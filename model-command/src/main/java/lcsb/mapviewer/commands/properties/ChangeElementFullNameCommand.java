package lcsb.mapviewer.commands.properties;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

import org.apache.log4j.Logger;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes full
 * name of the element connected to {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementFullNameCommand extends ChangeElementPropertyCommand<String> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(ChangeElementFullNameCommand.class);

	/**
	 * Default constructor.
	 * 
	 * @param model
	 *          {@link lcsb.mapviewer.commands.ModelCommand#model}
	 * @param alias
	 *          {@link ChangeElementPropertyCommand#alias}
	 * @param newName
	 *          new {@link Element#fullName} value
	 */
	public ChangeElementFullNameCommand(Model model, Element alias, String newName) {
		super(model, alias, newName);
	}

	@Override
	protected void executeImplementation() {

		// fullname is not visualized, so we don't need to report any
		// visualization changes
		// includeInAffectedRegion(getAlias());

		setOldValue(getAlias().getFullName());
		getAlias().setFullName((String) getNewValue());

		// fullname is not visualized, so we don't need to report any
		// visualization changes
		// includeInAffectedRegion(getAlias());
	}
}
