package lcsb.mapviewer.commands.properties;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

import org.apache.log4j.Logger;

/**
 * {@link lcsb.mapviewer.commands.ModelCommand ModelCommand} that changes name
 * of the element connected to {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class ChangeElementAbbreviationCommand extends ChangeElementPropertyCommand<String> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(ChangeElementAbbreviationCommand.class);

	/**
	 * Default constructor.
	 * 
	 * @param model
	 *          {@link lcsb.mapviewer.commands.ModelCommand#model}
	 * @param alias
	 *          {@link ChangeElementPropertyCommand#alias}
	 * @param newName
	 *          new name of the elemnt
	 */
	public ChangeElementAbbreviationCommand(Model model, Element alias, String newName) {
		super(model, alias, newName);
	}

	@Override
	protected void executeImplementation() {
		// abbreviation is not visualized, so we don't need to report any
		// visualization changes
		// includeInAffectedRegion(getAlias());

		setOldValue(getAlias().getAbbreviation());
		getAlias().setAbbreviation((String) getNewValue());

		// abbreviation is not visualized, so we don't need to report any
		// visualization changes
		// includeInAffectedRegion(getAlias());
	}
}
