package lcsb.mapviewer.commands;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This {@link ModelCommand command} class allows to transform model into
 * multilevel (nested) component structure. Some artificial compartments will
 * appear. All compartments have information about children compartments. All
 * objects have information about parents.
 * 
 * 
 */
public class CreateHierarchyCommand extends ModelCommand {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger				logger																					= Logger.getLogger(CreateHierarchyCommand.class);
	/**
	 * Natural logarithm of four.
	 */
	private static final double	LOG_4																						= Math.log(4);

	/**
	 * Top left corner x coordinate of the text associated with compratment.
	 */
	private static final double	DEFAULT_TITLE_X_COORD_IN_ARTIFITIAL_COMPARTMENT	= 10;
	/**
	 * Top left corner y coordinate of the text associated with compratment.
	 */
	private static final double	DEFAULT_TITLE_Y_COORD_IN_ARTIFITIAL_COMPARTMENT	= 10;

	/**
	 * How many levels are possible.
	 */
	private int									zoomLevels;
	/**
	 * What is the maximum zoom factor in the view.
	 */
	private double							maxZoomFactor;

	/**
	 * Default constructor that intializes data.
	 * 
	 * @param model
	 *          model on which command will be executed
	 * @param maxZoomFactor
	 *          what is the maximum zoom out factor
	 * @param zoomLevels
	 *          how many levels are possible
	 */
	public CreateHierarchyCommand(Model model, int zoomLevels, double maxZoomFactor) {
		super(model);
		this.zoomLevels = zoomLevels;
		this.maxZoomFactor = maxZoomFactor;
	}

	@Override
	protected void undoImplementation() {
		throw new NotImplementedException();
	}

	@Override
	protected void executeImplementation() {
		if (!ModelCommandStatus.CREATED.equals(getStatus()) && !ModelCommandStatus.UNDONE.equals(getStatus())) {
			throw new InvalidStateException("To execute command, the command must be in CREATED or UNDONE state. " + getStatus() + " found.");
		}
		Model model = getModel();
		List<Species> compacts = new ArrayList<>();
		for (Element alias : model.getElements()) {
			if (alias instanceof Complex) {
				if (((Complex) alias).getState().contains("brief")) {
					compacts.add((Species) alias);
					compacts.addAll(((Complex) alias).getElements());
				}
			}
		}
		model.getElements().removeAll(compacts);
		clean();
		createArtificials();
		assignAliases();

		List<Element> sortedAliases = model.getElementsSortedBySize();
		setParentingAndChildreningOfNonComplexChildrens(sortedAliases);
		model.getElements().addAll(compacts);
		sortedAliases = model.getElementsSortedBySize();
		preSettingOfVisibilityLevel(sortedAliases, maxZoomFactor);
		settingOfTransparencyLevel(sortedAliases);

		setStatus(ModelCommandStatus.EXECUTED);
	}

	@Override
	protected void redoImplementation() {
		throw new NotImplementedException();
	}

	/**
	 * Cleans hierarchical information from the model.
	 */
	protected void clean() {
		for (Element alias : getModel().getElements()) {
			alias.setCompartment(null);
			alias.setTransparencyLevel("");
		}
		for (BioEntity bioEntity : getModel().getBioEntities()) {
			bioEntity.setVisibilityLevel("0");
		}
		Set<Compartment> toRemove = new HashSet<>();
		for (Compartment alias : getModel().getCompartments()) {
			if (alias instanceof PathwayCompartment) {
				toRemove.add(alias);
			}
		}
		for (Compartment alias : toRemove) {
			getModel().removeElement(alias);
		}
	}

	/**
	 * Creates artifitial compartment alias for rectangles and texts object on
	 * additional graphic layers.
	 */
	private void createArtificials() {
		Model model = getModel();
		int id = 0;
		for (Layer layer : model.getLayers()) {
			for (LayerRect rect : layer.getRectangles()) {
				PathwayCompartment alias = new PathwayCompartment("art" + (id++));
				alias.setX(rect.getX());
				alias.setY(rect.getY());
				alias.setWidth(rect.getWidth());
				alias.setHeight(rect.getHeight());
				alias.setColor(rect.getColor());
				alias.setNamePoint(
						new Point2D.Double(rect.getX() + DEFAULT_TITLE_X_COORD_IN_ARTIFITIAL_COMPARTMENT, rect.getY() + DEFAULT_TITLE_Y_COORD_IN_ARTIFITIAL_COMPARTMENT));

				model.addElement(alias);
			}
			for (LayerText text : layer.getTexts()) {
				PathwayCompartment alias = new PathwayCompartment("art" + (id++));
				alias.setX(text.getX());
				alias.setY(text.getY());
				alias.setWidth(text.getWidth());
				alias.setHeight(text.getHeight());
				alias.setColor(text.getColor());
				alias.setName(text.getNotes().trim());
				alias.setNamePoint(
						new Point2D.Double(text.getX() + DEFAULT_TITLE_X_COORD_IN_ARTIFITIAL_COMPARTMENT, text.getY() + DEFAULT_TITLE_Y_COORD_IN_ARTIFITIAL_COMPARTMENT));

				model.addElement(alias);

			}
		}
	}

	/**
	 * Assign aliases in hierarchical structure.
	 */
	private void assignAliases() {
		assignToCompartments();
	}

	/**
	 * Computes visibility levels for aliases based on the size.
	 * 
	 * @param sortedAliases
	 *          list of aliases
	 * @param maxZoomFactor
	 *          max scale used on the map
	 */
	private void preSettingOfVisibilityLevel(List<Element> sortedAliases, double maxZoomFactor) {
		for (Element alias : sortedAliases) {
			double rate = computeRate(alias, Configuration.MIN_VISIBLE_OBJECT_SIZE);
			int logValue = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
			boolean hasCompartment = alias.getCompartment() != null;
			boolean hasComplex = false;
			if (alias instanceof Species) {
				hasComplex = ((Species) alias).getComplex() != null;
			}
			if (!hasCompartment && !hasComplex) {
				logValue = 0;
			}
			if (logValue >= zoomLevels) {
				if (hasComplex) {
					logValue = zoomLevels - 1;
				} else {
					logValue = zoomLevels;
				}
			}
			alias.setVisibilityLevel(logValue + "");
		}
	}

	/**
	 * Sets transparency level in hierarchical view for compartment alias.
	 * 
	 * @param compartment
	 *          comaprtment alias
	 */
	private void settingTransparencyLevelForCompartment(Compartment compartment) {
		int maxVisibilityLevel = Integer.MAX_VALUE;
		double rate = computeRate(compartment, Configuration.MAX_VISIBLE_OBJECT_SIZE);
		maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
		for (Element child : compartment.getElements()) {
			maxVisibilityLevel = Math.min(maxVisibilityLevel, Integer.valueOf(child.getVisibilityLevel()));
		}
		if (maxVisibilityLevel >= zoomLevels) {
			maxVisibilityLevel = zoomLevels;
		}
		if (maxVisibilityLevel <= 0) {
			maxVisibilityLevel = 1;
		}
		compartment.setTransparencyLevel(maxVisibilityLevel + "");
		for (Element child : compartment.getElements()) {
			child.setVisibilityLevel(compartment.getTransparencyLevel());
		}
	}

	/**
	 * Sets transparency level in hierarchical view for complex alias.
	 * 
	 * @param complex
	 *          complex alias
	 */
	private void settingTransparencyLevelForComplex(Complex complex) {
		int maxVisibilityLevel = Integer.MAX_VALUE;
		double rate = computeRate(complex, Configuration.MAX_VISIBLE_OBJECT_SIZE);
		maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
		for (Element child : complex.getElements()) {
			maxVisibilityLevel = Math.min(maxVisibilityLevel, Integer.valueOf(child.getVisibilityLevel()));
		}
		if (maxVisibilityLevel >= zoomLevels) {
			maxVisibilityLevel = zoomLevels;
		}
		if (maxVisibilityLevel <= 0) {
			maxVisibilityLevel = 1;
		}
		complex.setTransparencyLevel(maxVisibilityLevel + "");
		for (Element child : complex.getElements()) {
			child.setVisibilityLevel(complex.getTransparencyLevel());
		}
	}

	/**
	 * Sets transparency level in hierarchical view for all elements.
	 * 
	 * @param sortedAliases
	 *          list of aliases
	 */
	private void settingOfTransparencyLevel(List<Element> sortedAliases) {
		for (Element alias : sortedAliases) {
			if (alias instanceof Compartment) {
				settingTransparencyLevelForCompartment((Compartment) alias);
			} else if (alias instanceof Complex) {
				settingTransparencyLevelForComplex((Complex) alias);
			} else {
				alias.setTransparencyLevel("0");
			}
		}
	}

	/**
	 * Removes invalid compartment children.
	 * 
	 * @param sortedAliases
	 *          list of aliases
	 */
	private void setChildrening(List<Element> sortedAliases) {
		for (Element compartment : sortedAliases) {
			if (compartment instanceof Compartment) {
				Set<Element> removable = new HashSet<>();
				for (Element alias : ((Compartment) compartment).getElements()) {
					if (alias.getCompartment() != compartment) {
						removable.add(alias);
					}
				}
				((Compartment) compartment).getElements().removeAll(removable);
			}
		}
	}

	/**
	 * Sets parent for elements.
	 * 
	 * @param sortedAliases
	 *          list of aliases
	 */
	private void setParentingOfNonComplexChildrens(List<Element> sortedAliases) {
		for (Element element : getModel().getElements()) {
			if (element.getCompartment() == null) {
				for (Element compartment : sortedAliases) {
					if (compartment instanceof Compartment) {
						if (((Compartment) compartment).getElements().contains(element)) {
							element.setCompartment((Compartment) compartment);
						}
					}
				}

			}
			if (element instanceof Species) {
				Species species = (Species) element;
				if (species.getComplex() == null) {
					for (Element complex : sortedAliases) {
						if (complex instanceof Complex) {
							if (((Complex) complex).getElements().contains(element)) {
								species.setComplex((Complex) complex);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Set parents for the elements in hierarchical view.
	 * 
	 * @param sortedAliases
	 *          list of aliases
	 */
	private void setParentingAndChildreningOfNonComplexChildrens(List<Element> sortedAliases) {
		setParentingOfNonComplexChildrens(sortedAliases);
		setChildrening(sortedAliases);
	}

	/**
	 * Assign aliases to compartments.
	 */
	private void assignToCompartments() {
		Compartment nullCompartment = new Compartment("null");
		nullCompartment.setWidth(Double.MAX_VALUE);
		nullCompartment.setHeight(Double.MAX_VALUE);
		for (Element element : getModel().getElements()) {
			Compartment supposedParent = nullCompartment;
			for (Compartment alias : getModel().getCompartments()) {
				if (alias.contains(element) && alias.getSize() < supposedParent.getSize()) {
					supposedParent = alias;
				}
			}
			if (supposedParent != nullCompartment) {
				supposedParent.addElement(element);
			}
		}
	}

	/**
	 * Computes the ratio between the minimal object that should be visible, and
	 * alias visible on the top level.
	 * 
	 * @param alias
	 *          alias for which computation is done
	 * @param limit
	 *          size of the minimal visibe object
	 * @return ratio between the minimal object that should be visible, and alias
	 *         visible on the top level
	 */
	private double computeRate(Element alias, double limit) {
		double length = alias.getWidth() / maxZoomFactor;
		double height = alias.getHeight() / maxZoomFactor;
		double size = length * height;
		return (double) Math.ceil(limit / size);
	}

}
