package lcsb.mapviewer.commands;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelConnection;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Command that creates a new instance of the model with the same data.
 * 
 */

public class CopyCommand extends NewModelCommand {
  /**
   * Defaul class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(CopyCommand.class);

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link #model}
   */
  public CopyCommand(Model model) {
    super(model);
  }

  /**
   * Executed the operation.
   * 
   * @return copy of the model
   */
  public Model execute() {
    Map<Model, Model> copies = new HashMap<>();

    Model model = getModel();

    if (model.getModelData().getParentModels().size() > 0) {
      ModelSubmodelConnection parent = null;
      for (SubmodelConnection connection : model.getModelData().getParentModels()) {
        if (connection instanceof ModelSubmodelConnection) {
          if (parent != null) {
            throw new InvalidArgumentException(
                "It looks like the model is a submodel, but has more than one parent...");
          } else {
            parent = (ModelSubmodelConnection) connection;
          }
        }
      }
      if (parent != null) {
        CopyCommand copyParentCommand = new CopyCommand(parent.getParentModel().getModel());
        Model copy = copyParentCommand.execute();
        for (ModelSubmodelConnection submodel : copy.getSubmodelConnections()) {
          if (submodel.getName().equals(parent.getName())) {
            return submodel.getSubmodel().getModel();
          }
        }
        throw new InvalidStateException("Problem with copying submodel of a model");
      }
    }

    Model result = createNotNestedCopy(model);

    copies.put(model, result);

    for (ModelSubmodelConnection submodelConnection : model.getSubmodelConnections()) {
      Model submodel = submodelConnection.getSubmodel().getModel();
      Model submodelCopy = createNotNestedCopy(submodel);
      copies.put(submodel, submodelCopy);
    }
    for (Model modelCopy : copies.values()) {
      try {
        assignModelCopies(modelCopy, copies);
      } catch (InvalidModelException e) {
        throw new InvalidArgumentException(e);
      }
    }
    return result;

  }

  /**
   * Assign information about submodel to the copied links.
   * 
   * @param modelCopy
   *          copy of a model where substitution should be made
   * @param copies
   *          map with map copies
   * @throws InvalidModelException
   *           when there is inconsistency in data
   */
  private void assignModelCopies(Model modelCopy, Map<Model, Model> copies) throws InvalidModelException {
    for (ModelSubmodelConnection connection : modelCopy.getSubmodelConnections()) {
      // copy connection reference
      Model original = connection.getSubmodel().getModel();
      Model copy = copies.get(original);
      if (copy == null) {
        throw new InvalidModelException("Original model contain reference to model that wasn't copied");
      }
      connection.setSubmodel(copy);

      // copy connection parent reference
      if (connection.getParentModel() != null) {
        original = connection.getParentModel().getModel();
        if (original != null) {
          copy = copies.get(original);
          if (copy == null) {
            if (!copies.values().contains(original)) {
              throw new InvalidModelException("Original model contain reference to model that wasn't copied");
            }
          } else {
            connection.setParentModel(copy);
          }
        }
      }
    }
    for (Element alias : modelCopy.getElements()) {
      // if alias has connection to submodel
      if (alias.getSubmodel() != null) {
        ElementSubmodelConnection connection = alias.getSubmodel();
        // copy information about submodel
        Model original = connection.getSubmodel().getModel();
        Model copy = copies.get(original);
        if (copy == null) {
          throw new InvalidModelException("Original model contain reference to model that wasn't copied");
        }
        connection.setSubmodel(copy);

        // copy information about original alias
        if (connection.getFromElement() != null) {
          connection.setFromElement(modelCopy.getElementByElementId(connection.getFromElement().getElementId()));
        }
        // copy information about reference alias
        if (connection.getToElement() != null) {
          connection.setToElement(modelCopy.getElementByElementId(connection.getToElement().getElementId()));
        }
      }
    }
  }

  /**
   * Creates copy of the model without modifying information about submodels
   * (information about original submodels will be left in the copy).
   * 
   * @param model
   *          original model to copy
   * @return copy of the model
   */
  protected Model createNotNestedCopy(Model model) {
    Model result = new ModelFullIndexed(null);

    for (SbmlUnit unit : model.getUnits()) {
      result.addUnit(unit.copy());
    }

    for (SbmlFunction function : model.getFunctions()) {
      result.addFunction(function.copy());
    }

    for (SbmlParameter parameter : model.getParameters()) {
      result.addParameter(parameter.copy());
    }

    for (Element alias : model.getElements()) {
      if (alias instanceof Compartment) {
        Compartment copy = ((Compartment) alias).copy();
        copy.getElements().clear();
        copy.setCompartment(null);
        result.addElement(copy);
      }

    }

    for (Element alias : model.getElements()) {
      if (alias instanceof Species) {
        Species copy = ((Species) alias).copy();
        copy.setCompartment(null);
        result.addElement(copy);

        Compartment parentCompartment = alias.getCompartment();

        if (parentCompartment != null) {
          parentCompartment = result.getElementByElementId(parentCompartment.getElementId());
          copy.setCompartment(parentCompartment);
        }

      } else if (alias instanceof Compartment) {

        Compartment parentCompartment = alias.getCompartment();

        if (parentCompartment != null) {
          Compartment copy = result.getElementByElementId(alias.getElementId());
          parentCompartment = result.getElementByElementId(parentCompartment.getElementId());
          copy.setCompartment(parentCompartment);
        }
      } else {
        throw new InvalidClassException("Don't know what to do with: " + alias.getClass());
      }
    }

    for (Layer layer : model.getLayers()) {
      result.addLayer(layer.copy());
    }

    for (Reaction reaction : model.getReactions()) {
      result.addReaction(createCopy(reaction));
    }

    assignSimpleDataToCopy(result, model);
    for (Layout layout : model.getLayouts()) {
      result.addLayout(layout.copy());
    }
    result.setProject(model.getProject());

    for (Element element : result.getElements()) {
      updateElementReferences(element, result, model);
    }
    for (Reaction reaction : result.getReactions()) {
      updateReactionReferences(reaction, result);
    }
    for (ModelSubmodelConnection connection : model.getSubmodelConnections()) {
      result.addSubmodelConnection(connection.copy());
    }
    return result;
  }

  /**
   * Copies simple information about model to the copy.
   * 
   * @param copy
   *          to this model data will be copied
   * @param original
   *          original model
   */
  private void assignSimpleDataToCopy(Model copy, Model original) {
    copy.setName(original.getName());

    copy.setCreationDate(original.getCreationDate());

    copy.setWidth(original.getWidth());
    copy.setHeight(original.getHeight());
    copy.setNotes(original.getNotes());
    copy.setIdModel(original.getIdModel());
    copy.setZoomLevels(original.getZoomLevels());
    copy.setTileSize(original.getTileSize());
  }

  /**
   * Creates copy of the reaction that doesn't contain any references to the
   * original (so we have to substitute all references with the new ones...).
   * 
   * @param reaction
   *          original reaction
   * @return copy of the reaction
   */
  private Reaction createCopy(Reaction reaction) {
    Reaction copy = reaction.copy();
    reaction.getNodes().clear();
    for (AbstractNode node : copy.getNodes()) {
      reaction.addNode(node);
    }
    copy.getNodes().clear();

    for (AbstractNode node : reaction.getNodes()) {
      AbstractNode nodeCopy = node.copy();
      nodeCopy.setLine(new PolylineData(nodeCopy.getLine()));
      copy.addNode(nodeCopy);
    }

    for (int i = 0; i < copy.getOperators().size(); i++) {
      NodeOperator nodeCopy = copy.getOperators().get(i);
      NodeOperator nodeOriginal = reaction.getOperators().get(i);
      for (int j = 0; j < nodeCopy.getInputs().size(); j++) {
        AbstractNode input = nodeCopy.getInputs().get(j);
        int index1 = reaction.getOperators().indexOf(input);
        int index2 = reaction.getNodes().indexOf(input);
        if (index1 >= 0) {
          nodeCopy.getInputs().set(j, copy.getOperators().get(index1));
          copy.getOperators().get(index1).setNodeOperatorForInput(nodeCopy);

          reaction.getOperators().get(index1).setNodeOperatorForInput(nodeOriginal);
        } else if (index2 >= 0) {
          nodeCopy.getInputs().set(j, copy.getNodes().get(index2));
          copy.getNodes().get(index2).setNodeOperatorForInput(nodeCopy);

          reaction.getNodes().get(index2).setNodeOperatorForInput(nodeOriginal);
        } else {
          throw new InvalidStateException("WTF");
        }
      }

      for (int j = 0; j < nodeCopy.getOutputs().size(); j++) {
        AbstractNode output = nodeCopy.getOutputs().get(j);
        int index1 = reaction.getOperators().indexOf(output);
        int index2 = reaction.getNodes().indexOf(output);
        if (index1 >= 0) {
          nodeCopy.getOutputs().set(j, copy.getOperators().get(index1));
          copy.getOperators().get(index1).setNodeOperatorForOutput(nodeCopy);

          reaction.getOperators().get(index1).setNodeOperatorForOutput(nodeOriginal);
        } else if (index2 >= 0) {
          nodeCopy.getOutputs().set(j, copy.getNodes().get(index2));
          copy.getNodes().get(index2).setNodeOperatorForOutput(nodeCopy);

          reaction.getNodes().get(index2).setNodeOperatorForOutput(nodeOriginal);
        } else {
          throw new InvalidStateException("WTF");
        }
      }
    }

    return copy;
  }

  /**
   * Updates references to elements and aliases in reaction to the objects taken
   * from model.
   * 
   * @param reaction
   *          references in this reaction will be updated
   * @param model
   *          references from this model will be taken
   */
  private void updateReactionReferences(Reaction reaction, Model model) {
    reaction.setModel(model);
    for (ReactionNode rNode : reaction.getReactionNodes()) {
      rNode.setElement(model.getElementByElementId(rNode.getElement().getElementId()));
    }
    SbmlKinetics kinetics = reaction.getKinetics();
    if (kinetics != null) {
      Collection<SbmlArgument> arguments = new HashSet<>();
      for (SbmlArgument argument : kinetics.getArguments()) {
        if (argument instanceof Element) {
          arguments.add(model.getElementByElementId(argument.getElementId()));
        } else if (argument instanceof SbmlParameter) {
          if (model.getParameterById(argument.getElementId()) != null) {
            arguments.add(model.getParameterById(argument.getElementId()));
          } else {
            arguments.add(argument);
          }
        } else if (argument instanceof SbmlFunction) {
          arguments.add(model.getFunctionById(argument.getElementId()));
        } else {
          throw new InvalidStateException("Don't know what to do with class: " + argument.getClass());
        }
      }
      kinetics.removeArguments(kinetics.getArguments());
      kinetics.addArguments(arguments);
    }

  }

  /**
   * Updates information (like elements, parent aliases) in the alias with the
   * data from new model.
   * 
   * @param element
   *          object to be updated
   * @param model
   *          data from this model will be used for update
   * @param originalModel
   *          original model from which alias copy was created
   */
  private void updateElementReferences(Element element, Model model, Model originalModel) {
    if (element instanceof Compartment) {
      Compartment compartment = (Compartment) element;
      Compartment original = originalModel.getElementByElementId(element.getElementId());
      for (Element child : original.getElements()) {
        compartment.addElement(model.getElementByElementId(child.getElementId()));
      }
    }

    if (element instanceof Complex) {
      Complex complex = (Complex) element;
      for (int i = 0; i < complex.getElements().size(); i++) {
        Species newElement = model.getElementByElementId(complex.getElements().get(i).getElementId());
        complex.getElements().set(i, newElement);
      }
    }
    if (element instanceof Species) {
      Species species = (Species) element;

      if (species.getComplex() != null) {
        species.setComplex((Complex) model.getElementByElementId(species.getComplex().getElementId()));
      }

    }

    element.setModel(model);

    if (element.getCompartment() != null) {
      element.setCompartment(model.getElementByElementId(element.getCompartment().getElementId()));
    }
  }
}
