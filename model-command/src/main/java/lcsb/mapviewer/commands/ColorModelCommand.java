package lcsb.mapviewer.commands;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This {@link ModelCommand} colors a model (nodes and reactions) according to
 * set of {@link ColorSchema rules}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ColorModelCommand extends ModelCommand {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ColorModelCommand.class);

  /**
   * Set of color schemas used in this command to color model.
   */
  private Collection<ColorSchema> schemas;

  /**
   * Object that helps to convert {@link ColorSchema} values into colors.
   * 
   */
  private ColorExtractor colorExtractor;

  /**
   * Util class for all {@link BioEntity} elements.
   */
  private ElementUtils eu = new ElementUtils();

  private StringComparator stringComparator = new StringComparator();

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          object that helps to convert {@link ColorSchema} values into colors
   * @param model
   *          original model
   * @param schemas
   *          set of color schemas used in this command to color model.
   */
  public ColorModelCommand(Model model, Collection<ColorSchema> schemas, ColorExtractor colorExtractor) {
    super(model);
    this.schemas = schemas;
    this.colorExtractor = colorExtractor;
  }

  /**
   * Applies color schema into the reaction.
   * 
   * @param reaction
   *          object to be colored
   * @param schema
   *          color schema to be used
   * @throws InvalidColorSchemaException
   *           thrown when {@link Reaction} was already colored by other schema
   */
  private void applyColor(Reaction reaction, ColorSchema schema) throws InvalidColorSchemaException {
    if (!reaction.getReactants().get(0).getLine().getColor().equals(Color.BLACK)) {
      throw new InvalidColorSchemaException(
          "At least two rows try to set color to reaction: " + reaction.getIdReaction());
    }

    Color color = colorExtractor.getNormalizedColor(schema);
    for (AbstractNode node : reaction.getNodes()) {
      node.getLine().setColor(color);
      if (schema.getLineWidth() != null) {
        node.getLine().setWidth(schema.getLineWidth());
      }
    }
    if (schema.getReverseReaction() != null && schema.getReverseReaction()) {
      ArrowTypeData atdBegining = reaction.getReactants().get(0).getLine().getBeginAtd();
      ArrowTypeData atdEnd = reaction.getProducts().get(0).getLine().getEndAtd();
      for (Reactant reactant : reaction.getReactants()) {
        reactant.getLine().setBeginAtd(atdEnd);
      }
      for (Product product : reaction.getProducts()) {
        product.getLine().setEndAtd(atdBegining);
      }
    }
  }

  /**
   * Checks if the coloring schema should be used for the reaction.
   * 
   * @param reaction
   *          reaction to which coloring schema is checked
   * @param schema
   *          coloring schema
   * @return <code>true</code> if coloring schema should be used for
   *         {@link Reaction}, <code>false</code> otherwise
   */
  private boolean match(Reaction reaction, ColorSchema schema) {
    if (schema.getName() != null) {
      return false;
    }
    if (!modelMatch(reaction.getModelData(), schema)) {
      return false;
    }

    for (MiriamData md : schema.getMiriamData()) {
      if (!reaction.getMiriamData().contains(md)) {
        return false;
      }
    }

    return true;
  }

  private boolean modelMatch(ModelData model, ColorSchema schema) {
    if (schema.getModelName() != null && !schema.getModelName().isEmpty()) {
      if (model == null) {
        logger.warn("Model of element is null...");
        return false;
      }
      if (stringComparator.compare(model.getName(), schema.getModelName()) != 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * Applies color schema into the {@link Element}.
   * 
   * @param element
   *          object to be colored
   * @param schema
   *          color schema to be used
   * @throws InvalidColorSchemaException
   *           thrown when alias was already colored by other schema
   */
  private void applyColor(Element element, ColorSchema schema) throws InvalidColorSchemaException {
    if (element instanceof Species) {
      if (!element.getColor().equals(Color.WHITE)) {
        throw new InvalidColorSchemaException("At least two rows try to set color to element: " + element.getName());
      }
      element.setColor(colorExtractor.getNormalizedColor(schema));
    }
  }

  protected boolean match(BioEntity element, ColorSchema schema) {
    if (schema.getElementId() != null && !schema.getElementId().isEmpty()) {
      if (!element.getElementId().equalsIgnoreCase(schema.getElementId())) {
        return false;
      }
    }
    if (element instanceof Element) {
      return match((Element) element, schema);
    } else if (element instanceof Reaction) {
      return match((Reaction) element, schema);
    } else {
      throw new InvalidArgumentException("Don't know how to handle object: " + element);
    }

  }

  /**
   * Checks if the coloring schema should be used for the {@link Element}.
   * 
   * @param element
   *          {@link Element} for which coloring schema is checked
   * @param schema
   *          coloring schema
   * @return <code>true</code> if coloring schema should be used for
   *         {@link Element}, <code>false</code> otherwise
   */
  protected boolean match(Element element, ColorSchema schema) {
    if (element instanceof Species) {
      if (!modelMatch(element.getModelData(), schema)) {
        return false;
      }
      if (schema.getName() != null) {
        if (!element.getName().equalsIgnoreCase(schema.getName())) {
          return false;
        }
      }
      if (schema.getTypes().size() > 0) {
        boolean found = false;
        for (Class<?> clazz : schema.getTypes()) {
          if (clazz.isAssignableFrom(element.getClass())) {
            found = true;
          }
        }

        if (!found) {
          return false;
        }
      }
      for (MiriamData md : schema.getMiriamData()) {
        if (!element.getMiriamData().contains(md)) {
          return false;
        }
      }

      if (schema.getCompartments().size() > 0) {
        boolean found = false;
        for (Compartment compartment : element.getModelData().getModel().getCompartments()) {
          for (String compartmentName : schema.getCompartments()) {
            if (compartment.getName().equalsIgnoreCase(compartmentName)) {
              if (compartment.cross(element)) {
                found = true;
              }
            }
          }
        }
        if (!found) {
          return false;
        }
      }

      return true;
    } else {
      return false;
    }
  }

  /**
   * Checks which coloring schemas weren't used in the model coloring process.
   * 
   * @return list of schemas that weren't used during coloring
   */
  public Collection<ColorSchema> getMissingSchema() {
    List<ColorSchema> result = new ArrayList<ColorSchema>();
    for (ColorSchema schema : schemas) {
      boolean found = false;

      for (BioEntity element : getModel().getBioEntities()) {
        if (match(element, schema)) {
          found = true;
        }
      }
      if (!found) {
        result.add(schema);
      }
    }
    return result;
  }

  /**
   * Returns list of elements ({@link Reaction reactions} and {@link Element
   * elements}) that are modified by the coloring command.
   * 
   * @return {@link Map}, where key corresponds to modified {@link Reaction} or
   *         {@link Element} and value is a {@link ColorSchema} that should be
   *         used for coloring
   * @throws InvalidColorSchemaException
   *           thrown when more than one {@link ColorSchema} match an element
   */
  public Map<Object, ColorSchema> getModifiedElements() throws InvalidColorSchemaException {
    Map<Object, ColorSchema> result = new HashMap<>();

    List<Model> models = new ArrayList<>();
    models.add(getModel());
    models.addAll(getModel().getSubmodels());

    for (Model model : models) {
      for (BioEntity element : model.getBioEntities()) {
        for (ColorSchema schema : schemas) {
          if (match(element, schema)) {
            if (result.get(element) != null
                && !colorExtractor.getNormalizedColor(result.get(element)).equals(Color.WHITE)) {
              throw new InvalidColorSchemaException(
                  eu.getElementTag(element) + "BioEntity is colored by more than one rule.");
            }
            result.put(element, schema);
          }
        }
      }
    }
    return result;
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();

  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();
  }

  /**
   * Colors parameter model using coloring for this command. This method is used
   * internally to color either top {@link Model} or one of it's sumbodels.
   * 
   * @param result
   *          model to color
   * @param top
   *          is the model a top (parent) model
   * @throws InvalidColorSchemaException
   *           thrown when set of {@link ColorSchema} is invalid
   */
  private void colorModel(Model result, boolean top) throws InvalidColorSchemaException {

    for (Element element : result.getElements()) {
      element.setColor(Color.WHITE);
    }
    for (Reaction reaction : result.getReactions()) {
      for (AbstractNode node : reaction.getNodes()) {
        node.getLine().setColor(Color.BLACK);
      }
    }

    for (ColorSchema schema : schemas) {
      for (BioEntity element : result.getBioEntities()) {
        if (match(element, schema)) {
          schema.setMatches(schema.getMatches() + 1);
          applyColor(element, schema);
        }
      }
    }

    if (top) {
      for (ModelSubmodelConnection connection : result.getSubmodelConnections()) {
        colorModel(connection.getSubmodel().getModel(), false);
      }
    }

  }

  private void applyColor(BioEntity element, ColorSchema schema) throws InvalidColorSchemaException {
    if (element instanceof Element) {
      applyColor((Element) element, schema);
    } else if (element instanceof Reaction) {
      applyColor((Reaction) element, schema);
    } else {
      throw new InvalidArgumentException("Don't know how to handle: " + element);
    }
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    try {
      colorModel(getModel(), true);
    } catch (InvalidColorSchemaException e) {
      throw new CommandExecutionException(e);
    }
  }

}
