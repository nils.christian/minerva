package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

public class ReconAnnotatorTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(ReconAnnotatorTest.class);
  @Autowired
  ReconAnnotator reconAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotateReaction() throws Exception {
    try {
      Reaction reaction = new Reaction();
      reaction.setAbbreviation("P5CRm");
      reconAnnotator.annotateElement(reaction);
      assertTrue("No new annotations from recon db imported", reaction.getMiriamData().size() > 0);
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotating_O16G2e_Reaction() throws Exception {
    try {
      Reaction reaction = new Reaction();
      reaction.setName("O16G2e");
      reconAnnotator.annotateElement(reaction);
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSpeciesAnnotationWithInvalidResponse() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      Ion ion = new Ion("id");
      ion.setName("O16G2e");

      // exclude first cached value
      reconAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      reconAnnotator.setWebPageDownloader(mockDownloader);

      reconAnnotator.annotateElement(ion);
      assertEquals(1, getWarnings().size());
      assertTrue(getWarnings().get(0).getMessage().toString().contains("No recon annotations"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testSpeciesAnnotationWithReactionResponse() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      Ion ion = new Ion("id");
      ion.setName("O16G2e");

      // exclude first cached value
      reconAnnotator.setCache(null);

      String response = super.readFile("testFiles/annotation/recon_reaction_response.json");

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn(response);
      reconAnnotator.setWebPageDownloader(mockDownloader);

      reconAnnotator.annotateElement(ion);
      assertEquals(2, getWarnings().size());
      assertTrue(getWarnings().get(0).getMessage().toString().contains("Unknown field"));
      assertTrue(getWarnings().get(1).getMessage().toString().contains("Unknown field"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testReactionAnnotationWithChemicalResponse() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      Reaction reaction = new Reaction();
      reaction.setName("nad[m]");

      // exclude first cached value
      reconAnnotator.setCache(null);

      String response = super.readFile("testFiles/annotation/recon_chemical_response.json");

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn(response);
      reconAnnotator.setWebPageDownloader(mockDownloader);

      reconAnnotator.annotateElement(reaction);
      assertEquals(2, getWarnings().size());
      assertTrue(getWarnings().get(0).getMessage().toString().contains("Unknown field"));
      assertTrue(getWarnings().get(1).getMessage().toString().contains("Unknown field"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testAnnotateUnknownElement() throws Exception {
    try {
      BioEntity obj = Mockito.mock(BioEntity.class);
      when(obj.getName()).thenReturn("O16G2e");

      ReconAnnotator tweakedReconAnnotator = Mockito.spy(reconAnnotator);
      doReturn(true).when(tweakedReconAnnotator).isAnnotatable(any(BioEntity.class));

      tweakedReconAnnotator.annotateElement(obj);
      assertEquals(1, getWarnings().size());
      assertTrue(getWarnings().get(0).getMessage().toString().contains("Unknown class type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotatingWithParentehesis() throws Exception {
    try {
      Reaction reaction = new Reaction();
      reaction.setName("gm2_hs[g]");
      reconAnnotator.annotateElement(reaction);
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotatingWithWhitespace() throws Exception {
    try {
      Reaction reaction = new Reaction();
      reaction.setName("NDPK4m ");
      reconAnnotator.annotateElement(reaction);
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotateElement() throws Exception {
    try {
      SimpleMolecule smallMolecule = new SimpleMolecule("id");
      smallMolecule.setAbbreviation("h2o");
      reconAnnotator.annotateElement(smallMolecule);
      assertTrue("No new annotations from recon db imported", smallMolecule.getMiriamData().size() > 0);
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotateElement2() throws Exception {
    try {
      Species smallMolecule = new SimpleMolecule("id");
      smallMolecule.setAbbreviation("P5CRm");
      reconAnnotator.annotateElement(smallMolecule);
      assertEquals(0, smallMolecule.getMiriamData().size());
      assertFalse(getWarnings().size() == 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      reconAnnotator.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      reconAnnotator.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      String res = reconAnnotator.refreshCacheQuery("http://google.pl/");
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, reconAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      reconAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, reconAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("{\"results\":[{}]}");
      reconAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, reconAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus2() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("{\"results\":[{\"keggId\":\"C00001\",\"xxx\":\"yyy\"}]}");
      reconAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, reconAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = reconAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = reconAnnotator.getCache();
    try {
      // exclude first cached value
      reconAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      reconAnnotator.setWebPageDownloader(mockDownloader);
      reconAnnotator.refreshCacheQuery("http://google.pl/");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      reconAnnotator.setWebPageDownloader(downloader);
      reconAnnotator.setCache(originalCache);
    }
  }

}
