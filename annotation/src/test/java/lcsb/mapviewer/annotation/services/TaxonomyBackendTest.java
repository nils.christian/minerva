package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class TaxonomyBackendTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(TaxonomyBackendTest.class);

  @Autowired
  TaxonomyBackend taxonomyBackend;

  GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    cache = taxonomyBackend.getCache();
    // exclude two first cached values
    taxonomyBackend.setCache(new GeneralCacheWithExclusion(cache, 2));
  }

  @After
  public void tearDown() throws Exception {
    taxonomyBackend.setCache(cache);
  }

  @Test
  public void testByName() throws Exception {
    try {
      MiriamData md = taxonomyBackend.getByName("Human");
      assertTrue(md.equals(TaxonomyBackend.HUMAN_TAXONOMY));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByComplexName() throws Exception {
    try {
      assertNotNull(taxonomyBackend.getByName("Aplysia californica (Sea Hare)"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByCommonName() throws Exception {
    try {
      assertNotNull(taxonomyBackend.getByName("Rat"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByEmptyName() throws Exception {
    try {
      assertNull(taxonomyBackend.getByName(null));
      assertNull(taxonomyBackend.getByName(""));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByAbreviationName() throws Exception {
    try {
      taxonomyBackend.setCache(new GeneralCacheWithExclusion(cache, 1));
      assertNotNull(taxonomyBackend.getByName("C. elegans"));
      taxonomyBackend.setCache(new GeneralCacheWithExclusion(cache, 1));
      assertNotNull(taxonomyBackend.getByName("D. sechellia"));
      taxonomyBackend.setCache(new GeneralCacheWithExclusion(cache, 1));
      assertNotNull(taxonomyBackend.getByName("P. pacificus"));
      taxonomyBackend.setCache(new GeneralCacheWithExclusion(cache, 1));
      assertNotNull(taxonomyBackend.getByName("T. castaneum"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetName() throws Exception {
    try {
      String name = taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9606"));
      assertTrue(name.contains("Homo sapiens"));
      assertNotNull(taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9605")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

	@Test
	public void testGetNameWhenProblemWithDb() throws Exception {
		WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			taxonomyBackend.setWebPageDownloader(mockDownloader);
			taxonomyBackend.setCache(null);
			taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9606"));
			fail("Exception expected");
		} catch (TaxonomySearchException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			taxonomyBackend.setWebPageDownloader(downloader);
		}
	}

  @Test
  public void testGetNameForInvalidTax() throws Exception {
    try {
      String name = taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "-a-"));
      assertNull(name);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetNameForInvalidInputData() throws Exception {
    try {
      String name = taxonomyBackend.getNameForTaxonomy(null);
      assertNull(name);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByName2() throws Exception {
    try {
      MiriamData md = taxonomyBackend.getByName("homo sapiens");
      assertTrue(md.equals(TaxonomyBackend.HUMAN_TAXONOMY));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByName3() throws Exception {
    try {
      MiriamData md = taxonomyBackend.getByName("bla bla kiwi");
      assertNull(md);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByMusMusculusName() throws Exception {
    try {
      MiriamData md = taxonomyBackend.getByName("Mus musculus");
      assertTrue(md.equals(new MiriamData(MiriamType.TAXONOMY, "10090")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByMouseName() throws Exception {
    try {
      MiriamData md = taxonomyBackend.getByName("Mouse");
      assertTrue(md.equals(new MiriamData(MiriamType.TAXONOMY, "10090")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      String result = taxonomyBackend.refreshCacheQuery(TaxonomyBackend.TAXONOMY_CACHE_PREFIX + "homo sapiens");
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery2() throws Exception {
    try {
      String result = taxonomyBackend
          .refreshCacheQuery(TaxonomyBackend.TAXONOMY_NAME_CACHE_PREFIX + TaxonomyBackend.HUMAN_TAXONOMY.getResource());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery3() throws Exception {
    try {
      String result = taxonomyBackend.refreshCacheQuery("http://google.pl/");
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      taxonomyBackend.refreshCacheQuery("unk_query");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      taxonomyBackend.refreshCacheQuery(new Object());
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

	@Test
	public void testRefreshCacheQueryNotAvailable() throws Exception {
		WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			taxonomyBackend.setWebPageDownloader(mockDownloader);
			taxonomyBackend.refreshCacheQuery("http://google.pl/");
			fail("Exception expected");
		} catch (SourceNotAvailable e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			taxonomyBackend.setWebPageDownloader(downloader);
		}
	}

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, taxonomyBackend.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			taxonomyBackend.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, taxonomyBackend.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			taxonomyBackend.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateChangedStatus() throws Exception {
		WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
			taxonomyBackend.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, taxonomyBackend.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			taxonomyBackend.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateChangedStatus2() throws Exception {
		WebPageDownloader downloader = taxonomyBackend.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("<em>Taxonomy ID: </em>1234");
			taxonomyBackend.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, taxonomyBackend.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			taxonomyBackend.setWebPageDownloader(downloader);
		}
	}

}
