package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class UniprotAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  UniprotAnnotator uniprotAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotate1() throws Exception {
    try {

      Species protein = new GenericProtein("id");
      protein.setName("P12345");
      uniprotAnnotator.annotateElement(protein);

      assertTrue(protein.getMiriamData().size() > 0);

      boolean entrez = false;
      boolean hgnc = false;
      boolean uniprot = false;
      boolean ec = false;

      for (MiriamData md : protein.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.UNIPROT)) {
          uniprot = true;
        } else if (md.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
          hgnc = true;
        } else if (md.getDataType().equals(MiriamType.ENTREZ)) {
          entrez = true;
        } else if (md.getDataType().equals(MiriamType.EC)) {
          ec = true;
        }
      }
      assertTrue("No HGNC annotation extracted from uniprot annotator", hgnc);
      assertTrue("No ENTREZ annotation extracted from uniprot annotator", entrez);
      assertTrue("No UNIPROT annotation extracted from uniprot annotator", uniprot);
      assertTrue("No UNIPROT annotation extracted from uniprot annotator", ec);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testEC1() throws Exception {
    try {

      Collection<MiriamData> mds = uniprotAnnotator.uniProtToEC(new MiriamData(MiriamType.UNIPROT, "P12345"));

      assertEquals(mds.size(), 2);
      MiriamData md1 = new MiriamData(MiriamType.EC, "2.6.1.1", UniprotAnnotator.class);
      MiriamData md2 = new MiriamData(MiriamType.EC, "2.6.1.7", UniprotAnnotator.class);
      for (MiriamData md : mds) {
        assertTrue(md.compareTo(md1) == 0 || md.compareTo(md2) == 0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testEC2() throws Exception {
    try {

      Collection<MiriamData> mds = uniprotAnnotator.uniProtToEC(new MiriamData(MiriamType.UNIPROT, "P25405"));

      assertTrue("No EC miriam data extracted from uniprot annotator", mds.size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testInvalidUniprotToECNull() throws Exception {
    try {
      assertNull(uniprotAnnotator.uniProtToEC(null));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidUniprotToECWrongMd() throws Exception {
    try {
      uniprotAnnotator.uniProtToEC(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotateInvalid2() throws Exception {
    try {
      Species protein = new GenericProtein("id");
      uniprotAnnotator.annotateElement(protein);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testInvalidUniprotToHgnc() throws Exception {
    try {
      assertNull(uniprotAnnotator.uniProtToHgnc(null));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidUniprotToHgnc2() throws Exception {
    try {
      uniprotAnnotator.uniProtToHgnc(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotate2() throws Exception {
    try {
      Species protein = new GenericProtein("id");
      protein.setName("bla");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P01308"));
      uniprotAnnotator.annotateElement(protein);

      assertTrue(protein.getMiriamData().size() > 1);

      boolean entrez = false;
      boolean hgnc = false;
      boolean uniprot = false;

      for (MiriamData md : protein.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.UNIPROT)) {
          uniprot = true;
        } else if (md.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
          hgnc = true;
        } else if (md.getDataType().equals(MiriamType.ENTREZ)) {
          entrez = true;
        }
      }
      assertTrue("No HGNC annotation extracted from uniprot annotator", hgnc);
      assertTrue("No ENTREZ annotation extracted from uniprot annotator", entrez);
      assertTrue("No UNIPROT annotation extracted from uniprot annotator", uniprot);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    try {
      Species protein = new GenericProtein("id");
      protein.setName("bla");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
      uniprotAnnotator.annotateElement(protein);

      assertEquals(1, protein.getMiriamData().size());

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testAnnotateInvalid() throws Exception {
    try {
      Species protein = new GenericProtein("id");
      protein.setName("bla");
      uniprotAnnotator.annotateElement(protein);

      assertEquals(0, protein.getMiriamData().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testUniprotToHgnc() throws Exception {
    try {
      assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "KDR", UniprotAnnotator.class),
          uniprotAnnotator.uniProtToHgnc(new MiriamData(MiriamType.UNIPROT, "P35968")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      uniprotAnnotator.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      uniprotAnnotator.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      Object res = uniprotAnnotator.refreshCacheQuery("http://google.pl/");
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, uniprotAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, uniprotAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testAnnotateWithUniprotServerError() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    GeneralCacheInterface cache = uniprotAnnotator.getCache();
    uniprotAnnotator.setCache(new GeneralCacheWithExclusion(cache, 1));
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      Species protein = new GenericProtein("id");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P01308"));
      uniprotAnnotator.annotateElement(protein);
      fail("Exception expected");
    } catch (AnnotatorException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      uniprotAnnotator.setCache(cache);
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateDownStatus2() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, uniprotAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
      uniprotAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, uniprotAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      uniprotAnnotator.setWebPageDownloader(downloader);
    }
  }

}
