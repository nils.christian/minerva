package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class TairAnnotatorTest extends AnnotationTestFunctions {
	
	@Autowired
	TairAnnotator tairAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnnotate1() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT1G01030"));
			
			tairAnnotator.annotateElement(protein);

			MiriamData mdUniprot = null;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.UNIPROT)) {
					mdUniprot = md;
				}
			}
			
			assertTrue("No UNIPROT annotation extracted from TAIR annotator", mdUniprot != null);
			assertTrue("Invalid UNIPROT annotation extracted from TAIR annotator", mdUniprot.getResource().equalsIgnoreCase("Q9MAN1") );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateExistingUniprot() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT1G15950"));
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P32246")); // Human version of the protein
			
			tairAnnotator.annotateElement(protein);

			int cntUniProts = 0;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.UNIPROT)) {
					cntUniProts++;
				}
			}
			
			assertTrue("No UNIPROT annotation extracted from TAIR annotator", cntUniProts > 1);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	@Test
	public void testAnnotateInvalidTair() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
			tairAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testInvalidTairToUniprot() throws Exception {
		try {
			assertNull(tairAnnotator.tairToUniprot(null));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidTairToUniprot2() throws Exception {
		try {
			tairAnnotator.tairToUniprot(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	@Test
	public void testAnnotateInvalid() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			tairAnnotator.annotateElement(protein);

			assertEquals(0, protein.getMiriamData().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testTairToUniprot() throws Exception {
		try {
			assertEquals(new MiriamData(MiriamType.UNIPROT, "Q9MAN1", TairAnnotator.class), 
					tairAnnotator.tairToUniprot(new MiriamData(MiriamType.TAIR_LOCUS, "AT1G01030")));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			tairAnnotator.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			tairAnnotator.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshCacheQuery() throws Exception {
		try {
			Object res = tairAnnotator.refreshCacheQuery("http://google.cz/");
			assertNotNull(res);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, tairAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = tairAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			tairAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, tairAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			tairAnnotator.setWebPageDownloader(downloader);
		}
	}


//	@Test
//	public void testAnnotateWithUniprotServerError() throws Exception {
//		WebPageDownloader downloader = uniprotAnnotator.getWebPageDownloader();
//		GeneralCacheInterface cache = uniprotAnnotator.getCache();
//		uniprotAnnotator.setCache(new GeneralCacheWithExclusion(cache, 1));
//		try {
//			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
//			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
//			uniprotAnnotator.setWebPageDownloader(mockDownloader);
//			Species protein = new GenericProtein("id");
//			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P01308"));
//			uniprotAnnotator.annotateElement(protein);
//			fail("Exception expected");
//		} catch (AnnotatorException e) {
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		} finally {
//			uniprotAnnotator.setCache(cache);
//			uniprotAnnotator.setWebPageDownloader(downloader);
//		}
//	}

	@Test
	public void testSimulateChangedStatus() throws Exception {
		WebPageDownloader downloader = tairAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
			tairAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, tairAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			tairAnnotator.setWebPageDownloader(downloader);
		}
	}

}
