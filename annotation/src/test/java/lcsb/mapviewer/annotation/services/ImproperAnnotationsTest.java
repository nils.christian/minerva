package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ImproperAnnotationsTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor3() {
		try {
			List<MiriamData> list = new ArrayList<>();
			list.add(new MiriamData(MiriamType.CAS, "a"));
			ImproperAnnotations ie = new ImproperAnnotations(new GenericProtein("id"), list);
			assertNotNull(ie.toString());
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Only miriam data from elements are accepted"));
		}
	}

	@Test
	public void testConstructor4() {
		try {
			new ImproperAnnotations(new GenericProtein("id"), new ArrayList<>());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("List of improper annotations cannot be null"));
		}
	}

}
