package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.hibernate.AnnotationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import uk.ac.ebi.miriam.lib.MiriamLink;

public class MiriamConnectorTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(MiriamConnectorTest.class);

  @Autowired
  MiriamConnector miriamConnector;

  GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    cache = miriamConnector.getCache();
  }

  @After
  public void tearDown() throws Exception {
    miriamConnector.setCache(cache);
  }

  @Test
  public void testGoUri() throws Exception {
    try {
      assertFalse(MiriamType.GO.getUris().size() == 1);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testMeshUrl() throws Exception {
    try {
      // exclude first cached value
      miriamConnector.setCache(new GeneralCacheWithExclusion(cache, 1));
      MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.MESH_2012, "D004249");

      String pageContent = getWebpage(miriamConnector.getUrlString(md));
      assertTrue(pageContent.contains("DNA Damage"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrl() throws Exception {
    try {
      // exclude first cached value
      miriamConnector.setCache(new GeneralCacheWithExclusion(cache, 1));
      MiriamData md = TaxonomyBackend.HUMAN_TAXONOMY;

      String url = miriamConnector.getUrlString(md);
      assertNotNull(url);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrlForInvalidMiriam() throws Exception {
    try {
      miriamConnector.getUrlString(new MiriamData());
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Data type cannot be empty"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrlForInvalidMiriam2() throws Exception {
    try {
      miriamConnector.getUrlString(new MiriamData(MiriamType.UNKNOWN, ""));
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("cannot be retreived"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrlForInvalidMiriam3() throws Exception {
    try {
      String url = miriamConnector.getUrlString(new MiriamData(MiriamType.ENTREZ, "abc"));
      assertNull(url);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsValidIdentifier() throws Exception {
    try {
      assertTrue(miriamConnector.isValidIdentifier(MiriamType.ENTREZ.getCommonName() + ":" + "1234"));
      assertFalse(miriamConnector.isValidIdentifier("blablabla"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testResolveUrl() throws Exception {
    try {
      // exclude first cached value
      miriamConnector.setCache(new GeneralCacheWithExclusion(cache, 1));
      for (MiriamType mt : MiriamType.values()) {
        if (!MiriamType.UNKNOWN.equals(mt)) {
          assertTrue("Invalid URI for MiriamType: " + mt, miriamConnector.isValid(mt.getUris().get(0)));
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefresh() throws Exception {
    String query = MiriamConnector.VALID_URI_PREFIX + MiriamType.HGNC.getUris().get(0);
    try {
      String res = miriamConnector.refreshCacheQuery(query);
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefresh2() throws Exception {
    MiriamData md = TaxonomyBackend.HUMAN_TAXONOMY;
    String query = MiriamConnector.LINK_DB_PREFIX + md.getDataType().getUris().get(0) + "\n" + md.getResource();
    try {
      String res = miriamConnector.refreshCacheQuery(query);
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefresh3() throws Exception {
    try {
      String res = miriamConnector.refreshCacheQuery("https://www.google.pl/");
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrl2() throws Exception {
    try {
      // exclude first cached value
      miriamConnector.setCache(new GeneralCacheWithExclusion(cache, 1));
      MiriamData md = TaxonomyBackend.HUMAN_TAXONOMY;

      String url = miriamConnector.getUrlString2(md);
      assertNotNull(url);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrlForDoi() throws Exception {
    try {
      // exclude first cached value
      MiriamData md = new MiriamData(MiriamType.DOI, "10.1038/npjsba.2016.20");

      String url = miriamConnector.getUrlString(md);
      assertNotNull(url);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testMiriamDataToUri() throws Exception {
    try {
      assertNotNull(miriamConnector.miriamDataToUri(TaxonomyBackend.HUMAN_TAXONOMY));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUrl2WithInvalidApiResponse() throws Exception {
    WebPageDownloader downloader = miriamConnector.getWebPageDownloader();
    GeneralCacheInterface cache = miriamConnector.getCache();
    try {
      // exclude first cached value
      miriamConnector.setCache(new GeneralCacheWithExclusion(cache, 1));
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      miriamConnector.setWebPageDownloader(mockDownloader);

      miriamConnector.getUrlString2(TaxonomyBackend.HUMAN_TAXONOMY);

    } catch (AnnotationException e) {
      assertTrue(e.getMessage().contains("Problem with accessing miriam REST API"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miriamConnector.setWebPageDownloader(downloader);
      miriamConnector.setCache(cache);
    }
  }

  @Test
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = miriamConnector.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      miriamConnector.setWebPageDownloader(mockDownloader);
      miriamConnector.refreshCacheQuery("http://google.pl/");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miriamConnector.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      miriamConnector.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      miriamConnector.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery3() throws Exception {
    try {
      miriamConnector.refreshCacheQuery(MiriamConnector.LINK_DB_PREFIX);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Miriam link query is invalid"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCheckValidyOfEmptyUri() throws Exception {
    try {
      // user connector without cache
      boolean value = new MiriamConnector().isValid("");
      assertFalse(value);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, miriamConnector.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    MiriamLink link = miriamConnector.getLink();
    try {
      MiriamLink mockDownloader = Mockito.mock(MiriamLink.class);
      when(mockDownloader.getLocations(any())).thenThrow(new InvalidStateException());
      miriamConnector.setLink(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, miriamConnector.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miriamConnector.setLink(link);
    }
  }

  @Test
  public void testSimulateDownStatus2() throws Exception {
    MiriamLink link = miriamConnector.getLink();
    try {
      MiriamLink mockDownloader = Mockito.mock(MiriamLink.class);
      when(mockDownloader.getLocations(any())).thenReturn(null);
      miriamConnector.setLink(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, miriamConnector.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miriamConnector.setLink(link);
    }
  }

}
