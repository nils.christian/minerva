package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class BiocompendiumAnnotatorTest extends AnnotationTestFunctions {
	Logger												 logger		= Logger.getLogger(BiocompendiumAnnotatorTest.class);

	MiriamData										 camk4		= new MiriamData(MiriamType.HGNC_SYMBOL, "CAMK4");
	MiriamData										 slc25a27	= new MiriamData(MiriamType.HGNC_SYMBOL, "SLC25A27");
	MiriamData										 nsmf			= new MiriamData(MiriamType.HGNC_SYMBOL, "NSMF");
	MiriamData										 mir449a	= new MiriamData(MiriamType.HGNC_SYMBOL, "MIR449A");

	@Autowired
	private GeneralCacheInterface	 cache;

	@Autowired
	private BiocompendiumAnnotator restService;

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore("Bug 32")
	public void testGetAnnotationsForSpecies() throws Exception {
		try {
			String response = restService.getAnnotation(camk4);
			assertNotNull(response);
			assertTrue(response.contains("Symbol: CAMK4"));

			response = restService.getAnnotation(slc25a27);
			assertNotNull(response);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() throws Exception {
		try {
			assertNotNull(restService.getCommonName());
			assertNotNull(restService.getUrl());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsForInvalidMiriam() throws Exception {
		try {
			restService.getAnnotation(new MiriamData());
			fail("Exception expected");

		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseXml() throws Exception {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader("testFiles/annotation/sampleResponse.xml"));
			String line = null;
			StringBuilder stringBuilder = new StringBuilder();
			String ls = System.getProperty("line.separator");

			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
			reader.close();

			Map<String, String> res = restService.getAnnotationsFromXml(stringBuilder.toString());

			assertEquals(2, res.keySet().size());
			String response = res.get("CAMK4");
			assertNotNull(response);
			assertTrue(response.contains("Symbol: CAMK4"));

			assertNotNull(res.get("SLC25A27"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 10000)
	@Ignore("Bug 32")
	public void testGetAnnotationsForMiceSpecies() throws Exception {
		ArrayList<String> names = new ArrayList<String>();

		names.add("Fmo3");
		names.add("Nqo1");
		names.add("Abcc12");
		names.add("Mgst3");
		names.add("Txnrd1");
		names.add("Cbr3");
		names.add("Hspa1b");
		names.add("Prdx1");
		names.add("Ppard");
		names.add("Tgfb2");
		names.add("Fth1");
		names.add("Prdx6");
		names.add("Nr4a1");
		names.add("Tgfb1");
		names.add("Abcc4");
		names.add("Ager");
		names.add("Gsr");
		names.add("Sod3");
		names.add("Maff");
		names.add("Eif2ak3");
		names.add("Tgfa");
		names.add("Hbegf");
		names.add("Mafg");
		names.add("Adh7");
		names.add("Slc7a11");
		names.add("Epha3");
		names.add("Blvrb");
		// problematic one
		// names.add("Me1");
		names.add("Csnk2a2");
		names.add("Gpx3");
		names.add("Mapk8");
		names.add("Gclm");
		names.add("Epha2");
		names.add("Bdnf");
		// problematic one
		// names.add("ACC2");
		names.add("Ptgr1");
		names.add("Pdgfb");
		names.add("Mapk7");
		names.add("Cbr1");
		names.add("Hsp90aa1");
		names.add("Pgd");
		names.add("Sqstm1");
		names.add("Aldh9a1");
		names.add("Txn");
		names.add("Txnrd3");
		names.add("Srxn1");
		names.add("Gpx2");
		names.add("Npas4");
		names.add("Mapk1");
		names.add("Nrg1");
		names.add("Cbr");
		names.add("Hspa1a");
		names.add("Mgst2");
		names.add("Tgfbr2");
		names.add("Ephx1");
		names.add("Dnajb1");
		names.add("Abcc2");
		names.add("Gclc");
		names.add("Abcc5");
		names.add("Ggt1");
		names.add("Ftl");
		names.add("Egr1");
		names.add("Fgf13");
		// problematic one
		// names.add("Hgf");
		// problematic one
		// names.add("UbcH7");
		names.add("Abcc3");
		names.add("Nfe2l2");
		// problematic one
		// names.add("Hsp70");
		names.add("Hsp90ab1");

		try {
			for (int i = 0; i < names.size(); i++) {
				String geneName = names.get(i);
				String annotation = restService.getAnnotation(new MiriamData(MiriamType.HGNC_SYMBOL, geneName));
				assertNotNull("Problem with annotation of mouse gene: " + geneName, annotation);
				assertTrue("Problem with annotation of mouse gene: " + geneName, !annotation.trim().equals(""));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	@Ignore("Bug 451")
	public void testGetAnnotationsForNSMF() throws Exception {
		try {
			String response = restService.getAnnotation(nsmf);
			assertNotNull(response);
			assertFalse(response.trim().equals(""));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	@Ignore("Bug 451")
	public void testGetAnnotationsForMIR449A() throws Exception {
		try {
			String response = restService.getAnnotation(mir449a);
			assertNotNull(response);
			assertFalse(response.trim().equals(""));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testCachableInterfaceInvalidate() throws Exception {
		String query = "GLUD1";
		try {
			String newRes = "hello";

			waitForRefreshCacheQueueToEmpty();

			cache.setCachedQuery(query, restService.getCacheType(), newRes);
			String res = cache.getStringByQuery(query, restService.getCacheType());
			assertEquals(newRes, res);
			cache.invalidateByQuery(query, restService.getCacheType());

			waitForRefreshCacheQueueToEmpty();

			res = cache.getStringByQuery(query, restService.getCacheType());

			assertNotNull(res);

			assertFalse("Value wasn't refreshed from db", newRes.equals(res));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNameWithNewLine() throws Exception {
		try {
			String response = restService.getAnnotation(new MiriamData(MiriamType.HGNC_SYMBOL, "some\nname"));
			assertNotNull(response);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidValue() throws Exception {
		try {
			restService.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do "));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshValueWhenProblemWithSource() throws Exception {
		try {
			BiocompendiumAnnotator annotatorUnderTest = new BiocompendiumAnnotator() {
				@Override
				Map<String, String> getAnnotationsFromXml(String xml) throws InvalidXmlSchemaException {
					throw new InvalidXmlSchemaException();
				}
			};
			annotatorUnderTest.refreshCacheQuery(camk4.getResource());
			fail("Exception expected");
		} catch (SourceNotAvailable e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetInvalidAnnotations() throws Exception {
		try {
			restService.getAnnotation(new MiriamData(MiriamType.WIKIPEDIA, "world"));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testInvalidDataToStrubg() throws Exception {
		try {
			restService.dataToString(new MiriamData(MiriamType.WIKIPEDIA, "world"));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsFromInvaldXml() throws Exception {
		try {
			Map<String, String> map = restService.getAnnotationsFromXml("");
			assertEquals(0, map.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsFromInvaldXml2() throws Exception {
		try {
			Map<String, String> map = restService.getAnnotationsFromXml(null);
			assertEquals(0, map.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
