package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class AnnotatorExceptionTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		AnnotatorParamDefinition d = new AnnotatorParamDefinition("name", String.class, "description");
		assertEquals(d.getName(), "name");
		assertEquals(d.getDescription(), "description");
		assertEquals(d.getType(), String.class);
	}

}
