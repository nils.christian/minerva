package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;

public class PdbAnnotatorTest extends AnnotationTestFunctions {

	@Autowired
	PdbAnnotator pdbAnnotator;
	
	@Autowired
	UniprotAnnotator uniprotAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnnotate1() throws Exception {
		try {
			String uniprotId = "P29373"; 
			Species protein = new GenericProtein("id");
			protein.setName(uniprotId);
			/* One needs to have the UniProt ID first.
			 * This tests simulates situation when the Uniprot annotator
			 * is called first.
			 */
			uniprotAnnotator.annotateElement(protein); 
			int cntAnnotations1 = protein.getMiriamData().size();
			pdbAnnotator.annotateElement(protein);
			int cntAnnotations2 = protein.getMiriamData().size();

			assertTrue(cntAnnotations2 > cntAnnotations1);

			boolean pdb = false;			

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.PDB)) {
					pdb = true;
					break;
				}
			}
			assertTrue("No PDB annotation extracted from pdb annotator", pdb);
						
			Set<UniprotRecord> urs = protein.getUniprots();
			//Test whether there is a uniprot record
			assertTrue(urs.size() > 0);
			UniprotRecord ur = null;
			for (UniprotRecord ur1 : urs) {
				if (ur1.getUniprotId() == uniprotId) {
					ur = ur1;
				}
			}
			//Test whether there is a uniprot record with the uniprot ID which was stored
			assertNotNull(ur);
			//Test whether structures are there
			Set<Structure> ss = ur.getStructures(); 
			assertNotNull(ss);
			assertTrue(ss.size() > 0);
			//Test whether uniprot is accessible from structure
			assertTrue(ss.iterator().next().getUniprot() == ur);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
//	@Test
//	public void testAnnotate2() throws Exception {
//		try {
//			Species protein = new GenericProtein("id");
//			protein.setName("P29373");
//			/* One needs to have the UniProt ID first, and Uniprot lookup
//			 * through HGNC is included in PDB annotator.
//			 * This tests simulates situation when the PDB annotator is called first.
//			 */
//			pdbAnnotator.annotateElement(protein);
//			int cntAnnotations = protein.getMiriamData().size();
//
//			assertTrue(cntAnnotations > 0);
//
//			boolean pdb = false;			
//
//			for (MiriamData md : protein.getMiriamData()) {
//				if (md.getDataType().equals(MiriamType.PDB)) {
//					pdb = true;
//					break;
//				}
//			}
//			assertTrue("No PDB annotation extracted from pdb annotator", pdb);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		}
//	}
	
	@Test
	public void testAnnotate3() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("SNCA");
			/* One needs to have the UniProt ID first, but Uniprot lookup
			 * is included in the PDB annotator (as well as HGNC lookup).
			 * This tests simulates situation when the PDB annotator is called first
			 * and the name can be resolved by HGNC, but not UniProt.
			 */
			pdbAnnotator.annotateElement(protein);
			int cntAnnotations = protein.getMiriamData().size();

			assertTrue(cntAnnotations > 0);

			boolean pdb = false;			

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.PDB)) {
					pdb = true;
					break;
				}
			}
			assertTrue("No PDB annotation extracted from pdb annotator", pdb);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
//	@Test
//	public void testAnnotateNotUniprotAnnodated() throws Exception {
//		try {
//			Species protein = new GenericProtein("id");
//			protein.setName("P29373");
//			pdbAnnotator.annotateElement(protein);
//			
//			assertTrue("UniProt annotation in PDB annotator failed", protein.getMiriamData().size() > 0);
//
//			boolean pdb = false;			
//
//			for (MiriamData md : protein.getMiriamData()) {
//				if (md.getDataType().equals(MiriamType.PDB)) {
//					pdb = true;
//					break;
//				}
//			}
//			assertTrue("No PDB annotation extracted from PDB annotator", pdb);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		}
//	}	
	
	@Test
	public void testAnnotateInvalidUniprot() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");			
			pdbAnnotator.annotateElement(protein);

			assertEquals(0, protein.getMiriamData().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateValidUniprotNonexistingPdb() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("Q88VP8");			
			pdbAnnotator.annotateElement(protein);
			
			boolean pdb = false;
			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.PDB)) {
					pdb = true;
					break;
				}
			}
			assertTrue("PDB mapping found for structure for which no should be available", !pdb);
			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = pdbAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
			pdbAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, pdbAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pdbAnnotator.setWebPageDownloader(downloader);
		}
	}
	
	@Test
	public void testSimulateChangedStatus() throws Exception {
		WebPageDownloader downloader = pdbAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("{\"P29373\": [{\"xxx\": 140}]}");
			pdbAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, pdbAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pdbAnnotator.setWebPageDownloader(downloader);
		}
	}
	
	@Test
	public void testSimulateInvalidJson() throws Exception {
		WebPageDownloader downloader = pdbAnnotator.getWebPageDownloader();
		try {
			Species protein = new GenericProtein("id");
			protein.setName("Q88VP8");
			uniprotAnnotator.annotateElement(protein);
			int cntAnnotations1 = protein.getMiriamData().size();
			
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("\"P29373\": [{\"xxx\": 140}]}");
			pdbAnnotator.setWebPageDownloader(mockDownloader);
			
			pdbAnnotator.annotateElement(protein);
			int cntAnnotations2 = protein.getMiriamData().size();
			assertTrue(cntAnnotations1 == cntAnnotations2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pdbAnnotator.setWebPageDownloader(downloader);
		}
	}

}
