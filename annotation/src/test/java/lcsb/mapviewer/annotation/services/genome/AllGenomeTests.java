package lcsb.mapviewer.annotation.services.genome;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ UcscReferenceGenomeConnectorTest.class })
public class AllGenomeTests {

}
