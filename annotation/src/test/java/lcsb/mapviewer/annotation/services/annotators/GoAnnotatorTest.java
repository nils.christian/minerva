package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.PermanentDatabaseLevelCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;

public class GoAnnotatorTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(GoAnnotatorTest.class);

  @Autowired
  GoAnnotator goAnnotator;

  @Autowired
  private PermanentDatabaseLevelCacheInterface cache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testContent() throws Exception {
    try {
      Compartment compartmentAlias = new Compartment("id");
      compartmentAlias
          .addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0046902"));
      goAnnotator.annotateElement(compartmentAlias);
      assertFalse(compartmentAlias.getFullName().equals(""));
      assertFalse(compartmentAlias.getNotes().equals(""));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetGoData() throws Exception {
    try {
      MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0042644");
      Go go = goAnnotator.getGoElement(md);
      assertEquals("GO:0042644", go.getGoTerm());
      assertNotNull(go.getCommonName());
      assertNotNull(go.getDescription());
      assertTrue(go.getCommonName().toLowerCase().contains("chloroplast nucleoid"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotateWhenProblemWithNetworkResponse() throws Exception {
    try {
      MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0042644");
      GoAnnotator annotator = new GoAnnotator();
      annotator.setMc(goAnnotator.getMc());
      WebPageDownloader downloader = Mockito.mock(WebPageDownloader.class);
      when(downloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      annotator.setWebPageDownloader(downloader);
      annotator.getGoElement(md);
      fail("Exception expected");
    } catch (GoSearchException e) {
      assertTrue(e.getMessage().contains("Problem with accesing go database"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testCachableInterface() throws Exception {
    String query = GoAnnotator.GO_TERM_CACHE_PREFIX + "GO:0046902";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, goAnnotator.getCacheType(), newRes);
      cache.invalidateByQuery(query, goAnnotator.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, goAnnotator.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = goAnnotator.getCache();
    try {
      // exclude first cached value
      goAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      goAnnotator.setWebPageDownloader(mockDownloader);
      goAnnotator.refreshCacheQuery("http://google.pl/");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
      goAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      goAnnotator.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      goAnnotator.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      String res = goAnnotator.refreshCacheQuery("http://google.pl/");
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, goAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      goAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, goAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("{\"numberOfHits\": 1,\"results\": [{\"name\":\"x\"}]}");
      goAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, goAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader downloader = goAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("{\"numberOfHits\": 1,\"results\": [{}]}");
      goAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, goAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      goAnnotator.setWebPageDownloader(downloader);
    }
  }

}
