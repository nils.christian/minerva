package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.model.map.MiriamType;

public class DrugbankXMLParserTest extends AnnotationTestFunctions {
	Logger									 logger	= Logger.getLogger(DrugbankXMLParserTest.class);

	String									 file		= "testFiles/target_drugbank/small_drugbank.xml";

	@Autowired
	TaxonomyBackend					 taxonomyBackend;

	// this should be improved, but now for performance it's done via static field
	static DrugbankXMLParser read;

	@Before
	public void setup() {
		if (read == null) {
			read = new DrugbankXMLParser(file, taxonomyBackend);
		}
	}

	@Test
	public void test1FindDrug() throws Exception {
		Drug test = read.findDrug("Urokinase");
		assertEquals(MiriamType.DRUGBANK, test.getSources().get(0).getDataType());
		assertEquals("DB00013", test.getSources().get(0).getResource());
		assertEquals("Urokinase", test.getName());
		assertEquals(
				"Low molecular weight form of human urokinase, that consists of an A chain of 2,000 daltons linked by a sulfhydryl bond to a B chain of 30,400 daltons. Recombinant urokinase plasminogen activator",
				test.getDescription());
		assertEquals(10, test.getTargets().size());

		// test.write();
	}

	@Test
	public void test2FindDrug() throws Exception {
		Drug test = read.findDrug("diazoxide");
		assertEquals("Diazoxide", test.getName());
		assertEquals("DB01119", test.getSources().get(0).getResource());
		assertEquals(
				"A benzothiadiazine derivative that is a peripheral vasodilator used for hypertensive emergencies. It lacks diuretic effect, apparently because it lacks a sulfonamide group. [PubChem]",
				test.getDescription());
		assertEquals(6, test.getTargets().size());
	}

	@Test
	public void test3FindDrug() throws Exception {
		// finding synonym
		Drug test = read.findDrug("Rapamycin");
		assertEquals("Sirolimus", test.getName());
		assertEquals("DB00877", test.getSources().get(0).getResource());
		assertEquals(
				"A macrolide compound obtained from Streptomyces hygroscopicus that acts by selectively blocking the transcriptional activation of cytokines thereby inhibiting cytokine production. It is bioactive only when bound to immunophilins. Sirolimus is a potent immunosuppressant and possesses both antifungal and antineoplastic properties. [PubChem]",
				test.getDescription());
		assertEquals(3, test.getTargets().size());
	}

	@Test
	public void test4FindDrug() throws Exception {
		Drug test = read.findDrug("Methylamine");
		assertEquals("Methylamine", test.getName());
		assertEquals("DB01828", test.getSources().get(0).getResource());
		assertEquals(null, test.getDescription());
		assertEquals(1, test.getTargets().size());

		for (Target t : test.getTargets()) {
			assertEquals("Ammonia channel", t.getName());
			assertEquals("BE0001338", t.getSource().getResource());
			assertEquals(MiriamType.DRUGBANK_TARGET_V4, t.getSource().getDataType());
			assertEquals(2, t.getReferences().size());
			assertEquals(0, t.getGenes().size());
		}

		// test.write();
	}

	@Test
	public void test5FindDrug() throws Exception {
		Drug test = read.findDrug("Amantadine");
		assertEquals("Amantadine", test.getName());
		assertEquals("DB00915", test.getSources().get(0).getResource());
		assertEquals(
				"An antiviral that is used in the prophylactic or symptomatic treatment of influenza A. It is also used as an antiparkinsonian agent, to treat extrapyramidal reactions, and for postherpetic neuralgia. The mechanisms of its effects in movement disorders are not well understood but probably reflect an increase in synthesis and release of dopamine, with perhaps some inhibition of dopamine uptake. [PubChem]",
				test.getDescription());
		assertEquals(3, test.getTargets().size());
	}

	@Test
	public void test6FindDrug() throws Exception {

		Drug test = read.findDrug("qwertyuiop");
		assertEquals(null, test);
	}

	@Test
	public void test7FindDrug() throws Exception {
		Drug test = read.findDrug("Aluminum hydroxide");
		assertEquals("Aluminum hydroxide", test.getName());
		assertEquals("DB06723", test.getSources().get(0).getResource());
		assertEquals(
				"Aluminum hydroxide is an inorganic salt used as an antacid. It is a basic compound that acts by neutralizing hydrochloric acid in gastric secretions. Subsequent increases in pH may inhibit the action of pepsin. An increase in bicarbonate ions and prostaglandins may also confer cytoprotective effects.",
				test.getDescription());
		assertEquals(0, test.getTargets().size());
	}

	@Test
	public void testFindDrugInInvalidFile() throws Exception {
		try {
			new DrugbankXMLParser("unknown.file.bla", taxonomyBackend).findDrug("unk");
			fail("Exception expected");

		} catch (DrugSearchException e) {
			assertTrue(e.getMessage().contains("Problem with processing input file"));
		}
	}

	@Test
	public void testFindDrugWithTaxnomyBackendBroken() throws Exception {
		try {
			TaxonomyBackend taxonomyMock = Mockito.mock(TaxonomyBackend.class);
			when(taxonomyMock.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
			new DrugbankXMLParser(file, taxonomyMock).findDrug("Amantadine");
			fail("Exception expected");

		} catch (DrugSearchException e) {
			assertEquals(TaxonomySearchException.class, e.getCause().getClass());
		}
	}
}
