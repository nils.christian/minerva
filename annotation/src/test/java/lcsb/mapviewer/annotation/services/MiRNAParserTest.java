package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;

public class MiRNAParserTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(MiRNAParserTest.class);

  @Autowired
  MiRNAParser miRNAParser;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() throws Exception {
  }

  @Test
  public void testExternalDBStatus() throws Exception {
    ExternalServiceStatus status = miRNAParser.getServiceStatus();
    assertTrue(status.getStatus().equals(ExternalServiceStatusType.OK));
  }

  @Test
  public void testFindMultipleByIDFromDB() throws Exception {
    try {
      List<String> names = new ArrayList<>();
      names.add("gga-miR-489-3p");
      names.add("rno-miR-9a-5p");
      List<MiRNA> listFromDB = miRNAParser.getMiRnasByNames(names);
      assertTrue(!listFromDB.isEmpty());
      assertEquals(2, listFromDB.size());
      assertEquals(1, listFromDB.get(0).getTargets().size());
      assertEquals(5, listFromDB.get(1).getTargets().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindById() throws Exception {
    try {
      List<String> names = new ArrayList<>();
      names.add("mmu-miR-122-5p");
      List<MiRNA> listFromDB = miRNAParser.getMiRnasByNames(names);
      assertEquals(1, listFromDB.size());
      MiRNA mirna = listFromDB.get(0);
      for (Target target : mirna.getTargets()) {
        assertNotNull(target.getName());
        assertEquals(new MiriamData(MiriamType.TAXONOMY, "10090"), target.getOrganism());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend taxonomyBackend = miRNAParser.getTaxonomyBackend();
    GeneralCacheInterface cache = miRNAParser.getCache();
    try {
      List<String> names = new ArrayList<>();
      names.add("mmu-miR-122-5p");
      miRNAParser.setCache(null);
      TaxonomyBackend backend = Mockito.mock(TaxonomyBackend.class);
      when(backend.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
      miRNAParser.setTaxonomyBackend(backend);
      try {
        miRNAParser.getMiRnasByNames(names);
        fail("Exception expected");
      } catch (MiRNASearchException e) {

      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miRNAParser.setCache(cache);
      miRNAParser.setTaxonomyBackend(taxonomyBackend);
    }
  }

  @Test
  public void testFindById2() throws Exception {
    try {
      List<String> names = new ArrayList<>();
      names.add("hsa-miR-122-5p");
      List<MiRNA> listFromDB = miRNAParser.getMiRnasByNames(names);
      assertEquals(1, listFromDB.size());
      MiRNA mirna = listFromDB.get(0);
      for (Target target : mirna.getTargets()) {
        assertNotNull(target.getName());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindByTarget() throws Exception {

    GeneralCacheInterface cache = miRNAParser.getCache();
    // exclude first cached value
    miRNAParser.setCache(new GeneralCacheWithExclusion(cache, 1));
    try {
      Set<MiriamData> targets = new HashSet<MiriamData>();
      MiriamData hgncTarget = new MiriamData(MiriamType.HGNC_SYMBOL, "PLAG1");
      targets.add(hgncTarget);
      List<MiRNA> listFromDB = miRNAParser.getMiRnaListByTargets(targets);
      assertTrue(listFromDB.size() > 0);
      for (MiRNA miRNA : listFromDB) {
        boolean found = false;
        for (Target targt : miRNA.getTargets()) {
          if (targt.getGenes().contains(hgncTarget)) {
            found = true;
          }
        }
        assertTrue(found);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miRNAParser.setCache(cache);
    }
  }

  @Test(timeout = 50000)
  public void testCachableInterfaceInvalidateChemical() throws Exception {
    try {
      List<String> listIds = new ArrayList<>();
      listIds.add("gga-miR-489-3p");
      List<MiRNA> list = miRNAParser.getMiRnasByNames(listIds);
      assertTrue(!list.isEmpty());
      assertEquals(1, list.get(0).getTargets().size());

      waitForRefreshCacheQueueToEmpty();

      cache.invalidateByQuery(MiRNAParser.MI_RNA_PREFIX + list.get(0).getName(), miRNAParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      listIds.clear();
      listIds.add(list.get(0).getName());

      List<MiRNA> list2 = miRNAParser.getMiRnasByNames(listIds);
      assertNotNull(list2);
      assertEquals(1, list2.size());
      assertFalse("Value wasn't refreshed from db", list.get(0).equals(list2.get(0)));
      assertTrue(list.get(0).getName().equals(list2.get(0).getName()));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetMiRna() throws Exception {
    try {
      String mirBaseName = "hsa-miR-296-5p";
      List<String> names = new ArrayList<>();
      names.add(mirBaseName);
      List<MiRNA> list = miRNAParser.getMiRnasByNames(names);
      assertTrue(!list.isEmpty());
      MiRNA mirna = list.get(0);

      for (Target target : mirna.getTargets()) {
        Set<MiriamData> mds = new HashSet<>();
        mds.addAll(target.getReferences());
        assertEquals("Target contains duplicat references: " + target, mds.size(), target.getReferences().size());
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void invalidMiRNAToString2() throws Exception {
    try {

      String str = miRNAParser.getMiRnaSerializer().objectToString(null);
      assertNull(str);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      miRNAParser.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      miRNAParser.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      Object result = miRNAParser.refreshCacheQuery(MiRNAParser.MI_RNA_PREFIX + "mmu-miR-122-5p");
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend taxonomyBackend = miRNAParser.getTaxonomyBackend();
    try {
      TaxonomyBackend mockBackend = Mockito.mock(TaxonomyBackend.class);
      when(mockBackend.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
      miRNAParser.setTaxonomyBackend(mockBackend);
      miRNAParser.refreshCacheQuery(MiRNAParser.MI_RNA_PREFIX + "mmu-miR-122-5p");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      miRNAParser.setTaxonomyBackend(taxonomyBackend);
    }
  }

  @Test
  public void testRefreshCacheQuery2() throws Exception {
    try {
      Object result = miRNAParser
          .refreshCacheQuery(MiRNAParser.MI_RNA_TARGET_PREFIX + MiriamType.HGNC_SYMBOL + ":" + "PLAG1");
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    try {
      Project project = new Project();
      project.setId(-1);

      List<String> result = miRNAParser.getSuggestedQueryList(project);

      assertEquals(0, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    try {
      Project project = new Project();
      Model model = getModelForFile("testFiles/target_mirna/target.xml", false);
      project.addModel(model);

      List<String> result = miRNAParser.getSuggestedQueryList(project);

      assertTrue(result.size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

}
