package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.ChemicalDirectEvidence;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;

public class ChemicalParserTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(ChemicalParserTest.class);

  final MiriamData parkinsonDiseaseId = new MiriamData(MiriamType.MESH_2012, "D010300");

  @Autowired
  private ChemicalParser chemicalParser;

  @Autowired
  private GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    chemicalParser.setCache(new GeneralCacheWithExclusion(cache, 1));
  }

  @After
  public void tearDown() throws Exception {
    chemicalParser.setCache(cache);
  }

  @Test
  public void testCreateChemicalListFromDB() throws Exception {
    GeneralCacheInterface cache = chemicalParser.getCache();
    try {
      // skip first call to cache, so we will have to at least parse the data
      chemicalParser.setCache(new GeneralCacheWithExclusion(cache, 1));
      // Parkinson disease
      Map<MiriamData, String> result = chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
      assertNotNull(result);
      assertTrue(!result.isEmpty());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setCache(cache);
    }
  }

  @Test
  public void testGetChemicalListWhenProblemWithConnection() throws Exception {
    GeneralCacheInterface cache = chemicalParser.getCache();
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      // skip first call to cache, so we will have to at least parse the data
      chemicalParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemicalParser.setWebPageDownloader(mockDownloader);
      // Parkinson disease
      chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
    } catch (ChemicalSearchException e) {
      assertTrue(e.getMessage().contains("ctdbase service unavailable"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setCache(cache);
      chemicalParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetChemicalListForInvalidDiseaseId() throws Exception {
    try {
      chemicalParser.getChemicalsForDisease(null);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("disease cannot be null"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCreateChemicalListFromDBWithInvalidId() throws Exception {
    try {
      // Parkinson disease
      MiriamData diseaseID = new MiriamData(MiriamType.MESH_2012, "D01030012");
      Map<MiriamData, String> result = chemicalParser.getChemicalsForDisease(diseaseID);
      assertNotNull(result);
      assertTrue(result.isEmpty());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindByIDFromChemicalList() throws Exception {
    try {
      // Parkinson disease
      Map<MiriamData, String> listFromDB = chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
      assertNotNull(listFromDB);
      assertTrue(!listFromDB.isEmpty());
      Map.Entry<MiriamData, String> entry = listFromDB.entrySet().iterator().next();
      assertNotNull(entry);
      // Get the first entry
      String chemicalName1 = entry.getValue();
      assertTrue((chemicalName1 != null) && !chemicalName1.trim().isEmpty());
      chemicalName1 = chemicalName1.trim().toLowerCase();
      MiriamData chemicalId1 = entry.getKey();
      assertTrue(
          (chemicalId1 != null) && (chemicalId1.getResource() != null) && !chemicalId1.getResource().trim().isEmpty());
      // Dopamine with 23 Gens and 60 publication and one marker/mechanism and
      // 60 publications
      MiriamData chemicalId2 = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D019803");
      // lazabemide with one Gene MAOB and therapeutic and 3 publications
      MiriamData chemicalId3 = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "C059303");
      // caramiphen with no genes and one therapeutic
      MiriamData chemicalId4 = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "C004519");

      List<MiriamData> idsList = new ArrayList<MiriamData>();
      idsList.add(chemicalId1);
      idsList.add(chemicalId2);
      idsList.add(chemicalId3);
      idsList.add(chemicalId4);

      List<Chemical> chemcials = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
      for (Chemical chem : chemcials) {
        assertNotNull(chem);
        for (Target t : chem.getInferenceNetwork()) {
          for (MiriamData md : t.getReferences()) {
            assertEquals(MiriamType.PUBMED, md.getDataType());
            assertTrue(NumberUtils.isDigits(md.getResource()));
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetGlutathioneDisulfideData() throws Exception {
    try {
      MiriamData glutathioneDisulfideId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D019803");

      List<MiriamData> idsList = new ArrayList<>();
      idsList.add(glutathioneDisulfideId);
      List<Chemical> chemicals = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);

      assertEquals(1, chemicals.size());
      Chemical glutathioneDisulfide = chemicals.get(0);

      assertTrue(glutathioneDisulfide.getSynonyms().size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetChemicalBySynonym() throws Exception {
    try {
      List<Chemical> chemicals = chemicalParser.getChemicalsBySynonym(parkinsonDiseaseId, "MPTP");

      assertEquals(1, chemicals.size());
      Chemical mptp = chemicals.get(0);

      assertTrue(mptp.getSynonyms().contains("MPTP"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindByInvalidIdFromChemicalList() throws Exception {

    try {
      // Parkinson disease
      MiriamData chemicalId2 = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D0198012433");

      List<MiriamData> idsList = new ArrayList<MiriamData>();
      idsList.add(chemicalId2);

      List<Chemical> chemcials = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);

      assertTrue(chemcials.isEmpty());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindByInvalidDiseaseIdFromChemicalList() throws Exception {
    try {
      chemicalParser.getChemicals(null, new ArrayList<>());
      fail("Exceptione expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("disease cannot be null"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test(timeout = 30000)
  public void testCachableInterfaceInvalidateChemical() throws Exception {
    try {

      Map<MiriamData, String> listFromDB = chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
      assertNotNull(listFromDB);
      assertTrue(!listFromDB.isEmpty());
      MiriamData chemicalId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039");
      List<MiriamData> idsList = new ArrayList<MiriamData>();
      idsList.add(chemicalId);
      List<Chemical> chemcials = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
      assertNotNull(chemcials);
      assertTrue(chemcials.size() > 0);

      waitForRefreshCacheQueueToEmpty();

      cache.invalidateByQuery(chemicalParser.getIdentifier(parkinsonDiseaseId, chemcials.get(0).getChemicalId()),
          chemicalParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      idsList.clear();

      idsList.add(chemcials.get(0).getChemicalId());

      List<Chemical> chemcials2 = chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
      assertNotNull(chemcials2);
      assertTrue(chemcials2.size() > 0);

      assertFalse("Value wasn't refreshed from db", chemcials.get(0).equals(chemcials2.get(0)));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByTarget() throws Exception {
    try {
      MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "TNF");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
      assertNotNull(list);
      assertFalse(list.isEmpty());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByInvalidTarget() throws Exception {
    try {
      MiriamData target = new MiriamData(MiriamType.WIKIPEDIA, "TNF");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
      fail("Exception expected");

    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByUnknownHgnc() throws Exception {
    try {
      // Parkinson disease
      MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "UNK_HGNC");
      chemicalParser.getChemicalListByTarget(target, parkinsonDiseaseId);
      // we have one warning regarding unknown hgnc symbol (we shouldn't throw
      // exception here bcuase these symbols sometimes are some
      // generic gene names)
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByTargetWhenProblemWithHgncConnection() throws Exception {
    HgncAnnotator annotator = chemicalParser.getHgncAnnotator();
    try {
      HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
      when(mockAnnotator.hgncToEntrez(any())).thenThrow(new AnnotatorException(""));
      chemicalParser.setHgncAnnotator(mockAnnotator);
      // Parkinson disease
      MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "UNK_HGNC");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
      fail("Exception expected");
    } catch (ChemicalSearchException e) {
      assertTrue(e.getMessage().contains("Problem with accessing hgnc service"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setHgncAnnotator(annotator);
    }
  }

  @Test
  public void testGetByEntrezTarget() throws Exception {
    try {
      // Parkinson disease
      MiriamData target = new MiriamData(MiriamType.ENTREZ, "6647");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
      assertTrue(list.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByTarget2() throws Exception {
    try {
      // Parkinson disease
      MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "GALM");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
      assertNotNull(list);
      assertTrue(list.isEmpty());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshQuery() throws Exception {
    try {
      Object object = chemicalParser.refreshCacheQuery(ChemicalParser.DISEASE_CHEMICALS_PREFIX + "D010300");
      assertNotNull(object);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshQuery2() throws Exception {
    try {
      Object object = chemicalParser.refreshCacheQuery("https://www.google.pl/");
      assertNotNull(object);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByTargetXXX() throws Exception {
    try {
      // Parkinson disease
      MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "SOD1");
      List<MiriamData> targets = new ArrayList<>();
      targets.add(target);
      List<Chemical> list = chemicalParser.getChemicalListByTarget(targets, parkinsonDiseaseId);
      assertNotNull(list);
      assertTrue(!list.isEmpty());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      chemicalParser.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      chemicalParser.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryWhenCtdDbNotAvailable() throws Exception {
    MiriamData chemicalId = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039");

    String query = chemicalParser.getIdentifier(parkinsonDiseaseId, chemicalId);

    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemicalParser.getCache();
    try {
      // disable cache
      chemicalParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      chemicalParser.setWebPageDownloader(mockDownloader);
      chemicalParser.refreshCacheQuery(query);
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setWebPageDownloader(downloader);
      chemicalParser.setCache(cache);
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, chemicalParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemicalParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, chemicalParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      chemicalParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, chemicalParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetChemicalFromChemicalLineWithInvalidHgnc() throws Exception {
    try {
      String line[] = new String[ChemicalParser.CHEMICALS_COUNT_COL + 1];
      for (int i = 0; i < line.length; i++) {
        line[i] = "";
      }
      line[ChemicalParser.CHEMICALS_DIRECT_COL] = ChemicalDirectEvidence.THERAPEUTIC.getValue();
      line[ChemicalParser.CHEMICALS_NETWORK_COL] = "UNK_HGNC";
      Chemical result = chemicalParser.getChemicalFromChemicalLine(line,
          new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039000"));
      // we have two errors related to parsing inference score and reference
      // count
      assertEquals(2, getErrors().size());
      // there are no genes (we have one that is invalid and should be
      // discarderd)
      assertEquals(0, result.getInferenceNetwork().size());

      assertEquals(ChemicalDirectEvidence.THERAPEUTIC, result.getDirectEvidence());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetChemicalFromChemicalLineWithHgncConnectionProblem() throws Exception {
    HgncAnnotator hgncAnnotator = chemicalParser.getHgncAnnotator();
    try {
      String line[] = new String[ChemicalParser.CHEMICALS_COUNT_COL + 1];
      for (int i = 0; i < line.length; i++) {
        line[i] = "";
      }
      line[ChemicalParser.CHEMICALS_DIRECT_COL] = ChemicalDirectEvidence.THERAPEUTIC.getValue();
      line[ChemicalParser.CHEMICALS_NETWORK_COL] = "UNK_HGNC";
      HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
      when(mockAnnotator.hgncToEntrez(any())).thenThrow(new AnnotatorException(""));
      chemicalParser.setHgncAnnotator(mockAnnotator);
      chemicalParser.getChemicalFromChemicalLine(line, new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039000"));
      fail("Exception expected");
    } catch (ChemicalSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setHgncAnnotator(hgncAnnotator);
    }
  }

  @Test
  public void testProblemWithPubmeds() throws Exception {

    GeneralCacheInterface cache = chemicalParser.getCache();
    WebPageDownloader downloader = chemicalParser.getWebPageDownloader();
    try {
      // Parkinson disease
      MiriamData chemicalID = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D015039");

      List<MiriamData> idsList = new ArrayList<MiriamData>();
      idsList.add(chemicalID);

      // remove info about single publication from cache (so we will have to download
      // it)
      chemicalParser.getCache().removeByQuery(
          "https://ctdbase.org/detail.go?6578706f7274=1&d-1340579-e=5&type=relationship&ixnId=4802115",
          chemicalParser.getCacheType());

      // disallow to use cache for first query (it will be directly about drug),
      // so parser will have to parse results taking data from cache (or
      // web sites if needed)
      chemicalParser.setCache(new GeneralCacheWithExclusion(chemicalParser.getCache(), 1));

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemicalParser.setWebPageDownloader(mockDownloader);
      try {
        chemicalParser.getChemicals(parkinsonDiseaseId, idsList);
        fail("Exception expected");
      } catch (ChemicalSearchException e) {
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemicalParser.setCache(cache);
      chemicalParser.setWebPageDownloader(downloader);
    }

  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    try {
      Project project = new Project();
      project.setId(-1);

      List<String> result = chemicalParser.getSuggestedQueryList(project, parkinsonDiseaseId);

      assertEquals(0, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    try {
      Project project = new Project();
      Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
      project.addModel(model);

      List<String> result = chemicalParser.getSuggestedQueryList(project, parkinsonDiseaseId);

      assertTrue(result.size() > 0);
      logger.debug(result);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetSuggestedQueryListForUnknownDiseas() throws Exception {
    try {
      Project project = new Project();
      Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
      project.addModel(model);

      List<String> result = chemicalParser.getSuggestedQueryList(project, null);

      assertEquals(0, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

}
