package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.species.GenericProtein;

public class MissingAnnotationTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		MissingAnnotation annotation = new MissingAnnotation(new GenericProtein("id"));
		assertTrue(annotation.toString().contains("misses annotations"));
	}

}
