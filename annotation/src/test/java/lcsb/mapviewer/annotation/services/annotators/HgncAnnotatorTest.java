package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class HgncAnnotatorTest extends AnnotationTestFunctions {

  final Logger logger = Logger.getLogger(HgncAnnotatorTest.class);

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnnotationsForSNCA() throws Exception {
    try {
      MiriamData snca = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias.addMiriamData(snca);
      hgncAnnotator.annotateElement(proteinAlias);
      assertNotNull(proteinAlias.getSymbol());
      assertTrue(proteinAlias.getFormerSymbols().size() > 0);
      assertNotNull(proteinAlias.getFullName());
      assertTrue(proteinAlias.getMiriamData().size() > 1);
      assertTrue(proteinAlias.getSynonyms().size() > 0);

      boolean ensemble = false;
      boolean hgncId = false;
      boolean hgncSymbol = false;
      boolean refseq = false;
      boolean entrez = false;
      boolean uniprot = false;
      for (MiriamData md : proteinAlias.getMiriamData()) {
        if (MiriamType.ENSEMBL.equals(md.getDataType())) {
          ensemble = true;
        } else if (MiriamType.HGNC.equals(md.getDataType())) {
          assertEquals("Invalid HGNC id", "11138", md.getResource());
          hgncId = true;
        } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
          hgncSymbol = true;
        } else if (MiriamType.REFSEQ.equals(md.getDataType())) {
          refseq = true;
        } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
          entrez = true;
        } else if (MiriamType.UNIPROT.equals(md.getDataType())) {
          uniprot = true;
        }
      }

      assertTrue("Ensemble symbol cannot be found", ensemble);
      assertTrue("Hgnc id cannot be found", hgncId);
      assertTrue("Hgnc symbol cannot be found", hgncSymbol);
      assertTrue("RefSeq cannot be found", refseq);
      assertTrue("Entrez cannot be found", entrez);
      assertTrue("Uniprot cannot be found", uniprot);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForElementWithMultiHGNC() throws Exception {
    try {
      MiriamData snca = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
      MiriamData park7 = new MiriamData(MiriamType.HGNC_SYMBOL, "PARK7");
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias.addMiriamData(snca);
      proteinAlias.addMiriamData(park7);
      hgncAnnotator.annotateElement(proteinAlias);

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForHGNC_ID() throws Exception {
    try {
      MiriamData nsmf = new MiriamData(MiriamType.HGNC, "11138");
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias.addMiriamData(nsmf);
      hgncAnnotator.annotateElement(proteinAlias);
      assertNotNull(proteinAlias.getSymbol());
      assertTrue(proteinAlias.getFormerSymbols().size() > 0);
      assertNotNull(proteinAlias.getFullName());
      assertTrue(proteinAlias.getMiriamData().size() > 1);
      assertTrue(proteinAlias.getSynonyms().size() > 0);

      boolean ensemble = false;
      boolean hgncId = false;
      boolean hgncSymbol = false;
      boolean refseq = false;
      boolean entrez = false;
      for (MiriamData md : proteinAlias.getMiriamData()) {
        if (MiriamType.ENSEMBL.equals(md.getDataType())) {
          ensemble = true;
        } else if (MiriamType.HGNC.equals(md.getDataType())) {
          hgncId = true;
        } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
          hgncSymbol = true;
        } else if (MiriamType.REFSEQ.equals(md.getDataType())) {
          refseq = true;
        } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
          entrez = true;
        }
      }

      assertTrue("Ensemble symbol cannot be found", ensemble);
      assertTrue("Hgnc id cannot be found", hgncId);
      assertTrue("Hgnc symbol cannot be found", hgncSymbol);
      assertTrue("RefSeq cannot be found", refseq);
      assertTrue("Entrez cannot be found", entrez);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForSNCA2() throws Exception {
    try {
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias.setName("SNCA");
      hgncAnnotator.annotateElement(proteinAlias);
      assertNotNull(proteinAlias.getSymbol());
      assertNotNull(proteinAlias.getName());
      assertTrue(proteinAlias.getFormerSymbols().size() > 0);
      assertNotNull(proteinAlias.getFullName());
      assertTrue(proteinAlias.getMiriamData().size() > 1);
      assertTrue(proteinAlias.getSynonyms().size() > 0);

      boolean ensemble = false;
      boolean hgncId = false;
      boolean hgncSymbol = false;
      boolean refseq = false;
      boolean entrez = false;
      for (MiriamData md : proteinAlias.getMiriamData()) {
        if (MiriamType.ENSEMBL.equals(md.getDataType())) {
          ensemble = true;
        } else if (MiriamType.HGNC.equals(md.getDataType())) {
          hgncId = true;
        } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
          hgncSymbol = true;
        } else if (MiriamType.REFSEQ.equals(md.getDataType())) {
          refseq = true;
        } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
          entrez = true;
        }
      }

      assertTrue("Ensemble symbol cannot be found", ensemble);
      assertTrue("Hgnc id cannot be found", hgncId);
      assertTrue("Hgnc symbol cannot be found", hgncSymbol);
      assertTrue("RefSeq cannot be found", refseq);
      assertTrue("Entrez cannot be found", entrez);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForBID() throws Exception {
    try {
      GenericProtein bidProtein = new GenericProtein("id");
      bidProtein.setName("BID");
      hgncAnnotator.annotateElement(bidProtein);
      
      GenericProtein bidMutationProtein = new GenericProtein("id2");
      bidMutationProtein.setName("BID (p15)");
      hgncAnnotator.annotateElement(bidMutationProtein);
      
      assertEquals(bidProtein.getSymbol(),bidMutationProtein.getSymbol());
      assertEquals(bidProtein.getFormerSymbols(),bidMutationProtein.getFormerSymbols());
      assertEquals(bidProtein.getFullName(),bidMutationProtein.getFullName());
      assertEquals(bidProtein.getMiriamData(),bidMutationProtein.getMiriamData());
      assertEquals(bidProtein.getSynonyms(),bidMutationProtein.getSynonyms());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForInvalid() throws Exception {
    try {
      Species proteinAlias = new GenericProtein("id");
      proteinAlias.setName("UNKNNOWNASD asd");
      hgncAnnotator.annotateElement(proteinAlias);

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForInvalid2() throws Exception {
    try {
      Species proteinAlias = new GenericProtein("id");
      proteinAlias.setName("cAMP/cGMP-dependent protein kinase");
      hgncAnnotator.annotateElement(proteinAlias);

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testCachableInterfaceInvalidate() throws Exception {
    String query = "http://google.pl/";
    try {
      String newRes = "hello";

      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, hgncAnnotator.getCacheType(), newRes);
      String res = cache.getStringByQuery(query, hgncAnnotator.getCacheType());
      assertEquals(newRes, res);
      cache.invalidateByQuery(query, hgncAnnotator.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      res = cache.getStringByQuery(query, hgncAnnotator.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, hgncAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnnotationsForInvalidMiriamSet() throws Exception {
    try {
      MiriamData md1 = new MiriamData(MiriamType.HGNC, "11138");
      MiriamData md2 = new MiriamData(MiriamType.HGNC, "111382");
      Species proteinAlias = new GenericProtein("");
      proteinAlias.addMiriamData(md1);
      proteinAlias.addMiriamData(md2);
      hgncAnnotator.annotateElement(proteinAlias);
      assertEquals(1, getWarnings().size());
      assertEquals("SNCA", proteinAlias.getName());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncIdToUniprot() throws Exception {
    try {
      List<MiriamData> result = hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.HGNC, "11138"));
      assertEquals(1, result.size());
      assertEquals(new MiriamData(MiriamType.UNIPROT, "P37840", HgncAnnotator.class), result.get(0));
      assertEquals(0, hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.HGNC, "1", HgncAnnotator.class)).size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncIdToName() throws Exception {
    try {
      assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "FSD2", HgncAnnotator.class),
          hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.HGNC, "18024")));
      assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "LMOD1", HgncAnnotator.class),
          hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.HGNC, "6647")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUnknownHgncIdToName() throws Exception {
    try {
      assertNull(hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.HGNC, "asd")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncToUniProt1() throws Exception {
    try {
      List<MiriamData> list = hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.HGNC_SYMBOL, "CASP8"));
      assertEquals(1, list.size());
      assertEquals("Q14790", list.get(0).getResource());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncToUniProtWithInvalidArg() throws Exception {
    try {
      hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.CAS, "CASP8"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncToEntrezWithInvalidArg() throws Exception {
    try {
      hgncAnnotator.hgncToEntrez(new MiriamData(MiriamType.CAS, "CASP8"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsValidHgnc() throws Exception {
    try {
      assertFalse(hgncAnnotator.isValidHgncMiriam(new MiriamData(MiriamType.CAS, "CASP8")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsValidHgnc2() throws Exception {
    try {
      assertTrue(hgncAnnotator.isValidHgncMiriam(new MiriamData(MiriamType.HGNC_SYMBOL, "CASP8")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncIdToNameWithInvalidArg() throws Exception {
    try {
      hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.CAS, "CASP8"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncToUniProt2() throws Exception {
    try {

      MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "CASP8");
      MiriamData data2 = new MiriamData(MiriamType.HGNC_SYMBOL, "CASP10");

      List<MiriamData> list = hgncAnnotator.hgncToUniprot(data1);
      assertEquals(1, list.size());
      assertEquals("Q14790", list.get(0).getResource());

      list = hgncAnnotator.hgncToUniprot(data2);
      assertEquals(1, list.size());
      assertEquals("Q92851", list.get(0).getResource());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncToUniProt3() throws Exception {
    try {
      MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "blablabla invalid name");
      List<MiriamData> list = hgncAnnotator.hgncToUniprot(data1);
      assertNotNull(list);
      assertEquals(0, list.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHgncToEntrez() throws Exception {
    try {
      // check by symbol
      MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "PTGS1");
      MiriamData entrez = hgncAnnotator.hgncToEntrez(data1);
      assertNotNull(entrez);
      assertTrue(new MiriamData(MiriamType.ENTREZ, "5742", HgncAnnotator.class).equals(entrez));

      // check by id
      data1 = new MiriamData(MiriamType.HGNC, "11138");
      entrez = hgncAnnotator.hgncToEntrez(data1);
      assertNotNull(entrez);
      assertTrue(new MiriamData(MiriamType.ENTREZ, "6622", HgncAnnotator.class).equals(entrez));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidHgncToEntrez() throws Exception {
    try {
      MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "xxxxxsd");
      MiriamData entrez = hgncAnnotator.hgncToEntrez(data1);
      assertNull(entrez);

      data1 = new MiriamData(MiriamType.HGNC, "xxxxxsd");
      entrez = hgncAnnotator.hgncToEntrez(data1);
      assertNull(entrez);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      hgncAnnotator.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      hgncAnnotator.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = hgncAnnotator.getWebPageDownloader();
    GeneralCacheInterface originalCache = hgncAnnotator.getCache();
    try {
      // exclude first cached value
      hgncAnnotator.setCache(null);

      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      hgncAnnotator.setWebPageDownloader(mockDownloader);
      hgncAnnotator.refreshCacheQuery("http://google.pl/");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      hgncAnnotator.setWebPageDownloader(downloader);
      hgncAnnotator.setCache(originalCache);
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = hgncAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      hgncAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, hgncAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      hgncAnnotator.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = hgncAnnotator.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("<response><result/></response>");
      hgncAnnotator.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, hgncAnnotator.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      hgncAnnotator.setWebPageDownloader(downloader);
    }
  }

}
