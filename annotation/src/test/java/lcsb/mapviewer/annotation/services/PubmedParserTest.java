package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class PubmedParserTest extends AnnotationTestFunctions {
	Logger												logger = Logger.getLogger(PubmedParserTest.class);
	private boolean								status;
	private boolean								status2;
	@Autowired
	PubmedParser									pubmedParser;

	@Autowired
	private GeneralCacheInterface	cache;

	@Before
	public void setUp() throws Exception {
		status = Configuration.isDbCacheOn();
		status2 = Configuration.isApplicationCacheOn();
	}

	@After
	public void tearDown() throws Exception {
		Configuration.setDbCacheOn(status);
		Configuration.setApplicationCacheOn(status2);
	}

	@Test
	public void test() {
		try {
			Configuration.setDbCacheOn(false);
			Configuration.setApplicationCacheOn(false);
			Article art = pubmedParser.getPubmedArticleById(9481670);
			assertNotNull(art);
			assertEquals(
					"Adjacent asparagines in the NR2-subunit of the NMDA receptor channel control the voltage-dependent block by extracellular Mg2+.", art.getTitle());
			assertEquals((Integer) 1998, art.getYear());
			assertEquals("The Journal of physiology", art.getJournal());
			assertTrue(art.getStringAuthors().contains("Wollmuth"));
			assertTrue(art.getStringAuthors().contains("Kuner"));
			assertTrue(art.getStringAuthors().contains("Sakmann"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception " + e.getMessage());
		}
	}

	@Test
	public void testCache() {
		try {
			Configuration.setDbCacheOn(true);
			Configuration.setApplicationCacheOn(true);

			Article art = pubmedParser.getPubmedArticleById(9481671);
			assertNotNull(art);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception " + e.getMessage());
		}
	}

	@Test
	public void testSerialization() {
		try {

			Article art = new Article();
			List<String> list = new ArrayList<String>();
			list.add("aaa");
			list.add("bbb");
			art.setTitle("ttt");
			art.setAuthors(list);
			art.setJournal("jjjj");
			art.setYear(123);

			String serialString = pubmedParser.getArticleSerializer().objectToString(art);

			assertTrue(serialString.contains("aaa"));
			assertTrue(serialString.contains("bbb"));
			assertTrue(serialString.contains("ttt"));
			assertTrue(serialString.contains("jjjj"));
			assertTrue(serialString.contains("123"));

			Article art2 = pubmedParser.getArticleSerializer().xmlToObject(getNodeFromXmlString(serialString));

			assertEquals(art.getStringAuthors(), art2.getStringAuthors());
			assertEquals(art.getTitle(), art2.getTitle());
			assertEquals(art.getJournal(), art2.getJournal());
			assertEquals(art.getYear(), art2.getYear());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception occurred");
		}
	}

	/**
	 * This case was problematic with old API used to retrieve data from pubmed
	 */
	@Test
	public void testProblematicCase() {
		try {

			Article art = pubmedParser.getPubmedArticleById(22363258);
			assertNotNull(art);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception " + e.getMessage());
		}
	}

	@Test
	public void testCitationCount() {
		try {

			Article art = pubmedParser.getPubmedArticleById(18400456);
			assertNotNull(art);
			assertTrue(art.getCitationCount() >= 53);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception " + e.getMessage());
		}
	}

	@Test
	public void testGetSummary() throws Exception {
		try {
			String summary = pubmedParser.getSummary(18400456);
			assertNotNull(summary);
			assertFalse(summary.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSummary2() throws Exception {
		try {

			String summary = pubmedParser.getSummary(23644949);
			assertNull(summary);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSummary3() throws Exception {
		try {
			String summary = pubmedParser.getSummary("18400456");
			assertNotNull(summary);
			assertFalse(summary.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	// wait max 15 second
	public void testCachableInterface() throws Exception {
		String query = "pubmed: 2112";
		try {
			String newRes = "hello";
			waitForRefreshCacheQueueToEmpty();

			cache.setCachedQuery(query, pubmedParser.getCacheType(), newRes);
			cache.invalidateByQuery(query, pubmedParser.getCacheType());

			waitForRefreshCacheQueueToEmpty();

			String res = cache.getStringByQuery(query, pubmedParser.getCacheType());

			assertNotNull(res);

			assertFalse("Value wasn't refreshed from db", newRes.equals(res));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			pubmedParser.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshWhenExternalNotAvailable() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		try {
			pubmedParser.setCache(null);
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			pubmedParser.setWebPageDownloader(mockDownloader);

			String query = PubmedParser.PUBMED_PREFIX + PubmedParser.SERVICE_STATUS_PUBMED_ID;

			pubmedParser.refreshCacheQuery(query);
			fail("Exception expected");
		} catch (SourceNotAvailable e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setWebPageDownloader(downloader);
			pubmedParser.setCache(cache);
		}

	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			pubmedParser.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetHtmlFullLinkForId() throws Exception {
		try {
			String htmlString = pubmedParser.getHtmlFullLinkForId(1234);
			assertTrue(htmlString.contains("Change in the kinetics of sulphacetamide tissue distribution"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, pubmedParser.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			pubmedParser.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, pubmedParser.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateDownStatus2() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
					.thenReturn("<responseWrapper><version>" + PubmedParser.SUPPORTED_VERSION + "</version></responseWrapper>");
			pubmedParser.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, pubmedParser.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateChangeStatus() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
					.thenReturn("<responseWrapper><version>" + PubmedParser.SUPPORTED_VERSION + "blabla</version></responseWrapper>");
			pubmedParser.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, pubmedParser.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testGetApiVersion() throws Exception {
		try {
			assertNotNull(pubmedParser.getApiVersion());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetApiVersionWhenProblemWithExternalDb() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		pubmedParser.setCache(null);
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("unknown response");
			pubmedParser.setWebPageDownloader(mockDownloader);
			pubmedParser.getApiVersion();
			fail("Exception expected");
		} catch (PubmedSearchException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setCache(cache);
			pubmedParser.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testGetApiVersionWhenProblemWithExternalDb3() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		pubmedParser.setCache(null);
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("<xml/>");
			pubmedParser.setWebPageDownloader(mockDownloader);
			assertNull(pubmedParser.getApiVersion());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setCache(cache);
			pubmedParser.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testGetApiVersionWhenProblemWithExternalDb2() throws Exception {
		WebPageDownloader downloader = pubmedParser.getWebPageDownloader();
		pubmedParser.setCache(null);
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			pubmedParser.setWebPageDownloader(mockDownloader);
			pubmedParser.getApiVersion();
			fail("Exception expected");
		} catch (PubmedSearchException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			pubmedParser.setCache(cache);
			pubmedParser.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testGetters() throws Exception {
		try {
			MiriamConnector mc = new MiriamConnector();
			XmlSerializer<Article> serializer = new XmlSerializer<>(Article.class);
			PubmedParser parser = new PubmedParser();

			parser.setMiriamConnector(mc);
			assertEquals(mc, parser.getMiriamConnector());

			parser.setArticleSerializer(serializer);

			assertEquals(serializer, parser.getArticleSerializer());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
