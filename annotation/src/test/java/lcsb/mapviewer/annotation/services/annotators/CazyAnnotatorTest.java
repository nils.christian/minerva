package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class CazyAnnotatorTest extends AnnotationTestFunctions {
	
	@Autowired
	CazyAnnotator cazyAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testUniprotToCazy() throws Exception {
		try {
			assertEquals(new MiriamData(MiriamType.CAZY, "GH5_7", CazyAnnotator.class), 
					cazyAnnotator.uniprotToCazy(new MiriamData(MiriamType.UNIPROT, "Q9SG95")));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAnnotateFromUniprot() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
			
			cazyAnnotator.annotateElement(protein);

			MiriamData mdCazy = null;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.CAZY)) {
					mdCazy = md;
				}
			}
			
			assertTrue("No UNIPROT annotation extracted from TAIR annotator", mdCazy != null);
			assertTrue("Invalid UNIPROT annotation extracted from TAIR annotator", mdCazy.getResource().equalsIgnoreCase("GH5_7") );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromTair() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT3G53010"));
			
			cazyAnnotator.annotateElement(protein);

			MiriamData mdCazy = null;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.CAZY)) {
					mdCazy = md;
				}
			}
			
			assertTrue("No CAZy annotation extracted from CAZy annotator", mdCazy != null);
			assertTrue("Invalid CAZy annotation extracted from CAZy annotator", mdCazy.getResource().equalsIgnoreCase("CE6") );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
	
	@Test
	public void testAnnotateMultipleUniprots() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q12540"));
			
			cazyAnnotator.annotateElement(protein);

			int cntMDs = 0;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.CAZY)) {
					cntMDs++;
				}
			}
			
			assertTrue("Wrong number of CAZy identifiers extracted from CAZy annotator", cntMDs == 2 );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void testAnnotateInvalidEmpty() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			cazyAnnotator.annotateElement(protein);

			assertEquals(0, protein.getMiriamData().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidUniprot() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
			cazyAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidTair() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
			cazyAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidUniprotToCazyNull() throws Exception {
		try {
			assertNull(cazyAnnotator.uniprotToCazy(null));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidUniprotToCazyWrongMd() throws Exception {
		try {
			cazyAnnotator.uniprotToCazy(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			cazyAnnotator.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			cazyAnnotator.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshCacheQuery() throws Exception {
		try {
			Object res = cazyAnnotator.refreshCacheQuery("http://google.cz/");
			assertNotNull(res);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, cazyAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = cazyAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			cazyAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, cazyAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cazyAnnotator.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateChangedStatus() throws Exception {
		WebPageDownloader downloader = cazyAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
			cazyAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, cazyAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cazyAnnotator.setWebPageDownloader(downloader);
		}
	}

}
