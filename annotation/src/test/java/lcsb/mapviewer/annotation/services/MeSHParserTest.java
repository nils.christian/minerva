package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class MeSHParserTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(MeSHParserTest.class);

  @Autowired
  MeSHParser meshParser;

  @Autowired
  private GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    meshParser.setCache(new GeneralCacheWithExclusion(cache, 1));
  }

  @After
  public void tearDown() throws Exception {
    meshParser.setCache(cache);
  }

  @Test
  public void testGetMesh() throws Exception {
    try {
      // Parkinson disease
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D004298");
      MeSH mesh = meshParser.getMeSH(meshID);
      assertTrue(mesh != null);
      assertTrue(mesh.getMeSHId() != null);
      assertTrue(mesh.getName() != null);
      assertTrue(mesh.getName().toLowerCase().equals("dopamine"));
      assertTrue(mesh.getDescription() != null);
      assertTrue(mesh.getSynonyms().size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetMeshBySynonym() throws Exception {
    try {
      String synonym = "MPTP";
      List<MeSH> result = meshParser.getMeshBySynonym(synonym);
      assertEquals(1, result.size());
      MeSH mesh = result.get(0);
      assertTrue(mesh.getSynonyms().contains(synonym));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsValidMesh() throws Exception {
    try {
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D004298");
      assertTrue(meshParser.isValidMeshId(meshID));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsValidMesh3() throws Exception {
    try {
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "blablabla");
      assertFalse(meshParser.isValidMeshId(meshID));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsValidMesh2() throws Exception {
    try {
      assertFalse(meshParser.isValidMeshId(null));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetInvalidMesh() throws Exception {
    try {
      meshParser.getMeSH(null);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      Object res = meshParser.refreshCacheQuery("http://google.pl/");
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetRotenoneMesh() throws Exception {
    try {
      // Rotenone disease -no synonyms
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D012402");
      MeSH mesh = meshParser.getMeSH(meshID);
      assertTrue(mesh != null);
      assertTrue(mesh.getMeSHId() != null);
      assertTrue(mesh.getName() != null);
      assertTrue(mesh.getName().toLowerCase().equals("rotenone"));
      assertTrue(mesh.getDescription() != null);
      assertEquals(0, mesh.getSynonyms().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCreateChemicalListFromDB3() throws Exception {
    try {
      // One synonyms
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "C503102");
      MeSH mesh = meshParser.getMeSH(meshID);
      assertTrue(mesh != null);
      assertTrue(mesh.getMeSHId() != null);
      assertTrue(mesh.getName() != null);
      assertTrue(
          mesh.getName().toLowerCase().contains("1-(3-(2-(1-benzothiophen-5-yl) ethoxy) propyl)-3-azetidinol maleate"));
      assertNull(mesh.getDescription());
      assertEquals(1, mesh.getSynonyms().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetMEshWithNetworkProblems() throws Exception {
    try {
      // Parkinson disease
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D004298");
      MeSHParser parserUnderTest = new MeSHParser();
      WebPageDownloader webPageDownloader = Mockito.mock(WebPageDownloader.class);
      when(webPageDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      parserUnderTest.setWebPageDownloader(webPageDownloader);

      parserUnderTest.getMeSH(meshID);
      fail("Exception expected");
    } catch (AnnotatorException e) {
      assertTrue(e.getMessage().contains("Problem with accessing MeSH database"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testExternalDBStatus() throws Exception {
    ExternalServiceStatus status = meshParser.getServiceStatus();
    logger.debug("Status of MeSH DB : " + status.getStatus().name());
    assertTrue(status.getStatus().equals(ExternalServiceStatusType.OK));
  }

  @Test
  public void testCachableInterfaceInvalidateChemical() throws Exception {
    try {
      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D010300");
      MeSH mesh = meshParser.getMeSH(meshID);
      assertNotNull(mesh);

      waitForRefreshCacheQueueToEmpty();

      cache.invalidateByQuery(meshParser.getIdentifier(meshID), meshParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      MeSH mesh2 = meshParser.getMeSH(meshID);
      assertNotNull(mesh2);

      assertFalse("Value wasn't refreshed from db", mesh.equals(mesh2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCleanHtml() {
    try {
      String withouthHtml = meshParser.cleanHtml(
          "<TD colspan=1>One of the catecholamine <A href=\"/cgi/mesh/2014/MB_cgi?mode=&term=NEUROTRANSMITTERS\">");
      assertFalse(withouthHtml.contains("<"));
      assertFalse(withouthHtml.contains(">"));
      assertTrue(withouthHtml.contains("catecholamine"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      meshParser.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      meshParser.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery3() throws Exception {
    try {
      String query = MeSHParser.MESH_PREFIX + "::::::";
      meshParser.refreshCacheQuery(query);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Problematic query"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshWhenProblemWithSourceDb() throws Exception {
    WebPageDownloader downloader = meshParser.getWebPageDownloader();
    GeneralCacheInterface cache = meshParser.getCache();
    try {
      meshParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      meshParser.setWebPageDownloader(mockDownloader);

      MiriamData meshID = new MiriamData(MiriamType.MESH_2012, "D010300");

      meshParser.refreshCacheQuery(meshParser.getIdentifier(meshID));
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
      assertTrue(e.getMessage().contains("Problem with accessing Mesh database"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      meshParser.setWebPageDownloader(downloader);
      meshParser.setCache(cache);
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = meshParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      meshParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, meshParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      meshParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader downloader = meshParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenThrow(new WrongResponseCodeIOException(null, HttpStatus.SC_NOT_FOUND));
      meshParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, meshParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      meshParser.setWebPageDownloader(downloader);
    }
  }

}
