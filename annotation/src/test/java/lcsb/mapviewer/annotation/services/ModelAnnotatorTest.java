package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.mutable.MutableDouble;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ReconAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.map.ModelDao;

public class ModelAnnotatorTest extends AnnotationTestFunctions {
	Logger								 logger	 = Logger.getLogger(ModelAnnotatorTest.class);
	IProgressUpdater			 updater = new IProgressUpdater() {
																	 @Override
																	 public void setProgress(double progress) {
																	 }
																 };

	@Autowired
	private ModelAnnotator modelAnnotator;

	@Autowired
	private ModelDao			 modelDao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnnotateModel() {
		try {
			Model model = new ModelFullIndexed(null);

			model.addReaction(new Reaction());

			Species proteinAlias1 = new GenericProtein("a1");
			proteinAlias1.setName("SNCA");

			Species proteinAlias2 = new GenericProtein("a2");
			proteinAlias2.setName("PDK1");
			proteinAlias2.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CAS, "c"));

			model.addElement(proteinAlias1);
			model.addElement(proteinAlias2);

			modelAnnotator.annotateModel(model, updater, null);

			assertTrue(model.getElementByElementId("a1").getMiriamData().size() > 0);
			assertTrue(model.getElementByElementId("a2").getMiriamData().size() >= 1);
			
			modelAnnotator.annotateModel(model, updater, null);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testAnnotateModelWithGivenAnnotators() {
		try {
			Model model = new ModelFullIndexed(null);
			GenericProtein species = new GenericProtein("id1");
			species.setName("SNCA");
			model.addElement(species);

			GenericProtein species2 = new GenericProtein("id2");
			species2.setName("PDK1");
			species2.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CAS, "c"));
			model.addElement(species2);
			model.addReaction(new Reaction());

			Map<Class<?>, List<ElementAnnotator>> annotators = new HashMap<>();
			List<ElementAnnotator> annotatorList = new ArrayList<>();
			annotatorList.addAll(modelAnnotator.getDefaultAnnotators());
			annotators.put(Reaction.class, annotatorList);
			annotators.put(GenericProtein.class, new ArrayList<>());

			ReconAnnotator reconMock = Mockito.mock(ReconAnnotator.class);
			Mockito.doThrow(new AnnotatorException("")).when(reconMock).annotateElement(any());
			annotators.get(Reaction.class).add(reconMock);
			modelAnnotator.annotateModel(model, updater, annotators);

			// check if our mock threw an exception and if this exception is in logs
			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	
	@Test
	public void testCopyMissingAnnotationsInCD() throws Exception {
		try {
			Model model = getModelForFile("testFiles/annotation/copyingAnnotationModel.xml", true);

			modelAnnotator.copyAnnotationFromOtherSpecies(model, updater);

			for (Species element : model.getSpeciesList()) {
				if (element.getName().equals("s4"))
					assertEquals(0, element.getMiriamData().size());
				else if (element.getName().equals("hello"))
					assertEquals(1, element.getMiriamData().size());
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDuplicateAnnotations() throws Exception {
		try {
			int counter = 0;
			Model model = getModelForFile("testFiles/annotation/duplicate.xml", false);

			Set<String> knowAnnotations = new HashSet<String>();

			modelAnnotator.annotateModel(model, updater, null);
			for (MiriamData md : model.getElementByElementId("sa1").getMiriamData()) {
				knowAnnotations.add(md.getDataType() + ":" + md.getRelationType() + ":" + md.getResource());
			}
			counter = 0;
			for (String string : knowAnnotations) {
				if (string.contains("29108")) {
					counter++;
				}
			}
			assertEquals(1, counter);

			modelAnnotator.annotateModel(model, updater, null);

			knowAnnotations.clear();
			for (MiriamData md : model.getElementByElementId("sa1").getMiriamData()) {
				knowAnnotations.add(md.getDataType() + ":" + md.getRelationType() + ":" + md.getResource());
			}
			counter = 0;
			for (String string : knowAnnotations) {
				if (string.contains("29108")) {
					counter++;
				}
			}
			assertEquals(1, counter);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsForSYN() throws Exception {
		try {
			Model model = getModelForFile("testFiles/annotation/emptyAnnotationsSyn1.xml", true);
			Element sa1 = model.getElementByElementId("sa1");
			Element sa2 = model.getElementByElementId("sa2");

			assertFalse(sa1.getNotes().contains("Symbol"));
			assertFalse(sa2.getNotes().contains("Symbol"));
			modelAnnotator.annotateModel(model, updater, null);
			assertFalse(sa2.getNotes().contains("Symbol"));
			assertNotNull(sa1.getSymbol());
			assertFalse(sa1.getSymbol().equals(""));
			// modelAnnotator.removeIncorrectAnnotations(model, updater);
			assertNotNull(sa1.getSymbol());
			assertFalse(sa1.getSymbol().equals(""));
			assertFalse(sa1.getNotes().contains("Symbol"));
			assertFalse(sa2.getNotes().contains("Symbol"));
			assertNull(sa2.getSymbol());

			for (Species el : model.getSpeciesList()) {
				if (el.getNotes() != null) {
					assertFalse("Invalid notes: " + el.getNotes(), el.getNotes().contains("Symbol"));
					assertFalse("Invalid notes: " + el.getNotes(), el.getNotes().contains("HGNC"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFindInmproperAnnotations() throws Exception {
		try {
			Model model = getModelForFile("testFiles/annotation/centeredAnchorInModifier.xml", true);
			Collection<? extends ProblematicAnnotation> results = modelAnnotator.findImproperAnnotations(model, new IProgressUpdater() {

				@Override
				public void setProgress(double progress) {
					// TODO Auto-generated method stub

				}
			}, null);
			assertTrue(results.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFindInmproperAnnotations2() throws Exception {
		try {
			List<MiriamType> list = new ArrayList<>();
			list.add(MiriamType.PUBMED);
			Reaction reaction = new Reaction();
			reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));
			List<ImproperAnnotations> result = modelAnnotator.findImproperAnnotations(reaction, list);
			assertEquals(0, result.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFindInmproperAnnotations3() throws Exception {
		try {
			List<MiriamType> list = new ArrayList<>();
			list.add(MiriamType.PUBMED);
			Reaction reaction = new Reaction();
			reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "inv_id"));
			List<ImproperAnnotations> result = modelAnnotator.findImproperAnnotations(reaction, list);
			assertEquals(1, result.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFindMissingAnnotations() throws Exception {
		try {
			Model model = getModelForFile("testFiles/annotation/missingAnnotations.xml", false);
			Collection<? extends ProblematicAnnotation> results = modelAnnotator.findMissingAnnotations(model, null);
			assertEquals(7, results.size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFindMissingAnnotations2() throws Exception {
		try {
			Model model = getModelForFile("testFiles/annotation/missingAnnotations.xml", false);
			Map<Class<? extends BioEntity>, Set<MiriamType>> requestedAnnotations = new HashMap<>();;
			requestedAnnotations.put(GenericProtein.class, new HashSet<>());
			Collection<? extends ProblematicAnnotation> results = modelAnnotator.findMissingAnnotations(model, requestedAnnotations);
			assertEquals(1, results.size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAnnotateModelWithDrugMolecule() throws Exception {
		try {
			Model model = super.getModelForFile("testFiles/annotation/problematic.xml", false);

			modelAnnotator.annotateModel(model, updater, null);

			modelDao.add(model);

			modelDao.delete(model);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetDefaultRequired() throws Exception {
		try {
			Map<Class<? extends BioEntity>, Set<MiriamType>> map1 = modelAnnotator.getDefaultRequiredClasses();
			assertNotNull(map1);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetDefaultValid() throws Exception {
		try {
			Map<Class<? extends BioEntity>, Set<MiriamType>> map2 = modelAnnotator.getDefaultValidClasses();
			assertNotNull(map2);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPerformAnnotationAndCheckProgress() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			Model submodel = new ModelFullIndexed(null);
			Model submodel2 = new ModelFullIndexed(null);

			GenericProtein protein = new GenericProtein("el");

			model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.UNKNOWN));
			model.addSubmodelConnection(new ModelSubmodelConnection(submodel2, SubmodelType.UNKNOWN));

			model.addElement(protein);
			submodel.addElement(protein);
			submodel2.addElement(protein);

			final MutableDouble maxProgress = new MutableDouble(0.0);

			modelAnnotator.performAnnotations(model, new IProgressUpdater() {
				@Override
				public void setProgress(double progress) {
					maxProgress.setValue(Math.max(progress, maxProgress.getValue()));
				}
			});
			assertTrue(maxProgress.getValue() <= IProgressUpdater.MAX_PROGRESS);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAvailableAnnotators() {
		try {
			List<ElementAnnotator> list = modelAnnotator.getAvailableAnnotators(Protein.class);
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAvailableAnnotatorNamess() {
		try {
			List<String> list = modelAnnotator.getAvailableAnnotatorNames(Protein.class);
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAvailableAnnotators2() {
		try {
			List<ElementAnnotator> list = modelAnnotator.getAvailableAnnotators();
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAvailableDefaultAnnotators() {
		try {
			List<ElementAnnotator> list = modelAnnotator.getAvailableDefaultAnnotators(Protein.class);
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAvailableDefaultAnnotatorNamess() {
		try {
			List<String> list = modelAnnotator.getAvailableDefaultAnnotatorNames(Protein.class);
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotatorsFromCommonNames() {
		try {
			List<ElementAnnotator> list = modelAnnotator.getAnnotatorsFromCommonNames(modelAnnotator.getAvailableAnnotatorNames(Protein.class));
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotatorsFromInvalidName() {
		try {
			String name = "unkName";
			List<String> names = new ArrayList<>();
			names.add(name);
			modelAnnotator.getAnnotatorsFromCommonNames(names);
			fail();
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


}
