package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotSearchException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

public class ChEMBLParserTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(ChEMBLParserTest.class);

  @Autowired
  private GeneralCacheInterface cache;

  @Autowired
  private ChEMBLParser chemblParser;

  @Before
  public void setUp() throws Exception {
    chemblParser.setCache(new GeneralCacheWithExclusion(cache, 1));
  }

  @After
  public void tearDown() throws Exception {
    chemblParser.setCache(cache);
  }

  @Test
  public void test1FindDrug() throws Exception {
    try {
      Drug drug = chemblParser.findDrug("CABOZANTINIB");

      assertEquals("CHEMBL2105717", drug.getSources().get(0).getResource());
      assertEquals("CABOZANTINIB", drug.getName());
      assertNull(drug.getDescription());
      assertEquals(2, drug.getTargets().size());
      List<String> lowerCaseSynonyms = new ArrayList<>();
      for (String s : drug.getSynonyms()) {
        lowerCaseSynonyms.add(s.toLowerCase());
      }
      assertTrue(lowerCaseSynonyms.contains("cabozantinib"));
      assertTrue(drug.getApproved());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void test2FindDrug() throws Exception {
    try {
      String n = "DIMETHYL FUMARATE";
      Drug test = chemblParser.findDrug(n);

      assertEquals("CHEMBL2107333", test.getSources().get(0).getResource());
      assertEquals("DIMETHYL FUMARATE", test.getName());
      // assertNull(test.getDescription());
      assertEquals(1, test.getTargets().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test3FindDrug() throws Exception {
    try {
      String n = "LOMITAPIDE";
      Drug test = chemblParser.findDrug(n);

      assertEquals("CHEMBL354541", test.getSources().get(0).getResource());
      assertEquals("LOMITAPIDE", test.getName());
      assertNull(test.getDescription());
      assertEquals(1, test.getTargets().size());

      for (Target t : test.getTargets()) {
        assertEquals(0, t.getReferences().size());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test4FindDrug() throws Exception {
    try {
      String n = "COBICISTAT";
      Drug test = chemblParser.findDrug(n);

      assertEquals("CHEMBL2095208", test.getSources().get(0).getResource());
      assertEquals("COBICISTAT", test.getName());
      assertNull(test.getDescription());
      assertEquals(1, test.getTargets().size());

      for (Target t : test.getTargets()) {
        assertEquals(0, t.getReferences().size());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test5FindDrug() throws Exception {
    try {
      String n = "TALIGLUCERASE ALFA";
      Drug test = chemblParser.findDrug(n);

      assertEquals("CHEMBL1964120", test.getSources().get(0).getResource());
      assertEquals("TALIGLUCERASE ALFA", test.getName());
      assertNull(test.getDescription());
      assertEquals(1, test.getTargets().size());

      for (Target t : test.getTargets()) {
        assertEquals(0, t.getReferences().size());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test6FindDrug() throws Exception {
    try {
      String n = "ABIRATERONE ACETATE";
      Drug test = chemblParser.findDrug(n);

      assertEquals("CHEMBL271227", test.getSources().get(0).getResource());
      assertEquals("ABIRATERONE ACETATE", test.getName());
      assertNull(test.getDescription());
      assertEquals(1, test.getTargets().size());

      for (Target t : test.getTargets()) {
        assertEquals(0, t.getReferences().size());
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test7FindDrug() throws Exception {
    try {
      String n = "asdwrgwedas";
      Drug test = chemblParser.findDrug(n);
      assertNull(test);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test8FindDrug() throws Exception {
    try {
      String n = "aa a32";
      Drug test = chemblParser.findDrug(n);
      assertNull(test);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test9FindDrug() throws Exception {
    try {
      String name = "PONATINIB";
      Drug test = chemblParser.findDrug(name);

      assertEquals("CHEMBL1171837", test.getSources().get(0).getResource());
      assertEquals(name, test.getName());
      assertNull(test.getDescription());
      assertEquals(3, test.getTargets().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test10FindDrug() throws Exception {
    try {
      String n = "DEXAMETHASONE";
      Drug test = chemblParser.findDrug(n);

      assertEquals("CHEMBL384467", test.getSources().get(0).getResource());
      assertEquals("DEXAMETHASONE", test.getName());
      assertNull(test.getDescription());
      assertEquals(1, test.getTargets().size());

      for (Target t : test.getTargets()) {
        assertTrue(t.getReferences()
            .contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "16891588")));
        assertTrue(t.getReferences()
            .contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "16956592")));
        assertTrue(t.getReferences()
            .contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "16971495")));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  /**
   * It is impossible to get references just from target, that's why after
   * getTargetFromId PubMedRef should be empty
   */
  @Test
  public void test1getTargetFromId() throws Exception {
    try {
      Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));

      assertEquals(test.getSource().getResource(), "CHEMBL3717");
      assertEquals(test.getName(), "Hepatocyte growth factor receptor");
      assertEquals(test.getOrganism().getResource(), "9606");
      assertEquals(TargetType.SINGLE_PROTEIN, test.getType());
      boolean contains = false;
      for (MiriamData md : test.getGenes()) {
        if (md.getResource().equals("MET")) {
          contains = true;
        }
      }
      assertTrue(contains);
      assertEquals(0, test.getReferences().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetTargetFromInvalidId() throws Exception {
    try {
      chemblParser.getTargetFromId(new MiriamData(MiriamType.WIKIPEDIA, "water"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetTargetFromIdWhenProblemWithDbConnection() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
      assertTrue(e.getMessage().contains("Problem with accessing information about target"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }

  }

  @Test
  public void testFindDrugWhenProblemWithDbConnection() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.findDrug("test");
      fail("Exception expected");
    } catch (DrugSearchException e) {
      assertTrue(e.getMessage().contains("Problem with parsing data from CHEMBL database"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }

  }

  @Test
  public void testGetTargetFromIdWhenProblemWithDbConnection2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
      assertTrue(e.getMessage().contains("Problem with accessing information about target"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }

  }

  @Test
  public void testGetTargetFromIdWhenProblemWithUniprot() throws Exception {
    UniprotAnnotator uniprotAnnotator = chemblParser.getUniprotAnnotator();

    try {
      UniprotAnnotator mockAnnotator = Mockito.mock(UniprotAnnotator.class);
      when(mockAnnotator.uniProtToHgnc(any())).thenThrow(new UniprotSearchException(null, null));
      chemblParser.setUniprotAnnotator(mockAnnotator);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setUniprotAnnotator(uniprotAnnotator);
    }

  }

  @Test
  public void testGetTargetFromIdWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend taxonomyBackend = chemblParser.getTaxonomyBackend();

    try {
      TaxonomyBackend mockAnnotator = Mockito.mock(TaxonomyBackend.class);
      when(mockAnnotator.getNameForTaxonomy(any())).thenThrow(new TaxonomySearchException(null, null));
      chemblParser.setTaxonomyBackend(mockAnnotator);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setTaxonomyBackend(taxonomyBackend);
    }

  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("<target><unk/></target>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("<target><target_components><target_component/></target_components></target>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse3() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("<target><target_chembl_id>inv id</target_chembl_id></target>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void test2getTargetFromId() throws Exception {
    try {
      Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3522"));

      assertEquals(test.getSource().getResource(), "CHEMBL3522");
      assertEquals(test.getName(), "Cytochrome P450 17A1");
      assertEquals(test.getOrganism().getResource(), "9606");
      boolean contains = false;
      for (MiriamData md : test.getGenes()) {
        if (md.getResource().equals("CYP17A1")) {
          contains = true;
        }
      }
      assertTrue(contains);
      assertEquals(0, test.getReferences().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test4getTargetFromId() throws Exception {
    try {
      Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2364681"));

      assertEquals(test.getSource().getResource(), "CHEMBL2364681");
      assertEquals(test.getName(), "Microsomal triglyceride transfer protein");
      assertEquals(test.getOrganism().getResource(), "9606");
      boolean contains = false;
      boolean contains2 = false;
      for (MiriamData md : test.getGenes()) {
        if (md.getResource().equals("MTTP")) {
          contains = true;
        }
        if (md.getResource().equals("P4HB")) {
          contains2 = true;
        }
      }
      assertTrue(contains);
      assertTrue(contains2);
      assertEquals(0, test.getReferences().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test5getTargetFromId() throws Exception {
    try {
      Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2364176"));

      assertEquals(test.getSource().getResource(), "CHEMBL2364176");
      assertEquals(test.getName(), "Glucocerebroside");
      assertEquals(test.getOrganism().getResource(), "9606");
      assertEquals(0, test.getGenes().size());
      assertEquals(0, test.getReferences().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void test6getTargetFromId() throws Exception {
    try {
      Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2069156"));

      assertEquals(test.getSource().getResource(), "CHEMBL2069156");
      assertEquals(test.getName(), "Kelch-like ECH-associated protein 1");
      assertEquals(test.getOrganism().getResource(), "9606");
      boolean contains = false;
      for (MiriamData md : test.getGenes()) {
        if (md.getResource().equals("KEAP1")) {
          contains = true;
        }
      }
      assertTrue(contains);
      assertEquals(0, test.getReferences().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetTargetsForAmantadine() throws Exception {
    try {
      Drug drug = chemblParser.findDrug("AMANTADINE");
      for (Target target : drug.getTargets()) {
        assertEquals(0, target.getReferences().size());
      }
      assertTrue(drug.getTargets().size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetTargetsWithReferences() throws Exception {
    try {
      Drug drug = chemblParser.findDrug("TRIMETHOPRIM");
      for (Target target : drug.getTargets()) {
        assertEquals(0, target.getReferences().size());
      }
      assertTrue(drug.getTargets().size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByInvalidHgncTarget() throws Exception {
    try {
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NR3B"));
      assertNotNull(drugs);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByHgncWhenHgncCrash() throws Exception {
    HgncAnnotator hgncAnnotator = chemblParser.getHgncAnnotator();
    try {
      HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
      when(mockAnnotator.hgncToUniprot(any())).thenThrow(new AnnotatorException(""));
      chemblParser.setHgncAnnotator(mockAnnotator);
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NR3B"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setHgncAnnotator(hgncAnnotator);
    }
  }

  @Test
  public void testFindDrugByHgncTargetWithManyUniprot() throws Exception {
    try {
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "AKAP7"));
      assertNotNull(drugs);
      // we should have a warning that akap7 has more than one uniprot id
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindDrugByUniprotTarget() throws Exception {
    try {
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
      assertTrue(drugs.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindDrugByInvalidTarget() throws Exception {
    try {
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.MESH_2012, "D010300"));
    } catch (DrugSearchException e) {
      assertTrue(e.getMessage().contains("Don't know how to process target of"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    try {
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      assertNotNull(drugs);
      assertTrue(drugs.size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindDrugByPFKMTarget() throws Exception {
    try {
      // this gene raised some errors at some point
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "PFKM"));
      assertNotNull(drugs);
      assertTrue(drugs.size() >= 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindDrugsByHgncTargets() throws Exception {
    try {
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      List<Drug> drugs2 = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3A"));
      List<MiriamData> hgnc = new ArrayList<MiriamData>();
      hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3A"));
      List<Drug> drugs3 = chemblParser.getDrugListByTargets(hgnc);
      assertNotNull(drugs3);
      assertTrue(drugs.size() + drugs2.size() > drugs3.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredOutByOrganism() throws Exception {
    try {
      List<MiriamData> organisms = new ArrayList<>();
      organisms.add(new MiriamData(MiriamType.TAXONOMY, "-1"));
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"), organisms);
      assertNotNull(drugs);
      assertEquals("No drugs for this organisms should be found", 0, drugs.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredByOrganism() throws Exception {
    try {
      List<MiriamData> organisms = new ArrayList<>();
      organisms.add(TaxonomyBackend.HUMAN_TAXONOMY);
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"), organisms);
      assertNotNull(drugs);
      assertTrue(drugs.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugsByRepeatingHgncTargets() throws Exception {
    try {
      List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      List<MiriamData> hgnc = new ArrayList<MiriamData>();
      hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
      List<Drug> drugs3 = chemblParser.getDrugListByTargets(hgnc);
      assertNotNull(drugs3);
      assertEquals(drugs.size(), drugs3.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testCachableInterfaceInvalidateUrl() throws Exception {
    String query = "http://google.lu/";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, chemblParser.getCacheType(), newRes);
      cache.invalidateByQuery(query, chemblParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, chemblParser.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  // wait max 15 second
  @Test(timeout = 15000)
  public void testCachableInterfaceInvalidateDrug() throws Exception {
    String query = ChEMBLParser.NAME_PREFIX + "TRIMETHOPRIM";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, chemblParser.getCacheType(), newRes);
      cache.invalidateByQuery(query, chemblParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, chemblParser.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      chemblParser.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      chemblParser.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    try {
      Object res = chemblParser.refreshCacheQuery("http://google.pl/");
      assertNotNull(res);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.refreshCacheQuery("http://google.pl/");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetDrugsByChemblTargetWhenChemblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugsByChemblTarget(new MiriamData());

      fail("Exception expected");
    } catch (DrugSearchException e) {
      assertEquals("Problem with accessing Chembl database", e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetDrugsByChemblTargetWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugsByChemblTarget(new MiriamData());

      fail("Exception expected");
    } catch (DrugSearchException e) {
      assertEquals("Problem with parsing Chembl response", e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testRefreshCacheQueryWhenChemblDbNotAvailable() throws Exception {
    String query = ChEMBLParser.NAME_PREFIX + "TRIMETHOPRIM";

    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    GeneralCacheInterface cache = chemblParser.getCache();
    try {
      // disable cache
      chemblParser.setCache(null);

      // simulate problem with downloading
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.refreshCacheQuery(query);
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, chemblParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, chemblParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("<response><molecules/></response>");
      chemblParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, chemblParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);

      // valid xml but with empty data
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("<response><molecules>"
          + "<molecule><pref_name/><max_phase/><molecule_chembl_id/><molecule_synonyms/></molecule>"
          + "</molecules><mechanisms/><molecule_forms/></response>");
      chemblParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, chemblParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetter() throws Exception {
    ChEMBLParser parser = new ChEMBLParser();
    XmlSerializer<Drug> drugSerializer = new XmlSerializer<>(Drug.class);
    parser.setDrugSerializer(drugSerializer);
    assertEquals(drugSerializer, parser.getDrugSerializer());
  }

  @Test
  public void testGetTargetsByInvalidDrugId() throws Exception {
    try {
      chemblParser.getTargetsByDrugId(new MiriamData());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetTargetsByDrugIdWithExternalDbDown() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, "123"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetTargetsByDrugIdWithExternalDbDown2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, "123"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testFindDrugByUniprotTargetWhenChemblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }

  }

  @Test
  public void testFindDrugByUniprotTargetWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }

  }

  @Test
  public void testParseReferences() throws Exception {
    try {
      Node node = super.getXmlDocumentFromFile("testFiles/chembl/references.xml");
      Set<MiriamData> references = chemblParser.parseReferences(node.getFirstChild());
      // we might have more references (if we decide to parse wikipedia, isbn,
      // etc)
      assertTrue(references.size() >= 3);
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testMoreThanOneDrugByName() throws Exception {
    try {
      chemblParser.setCache(new GeneralCacheWithExclusion(cache, 1));
      // we found out that carbidopa drug is doubled in the chembl databse
      chemblParser.findDrug("CARBIDOPA");
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetDrugByInvalidDrugId() throws Exception {
    try {
      chemblParser.getDrugById(new MiriamData());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetDrugByDrugIdWhenCehmblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, ""));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetDrugByDrugIdWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, ""));
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testParseSynonymsNode() throws Exception {
    try {
      Node node = super.getNodeFromXmlString(
          "<molecule_synonyms><synonym><synonyms>some synonym</synonyms></synonym><unknode/></molecule_synonyms>");
      List<String> synonyms = chemblParser.parseSynonymsNode(node);
      assertEquals(1, synonyms.size());
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetTargetsForChildElementsWhenChemblCrash() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsForChildElements(new MiriamData());
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetTargetsForChildElementsWhenChemblCrash2() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("invalid xml");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsForChildElements(new MiriamData());
      fail("Exception expected");
    } catch (DrugSearchException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

  @Test
  public void testGetTargetsForChildElementsWhenChemblReturnPartlyInvalidResult() throws Exception {
    WebPageDownloader downloader = chemblParser.getWebPageDownloader();
    try {
      chemblParser.setCache(null);
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
          .thenReturn("<response><molecule_forms><node/></molecule_forms></response>");
      chemblParser.setWebPageDownloader(mockDownloader);
      chemblParser.getTargetsForChildElements(new MiriamData());
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      chemblParser.setWebPageDownloader(downloader);
      chemblParser.setCache(cache);
    }
  }

}
