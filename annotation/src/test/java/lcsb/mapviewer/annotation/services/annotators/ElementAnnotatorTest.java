package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;

public class ElementAnnotatorTest extends AnnotationTestFunctions {

	class EAMock extends ElementAnnotator {

		public EAMock() {
			super(CachableInterface.class, new Class<?>[] {}, false);
		}

		@Override
		public void annotateElement(BioEntity element) throws AnnotatorException {
		}

		@Override
		public String getCommonName() {
			return null;
		}

		@Override
		public String getUrl() {
			return null;
		}

		@Override
		public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
			return null;
		}
	};

	ElementAnnotator annotator = new EAMock();

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetNotMatchingSymbol() {
		GenericProtein species = new GenericProtein("id");
		species.setSymbol("X");
		annotator.setSymbol(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingFullname() {
		GenericProtein species = new GenericProtein("id");
		species.setFullName("X");
		annotator.setFullName(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingNotes() {
		GenericProtein species = new GenericProtein("id");
		species.setNotes("X");
		annotator.setDescription(species, "Y");

		assertEquals(0, getWarnings().size());
	}

	@Test
	public void testSetEmptyDescription() {
		GenericProtein species = new GenericProtein("id");
		species.setNotes("X");
		annotator.setDescription(species, null);
		assertEquals("X", species.getNotes());
	}

	@Test
	public void testSetNotMatchingIchi() {
		Ion species = new Ion("id");
		species.setInChI("X");
		annotator.setInchi(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingSmile() {
		Ion species = new Ion("id");
		annotator.setSmile(species, "X", null);
		annotator.setSmile(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingCharge() {
		Ion species = new Ion("id");
		species.setCharge(2);
		annotator.setCharge(species, "3", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingSubsystem() {
		Reaction species = new Reaction();
		species.setSubsystem("X");
		annotator.setSubsystem(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingFormula() {
		Reaction species = new Reaction();
		species.setFormula("X");
		annotator.setFormula(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingAbbreviation() {
		Reaction species = new Reaction();
		species.setAbbreviation("X");
		annotator.setAbbreviation(species, "Y", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetNotMatchingMCS() {
		Reaction species = new Reaction();
		species.setMechanicalConfidenceScore(1);
		annotator.setMcs(species, "2", null);

		assertEquals(1, getWarnings().size());
	}

	@Test
	public void testSetTheSameMcs() throws Exception {
		try {
			Reaction reaction = new Reaction();
			reaction.setMechanicalConfidenceScore(4);
			annotator.setMcs(reaction, "4", null);
			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	@Test
	public void testAddKegg() {
		Reaction species = new Reaction();
		annotator.addKeggMiriam(species, "R123", null);
		assertEquals(1, species.getMiriamData().size());
	}

	@Test
	public void testAddChebi() {
		Reaction species = new Reaction();
		annotator.addChebiMiriam(species, "123");
		assertEquals(1, species.getMiriamData().size());
		assertTrue(species.getMiriamData().iterator().next().getResource().contains("CHEBI"));
	}

	@Test
	public void testAddInvalidKegg() {
		Reaction species = new Reaction();
		annotator.addKeggMiriam(species, "unk_id", null);
		assertEquals(0, species.getMiriamData().size());

		assertEquals(1, getWarnings().size());
	}
	
	@Test
	public void createMiriamData1() {
		MiriamData md = annotator.createMiriamData();
		assertEquals(md.getAnnotator(), annotator.getClass()); 
	}
	
	@Test
	public void createMiriamData2() {
		MiriamData md = annotator.createMiriamData(MiriamType.UNIPROT, "XXX");
		assertEquals(md.getAnnotator(), annotator.getClass()); 
	}
	
	@Test
	public void createMiriamData3() {
		MiriamData md = annotator.createMiriamData(MiriamRelationType.BQ_BIOL_ENCODES, MiriamType.UNIPROT, "XXX");
		assertEquals(md.getAnnotator(), annotator.getClass()); 
	}
	
	@Test
	public void createMiriamData4() {
		MiriamData md1 = new MiriamData(MiriamType.UNIPROT, "XXX");
		assertNull(md1.getAnnotator());
		
		MiriamData md2 = annotator.createMiriamData(md1);
		assertEquals(md2.getAnnotator(), annotator.getClass());
	}
	
	@Test
	public void testEmptyParamsDefs() {
		assertEquals(annotator.getParametersDefinitions().size(), 0);
	}

}
