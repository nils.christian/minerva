package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class StringAnnotatorTest extends AnnotationTestFunctions {
	
	@Autowired
	StringAnnotator testedAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnnotateUniprot() throws Exception {
		try {

			Species bioEntity = new GenericProtein("id");
			bioEntity.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P53350"));
			
			testedAnnotator.annotateElement(bioEntity);

			MiriamData mdString = null;

			for (MiriamData md : bioEntity.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.STRING)) {
					mdString = md; //there should be only one EC number for that TAIR<->UNIPROT record
				}
			}
			
			assertTrue("No STRING annotation extracted from STRING annotator", mdString != null);
			assertTrue("Wrong number of annotations extract from STRING annotator", bioEntity.getMiriamData().size() == 2);
			assertTrue("Invalid STRING annotation extracted from STRING annotator based on the UniProt annotation", mdString.getResource().equalsIgnoreCase("P53350") );
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateTair() throws Exception {
		try {

			Species bioEntity = new GenericProtein("id");
			bioEntity.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT1G01030"));
			
			testedAnnotator.annotateElement(bioEntity);

			MiriamData mdString = null;

			for (MiriamData md : bioEntity.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.STRING)) {
					mdString = md; //there should be only one EC number for that TAIR<->UNIPROT record
				}
			}
			
			assertTrue("No STRING annotation extracted from STRING annotator", mdString != null);
			assertTrue("Wrong number of annotations extract from STRING annotator", bioEntity.getMiriamData().size() == 3);
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
		
	@Test
	public void testAnnotateInvalidEmpty() throws Exception {
		try {
			Species bioEntity = new GenericProtein("id");			
			testedAnnotator.annotateElement(bioEntity);

			assertEquals(0, bioEntity.getMiriamData().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidTair() throws Exception {
		try {
			Species bioEntity = new GenericProtein("id");
			bioEntity.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
			testedAnnotator.annotateElement(bioEntity);

			assertEquals(1, bioEntity.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	//All the service status tests are not necessary, since STRING annotator 
	//internally calls TAIR annotator which has it own set of tests
}
