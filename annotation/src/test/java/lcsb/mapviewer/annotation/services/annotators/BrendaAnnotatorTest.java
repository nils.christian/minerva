package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class BrendaAnnotatorTest extends AnnotationTestFunctions {
	
	@Autowired
	BrendaAnnotator brendaAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnnotateFromUniprot() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));
			
			brendaAnnotator.annotateElement(protein);

			int cntMds = 0;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.BRENDA)) {
					cntMds++;
					assertTrue("Invalid BRENDA annotation extracted from BRENDA annotator", 
							md.getResource().equals("2.6.1.1") || md.getResource().equals("2.6.1.7") );
				}
			}
			
			assertTrue("Incorrect BRENDA annotations extracted from BRENDA annotator", cntMds == 2);
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromTair() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT5G48930"));
			
			brendaAnnotator.annotateElement(protein);

			MiriamData mdBrenda = null;

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.BRENDA)) {
					mdBrenda = md; //there should be only one EC number for that TAIR<->UNIPROT record
				}
			}
			
			assertTrue("No BRENDA annotation extracted from BRENDA annotator", mdBrenda != null);
			assertTrue("Invalid BRENDA annotation extracted from BRENDA annotator based on TAIR", mdBrenda.getResource().equals("2.3.1.133") );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void testAnnotateMultipleUniprots() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q12540"));
			
			brendaAnnotator.annotateElement(protein);

			assertTrue("Wrong number of BRENDA identifiers extracted from BRENDA annotator", protein.getMiriamData().size() == 4 );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void testAnnotateMultipleUniprotsWithIdenticalEC() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q9SG95"));
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q8L5J1"));
			
			brendaAnnotator.annotateElement(protein);

			assertTrue("Wrong number of BRENDA identifiers extracted from BRENDA annotator", protein.getMiriamData().size() == 3 );

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void testAnnotateInvalidEmpty() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			brendaAnnotator.annotateElement(protein);

			assertEquals(0, protein.getMiriamData().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidUniprot() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
			brendaAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidTair() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
			brendaAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}	

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			brendaAnnotator.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			brendaAnnotator.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshCacheQuery() throws Exception {
		try {
			Object res = brendaAnnotator.refreshCacheQuery("http://google.cz/");
			assertNotNull(res);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, brendaAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/* Relying on uniprot and tair annotators which have their own tests
	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = brendaAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			brendaAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, brendaAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			brendaAnnotator.setWebPageDownloader(downloader);
		}
	}
	

	@Test
	public void testSimulateChangedStatus() throws Exception {
		WebPageDownloader downloader = brendaAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
			brendaAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, brendaAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			brendaAnnotator.setWebPageDownloader(downloader);
		}
	}
	*/

}
