package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.data.Chebi;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import uk.ac.ebi.chebi.webapps.chebiWS.client.ChebiWebServiceClient;
import uk.ac.ebi.chebi.webapps.chebiWS.model.ChebiWebServiceFault_Exception;
import uk.ac.ebi.chebi.webapps.chebiWS.model.DataItem;
import uk.ac.ebi.chebi.webapps.chebiWS.model.Entity;
import uk.ac.ebi.chebi.webapps.chebiWS.model.LiteEntity;
import uk.ac.ebi.chebi.webapps.chebiWS.model.LiteEntityList;
import uk.ac.ebi.chebi.webapps.chebiWS.model.OntologyDataItem;
import uk.ac.ebi.chebi.webapps.chebiWS.model.SearchCategory;

public class ChebiAnnotatorTest extends AnnotationTestFunctions {
  static Logger logger = Logger.getLogger(ChebiAnnotatorTest.class);

  @Autowired
  ChebiAnnotator backend;

  @Autowired
  private GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetIdByName() throws Exception {
    try {
      MiriamData id = backend.getChebiForChebiName("adenine");
      assertEquals(new MiriamData(MiriamType.CHEBI, "CHEBI:16708", ChebiAnnotator.class), id);
      id = backend.getChebiForChebiName("asdf bvclcx lcxj vxlcvkj");
      assertNull(id);
      assertNull(backend.getChebiForChebiName(null));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testNameById() throws Exception {
    try {
      String name = backend.getChebiNameForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
      assertEquals("adenine", name);
      name = backend.getChebiNameForChebiId(new MiriamData(MiriamType.CHEBI, "16708"));
      assertEquals("adenine", name);
      name = backend.getChebiNameForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708a"));
      assertNull(name);
      name = backend.getChebiNameForChebiId(null);
      assertNull(name);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiById() throws Exception {
    try {
      Chebi chebi = backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
      assertNotNull(chebi);
      assertEquals("adenine", chebi.getName());
      assertEquals("Nc1ncnc2[nH]cnc12", chebi.getSmiles());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiByIdWhenConnectionFails() throws Exception {
    try {
      cache = backend.getCache();
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();
      item.setChebiId("CHEBI:18008");
      item.setType("is a");

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      when(chebiWebServiceClient.getLiteEntity(any(), any(), eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API),
          any())).thenThrow(new ChebiWebServiceFault_Exception(null, null));

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
      fail("Exception expected");

    } catch (ChebiSearchException e) {
      assertTrue(e.getMessage().contains("Problem with chebi"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testChebiByInvalidId() throws Exception {
    try {
      backend.getChebiElementForChebiId(new MiriamData(MiriamType.WIKIPEDIA, "water"));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebi() throws Exception {
    try {

      Chebi chebi = backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
      assertNotNull(chebi);
      assertEquals("adenine", chebi.getName());
      assertEquals("Nc1ncnc2[nH]cnc12", chebi.getSmiles());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiOntologyForWater() throws Exception {
    try {

      Chebi chebi = backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:15377"));
      assertNotNull(chebi);
      assertTrue(chebi.getIncomingChebi().size() > 0);
      assertTrue(chebi.getOutgoingChebi().size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testCachableInterfaceById() throws Exception {
    String query = "id: CHEBI:28423";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, backend.getCacheType(), newRes);
      cache.invalidateByQuery(query, backend.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, backend.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testCachableInterfaceByName() throws Exception {
    String query = "name: water";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, backend.getCacheType(), newRes);
      cache.invalidateByQuery(query, backend.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, backend.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiNameToId1() throws Exception {
    try {
      // easy tests with some molecule
      MiriamData result = backend.getChebiForChebiName("L-glutamate(2-)");
      assertNotNull(result);
      assertTrue(result.getResource().contains("CHEBI:29988"));
      // unknown molecule
      MiriamData string = backend.getChebiForChebiName("blablasblasblsabsal");
      assertNull(string);

      // and water
      result = backend.getChebiForChebiName("H2O");
      assertNotNull(result);
      assertTrue(result.getResource().contains("CHEBI:15377"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiNameToId4() throws Exception {
    try {
      // easy tests with some molecule
      MiriamData result = backend.getChebiForChebiName("Ca2+");
      assertEquals(new MiriamData(MiriamType.CHEBI, "CHEBI:29108", ChebiAnnotator.class), result);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiOntologyIds() throws Exception {
    try {
      // unknown molecule
      List<MiriamData> res = backend.getOntologyChebiIdsForChebiName("blablasblasblsabsal");
      assertNotNull(res);
      assertEquals(0, res.size());

      // and water
      res = backend.getOntologyChebiIdsForChebiName("h2o");
      assertNotNull(res);
      assertTrue(res.size() > 1);
      assertTrue(
          res.contains(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "CHEBI:24431", ChebiAnnotator.class)));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiNameToId2() throws Exception {
    try {
      // and now tests more complicated (how to find hydron?)
      MiriamData md = backend.getChebiForChebiName("hydron");
      assertNotNull(md);
      assertTrue(md.getResource().contains("CHEBI:15378"));
      md = backend.getChebiForChebiName("H+");
      assertNotNull(md);
      assertTrue(md.getResource().contains("CHEBI:15378"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChebiNameToId3() throws Exception {
    try {
      // and the tricky one - I couldn't find it via chebi online search tool...
      // But java API do the trick...
      // assertEquals("CHEBI:456214",converter.chebiNameToId("NADP+"));
      // however manual mapping done by chemist says different...:
      MiriamData md = backend.getChebiForChebiName("NADP+");
      assertNotNull(md);
      assertTrue(md.getResource().contains("CHEBI:18009"));
      md = backend.getChebiForChebiName("NADP(+)");
      assertNotNull(md);
      assertTrue(md.getResource().contains("CHEBI:18009"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotateChemical() throws Exception {
    try {
      SimpleMolecule simpleMolecule = new SimpleMolecule("id");
      simpleMolecule.setName("water");

      backend.annotateElement(simpleMolecule);

      assertFalse(simpleMolecule.getFullName().equals(""));
      assertFalse(simpleMolecule.getInChI().equals(""));
      assertFalse(simpleMolecule.getInChIKey().equals(""));
      assertFalse(simpleMolecule.getSmiles().equals(""));
      assertFalse(simpleMolecule.getSynonyms().size() == 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAnnotateWhenConnectionFails() throws Exception {
    try {
      SimpleMolecule simpleMolecule = new SimpleMolecule("id");
      simpleMolecule.setName("water");

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();
      item.setChebiId("CHEBI:18008");
      item.setType("is a");

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any()))
              .thenThrow(new ChebiWebServiceFault_Exception(null, null));

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      backend.setCache(null);
      backend.annotateElement(simpleMolecule);

      fail("Exceptione expected");
    } catch (AnnotatorException e) {
      assertTrue(e.getMessage().contains("Problem with getting information about chebi"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      backend.setCache(cache);
      backend.setClient(null);
    }
  }

  @Test
  public void testAnnotateChemicalWithInvalidName() throws Exception {
    try {
      SimpleMolecule simpleMolecule = new SimpleMolecule("id");
      simpleMolecule.setName("blasdh");

      backend.annotateElement(simpleMolecule);

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateChemicalFromDb() throws Exception {
    try {
      SimpleMolecule simpleMolecule = new SimpleMolecule("id");
      simpleMolecule
          .addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "CHEBI:15377"));

      backend.annotateElement(simpleMolecule);

      assertFalse("".equals(simpleMolecule.getFullName()));
      assertFalse(simpleMolecule.getInChI().equals(""));
      assertFalse(simpleMolecule.getInChIKey().equals(""));
      assertFalse(simpleMolecule.getSmiles().equals(""));
      assertFalse(simpleMolecule.getSynonyms().size() == 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateChemicalFromDbWithDifferentData() throws Exception {
    try {
      SimpleMolecule simpleMolecule = new SimpleMolecule("id");
      simpleMolecule.setName("water");
      simpleMolecule.setFullName("xxx");
      simpleMolecule.setInChI("x");
      simpleMolecule.setInChIKey("x");
      simpleMolecule.setSmiles("x");
      simpleMolecule.addSynonym("x");
      simpleMolecule
          .addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "CHEBI:15377"));

      backend.annotateElement(simpleMolecule);

      assertEquals(5, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() throws Exception {
    try {
      assertNotNull(backend.getCommonName());
      assertNotNull(backend.getUrl());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshGetOntology() throws Exception {
    cache = backend.getCache();
    try {
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();
      item.setChebiId("CHEBI:18008");
      item.setType("is a");

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      when(chebiWebServiceClient.getCompleteEntity(any(String.class))).thenReturn(entity);

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      String query = ChebiAnnotator.ONTOLOGY_PREFIX + "CHEBI:18009";
      String result = backend.refreshCacheQuery(query);
      assertNotNull(result);
      assertTrue(result.contains("CHEBI:18008"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testGetChebiForChebiNameInFormula() throws Exception {
    cache = backend.getCache();
    try {
      String chebiId = "CHEBI:18008";
      String name = "art_name";
      DataItem matchedElement = new DataItem();
      matchedElement.setData(name);
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      entity.setChebiAsciiName("");
      entity.getFormulae().add(matchedElement);
      entity.setChebiId(chebiId);

      when(chebiWebServiceClient.getCompleteEntity(any(String.class))).thenReturn(entity);
      LiteEntityList liteEntityList = new LiteEntityList();

      LiteEntity liteEntity = new LiteEntity();
      liteEntityList.getListElement().add(liteEntity);
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(liteEntityList);

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      MiriamData result = backend.getChebiForChebiName(name);
      assertNotNull(result);
      assertEquals(chebiId, result.getResource());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testGetChebiForChebiNameInFormula2() throws Exception {
    cache = backend.getCache();
    try {
      String chebiId = "CHEBI:18008";
      String name = "art_name";
      DataItem matchedElement = new DataItem();
      matchedElement.setData(name);
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      entity.setChebiAsciiName("");
      entity.getFormulae().add(matchedElement);
      entity.setChebiId(chebiId);

      when(chebiWebServiceClient.getCompleteEntity(any(String.class))).thenReturn(entity);
      LiteEntityList liteEntityList = new LiteEntityList();

      LiteEntity liteEntity = new LiteEntity();
      liteEntityList.getListElement().add(liteEntity);
      // when searching by name return empty list
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(new LiteEntityList());
      // when searchng by all return list with elements
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.ALL),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(liteEntityList);

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      MiriamData result = backend.getChebiForChebiName(name);
      assertNotNull(result);
      assertEquals(chebiId, result.getResource());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testGetChebiForChebiNameInSynonym() throws Exception {
    cache = backend.getCache();
    try {
      String chebiId = "CHEBI:18008";
      String name = "art_name";
      DataItem matchedElement = new DataItem();
      matchedElement.setData(name);
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      entity.setChebiAsciiName("");
      entity.getSynonyms().add(matchedElement);
      entity.setChebiId(chebiId);

      when(chebiWebServiceClient.getCompleteEntity(any(String.class))).thenReturn(entity);
      LiteEntityList liteEntityList = new LiteEntityList();

      LiteEntity liteEntity = new LiteEntity();
      liteEntityList.getListElement().add(liteEntity);
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(liteEntityList);

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      MiriamData result = backend.getChebiForChebiName(name);
      assertNotNull(result);
      assertEquals(chebiId, result.getResource());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testGetChebiForChebiNameInSynonym2() throws Exception {
    cache = backend.getCache();
    try {
      String chebiId = "CHEBI:18008";
      String name = "art_name";
      DataItem matchedElement = new DataItem();
      matchedElement.setData(name);
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      entity.setChebiAsciiName("");
      entity.getSynonyms().add(matchedElement);
      entity.setChebiId(chebiId);

      when(chebiWebServiceClient.getCompleteEntity(any(String.class))).thenReturn(entity);
      LiteEntityList liteEntityList = new LiteEntityList();

      LiteEntity liteEntity = new LiteEntity();
      liteEntityList.getListElement().add(liteEntity);
      // when searching by name return empty list
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(new LiteEntityList());
      // when searchng by all return list with elements
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.ALL),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(liteEntityList);

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      MiriamData result = backend.getChebiForChebiName(name);
      assertNotNull(result);
      assertEquals(chebiId, result.getResource());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testGetChebiForChebiNameWithConnectionProblems() throws Exception {
    cache = backend.getCache();
    try {
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      // when searching by name return empty list
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any()))
              .thenThrow(new ChebiWebServiceFault_Exception(null, null));

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      backend.getChebiForChebiName("bla");

    } catch (ChebiSearchException e) {
      assertTrue(e.getMessage().contains("Problem with chebi connection"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testRefreshWhenProblemWithConnnector() throws Exception {
    cache = backend.getCache();
    try {
      // exclude first cached value
      backend.setCache(null);

      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);

      OntologyDataItem item = new OntologyDataItem();
      item.setChebiId("CHEBI:18008");
      item.setType("is a");

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      when(chebiWebServiceClient.getCompleteEntity(any(String.class)))
          .thenThrow(new ChebiWebServiceFault_Exception(null, null));

      // we will use mock object as a connection to chebi server
      backend.setClient(chebiWebServiceClient);

      String query = ChebiAnnotator.ONTOLOGY_PREFIX + "CHEBI:18009";
      backend.refreshCacheQuery(query);
      fail("Exception expected");

    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore cache for other tests
      backend.setCache(cache);
      // remove mock
      backend.setClient(null);
    }
  }

  @Test
  public void testRefreshInvalidQuery() throws Exception {
    try {
      backend.refreshCacheQuery("invalid string");

      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidQuery2() throws Exception {
    try {
      backend.refreshCacheQuery(new Object());

      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, backend.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    backend.setCache(null);
    try {
      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);
      when(chebiWebServiceClient.getLiteEntity(any(), any(), eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API),
          any())).thenThrow(new ChebiWebServiceFault_Exception(null, null));
      backend.setClient(chebiWebServiceClient);
      assertEquals(ExternalServiceStatusType.DOWN, backend.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      backend.setCache(cache);
      backend.setClient(null);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    backend.setCache(null);
    try {
      ChebiWebServiceClient chebiWebServiceClient = Mockito.mock(ChebiWebServiceClient.class);
      backend.setClient(chebiWebServiceClient);

      String chebiId = "CHEBI:18008";
      String name = "art";
      DataItem matchedElement = new DataItem();
      matchedElement.setData(name);
      // exclude first cached value
      backend.setCache(null);

      OntologyDataItem item = new OntologyDataItem();

      Entity entity = new Entity();
      entity.getOntologyChildren().add(item);
      entity.getOntologyParents().add(item);
      entity.setChebiAsciiName("");
      entity.getSynonyms().add(matchedElement);
      entity.setChebiId(chebiId);

      when(chebiWebServiceClient.getCompleteEntity(any(String.class))).thenReturn(entity);
      LiteEntityList liteEntityList = new LiteEntityList();

      LiteEntity liteEntity = new LiteEntity();
      liteEntityList.getListElement().add(liteEntity);
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.CHEBI_NAME),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(liteEntityList);
      when(chebiWebServiceClient.getLiteEntity(any(), eq(SearchCategory.ALL),
          eq(ChebiAnnotator.MAX_SEARCH_RESULTS_FROM_CHEBI_API), any())).thenReturn(liteEntityList);

      assertEquals(ExternalServiceStatusType.CHANGED, backend.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      backend.setCache(cache);
      backend.setClient(null);
    }
  }

}
