package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.UserAnnotatorsParam;

public class KeggAnnotatorTest extends AnnotationTestFunctions {
	
	@Autowired
	KeggAnnotator keggAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	private void Evaluate_3_1_2_14(Species protein) {
		
		Collection<MiriamData> mdPubmed = new ArrayList<>();

		for (MiriamData md : protein.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.PUBMED)) {
				mdPubmed.add(md);
			}
		}
		
		assertTrue("No PUBMED annotation extracted from KEGG annotator", mdPubmed.size() > 0);
		assertTrue("Wrong number of publications extracted from KEGG annotator", mdPubmed.size() == 2);
		
		Set<String> pmids = new HashSet<String>();
		pmids.add("30409");
		pmids.add("3134");
		int cntMatches = 0;
		for (MiriamData md: protein.getMiriamData()) {
			if (pmids.contains(md.getResource())) {
				cntMatches++;
			}				
		}
		
		assertTrue("Wrong PUBMED IDs extracted from KEGG annotator", cntMatches == 2);
		
	}
	
		

	@Test
	public void testAnnotateFromUniprotWithoutParams() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));
			
			keggAnnotator.annotateElement(protein);
			
			Evaluate_3_1_2_14(protein);
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromUniprotWithParams() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));
			
			UserAnnotatorsParam ap = new UserAnnotatorsParam(String.class, "KEGG organism identifier", "ATH");
			List<UserAnnotatorsParam> aps = new ArrayList<>();
			aps.add(ap);
			keggAnnotator.annotateElement(protein, aps);
			
			int cntTairs = 0;
			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
					cntTairs++;
				}
			}
			
			assertTrue("Invalid number of TAIR annotators from KEGG", cntTairs == 3);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromUniprotWithWrongParams1() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));
			
			UserAnnotatorsParam ap = new UserAnnotatorsParam(String.class, "XXX", "XXX");
			List<UserAnnotatorsParam> aps = new ArrayList<>();
			aps.add(ap);
			keggAnnotator.annotateElement(protein, aps);
			
			assertEquals("There should be warning about unsupported parameter", 1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromUniprotWithWrongParams2() throws Exception {
		try {

			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q42561"));
			List<UserAnnotatorsParam> aps = new ArrayList<>();
			aps.add(new UserAnnotatorsParam(String.class, "KEGG organism identifier", "ATH AAA"));
			keggAnnotator.annotateElement(protein, aps);
			
			int cntTairs = 0;
			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
					cntTairs++;
				}
			}
			
			assertTrue("Invalid number of TAIR annotators from KEGG", cntTairs == 3);
			assertEquals("There should be warning about unsupported parameter", 1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromEc() throws Exception {
		try {
			
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.EC, "3.1.2.14"));
			
			keggAnnotator.annotateElement(protein);
			
			Evaluate_3_1_2_14(protein);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateFromTair() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT3G25110"));
			
			keggAnnotator.annotateElement(protein);
			
			Evaluate_3_1_2_14(protein);
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}	
	
	
	@Test
	public void testAnnotateFromUniprotWithMultipleECs() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));
			
			keggAnnotator.annotateElement(protein);
			
			Collection<MiriamData> mdPubmed = new ArrayList<>();

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.PUBMED)) {
					mdPubmed.add(md);
				}
			}
			
			assertTrue("Wrong number of publications extracted from KEGG annotator", mdPubmed.size() == 9);			
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void testAnnotateFromUniprotWithMultipleECsAndEC() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));
			protein.addMiriamData(new MiriamData(MiriamType.EC, "3.1.2.14"));
			
			keggAnnotator.annotateElement(protein);
			
			Collection<MiriamData> mdPubmed = new ArrayList<>();

			for (MiriamData md : protein.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.PUBMED)) {
					mdPubmed.add(md);
				}
			}
			
			assertTrue("Wrong number of publications extracted from KEGG annotator", mdPubmed.size() == 11);			
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	@Test
	public void testAnnotateInvalidEmpty() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.setName("bla");
			keggAnnotator.annotateElement(protein);

			assertEquals(0, protein.getMiriamData().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidUniprot() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
			keggAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnnotateInvalidTair() throws Exception {
		try {
			Species protein = new GenericProtein("id");
			protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
			keggAnnotator.annotateElement(protein);

			assertEquals(1, protein.getMiriamData().size());

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			keggAnnotator.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			keggAnnotator.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshCacheQuery() throws Exception {
		try {
			Object res = keggAnnotator.refreshCacheQuery("http://google.cz/");
			assertNotNull(res);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, keggAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = keggAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			keggAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, keggAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			keggAnnotator.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateChangedStatus() throws Exception {
		WebPageDownloader downloader = keggAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
			keggAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, keggAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			keggAnnotator.setWebPageDownloader(downloader);
		}
	}

}
