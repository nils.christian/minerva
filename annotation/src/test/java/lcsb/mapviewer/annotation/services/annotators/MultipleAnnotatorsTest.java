package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class MultipleAnnotatorsTest extends AnnotationTestFunctions {

  @Autowired
  KeggAnnotator keggAnnotator;

  @Autowired
  UniprotAnnotator uniprotAnnotator;

  @Autowired
  HgncAnnotator hgncAnnotator;

  @Autowired
  BiocompendiumAnnotator biocompendiumAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotateUniprotByUniprotAndKegg() throws Exception {
    try {

      Species protein = new GenericProtein("id");
      protein.setName("bla");
      protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));

      keggAnnotator.annotateElement(protein);
      uniprotAnnotator.annotateElement(protein);
      hgncAnnotator.annotateElement(protein);
      // biocompendiumAnnotator.annotateElement(protein);

      int cntNoAnnotator = 0;
      for (MiriamData md : protein.getMiriamData()) {
        if (md.getAnnotator() == null) {
          cntNoAnnotator++;
        }
      }

      assertTrue("Wrong number of annotated elements with no information about annotator", cntNoAnnotator == 1);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
