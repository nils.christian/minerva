package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class EnsemblAnnotatorTest extends AnnotationTestFunctions {

	@Autowired
	EnsemblAnnotator ensemblAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAnnotationsForEnsemblId() throws Exception {
		try {
			MiriamData nsmf = new MiriamData(MiriamType.ENSEMBL, "ENSG00000157764");
			GenericProtein proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(nsmf);
			ensemblAnnotator.annotateElement(proteinAlias);
			assertNotNull(proteinAlias.getSymbol());
			assertNotNull(proteinAlias.getFullName());
			assertTrue(proteinAlias.getMiriamData().size() > 1);
			assertTrue(proteinAlias.getSynonyms().size() > 0);

			boolean ensemble = false;
			boolean hgncId = false;
			boolean hgncSymbol = false;
			boolean entrez = false;
			for (MiriamData md : proteinAlias.getMiriamData()) {
				if (MiriamType.ENSEMBL.equals(md.getDataType())) {
					ensemble = true;
				} else if (MiriamType.HGNC.equals(md.getDataType())) {
					hgncId = true;
				} else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
					hgncSymbol = true;
				} else if (MiriamType.ENTREZ.equals(md.getDataType())) {
					entrez = true;
				}
			}

			assertTrue("Ensemble symbol cannot be found", ensemble);
			assertTrue("Hgnc id cannot be found", hgncId);
			assertTrue("Hgnc symbol cannot be found", hgncSymbol);
			assertTrue("Entrez cannot be found", entrez);

			assertEquals(0, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsWhenMoreThanOneId() throws Exception {
		try {
			MiriamData nsmf = new MiriamData(MiriamType.ENSEMBL, "ENSG00000157764");
			MiriamData nsmf1 = new MiriamData(MiriamType.ENSEMBL, "ENSG00000157765");
			GenericProtein proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(nsmf);
			proteinAlias.addMiriamData(nsmf1);
			ensemblAnnotator.annotateElement(proteinAlias);

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsWhenNoIdFound() throws Exception {
		try {
			GenericProtein proteinAlias = new GenericProtein("id");
			ensemblAnnotator.annotateElement(proteinAlias);

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAnnotationsForInvalid() throws Exception {
		try {
			MiriamData nsmf = new MiriamData(MiriamType.ENSEMBL, "blabla");
			GenericProtein proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(nsmf);
			ensemblAnnotator.annotateElement(proteinAlias);

			assertEquals("There should be warning about invalid ensembl identifier", 1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testCachableInterfaceInvalidate() throws Exception {
		String query = "http://google.pl/";
		try {
			String newRes = "hello";

			waitForRefreshCacheQueueToEmpty();

			cache.setCachedQuery(query, ensemblAnnotator.getCacheType(), newRes);
			String res = cache.getStringByQuery(query, ensemblAnnotator.getCacheType());
			assertEquals(newRes, res);
			cache.invalidateByQuery(query, ensemblAnnotator.getCacheType());

			waitForRefreshCacheQueueToEmpty();

			res = cache.getStringByQuery(query, ensemblAnnotator.getCacheType());

			assertNotNull(res);

			assertFalse("Value wasn't refreshed from db", newRes.equals(res));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, ensemblAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatusDown() throws Exception {
		WebPageDownloader downloader = ensemblAnnotator.getWebPageDownloader();
		GeneralCacheInterface originalCache = ensemblAnnotator.getCache();
		try {
			// exclude first cached value
			ensemblAnnotator.setCache(null);

			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			ensemblAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, ensemblAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			ensemblAnnotator.setWebPageDownloader(downloader);
			ensemblAnnotator.setCache(originalCache);
		}
	}

	@Test
	public void testStatusChanged() throws Exception {
		WebPageDownloader downloader = ensemblAnnotator.getWebPageDownloader();
		GeneralCacheInterface originalCache = ensemblAnnotator.getCache();
		try {
			// exclude first cached value
			ensemblAnnotator.setCache(null);

			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			String dataXml = "<opt></opt>";
			String versionXml = "<opt><data release=\"" + EnsemblAnnotator.SUPPORTED_VERSION + "\"/></opt>";
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn(dataXml).thenReturn(versionXml);
			ensemblAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, ensemblAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			ensemblAnnotator.setWebPageDownloader(downloader);
			ensemblAnnotator.setCache(originalCache);
		}
	}

	@Test
	public void testStatusChanged2() throws Exception {
		WebPageDownloader downloader = ensemblAnnotator.getWebPageDownloader();
		GeneralCacheInterface originalCache = ensemblAnnotator.getCache();
		try {
			// exclude first cached value
			ensemblAnnotator.setCache(null);

			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			String dataXml = "<opt></opt>";
			String versionXml = "<opt><data release=\"blablabla\"/></opt>";
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn(dataXml).thenReturn(versionXml);
			ensemblAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.CHANGED, ensemblAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			ensemblAnnotator.setWebPageDownloader(downloader);
			ensemblAnnotator.setCache(originalCache);
		}
	}

	@Test
	public void testEnsemblToEntrez() throws Exception {
		try {
			MiriamData ensembl = new MiriamData(MiriamType.ENSEMBL, "ENSG00000154930");
			MiriamData entrez = ensemblAnnotator.ensemblIdToEntrezId(ensembl);
			assertEquals(new MiriamData(MiriamType.ENTREZ, "84532", EnsemblAnnotator.class), entrez);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidEnsemblToEntrez() throws Exception {
		try {
			MiriamData ensembl = new MiriamData(MiriamType.WIKIPEDIA, "ENSG00000154930");
			ensemblAnnotator.ensemblIdToEntrezId(ensembl);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEnsemblToEntrez2() throws Exception {
		try {
			MiriamData ensembl = new MiriamData(MiriamType.ENSEMBL, "ENSRNOG00000050619");
			MiriamData entrez = ensemblAnnotator.ensemblIdToEntrezId(ensembl);
			assertNull(entrez);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAnnotateWhenEntrezReturnWrongResponse() throws Exception {
		WebPageDownloader downloader = ensemblAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new WrongResponseCodeIOException(null, 0));
			ensemblAnnotator.setWebPageDownloader(mockDownloader);
			Species proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(new MiriamData(MiriamType.ENSEMBL, "1234"));
			ensemblAnnotator.annotateElement(proteinAlias);
			assertEquals(1, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			ensemblAnnotator.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testRefreshCacheQueryNotAvailable() throws Exception {
		WebPageDownloader downloader = ensemblAnnotator.getWebPageDownloader();
		GeneralCacheInterface originalCache = ensemblAnnotator.getCache();
		try {
			// exclude first cached value
			ensemblAnnotator.setCache(null);

			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			ensemblAnnotator.setWebPageDownloader(mockDownloader);
			ensemblAnnotator.refreshCacheQuery("http://google.pl/");
			fail("Exception expected");
		} catch (SourceNotAvailable e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			ensemblAnnotator.setWebPageDownloader(downloader);
			ensemblAnnotator.setCache(originalCache);
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			ensemblAnnotator.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			ensemblAnnotator.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
