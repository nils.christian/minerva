package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;

public class DrugbankHTMLParserTest extends AnnotationTestFunctions {
  Logger logger = Logger.getLogger(DrugbankHTMLParserTest.class);

  @Autowired
  private DrugbankHTMLParser drugBankHTMLParser;

  @Autowired
  private GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
    drugBankHTMLParser.setCache(new GeneralCacheWithExclusion(cache, 1));
  }

  @After
  public void tearDown() throws Exception {
    drugBankHTMLParser.setCache(cache);
  }

  @Test
  public void test1FindDrug() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("Urokinase");
      assertNotNull(test);
      assertEquals(MiriamType.DRUGBANK, test.getSources().get(0).getDataType());
      assertEquals("DB00013", test.getSources().get(0).getResource());
      assertEquals("Urokinase", test.getName());
      assertTrue(test.getBloodBrainBarrier().equalsIgnoreCase("N/A"));
      boolean res = test.getDescription().contains(
          "Low molecular weight form of human urokinase, that consists of an A chain of 2,000 daltons linked by a sulfhydryl bond to a B chain of 30,400 daltons. Recombinant urokinase plasminogen activator");
      assertTrue(res);
      assertEquals(10, test.getTargets().size());
      assertNull(test.getApproved());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void test2FindDrug() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("diazoxide");
      assertNotNull(test);
      assertEquals("Diazoxide", test.getName());
      assertEquals("DB01119", test.getSources().get(0).getResource());
      assertTrue(test.getBloodBrainBarrier().equalsIgnoreCase("YES"));
      boolean res = test.getDescription().contains(
          "A benzothiadiazine derivative that is a peripheral vasodilator used for hypertensive emergencies. It lacks diuretic effect, apparently because it lacks a sulfonamide group. [PubChem]");
      assertTrue(res);
      assertEquals(6, test.getTargets().size());
      assertTrue(test.getApproved());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindRapamycin() throws Exception {
    try {
      // finding synonym
      Drug rapamycinDrug = drugBankHTMLParser.findDrug("Rapamycin");
      assertNotNull(rapamycinDrug);
      assertEquals("Sirolimus", rapamycinDrug.getName());
      assertEquals("DB00877", rapamycinDrug.getSources().get(0).getResource());
      assertTrue(rapamycinDrug.getBloodBrainBarrier().equalsIgnoreCase("NO"));
      boolean res = rapamycinDrug.getDescription().contains("A macrolide compound obtained from Streptomyces");
      assertTrue(res);
      assertEquals(3, rapamycinDrug.getTargets().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindAfegostat() throws Exception {
    try {
      // finding synonym
      Drug rapamycinDrug = drugBankHTMLParser.findDrug("Afegostat");
      assertNotNull(rapamycinDrug);
      assertFalse(rapamycinDrug.getApproved());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void test4FindDrug() throws Exception {

    try {
      Drug test = drugBankHTMLParser.findDrug("Methylamine");
      assertNotNull(test);
      assertEquals("Methylamine", test.getName());
      assertEquals("DB01828", test.getSources().get(0).getResource());
      assertEquals(null, test.getDescription());
      assertEquals(1, test.getTargets().size());

      List<Target> tmp;
      tmp = test.getTargets();
      for (Target a : tmp) {
        assertEquals("Ammonia channel", a.getName());
        assertEquals(2, a.getReferences().size());
        assertEquals(1, a.getGenes().size());
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void test5FindDrug() throws Exception {
    try {
      Drug amantadineDrug = drugBankHTMLParser.findDrug("Amantadine");
      assertNotNull(amantadineDrug);
      assertEquals("Amantadine", amantadineDrug.getName());
      assertEquals("DB00915", amantadineDrug.getSources().get(0).getResource());
      boolean res = amantadineDrug.getDescription().contains(
          "An antiviral that is used in the prophylactic or symptomatic treatment of influenza A. It is also used as an antiparkinsonian agent, to treat extrapyramidal reactions, and for postherpetic neuralgia. The mechanisms of its effects in movement disorders are not well understood but probably reflect an increase in synthesis and release of dopamine, with perhaps some inhibition of dopamine uptake. [PubChem]");
      assertTrue(res);
      assertEquals(3, amantadineDrug.getTargets().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void test6FindDrug() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("qwertyuiop");
      assertEquals(null, test);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void test7FindDrug() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("Aluminum hydroxide");
      assertNotNull(test);
      assertEquals("Aluminum hydroxide", test.getName());
      assertEquals("DB06723", test.getSources().get(0).getResource());
      assertTrue(test.getDescription().contains(
          "Aluminum hydroxide is an inorganic salt used as an antacid. It is a basic compound that acts by neutralizing hydrochloric acid in gastric secretions. Subsequent increases in pH may inhibit the action of pepsin. An increase in bicarbonate ions and prostaglandins may also confer cytoprotective effects."));
      assertEquals(0, test.getTargets().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugAmantadineWithBrandNames() throws Exception {
    try {
      Drug drug = drugBankHTMLParser.findDrug("amantadine");

      assertNotNull(drug);
      assertTrue(drug.getBrandNames().contains("PK-Merz"));
      assertTrue(drug.getBrandNames().contains("Symadine"));
      assertTrue(drug.getBrandNames().contains("Symmetrel"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugSelegiline() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("Selegiline");
      assertNotNull(test);
      assertEquals(2, test.getTargets().size());
      for (String string : new String[] { "MAOA", "MAOB" }) {
        boolean contain = false;
        for (Target target : test.getTargets()) {
          for (MiriamData md : target.getGenes()) {
            if (md.getResource().equals(string)) {
              contain = true;
            }
          }
        }
        assertTrue("Missing traget: " + string, contain);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugDaidzinWithHtmlCharacters() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("Daidzin");
      assertNotNull(test);
      for (String str : test.getSynonyms()) {
        assertFalse(str.contains("&#39;"));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugGliclazideWithHtmlTags() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("Gliclazide");
      assertNotNull(test);
      assertFalse(test.getDescription().contains("<span"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugSevofluraneWithHtmlTags() throws Exception {
    try {
      Drug test = drugBankHTMLParser.findDrug("sevoflurane");
      assertNotNull(test);
      for (String str : test.getSynonyms()) {
        assertFalse(str.contains("<div"));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    try {
      List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB2"));
      assertNotNull(drugs);
      assertTrue("Only " + drugs.size() + " drugs were found, but at least 8 were expected", drugs.size() >= 8);

      drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB2"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByTFTarget() throws Exception {
    try {
      List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "TF"));
      assertNotNull(drugs);
      for (Drug drug : drugs) {
        assertFalse(drug.getName().equalsIgnoreCase("Iron saccharate"));
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindAluminiumByName() throws Exception {
    try {
      Drug drug = drugBankHTMLParser.findDrug("Aluminium");
      assertTrue(drug.getName().equalsIgnoreCase("Aluminium"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindBIIB054ByName() throws Exception {
    try {
      Drug drug = drugBankHTMLParser.findDrug("BIIB054");
      if (drug != null) {
        // check if are not pointint to BIIB021
        assertFalse(drug.getSources().get(0).getResource().equalsIgnoreCase("DB12359"));
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredOutByOrganism() throws Exception {
    try {
      List<MiriamData> organisms = new ArrayList<>();
      organisms.add(new MiriamData(MiriamType.TAXONOMY, "-1"));
      List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB2"),
          organisms);
      assertNotNull(drugs);
      assertEquals("No drugs for this organisms should be found", 0, drugs.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredByOrganism() throws Exception {
    try {
      List<MiriamData> organisms = new ArrayList<>();
      organisms.add(TaxonomyBackend.HUMAN_TAXONOMY);
      List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB2"),
          organisms);
      assertNotNull(drugs);
      assertTrue(drugs.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByInvalidTarget() throws Exception {
    try {
      drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.WIKIPEDIA, "h2o"));
      fail("Exception");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("type is accepted"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByEmptyTarget() throws Exception {
    try {
      List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(null);
      assertEquals(0, drugs.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testWrongHgncTarget() throws Exception {
    try {

      List<Drug> drugs = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "BECN1"));
      assertNotNull(drugs);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugByHgncTargets() throws Exception {
    try {
      List<Drug> drug1 = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB1"));
      List<Drug> drug2 = drugBankHTMLParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB2"));
      int size = drug1.size() + drug2.size();

      List<MiriamData> list = new ArrayList<MiriamData>();
      list.add(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB2"));
      list.add(new MiriamData(MiriamType.HGNC_SYMBOL, "NFKB1"));

      List<Drug> drugs = drugBankHTMLParser.getDrugListByTargets(list);
      assertNotNull(drugs);
      assertTrue("Merged list should be shorter then sum of the two lists", drugs.size() < size);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test(timeout = 15000)
  public void testCachableInterfaceInvalidateUrl() throws Exception {
    String query = "http://google.lu/";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, drugBankHTMLParser.getCacheType(), newRes);
      cache.invalidateByQuery(query, drugBankHTMLParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, drugBankHTMLParser.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  // wait max 15 second
  @Test(timeout = 150000)
  public void testCachableInterfaceInvalidateDrug() throws Exception {

    String query = "drug:amantadine";
    String newRes = "hello";
    try {
      waitForRefreshCacheQueueToEmpty();

      cache.setCachedQuery(query, drugBankHTMLParser.getCacheType(), newRes);
      cache.invalidateByQuery(query, drugBankHTMLParser.getCacheType());

      waitForRefreshCacheQueueToEmpty();

      String res = cache.getStringByQuery(query, drugBankHTMLParser.getCacheType());

      assertNotNull(res);

      assertFalse("Value wasn't refreshed from db", newRes.equals(res));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testByTargetsForCCNB1() throws Exception {
    try {
      List<MiriamData> targets = new ArrayList<>();
      MiriamData miriamTarget = new MiriamData(MiriamType.HGNC_SYMBOL, "CCNB1");
      targets.add(miriamTarget);
      List<Drug> drugs = drugBankHTMLParser.getDrugListByTargets(targets);
      for (Drug drug : drugs) {
        boolean found = false;
        for (Target target : drug.getTargets()) {
          for (MiriamData md : target.getGenes()) {
            if (miriamTarget.equals(md)) {
              found = true;
            }
          }
        }
        assertTrue("Drug " + drug.getName() + " doesn't contain expected target", found);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testFindIbuprofen() throws Exception {
    try {
      Drug drug = drugBankHTMLParser.findDrug("Ibuprofen");
      assertNotNull(drug);
      assertEquals("Ibuprofen", drug.getName());
      assertEquals("DB01050", drug.getSources().get(0).getResource());
      assertNotNull(drug.getDescription());
      assertTrue(drug.getTargets().size() >= 8);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindIronSaccharate() throws Exception {
    try {
      Drug drug = drugBankHTMLParser.findDrug("Iron saccharate");
      assertNotNull(drug);
      MiriamData tfProtein = new MiriamData(MiriamType.HGNC_SYMBOL, "TF");
      for (Target target : drug.getTargets()) {
        for (MiriamData md : target.getGenes()) {
          assertFalse("TF is a carrier, not a target", md.equals(tfProtein));
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDopamine() throws Exception {

    try {
      Drug test = drugBankHTMLParser.findDrug("Dopamine");
      assertNotNull(test);
      for (Target target : test.getTargets()) {
        assertFalse(target.getName().contains("Details"));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindGadobutrol() throws Exception {
    try {
      Drug gadobutrolDrug = drugBankHTMLParser.findDrug("Gadobutrol");
      assertNotNull(gadobutrolDrug);
      assertTrue(gadobutrolDrug.getBloodBrainBarrier().equalsIgnoreCase("NO"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testFindDrugWithSpecialCharacters() throws Exception {

    try {
      Drug test = drugBankHTMLParser.findDrug("Guanosine-5’-Diphosphate");
      assertNotNull(test);
      assertFalse(test.getName().contains("&"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery() throws Exception {
    try {
      drugBankHTMLParser.refreshCacheQuery("invalid_query");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshInvalidCacheQuery2() throws Exception {
    try {
      drugBankHTMLParser.refreshCacheQuery(new Object());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Don't know what to do"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      drugBankHTMLParser.refreshCacheQuery("http://google.pl/");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testRefreshCacheQueryWhenDbNotAvailable() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    GeneralCacheInterface cache = drugBankHTMLParser.getCache();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      drugBankHTMLParser.setCache(null);

      drugBankHTMLParser.refreshCacheQuery(DrugbankHTMLParser.DRUG_NAME_PREFIX + "aspirin");
      fail("Exception expected");
    } catch (SourceNotAvailable e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
      drugBankHTMLParser.setCache(cache);
    }
  }

  @Test
  public void testGetPubmedFromRef() throws Exception {
    try {
      String descriptionString = "<a href=\"/pubmed/1234\">link to 1234</a>";
      List<MiriamData> result = drugBankHTMLParser.getPubmedFromRef(descriptionString);
      assertEquals(1, result.size());
      assertEquals(new MiriamData(MiriamType.PUBMED, "1234"), result.get(0));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetPubmedFromRef2() throws Exception {
    try {
      String descriptionString = "<a href=\"/pubmed/12345678901234_\">link to 123456789</a>";
      List<MiriamData> result = drugBankHTMLParser.getPubmedFromRef(descriptionString);
      assertEquals(1, result.size());
      assertEquals(new MiriamData(MiriamType.PUBMED, "123456789"), result.get(0));
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetPubmedFromRef3() throws Exception {
    try {
      String descriptionString = "<a href=\"/pubmed/invalid\">link to invalid</a>";
      List<MiriamData> result = drugBankHTMLParser.getPubmedFromRef(descriptionString);
      assertEquals(0, result.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetDescription() throws Exception {
    try {
      String descriptionString = "untagged description";
      assertNull(drugBankHTMLParser.getDescriptionForDrug(descriptionString));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetTargets() throws Exception {
    try {
      String descriptionString = "invalid content";
      List<Target> targets = drugBankHTMLParser.getTargetsForDrug(descriptionString);
      assertEquals(0, targets.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStatus() throws Exception {
    try {
      assertEquals(ExternalServiceStatusType.OK, drugBankHTMLParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.DOWN, drugBankHTMLParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader downloader = drugBankHTMLParser.getWebPageDownloader();
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn("");
      drugBankHTMLParser.setWebPageDownloader(mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, drugBankHTMLParser.getServiceStatus().getStatus());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      drugBankHTMLParser.setWebPageDownloader(downloader);
    }
  }

  @Test
  public void testGetBBB() throws Exception {
    try {
      assertTrue(
          drugBankHTMLParser.getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td><td>-</td><td>0.7581</td></tr>")
              .equalsIgnoreCase("NO"));
      assertTrue(
          drugBankHTMLParser.getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td><td>+</td><td>0.7581</td></tr>")
              .equalsIgnoreCase("YES"));
      assertTrue(drugBankHTMLParser
          .getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td><td>don't know</td><td>0.7581</td></tr>")
          .equalsIgnoreCase("N/A"));
      assertTrue(drugBankHTMLParser.getBloodBrainBarrier("invalid str").equalsIgnoreCase("N/A"));
      assertTrue(drugBankHTMLParser.getBloodBrainBarrier("<tr><td>Blood Brain Barrier</td>invalid str")
          .equalsIgnoreCase("N/A"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() throws Exception {
    DrugbankHTMLParser parser = new DrugbankHTMLParser();
    UniprotAnnotator uniprotAnnotator = new UniprotAnnotator();
    parser.setUniprotAnnotator(uniprotAnnotator);
    assertEquals(uniprotAnnotator, parser.getUniprotAnnotator());
  }

  @Test
  public void parseTarget() throws Exception {
    String content = super.readFile("testFiles/drugbank/target-html-part.html");
    Target target = drugBankHTMLParser.parseTarget(content);
    assertNotNull(target);
    assertEquals(6, target.getReferences().size());
    assertEquals("Matrix protein 2", target.getName());
  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    try {
      Project project = new Project();

      List<String> result = drugBankHTMLParser.getSuggestedQueryList(project, TaxonomyBackend.HUMAN_TAXONOMY);

      assertEquals(0, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    try {
      Project project = new Project();
      Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
      project.addModel(model);

      List<String> result = drugBankHTMLParser.getSuggestedQueryList(project, TaxonomyBackend.HUMAN_TAXONOMY);

      assertTrue(result.size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetSuggestedQueryListForUnknownOrganism() throws Exception {
    try {
      Project project = new Project();
      Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
      project.addModel(model);

      List<String> result = drugBankHTMLParser.getSuggestedQueryList(project, null);

      assertEquals(0, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

}
