package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheWithExclusion;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.EntrezData;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class EntrezAnnotatorTest extends AnnotationTestFunctions {

	@Autowired
	EntrezAnnotator entrezAnnotator;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAnnotationsForEntrezId() throws Exception {
		try {
			MiriamData nsmf = new MiriamData(MiriamType.ENTREZ, "84532");
			GenericProtein proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(nsmf);
			entrezAnnotator.annotateElement(proteinAlias);
			assertNotNull(proteinAlias.getSymbol());
			assertNotNull(proteinAlias.getFullName());
			assertNotNull(proteinAlias.getNotes());
			assertFalse(proteinAlias.getNotes().isEmpty());
			assertTrue(proteinAlias.getMiriamData().size() > 1);
			assertTrue(proteinAlias.getSynonyms().size() > 0);

			boolean ensemble = false;
			boolean hgncId = false;
			boolean entrez = false;
			for (MiriamData md : proteinAlias.getMiriamData()) {
				if (MiriamType.ENSEMBL.equals(md.getDataType())) {
					ensemble = true;
				} else if (MiriamType.HGNC.equals(md.getDataType())) {
					hgncId = true;
				} else if (MiriamType.ENTREZ.equals(md.getDataType())) {
					entrez = true;
				}
			}

			assertTrue("Ensemble symbol cannot be found", ensemble);
			assertTrue("Hgnc id cannot be found", hgncId);
			assertTrue("Entrez cannot be found", entrez);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testGetAnnotationsForInvalid() throws Exception {
		try {
			MiriamData nsmf = new MiriamData(MiriamType.ENTREZ, "blabla");
			GenericProtein proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(nsmf);
			entrezAnnotator.annotateElement(proteinAlias);

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testCachableInterfaceInvalidate() throws Exception {
		String query = "http://google.pl/";
		try {
			String newRes = "hello";

			waitForRefreshCacheQueueToEmpty();

			cache.setCachedQuery(query, entrezAnnotator.getCacheType(), newRes);
			String res = cache.getStringByQuery(query, entrezAnnotator.getCacheType());
			assertEquals(newRes, res);
			cache.invalidateByQuery(query, entrezAnnotator.getCacheType());

			waitForRefreshCacheQueueToEmpty();

			res = cache.getStringByQuery(query, entrezAnnotator.getCacheType());

			assertNotNull(res);

			assertFalse("Value wasn't refreshed from db", newRes.equals(res));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testStatus() throws Exception {
		try {
			assertEquals(ExternalServiceStatusType.OK, entrezAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimulateDownStatus() throws Exception {
		WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
		try {
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			entrezAnnotator.setWebPageDownloader(mockDownloader);
			assertEquals(ExternalServiceStatusType.DOWN, entrezAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setWebPageDownloader(downloader);
		}
	}

	@Test
	public void testSimulateChangedStatus() throws Exception {
		XmlSerializer<EntrezData> entrezSerializer = entrezAnnotator.getEntrezSerializer();
		try {
			@SuppressWarnings("unchecked")
			XmlSerializer<EntrezData> mockSerializer = Mockito.mock(XmlSerializer.class);
			when(mockSerializer.xmlToObject(any())).thenReturn(new EntrezData());
			entrezAnnotator.setEntrezSerializer(mockSerializer);
			assertEquals(ExternalServiceStatusType.CHANGED, entrezAnnotator.getServiceStatus().getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setEntrezSerializer(entrezSerializer);
		}
	}

	@Test
	public void testGetHgncFromEntrez() throws Exception {
		try {
			assertEquals(new MiriamData(MiriamType.HGNC, "11179", EntrezAnnotator.class), entrezAnnotator.getHgncIdFromEntrez(new MiriamData(MiriamType.ENTREZ, "6647", EntrezAnnotator.class)));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetHgncFromEntrezForInvalidId() throws Exception {
		try {
			assertNull(entrezAnnotator.getHgncIdFromEntrez(new MiriamData(MiriamType.ENTREZ, "664711111")));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetHgncFromEntrezForInvalidId1() throws Exception {
		try {
			entrezAnnotator.getHgncIdFromEntrez(new MiriamData(MiriamType.WIKIPEDIA, "664711111"));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testRefreshToHgncConversion() throws Exception {
		GeneralCacheInterface cache = entrezAnnotator.getCache();
		try {
			entrezAnnotator.setCache(null);
			assertNull(entrezAnnotator.refreshCacheQuery(EntrezAnnotator.CACHE_HGNC_ID_PREFIX + "blablabla"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setCache(cache);
		}
	}

	@Test(timeout = 60000)
	public void testRefreshToHgncConversion2() throws Exception {
		try {
			assertNotNull(entrezAnnotator.refreshCacheQuery(EntrezAnnotator.CACHE_HGNC_ID_PREFIX + "6647"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery() throws Exception {
		try {
			entrezAnnotator.refreshCacheQuery("invalid_query");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshInvalidCacheQuery2() throws Exception {
		try {
			entrezAnnotator.refreshCacheQuery(new Object());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Don't know what to do"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testRefreshEntrezData() throws Exception {
		try {
			assertNotNull(entrezAnnotator.refreshCacheQuery(EntrezAnnotator.ENTREZ_DATA_PREFIX + "6647"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshCacheQueryNotAvailable() throws Exception {
		WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
		GeneralCacheInterface originalCache = entrezAnnotator.getCache();
		try {
			// exclude first cached value
			entrezAnnotator.setCache(null);

			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			entrezAnnotator.setWebPageDownloader(mockDownloader);
			entrezAnnotator.refreshCacheQuery("http://google.pl/");
			fail("Exception expected");
		} catch (SourceNotAvailable e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setWebPageDownloader(downloader);
			entrezAnnotator.setCache(originalCache);
		}
	}

	@Test
	public void testRefreshCacheQueryWithInvalidEntrezServerResponse() throws Exception {
		WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
		GeneralCacheInterface originalCache = entrezAnnotator.getCache();
		try {
			// exclude first cached value
			entrezAnnotator.setCache(null);

			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new IOException());
			entrezAnnotator.setWebPageDownloader(mockDownloader);
			entrezAnnotator.refreshCacheQuery(EntrezAnnotator.ENTREZ_DATA_PREFIX + "6647");
			fail("Exception expected");
		} catch (SourceNotAvailable e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setWebPageDownloader(downloader);
			entrezAnnotator.setCache(originalCache);
		}
	}

	@Test
	public void testAnnotateElementWithoutId() throws Exception {
		try {
			GenericProtein proteinAlias = new GenericProtein("id");
			entrezAnnotator.annotateElement(proteinAlias);

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAnnotateElementWithTwoEntrez() throws Exception {
		try {
			Species proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "6647"));
			proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "6648"));
			entrezAnnotator.annotateElement(proteinAlias);

			assertEquals(1, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetHgncFromEntrezWithoutCache() throws Exception {
		GeneralCacheInterface cache = entrezAnnotator.getCache();
		try {
			entrezAnnotator.setCache(new GeneralCacheWithExclusion(cache, 1));
			assertNotNull(entrezAnnotator.getHgncIdFromEntrez(new MiriamData(MiriamType.ENTREZ, "6647")));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setCache(cache);
		}
	}

	@Test
	public void testParseEntrezResponse() throws Exception {
		WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
		GeneralCacheInterface cache = entrezAnnotator.getCache();
		try {
			entrezAnnotator.setCache(null);
			String response = super.readFile("testFiles/annotation/entrezResponse.xml");
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn(response);
			entrezAnnotator.setWebPageDownloader(mockDownloader);
			EntrezData data = entrezAnnotator.getEntrezForMiriamData(new MiriamData(), "");
			boolean ensembl = false;
			boolean hgnc = false;
			for (MiriamData md : data.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.HGNC)) {
					hgnc = true;
				}
				if (md.getDataType().equals(MiriamType.ENSEMBL)) {
					ensembl = true;
				}
			}
			assertTrue(ensembl);
			assertTrue(hgnc);
			assertNotNull(data.getDescription());
			assertFalse(data.getDescription().isEmpty());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setWebPageDownloader(downloader);
			entrezAnnotator.setCache(cache);
		}
	}

	@Test
	public void testParseInvalidEntrezResponse() throws Exception {
		WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
		GeneralCacheInterface cache = entrezAnnotator.getCache();
		try {
			entrezAnnotator.setCache(null);
			String response = "";
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenReturn(response);
			entrezAnnotator.setWebPageDownloader(mockDownloader);
			entrezAnnotator.getEntrezForMiriamData(new MiriamData(), "");
			fail("Exception expected");
		} catch (AnnotatorException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setWebPageDownloader(downloader);
			entrezAnnotator.setCache(cache);
		}

	}

	@Test
	public void testParseInvalidEntrezResponse2() throws Exception {
		WebPageDownloader downloader = entrezAnnotator.getWebPageDownloader();
		GeneralCacheInterface cache = entrezAnnotator.getCache();
		try {
			entrezAnnotator.setCache(null);
			WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
			when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString())).thenThrow(new WrongResponseCodeIOException(null, 404));
			entrezAnnotator.setWebPageDownloader(mockDownloader);
			entrezAnnotator.getEntrezForMiriamData(new MiriamData(), "");
			fail("Exception expected");
		} catch (AnnotatorException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			entrezAnnotator.setWebPageDownloader(downloader);
			entrezAnnotator.setCache(cache);
		}

	}

}
