package lcsb.mapviewer.annotation;

import lcsb.mapviewer.annotation.cache.AllCacheTests;
import lcsb.mapviewer.annotation.data.AllDataTests;
import lcsb.mapviewer.annotation.services.AllServicesTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AllCacheTests.class, //
		AllDataTests.class, //
		AllServicesTests.class, //
})

public class AllAnnotationTests {
}
