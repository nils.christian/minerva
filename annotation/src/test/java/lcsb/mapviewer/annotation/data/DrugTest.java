package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class DrugTest extends AnnotationTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Drug());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Drug d = new Drug();
			d.setDescription("x");
			Drug copy = new Drug(d);
			assertEquals("x", copy.getDescription());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			Drug d = new Drug(null);
			assertNull(d.getName());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToString() {
		try {
			Drug d = new Drug();
			d.addSynonym("Synonym");
			d.addBrandName("bname");
			d.addSource(new MiriamData(MiriamType.DRUGBANK, "xxx"));
			Target target = new Target();
			target.setSource(new MiriamData());
			target.addReference(TaxonomyBackend.HUMAN_TAXONOMY);
			d.addTarget(target);

			assertTrue(d.toString().contains("Synonym"));
			assertFalse(d.toString().contains("bname"));
			assertTrue(d.toString().contains("xxx"));
			assertTrue(d.toString().contains(TaxonomyBackend.HUMAN_TAXONOMY.getResource()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddSynonym() {
		try {
			Drug d = new Drug();
			d.addSynonym("Synonym");
			d.addSynonym("Synonym");

			assertEquals(1, d.getSynonyms().size());
			assertEquals(1, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			Drug d = new Drug();
			List<Target> targets = new ArrayList<>();
			List<MiriamData> sources = new ArrayList<>();
			d.setTargets(targets);
			d.setSources(sources);

			assertEquals(targets, d.getTargets());
			assertEquals(sources, d.getSources());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
