package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ebi.chebi.webapps.chebiWS.model.Entity;
import uk.ac.ebi.chebi.webapps.chebiWS.model.OntologyDataItem;

public class ChebiTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Chebi());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			List<ChebiRelation> incomingChebi = new ArrayList<>();
			List<ChebiRelation> outgoingChebi = new ArrayList<>();
			List<String> synonyms = new ArrayList<>();
			Chebi chebi = new Chebi();
			chebi.setIncomingChebi(incomingChebi);
			chebi.setOutgoingChebi(outgoingChebi);
			chebi.setSynonyms(synonyms);

			assertEquals(incomingChebi, chebi.getIncomingChebi());
			assertEquals(outgoingChebi, chebi.getOutgoingChebi());
			assertEquals(synonyms, chebi.getSynonyms());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testConstructor() {
		try {
			Entity entity = new Entity();
			entity.getOntologyChildren().add(new OntologyDataItem());
			entity.setChebiAsciiName("");
			Chebi chebi = new Chebi(entity);
			assertEquals(1, chebi.getOutgoingChebi().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
