package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ArticleCitationComparatorTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCompare() {
		Article a1 = new Article();
		a1.setCitationCount(2);
		Article a2 = new Article();
		a2.setCitationCount(10);
		Article a3 = new Article();
		a3.setCitationCount(5);
		List<Article> list = new ArrayList<>();
		list.add(a1);
		list.add(a2);
		list.add(a3);
		
		list.sort(new ArticleCitationComparator());
		
		assertEquals(a2,list.get(0));
		assertEquals(a3,list.get(1));
		assertEquals(a1,list.get(2));
	}

}
