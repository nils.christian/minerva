package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.annotation.Rollback;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.cache.CacheQueryDao;

@Rollback(true)
public class PermanentDatabaseLevelCacheTest extends AnnotationTestFunctions {

	Logger logger = Logger.getLogger(PermanentDatabaseLevelCacheTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testByQuery() throws Exception {
		try {
			String xml = "<hello/>";
			String query = "blabla";

			String xml2 = "<hello23/>";

			CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

			Node sourceNode = getNodeFromXmlString(xml);
			Node sourceNode2 = getNodeFromXmlString(xml2);
			cache.removeByQuery(query, type);

			Node node = cache.getXmlNodeByQuery(query, type);
			assertNull(node);

			cache.setCachedQuery(query, type, sourceNode);
			node = cache.getXmlNodeByQuery(query, type);
			assertNotNull(node);
			assertEquals("hello", node.getNodeName());

			cache.setCachedQuery(query, type, sourceNode2);
			node = cache.getXmlNodeByQuery(query, type);
			assertNotNull(node);
			assertEquals("hello23", node.getNodeName());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetInvalidQuery() {
		try {
			String query = "blabla";
			Object object = new Object();

			CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

			cache.setCachedQuery(query, type, object);
			fail("Exception expected");

		} catch (CacheException e) {
			assertTrue(e.getMessage().contains("Unknown object type"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetInvalidXmlNode() {
		try {
			String query = "blabla";
			String object = "hello";

			CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

			cache.setCachedQuery(query, type, object);
			assertNull(cache.getXmlNodeByQuery(query, type));

			assertEquals(2, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout = 15000)
	public void testRefresh() throws Exception {
		try {
			waitForRefreshCacheQueueToEmpty();
			CacheType invalidType = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());

			String query = "test query";
			String val = "halo";
			dbUtils.createSessionForCurrentThread();
			cache.setCachedQuery(query, invalidType, val);
			String res = cache.getStringByQuery(query, invalidType);
			dbUtils.closeSessionForCurrentThread();
			assertNotNull(res);
			// for the first time value should be the same (it's not valid but cache
			// wasn't updated)
			assertEquals(val, res);
			// sleep a bit to wait for the second thread
			waitForRefreshCacheQueueToEmpty();
			// for the second time it should change
			dbUtils.createSessionForCurrentThread();
			res = cache.getStringByQuery(query, invalidType);
			dbUtils.closeSessionForCurrentThread();
			assertNotNull(res);
			assertFalse(val.equals(res));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRefreshNode() throws Exception {
		try {
			CacheType invalidType = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());

			String query = "test query";
			String val = "<node><subnode>halo</subnode></node>";
			Node node = getNodeFromXmlString(val);
			dbUtils.createSessionForCurrentThread();
			cache.setCachedQuery(query, invalidType, node);
			Node res = cache.getXmlNodeByQuery(query, invalidType);
			dbUtils.closeSessionForCurrentThread();

			assertNotNull(res);
			// sleep a bit to wait for the second thread
			int counter = dbUtils.getSessionCounter();
			// wait until the other sql session finish
			do {
				Thread.sleep(100);
				counter = dbUtils.getSessionCounter();
			} while (counter > 0);
			Thread.sleep(1000);
			// for the second time it should change
			dbUtils.createSessionForCurrentThread();
			Node res2 = cache.getXmlNodeByQuery(query, invalidType);
			dbUtils.closeSessionForCurrentThread();
			assertNotNull(res2);

			String strRes = nodeToString(res);
			String strRes2 = nodeToString(res2);
			assertFalse(strRes.equals(strRes2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidateWhenDbConnectionIsInterrupted() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType invalidType = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());
			String query = "test query";
			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate db connection hanging
			when(mockDao.getByQuery(query, invalidType)).then(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					Thread.sleep(100);
					return null;
				}
			});
			cache.setCacheQueryDao(mockDao);
			// run invalidation in separate thread
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					cache.invalidateByQuery(query, invalidType);
				}
			});

			t.start();
			// interrupt thread
			t.interrupt();
			t.join();

			// check if we have information in the logs about interruption (this is
			// not normal situaion)
			assertEquals(1, getErrors().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test
	public void testInvalidateWhenDbConnectionCrash() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType invalidType = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());
			String query = "test query";
			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate db crash
			when(mockDao.getByQuery(query, invalidType)).thenThrow(new InvalidStateException());
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, invalidType);

			// check if we have information in the logs about interruption (this is
			// not normal situaion)
			assertEquals(1, getErrors().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test
	public void testClearCache() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);

			MutableBoolean cleared = new MutableBoolean(false);

			// simulate db crash
			Mockito.doAnswer(new Answer<Void>() {

				@Override
				public Void answer(InvocationOnMock invocation) throws Throwable {
					cleared.setValue(true);
					return null;
				}
			}).when(mockDao).clearTable();
			cache.setCacheQueryDao(mockDao);

			cache.clearCache();

			assertTrue(cleared.getValue());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test(timeout = 15000)
	public void testRefreshInvalidType() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType invalidType = new CacheType();
			String query = "test query";

			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate result that is not valid anymore
			when(mockDao.getByQuery(query, invalidType)).thenAnswer(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					CacheQuery res = new CacheQuery();
					Calendar expired = Calendar.getInstance();
					expired.add(Calendar.DATE, -1);
					res.setExpires(expired);
					return res;
				}
			});
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, invalidType);

			while (cache.getRefreshExecutingTasksSize() + cache.getRefreshPendingQueueSize() > 0) {
				Thread.sleep(10);
			}
			assertEquals(1, getFatals().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test(timeout = 15000)
	public void testRefreshInvalidType2() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType invalidType = new CacheType();
			invalidType.setClassName(String.class.getName());
			String query = "test query";

			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate result that is not valid anymore
			when(mockDao.getByQuery(query, invalidType)).thenAnswer(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					CacheQuery res = new CacheQuery();
					Calendar expired = Calendar.getInstance();
					expired.add(Calendar.DATE, -1);
					res.setExpires(expired);
					return res;
				}
			});
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, invalidType);

			while (cache.getRefreshExecutingTasksSize() + cache.getRefreshPendingQueueSize() > 0) {
				Thread.sleep(10);
			}
			assertEquals(1, getFatals().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test(timeout = 15000)
	public void testRefreshInvalidQuery() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());
			String query = "invalid test query";

			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate result that is not valid anymore
			when(mockDao.getByQuery(query, type)).thenAnswer(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					CacheQuery res = new CacheQuery();
					Calendar expired = Calendar.getInstance();
					expired.add(Calendar.DATE, -1);
					res.setExpires(expired);
					return res;
				}
			});
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, type);

			while (cache.getRefreshExecutingTasksSize() + cache.getRefreshPendingQueueSize() > 0) {
				Thread.sleep(10);
			}
			assertEquals(1, getErrors().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test(timeout = 15000)
	public void testRefreshQueryThatResultsWithNull() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType type = cacheTypeDao.getByClassName(ChebiAnnotator.class.getCanonicalName());
			String query = ChebiAnnotator.ID_PREFIX + "1234";

			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate result that is not valid anymore
			when(mockDao.getByQuery(query, type)).thenAnswer(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					CacheQuery res = new CacheQuery();
					Calendar expired = Calendar.getInstance();
					expired.add(Calendar.DATE, -1);
					res.setExpires(expired);
					return res;
				}
			});
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, type);

			while (cache.getRefreshExecutingTasksSize() + cache.getRefreshPendingQueueSize() > 0) {
				Thread.sleep(10);
			}
			assertEquals(0, getErrors().size());
			assertEquals(0, getFatals().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test(timeout = 15000)
	public void testRefreshQueryWithExceptionThrown() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			waitForRefreshCacheQueueToEmpty();
			
			CacheType type = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());
			String query = ChebiAnnotator.ID_PREFIX + "1234";
			
			MockCacheInterface.exceptionToThrow = new RuntimeException();

			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate result that is not valid anymore
			when(mockDao.getByQuery(query, type)).thenAnswer(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					CacheQuery res = new CacheQuery();
					Calendar expired = Calendar.getInstance();
					expired.add(Calendar.DATE, -1);
					res.setExpires(expired);
					return res;
				}
			});
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, type);

			waitForRefreshCacheQueueToEmpty();
			
			assertEquals(1, getErrors().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			MockCacheInterface.exceptionToThrow = null;
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

	@Test(timeout = 15000)
	public void testRefreshQueryWithErrorThrown() throws Exception {
		CacheQueryDao cacheQueryDao = cache.getCacheQueryDao();
		try {
			CacheType type = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());
			String query = ChebiAnnotator.ID_PREFIX + "1234";
			
			MockCacheInterface.errorToThrow = new Error();

			CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
			// simulate result that is not valid anymore
			when(mockDao.getByQuery(query, type)).thenAnswer(new Answer<CacheQuery>() {
				@Override
				public CacheQuery answer(InvocationOnMock invocation) throws Throwable {
					CacheQuery res = new CacheQuery();
					Calendar expired = Calendar.getInstance();
					expired.add(Calendar.DATE, -1);
					res.setExpires(expired);
					return res;
				}
			});
			cache.setCacheQueryDao(mockDao);

			cache.invalidateByQuery(query, type);

			while (cache.getRefreshExecutingTasksSize() + cache.getRefreshPendingQueueSize() > 0) {
				Thread.sleep(10);
			}
			//errors are not caught
			assertEquals(0, getErrors().size());
			assertEquals(0, getFatals().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			MockCacheInterface.errorToThrow = null;
			cache.setCacheQueryDao(cacheQueryDao);
		}
	}

}
