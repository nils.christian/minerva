package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;

public class GeneralCacheTest extends AnnotationTestFunctions {
	Logger															 logger	= Logger.getLogger(GeneralCacheTest.class);

	GeneralCache												 cache	= new GeneralCache();

	@Autowired
	PermanentDatabaseLevelCacheInterface databaseLevelCache;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		cache.setCache2(databaseLevelCache);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testClear() {
		QueryCacheInterface origianlCache = cache.getCache2();
		try {
			CacheType type = new CacheType();
			type.setId(-1);
			cache.setCache2(ApplicationLevelCache.getInstance());
			ApplicationLevelCache.getInstance().setCachedQuery("str", type, "val");
			assertNotNull(cache.getStringByQuery("str", type));
			cache.clearCache();
			assertNull(cache.getStringByQuery("str", type));
		} finally {
			cache.setCache2(origianlCache);
		}
	}

	@Test
	public void testGetXmlNodeByQueryForInvalidType() {
		try {
			cache.getXmlNodeByQuery("str", null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Cache type cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetCachedQueryForInvalidType() {
		try {
			cache.setCachedQuery("str", null, null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Cache type cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRemoveByQueryForInvalidType() {
		try {
			cache.removeByQuery("str", null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Cache type cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidateByQueryForInvalidType() {
		try {
			cache.invalidateByQuery("str", null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Cache type cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void testGetStringByQueryForInvalidType() {
		try {
			cache.getStringByQuery("str", null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Cache type cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	

	@Test
	public void testRemoveByQuery() {
		try {
			CacheType type = new CacheType();
			cache.setCachedQuery("str", type, "val");
			assertNotNull(cache.getStringByQuery("str", type));
			cache.removeByQuery("str", type);
			assertNull(cache.getStringByQuery("str", type));
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Cache type cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
