package lcsb.mapviewer.annotation.cache;

public class CachableInterfaceMock extends CachableInterface {

	public CachableInterfaceMock(Class<? extends CachableInterface> clazz) {
		super(clazz);
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		return null;
	}

}
