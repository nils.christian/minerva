package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;


public class CachableInterfaceTest extends AnnotationTestFunctions {
	Logger logger = Logger.getLogger(CachableInterfaceTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetWebPage() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			String result = ci.getWebPageContent("http://www.drugbank.ca/biodb/polypeptides/P21728");
			assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetWebPage2() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			String result = ci.getWebPageContent("http://www.drugbank.ca/drugs/DB00091");
			assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testGetWebPageGetCache() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			
			CacheTypeDao mock = Mockito.mock(CacheTypeDao.class);
			when(mock.getByClassName(anyString())).thenReturn(new CacheType());
			ci.setCacheTypeDao(mock);
			ci.setCache(new GeneralCache());			
			
			String cacheKey = "https://www.google.com/";
			String result = ci.getWebPageContent(cacheKey);
			assertNotNull(result);
			String cacheValue = ci.getCacheValue(cacheKey); 
			assertTrue(result.equals(cacheValue));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetNode() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			
			CacheTypeDao mock = Mockito.mock(CacheTypeDao.class);
			when(mock.getByClassName(anyString())).thenReturn(new CacheType());
			ci.setCacheTypeDao(mock);
			
			String xml = "<xml><child/></xml>";
			ci.setCache(new GeneralCache());
			ci.setCacheNode("id", super.getNodeFromXmlString(xml));
			
			Node node = ci.getCacheNode("id");
			assertNotNull(node);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNewLinesGetWebPage() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			ci.setCache(null);
			String result = ci.getWebPageContent("http://google.com/");
			assertNotNull(result);
			assertTrue(result.contains("\n"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCleanHtml() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			String res = ci.cleanHtml("blabla <invalid_tag");
			assertEquals("blabla ",res);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCleanHtml2() throws Exception {
		try {
			CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
			String res = ci.cleanHtml("blabla <valid_tag> etc");
			assertEquals("blabla  etc",res);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


}
