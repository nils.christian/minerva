package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.persist.dao.cache.BigFileEntryDao;

public class BigFileCacheTest extends AnnotationTestFunctions {

  Logger logger = Logger.getLogger(BigFileCacheTest.class);

  String ftpUrl = "ftp://ftp.informatik.rwth-aachen.de/pub/Linux/debian-cd/current/i386/bt-cd/MD5SUMS";

  String httpUrl = "https://www.google.pl/humans.txt";

  @Autowired
  BigFileCache bigFileCache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDownloadFile() throws Exception {
    try {
      bigFileCache.downloadFile(ftpUrl, false, null);
      String localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);
      assertTrue(bigFileCache.isCached(ftpUrl));
      assertTrue(bigFileCache.isLocalFileUpToDate(ftpUrl));
      File f = new File(localFile);
      assertTrue(f.exists());
      String content = super.readFile(localFile);
      assertTrue(content.contains("iso"));

      // when we try to download file that is already downloaded there should be
      // a warning
      bigFileCache.downloadFile(ftpUrl, false, null);
      assertEquals(1, getWarnings().size());

      // however when due to some reason the file will be removed from file
      // system
      f.delete();
      // the file should be downloaded again
      bigFileCache.downloadFile(ftpUrl, false, null);
      // additional warning about it
      assertEquals(2, getWarnings().size());

      localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);
      f = new File(localFile);
      assertTrue(f.exists());

      // when we are trying to update file that is up to date then we will have
      // a warning about it (but file shouldn't be changed)
      bigFileCache.updateFile(ftpUrl, false);
      assertEquals(3, getWarnings().size());

      bigFileCache.removeFile(ftpUrl);

      assertNull(bigFileCache.getAbsolutePathForFile(ftpUrl));

      assertFalse(f.exists());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDownloadFileWithUnknownProtocol() throws Exception {
    try {
      bigFileCache.downloadFile("wtf://file.com", false, null);

    } catch (URISyntaxException e) {
      assertTrue(e.getMessage().contains("Unknown protocol for url"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDownloadAsyncFileFromHttp() throws Exception {
    try {
      bigFileCache.downloadFile(httpUrl, true, null);
      waitForDownload();
      String localFile = bigFileCache.getAbsolutePathForFile(httpUrl);
      assertTrue(bigFileCache.isLocalFileUpToDate(httpUrl));
      File f = new File(localFile);
      assertTrue(f.exists());
      String content = super.readFile(localFile);
      assertTrue(content.contains("Google"));
      bigFileCache.removeFile(httpUrl);

      assertNull(bigFileCache.getAbsolutePathForFile(httpUrl));

      assertFalse(f.exists());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSavingToInvalidDirectory() throws Exception {
    try {
      String url = httpUrl + "?invalid";
      Configuration.setWebAppDir("/");
      bigFileCache.downloadFile(url, false, null);
      fail("Exception expected");
    } catch (IOException e) {

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      Configuration.setWebAppDir("./");
    }
  }

  private void waitForDownload() throws InterruptedException {
    while (bigFileCache.getDownloadThreadCount() > 0) {
      logger.debug("Waiting for download to finish");
      Thread.sleep(100);
    }
  }

  @Test
  public void testIsLocalHttpFileUpToDateWhenMissing() throws Exception {
    try {
      bigFileCache.isLocalFileUpToDate(httpUrl + "?some_get_param");
      fail("Exception expected");
    } catch (FileNotFoundException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsLocalHttpFileUpToDateWhenFileRemovedManually() throws Exception {
    try {
      bigFileCache.downloadFile(httpUrl, false, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      });
      new File(bigFileCache.getAbsolutePathForFile(httpUrl)).delete();
      try {
        bigFileCache.isLocalFileUpToDate(httpUrl);
        fail("Exception expected");
      } catch (FileNotFoundException e) {
      }
      bigFileCache.removeFile(httpUrl);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsLocalFtpFileUpToDateWhenMissing() throws Exception {
    try {
      bigFileCache.isLocalFileUpToDate(ftpUrl);
      fail("Exception expected");
    } catch (FileNotFoundException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRefreshCacheQueryWhenFtpConnectionFails() throws Exception {

    try {
      bigFileCache.downloadFile(ftpUrl, false, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      });
      assertTrue(bigFileCache.isLocalFileUpToDate(ftpUrl));

      FTPClient mockClient = Mockito.mock(FTPClient.class);
      Mockito.doAnswer(new Answer<Integer>() {

        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          return FTPReply.REQUEST_DENIED;
        }
      }).when(mockClient).getReplyCode();

      FtpClientFactory factory = Mockito.mock(FtpClientFactory.class);
      when(factory.createFtpClient()).thenReturn(mockClient);

      bigFileCache.setFtpClientFactory(factory);

      bigFileCache.isLocalFileUpToDate(ftpUrl);
      fail("Exception expected");
    } catch (IOException e) {
      assertTrue(e.getMessage().contains("FTP server refused connection"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      bigFileCache.setFtpClientFactory(new FtpClientFactory());
      bigFileCache.removeFile(ftpUrl);
    }
  }

  @Test
  public void testDownloadWhenFtpConnectionFails() throws Exception {
    try {
      FTPClient mockClient = Mockito.mock(FTPClient.class);
      Mockito.doAnswer(new Answer<Integer>() {
        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          return FTPReply.REQUEST_DENIED;
        }
      }).when(mockClient).getReplyCode();

      FtpClientFactory factory = Mockito.mock(FtpClientFactory.class);
      when(factory.createFtpClient()).thenReturn(mockClient);

      bigFileCache.setFtpClientFactory(factory);
      bigFileCache.downloadFile(ftpUrl, false, null);

      fail("Exception expected");
    } catch (IOException e) {
      assertTrue(e.getMessage().contains("FTP server refused connection"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      bigFileCache.setFtpClientFactory(new FtpClientFactory());
      bigFileCache.removeFile(ftpUrl);
    }
  }

  @Test
  public void testDownloadHttpWhenDownloadFails() throws Exception {
    ConfigurationDao configurationDao = bigFileCache.getConfigurationDao();

    try {

      ConfigurationDao mockDao = Mockito.mock(ConfigurationDao.class);
      when(mockDao.getValueByType(any())).thenThrow(new RuntimeException());
      bigFileCache.setConfigurationDao(mockDao);
      bigFileCache.downloadFile(httpUrl, false, null);

      fail("Exception expected");
    } catch (InvalidStateException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      bigFileCache.setConfigurationDao(configurationDao);
    }
  }

  @Test
  public void testDownloadedFileRemovedException() throws Exception {
    try {
      String url = ftpUrl;
      bigFileCache.downloadFile(url, false, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      });
      String localFile = bigFileCache.getAbsolutePathForFile(url);
      File f = new File(localFile);
      assertTrue(f.exists());
      f.delete();

      try {
        bigFileCache.getAbsolutePathForFile(url);
        fail("Exception expected");
      } catch (FileNotFoundException e) {

      }
      try {
        bigFileCache.isLocalFileUpToDate(url);
        fail("Exception expected");
      } catch (FileNotFoundException e) {

      }

      bigFileCache.removeFile(url);

      assertNull(bigFileCache.getAbsolutePathForFile(url));

      assertFalse(f.exists());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetPathForFileBeingDownloaded() throws Exception {
    try {
      String url = ftpUrl;
      BigFileCache bigFileCacheUnderTest = new BigFileCache();
      BigFileEntryDao mockDao = Mockito.mock(BigFileEntryDao.class);
      BigFileEntry entry = new BigFileEntry();
      when(mockDao.getByUrl(anyString())).thenReturn(entry);

      bigFileCacheUnderTest.setBigFileEntryDao(mockDao);
      try {
        bigFileCacheUnderTest.getAbsolutePathForFile(url);
        fail("Exception expected");
      } catch (FileNotFoundException e) {
        assertTrue(e.getMessage().contains("File is not complete"));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateFile() throws Exception {
    try {
      bigFileCache.downloadFile(ftpUrl, false, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
        }
      });
      String localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);
      File f = new File(localFile);
      f.delete();
      PrintWriter out = new PrintWriter(localFile);
      out.println("tesxt");
      out.close();

      String content = super.readFile(localFile);
      assertFalse(content.contains("iso"));

      assertFalse(bigFileCache.isLocalFileUpToDate(ftpUrl));
      bigFileCache.updateFile(ftpUrl, false);

      localFile = bigFileCache.getAbsolutePathForFile(ftpUrl);

      content = super.readFile(localFile);
      assertTrue(content.contains("iso"));

      // now try update asynchronously
      f = new File(localFile);
      f.delete();
      out = new PrintWriter(localFile);
      out.println("tesxt");
      out.close();

      assertFalse(bigFileCache.isLocalFileUpToDate(ftpUrl));
      bigFileCache.updateFile(ftpUrl, true);

      waitForDownload();

      assertTrue(bigFileCache.isLocalFileUpToDate(ftpUrl));

      // check when connection fails
      f = new File(localFile);
      f.delete();
      out = new PrintWriter(localFile);
      out.println("tesxt");
      out.close();

      FTPClient mockClient = Mockito.mock(FTPClient.class);
      Mockito.doAnswer(new Answer<Integer>() {

        // first answer (for checking if the file should be updated)
        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          return FTPReply.COMMAND_OK;
        }
      }).doAnswer(new Answer<Integer>() {
        // second answer (for downloading)
        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          return FTPReply.REQUEST_DENIED;
        }
      }).when(mockClient).getReplyCode();

      Mockito.doAnswer(new Answer<FTPFile[]>() {

        @Override
        public FTPFile[] answer(InvocationOnMock invocation) throws Throwable {
          return new FTPFile[] {};
        }
      }).when(mockClient).listFiles(anyString());

      FtpClientFactory factory = Mockito.mock(FtpClientFactory.class);
      when(factory.createFtpClient()).thenReturn(mockClient);

      bigFileCache.setFtpClientFactory(factory);
      try {
        bigFileCache.updateFile(ftpUrl, false);
        fail("Exception expected");
      } catch (IOException e) {
        assertTrue(e.getMessage().contains("FTP server refused connection"));
      } catch (Exception e) {
        e.printStackTrace();
        throw e;
      } finally {
        bigFileCache.setFtpClientFactory(new FtpClientFactory());
      }

      bigFileCache.removeFile(ftpUrl);

      assertNull(bigFileCache.getAbsolutePathForFile(ftpUrl));

      assertFalse(f.exists());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetRemoteHttpFileSizeForInvalidUrl() throws Exception {
    try {
      bigFileCache.getRemoteHttpFileSize("invalidurl");
      fail("Exception expected");
    } catch (IOException e) {

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveFileThatDoesntExist() throws Exception {
    try {
      bigFileCache.removeFile("unexisting file");
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsLocalFileUpToDateForInvalidURI() throws Exception {
    try {
      bigFileCache.isLocalFileUpToDate("unexistingFileProtocol://bla");
      fail("Exception expected");
    } catch (URISyntaxException e) {
      assertTrue(e.getMessage().contains("Unknown protocol"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetDomainName() throws Exception {
    try {
      String domain = bigFileCache.getDomainName("http://www.eskago.pl/radio/eska-rock");
      assertEquals("eskago.pl", domain);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExecuteProblematicTask() throws Exception {
    try {
      Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
          Thread t = new Thread(r);
          t.setDaemon(true);
          return t;
        }
      }).submit(new Callable<Void>() {
        @Override
        public Void call() throws Exception {
          throw new Exception();
        }
      });
      bigFileCache.executeTask(task);
      fail("Exception expected");
    } catch (InvalidStateException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExecuteProblematicTask2() throws Exception {
    try {
      Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
          Thread t = new Thread(r);
          t.setDaemon(true);
          return t;
        }
      }).submit(new Callable<Void>() {
        @Override
        public Void call() throws Exception {
          throw new URISyntaxException("", "");
        }
      });
      bigFileCache.executeTask(task);
      fail("Exception expected");
    } catch (URISyntaxException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExecuteInterruptedTask() throws Exception {
    try {
      Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
          Thread t = new Thread(r);
          t.setDaemon(true);
          return t;
        }
      }).submit(new Callable<Void>() {
        @Override
        public Void call() throws Exception {
          Thread.sleep(100);
          return null;
        }
      });
      Thread t = new Thread(new Runnable() {
        @Override
        public void run() {
          try {
            bigFileCache.executeTask(task);
          } catch (Exception e) {
            logger.fatal(e, e);
          }
        }
      });

      t.start();
      // interrupt thread
      t.interrupt();
      t.join();

      assertEquals(1, getErrors().size());
      assertEquals(0, getFatals().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
