package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class WebPageDownloaderTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConnectionProblems() {
		WebPageDownloader downloader = new WebPageDownloader() {
			@Override
			public HttpURLConnection openConnection(String url) throws IOException {
				HttpURLConnection result = Mockito.mock(HttpURLConnection.class);
				Mockito.doReturn(HttpURLConnection.HTTP_INTERNAL_ERROR).when(result).getResponseCode();
				Mockito.doThrow(new IOException()).when(result).getInputStream();
				return result;
			}
		};
		try {
			downloader.getFromNetwork("https://www.google.pl/?gws_rd=ssl");
			fail("Exceptio expected");
		} catch (IOException e) {
		}
	}

	@Test
	public void testConnectionProblems2() {
		WebPageDownloader downloader = new WebPageDownloader() {
			@Override
			public HttpURLConnection openConnection(String url) throws IOException {
				HttpURLConnection result = Mockito.mock(HttpURLConnection.class);
				Mockito.doReturn(HttpURLConnection.HTTP_OK).when(result).getResponseCode();
				Mockito.doThrow(new UnknownHostException()).when(result).getInputStream();
				return result;
			}
		};
		try {
			downloader.getFromNetwork("https://www.google.pl/?gws_rd=ssl");
			fail("Exceptio expected");
		} catch (IOException e) {
		}
	}
	
	@Test
	public void testSend1() {
		WebPageDownloader downloader = new WebPageDownloader();
		try {
		String result = downloader.getFromNetwork("https://www.google.com/");
		assertNotNull("GET request to Google should return non-null result", result);
		} catch (IOException e) {
		}
	}
	
	@Test
	public void testPost1() {
		WebPageDownloader downloader = new WebPageDownloader();
		try {
		String result = downloader.getFromNetwork("https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/", "POST", "P29373");
		assertNotNull("POST request to Uniprot should return non-null result", result);
		} catch (IOException e) {
		}
		
	}
	
	@Test
	public void testInvalidHttpRequestType() {
		WebPageDownloader downloader = new WebPageDownloader();
		try {
		downloader.getFromNetwork("https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/", "XXX", "P29373");
		fail("Invalid request exception expected");
		} catch (IOException e) {
		}
		
	}

}
