package lcsb.mapviewer.annotation.cache;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.model.cache.CacheType;

/**
 * Implementation of cache that stores everything in application memory. Data is
 * lost when application restarts. It implements singleton pattern. All method
 * are synchronized (so there are no issues when few thread are trying to access
 * data at the same time),
 * 
 * @author Piotr Gawron
 * 
 */
public final class ApplicationLevelCache extends XmlParser implements QueryCacheInterface {

  /**
   * How many values should be stored in cache before we even try to consider
   * releasing object due to huge memory usage.
   */
  static final int MIN_CACHED_VALUES_BEFORE_CLEAN = 100;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ApplicationLevelCache.class);

  /**
   * Cached nodes stored locally and identified by string.
   */
  private static Map<String, Node> cachedQueryNodes = new HashMap<String, Node>();

  /**
   * Cached strings stored locally and identified by string.
   */
  private static Map<String, String> cachedQueryString = new HashMap<String, String>();

  /**
   * Global instance of cache.
   */
  private static ApplicationLevelCache cache = null;

  /**
   * Returns single instance of global application cache (singleton pattern).
   * 
   * @return single instance of global application cache
   */
  public static ApplicationLevelCache getInstance() {
    if (Configuration.isApplicationCacheOn()) {
      if (cache == null) {
        cache = new ApplicationLevelCache();
      }
      return cache;
    } else {
      return null;
    }
  }

  /**
   * Default constructor. Prevents instantiation.
   */
  private ApplicationLevelCache() {

  }

  @Override
  public synchronized Node getXmlNodeByQuery(String query, CacheType type) {
    Node result = null;
    result = cachedQueryNodes.get(type.getId() + "\n" + query);
    return result;
  }

  @Override
  public synchronized String getStringByQuery(String query, CacheType type) {
    String result = null;
    result = cachedQueryString.get(type.getId() + "\n" + query);
    return result;
  }

  @Override
  public synchronized void setCachedQuery(String query, CacheType type, Object object) {
    setCachedQuery(query, type, object, 0);
  }
  
  @Override
  public synchronized void setCachedQuery(String query, CacheType type, Object object, int validDays) {
    performMemoryBalance();

    if (object instanceof String) {
      cachedQueryString.put(type.getId() + "\n" + query, (String) object);
    } else if (object instanceof Node) {
      cachedQueryNodes.put(type.getId() + "\n" + query, (Node) object);
    } else if (object == null) {
      cachedQueryString.put(type.getId() + "\n" + query, (String) object);
    } else {
      throw new CacheException("Unknown object type: " + object.getClass());
    }
  }

  /**
   * Method that clean cache if memory usage is too high.
   */
  synchronized void performMemoryBalance() {
    long cacheCount = 0;
    cacheCount += cachedQueryString.size();
    cacheCount += cachedQueryNodes.size();
    // check memory usage only if we have some data to clear
    if (cacheCount > MIN_CACHED_VALUES_BEFORE_CLEAN) {
      Runtime runtime = Runtime.getRuntime();
      long maxMem = runtime.maxMemory();
      long useMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
      // if free memory is less than 10% then we clear cache
      if (useMem > maxMem * Configuration.getMemorySaturationRatioTriggerClean()) {
        logger.info("Cache will be cleared");
        logger.info("Memory usage: " + useMem + " out of " + maxMem + " is used. Exceeded: "
            + maxMem * Configuration.getMemorySaturationRatioTriggerClean());
        logger.info("Elements in cache: " + cacheCount);
        clearCache();
        System.gc();
      }
    }
  }

  @Override
  public synchronized void removeByQuery(String query, CacheType type) {
    cachedQueryString.remove(type.getId() + "\n" + query);
    cachedQueryNodes.remove(type.getId() + "\n" + query);
  }

  @Override
  public synchronized void clearCache() {
    logger.info("Clearing application cache");
    cachedQueryNodes.clear();
    cachedQueryString.clear();
  }

  @Override
  public void invalidateByQuery(String query, CacheType type) {
    removeByQuery(query, type);
  }

}
