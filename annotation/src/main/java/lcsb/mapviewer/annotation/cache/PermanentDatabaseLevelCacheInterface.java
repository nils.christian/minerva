package lcsb.mapviewer.annotation.cache;

import lcsb.mapviewer.persist.dao.cache.CacheQueryDao;

/**
 * Interface describing database level cache. It has the same functionality as
 * {@link QueryCacheInterface}. It allows to cache data within application
 * scope. After application restarts everything is lost.
 * 
 * @author Piotr Gawron
 * 
 */
public interface PermanentDatabaseLevelCacheInterface extends QueryCacheInterface {

	/**
	 * Returns how many tasks are pending for refreshing.
	 * 
	 * @return how many tasks are pending for refreshing.
	 */
	int getRefreshPendingQueueSize();

	/**
	 * Returns true if refreshing of entries in database is in progress.
	 * 
	 * @return true if refreshing of entries in database is in progress.
	 */
	boolean refreshIsBusy();

	/**
	 * Returns approximate number of refresh tasks that are currently executed.
	 * 
	 * @return approximate number of refresh tasks that are currently executed.
	 */
	int getRefreshExecutingTasksSize();

	/**
	 * Returns {@link CacheQueryDao} used by the cache.
	 * 
	 * @return {@link CacheQueryDao} used by the cache
	 */
	CacheQueryDao getCacheQueryDao();

	/**
	 * Sets {@link CacheQueryDao}.
	 * 
	 * @param cacheQueryDao
	 *          new {@link CacheQueryDao} used by the cache
	 */
	void setCacheQueryDao(CacheQueryDao cacheQueryDao);

}
