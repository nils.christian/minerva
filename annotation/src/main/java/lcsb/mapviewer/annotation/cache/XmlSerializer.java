package lcsb.mapviewer.annotation.cache;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.SerializationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;

/**
 * This class allos to serialize and deserialize object of class T into xml
 * {@link String}.
 * 
 * @author Piotr Gawron
 *
 * @param <T>
 *          type of the object to serialize
 */
public class XmlSerializer<T> {
	
	/**
	 * Default class logger.
	 */
	private final Logger			 logger	= Logger.getLogger(XmlSerializer.class);

	/**
	 * Class of the object that DAO works on.
	 */
	private Class<? extends T> clazz;

	/**
	 * Java Architecture for XML Binding (JAXB) class that allows transforming T
	 * object into xml.
	 */
	private Marshaller				 jaxbMarshaller;

	/**
	 * Java Architecture for XML Binding (JAXB) class that allows transforming xml
	 * describing T into T object.
	 */
	private Unmarshaller			 jaxbUnmarshaller;

	/**
	 * Default constructor.
	 * 
	 * @param theClass
	 *          class of the object that DAO will work on
	 */
	public XmlSerializer(Class<? extends T> theClass) {
		this.clazz = theClass;
		try {
			JAXBContext jaxbContext;
			jaxbContext = JAXBContext.newInstance(clazz);
			jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			throw new SerializationException(e);
		}
	}

	/**
	 * Transforms object into xml string.
	 * 
	 * @param object
	 *          object to transform
	 * @return xml string representing object
	 */
	public String objectToString(T object) {
		if (object == null) {
			return null;
		}
		StringWriter sw = new StringWriter();
		try {
			jaxbMarshaller.marshal(object, sw);
		} catch (JAXBException e) {
			throw new SerializationException(e);
		} catch (Exception e) {
			logger.error(e, e);
			return null;
		}
		return sw.toString();
	}

	/**
	 * Creates object from xml node.
	 * 
	 * @param node
	 *          xml node
	 * @return object corresponding to xml node
	 */
	@SuppressWarnings("unchecked")
	public T xmlToObject(Node node) {
		if (node == null) {
			return null;
		}
		try {
			return (T) jaxbUnmarshaller.unmarshal(node);
		} catch (JAXBException e) {
			throw new SerializationException(e);
		} catch (Exception e) { // the library can throw NPE sometimes...
			logger.error(e, e);
			return null;
		}
	}

}
