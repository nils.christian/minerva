package lcsb.mapviewer.annotation.cache;

import org.apache.commons.net.ftp.FTPClient;

/**
 * Factory class used for creating {@link FTPClient} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class FtpClientFactory {

	/**
	 * Creates new instance of {@link FTPClient}.
	 * 
	 * @return new instance of {@link FTPClient}
	 */
	public FTPClient createFtpClient() {
		return new FTPClient();
	}
}
