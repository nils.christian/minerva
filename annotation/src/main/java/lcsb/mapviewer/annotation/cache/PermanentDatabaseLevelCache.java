package lcsb.mapviewer.annotation.cache;

import java.lang.reflect.Constructor;
import java.util.Calendar;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.cache.CacheQueryDao;

/**
 * Implementation of database level cache (
 * {@link PermanentDatabaseLevelCacheInterface}). The bean implements
 * {@link ApplicationContextAware} to be able to inject dependencies into
 * {@link CachableInterface} classes that are responsible for specific parts of
 * the cache. Every single task in this implementation is performed in new
 * transaction (and separate thread).
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class PermanentDatabaseLevelCache extends XmlParser
    implements PermanentDatabaseLevelCacheInterface, ApplicationContextAware {

  /**
   * This class represents new thread task for querying database.
   *
   * @author Piotr Gawron
   *
   */
  private final class QueryTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#query
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#type
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private QueryTask(String query, CacheType type) {
      logger.debug("Query task start");
      this.query = query;
      this.type = type;
    }

		@Override
		public CacheQuery call() throws Exception {
          logger.debug("Query task call");
          try {
            dbUtils.createSessionForCurrentThread();
            logger.debug("Query task session started");
            CacheQuery entry = getCacheQueryDao().getByQuery((String) query, type);
            logger.debug("Query task data retrieved");
            return entry;
          } finally {
            dbUtils.closeSessionForCurrentThread();
          }
		}

  }

  /**
   * This class represents new thread task for refreshing element in db.
   *
   * @author Piotr Gawron
   *
   */
  private final class RefreshTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#query
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#type
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private RefreshTask(String query, CacheType type) {
      logger.debug("Refresh task start (query: " + query + ")");
      this.query = query;
      this.type = type;
    }

    @Override
    public CacheQuery call() throws Exception {
      logger.debug("Refresh task call");
      CachableInterface cachableInterface = null;
      try {
        Constructor<?> ctor;
        Class<?> clazz = Class.forName(type.getClassName());
        ctor = clazz.getConstructor();
        Object object = ctor.newInstance();
        if (object instanceof CachableInterface) {
          cachableInterface = (CachableInterface) object;
        } else {
          logger.fatal(
              "Invalid class type: " + object.getClass() + ". Class cannot be cast into " + CachableInterface.class);
        }
        applicationContext.getAutowireCapableBeanFactory().autowireBean(object);

      } catch (Exception e) {
        logger.fatal("Problem with creating cache query updater", e);
      }
      if (cachableInterface != null) {
        cachableInterface.setCache(new GeneralCacheWithExclusion(cachableInterface.getCache(), 1));
        dbUtils.createSessionForCurrentThread();
        logger.debug("Refresh task session started");
        try {
          Object result = cachableInterface.refreshCacheQuery(query);
          if (result == null) {
            removeByQuery(query, type);
          } else {
            setCachedQuery(query, type, result);

          }
        } catch (SourceNotAvailable e) {
          logger.error("Cannot refresh cache", e);
        } catch (InvalidArgumentException e) {
          removeByQuery(query, type);
          logger.error("Problem with refreshing. Invalid data to refresh", e);
        } catch (Exception e) {
          logger.error("Severe problem in cache", e);
        } finally {
          // close the transaction for this thread
          dbUtils.closeSessionForCurrentThread();
          logger.debug("Refresh task session closed");
        }
      }
      logger.debug("Refresh task finish");
      final Future<CacheQuery> task = service.submit(new QueryTask(query, type));
      return task.get();
    }

  }

  /**
   * This class represents new thread task for adding entry to database.
   *
   * @author Piotr Gawron
   *
   */
  private final class AddTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#query
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#type
     */
    private CacheType type;

    /**
     * Value to cache.
     *
     * @see CacheQuery#value
     */
    private String value;

    /**
     * How long should the entry be valid in days.
     *
     */
    private int validDays;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     * @param value
     *          {@link #value}
     */
    private AddTask(String query, CacheType type, String value, int validDays) {
      logger.debug("Add task start");
      this.query = query;
      this.type = type;
      this.value = value;
      this.validDays = validDays;
    }

		@Override
		public CacheQuery call() throws Exception {
            logger.debug("Add task call");
            try {
              dbUtils.createSessionForCurrentThread();
              CacheQuery entry = getCacheQueryDao().getByQuery((String) query, type);

              if (entry == null) {
                entry = new CacheQuery();
                entry.setQuery((String) query);
                entry.setAccessed(Calendar.getInstance());
              } else {
                entry.setAccessed(Calendar.getInstance());
              }
              entry.setValue(value);
              entry.setType(type);
              Calendar expires = Calendar.getInstance();
              expires.add(Calendar.DAY_OF_MONTH, validDays);
              entry.setExpires(expires);

              getCacheQueryDao().add(entry);
              return entry;
            } finally {
              dbUtils.closeSessionForCurrentThread();
            }
		}

  }

  /**
   * This class represents new thread task for removing entry from database.
   *
   * @author Piotr Gawron
   *
   */
  private final class RemoveTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#query
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#type
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private RemoveTask(String query, CacheType type) {
      logger.debug("Remove task start");
      this.query = query;
      this.type = type;
    }

		@Override
		public CacheQuery call() throws Exception {
			logger.debug("Remove task call");
            try {
              dbUtils.createSessionForCurrentThread();
              CacheQuery entry = getCacheQueryDao().getByQuery((String) query, type);
              if (entry != null) {
                getCacheQueryDao().delete(entry);
              }
              return entry;
            } finally {
              dbUtils.closeSessionForCurrentThread();
            }
		}

  }

  /**
   * This class represents new thread task for invalidating entry in database.
   *
   * @author Piotr Gawron
   *
   */
  private final class InvalidateTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     */
    private String query;

    /**
     * Type of cached entry.
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private InvalidateTask(String query, CacheType type) {
      logger.debug("Invalidate task start");
      this.query = query;
      this.type = type;
    }

    @Override
    public CacheQuery call() throws Exception {
      logger.debug("Invalidate task call");
      dbUtils.createSessionForCurrentThread();
      try {
        Calendar date = Calendar.getInstance();

        date.add(Calendar.DATE, -1);

        CacheQuery entry = getCacheQueryDao().getByQuery(query, type);
        if (entry != null) {
          entry.setExpires(date);
          getCacheQueryDao().update(entry);
          cacheRefreshService.submit(new RefreshTask(query, type));
        }
        return entry;
      } finally {
        // close session even when we had a problem
        dbUtils.closeSessionForCurrentThread();
      }
    }

  }

  /**
   * Spring application context.
   */
  private static ApplicationContext applicationContext;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(PermanentDatabaseLevelCache.class);

  /**
   * Data access object for query entries accessed by string key.
   */
  @Autowired
  private CacheQueryDao cacheQueryDao;

  /**
   * Utility that help to manage the sessions in custom multithreaded
   * implementation.
   */
  @Autowired
  private DbUtils dbUtils;

  /**
   * Service used for executing database tasks in separate thread.
   */
  private ExecutorService service;

  /**
   * This service is used for execution and queue of the refresh entries in the
   * database.
   */
  private ExecutorService cacheRefreshService;

  /**
   * Post init spring method used for initialization of {@link #service} used for
   * execution of db tasks.
   */
  @PostConstruct
  public void init() {
    // the executor is a daemon thread so that it will get killed automatically
    // when the main program exits
    service = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });
    cacheRefreshService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });

    // put in the queue empty task to make sure that everything was initialized
    // (additional managing thread was created)
    service.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
    cacheRefreshService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
  }

  @Override
  public void clearCache() {
    cacheQueryDao.clearTable();
  }

  @Override
  public Node getXmlNodeByQuery(String query, CacheType type) {
    final Future<CacheQuery> task = service.submit(new QueryTask(query, type));
    CacheQuery entry = executeTask(task);

    if (entry == null) {
      return null;
    }
    try {
      Document document = getXmlDocumentFromString(entry.getValue());
      Calendar currentDate = Calendar.getInstance();
      if (currentDate.after(entry.getExpires())) {
        cacheRefreshService.submit(new RefreshTask(query, type));
      }
      Node result = null;
      if (document != null) {
        result = document.getFirstChild();
      }
      return result;
    } catch (InvalidXmlSchemaException e) {
      logger.warn("Invalid xml for query: " + query);
      logger.warn("xml: " + entry.getValue());
      removeByQuery(query, type);
      return null;
    }
  }

  @Override
  public String getStringByQuery(String query, CacheType type) {
    Calendar currentDate = Calendar.getInstance();
    final Future<CacheQuery> task = service.submit(new QueryTask(query, type));
    CacheQuery entry = executeTask(task);
    if (entry == null) {
      return null;
    }
    if (currentDate.before(entry.getExpires())) {
      return entry.getValue();
    } else {
      cacheRefreshService.submit(new RefreshTask(query, type));
      return entry.getValue();
    }
  }

  @Override
  public void setCachedQuery(String query, CacheType type, Object object) {
    setCachedQuery(query, type, object, type.getValidity());
  }

  @Override
  public void setCachedQuery(String query, CacheType type, Object object, int validDays) {
    String value = null;
    if (object instanceof String) {
      value = (String) object;
    } else if (object instanceof Node) {
      value = nodeToString((Node) object, true);
    } else if (object == null) {
      value = null;
    } else {
      throw new CacheException("Unknown object type: " + object.getClass());
    }
    if (value == null) {
      removeByQuery(query, type);
    } else {
      final Future<CacheQuery> task = service.submit(new AddTask(query, type, value, validDays));
      executeTask(task);
    }
  }

  @Override
  public void removeByQuery(String query, CacheType type) {
    final Future<CacheQuery> task = service.submit(new RemoveTask(query, type));
    executeTask(task);
  }

  @Override
  public void invalidateByQuery(String query, CacheType type) {
    final Future<CacheQuery> task = service.submit(new InvalidateTask(query, type));
    executeTask(task);
  }

  /**
   * Executes and returns result of the task provided in the parameter. This
   * method is blocking (it's waiting for the results).
   *
   * @param task
   *          task to be executed
   * @return value returned by the task
   */
  private CacheQuery executeTask(final Future<CacheQuery> task) {
    try {
      return task.get();
    } catch (InterruptedException e1) {
      logger.error(e1, e1);
    } catch (ExecutionException e1) {
      logger.error(e1, e1);
    }
    return null;
  }

  @Override
  public void setApplicationContext(ApplicationContext arg0) {
    applicationContext = arg0;
  }

  @Override
  public int getRefreshPendingQueueSize() {
    return ((ScheduledThreadPoolExecutor) cacheRefreshService).getQueue().size();
  }

  @Override
  public boolean refreshIsBusy() {
    return getRefreshPendingQueueSize() != 0 || getRefreshExecutingTasksSize() != 0;
  }

  @Override
  public int getRefreshExecutingTasksSize() {
    return ((ScheduledThreadPoolExecutor) cacheRefreshService).getActiveCount();
  }

  @Override
  public CacheQueryDao getCacheQueryDao() {
    return cacheQueryDao;
  }

  @Override
  public void setCacheQueryDao(CacheQueryDao cacheQueryDao) {
    this.cacheQueryDao = cacheQueryDao;
  }

}