package lcsb.mapviewer.annotation.cache;

/**
 * General cache service used by the application. It allows user to store data
 * that requires plenty of time to compute or download and access it later on.
 * 
 * @author Piotr Gawron
 * 
 */
public interface GeneralCacheInterface extends QueryCacheInterface {

}
