package lcsb.mapviewer.annotation.cache;

/**
 * Thrown when query to the database is of wrong type.
 * 
 * @author Piotr Gawron
 * 
 */
public class CacheException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor receives some kind of message.
	 * 
	 * @param string
	 *          message associated with exception
	 */
	public CacheException(final String string) {
		super(string);
	}

}
