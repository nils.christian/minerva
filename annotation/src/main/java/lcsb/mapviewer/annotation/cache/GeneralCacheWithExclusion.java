package lcsb.mapviewer.annotation.cache;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;

/**
 * This implementation of cache works as a normal cache except of the fact that
 * first few query requests are ignored (return null). It's used when refreshing
 * data from cache. Object of this class is injected into a
 * {@link CachableInterface} and after that normal call is performed. First
 * query to database will be ignored (so it won't be taken from cache), the rest
 * will go to cache.
 * 
 * @author Piotr Gawron
 *
 */
public class GeneralCacheWithExclusion implements GeneralCacheInterface {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(GeneralCacheWithExclusion.class);

  /**
   * How many queries should be ignored.
   */
  private int counter = 0;

  /**
   * Original cache object.
   */
  private GeneralCacheInterface cache;

  /**
   * Constructor that creates cache.
   * 
   * @param originalCache
   *          {@link #cache}
   * @param exclusionCount
   *          {@link #counter}
   */
  public GeneralCacheWithExclusion(GeneralCacheInterface originalCache, int exclusionCount) {
    if (originalCache == null) {
      throw new InvalidArgumentException("Cache passed as argument cannot be null");
    }
    this.counter = exclusionCount;
    this.cache = originalCache;
  }

  @Override
  public Node getXmlNodeByQuery(String identifier, CacheType type) {
    if (counter > 0) {
      counter--;
      logger.debug("Ignoring cache query due to cache refresh: " + identifier + ", " + type);
      return null;
    }
    return cache.getXmlNodeByQuery(identifier, type);
  }

  @Override
  public String getStringByQuery(String identifier, CacheType type) {
    if (counter > 0) {
      counter--;
      logger.debug("Ignoring cache query due to cache refresh: " + identifier + ", " + type);
      return null;
    }
    return cache.getStringByQuery(identifier, type);
  }

  @Override
  public void setCachedQuery(String identifier, CacheType type, Object value) {
    cache.setCachedQuery(identifier, type, value);
  }
  
  @Override
  public void setCachedQuery(String identifier, CacheType type, Object value, int validDays) {
    cache.setCachedQuery(identifier, type, value, validDays);
  }

  @Override
  public void clearCache() {
    cache.clearCache();

  }

  @Override
  public void removeByQuery(String identifier, CacheType type) {
    cache.removeByQuery(identifier, type);
  }

  @Override
  public void invalidateByQuery(String identifier, CacheType type) {
    cache.invalidateByQuery(identifier, type);
  }
}
