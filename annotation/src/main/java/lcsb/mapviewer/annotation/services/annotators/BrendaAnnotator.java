package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;

/**
 * This is a class that implements a backend to Brenda enzyme database.
 * 
 * @author David Hoksza
 * 
 */
public class BrendaAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(BrendaAnnotator.class);

  /**
   * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
   * TAIR}.
   */
  @Autowired
  private TairAnnotator tairAnnotator;

  /**
   * Default constructor.
   */
  public BrendaAnnotator() {
    super(BrendaAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Collection<MiriamData> mds = new UniprotAnnotator().uniProtToEC(createMiriamData(MiriamType.UNIPROT, "P12345"));

      status.setStatus(ExternalServiceStatusType.OK);
      List<String> ecs = new ArrayList<>();
      if (mds != null) {
        for (MiriamData md : mds) {
          ecs.add(md.getResource());
        }
      }
      if (mds == null || mds.size() != 2 || ecs.indexOf("2.6.1.1") < 0 || ecs.indexOf("2.6.1.7") < 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public void annotateElement(BioEntity object) throws AnnotatorException {
    if (isAnnotatable(object)) {

      MiriamData mdTair = null;
      for (MiriamData md : object.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.BRENDA)) {
          return;
        } else if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
          mdTair = md;
        }
      }

      if (mdTair != null) {
        tairAnnotator.annotateElement(object);
      }

      List<MiriamData> mdUniprots = new ArrayList<MiriamData>();
      for (MiriamData md : object.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.UNIPROT)) {
          mdUniprots.add(md);
        }
      }

      UniprotAnnotator uniprotAnnotator = new UniprotAnnotator();

      List<String> ecIds = new ArrayList<String>();
      for (MiriamData mdUniprot : mdUniprots) {
        try {
          Collection<MiriamData> mdECs = uniprotAnnotator.uniProtToEC(mdUniprot);
          if (mdECs != null) {
            for (MiriamData mdEC : mdECs) {
              mdEC.setAnnotator(BrendaAnnotator.class);
              mdEC.setDataType(MiriamType.BRENDA);
              if (ecIds.indexOf(mdEC.getResource()) == -1) {
                ecIds.add(mdEC.getResource());
                object.addMiriamData(mdEC);
              }
            }
          }
        } catch (UniprotSearchException e) {
          logger.warn("Cannot find EC data for UniProt id: " + mdUniprot.getResource());
        }
      }
    }
  }

  @Override
  public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
    String name;
    String result = null;
    if (query instanceof String) {
      name = (String) query;
      if (name.startsWith("http")) {
        try {
          result = getWebPageContent(name);
        } catch (IOException e) {
          throw new SourceNotAvailable(e);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with query: " + query);
      }
    } else {
      throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
    }
    return result;
  }

  @Override
  public String getCommonName() {
    return MiriamType.BRENDA.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.BRENDA.getDbHomepage();
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
