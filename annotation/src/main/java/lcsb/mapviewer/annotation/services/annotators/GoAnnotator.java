package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.internal.StringMap;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is a backend to Gene Ontology API.
 * 
 * @author Piotr Gawron
 * 
 */
public class GoAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Prefix string used for marking the query in database as query by go term.
   */
  static final String GO_TERM_CACHE_PREFIX = "GO_TERM: ";

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(GoAnnotator.class);

  /**
   * Connector used for accessing data from miriam registry.
   */
  @Autowired
  private MiriamConnector mc;

  /**
   * Object that allows to serialize {@link Go} elements into xml string and
   * deserialize xml into {@link Go} objects.
   */
  private XmlSerializer<Go> goSerializer;

	@Override
	public String refreshCacheQuery(Object query) throws SourceNotAvailable {
		String result = null;
		try {
			if (query instanceof String) {
				String name = (String) query;
				if (name.startsWith(GO_TERM_CACHE_PREFIX)) {
					String term = name.substring(GO_TERM_CACHE_PREFIX.length());
					MiriamData md = createMiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, term);
					result = goSerializer.objectToString(getGoElement(md));
				} else if (name.startsWith("http")) {
					result = getWebPageContent(name);
				} else {
					throw new InvalidArgumentException("Don't know what to do with query: " + query);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
			}
		} catch (IOException | GoSearchException e) {
			throw new SourceNotAvailable(e);
		}
		return result;
	}

  /**
   * Default constructor. Initializes structures used for transforming {@link Go}
   * from/to xml.
   */
  public GoAnnotator() {
    super(GoAnnotator.class, new Class[] { Phenotype.class, Compartment.class, Complex.class }, true);
    goSerializer = new XmlSerializer<>(Go.class);
  }

  @Override
  public void annotateElement(BioEntity object) throws AnnotatorException {
    if (isAnnotatable(object)) {
      MiriamData goTerm = null;
      for (MiriamData md : object.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.GO)) {
          goTerm = md;
        }
      }
      if (goTerm == null) {
        return;
      }

      try {
        Go go = getGoElement(goTerm);
        if (go != null) {
          setFullName((Element) object, go.getCommonName(), new ElementUtils().getElementTag(object, this));
          setDescription(object, go.getDescription());
        }
      } catch (GoSearchException e) {
        throw new AnnotatorException(e);
      }
    }
  }

  /**
   * Returns go entry from the Gene Ontology database for the goTerm (identifier).
   *
   * @param md
   *          {@link MiriamData} object referencing to entry in GO database
   * @return entry in Gene Ontology database
   * @throws GoSearchException
   *           thrown when there is a problem with accessing data in go database
   */
  protected Go getGoElement(MiriamData md) throws GoSearchException {
    Go result = goSerializer.xmlToObject(getCacheNode(GO_TERM_CACHE_PREFIX + md.getResource()));

    if (result != null) {
      return result;
    } else {
      result = new Go();
    }

    String accessUrl = "https://www.ebi.ac.uk/QuickGO/services/ontology/go/terms/" + md.getResource();
    try {
      String page = getWebPageContent(accessUrl);

      Gson gson = new Gson();

      Map<?, ?> gsonObject = (Map<?, ?>) gson.fromJson(page, HashMap.class);
      Double hits = (Double) gsonObject.get("numberOfHits");
      if (hits < 1) {
        return null;
      }
      List<?> objects = (List<?>) gsonObject.get("results");

      StringMap<?> object = (StringMap<?>) objects.get(0);
      result.setGoTerm((String) object.get("id"));
      result.setCommonName((String) object.get("name"));

      StringMap<?> descr = (StringMap<?>) object.get("definition");
      if (descr != null) {
        result.setDescription((String) descr.get("text"));
      }

      return result;
    } catch (Exception e) {
      throw new GoSearchException("Problem with accesing go database", e);
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Compartment compartment = new Compartment("some_id");
      compartment.addMiriamData(createMiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.GO, "GO:0046902"));
      annotateElement(compartment);

      if (compartment.getFullName() == null || compartment.getFullName().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (compartment.getNotes() == null || compartment.getNotes().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (Exception e) {
      logger.error("GeneOntology is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * @return the mc
   * @see #mc
   */
  public MiriamConnector getMc() {
    return mc;
  }

  /**
   * @param mc
   *          the mc to set
   * @see #mc
   */
  public void setMc(MiriamConnector mc) {
    this.mc = mc;
  }

  @Override
  public String getCommonName() {
    return MiriamType.GO.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.GO.getDbHomepage();
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
