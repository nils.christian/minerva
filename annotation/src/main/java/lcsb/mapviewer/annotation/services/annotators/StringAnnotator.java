package lcsb.mapviewer.annotation.services.annotators;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;

/**
 * This is a class that implements a mapping to STRING database.
 * 
 * @author David Hoksza
 * 
 */
public class StringAnnotator extends ElementAnnotator implements IExternalService {
	
	/**
	 * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
	 * TAIR}.
	 */
	@Autowired
	private TairAnnotator		tairAnnotator;	
	
	/**
	 * Default constructor.
	 */
	public StringAnnotator() {
		super(StringAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		return tairAnnotator.getServiceStatus();
	}

	@Override
	public void annotateElement(BioEntity object) throws AnnotatorException {
		if (isAnnotatable(object)) {
			
			MiriamData mdTair = null;			
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.STRING)) {
					return;			
				}
				if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
					mdTair = md;
				} 
			}			
			if (mdTair != null) {
				tairAnnotator.annotateElement(object);
			}
			
			List<MiriamData> mdUniprots = new ArrayList<MiriamData>();
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.UNIPROT)) {
					mdUniprots.add(md);
				}			
			}
			
			List<String> stringIds = new ArrayList<String>();
			for (MiriamData mdUniprot: mdUniprots) {
				MiriamData mdString = uniprotToString(mdUniprot);
				if (mdString != null && stringIds.indexOf(mdString.getResource()) < 0) {
					stringIds.add(mdString.getResource());
					object.addMiriamData(mdString);
				}								
			}
		}		
	}

	/**
	 * Transform UniProt {@link MiriamData} data to STRING {@link MiriamData}.
	 * 
	 * @param UniProt
	 *          {@link MiriamData} with UniProt identifier
	 * @return {@link MiriamData} with STRING identifier
	 * @throws AnnotatorException
	 *           thrown when there is a problem with accessing external database
	 */
	public MiriamData uniprotToString(MiriamData uniprot) throws AnnotatorException {
		if (uniprot == null) {
			return null;
		}

		if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
			throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
		}

		return createMiriamData(MiriamType.STRING, uniprot.getResource());
	}

	@Override
	public String getCommonName() {
		return MiriamType.STRING.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.STRING.getDbHomepage();
	}
}
