package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Defines annotation problem with element when there are no annotations for the
 * element.
 * 
 * @author Piotr Gawron
 *
 */
public class MissingAnnotation implements ProblematicAnnotation {

	/**
	 * BioEntity improperly annotated.
	 */
	private BioEntity bioEntity;

	/**
	 * Constructor that initializes the data with {@link #bioEntity bioEntity} .
	 * 
	 * @param bioEntity
	 *          bioEntity that misses annotation
	 */
	public MissingAnnotation(BioEntity bioEntity) {
		this.bioEntity = bioEntity;
	}

	@Override
	public String getMessage() {
		return new ElementUtils().getElementTag(bioEntity) + "misses annotations.";
	}

	@Override
	public String toString() {
		return getMessage();
	}

}
