package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This is a class that implements a backend to uniprot restfull API.
 * 
 * @author Piotr Gawron
 * 
 */
public class UniprotAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Default class logger.
	 */
	private static Logger	logger					= Logger.getLogger(UniprotAnnotator.class);

	/**
	 * Pattern used for finding hgnc symbol from uniprot info page .
	 */
	private Pattern				uniprotToHgnc		= Pattern.compile("GN[\\ ]+Name=([^;\\ ]+)");

	/**
	 * Pattern used for finding entrez identifier from uniprot info page .
	 */
	private Pattern				uniprotToEntrez	= Pattern.compile("DR[\\ ]+GeneID;\\ ([^;\\ ]+)");
	
	/**
	 * Pattern used for finding EC symbol from UniProt info page .
	 */
	private Pattern				uniprotToEC	= Pattern.compile("EC=((\\d+\\.-\\.-\\.-)|(\\d+\\.\\d+\\.-\\.-)|(\\d+\\.\\d+\\.\\d+\\.-)|(\\d+\\.\\d+\\.\\d+\\.\\d+))");


	/**
	 * Class used for some simple operations on {@link BioEntity} elements.
	 */
	private ElementUtils	elementUtils		= new ElementUtils();

	/**
	 * Default constructor.
	 */
	public UniprotAnnotator() {
		super(UniprotAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {
			MiriamData md = uniProtToHgnc(createMiriamData(MiriamType.UNIPROT, "P37840"));

			status.setStatus(ExternalServiceStatusType.OK);
			if (md == null) {
				status.setStatus(ExternalServiceStatusType.DOWN);
			} else if (!md.getResource().equalsIgnoreCase("SNCA")) {
				status.setStatus(ExternalServiceStatusType.CHANGED);
			}
		} catch (Exception e) {
			logger.error(status.getName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}

	@Override
	public void annotateElement(BioEntity object) throws AnnotatorException {
		if (isAnnotatable(object)) {
			String uniprotId = object.getName();
			boolean uniprotFound = false;
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.UNIPROT)) {
					uniprotId = md.getResource();
					uniprotFound = true;
				}
			}

			if (uniprotId == null || uniprotId.equals("")) {
				return;
			}

			String accessUrl = getUniprotUrl(uniprotId);
			try {
				String pageContent = getWebPageContent(accessUrl);
				// check if we have data over there
				if (!pageContent.contains("<title>Error</title>")) {
					Set<MiriamData> annotations = new HashSet<MiriamData>();
					annotations.addAll(parseHgnc(pageContent));
					annotations.addAll(parseEntrez(pageContent));
					annotations.addAll(parseEC(pageContent));
					if (!uniprotFound) {
						annotations.add(createMiriamData(MiriamType.UNIPROT, uniprotId));
					}
					object.addMiriamData(annotations);
				} else {
					// this means that entry with a given uniprot id doesn't exist
					if (uniprotFound) {
						logger.warn(elementUtils.getElementTag(object) + " Invalid uniprot id: " + uniprotId);
					}
				}
			} catch (WrongResponseCodeIOException exception) {
				logger.warn("Cannot find uniprot data for id: " + uniprotId);
			} catch (IOException exception) {
				throw new AnnotatorException(exception);
			}
		}
	}

	/**
	 * Returns url to uniprot restfull API about uniprot entry.
	 * 
	 * @param uniprotId
	 *          uniprot identifier
	 * @return url to uniprot restfull API about uniprot entry
	 */
	private String getUniprotUrl(String uniprotId) {
		return "https://www.uniprot.org/uniprot/" + uniprotId + ".txt";
	}

	/**
	 * Parse uniprot webpage to find information about {@link MiriamType#ENTREZ}
	 * and returns them.
	 * 
	 * @param pageContent
	 *          uniprot info page
	 * @return set of entrez identifiers found on the webpage
	 */
	private Collection<MiriamData> parseEntrez(String pageContent) {
		Collection<MiriamData> result = new HashSet<MiriamData>();
		Matcher m = uniprotToEntrez.matcher(pageContent);
		if (m.find()) {
			result.add(createMiriamData(MiriamType.ENTREZ, m.group(1)));
		}
		return result;
	}

	/**
	 * Parse uniprot webpage to find information about
	 * {@link MiriamType#HGNC_SYMBOL} and returns them.
	 * 
	 * @param pageContent
	 *          uniprot info page
	 * @return set of entrez identifiers found on the webpage
	 */
	private Collection<MiriamData> parseHgnc(String pageContent) {
		Collection<MiriamData> result = new HashSet<MiriamData>();
		Matcher m = uniprotToHgnc.matcher(pageContent);
		if (m.find()) {
			result.add(createMiriamData(MiriamType.HGNC_SYMBOL, m.group(1)));
		}
		return result;
	}
	
	/**
	 * Parse UniProt webpage to find information about
	 * {@link MiriamType#EC}s and returns them.
	 * 
	 * @param pageContent
	 *          UniProt info page
	 * @return EC found on the page
	 */
	private Collection<MiriamData> parseEC(String pageContent) {
		Collection<MiriamData> result = new HashSet<MiriamData>();
		Matcher m = uniprotToEC.matcher(pageContent);
		while (m.find()) {
			result.add(createMiriamData(MiriamType.EC, m.group(1)));
		}
		return result;
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		String name;
		String result = null;
		if (query instanceof String) {
			name = (String) query;
			if (name.startsWith("http")) {
				try {
					result = getWebPageContent(name);
				} catch (IOException e) {
					throw new SourceNotAvailable(e);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with query: " + query);
			}
		} else {
			throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
		}
		return result;
	}

	/**
	 * Transform uniprot identifier into hgnc name.
	 * 
	 * @param uniprot
	 *          {@link MiriamData} with uniprot identifier
	 * @return {@link MiriamData} with hgnc name
	 * @throws UniprotSearchException
	 *           thrown when there is aproblem with accessing external database
	 */
	public MiriamData uniProtToHgnc(MiriamData uniprot) throws UniprotSearchException {
		if (uniprot == null) {
			return null;
		}

		if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
			throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
		}

		String accessUrl = getUniprotUrl(uniprot.getResource());
		try {
			String pageContent = getWebPageContent(accessUrl);
			Collection<MiriamData> collection = parseHgnc(pageContent);
			if (collection.size() > 0) {
				return collection.iterator().next();
			} else {
				return null;
			}
		} catch (IOException e) {
			throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
		}

	}
	
	/**
	 * Transform uniprot identifier into EC identifiers.
	 * 
	 * @param uniprot
	 *          {@link MiriamData} with uniprot identifier
	 * @return ArrayList of  {@link MiriamData} with EC codes
	 * @throws UniprotSearchException
	 *           thrown when there is a problem with accessing external database
	 */
	public Collection<MiriamData> uniProtToEC(MiriamData uniprot) throws UniprotSearchException {
		if (uniprot == null) {
			return null;
		}

		if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
			throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
		}

		String accessUrl = getUniprotUrl(uniprot.getResource());
		try {
			String pageContent = getWebPageContent(accessUrl);
			Collection<MiriamData> collection = parseEC(pageContent);
			if (collection.size() > 0) {
				return collection;
			} else {
				return null;
			}
		} catch (IOException e) {
			throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
		}

	}

	@Override
	public String getCommonName() {
		return MiriamType.UNIPROT.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.UNIPROT.getDbHomepage();
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}

}
