package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;

/**
 * This is a class that implements a backend to TAIR.
 * 
 * @author David Hoksza
 * 
 */
public class TairAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Default class logger.
	 */
	private static Logger	logger					= Logger.getLogger(TairAnnotator.class);

	/**
	 * Pattern used for finding UniProt symbol from TAIR info page .
	 */
	private Pattern				tairToUniprot		= Pattern.compile("UniProtKB=([^-\"]*)");

	/**
	 * Default constructor.
	 */
	public TairAnnotator() {
		super(TairAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {
			MiriamData md = tairToUniprot(createMiriamData(MiriamType.TAIR_LOCUS, "AT1G01030"));

			status.setStatus(ExternalServiceStatusType.OK);
			if (md == null || !md.getResource().equalsIgnoreCase("Q9MAN1")) {
				status.setStatus(ExternalServiceStatusType.CHANGED);
			}
		} catch (Exception e) {
			logger.error(status.getName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}

	@Override
	public void annotateElement(BioEntity object) throws AnnotatorException {
		if (isAnnotatable(object)) {
//			boolean uniprotFound = false;
			List<MiriamData> mdUniProts = new ArrayList<MiriamData>(); //keeps record of existing UniProt annotations which can differ from those retrieved using TAIR
			MiriamData mdTair = null;
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
					mdTair = md;
				}
				if (md.getDataType().equals(MiriamType.UNIPROT)) {
					mdUniProts.add(md);
//					uniprotFound = true;
				}
			}

			if (mdTair == null /*|| uniprotFound*/) {
				return;
			}
			
			MiriamData mdUniprot = tairToUniprot(mdTair);			
			if (mdUniprot != null && mdUniProts.indexOf(mdUniprot) == -1) {
				object.addMiriamData(mdUniprot);
			}
		}		
	}

	/**
	 * Returns URL to TAIR page about TAIR entry.
	 * 
	 * @param tairId
	 *          TAIR identifier
	 * @return URL to TAIR page about the TAIR entry
	 */
	private String getTairUrl(String tairId) {
		return "http://arabidopsis.org/servlets/TairObject?type=locus&name=" + tairId;
	}

	/**
	 * Parse TAIR webpage to find information about
	 * {@link MiriamType#UNIPROT} and returns them.
	 * 
	 * @param pageContent
	 *          tair info page
	 * @return uniprot identifier found on the page
	 */
	private Collection<MiriamData> parseUniprot(String pageContent) {
		Collection<MiriamData> result = new HashSet<MiriamData>();
		Matcher m = tairToUniprot.matcher(pageContent);
		if (m.find()) {
			result.add(createMiriamData(MiriamType.UNIPROT, m.group(1)));
		}		
		return result;
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		String name;
		String result = null;
		if (query instanceof String) {
			name = (String) query;
			if (name.startsWith("http")) {
				try {
					result = getWebPageContent(name);
				} catch (IOException e) {
					throw new SourceNotAvailable(e);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with query: " + query);
			}
		} else {
			throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
		}
		return result;
	}

	/**
	 * Transform TAIR identifier into uniprot identifier.
	 * 
	 * @param tair
	 *          {@link MiriamData} with TAIR identifier
	 * @return {@link MiriamData} with UniProt identifier
	 * @throws AnnotatorException
	 *           thrown when there is a problem with accessing external database
	 */
	public MiriamData tairToUniprot(MiriamData tair) throws AnnotatorException {
		if (tair == null) {
			return null;
		}

		if (!MiriamType.TAIR_LOCUS.equals(tair.getDataType())) {
			throw new InvalidArgumentException(MiriamType.TAIR_LOCUS + " expected.");
		}

		String accessUrl = getTairUrl(tair.getResource());
		try {
			String pageContent = getWebPageContent(accessUrl);
			Collection<MiriamData> collection = parseUniprot(pageContent);
			if (collection.size() > 0) {
				return collection.iterator().next();
			} else {
				logger.warn("Cannot find uniprot data for id: " + tair.getResource() + " in the tair page");
				return null;
			}
		} catch (WrongResponseCodeIOException exception) {
			logger.warn("Wrong reponse code when accessing tair data with id: " + tair.getResource());
			return null;
		} catch (IOException exception) {
			throw new AnnotatorException(exception);
		}
	}

	@Override
	public String getCommonName() {
		return "TAIR";
	}

	@Override
	public String getUrl() {
		return MiriamType.TAIR_LOCUS.getDbHomepage();
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}

}
