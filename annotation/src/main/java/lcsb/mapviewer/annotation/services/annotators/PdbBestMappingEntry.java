package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.model.map.species.field.Structure;

/**
 * Structure of the PDB entries returned by the PDBe REST API "Best Structures"
 * 
 * @author David Hoksza
 *
 */
public class PdbBestMappingEntry {	
	public String chain_id;
	public String experimental_method;
	public String pdb_id;
	public int start;
	public int end;
	public int unp_end;
	public double coverage;
	public int unp_start;
	public double resolution;
	public int tax_id;
	
	public Structure convertToStructure() {
		
		Structure s = new Structure();
		
		s.setPdbId(this.pdb_id);
		s.setChainId(this.chain_id);
		s.setCoverage(this.coverage);
		s.setResolution(this.resolution);
		s.setStructStart(this.start);
		s.setStructEnd(this.end);
		s.setUnpStart(this.unp_start);
		s.setUnpEnd(this.unp_end);
		s.setExperimentalMethod(this.experimental_method);
		s.setTaxId(this.tax_id);
		
		return s;
	}
}
