package lcsb.mapviewer.annotation.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * Class used for parsing and accessing data from xml drugbank database. Parser
 * was developed by Janek. I tried to comment it as best as I could, but...
 * 
 * @author Piotr Gawron
 * 
 */
public class DrugbankXMLParser {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger		 logger									 = Logger.getLogger(DrugbankXMLParser.class);

	/**
	 * ???
	 */
	private static final int RELATED_INFORMATION_TAG = 3;

	/**
	 * Node identifing drug.
	 */
	static final String			 DRUG										 = "drug";

	/**
	 * Node identifing drug identifier.
	 */
	static final String			 DID										 = "drugbank-id";

	/**
	 * Node identifing drug name.
	 */
	static final String			 NAME										 = "name";

	/**
	 * Node identifing drug description.
	 */
	static final String			 DES										 = "description";

	/**
	 * Node identifing single synonym.
	 */
	static final String			 SY											 = "synonym";

	/**
	 * Node identifing list of synonym.
	 */
	static final String			 SYS										 = "synonyms";

	/**
	 * Node identifing single target.
	 */
	static final String			 TAR										 = "target";

	/**
	 * Node identifing list of targets.
	 */
	static final String			 TARGETS								 = "targets";

	/**
	 * File with the drugbank xml file.
	 */
	private String					 dbFile									 = null;

	/**
	 * Object used to access information about organism taxonomy.
	 */
	private TaxonomyBackend	 taxonomyBackend;

	/**
	 * List of drugs found in the xml file.
	 */
	private List<Drug>			 drugList								 = null;

	/**
	 * Default constructor.
	 * 
	 * @param file
	 *          file where data is stored
	 * @param backend
	 *          {@link #taxonomyBackend}
	 */
	public DrugbankXMLParser(String file, TaxonomyBackend backend) {
		this.dbFile = file;
		this.taxonomyBackend = backend;
	}

	/**
	 * Transforms string representing pubmed identifiers into {@link MiriamData}
	 * objects and set them to {@link Target}.
	 * 
	 * @param target
	 *          where the data should be put
	 * @param referencesString
	 *          string with pubmed identifiers
	 */
	void getPubmedFromRef(Target target, String referencesString) {
		int position = 0;
		int length = "/pubmed/".length();
		while ((position = referencesString.indexOf("/pubmed/", position)) > 0) {
			position += length;
			StringBuilder tmp = new StringBuilder("");
			while (position < referencesString.length() && referencesString.charAt(position) >= '0' && referencesString.charAt(position) <= '9') {
				tmp.append(referencesString.charAt(position));
				position++;
			}
			target.addReference(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, tmp.toString()));
		}
	}

	/**
	 * Adds gene identifiers to the target.
	 * 
	 * @param reader
	 *          xml reader used for parsing xml
	 * @param target
	 *          where the data should be added
	 * @throws XMLStreamException
	 *           thrown when there is a problem with xml
	 */
	public void getGeneCardId(XMLStreamReader reader, Target target) throws XMLStreamException {
		boolean relevant = false;
		int event;
		while (true) {
			event = reader.next();
			if (event == XMLStreamConstants.CHARACTERS) {
				if (reader.getText().trim().equals("GeneCards")) {
					relevant = true;
				} else if (relevant && !reader.getText().trim().equals("")) {
					String identifier = reader.getText().trim();
					target.addGene(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.HGNC_SYMBOL, identifier));
					relevant = false;
				}
			} else if (event == XMLStreamConstants.END_ELEMENT) {
				if (reader.getLocalName().equals("external-identifiers")) {
					break;
				}
			}
		}
	}

	/**
	 * This function is magic...
	 * 
	 * @param localName
	 *          ???
	 * @param tagContent
	 *          ???
	 * @param tagName
	 *          ???
	 * @param rel
	 *          ???
	 * @param target
	 *          ???
	 * @param drug
	 *          ???
	 * @return ???
	 * @throws DrugSearchException
	 *           thrown when there is a problem with data coming from external
	 *           databases
	 */
	public int targetEndElement(String localName, String tagContent, String tagName, Boolean rel, Target target, Drug drug) throws DrugSearchException {
		if (localName.equals("id")) {
			if (rel) {
				MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.DRUGBANK_TARGET_V4, tagContent);
				target.setSource(md);
				return 0;
			}
		} else if (localName.equals("name")) {
			if (rel) {
				target.setName(tagContent);
				return 0;
			}
		} else if (localName.equals("organism")) {
			if (rel) {
				try {
					target.setOrganism(taxonomyBackend.getByName(tagContent));
				} catch (TaxonomySearchException e) {
					throw new DrugSearchException(e);
				}
				return 0;
			}

		} else if (localName.equals("references")) {
			if (rel) {
				return 1;
			}
		} else if (localName.equals(TAR)) {
			if (rel) {
				drug.addTarget(target);
				return 0;
			}

		} else if (localName.equals(TARGETS)) {
			return 2;
		} else if (localName.equals(tagName)) {
			return RELATED_INFORMATION_TAG;
		}
		return 0;
	}

	/**
	 * Adds target to a drug.
	 * 
	 * @param reader
	 *          xml reader used for parsing xml
	 * @param drug
	 *          where target should be added
	 * @throws XMLStreamException
	 *           thrown when there is a problem with xml
	 * @throws DrugSearchException
	 *           thrown when there is a problem with data coming from external
	 *           databases
	 */
	public void addTargetToDrug(XMLStreamReader reader, Drug drug) throws XMLStreamException, DrugSearchException {
		boolean relatedInformation = true;
		String tagName = null;
		String tagContent = null;
		Target target = null;
		boolean readingReferences = false;
		while (true) {
			int event = reader.next();

			if (event == XMLStreamConstants.START_ELEMENT) {
				if (relatedInformation) {
					if (TAR.equals(reader.getLocalName())) {
						target = new Target();
						target.setType(TargetType.SINGLE_PROTEIN);
					} else {
						relatedInformation = false;
						tagName = reader.getLocalName();
						if ("id".equals(tagName) || "name".equals(tagName) || "organism".equals(tagName)) {
							relatedInformation = true;
						}
						if ("references".equals(tagName)) {
							relatedInformation = true;
							readingReferences = true;
						}
						if ("components".equals(tagName)) {
							relatedInformation = true;
							getGeneCardId(reader, target);
						}
					}
				}
			}
			if (event == XMLStreamConstants.CHARACTERS) {
				tagContent = reader.getText().trim();
				if (readingReferences) {
					getPubmedFromRef(target, tagContent);
				}
			}

			if (event == XMLStreamConstants.END_ELEMENT) {
				int what = targetEndElement(reader.getLocalName(), tagContent, tagName, relatedInformation, target, drug);
				if (what == 1) {
					readingReferences = false;
				} else if (what == 2) {
					break;
				} else if (what == RELATED_INFORMATION_TAG) {
					relatedInformation = true;
				}
			}
		}

	}

	/**
	 * Checks if tag is important for us or not.
	 * 
	 * @param tagName
	 *          name of the tag
	 * @return true if tag is relevant, <code>false</code> otherwise
	 */
	public Boolean tagNameRelated(String tagName) {
		if ("drugs".equals(tagName) || DID.equals(tagName) || NAME.equals(tagName) || DES.equals(tagName) || DES.equals(tagName) || SY.equals(tagName)
				|| SYS.equals(tagName)) {
			return true;
		}
		return false;

	}

	/**
	 * Gets drug from xml database.
	 * 
	 * @param drugName
	 *          name of the drug
	 * @return drug from the database
	 * @throws DrugSearchException
	 *           thrown when there is a problem with finding drug
	 */
	public Drug findDrug(String drugName) throws DrugSearchException {
		for (Drug drug : getDrugList()) {
			if (drug.getName().equalsIgnoreCase(drugName)) {
				return drug;
			}
			for (String string : drug.getSynonyms()) {
				if (string.equalsIgnoreCase(drugName)) {
					return drug;
				}
			}
		}
		return null;
	}

	/**
	 * Gets drug list from xml database.
	 * 
	 * @return drug list from the database
	 * @throws DrugSearchException
	 *           thrown when there is problem with finding drug
	 */
	public List<Drug> getDrugList() throws DrugSearchException {
		if (drugList != null) {
			return drugList;
		}
		drugList = new ArrayList<Drug>();
		/*************** INIT ******************************/
		try {
			XMLInputFactory f = XMLInputFactory.newInstance();
			FileInputStream temp = new FileInputStream(dbFile);
			XMLStreamReader reader = f.createXMLStreamReader(temp);

			Drug drug = null;
			String tagContent = null;
			boolean relevant = true;
			String tagName = null;
			/*************** END OF INIT ***********************/
			while (reader.hasNext()) {
				int event = reader.next();

				if (event == XMLStreamConstants.START_ELEMENT) {
					if (relevant) {
						if (DRUG.equals(reader.getLocalName())) {
							drug = new Drug();
						} else {
							relevant = false;
							tagName = reader.getLocalName();
							if (tagNameRelated(tagName)) {
								relevant = true;
							}
							if (TARGETS.equals(tagName)) {
								relevant = true;
								addTargetToDrug(reader, drug);
							}
						}
					}
				} else if (event == XMLStreamConstants.CHARACTERS) {
					tagContent = reader.getText().trim();
				} else if (event == XMLStreamConstants.END_ELEMENT) {
					if (reader.getLocalName().equals(DID)) {
						if (relevant) {
							MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.DRUGBANK, tagContent);
							drug.addSource(md);
						}
					} else if (reader.getLocalName().equals(NAME)) {
						if (relevant) {
							drug.setName(tagContent);
						}
					} else if (reader.getLocalName().equals(DES)) {
						if (relevant && (!"".equals(tagContent.trim()))) {
							drug.setDescription(tagContent);
						}
					} else if (reader.getLocalName().equals(DRUG)) {
						if (relevant) {
							drugList.add(drug);
						}
					} else if (reader.getLocalName().equals(SY)) {
						if (relevant) {
							drug.addSynonym(tagContent);
						}
					} else if (!relevant && tagName.equals(reader.getLocalName())) {
						relevant = true;
					}
				}
			}

		} catch (XMLStreamException | FileNotFoundException e) {
			throw new DrugSearchException("Problem with processing input file", e);
		}
		return drugList;
	}
}