package lcsb.mapviewer.annotation.services.annotators;

/**
 * Definition of a single parameter of an annotator.
 * 
 * @author David Hoksza
 * 
 */
public class AnnotatorParamDefinition {
	private String name;
	
	private String description;
	
	private Class<?> type;

	public AnnotatorParamDefinition(String name, Class<?> type, String description) {
		super();
		this.name = name;
		this.description = description;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Class<?> getType() {
		return type;
	}	

}
