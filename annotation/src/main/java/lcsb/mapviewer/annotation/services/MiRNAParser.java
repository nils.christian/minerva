package lcsb.mapviewer.annotation.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.ProjectDao;

/**
 * Class used for accessing and parsing data from MiRNA database.
 * 
 * @author Ayan Rota
 * 
 */
public class MiRNAParser extends CachableInterface implements IExternalService {

  /**
   * Url used for searching mirna by name.
   */
  private static final String URL_SOURCE_DATABASE = "http://mirtarbase.mbc.nctu.edu.tw/cache/download/6.1/miRTarBase_SE_WR.xls";

  /**
   * Cached file with static database about miriam (right now we parse it from
   * excel file).
   */
  private ByteArrayOutputStream sourceInputStream;

  /**
   * Prefix used in the DB to identify the cache entry.
   */
  protected static final String MI_RNA_PREFIX = "mirBase:";

  protected static final String PROJECT_SUGGESTED_QUERY_PREFIX = "PROJECT_MI_RNA_QUERIES:";

  /**
   * Prefix used in the DB to identify the cache entry that will have information
   * about mirna that targets specific target.
   */
  static final String MI_RNA_TARGET_PREFIX = "MIRNA_TARGET:";

  /**
   * The location of Mirtarbase id column of Mirtarbase database.
   */
  public static final Integer MIR_TAR_BASE_ID_COL = 0;

  /**
   * The location of Mirbase name column of Mirbase database.
   */
  public static final Integer MIR_BASE_NAME_COL = 1;

  /**
   * The location of species column.
   */
  public static final Integer SPECIES_COL = 5;

  /**
   * The location of target gene HGNC ID column.
   */
  public static final Integer GENE_HGNC_ID_COL = 3;

  /**
   * The location of target gene ENTREZ ID column.
   */
  public static final Integer GENE_ENTREZ_ID_COL = 4;

  /**
   * The location of publication PEDMED ID column.
   */
  public static final Integer INTERACTION_PEDMED_ID_COL = 8;

  /**
   * Size of the buffer when copying files.
   */
  private static final int BUFFER_SIZE = 1024;

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(MiRNAParser.class);

  /**
   * Class used to access information about organisms taxonomy.
   */
  @Autowired
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private HgncAnnotator hgncAnnotator;

  /**
   * Cache used for storing big files.
   */
  @Autowired
  private BigFileCache bigFileCache;

  @Autowired
  private ProjectDao projectDao;

  /**
   * Object that allows to serialize {@link MiRNA} elements into xml string and
   * deserialize xml into {@link MiRNA} objects.
   */
  private XmlSerializer<MiRNA> miRnaSerializer;

  /**
   * The status of the db.
   */
  private ExternalServiceStatus status = new ExternalServiceStatus("mirTarBase",
      MiriamType.MIR_TAR_BASE_MATURE_SEQUENCE.getDbHomepage());

  /**
   * Default constructor.
   */
  public MiRNAParser() {
    super(MiRNAParser.class);
    miRnaSerializer = new XmlSerializer<>(MiRNA.class);
  }

  @Override
  public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
    try {
      Object result = null;
      if (query instanceof String) {
        String identifier = (String) query;
        if (identifier.startsWith(MI_RNA_PREFIX)) {
          String[] ids = identifier.split("\\:");
          List<String> miRNAIDs = new ArrayList<>();
          miRNAIDs.add(ids[1]);
          List<MiRNA> list = getMiRnasByNameFromDb(miRNAIDs);
          if (!list.isEmpty()) {
            result = miRnaSerializer.objectToString(list.get(0));
          }
        } else if (identifier.startsWith(MI_RNA_TARGET_PREFIX)) {
          String[] tmp = identifier.substring(MI_RNA_TARGET_PREFIX.length()).split(":");
          MiriamData md = new MiriamData();
          md.setDataType(MiriamType.valueOf(tmp[0]));
          md.setResource(tmp[1]);
          getMiRnaListByTarget(md);
          result = super.getCacheValue(identifier);
        } else if (identifier.startsWith(PROJECT_SUGGESTED_QUERY_PREFIX)) {
          String[] tmp = identifier.split("\n");
          Integer id = Integer.valueOf(tmp[1]);
          Project project = projectDao.getById(id);
          if (project == null) {
            throw new SourceNotAvailable("Project with given id doesn't exist: " + id);
          }
          List<String> list = getSuggestedQueryListWithoutCache(project);
          result = StringUtils.join(list, "\n");
        } else {
          result = super.refreshCacheQuery(query);
        }
      } else {
        result = super.refreshCacheQuery(query);
      }
      return result;
    } catch (MiRNASearchException e) {
      throw new SourceNotAvailable(e);
    }
  }

  /**
   * @param names
   *          list of rna names to be retrieved.
   * @return list of all object retrieved.
   * @throws MiRNASearchException
   *           thrown when there is a problem with accessing mirna database
   */
  public List<MiRNA> getMiRnasByNames(Collection<String> names) throws MiRNASearchException {
    List<MiRNA> miRNAs = new ArrayList<>();
    List<String> idsToRemove = new ArrayList<>();
    MiRNA miRNA = null;

    for (String name : names) {
      // look for MiRNA in the cache
      Node miRNANode = super.getCacheNode(MI_RNA_PREFIX + name.toLowerCase());
      if (miRNANode != null && miRNANode.hasChildNodes()) {
        miRNA = miRnaSerializer.xmlToObject(miRNANode);
        miRNAs.add(miRNA);
        idsToRemove.add(name);
      }
    }
    List<String> idsToFind = new ArrayList<>();
    idsToFind.addAll(names);
    for (String toRemove : idsToRemove) {
      idsToFind.remove(toRemove);
    }
    // Not in the cache then get it from external database
    if (!idsToFind.isEmpty()) {
      // logger.debug("MiRNAParser.getMiRNA Start:"+new Date());
      List<MiRNA> fromDBList = this.getMiRnasByNameFromDb(idsToFind);
      for (MiRNA mirna : fromDBList) {
        super.setCacheValue(MI_RNA_PREFIX + mirna.getName().toLowerCase(), miRnaSerializer.objectToString(mirna));
      }
      // logger.debug("MiRNAParser.getMiRNA End:"+new Date());
      // Add them to result
      miRNAs.addAll(fromDBList);

    }
    return miRNAs;
  }

  /**
   * Returns input stream with a file containing Excel database with mirna data.
   * Original source is located at {@link #URL_SOURCE_DATABASE}.
   * 
   * @return input stream with a file containing Excel database with mirna data
   * @throws IOException
   *           thrown when there is a problem with accessing mirna data file
   */
  private InputStream getSourceInputStream() throws IOException {
    if (this.sourceInputStream == null) {
      this.sourceInputStream = new ByteArrayOutputStream();

      if (!bigFileCache.isCached(URL_SOURCE_DATABASE)) {
        try {
          bigFileCache.downloadFile(URL_SOURCE_DATABASE, false, new IProgressUpdater() {
            @Override
            public void setProgress(double progress) {
            }
          });
        } catch (URISyntaxException e) {
          throw new IOException("Problem with downloading file: " + URL_SOURCE_DATABASE, e);
        }
      }
      String filename = bigFileCache.getAbsolutePathForFile(URL_SOURCE_DATABASE);
      byte[] buffer = new byte[BUFFER_SIZE];
      int len;
      InputStream stream = new FileInputStream(new File(filename));
      try {
        while ((len = stream.read(buffer)) > -1) {
          this.sourceInputStream.write(buffer, 0, len);
        }
      } finally {
        stream.close();
      }
      this.sourceInputStream.flush();
    }
    return new ByteArrayInputStream(sourceInputStream.toByteArray());
  }

  /**
   * @param originalNames
   *          names of miRNAs
   * @return List of MiRNA
   * @throws MiRNASearchException
   *           thrown when there is a problem with accessing mirna database
   */
  protected List<MiRNA> getMiRnasByNameFromDb(List<String> originalNames) throws MiRNASearchException {
    List<String> names = new ArrayList<>();
    for (String string : originalNames) {
      names.add(string.toLowerCase());
    }

    Map<String, MiRNA> miRNAs = new HashMap<>();
    List<MiRNA> result = new ArrayList<MiRNA>();
    NPOIFSFileSystem fileInputStream = null;
    try {
      fileInputStream = new NPOIFSFileSystem(getSourceInputStream());
      Workbook workbook = WorkbookFactory.create(fileInputStream);
      Sheet sheet = workbook.getSheetAt(0);
      Iterator<Row> rows = sheet.rowIterator();
      // Skip header
      if (rows.hasNext()) {
        rows.next();
      }
      while (rows.hasNext()) {
        Row row = rows.next();
        Cell targetSpeciesCell = row.getCell(SPECIES_COL, Row.RETURN_BLANK_AS_NULL);
        MiriamData organism = null;
        if (targetSpeciesCell != null) {
          targetSpeciesCell.setCellType(Cell.CELL_TYPE_STRING);
          String value = targetSpeciesCell.getStringCellValue();
          organism = taxonomyBackend.getByName(value);
        }

        Cell mirBaseNamecell = row.getCell(MIR_BASE_NAME_COL, Row.RETURN_BLANK_AS_NULL);
        if (mirBaseNamecell == null) {
          continue;
        }

        mirBaseNamecell.setCellType(Cell.CELL_TYPE_STRING);
        String name = mirBaseNamecell.getStringCellValue();
        if (name == null || name.trim().isEmpty()) {
          continue;
        }
        if (miRNAs.get(name) == null) {
          miRNAs.put(name, new MiRNA(name));
        }

        MiriamData mirTaRBaseId = null;
        Cell mirTaRBaseIdCell = row.getCell(MIR_TAR_BASE_ID_COL, Row.RETURN_BLANK_AS_NULL);
        if (mirTaRBaseIdCell == null) {
          continue;
        } else {
          mirTaRBaseIdCell.setCellType(Cell.CELL_TYPE_STRING);
          String value = mirTaRBaseIdCell.getStringCellValue();
          if (value == null || value.trim().isEmpty()) {
            continue;
          } else {
            mirTaRBaseId = new MiriamData(MiriamType.MIR_TAR_BASE_MATURE_SEQUENCE, value.trim());
          }
        }

        Cell hgncCell = row.getCell(GENE_HGNC_ID_COL, Row.RETURN_BLANK_AS_NULL);
        if (hgncCell != null) {
          hgncCell.setCellType(Cell.CELL_TYPE_STRING);
          String geneName = hgncCell.getStringCellValue();
          if (geneName != null && !geneName.trim().isEmpty()) {
            MiriamData gene = null;
            if (TaxonomyBackend.HUMAN_TAXONOMY.equals(organism)) {
              gene = new MiriamData(MiriamType.HGNC_SYMBOL, geneName.trim());
            } else {
              Cell entrezCell = row.getCell(GENE_ENTREZ_ID_COL, Row.RETURN_BLANK_AS_NULL);
              if (entrezCell != null) {
                entrezCell.setCellType(Cell.CELL_TYPE_STRING);
                String entrezVal = entrezCell.getStringCellValue();
                // in case of non human target set the entrez
                gene = new MiriamData(MiriamType.ENTREZ, entrezVal.trim());
              }
            }
            MiRNA rna = miRNAs.get(name);
            Target target = null;
            for (Target t : rna.getTargets()) {
              if (t.getGenes().contains(gene)) {
                if (organism == null && t.getOrganism() == null) {
                  target = t;
                } else if (organism != null && organism.equals(t.getOrganism())) {
                  target = t;
                }
              }
            }
            if (target == null) {
              target = new Target(mirTaRBaseId, gene, new ArrayList<>());
              target.setType(TargetType.SINGLE_PROTEIN);
              target.setOrganism(organism);
              target.setName(geneName);
              rna.addTarget(target);
            }
            Cell pubmedCell = row.getCell(INTERACTION_PEDMED_ID_COL, Row.RETURN_BLANK_AS_NULL);
            if (pubmedCell != null) {
              pubmedCell.setCellType(Cell.CELL_TYPE_STRING);
              String value = pubmedCell.getStringCellValue();
              if (value != null && !value.trim().isEmpty()) {
                target.addReference(new MiriamData(MiriamType.PUBMED, value));
              }
            }
          }

        }

      }
    } catch (IOException | TaxonomySearchException e) {
      throw new MiRNASearchException("Problem with reading data about MiRNA", e);
    } finally {
      if (fileInputStream != null) {
        try {
          fileInputStream.close();
        } catch (Exception ex) {
        }
      }
    }

    for (String name : miRNAs.keySet()) {
      MiRNA rna = miRNAs.get(name);
      // put them in cache
      super.setCacheValue(MI_RNA_PREFIX + name.toLowerCase(), miRnaSerializer.objectToString(rna));

      // add to results if needed
      if (names.contains(name.toLowerCase())) {
        result.add(rna);
      }
    }
    return result;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);
    status.setStatus(ExternalServiceStatusType.OK);
    try {
      List<String> names = new ArrayList<>();
      names.add("hsa-miR-375");
      List<MiRNA> list = this.getMiRnasByNames(names);
      if (list.isEmpty()) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (!list.get(0).getName().equals(names.get(0))) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error("ctdbase service unavailable", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Returns list of MiRNA that targets objects identified by elements given in
   * the parameter.
   * 
   * @param targetsMiriam
   *          targets that should be targeted by resulting mir rnas
   * @return list of mirnas that target elements identified by paramter
   * @throws MiRNASearchException
   *           thrown when there is a problem with accessing mirna database
   */
  public List<MiRNA> getMiRnaListByTargets(Collection<MiriamData> targetsMiriam) throws MiRNASearchException {
    Set<String> mirnaNames = new HashSet<>();
    Set<MiriamData> targets = new HashSet<>();
    for (MiriamData miriamData : targetsMiriam) {
      if (!MiriamType.HGNC_SYMBOL.equals(miriamData.getDataType())
          && !MiriamType.ENTREZ.equals(miriamData.getDataType())) {
        throw new InvalidArgumentException(
            "Only " + MiriamType.HGNC_SYMBOL + " or " + MiriamType.ENTREZ + " objects are accepted");
      } else {
        String miRnaIds = super.getCacheValue(
            MI_RNA_TARGET_PREFIX + miriamData.getDataType() + ":" + miriamData.getResource());
        if (miRnaIds != null) {
          String[] ids = miRnaIds.split("\n");
          for (String string : ids) {
            if (!string.isEmpty()) {
              mirnaNames.add(string);
            }
          }
        } else {
          targets.add(miriamData);
        }
      }
    }

    if (targets.size() > 0) {
      NPOIFSFileSystem fileInputStream = null;
      String value = null;

      try {
        fileInputStream = new NPOIFSFileSystem(getSourceInputStream());
        Workbook workbook = WorkbookFactory.create(fileInputStream);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.rowIterator();
        MiriamData gene = null;

        // Skip header
        if (rows.hasNext()) {
          rows.next();
        }
        Map<MiriamData, List<String>> lists = new HashMap<>();
        for (MiriamData md : targets) {
          lists.put(md, new ArrayList<>());
        }
        while (rows.hasNext()) {
          Row row = rows.next();
          Cell cell = row.getCell(SPECIES_COL, Row.RETURN_BLANK_AS_NULL);
          MiriamData organism = null;
          if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            value = cell.getStringCellValue();
            organism = taxonomyBackend.getByName(value);
          }

          cell = row.getCell(MIR_BASE_NAME_COL, Row.RETURN_BLANK_AS_NULL);
          if (cell == null) {
            continue;
          }

          cell.setCellType(Cell.CELL_TYPE_STRING);
          value = cell.getStringCellValue();
          if (value == null || value.trim().isEmpty()) {
            continue;
          }
          String mirBaseName = value.trim();

          cell = row.getCell(GENE_HGNC_ID_COL, Row.RETURN_BLANK_AS_NULL);
          gene = null;
          if (cell != null) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String geneName = cell.getStringCellValue();
            if (geneName != null && !geneName.trim().isEmpty()) {
              if (TaxonomyBackend.HUMAN_TAXONOMY.equals(organism)) {
                gene = new MiriamData(MiriamType.HGNC_SYMBOL, geneName.trim());
              } else {
                Cell entrezCell = row.getCell(GENE_ENTREZ_ID_COL, Row.RETURN_BLANK_AS_NULL);
                if (entrezCell != null) {
                  entrezCell.setCellType(Cell.CELL_TYPE_STRING);
                  String entrezVal = entrezCell.getStringCellValue();
                  // in case of non human target set the entrez
                  gene = new MiriamData(MiriamType.ENTREZ, entrezVal.trim());
                }
              }
              if (gene != null) {
                List<String> list = lists.get(gene);
                if (list == null) {
                  list = new ArrayList<>();
                  lists.put(gene, list);
                }
                list.add(mirBaseName);
              }
            }
          }
        }
        for (MiriamData md : lists.keySet()) {
          StringBuilder serializedStr = new StringBuilder();
          for (String name : lists.get(md)) {
            serializedStr.append(name + "\n");
          }
          super.setCacheValue(MI_RNA_TARGET_PREFIX + md.getDataType() + ":" + md.getResource(),
              serializedStr.toString());
          if (targets.contains(md)) {
            mirnaNames.addAll(lists.get(md));
          }
        }
      } catch (IOException | TaxonomySearchException e) {
        throw new MiRNASearchException("Problem with reading data about MiRNA", e);
      } finally {
        if (fileInputStream != null) {
          try {
            fileInputStream.close();
          } catch (Exception ex) {
          }
        }
      }
    }
    return getMiRnasByNames(mirnaNames);
  }

  /**
   * Returns list of MiRNA that targets objects identified by {@link MiriamData}
   * given in the parameter.
   * 
   * @param target
   *          target that should be targeted by resulting mi rnas
   * @return list of mirnas that target element identified by parameter
   * @throws MiRNASearchException
   *           thrown when there is a problem with accessing mirna database
   */
  public List<MiRNA> getMiRnaListByTarget(MiriamData target) throws MiRNASearchException {
    List<MiriamData> list = new ArrayList<>();
    list.add(target);
    return getMiRnaListByTargets(list);
  }

  /**
   * @return the miRnaSerializer
   * @see #miRnaSerializer
   */
  protected XmlSerializer<MiRNA> getMiRnaSerializer() {
    return miRnaSerializer;
  }

  /**
   * @param miRnaSerializer
   *          the miRnaSerializer to set
   * @see #miRnaSerializer
   */
  protected void setMiRnaSerializer(XmlSerializer<MiRNA> miRnaSerializer) {
    this.miRnaSerializer = miRnaSerializer;
  }

  /**
   * @return the taxonomyBackend
   * @see #taxonomyBackend
   */
  protected TaxonomyBackend getTaxonomyBackend() {
    return taxonomyBackend;
  }

  /**
   * @param taxonomyBackend
   *          the taxonomyBackend to set
   * @see #taxonomyBackend
   */
  protected void setTaxonomyBackend(TaxonomyBackend taxonomyBackend) {
    this.taxonomyBackend = taxonomyBackend;
  }

  public List<String> getSuggestedQueryList(Project project) throws MiRNASearchException {
    String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + project.getId();
    String cachedData = getCacheValue(cacheQuery);
    List<String> result;
    if (cachedData == null) {
      result = getSuggestedQueryListWithoutCache(project);
      cachedData = StringUtils.join(result, "\n");
      setCacheValue(cacheQuery, cachedData);
    } else {
      result = new ArrayList<>();
      for (String string : cachedData.split("\n")) {
        if (!string.isEmpty()) {
          result.add(string);
        }
      }
    }
    return result;
  }

  private List<String> getSuggestedQueryListWithoutCache(Project project) throws MiRNASearchException {
    Set<String> resultSet = new HashSet<>();
    Set<MiriamData> targets = new HashSet<>();
    for (ModelData model : project.getModels()) {
      for (Element element : model.getElements()) {
        MiriamData entrez = null;
        MiriamData hgncSymbol = null;
        for (MiriamData miriam : element.getMiriamData()) {
          if (miriam.getDataType().equals(MiriamType.ENTREZ)) {
            entrez = miriam;
          } else if (miriam.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
            hgncSymbol = miriam;
          }
        }
        if (hgncSymbol != null) {
          targets.add(hgncSymbol);
        } else if (entrez != null) {
          targets.add(entrez);
        } else {
          boolean validClass = false;
          for (Class<?> clazz : MiriamType.HGNC_SYMBOL.getValidClass()) {
            if (clazz.isAssignableFrom(element.getClass())) {
              validClass = true;
            }
          }
          if (validClass) {
            MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
            try {
              if (hgncAnnotator.isValidHgncMiriam(md)) {
                targets.add(md);
              }
            } catch (AnnotatorException e) {
              logger.error("Problem with accessing HGNC database", e);
            }
          }
        }
      }
    }
    List<MiRNA> miRnas = getMiRnaListByTargets(targets);
    for (MiRNA mirna : miRnas) {
      resultSet.add(mirna.getName());
    }
    List<String> result = new ArrayList<>();
    result.addAll(resultSet);
    Collections.sort(result);
    return result;
  }

}
