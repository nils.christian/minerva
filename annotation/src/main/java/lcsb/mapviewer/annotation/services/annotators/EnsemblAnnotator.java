package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.ensembl.org">Ensembl API</a> and annotate elements with
 * information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public class EnsemblAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Version of the rest API that is supported by this annotator.
	 */
	static final String					SUPPORTED_VERSION				 = "6.2";

	/**
	 * Url address of ensembl restful service.
	 */
	private static final String	REST_SERVICE_URL				 = "https://rest.ensembl.org/xrefs/id/";

	/**
	 * Suffix that is needed for getting proper result using
	 * {@link #REST_SERVICE_URL}.
	 */
	private static final String	URL_SUFFIX							 = "?content-type=text/xml";

	/**
	 * Url used for retrieving version of the restful API.
	 */
	private static final String	REST_SERVICE_VERSION_URL = "https://rest.ensembl.org/info/rest?content-type=text/xml";

	/**
	 * Default constructor.
	 */
	public EnsemblAnnotator() {
		super(EnsemblAnnotator.class, new Class[] { Protein.class, Rna.class, Gene.class }, false);
	}

	@Override
	public String refreshCacheQuery(Object query) throws SourceNotAvailable {
		if (query instanceof String) {
			String name = (String) query;
			if (name.startsWith("http")) {
				try {
					String result = getWebPageContent(name);
					return result;
				} catch (IOException e) {
					throw new SourceNotAvailable(e);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with input string: " + name);
			}
		} else {
			throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
		}
	}

	/**
	 * Standard class logger.
	 */
	private final Logger logger = Logger.getLogger(EnsemblAnnotator.class);

	@Override
	public String getCommonName() {
		return MiriamType.ENSEMBL.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.ENSEMBL.getDbHomepage();
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {

			GenericProtein proteinAlias = new GenericProtein("mock_id");
			proteinAlias.addMiriamData(createMiriamData(MiriamType.ENSEMBL, "ENSG00000157764"));
			annotateElement(proteinAlias);

			if (proteinAlias.getFullName() == null || proteinAlias.getFullName().isEmpty()) {
				status.setStatus(ExternalServiceStatusType.CHANGED);
			} else {
				status.setStatus(ExternalServiceStatusType.OK);
			}

			String version = getRestfulApiVersion();
			if (!SUPPORTED_VERSION.equals(version)) {
				logger.debug("Version of Ensembl API changed... (new version: " + version + ")");
				status.setStatus(ExternalServiceStatusType.CHANGED);
			}

		} catch (Exception e) {
			logger.error(getCommonName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}

	/**
	 * Returns current version of restful API.
	 * 
	 * @return version of Ensembl restful API
	 * @throws IOException
	 *           thrown when there is a problem with accessing API
	 * @throws InvalidXmlSchemaException
	 *           thrown when the result returned by API is invalid
	 */
	private String getRestfulApiVersion() throws IOException, InvalidXmlSchemaException {
		String content = getWebPageContent(REST_SERVICE_VERSION_URL);
		Node xml = getXmlDocumentFromString(content);
		Node response = getNode("opt", xml.getChildNodes());
		Node data = getNode("data", response.getChildNodes());

		String version = super.getNodeAttr("release", data);
		return version;
	}

	@Override
	public void annotateElement(BioEntity element) throws AnnotatorException {
		if (isAnnotatable(element)) {
			ElementUtils eu = new ElementUtils();
			String prefix = eu.getElementTag(element);
			MiriamData entrezMiriamData = null;
			for (MiriamData md : element.getMiriamData()) {
				if (MiriamType.ENSEMBL.equals(md.getDataType())) {
					if (entrezMiriamData != null) {
						logger.warn(prefix + " More than one " + MiriamType.ENSEMBL.getCommonName() + " identifier");
					}
					entrezMiriamData = md;
				}
			}
			if (entrezMiriamData != null) {
				annotateElement(element, entrezMiriamData, prefix);
			} else {
				logger.warn(prefix + " No " + MiriamType.ENSEMBL.getCommonName() + " identifier found");
			}
		}
	}

	/**
	 * Annotates element using identifier given in the parameter.
	 * 
	 * @param annotatedObject
	 *          element that we want to annotate
	 * @param entrezMiriamData
	 *          identifier that should be used for annotation
	 * @param prefix
	 *          prefix used in warnings
	 * @throws AnnotatorException
	 *           thrown when there is a problem with annotating element
	 */
	private void annotateElement(BioEntity annotatedObject, MiriamData entrezMiriamData, String prefix) throws AnnotatorException {
		String query = REST_SERVICE_URL + entrezMiriamData.getResource() + URL_SUFFIX;
		try {
			String content = getWebPageContent(query);
			Node xml = getXmlDocumentFromString(content);
			Node response = getNode("opt", xml.getChildNodes());

			NodeList list = response.getChildNodes();
			Set<String> synonyms = new HashSet<String>();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equals("data")) {
						String error = super.getNodeAttr("error", node);
						if (error != null && !error.isEmpty()) {
							logger.warn(prefix + error);
						}
						String dbname = super.getNodeAttr("dbname", node);

						if ("EntrezGene".equals(dbname)) {
							String entrezId = super.getNodeAttr("primary_id", node);
							if (entrezId != null && !entrezId.isEmpty()) {
								annotatedObject.addMiriamData(createMiriamData(MiriamType.ENTREZ, entrezId));
							}
							String symbol = super.getNodeAttr("display_id", node);
							if (symbol != null) {
								setSymbol(annotatedObject, symbol, prefix);
							}
							String fullName = super.getNodeAttr("description", node);
							if (symbol != null) {
								setFullName((Element) annotatedObject, fullName, prefix);
							}
							NodeList synonymNodeList = node.getChildNodes();

							for (int j = 0; j < synonymNodeList.getLength(); j++) {
								Node synonymNode = synonymNodeList.item(j);
								if (synonymNode.getNodeType() == Node.ELEMENT_NODE && "synonyms".equalsIgnoreCase(synonymNode.getNodeName())) {
									synonyms.add(synonymNode.getTextContent());
								}
							}
						} else if ("HGNC".equals(dbname)) {
							String hgncId = super.getNodeAttr("primary_id", node);
							if (hgncId != null && !hgncId.isEmpty()) {
								hgncId = hgncId.replaceAll("HGNC:", "");
								annotatedObject.addMiriamData(createMiriamData(MiriamType.HGNC, hgncId));
							}
							String hgncSymbol = super.getNodeAttr("display_id", node);
							if (hgncSymbol != null && !hgncSymbol.isEmpty()) {
								annotatedObject.addMiriamData(createMiriamData(MiriamType.HGNC_SYMBOL, hgncSymbol));
							}
							NodeList synonymNodeList = node.getChildNodes();

							for (int j = 0; j < synonymNodeList.getLength(); j++) {
								Node synonymNode = synonymNodeList.item(j);
								if (synonymNode.getNodeType() == Node.ELEMENT_NODE && "synonyms".equalsIgnoreCase(synonymNode.getNodeName())) {
									synonyms.add(synonymNode.getTextContent());
								}
							}
						}
					}
				}
			}
			if (synonyms.size() > 0) {
				setSynonyms(annotatedObject, synonyms, prefix);
			}
		} catch (WrongResponseCodeIOException e) {
			logger.warn(prefix + "Cannot find information for ensembl: " + entrezMiriamData.getResource());
		} catch (Exception e) {
			throw new AnnotatorException(e);
		}
	}

	/**
	 * Converts {@link MiriamType#ENSEMBL} identifier into
	 * {@link MiriamType#ENTREZ} identifier.
	 * 
	 * @param miriamData
	 *          {@link MiriamData} with {@link MiriamType#ENSEMBL} identifier
	 * @return {@link MiriamData} with {@link MiriamType#ENTREZ} or null if such
	 *         identifier doesn't exists
	 * @throws AnnotatorException
	 *           thrown when there was problem with accessing external database or
	 *           any other unknown problem
	 */
	public MiriamData ensemblIdToEntrezId(MiriamData miriamData) throws AnnotatorException {
		if (!MiriamType.ENSEMBL.equals(miriamData.getDataType())) {
			throw new InvalidArgumentException("Only " + MiriamType.ENSEMBL + " identifier is accepted as an input");
		}
		GenericProtein proteinAlias = new GenericProtein("id");
		annotateElement(proteinAlias, miriamData, "");
		for (MiriamData md : proteinAlias.getMiriamData()) {
			if (MiriamType.ENTREZ.equals(md.getDataType())) {
				return md;
			}
		}
		return null;
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}
}
