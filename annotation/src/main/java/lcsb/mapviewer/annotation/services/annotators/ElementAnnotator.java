package lcsb.mapviewer.annotation.services.annotators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerChemical;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.UserAnnotatorsParam;

/**
 * Interface that allows to annotate {@link BioEntity elements} in the system.
 * Different implementation use different resources to perform annotation. They
 * can annotate different types of elements.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class ElementAnnotator extends CachableInterface {

  /**
   * Default class logger.
   */
  private final Logger logger = Logger.getLogger(ElementAnnotator.class);

  /**
   * List of classes that can be annotated by this {@link IElementAnnotator
   * annotator}.
   */
  private final List<Class<? extends BioEntity>> validClasses = new ArrayList<>();

  /**
   * Should be this annotator used as a default annotator.
   */
  private boolean isDefault = false;

  /**
   * Parameters which this annotator can be provided. Should be set in
   * constructor.
   */
  protected List<AnnotatorParamDefinition> paramsDefs = new ArrayList<>();

  /**
   * Default constructor.
   * 
   * @param validClasses
   *          list of classes for which this annotator is valid
   * @param isDefault
   *          {@link #isDefault}
   * @param clazz
   *          type that defines this interface
   */
  @SuppressWarnings("unchecked")
  public ElementAnnotator(Class<? extends CachableInterface> clazz, Class<?>[] validClasses, boolean isDefault) {
    super(clazz);
    for (Class<?> validClass : validClasses) {
      if (BioEntity.class.isAssignableFrom(validClass)) {
        addValidClass((Class<? extends BioEntity>) validClass);
      } else {
        throw new InvalidArgumentException("Cannot pass class of type: " + validClass + ". Only classes extending "
            + BioEntity.class + " are accepted.");
      }
    }
    this.isDefault = isDefault;
  }

  /**
   * Annotate element.
   * 
   * @param element
   *          object to be annotated
   * @throws AnnotatorException
   *           thrown when there is a problem with annotating not related to data
   */
  public abstract void annotateElement(BioEntity element) throws AnnotatorException;

  /**
   * Annotate element using parameters.
   * 
   * @param element
   *          object to be annotated
   * @param parameters
   *          list of parameters passed to the annotator which is expected to be
   *          in the same order as its {@link this#parameterDefs}
   * @throws AnnotatorException
   *           thrown when there is a problem with annotating not related to data
   */
  public void annotateElement(BioEntity element, List<UserAnnotatorsParam> parameters) throws AnnotatorException {
    annotateElement(element);
  }

  /**
   * Returns a list of all classes that can be annotated using this annotator.
   * 
   * @return a list of all classes that can be annotated using this annotator
   */
  public List<Class<? extends BioEntity>> getValidClasses() {
    return validClasses;
  }

  /**
   * Returns <code>true</code> if this annotator can annotate the object given in
   * the parameter.
   * 
   * @param object
   *          object to be tested if can be annotated
   * @return <code>true</code> if object can be annotated by this annotator,
   *         <code>false</code> otherwise
   */
  public boolean isAnnotatable(BioEntity object) {
    Class<?> clazz = object.getClass();
    for (Class<?> validClazz : getValidClasses()) {
      if (validClazz.isAssignableFrom(clazz)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds a class to list of classes that can be annotated by the annotator.
   * 
   * @param clazz
   *          class to add
   */
  private void addValidClass(Class<? extends BioEntity> clazz) {
    validClasses.add(clazz);
  }

  /**
   * Returns the common name that should be presented to user.
   * 
   * @return the common name
   */
  public abstract String getCommonName();

  /**
   * Returns url to the external resource used for annotation.
   * 
   * @return url
   */
  public abstract String getUrl();

  /**
   * Provides description of the extraction process for {@link ElementAnnotator}
   * to be used in the front end.
   * 
   * @return the description
   */
  public String getDescription() {
    return "";
  }

  /**
   * Provides description for {@link ElementAnnotator} and {@link MiriamType} pair
   * to be used in the frontend.
   * 
   * @param mt
   *          {@link MiriamType} annotated by this annotator.
   * @return the description
   */
  public String getDescription(MiriamType mt) {
    return "";
  }

  /**
   * Provides description for {@link ElementAnnotator}, {@link MiriamType} and
   * {@link MiriamRelationType} triplet to be used in the frontend.
   * 
   * @param mt
   *          {@link MiriamType} annotated by this annotator
   * @param relationType
   *          {@link MiriamRelationType} annotated by this annotator.
   * @return the description
   */
  public String getDescription(MiriamType mt, MiriamRelationType relationType) {
    return "";
  }

  /**
   * Returns list with definitions of the parameters available for this annotator.
   * 
   * @return the parameters {@link AnnotatorParamDefinition} list
   */
  public Collection<AnnotatorParamDefinition> getParametersDefinitions() {
    return paramsDefs;
  }

  /**
   * Returns <code>true</code> if this annotator can annotate the object of given
   * class type.
   * 
   * @param clazz
   *          class to be tested if can be annotated
   * @return <code>true</code> if class can be annotated by this annotator,
   *         <code>false</code> otherwise
   */
  public boolean isAnnotatable(Class<?> clazz) {
    for (Class<?> clazz2 : validClasses) {
      if (clazz2.isAssignableFrom(clazz)) {
        return true;
      }
    }
    return false;
  }

  /**
   * 
   * @return {@link #isDefault}
   */
  public boolean isDefault() {
    return isDefault;
  }

  /**
   * Sets symbol value to the element.
   * 
   * @param element
   *          element where we change data
   * @param symbol
   *          new symbol
   * @param prefix
   *          prefix used in warnings
   */
  protected void setSymbol(BioEntity element, String symbol, String prefix) {
    if (element.getSymbol() == null || element.getSymbol().equals("") || element.getSymbol().equals(symbol)) {
      element.setSymbol(symbol);
    } else {
      logger.warn(prefix + "Symbols doesn't match: \"" + symbol + "\", \"" + element.getSymbol() + "\"");
    }
    if (element.getName() == null || element.getName().equals("")) {
      element.setName(symbol);
    }
  }

  /**
   * Sets synonyms to the element.
   * 
   * @param element
   *          element where we change data
   * @param synonyms
   *          new synonyms list
   * @param prefix
   *          prefix used in warnings
   */
  protected void setSynonyms(BioEntity element, Collection<String> synonyms, String prefix) {
    if (element.getSynonyms() == null || element.getSynonyms().size() == 0) {
      List<String> sortedSynonyms = new ArrayList<>();
      sortedSynonyms.addAll(synonyms);
      Collections.sort(sortedSynonyms);

      element.setSynonyms(sortedSynonyms);
    } else {
      logger.warn(prefix + "Synonyms don't match: \"" + synonyms + "\", \"" + element.getSynonyms() + "\"");
    }
  }

  /**
   * Sets name to the element.
   * 
   * @param element
   *          element where we change data
   * @param name
   *          new name
   * @param prefix
   *          prefix used in warnings
   */
  protected void setFullName(Element element, String name, String prefix) {
    if (element.getFullName() == null || element.getFullName().equals("") || element.getFullName().equals(name)) {
      element.setFullName(name);
    } else {
      logger.warn(prefix + "Names doesn't match (" + getCommonName() + " annotator and current data): \"" + name
          + "\", \"" + element.getFullName() + "\"");
    }
  }

  /**
   * Adds description to {@link BioEntity#getNotes()}.
   * 
   * @param element
   *          element where annotation should be added
   * @param description
   *          value to set
   */
  protected void setDescription(BioEntity element, String description) {
    if (description != null) {
      if (element.getNotes() == null || element.getNotes().equals("") || element.getNotes().equals(description)) {
        element.setNotes(description);
      } else if (!element.getNotes().toLowerCase().contains(description.toLowerCase())) {
        element.setNotes(element.getNotes() + "\n" + description);
      }
    }
  }

  /**
   * Adds {@link MiriamType#HMDB} annotation to element.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          annotation identifier
   */
  protected void addHmdbMiriam(BioEntity element, String value) {
    element.addMiriamData(createMiriamData(MiriamType.HMDB, value));
  }

  protected void addMiriam(BioEntity element, String generalIdentifier, String prefix) {
    MiriamData md = null;
    try {
      md = MiriamType.getMiriamByUri(generalIdentifier);
    } catch (InvalidArgumentException e) {
      try {
        md = MiriamType.getMiriamByUri("urn:miriam:" + generalIdentifier);
      } catch (InvalidArgumentException e1) {
        logger.warn(prefix + "Unknown miriam uri: " + generalIdentifier);
      }
    }
    if (md != null && !element.getMiriamData().contains(md)) {
      element.addMiriamData(createMiriamData(md));
    }
  }

  /**
   * Sets {@link CellDesignerChemical#inChI}.
   * 
   * @param element
   *          element where annotation should be added
   * @param prefix
   *          prefix used in warnings
   * @param value
   *          value to set
   */
  protected void setInchi(Chemical element, String value, String prefix) {
    if (element.getInChI() == null || element.getInChI().isEmpty() || element.getInChI().equals(value)) {
      element.setInChI(value);
    } else {
      logger.warn(prefix + "InChI in db different: \"" + element.getInChI() + "\", " + value);
    }

  }

  /**
   * Sets {@link CellDesignerChemical#inChIKey}.
   * 
   * @param element
   *          element where annotation should be added
   * @param prefix
   *          prefix used in warnings
   * @param value
   *          value to set
   */
  protected void setInchiKey(Chemical element, String value, String prefix) {
    if (element.getInChIKey() == null || element.getInChIKey().equals("")) {
      element.setInChIKey(value);
    } else if (!element.getInChIKey().equalsIgnoreCase(value)) {
      logger.warn(prefix + "Different inchikey for chemical: " + element.getInChIKey() + ", " + value);
    }

  }

  /**
   * Sets {@link CellDesignerChemical#smiles}.
   * 
   * @param element
   *          element where annotation should be added
   * @param prefix
   *          prefix used in warnings
   * @param value
   *          value to set
   */
  protected void setSmile(Chemical element, String value, String prefix) {
    if (element.getSmiles() == null || element.getSmiles().isEmpty() || element.getSmiles().equals(value)) {
      element.setSmiles(value);
    } else {
      logger.warn(prefix + "InChI in db different: \"" + element.getSmiles() + "\", " + value);
    }

  }

  /**
   * Adds {@link MiriamType#CHEBI} annotation to element.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          annotation identifier
   */
  protected void addChebiMiriam(BioEntity element, String value) {
    if (!value.startsWith("CHEBI:")) {
      value = "CHEBI:" + value;
    }
    MiriamData md = createMiriamData(MiriamType.CHEBI, value);
    if (md != null && !element.getMiriamData().contains(md)) {
      element.addMiriamData(createMiriamData(md));
    }
  }

  /**
   * Sets {@link Species#charge}.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          value to set
   * @param prefix
   *          prefix used in warnings
   */
  protected void setCharge(Species element, String value, String prefix) {
    Integer charge = Integer.parseInt(value);
    if (element.getCharge() == null || element.getCharge() == 0 || element.getCharge().equals(charge)) {
      element.setCharge(Integer.valueOf(charge));
    } else {
      logger.warn(prefix + "Charge in db different: \"" + element.getCharge() + "\", " + charge);
    }

  }

  /**
   * Adds {@link MiriamType#PUBCHEM} annotation to element.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          annotation identifier
   */
  protected void addPubchemMiriam(BioEntity element, String value) {
    element.addMiriamData(createMiriamData(MiriamType.PUBCHEM, value));
  }

  /**
   * Adds {@link MiriamType#COG} annotation to element.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          annotation identifier
   */
  protected void addCogMiriam(BioEntity element, String value) {
    element.addMiriamData(createMiriamData(MiriamType.COG, value));
  }

  /**
   * Sets {@link Reaction#subsystem}.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          value to set
   * @param prefix
   *          prefix used in warnings
   */
  protected void setSubsystem(Reaction element, String value, String prefix) {
    if (element.getSubsystem() == null || element.getSubsystem().isEmpty() || element.getSubsystem().equals(value)) {
      element.setSubsystem(value);
    } else {
      logger.warn(prefix + "Subsystem in db different: \"" + element.getSubsystem() + "\", " + value);
    }
  }

  /**
   * Adds {@link MiriamType#EC} annotation to element.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          annotation identifier
   */
  protected void addEcMiriam(BioEntity element, String value) {
    element.addMiriamData(createMiriamData(MiriamType.EC, value));
  }

  /**
   * Adds {@link MiriamType#KEGG_COMPOUND} or {@link MiriamType#KEGG_REACTION}
   * annotation to element.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          annotation identifier
   * @param prefix
   *          prefix used in warnings
   */
  protected void addKeggMiriam(BioEntity element, String value, String prefix) {
    if (value.startsWith("C")) {
      MiriamData md = createMiriamData(MiriamType.KEGG_COMPOUND, value);
      element.addMiriamData(md);
    } else if (value.startsWith("R")) {
      MiriamData md = createMiriamData(MiriamType.KEGG_REACTION, value);
      element.addMiriamData(md);
    } else if (value.startsWith("K")) {
      MiriamData md = createMiriamData(MiriamType.KEGG_ORTHOLOGY, value);
      element.addMiriamData(md);
    } else {
      logger.warn(
          prefix + "Unknown Kegg identifier type (only Kegg compounds and reactions are supported): \"" + value + "\"");
    }
  }

  protected void addChemspider(BioEntity element, String value) {
    MiriamData md = createMiriamData(MiriamType.CHEMSPIDER, value);
    element.addMiriamData(md);
  }

  protected void addWikipedia(BioEntity element, String value) {
    MiriamData md = createMiriamData(MiriamType.WIKIPEDIA, value);
    element.addMiriamData(md);
  }

  protected void addCas(BioEntity element, String value) {
    MiriamData md = createMiriamData(MiriamType.CAS, value);
    element.addMiriamData(md);
  }

  /**
   * Sets {@link BioEntity#getFormula()}.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          value to set
   * @param prefix
   *          prefix used in warnings
   */
  protected void setFormula(BioEntity element, String value, String prefix) {
    if (element.getFormula() == null || element.getFormula().isEmpty() || element.getFormula().equals(value)) {
      element.setFormula(value);
    } else {
      logger.warn(prefix + "Formula in db different: \"" + element.getFormula() + "\", \"" + value + "\"");
    }
  }

  /**
   * Sets {@link BioEntity#getAbbreviation()}.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          value to set
   * @param prefix
   *          prefix used in warnings
   */
  protected void setAbbreviation(BioEntity element, String value, String prefix) {
    if (element.getAbbreviation() == null || element.getAbbreviation().isEmpty()
        || element.getAbbreviation().equals(value)) {
      element.setAbbreviation(value);
    } else if (!element.getAbbreviation().contains(value)) {
      logger.warn(prefix + "Abbreviation in db different: \"" + element.getAbbreviation() + "\", " + value);
    }
  }

  /**
   * Sets {@link Reaction#getMechanicalConfidenceScore()}.
   * 
   * @param element
   *          element where annotation should be added
   * @param value
   *          value to set
   * @param prefix
   *          prefix used in warnings
   */
  protected void setMcs(Reaction element, String value, String prefix) {
    if (element.getMechanicalConfidenceScore() == null || element.getMechanicalConfidenceScore() == 0
        || element.getMechanicalConfidenceScore().toString().equals(value)) {
      element.setMechanicalConfidenceScore(Integer.valueOf(value));
    } else {
      logger
          .warn(prefix + "MCS in db different: \"" + element.getMechanicalConfidenceScore() + "\", \"" + value + "\"");
    }
  }

  /**
   * Sets definitions of parameters for given annotator.
   * 
   * @param paramDefs
   *          definitions to be set
   */
  public void setParametersDefinitions(List<AnnotatorParamDefinition> paramDefs) {
    this.paramsDefs = paramDefs;
  }

  /**
   * Adds parameter definition to the definitions of parameters for given
   * annotator
   * 
   * @param paramDef
   *          parameter definition to be added
   */
  public void addParameterDefinition(AnnotatorParamDefinition paramDef) {
    this.paramsDefs.add(paramDef);
  }

  /**
   * Sets MiriamData.annotator to this annotator
   * 
   * @param md
   *          data for which the annotator is to be set
   */
  private void setAnnotator(MiriamData md) {
    md.setAnnotator(this.getClass());
  }

  /**
   * Creates {@link MiriamData} and sets its annotator to this annotator
   * 
   * @return created {@link MiriamData}
   */
  protected MiriamData createMiriamData() {
    MiriamData md = new MiriamData();
    setAnnotator(md);
    return md;
  }

  /**
   * Creates {@link MiriamData} and sets its annotator to this annotator
   * 
   * @param _md
   *          {@link MiriamData} based on which the new {@link MiriamData} should
   *          be created
   * @return created {@link MiriamData}
   */
  protected MiriamData createMiriamData(MiriamData _md) {
    MiriamData md = new MiriamData(_md);
    setAnnotator(md);
    return md;
  }

  /**
   * Creates {@link MiriamData} and sets its annotator to this annotator
   * 
   * @param mt
   *          {@link MiriamType} to be set
   * @param resource
   *          resource to be set
   * @return created {@link MiriamData}
   */
  protected MiriamData createMiriamData(MiriamType mt, String resource) {
    MiriamData md = new MiriamData(mt, resource);
    setAnnotator(md);
    return md;
  }

  /**
   * Creates {@link MiriamData} and sets its annotator to this annotator
   * 
   * @param relationType
   *          {@link MiriamRelationType} to be set
   * @param mt
   *          {@link MiriamType} to be set
   * @param resource
   *          resource to be set
   * @return created {@link MiriamData}
   */
  protected MiriamData createMiriamData(MiriamRelationType relationType, MiriamType mt, String resource) {
    MiriamData md = new MiriamData(relationType, mt, resource);
    setAnnotator(md);
    return md;
  }
}
