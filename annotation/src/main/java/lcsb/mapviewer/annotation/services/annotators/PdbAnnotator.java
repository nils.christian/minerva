package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * This is a class that implements a backend to the EBI's PDB SIFTS REST API mapping.
 * 
 * @author David Hoksza
 * 
 */
public class PdbAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Default class logger.
	 */
	private static Logger	logger					= Logger.getLogger(PdbAnnotator.class);
	
	/**
	 * Class used for some simple operations on {@link BioEntity} elements.
	 */
	private ElementUtils	elementUtils		= new ElementUtils();
	
	/**
	 * Service used for annotation of proteins using {@link MiriamType#HGNC
	 * hgnc} (this can include lookup and loading annotation from {@link 
	 * MiriamType#UNIPROT uniprot}.
	 */
	@Autowired
	private HgncAnnotator			hgncAnnotator;
		

	/**
	 * Default constructor.
	 */
	public PdbAnnotator() {
		super(PdbAnnotator.class, new Class[] { Protein.class, Rna.class, Gene.class }, false);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {
			Collection<Structure> structures = uniProtToPdb(createMiriamData(MiriamType.UNIPROT, "P29373"));			

			if (structures.size() > 0){
				if (structures.iterator().next().getPdbId() != null) { //TODO - is this id?
					status.setStatus(ExternalServiceStatusType.OK);
				} else {
					status.setStatus(ExternalServiceStatusType.CHANGED);					
				}
			}
			else {
				status.setStatus(ExternalServiceStatusType.DOWN);				
			}
		} catch (Exception e) {			
			logger.error(status.getName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}
	
	public Set<MiriamData> getUnitProts(BioEntity bioEntity) {
		HashSet<MiriamData> mds = new HashSet<>(); 
		for (MiriamData md : bioEntity.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.UNIPROT)) {
				mds.add(md);
			}
		}
		return mds;
	}

	@Override
	public void annotateElement(BioEntity bioEntity) throws AnnotatorException {
		if (isAnnotatable(bioEntity)) {
			Set<MiriamData> mds = getUnitProts(bioEntity);
			
			/*
			if (mds == null) {				
				uniprotAnnotator.annotateElement(bioEntity);
				mds = getUnitProts(bioEntity);
			}*/
						
			/*If no UniProt ID was found, try to search HGNC which then searches UNIPROT 
			 * based on the gene name.
			 */
			if (mds.size() == 0) {				
				hgncAnnotator.annotateElement(bioEntity);
				mds = getUnitProts(bioEntity);
			}

			if (mds.size() == 0) {
				return;
			}
			
			for (MiriamData md : mds) {
				try {				
					Collection<Structure> structures = uniProtToPdb(md);				
					if (structures.size() == 0) {
						logger.warn(elementUtils.getElementTag(bioEntity) + " No PDB mapping for UniProt ID: " + md.getResource());
					} else {
						//add the annotations to the set of annotation irrespective on
						//which uniprot record the structures belong to (since one molecule
						//can have multiple uniprot records associated, but annotations do 
						//do not have the concept of hierarchy or complex data types)
						Set<MiriamData> annotations = new HashSet<MiriamData>();
						for (Structure s : structures) {
							annotations.add(createMiriamData(MiriamType.PDB, s.getPdbId()));							
						}						
						bioEntity.addMiriamData(annotations);
						
						//insert the full information directly into species, .i.e.
						//create new uniprot record which includes the mapped structures 
						//and add it to the species (bioentity)						
						UniprotRecord ur = new UniprotRecord();
						ur.setUniprotId(md.getResource());
						ur.setSpecies((Species)bioEntity);
						for (Structure s : structures) {
							s.setUniprot(ur);							
						}
						ur.addStructures(structures);
						((Species)bioEntity).getUniprots().add(ur);
					}
				} catch (WrongResponseCodeIOException exception) {
					logger.warn("Not found PDB mapping (wrong response code) for UniProt ID: " + md.getResource());
				} catch (IOException exception) {
					throw new AnnotatorException(exception);
				}			
				
			}			
		}
	}

	/**
	 * Returns url to JSON with best mapping PDB entries given the UniProt entry.
	 * 
	 * @param uniprotId
	 *          uniprot identifier
	 * @return url with best mapping PDB entries to the UniProt entry
	 */
	private String getPdbMappingUrl(String uniprotId) {
		return "https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/" + uniprotId;
	}

	/**
	 * Parse UniProt-to-PDB mapping JSON file.
	 * {@link MiriamType#PDB} and returns them.
	 * 
	 * @param pageContentJson
	 *          JSON file with the UniProt to PDB mapping
	 * @return set of PDB identifiers found on the webpage
	 */
	private Collection<Structure> processMappingData(String pageContentJson) {
		Collection<Structure> result = new HashSet<Structure>();
		Gson g = new Gson();
		java.lang.reflect.Type t = new TypeToken<Map<String, List<PdbBestMappingEntry>>>(){}.getType();
		Map<String, List<PdbBestMappingEntry>> m = g.fromJson(pageContentJson, t);
		if (m != null){
			for (String key : m.keySet()){
			    for(PdbBestMappingEntry e : m.get(key)) {
			    	if (e != null){
			    		result.add(e.convertToStructure());
			    		//result.add(new MiriamData(MiriamType.PDB, e.pdb_id));			    		
			    	}			    	
			    }
			}
		}
		
		return result;
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		String name;
		String result = null;
		if (query instanceof String) {
			name = (String) query;
			if (name.startsWith("http")) {
				try {
					result = getWebPageContent(name);
				} catch (IOException e) {
					throw new SourceNotAvailable(e);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with query: " + query);
			}
		} else {
			throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
		}
		return result;
	}
	
	
	/**
	 * Tests if given input string is a valid JSON document.
	 * 
	 * @param json
	 * 				Input document as a string.
	 * @return True or false dependent on whether the input string is a valid JSON document
	 */
	public static boolean isJson(String json) {
        Gson gson = new Gson();
        try {
            gson.fromJson(json, Object.class);
            return true;
        } catch (com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }

	/**
	 * Transform UniProt identifier into PDB IDs.
	 * 
	 * @param uniprot
	 *          {@link MiriamData} with UniProt identifier
	 * @return JSON String with mapping.
	 *           thrown when there is a problem with accessing external database
	 */
	public Collection<Structure> uniProtToPdb(MiriamData uniprot) throws IOException {
		if (uniprot == null) {
			return new ArrayList<>();
		}

		if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
			throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
		}

		String accessUrl = getPdbMappingUrl(uniprot.getResource());
		String json = getWebPageContent(accessUrl);
		
		return isJson(json) ? processMappingData(json) : new ArrayList<>();
	}

	@Override
	public String getCommonName() {
	return MiriamType.PDB.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.PDB.getDbHomepage();
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}

}
