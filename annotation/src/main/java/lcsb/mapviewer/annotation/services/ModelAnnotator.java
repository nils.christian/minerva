package lcsb.mapviewer.annotation.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.BiocompendiumAnnotator;
import lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator;
import lcsb.mapviewer.annotation.services.annotators.CazyAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.annotation.services.annotators.EnsemblAnnotator;
import lcsb.mapviewer.annotation.services.annotators.EntrezAnnotator;
import lcsb.mapviewer.annotation.services.annotators.GoAnnotator;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.annotators.KeggAnnotator;
import lcsb.mapviewer.annotation.services.annotators.PdbAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ReconAnnotator;
import lcsb.mapviewer.annotation.services.annotators.StitchAnnotator;
import lcsb.mapviewer.annotation.services.annotators.StringAnnotator;
import lcsb.mapviewer.annotation.services.annotators.TairAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.UserAnnotatorsParam;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class annotates all elements in the model.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModelAnnotator {

  /**
   * Time required for annotating in
   * {@link #performAnnotations(Model, IProgressUpdater)} method.
   */
  private static final double ANNOTATING_RATIO = 0.8;
  /**
   * Time required for copying annotations from other elements in
   * {@link #performAnnotations(Model, IProgressUpdater)} method.
   */
  private static final double COPYING_RATIO = 0.2;

  /**
   * Connector used for accessing data from miriam registry.
   */
  @Autowired
  private MiriamConnector miriamConnector;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ModelAnnotator.class);

  /**
   * BRENDA annotator.
   */
  @Autowired
  private BrendaAnnotator brendaAnnotator;

  /**
   * Service accessing <a href= "http://biocompendium.embl.de/" >internal
   * annotating service</a>.
   */
  @Autowired
  private BiocompendiumAnnotator biocompendiumAnnotator;

  /**
   * CAZy annotator.
   */
  @Autowired
  private CazyAnnotator cazyAnnotator;

  /**
   * Backend to the chebi database.
   */
  @Autowired
  private ChebiAnnotator chebiBackend;

  /**
   * Uniprot annotator.
   */
  @Autowired
  private UniprotAnnotator uniprotAnnotator;

  /**
   * PDB annotator.
   */
  @Autowired
  private PdbAnnotator pdbAnnotator;

  /**
   * Recon annotator.
   */
  @Autowired
  private ReconAnnotator reconAnnotator;

  /**
   * Service accessing <a href= "http://www.ebi.ac.uk/QuickGO/" >Gene
   * Ontology</a>.
   */
  @Autowired
  private GoAnnotator goAnnotator;

  /**
   * Service accessing <a href= "http://www.genenames.org/" >HUGO Gene
   * Nomenclature Committee</a>.
   */
  @Autowired
  private HgncAnnotator hgncAnnotator;

  /**
   * Service accessing <a href= "http://www.kegg.jp/" > KEGG EC Nomenclature</a>.
   */
  @Autowired
  private KeggAnnotator keggAnnotator;

  /**
   * Service accessing <a href= "http://www.ncbi.nlm.nih.gov/gene/" >Entrez</a>.
   */
  @Autowired
  private EntrezAnnotator entrezAnnotator;

  /**
   * Service accessing <a href= "http://ensembl.org">Ensembl</a>.
   */
  @Autowired
  private EnsemblAnnotator ensemblAnnotator;

  /**
   * STITCH annotator.
   */
  @Autowired
  private StitchAnnotator stitchAnnotator;

  /**
   * STRING annotator.
   */
  @Autowired
  private StringAnnotator stringAnnotator;

  /**
   * TAIR annotator.
   */
  @Autowired
  private TairAnnotator tairAnnotator;

  /**
   * List of all avaliable {@link ElementAnnotator} objects.
   */
  private List<ElementAnnotator> availableAnnotators;

  /**
   * List of all avaliable {@link ElementAnnotator} objects.
   */
  private List<ElementAnnotator> defaultAnnotators;

  /**
   * Post intintialization method. Will be called after spring creates beans and
   * set the list of valid annotators.
   */
  @PostConstruct
  public final void init() {
    availableAnnotators = new ArrayList<>();
    defaultAnnotators = new ArrayList<>();

    addAnnotator(brendaAnnotator);
    addAnnotator(biocompendiumAnnotator);
    addAnnotator(cazyAnnotator);
    addAnnotator(chebiBackend);
    addAnnotator(uniprotAnnotator);
    addAnnotator(goAnnotator);
    addAnnotator(hgncAnnotator);
    addAnnotator(keggAnnotator);
    addAnnotator(pdbAnnotator);
    addAnnotator(reconAnnotator);
    addAnnotator(entrezAnnotator);
    addAnnotator(ensemblAnnotator);
    addAnnotator(stitchAnnotator);
    addAnnotator(stringAnnotator);
    addAnnotator(tairAnnotator);
  }

  /**
   * Adds annotator list of available annotators in {@link ModelAnnotator} class.
   * 
   * @param annotator
   *          {@link ElementAnnotator} annotator o add
   */
  private void addAnnotator(ElementAnnotator annotator) {
    availableAnnotators.add(annotator);
    if (annotator.isDefault()) {
      defaultAnnotators.add(annotator);
    }
  }

  /**
   * Performs all possible and automatic annotations on the model.
   * 
   * @param model
   *          model to update
   * @param progressUpdater
   *          callback function used for updating progress of the function
   */
  public void performAnnotations(Model model, final IProgressUpdater progressUpdater) {	
    performAnnotations(model, progressUpdater, null, null);
  }

  /**
   * Performs all possible and automatic annotations on the model.
   * 
   * @param annotators
   *          this map contains lists of {@link ElementAnnotator} objects that
   *          should be used for a given classes
   * @param model
   *          model to update
   * @param progressUpdater
   *          callback function used for updating progress of the function
   */
  public void performAnnotations(Model model, final IProgressUpdater progressUpdater,
      Map<Class<?>, List<ElementAnnotator>> annotators, Map<Class<?>, List<UserAnnotatorsParam>> annotatorsParams) {	  
    progressUpdater.setProgress(0);
    List<Model> models = new ArrayList<Model>();
    models.add(model);
    models.addAll(model.getSubmodels());
    final double size = models.size();
    double counter = 0;
    for (Model m : models) {
      final double ratio = counter / size;

      copyAnnotationFromOtherSpecies(m, new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
          progressUpdater.setProgress(ratio * IProgressUpdater.MAX_PROGRESS + (progress * COPYING_RATIO) / size);
        }
      });
      annotateModel(m, new IProgressUpdater() {

        @Override
        public void setProgress(double progress) {
          progressUpdater.setProgress(ratio * IProgressUpdater.MAX_PROGRESS
              + (COPYING_RATIO * IProgressUpdater.MAX_PROGRESS + progress * ANNOTATING_RATIO) / size);
        }
      }, annotators, annotatorsParams);
      counter++;
    }
  }

  /**
   * Due to some limitation of CellDesigner, some annotations are missing in xml
   * file. Therefore there are two other places where we look for annotations:
   * <ul>
   * <li>notes,</li>
   * <li>other elements with the same name and type.</li>
   * </ul>
   * 
   * This method copies data to elements without annotations from other elements
   * with the same name and type.
   * 
   * @param model
   *          model in which element annotations are updated
   * @param progressUpdater
   *          callback function that updates the progress of function execution
   */
  protected void copyAnnotationFromOtherSpecies(Model model, IProgressUpdater progressUpdater) {
    double counter = 0;
    double amount = model.getElements().size();
    for (Species element : model.getSpeciesList()) {
      if (element.getMiriamData().size() == 0) {
        List<Element> speciesList = model.getElementsByName(element.getName());
        for (Element species2 : speciesList) {
          if (species2.getClass().equals(element.getClass()) && species2.getMiriamData().size() > 0
              && element.getMiriamData().size() == 0) {
            for (MiriamData md : species2.getMiriamData()) {
              element.addMiriamData(new MiriamData(md));
            }
          }
        }
      }
      counter++;
      progressUpdater.setProgress(counter / amount * IProgressUpdater.MAX_PROGRESS);
    }
  }

  /**
   * Annotates all elements in the model using set of {@link ElementAnnotator}
   * given in the param. If the set is empty then all annotators will be used.
   * 
   * @param annotators
   *          this map contains lists of {@link ElementAnnotator} objects that
   *          should be used for a given classes
   * @param model
   *          model where annotation shoud be updated
   * @param progressUpdater
   *          callback function used to refresh progress of function execution
   */
  protected void annotateModel(Model model, IProgressUpdater progressUpdater,
      Map<Class<?>, List<ElementAnnotator>> annotators) {
    annotateModel(model, progressUpdater, annotators, null);
  }

  /**
   * Annotates all elements in the model using set of {@link ElementAnnotator}
   * given in the param. If the set is empty then all annotators will be used.
   * 
   * @param annotators
   *          this map contains lists of {@link ElementAnnotator} objects that
   *          should be used for a given classes
   * @param annotatorsParams
   *          this map contains lists of {@link UserAnnotatorsParam} objects that
   *          should be used for a given {@link ElementAnnotator} class
   * @param model
   *          model where annotation should be updated
   * @param progressUpdater
   *          callback function used to refresh progress of function execution
   */
  protected void annotateModel(Model model, IProgressUpdater progressUpdater,
      Map<Class<?>, List<ElementAnnotator>> annotators, Map<Class<?>, List<UserAnnotatorsParam>> annotatorsParams) {	  
	  	  
    ElementUtils elementUtils = new ElementUtils();

    progressUpdater.setProgress(0);

    double counter = 0;
    double amount = model.getElements().size() + model.getReactions().size();

    // annotate all elements
    for (Element element : model.getElements()) {
      List<ElementAnnotator> list = null;
      if (annotators != null) {
        list = annotators.get(element.getClass());
      }
      if (list == null) {
        list = getDefaultAnnotators();
      }
      
      for (ElementAnnotator elementAnnotator : list) {    	  
        try {
          if (annotatorsParams != null) {
            List<UserAnnotatorsParam> params = annotatorsParams.get(elementAnnotator.getClass());
            if (params != null) {
              elementAnnotator.annotateElement(element, params);
            } else {
            	elementAnnotator.annotateElement(element);            	
            }
          } else {
            elementAnnotator.annotateElement(element);
          }
        } catch (AnnotatorException e) {
          logger.warn(elementUtils.getElementTag(element) + " " + elementAnnotator.getCommonName()
              + " annotation problem: " + e.getMessage());
        }
      }
      counter++;
      progressUpdater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
    }

    // annotate all reactions
    for (BioEntity element : model.getReactions()) {
      List<ElementAnnotator> list = null;
      if (annotators != null) {
        list = annotators.get(element.getClass());
      }
      if (list == null) {
        list = getDefaultAnnotators();
      }

      for (ElementAnnotator elementAnnotator : list) {
        try {
          elementAnnotator.annotateElement(element);
        } catch (AnnotatorException e) {
          logger.warn(
              elementUtils.getElementTag(element) + " " + elementAnnotator.getCommonName() + " annotation problem", e);
        }
      }
      counter++;
      progressUpdater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
    }
  }

  /**
   * Returns list of default annotators ({@link ElementAnnotator} with
   * {@link ElementAnnotator#isDefault} flag).
   * 
   * @return list of default annotators
   */
  List<ElementAnnotator> getDefaultAnnotators() {
    return defaultAnnotators;
  }

  /**
   * This method returns list of improper annotations for the model. Improper
   * annotation is the annotation of the wrong type for given type of element.
   * Types of proper annotations are defined in {@link MiriamType} enum.
   * 
   * @param m
   *          model
   * @param updater
   *          updater call back function that update information about progress of
   *          the function
   * @param validAnnotations
   *          map that contains information which {@link MiriamType miriam types}
   *          are valid for which class
   * @return list of improper annotations
   */
  public Collection<ImproperAnnotations> findImproperAnnotations(Model m, IProgressUpdater updater,
      Map<Class<? extends BioEntity>, Set<MiriamType>> validAnnotations) {
    if (validAnnotations == null) {
      logger.warn("List of valid annotations is missing. Using default.");
      validAnnotations = getDefaultValidClasses();
    }
    updater.setProgress(0.0);
    List<Model> models = new ArrayList<Model>();
    models.add(m);
    models.addAll(m.getSubmodels());
    List<ImproperAnnotations> result = new ArrayList<>();

    double modelSize = models.size();
    double modelCounter = 0;
    for (Model model : models) {
      double ratio = modelCounter / modelSize;
      double size = model.getElements().size() + model.getReactions().size();
      double counter = 0;
      for (Element element : model.getElements()) {
        result.addAll(findImproperAnnotations(element, validAnnotations.get(element.getClass())));
        counter++;
        updater.setProgress(
            ratio * IProgressUpdater.MAX_PROGRESS + (counter / size * IProgressUpdater.MAX_PROGRESS) / modelSize);
      }
      for (Reaction reaction : model.getReactions()) {
        result.addAll(findImproperAnnotations(reaction, validAnnotations.get(reaction.getClass())));
        counter++;
        updater.setProgress(
            ratio * IProgressUpdater.MAX_PROGRESS + (counter / size * IProgressUpdater.MAX_PROGRESS) / modelSize);
      }
      modelCounter++;
    }
    return result;
  }

  /**
   * This method returns list of improper annotations for the element.
   * 
   * @param element
   *          where we look for improper annotations
   * @param validClasses
   *          list of {@link MiriamType miriam types} that are valid for a given
   *          {@link BioEntity}
   * @return list of improper annotations
   */
  protected List<ImproperAnnotations> findImproperAnnotations(BioEntity element, Collection<MiriamType> validClasses) {
    List<ImproperAnnotations> result = new ArrayList<>();
    for (MiriamData md : element.getMiriamData()) {
      boolean valid = false;
      if (validClasses.contains(md.getDataType())) {
        valid = true;
      }
      if (valid && miriamConnector.getUrlString(md) == null) {
        valid = false;
      }
      if (!valid) {
        result.add(new ImproperAnnotations(element, md));
      }
    }
    return result;
  }

  /**
   * Returns list of all elements that miss annotation.
   * 
   * @param m
   *          model where the search is performed
   * @param requestedAnnotations
   *          map that contains information which {@link MiriamType miriam types}
   *          are obigatory for which class
   * 
   * @return list of all elements that miss annotation
   */
  public Collection<ProblematicAnnotation> findMissingAnnotations(Model m,
      Map<Class<? extends BioEntity>, Set<MiriamType>> requestedAnnotations) {
    if (requestedAnnotations == null) {
      logger.warn("List of requested annotations is missing. Using default.");
      requestedAnnotations = getDefaultRequiredClasses();
    }
    List<ProblematicAnnotation> result = new ArrayList<>();

    List<Model> models = new ArrayList<>();
    models.add(m);
    models.addAll(m.getSubmodels());
    for (Model model : models) {
      for (Element alias : model.getElements()) {
        result.addAll(findMissing(alias, requestedAnnotations.get(alias.getClass())));
      }
      for (Reaction reaction : model.getReactions()) {
        result.addAll(findMissing(reaction, requestedAnnotations.get(reaction.getClass())));
      }
    }
    return result;
  }

  /**
   * Checks if {@link BioEntity} is properly annotated and if not return info
   * about missing annotation.
   * 
   * @param element
   *          object to be checked
   * @param requestedAnnotations
   *          at list one of this annotations should appear in the element, if the
   *          list is null then check is skipped, if the list is empty (size()==0)
   *          then method checks if element contains at least one miriam
   * @return list of containing improper annotations about element
   */
  private List<ProblematicAnnotation> findMissing(BioEntity element, Collection<MiriamType> requestedAnnotations) {
    List<ProblematicAnnotation> result = new ArrayList<>();
    // if there are no requested annotations then don't check
    if (requestedAnnotations == null) {
      return result;
      // if there are no requested annotations, but the list was mentioned then
      // we check if at least one annotation exists
    } else if (requestedAnnotations.size() == 0) {
      if (element.getMiriamData().size() == 0) {
        result.add(new MissingAnnotation(element));
      }
      return result;
    }
    // check if at least one annotation match requested list
    for (MiriamData md : element.getMiriamData()) {
      for (MiriamType mt : requestedAnnotations) {
        if (mt.equals(md.getDataType())) {
          return result;
        }
      }
    }
    result.add(new MissingRequiredAnnotations(element, requestedAnnotations));
    return result;
  }

  /**
   * 
   * @return {@link #availableAnnotators}
   */
  public List<ElementAnnotator> getAvailableAnnotators() {
    return availableAnnotators;
  }

  /**
   * Returns list of available annotators for a given class type.
   * 
   * @param clazz
   *          class type
   * @return list of available annotators for a given class type
   */
  public List<ElementAnnotator> getAvailableAnnotators(Class<?> clazz) {
    List<ElementAnnotator> result = new ArrayList<ElementAnnotator>();
    for (ElementAnnotator annotator : availableAnnotators) {
      if (annotator.isAnnotatable(clazz)) {
        result.add(annotator);
      }
    }
    return result;
  }

  /**
   * Returns list of {@link ElementAnnotator} names that are available for class
   * given in the parameter.
   * 
   * @param clazz
   *          class for which list of annotators will be returned
   * @return annotators names for a given class
   */
  public List<String> getAvailableAnnotatorNames(Class<?> clazz) {
    List<String> result = new ArrayList<>();
    for (ElementAnnotator annotator : getAvailableAnnotators(clazz)) {
      result.add(annotator.getCommonName());
    }
    return result;
  }

  /**
   * Returns list of default {@link ElementAnnotator} names that are available for
   * class given in the parameter.
   * 
   * @param clazz
   *          class for which list of default annotators will be returned
   * @return annotators names for a given class
   */
  public List<String> getAvailableDefaultAnnotatorNames(Class<?> clazz) {
    List<String> result = new ArrayList<>();
    for (ElementAnnotator annotator : getAvailableDefaultAnnotators(clazz)) {
      result.add(annotator.getCommonName());
    }
    return result;
  }

  /**
   * Returns list of default {@link ElementAnnotator annotators} that are
   * available for class given in the parameter.
   * 
   * @param clazz
   *          class for which list of default annotators will be returned
   * @return annotators for a given class
   */
  public List<ElementAnnotator> getAvailableDefaultAnnotators(Class<?> clazz) {
    List<ElementAnnotator> result = new ArrayList<>();
    for (ElementAnnotator annotator : availableAnnotators) {
      if (annotator.isAnnotatable(clazz) && annotator.isDefault()) {
        result.add(annotator);
      }
    }
    return result;
  }

  /**
   * Converts list of strings into list of {@link ElementAnnotator}. Strings must
   * be valid {@link ElementAnnotator} common names.
   * 
   * @param list
   *          list of {@link ElementAnnotator#getCommonName()}.
   * @return list of {@link ElementAnnotator annotators}
   */
  public List<ElementAnnotator> getAnnotatorsFromCommonNames(List<String> list) {
    List<ElementAnnotator> result = new ArrayList<>();
    for (String string : list) {
      boolean added = false;
      for (ElementAnnotator annotator : availableAnnotators) {
        if (annotator.getCommonName().equals(string)) {
          added = true;
          result.add(annotator);
        }
      }
      if (!added) {
        throw new InvalidArgumentException("Unknown annotator name: " + string);
      }
    }
    return result;
  }

  /**
   * Returns map with informations about default valid {@link MiriamType miriam
   * types } for {@link BioEntity} class type.
   * 
   * @return map with informations about valid {@link MiriamType miriam types }
   *         for {@link BioEntity} class type
   */
  @SuppressWarnings("unchecked")
  public Map<Class<? extends BioEntity>, Set<MiriamType>> getDefaultValidClasses() {
    Map<Class<? extends BioEntity>, Set<MiriamType>> result = new HashMap<Class<? extends BioEntity>, Set<MiriamType>>();
    ElementUtils eu = new ElementUtils();
    ClassTreeNode tree = eu.getAnnotatedElementClassTree();

    Queue<ClassTreeNode> nodes = new LinkedList<ClassTreeNode>();
    nodes.add(tree);
    while (!nodes.isEmpty()) {
      ClassTreeNode node = nodes.poll();
      Set<MiriamType> set = new HashSet<MiriamType>();
      Class<? extends BioEntity> clazz = (Class<? extends BioEntity>) node.getClazz();
      for (MiriamType mt : MiriamType.values()) {
        for (Class<?> clazz2 : mt.getValidClass()) {
          if (clazz2.isAssignableFrom(clazz)) {
            set.add(mt);
          }
        }
      }
      result.put(clazz, set);
      for (ClassTreeNode child : node.getChildren()) {
        nodes.add(child);
      }
    }
    return result;
  }

  /**
   * Returns map with informations about default required {@link MiriamType miriam
   * types } for {@link BioEntity} class type.
   * 
   * @return map with informations about required {@link MiriamType miriam types }
   *         for {@link BioEntity} class type
   */
  @SuppressWarnings("unchecked")
  public Map<Class<? extends BioEntity>, Set<MiriamType>> getDefaultRequiredClasses() {
    Map<Class<? extends BioEntity>, Set<MiriamType>> result = new HashMap<>();
    ElementUtils eu = new ElementUtils();
    ClassTreeNode tree = eu.getAnnotatedElementClassTree();

    Queue<ClassTreeNode> nodes = new LinkedList<>();
    nodes.add(tree);
    while (!nodes.isEmpty()) {
      ClassTreeNode node = nodes.poll();
      Set<MiriamType> set = null;
      Class<? extends BioEntity> clazz = (Class<? extends BioEntity>) node.getClazz();
      if ((Boolean) (node.getData())) {
        set = new HashSet<>();
        for (MiriamType mt : MiriamType.values()) {
          for (Class<?> clazz2 : mt.getRequiredClass()) {
            if (clazz2.isAssignableFrom(clazz)) {
              set.add(mt);
            }
          }
        }
      }
      result.put(clazz, set);
      for (ClassTreeNode child : node.getChildren()) {
        nodes.add(child);
      }
    }
    return result;
  }
}
