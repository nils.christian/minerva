package lcsb.mapviewer.annotation.services.genome;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeGeneMappingDao;

/**
 * Class containing generic methods for accessing storing and retrieving
 * reference genomes.
 * 
 * @author Piotr Gawron
 *
 */
public abstract class AbstractReferenceGenomeConnector extends CachableInterface implements ReferenceGenomeConnector {

	/**
	 * Default class logger.
	 */
	private Logger												logger = Logger.getLogger(AbstractReferenceGenomeConnector.class);

	/**
	 * Utils that help to manage the sessions in custom multithreaded
	 * implementation.
	 */
	@Autowired
	private DbUtils												dbUtils;

	/**
	 * Data access object for reference genomes that we have in a database.
	 */
	@Autowired
	private ReferenceGenomeDao						referenceGenomeDao;

	/**
	 * Data access object for reference genome mappings that we have in a
	 * database.
	 */
	@Autowired
	private ReferenceGenomeGeneMappingDao	referenceGenomeGeneMappingDao;

	/**
	 * Interface for storing/accessing big files on teh server (we have local copy
	 * if big files on the server).
	 */
	@Autowired
	private BigFileCache									bigFileCache;

	/**
	 * Service used for executing tasks in separate thread.
	 */
	private ExecutorService								asyncExecutorService;

	/**
	 * Service used for executing tasks immediately.
	 */
	private ExecutorService								syncExecutorService;

	/**
	 * Default constructor.
	 * 
	 * @param clazz
	 *          class that extends this abstract interface
	 */
	public AbstractReferenceGenomeConnector(Class<? extends CachableInterface> clazz) {
		super(clazz);
		// the executor is a daemon thread so that it will get killed automatically
		// when the main program exits
		asyncExecutorService = Executors.newScheduledThreadPool(10, new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r);
				t.setDaemon(true);
				return t;
			}
		});
		syncExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r);
				t.setDaemon(true);
				return t;
			}
		});

		// put in the queue empty task to make sure that everything was initialized
		// (additional managing thread was createed)
		asyncExecutorService.submit(new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				return null;
			}
		});
		syncExecutorService.submit(new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				return null;
			}
		});
	}

	/**
	 * Task that will be able to fetch mapping genome file with given version from
	 * externalserver.
	 * 
	 * @author Piotr Gawron
	 *
	 */
	private final class DownloadGeneMappingGenomeVersionTask implements Callable<Void> {

		/**
		 * Url to the file that we want to download.
		 * 
		 */
		private String					 url;

		/**
		 * Callback listener that will receive information about download progress.
		 * 
		 */
		private IProgressUpdater updater;

		/**
		 * Name of the gen genome mapping.
		 */
		private String					 name;

		/**
		 * Genome where mapping should be added.
		 */
		private ReferenceGenome	 referenceGenome;

		/**
		 * Default constructor.
		 * 
		 * @param url
		 *          {@link #url}
		 * @param updater
		 *          {@link #updater}
		 * @param referenceGenome
		 *          {@link #referenceGenome}
		 * @param name
		 *          {@link #name}
		 */
		private DownloadGeneMappingGenomeVersionTask(ReferenceGenome referenceGenome, String name, String url, IProgressUpdater updater) {
			this.url = url;
			this.referenceGenome = referenceGenome;
			this.name = name;
			if (updater != null) {
				this.updater = updater;
			} else {
				this.updater = new IProgressUpdater() {
					@Override
					public void setProgress(double progress) {
					}
				};
			}
		}

		@Override
		public Void call() throws Exception {
			getDbUtils().createSessionForCurrentThread();
			try {
				ReferenceGenome referenceGenome = getReferenceGenomeDao().getById(this.referenceGenome.getId());
				for (ReferenceGenomeGeneMapping mapping : referenceGenome.getGeneMapping()) {
					if (mapping.getName().equals(name)) {
						throw new ReferenceGenomeConnectorException("Gene mapping with name: \"" + name + "\" already exists.");
					}
				}
				if (!url.toLowerCase().endsWith("bb")) {
					throw new ReferenceGenomeConnectorException("Only big bed format files are supported but found: \"" + url + "\".");
				}
				ReferenceGenomeGeneMapping mapping = new ReferenceGenomeGeneMapping();
				mapping.setReferenceGenome(referenceGenome);
				mapping.setName(name);
				mapping.setSourceUrl(url);
				referenceGenome.addReferenceGenomeGeneMapping(mapping);
				getReferenceGenomeGeneMappingDao().add(mapping);
				getReferenceGenomeGeneMappingDao().flush();
				getReferenceGenomeDao().commit();
				getBigFileCache().downloadFile(url, false, new IProgressUpdater() {
					@Override
					public void setProgress(double progress) {
						if (updater != null) {
							updater.setProgress(progress);
						}
						// we have to get the object because it's in separate thred
						ReferenceGenomeGeneMapping temp = getReferenceGenomeGeneMappingDao().getById(mapping.getId());
						temp.setDownloadProgress(progress);
						getReferenceGenomeGeneMappingDao().update(temp);
						getReferenceGenomeGeneMappingDao().commit();
					}
				});
				ReferenceGenomeGeneMapping temp = getReferenceGenomeGeneMappingDao().getById(mapping.getId());
				temp.setDownloadProgress(100.0);
				getReferenceGenomeGeneMappingDao().update(temp);
				return null;
			} catch (Exception e) {
				logger.error(e, e);
				throw e;
			} finally {
				getDbUtils().closeSessionForCurrentThread();
			}
		}

	}

	@Override
	public void downloadGeneMappingGenomeVersion(ReferenceGenome referenceGenome, String name, IProgressUpdater updater, boolean async, String url)
			throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
		Callable<Void> computations = new DownloadGeneMappingGenomeVersionTask(referenceGenome, name, url, updater);
		if (async) {
			getAsyncExecutorService().submit(computations);
		} else {
			Future<Void> task = getSyncExecutorService().submit(computations);
			executeTask(task);
		}
	}

	/**
	 * @return the dbUtils
	 * @see #dbUtils
	 */
	public DbUtils getDbUtils() {
		return dbUtils;
	}

	/**
	 * @param dbUtils
	 *          the dbUtils to set
	 * @see #dbUtils
	 */
	public void setDbUtils(DbUtils dbUtils) {
		this.dbUtils = dbUtils;
	}

	/**
	 * @return the referenceGenomeDao
	 * @see #referenceGenomeDao
	 */
	public ReferenceGenomeDao getReferenceGenomeDao() {
		return referenceGenomeDao;
	}

	/**
	 * @param referenceGenomeDao
	 *          the referenceGenomeDao to set
	 * @see #referenceGenomeDao
	 */
	public void setReferenceGenomeDao(ReferenceGenomeDao referenceGenomeDao) {
		this.referenceGenomeDao = referenceGenomeDao;
	}

	/**
	 * @return the referenceGenomeGeneMappingDao
	 * @see #referenceGenomeGeneMappingDao
	 */
	public ReferenceGenomeGeneMappingDao getReferenceGenomeGeneMappingDao() {
		return referenceGenomeGeneMappingDao;
	}

	/**
	 * @param referenceGenomeGeneMappingDao
	 *          the referenceGenomeGeneMappingDao to set
	 * @see #referenceGenomeGeneMappingDao
	 */
	public void setReferenceGenomeGeneMappingDao(ReferenceGenomeGeneMappingDao referenceGenomeGeneMappingDao) {
		this.referenceGenomeGeneMappingDao = referenceGenomeGeneMappingDao;
	}

	/**
	 * @return the bigFileCache
	 * @see #bigFileCache
	 */
	public BigFileCache getBigFileCache() {
		return bigFileCache;
	}

	/**
	 * @param bigFileCache
	 *          the bigFileCache to set
	 * @see #bigFileCache
	 */
	public void setBigFileCache(BigFileCache bigFileCache) {
		this.bigFileCache = bigFileCache;
	}

	@Override
	public void removeGeneMapping(ReferenceGenomeGeneMapping mapping2) throws IOException {
		ReferenceGenomeGeneMapping dbMapping = getReferenceGenomeGeneMappingDao().getById(mapping2.getId());
		if (dbMapping == null) {
			logger.warn("Mapping doesn't exist in the DB");
		} else {
			if (getBigFileCache().isCached(dbMapping.getSourceUrl())) {
				getBigFileCache().removeFile(dbMapping.getSourceUrl());
			}
			ReferenceGenome genome = dbMapping.getReferenceGenome();
			genome.getGeneMapping().remove(dbMapping);
			getReferenceGenomeGeneMappingDao().delete(dbMapping);
		}
	}

	/**
	 * @return the asyncExecutorService
	 * @see #asyncExecutorService
	 */
	protected ExecutorService getAsyncExecutorService() {
		return asyncExecutorService;
	}

	/**
	 * @param asyncExecutorService
	 *          the asyncExecutorService to set
	 * @see #asyncExecutorService
	 */
	protected void setAsyncExecutorService(ExecutorService asyncExecutorService) {
		this.asyncExecutorService = asyncExecutorService;
	}

	/**
	 * @return the syncExecutorService
	 * @see #syncExecutorService
	 */
	protected ExecutorService getSyncExecutorService() {
		return syncExecutorService;
	}

	/**
	 * @param syncExecutorService
	 *          the syncExecutorService to set
	 * @see #syncExecutorService
	 */
	protected void setSyncExecutorService(ExecutorService syncExecutorService) {
		this.syncExecutorService = syncExecutorService;
	}

	/**
	 * Return number of tasks that are executed or are waiting for execution.
	 * 
	 * @return number of tasks that are executed or are waiting for execution
	 */
	public int getDownloadThreadCount() {
		return ((ScheduledThreadPoolExecutor) asyncExecutorService).getQueue().size() + ((ScheduledThreadPoolExecutor) asyncExecutorService).getActiveCount()
				+ ((ScheduledThreadPoolExecutor) syncExecutorService).getQueue().size() + ((ScheduledThreadPoolExecutor) syncExecutorService).getActiveCount();
	}

	/**
	 * Executes download/update task.
	 * 
	 * @param task
	 *          task to be executed
	 * @throws URISyntaxException
	 *           thrown when task finsihed with {@link URISyntaxException}
	 * @throws IOException
	 *           thrown when task finsihed with {@link IOException}
	 * @throws ReferenceGenomeConnectorException
	 *           thrown when there is a problem with genome connector
	 */
	void executeTask(Future<?> task) throws URISyntaxException, IOException, ReferenceGenomeConnectorException {
		try {
			task.get();
		} catch (InterruptedException e) {
			logger.error(e, e);
		} catch (ExecutionException e) {
			if (e.getCause() instanceof URISyntaxException) {
				throw (URISyntaxException) e.getCause();
			} else if (e.getCause() instanceof IOException) {
				throw new IOException((IOException) e.getCause());
			} else if (e.getCause() instanceof ReferenceGenomeConnectorException) {
				throw new ReferenceGenomeConnectorException((ReferenceGenomeConnectorException) e.getCause());
			} else {
				throw new InvalidStateException(e);
			}
		}
	}

}
