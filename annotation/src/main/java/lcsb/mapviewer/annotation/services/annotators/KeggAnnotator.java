package lcsb.mapviewer.annotation.services.annotators;


import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.UserAnnotatorsParam;

/**
 * This is a class that implements KEGG annotator which extract from KEGG 
 * PUBMED records and homologous information about homologous genes in 
 * different organisms based on parameterization of the annotator.
 * 
 * @author David Hoksza
 * 
 */
public class KeggAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Default class logger.
	 */
	private static Logger	logger					= Logger.getLogger(KeggAnnotator.class);
	
	/**
	 * Pattern used for finding PUBMED IDs in KEGG page.
	 */
	private Pattern				pubmedMatcher	= Pattern.compile("\\[PMID:(\\d+)\\]");

	/**
	 * Pattern used for finding ATH orghologs in KEGG GENE sectionpage.
	 */
	private Pattern				athOrthologMatcher	= Pattern.compile(" *ATH: (.*)");

	
	/**
	 * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS TAIR}.
	 */
	@Autowired
	private TairAnnotator		tairAnnotator;
	
	/**
	 * Service used for retrieving EC numbers based on {@link MiriamType#UNIPROT}
	 */
	@Autowired
	private UniprotAnnotator		uniprotAnnotator;

	/**
	 * Default constructor.
	 */
	public KeggAnnotator() {
		super(KeggAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
		AnnotatorParamDefinition paramDef = new AnnotatorParamDefinition(
				"KEGG organism identifier",				
				String.class,
				"Space-delimited list of organisms codes for which homologous genes"
				+ " (GENE section in the KEGG enzyme record) should be imported."
				+ " Currently only ATH (Arabidopsis Thaliana) is supported.");
		this.addParameterDefinition(paramDef);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {
			Species protein = new GenericProtein("id");
			MiriamData mdEC = createMiriamData(MiriamType.EC, "3.1.2.14");
			mdEC.setAnnotator(null);
			protein.addMiriamData(mdEC);
			annotateElement(protein);

			status.setStatus(ExternalServiceStatusType.OK);
			
			Set<String> pmids = new HashSet<String>();
			pmids.add("30409");
			pmids.add("3134");
			int cntMatches = 0;
			for (MiriamData md: protein.getMiriamData()) {
				if (pmids.contains(md.getResource())) {
					cntMatches++;
				}				
			}
			
			if (cntMatches != 2) {
				status.setStatus(ExternalServiceStatusType.CHANGED);
			}
		} catch (Exception e) {
			logger.error(status.getName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}
	
	
	@Override
	public void annotateElement(BioEntity object, List<UserAnnotatorsParam> params) throws AnnotatorException {
		
		if (!isAnnotatable(object)) { return; }
			
		MiriamData mdTair = null;
		for (MiriamData md : object.getMiriamData()) {
			Class<?> annotator =  md.getAnnotator();
			if (annotator == this.getClass()) {
				//this annotator was already used
				return;
			}
			else if (md.getDataType().equals(MiriamType.TAIR_LOCUS) && 
			 		(annotator == null ) ) {
			 	mdTair = md;
			}				
		}
		
		if (mdTair != null) tairAnnotator.annotateElement(object);
		
		MiriamData mdUniprot = null;
		for (MiriamData md : object.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.UNIPROT) ) {
			 	mdUniprot = md;
			}
		}
		if (mdUniprot != null) uniprotAnnotator.annotateElement(object);
		
		Set<String> ecs = new HashSet<String>();						
		for (MiriamData md : object.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.EC)) {					
				ecs.add(md.getResource());
			}
		}
		
		if (ecs.size() == 0) {				 
			return;
		}			
								
		//annotate from KEGG
		Set<MiriamData> annotations = new HashSet<MiriamData>();
		for (String ec: ecs) {
			
			String accessUrl = getKeggUrl(ec);
			
			try {
				String pageContent = getWebPageContent(accessUrl);
				annotations.addAll(parseKegg(pageContent, params));
					
			} catch (WrongResponseCodeIOException exception) {
				logger.warn("Cannot find kegg data for id: " + ec);
			} catch (IOException exception) {
				throw new AnnotatorException(exception);
			}
		}
		object.addMiriamData(annotations);
	}
	
	@Override
	public void annotateElement(BioEntity object) throws AnnotatorException {
		this.annotateElement(object, null);				
	}
	
	/**
	 * Returns url to KEGG restfull API about enzyme classification.
	 * 
	 * @param ecId
	 *          enzyme classification
	 * @return url to KEGG restfull API about given EC
	 */
	private String getKeggUrl(String ecId) {
		return "http://rest.kegg.jp/get/" + ecId;
	}
	
	/**
	 * Parse KEGG webpage to find information about
	 * {@link MiriamType#PUBMED}s and returns them.
	 * 
	 * @param pageContent
	 *          Kegg page
	 * @param params
	 * 			List of {@link UserAnnotatorsParam} to be used 
	 * 			for parameterization. Should contain only at most 
	 * 			one record with space separated KEGG organisms names.
	 * 			If the value has not been set by the user, 
	 * 			null will be passed. 
	 * @return 
	 * 			{@link MiriamType#PUBMED}s found on the page
	 */
	private Collection<MiriamData> parseKegg(String pageContent, List<UserAnnotatorsParam> params) {
		
		//Retrieve Pubmeds
		Collection<MiriamData> result = new HashSet<MiriamData>();
		Matcher m = pubmedMatcher.matcher(pageContent);
		while (m.find()) {
			result.add(createMiriamData(MiriamType.PUBMED, m.group(1)));
		}
		
		//Retrieve homologous organisms based on parameterization	
		if (params != null) {
			String[] keggOrgnismCodes = params.get(0).getParamValue().trim().split(" +");
			for (String code: keggOrgnismCodes) {
				if (!code.equalsIgnoreCase("ATH")) {
					logger.warn("KEGG annotator currently supports only ATH (" + code + " passed)" );
				} else {
					m = athOrthologMatcher.matcher(pageContent);
					if (m.find()) {
						String[] tairCodes = m.group(1).trim().split(" ");
						for (String tairCode: tairCodes) {
							tairCode = tairCode.split("\\(")[0]; //some codes are in the form AT1G08510(FATB)
							result.add(createMiriamData(MiriamType.TAIR_LOCUS, tairCode));
			            }
					}
				}
			}
		}
		
		return result;
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		String name;
		String result = null;
		if (query instanceof String) {
			name = (String) query;
			if (name.startsWith("http")) {
				try {
					result = getWebPageContent(name);
				} catch (IOException e) {
					throw new SourceNotAvailable(e);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with query: " + query);
			}
		} else {
			throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
		}
		return result;
	}
	

	@Override
	public String getCommonName() {
		return "KEGG";
	}

	@Override
	public String getUrl() {
		return "http://www.genome.jp/kegg/";
	}
	
	@Override
	public String getDescription() {
		return "Annotations extracted from KEGG ENZYME Database based on species EC numbers. "
				+ "Annotation include relevant publications and homologous genes for given EC numbers.";
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}
}
