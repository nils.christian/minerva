package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;

/**
 * This is a class that implements a backend to CAZy.
 * 
 * @author David Hoksza
 * 
 */
public class CazyAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Default class logger.
	 */
	private static Logger	logger					= Logger.getLogger(CazyAnnotator.class);
	
	/**
	 * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
	 * tair}.
	 */
	@Autowired
	private TairAnnotator		tairAnnotator;

	/**
	 * Pattern used for finding UniProt symbol from TAIR info page .
	 */
	private Pattern				cazyIdMatcher		= Pattern.compile("\\/((GT|GH|PL|CE|CBM)\\d+(\\_\\d+)?)\\.html");

	/**
	 * Default constructor.
	 */
	public CazyAnnotator() {
		super(CazyAnnotator.class, new Class[] { Protein.class, Gene.class, Rna.class }, false);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() { 
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {
			MiriamData md = uniprotToCazy(createMiriamData(MiriamType.UNIPROT, "Q9SG95"));

			status.setStatus(ExternalServiceStatusType.OK);
			if (md == null || !md.getResource().equalsIgnoreCase("GH5_7")) {
				status.setStatus(ExternalServiceStatusType.CHANGED);
			}
		} catch (Exception e) {
			logger.error(status.getName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}

	@Override
	public void annotateElement(BioEntity object) throws AnnotatorException {
		if (isAnnotatable(object)) {
			
			MiriamData mdTair = null;
			MiriamData mdCazy = null;
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.CAZY)) {
					mdCazy = md;
				}
				else if (md.getDataType().equals(MiriamType.TAIR_LOCUS)) {
					mdTair = md;
				}				
			}

			if (mdCazy != null) {
				return;
			}
			
			if (mdTair != null) {
				tairAnnotator.annotateElement(object);
			}
			
			List<MiriamData> mdUniprots = new ArrayList<MiriamData>();
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.UNIPROT)) {
					mdUniprots.add(md);
				}			
			}
			
			List<String> cazyIds = new ArrayList<String>();
			for (MiriamData mdUniprot: mdUniprots) {
				mdCazy = uniprotToCazy(mdUniprot);
				if (mdCazy != null && cazyIds.indexOf(mdCazy.getResource()) == -1) {
					cazyIds.add(mdCazy.getResource());
					object.addMiriamData(mdCazy);
				}
			}
		}		
	}

	/**
	 * Returns URL to TAIR page about TAIR entry.
	 * 
	 * @param uniProtId
	 *          UniProt identifier
	 * @return URL to CAZY UniProt accession search result page
	 */
	private String getCazyUrl(String uniProtId) {
		return "http://www.cazy.org/search?page=recherche&recherche=" + uniProtId + "&tag=10";
	}

	/**
	 * Parse CAZy webpage to find information about
	 * {@link MiriamType#CAZY} and returns them.
	 * 
	 * @param pageContent
	 *          CAZy info page
	 * @return CAZy family identifier found on the page
	 */
	private Collection<MiriamData> parseCazy(String pageContent) {
		Collection<MiriamData> result = new HashSet<MiriamData>();
		Matcher m = cazyIdMatcher.matcher(pageContent);
		if (m.find()) {
			result.add(createMiriamData(MiriamType.CAZY, m.group(1)));
		}
		return result;
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		String name;
		String result = null;
		if (query instanceof String) {
			name = (String) query;
			if (name.startsWith("http")) {
				try {
					result = getWebPageContent(name);
				} catch (IOException e) {
					throw new SourceNotAvailable(e);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with query: " + query);
			}
		} else {
			throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
		}
		return result;
	}

	/**
	 * Transform UniProt identifier to CAZy identifier.
	 * 
	 * @param UniProt
	 *          {@link MiriamData} with UniProt identifier
	 * @return {@link MiriamData} with CAZy identifier
	 * @throws AnnotatorException
	 *           thrown when there is a problem with accessing external database
	 */
	public MiriamData uniprotToCazy(MiriamData uniprot) throws AnnotatorException {
		if (uniprot == null) {
			return null;
		}

		if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
			throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
		}

		String accessUrl = getCazyUrl(uniprot.getResource());
		try {
			String pageContent = getWebPageContent(accessUrl);
			Collection<MiriamData> collection = parseCazy(pageContent);
			if (collection.size() > 0) {
				return collection.iterator().next();
			} else {
				logger.warn("Cannot find CAZy data for UniProt id: " + uniprot.getResource());
				return null;
			}
		} catch (WrongResponseCodeIOException exception) {
			logger.warn("Wrong response code when retrieving CAZy data for UniProt id: " + uniprot.getResource());
			return null;
		} catch (IOException exception) {
			throw new AnnotatorException(exception);
		}
	}

	@Override
	public String getCommonName() {
		return MiriamType.CAZY.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.CAZY.getDbHomepage();
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}

}
