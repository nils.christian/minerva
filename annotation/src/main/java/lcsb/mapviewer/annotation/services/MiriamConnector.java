package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.AnnotationException;

import com.google.gson.Gson;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import uk.ac.ebi.miriam.lib.MiriamLink;

/**
 * Class responsible for connection to Miriam DB. It allows to update URI of
 * database being used and retrieve urls for miriam uri.
 * 
 * @author Piotr Gawron
 * 
 */
public final class MiriamConnector extends CachableInterface implements IExternalService {

  /**
   * String used to distinguish cached data for links.
   */
  static final String LINK_DB_PREFIX = "Link: ";

  /**
   * String used to distinguish cached data for checking if uri is valid.
   */
  protected static final String VALID_URI_PREFIX = "Validity: ";

  /**
   * String describing invalid miriam entries that will be put into db (instead of
   * null).
   */
  private static final String INVALID_LINK = "INVALID";

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(MiriamConnector.class.getName());

  /**
   * Miriam Registry API.
   */
  private MiriamLink link;

  /**
   * Default class constructor. Prevent initialization.
   */
  public MiriamConnector() {
    super(MiriamConnector.class);
    link = new MiriamLink();
    link.setAddress("https://www.ebi.ac.uk/miriamws/main/MiriamWebServices");
  }

  /**
   * Returns url to the web page represented by {@link MiriamData} parameter.
   * 
   * @param miriamData
   *          miriam data
   * @return url to resource pointed by miriam data
   */
  public String getUrlString(MiriamData miriamData) {
    if (miriamData.getDataType() == null) {
      throw new InvalidArgumentException("Data type cannot be empty.");
    } else if (miriamData.getDataType().getUris().size() == 0) {
      throw new InvalidArgumentException("Url for " + miriamData.getDataType() + " cannot be retreived.");
    }
    String query = LINK_DB_PREFIX + miriamData.getDataType().getUris().get(0) + "\n" + miriamData.getResource();
    String result = getCacheValue(query);
    if (result != null) {
      if (INVALID_LINK.equals(result)) {
        return null;
      }
      return result;
    }
    // hard-coded specific treatment for mesh
    if (MiriamType.MESH_2012.equals(miriamData.getDataType())) {
      result = "http://bioportal.bioontology.org/ontologies/1351?p=terms&conceptid=" + miriamData.getResource();
    } else {
      String uri = miriamData.getDataType().getUris().get(0) + ":" + miriamData.getResource();
      String[] urls = getLink().getLocations(uri);
      if (urls == null) {
        result = null;
      } else if (urls.length > 0) {
        result = urls[0];
      }
    }
    if (result != null) {
      setCacheValue(query, result);
      return result;
    } else {
      logger.warn("Cannot find url for miriam: " + miriamData);
      // if url cannot be found then mark miriam data as invalid for one day
      setCacheValue(query, INVALID_LINK, 1);
      return null;
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("MIRIAM Registry", "https://www.ebi.ac.uk/miriam/main/");
    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      String url = getUrlString(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "3453"));
      status.setStatus(ExternalServiceStatusType.OK);
      if (url == null) {
        status.setStatus(ExternalServiceStatusType.DOWN);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Check if identifier can be transformed into {@link MiriamData}.
   * 
   * @param string
   *          identifier in the format NAME:IDENTIFIER. Where NAME is the name
   *          from {@link MiriamType#commonName} and IDENTIFIER is resource
   *          identifier.
   * @return <code>true</code> if identifier can be transformed into
   *         {@link MiriamData}
   */
  public boolean isValidIdentifier(String string) {
    try {
      MiriamType.getMiriamDataFromIdentifier(string);
      return true;
    } catch (InvalidArgumentException e) {
      return false;
    }
  }

  @Override
  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    String result = null;
    if (query instanceof String) {
      String name = (String) query;
      if (name.startsWith(LINK_DB_PREFIX)) {
        String tmp = name.substring(LINK_DB_PREFIX.length());
        String[] rows = tmp.split("\n");
        if (rows.length != 2) {
          throw new InvalidArgumentException("Miriam link query is invalid: " + query);
        }
        MiriamType dataType = MiriamType.getTypeByUri(rows[0]);
        String resource = rows[1];
        result = getUrlString(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, dataType, resource));
      } else if (name.startsWith(VALID_URI_PREFIX)) {
        String tmp = name.substring(VALID_URI_PREFIX.length());
        result = "" + isValid(tmp);
      } else if (name.startsWith("http")) {
        try {
          result = getWebPageContent(name);
        } catch (IOException e) {
          throw new SourceNotAvailable(e);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
      }
    } else {
      throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
    }

    return result;
  }

  /**
   * Checks if uri (like the one defined in {@link MiriamType#uris}) is valid.
   * 
   * @param uri
   *          uri to check
   * @return <code>true</code> if uri in parameter is valid, <code>false</code>
   *         otherwise
   */
  public boolean isValid(String uri) {
    String query = VALID_URI_PREFIX + uri;
    String val = getCacheValue(query);
    if (val != null) {
      return "true".equalsIgnoreCase(val);
    }

    boolean result = false;
    String name = getLink().getName(uri);
    if (name == null) {
      result = false;
    } else {
      result = !name.isEmpty();
    }
    setCacheValue(query, "" + result);
    return result;
  }

  /**
   * This is alternative version of {@link #getUrlString(MiriamData)} method that
   * is around 30% faster. But it refers to beta version of
   * <a href="https://www.ebi.ac.uk/miriamws/main/rest/">miriam Rest API</a>.
   * 
   * @param md
   *          miriam data for which access url will be returned
   * @return url to resource pointed by miriam data
   * @throws AnnotationException
   *           thrown when there is a problem with accessing miriam REST API
   */
  protected String getUrlString2(MiriamData md) throws AnnotationException {
    try {
      String result = null;
      String queryUri = md.getDataType().getUris().get(0) + ":" + md.getResource();
      String query = "https://www.ebi.ac.uk/miriamws/main/rest/resolve/" + queryUri;
      String page = getWebPageContent(query);
      Gson gson = new Gson();

      Map<?, ?> gsonObject = new HashMap<String, Object>();
      gsonObject = (Map<?, ?>) gson.fromJson(page, gsonObject.getClass());
      Object uris = gsonObject.get("uri");
      Map<?, ?> entry = (Map<?, ?>) ((List<?>) uris).get(0);
      result = (String) entry.get("$");

      return result;
    } catch (Exception e) {
      throw new AnnotationException("Problem with accessing miriam REST API", e);
    }
  }

  /**
   * Returns uri to miriam resource.
   * 
   * @param md
   *          {@link MiriamData} object for which uri should be returned
   * @return uri to miriam resource
   */
  public String miriamDataToUri(MiriamData md) {
    return md.getDataType().getUris().get(0) + ":" + md.getResource();
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

  /**
   * @return the link
   * @see #link
   */
  MiriamLink getLink() {
    return link;
  }

  /**
   * @param link
   *          the link to set
   * @see #link
   */
  void setLink(MiriamLink link) {
    this.link = link;
  }

}
