package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.genenames.org/">HGNC restfull API</a> and annotate
 * elements with information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public class HgncAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Address of HGNC API that should be used for retrieving data.
   */
  private static final String REST_API_URL = "https://rest.genenames.org/fetch/";

  @Autowired
  private MiriamConnector miriamConnector;

  /**
   * Default constructor.
   */
  public HgncAnnotator() {
    super(HgncAnnotator.class, new Class[] { Protein.class, Rna.class, Gene.class }, true);
  }

  @Override
  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    if (query instanceof String) {
      String name = (String) query;
      if (name.startsWith("http")) {
        try {
          String result = getWebPageContent(name);
          return result;
        } catch (IOException e) {
          throw new SourceNotAvailable(e);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with input string: " + name);
      }
    } else {
      throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
    }
  }

  /**
   * Standard class logger.
   */
  private final Logger logger = Logger.getLogger(HgncAnnotator.class);

  @Override
  public String getCommonName() {
    return MiriamType.HGNC.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.HGNC.getDbHomepage();
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias
          .addMiriamData(createMiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.HGNC_SYMBOL, "SNCA"));
      annotateElement(proteinAlias);

      if (proteinAlias.getFullName() == null || proteinAlias.getFullName().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (Exception e) {
      logger.error(getCommonName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public void annotateElement(BioEntity element) throws AnnotatorException {
    if (isAnnotatable(element)) {
      ElementUtils eu = new ElementUtils();
      String prefix = eu.getElementTag(element);
      List<String> symbols = new ArrayList<>();
      List<String> ids = new ArrayList<>();
      for (MiriamData md : element.getMiriamData()) {
        if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
          symbols.add(md.getResource());
        } else if (MiriamType.HGNC.equals(md.getDataType())) {
          ids.add(md.getResource());
        }
      }
      String content = null;
      String query = null;
      if (symbols.size() > 0) {
        if (symbols.size() > 1) {
          StringBuilder sb = new StringBuilder(
              prefix + "Too many hgnc symbols in element. Only one symbol acceptable but found: ");
          for (String string : symbols) {
            sb.append(string + ", ");
          }
          logger.warn(sb.toString());
        }

        query = getHgncNameUrl(symbols.get(0));
      } else if (ids.size() > 0) {
        if (ids.size() > 1) {
          StringBuilder sb = new StringBuilder(
              prefix + "Too many hgnc identifiers in element. Only one identifier acceptable but found: ");
          for (String string : ids) {
            sb.append(string + ", ");
          }
          logger.warn(sb.toString());
        }

        query = getHgncIdUrl(ids.get(0));
      } else {
        query = getHgncNameUrl(element.getName().replaceAll("/", "_"));
      }
      try {
        content = getWebPageContent(query);
        Node xml = getXmlDocumentFromString(content);
        Node response = getNode("response", xml.getChildNodes());
        Node result = getNode("result", response.getChildNodes());
        String count = getNodeAttr("numFound", result);
        if (count == null || count.equals("") || count.equals("0")) {
          if (element.getMiriamData().size() <= ids.size()) {
            // print warn info when we have only hgnc identifiers (or no
            // identifiers at all)
            logger.warn(prefix + "Cannot find information for element.");
          }
        } else {
          Node entry = getNode("doc", result.getChildNodes());

          NodeList list = entry.getChildNodes();
          for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
              if (node.getNodeName().equals("str")) {
                String type = getNodeAttr("name", node);
                if (type.equals("hgnc_id") && ids.size() == 0) {
                  // add hgnc id only when there was no hgnc_id in the element
                  String id = getNodeValue(node);
                  id = id.replaceAll("HGNC:", "");
                  element.addMiriamData(createMiriamData(MiriamType.HGNC, id));
                } else if (type.equals("ensembl_gene_id")) {
                  element.addMiriamData(createMiriamData(MiriamType.ENSEMBL, getNodeValue(node)));
                } else if (type.equals("entrez_id")) {
                  element.addMiriamData(createMiriamData(MiriamType.ENTREZ, getNodeValue(node)));
                } else if (type.equals("symbol")) {
                  if (symbols.size() == 0) {
                    // add hgnc symbol annnotation only when there was no
                    // hgnc_symbol in the element
                    element.addMiriamData(createMiriamData(MiriamType.HGNC_SYMBOL, getNodeValue(node)));
                  }
                  setSymbol(element, getNodeValue(node), prefix);
                } else if (type.equals("name")) {
                  setFullName((Element) element, getNodeValue(node), prefix);
                }
              } else if (node.getNodeName().equals("arr")) {
                String type = getNodeAttr("name", node);
                if (type.equals("refseq_accession")) {
                  NodeList sublist = node.getChildNodes();
                  for (int j = 0; j < sublist.getLength(); j++) {
                    Node subnode = sublist.item(j);
                    if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                      element.addMiriamData(createMiriamData(MiriamType.REFSEQ, getNodeValue(subnode)));
                    }
                  }
                } else if (type.equals("prev_symbol")) {
                  List<String> strings = new ArrayList<String>();
                  NodeList sublist = node.getChildNodes();
                  for (int j = 0; j < sublist.getLength(); j++) {
                    Node subnode = sublist.item(j);
                    if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                      strings.add(getNodeValue(subnode));
                    }
                  }
                  ((Element) element).setFormerSymbols(strings);
                } else if (type.equals("alias_symbol")) {
                  List<String> strings = new ArrayList<>();
                  NodeList sublist = node.getChildNodes();
                  for (int j = 0; j < sublist.getLength(); j++) {
                    Node subnode = sublist.item(j);
                    if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                      strings.add(getNodeValue(subnode));
                    }
                  }
                  setSynonyms(element, strings, prefix);
                } else if (type.equals("uniprot_ids")) {
                  NodeList sublist = node.getChildNodes();
                  for (int j = 0; j < sublist.getLength(); j++) {
                    Node subnode = sublist.item(j);
                    if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                      element.addMiriamData(createMiriamData(MiriamType.UNIPROT, getNodeValue(subnode)));
                    }
                  }
                }

              }
            }
          }
        }
      } catch (WrongResponseCodeIOException e) {
        logger.warn(prefix + "Cannot find information for element.");
      } catch (Exception e) {
        throw new AnnotatorException(e);
      }
    }
  }

  /**
   * Creates query url for given {@link MiriamType#HGNC} identifier.
   *
   * @param id
   *          {@link MiriamType#HGNC} identifier
   * @return url to restful api webpage for given identifier
   */
  private String getHgncIdUrl(String id) {
    return REST_API_URL + "hgnc_id/" + id;
  }

  /**
   * Creates query url for given {@link MiriamType#HGNC_SYMBOL}.
   *
   * @param name
   *          {@link MiriamType#HGNC_SYMBOL}
   * @return url to restful API web page for given HGNC symbol
   */
  private String getHgncNameUrl(String name) {
    String hgncSymbol = "" + name;
    hgncSymbol = hgncSymbol.split("\\s+")[0];
    return REST_API_URL + "symbol/" + hgncSymbol;
  }

  /**
   * Converts {@link MiriamType#HGNC}/{@link MiriamType#HGNC_SYMBOL} identifier
   * into list of {@link MiriamType#UNIPROT} identifiers.
   *
   * @param miriamData
   *          {@link MiriamData} with {@link MiriamType#HGNC} identifier
   * @return list of {@link MiriamData} with {@link MiriamType#UNIPROT}
   * @throws AnnotatorException
   *           thrown when there was problem with accessing external database or
   *           any other unknown problem
   */
  public List<MiriamData> hgncToUniprot(MiriamData miriamData) throws AnnotatorException {
    String query = null;
    if (MiriamType.HGNC.equals(miriamData.getDataType())) {
      query = getHgncIdUrl(miriamData.getResource());
    } else if (MiriamType.HGNC_SYMBOL.equals(miriamData.getDataType())) {
      query = getHgncNameUrl(miriamData.getResource());
    } else {
      throw new InvalidArgumentException("Only " + MiriamType.HGNC + " and " + MiriamType.HGNC_SYMBOL
          + " are accepted but " + miriamData.getDataType() + " found.");
    }
    try {
      List<MiriamData> result = new ArrayList<>();
      String content = getWebPageContent(query);
      Node xml = getXmlDocumentFromString(content);
      Node response = getNode("response", xml.getChildNodes());
      Node resultNode = getNode("result", response.getChildNodes());
      String count = getNodeAttr("numFound", resultNode);
      if (count == null || count.equals("") || count.equals("0")) {
        return result;
      } else {
        Node entry = getNode("doc", resultNode.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("arr")) {
              String type = getNodeAttr("name", node);
              if (type.equals("uniprot_ids")) {
                NodeList uniprotList = node.getChildNodes();
                for (int j = 0; j < uniprotList.getLength(); j++) {
                  Node uniprotNode = uniprotList.item(j);
                  if (uniprotNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (uniprotNode.getNodeName().equals("str")) {
                      result.add(createMiriamData(MiriamType.UNIPROT, uniprotNode.getTextContent()));
                    }
                  }
                }
              }
            }
          }
        }
      }
      return result;
    } catch (WrongResponseCodeIOException e) {
      logger.warn("No HGNC data found for id: " + miriamData);
      return new ArrayList<>();
    } catch (Exception e) {
      throw new AnnotatorException(e);
    }
  }

  /**
   * Converts {@link MiriamType#HGNC} identifier into
   * {@link MiriamType#HGNC_SYMBOL}.
   *
   * @param miriamData
   *          {@link MiriamData} with {@link MiriamType#HGNC} identifier
   * @return {@link MiriamData} with {@link MiriamType#HGNC_SYMBOL}
   * @throws AnnotatorException
   *           thrown when there was problem with accessing external database or
   *           any other unknown problem
   */
  public MiriamData hgncIdToHgncName(MiriamData miriamData) throws AnnotatorException {
    String query = null;
    if (MiriamType.HGNC.equals(miriamData.getDataType())) {
      query = getHgncIdUrl(miriamData.getResource());
    } else {
      throw new InvalidArgumentException(
          "Only " + MiriamType.HGNC + " is accepted but " + miriamData.getDataType() + " found.");
    }
    try {
      String content = getWebPageContent(query);
      Node xml = getXmlDocumentFromString(content);
      Node response = getNode("response", xml.getChildNodes());
      Node resultNode = getNode("result", response.getChildNodes());
      String count = getNodeAttr("numFound", resultNode);
      if (count == null || count.equals("") || count.equals("0")) {
        return null;
      } else {
        Node entry = getNode("doc", resultNode.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("str")) {
              String type = getNodeAttr("name", node);
              if (type.equals("symbol")) {
                return createMiriamData(MiriamType.HGNC_SYMBOL, node.getTextContent());
              }
            }
          }
        }
      }
    } catch (Exception e) {
      throw new AnnotatorException(e);
    }
    return null;
  }

  /**
   * Returns <code>true</code> if the {@link MiriamData} given in the parameter is
   * a valid {@link MiriamType#HGNC} or {@link MiriamType#HGNC_SYMBOL}.
   *
   * @param md
   *          {@link MiriamData} to validate
   * @return <code>true</code> if the {@link MiriamData} given in the parameter is
   *         a valid {@link MiriamType#HGNC} or {@link MiriamType#HGNC_SYMBOL}
   * @throws AnnotatorException
   *           thrown when there is a problem accessing HGNC restful AI
   */
  public boolean isValidHgncMiriam(MiriamData md) throws AnnotatorException {
    if (MiriamType.HGNC.equals(md.getDataType()) || MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
      String url = miriamConnector.getUrlString(md);
      if (url == null) {
        return false;
      }
      return hgncToUniprot(md).size() > 0;
    } else {
      return false;
    }
  }

  /**
   * Transforms HGNC symbol into {@link MiriamType#ENTREZ} identifier.
   *
   * @param md
   *          hgnc symbol
   * @return {@link MiriamType#ENTREZ} identifier for a given hgnc symbol
   * @throws AnnotatorException
   *           thrown when there is a problem with accessing hgnc server
   */
  public MiriamData hgncToEntrez(MiriamData md) throws AnnotatorException {
    String query = null;
    if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
      query = getHgncNameUrl(md.getResource());
    } else if (MiriamType.HGNC.equals(md.getDataType())) {
      query = getHgncIdUrl(md.getResource());
    } else {
      throw new InvalidArgumentException("Invalid miriam data: " + md);
    }
    try {
      String content = getWebPageContent(query);
      Node xml = getXmlDocumentFromString(content);
      Node response = getNode("response", xml.getChildNodes());
      Node result = getNode("result", response.getChildNodes());
      String count = getNodeAttr("numFound", result);
      if (count == null || count.equals("") || count.equals("0")) {
        return null;
      } else {
        Node entry = getNode("doc", result.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("str")) {
              String type = getNodeAttr("name", node);
              if (type.equals("entrez_id")) {
                String id = getNodeValue(node);
                return createMiriamData(MiriamType.ENTREZ, id);
              }
            }
          }
        }
      }
      return null;
    } catch (IOException e) {
      throw new AnnotatorException(e);
    } catch (InvalidXmlSchemaException e) {
      throw new AnnotatorException(e);
    }
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
