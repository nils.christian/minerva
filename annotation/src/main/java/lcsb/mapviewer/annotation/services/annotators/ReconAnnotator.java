package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This is a class that implements a backend to RECON database.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReconAnnotator extends ElementAnnotator implements IExternalService {

  /**
   * Address of annotation rest service for elements.
   */
  private static final String ELEMENT_ANNOTATION_URL_PREFIX = "https://vmh.uni.lu/_api/metabolites/?page_size=10000&format=json&search=";

  /**
   * Address of annotation rest service for reactions.
   */
  private static final String REACTION_ANNOTATION_URL_PREFIX = "https://vmh.uni.lu/_api/reactions/?page_size=10000&format=json&search=";

  /**
   * Class used for some simple operations on {@link BioEntity} elements.
   */
  private ElementUtils elementUtils = new ElementUtils();

  @Override
  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    String name;
    String result = null;
    if (query instanceof String) {
      name = (String) query;
      if (name.startsWith("http")) {
        try {
          result = getWebPageContent(name);
        } catch (IOException e) {
          throw new SourceNotAvailable(e);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
      }
    } else {
      throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
    }
    return result;
  }

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ReconAnnotator.class);

  /**
   * Default constructor.
   */
  public ReconAnnotator() {
    super(ReconAnnotator.class, new Class[] { Chemical.class, Reaction.class }, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);
    EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
    Logger.getRootLogger().addAppender(appender);
    try {
      SimpleMolecule smallMoleculeAlias = new SimpleMolecule("some_id");
      smallMoleculeAlias.setName("h2o");
      annotateElement(smallMoleculeAlias);
      status.setStatus(ExternalServiceStatusType.OK);
      if (smallMoleculeAlias.getMiriamData().size() == 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (appender.getWarnings().size() > 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    Logger.getRootLogger().removeAppender(appender);
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public void annotateElement(BioEntity annotatedObject) throws AnnotatorException {
    if (isAnnotatable(annotatedObject)) {
      String id = annotatedObject.getAbbreviation();
      if (id == null || id.isEmpty()) {
        id = annotatedObject.getName();
      }
      if (id.indexOf("[") > 0) {
        id = id.substring(0, id.indexOf("["));
      }
      if (id.indexOf(" ") >= 0) {
        String[] tmp = id.split(" ");
        id = tmp[tmp.length - 1];
      }
      String url = null;
      if (annotatedObject instanceof Element) {
        url = ELEMENT_ANNOTATION_URL_PREFIX + id;
      } else if (annotatedObject instanceof Reaction) {
        url = REACTION_ANNOTATION_URL_PREFIX + id;
      } else {
        logger.warn("Unknown class type: " + annotatedObject.getClass());
        return;
      }
      try {
        String content = getWebPageContent(url);

        String prefix = elementUtils.getElementTag(annotatedObject, this);
        if (!content.isEmpty()) {

          JsonElement jelement = new JsonParser().parse(content);
          JsonObject jobject = jelement.getAsJsonObject();
          if (jobject.get("results").getAsJsonArray().size() > 0) {
            jobject = jobject.get("results").getAsJsonArray().get(0).getAsJsonObject();

            for (Entry<String, JsonElement> entry : jobject.entrySet()) {
              String key = entry.getKey();
              String value = null;
              if (!entry.getValue().isJsonNull()) {
                // if (entry.getValue().isJsonPrimitive()) {
                value = entry.getValue().getAsString().trim();
              }
              if (value == null || value.isEmpty()) {
                continue;
              }

              boolean unknown = false;
              if (key.equals("url")) {
                continue;
              } else if (key.equals("rxn_id")) {
                continue;
              } else if (key.equals("abbreviation")) {
                setAbbreviation(annotatedObject, value, prefix);
              } else if (key.equals("description")) {
                setDescription(annotatedObject, value);
              } else if (key.equals("formula")) {
                setFormula(annotatedObject, value, prefix);
              } else if (key.equals("reversible")) {
                continue;
              } else if (key.equals("mcs")) {
                if (annotatedObject instanceof Reaction) {
                  setMcs((Reaction) annotatedObject, value, prefix);
                } else {
                  unknown = true;
                }
              } else if (key.equals("notes")) {
                setDescription(annotatedObject, value);
              } else if (key.equals("ecnumber")) {
                addEcMiriam(annotatedObject, value);
              } else if (key.equals("ref")) {
                continue;
              } else if (key.equals("lastModified")) {
                continue;
              } else if (key.equals("met_id")) {
                continue;
              } else if (key.equals("csecoida")) {
                continue;
              } else if (key.equals("chargedFormula")) {
                setFormula(annotatedObject, value, prefix);
              } else if (key.equals("charge")) {
                if (annotatedObject instanceof Species) {
                  setCharge((Species) annotatedObject, value, prefix);
                } else {
                  unknown = true;
                }
              } else if (key.equals("inchiString")) {
                if (annotatedObject instanceof Chemical) {
                  setInchi((Chemical) annotatedObject, value, prefix);
                } else {
                  unknown = true;
                }
              } else if (key.equals("cheBlId")) {
                addChebiMiriam(annotatedObject, value);
              } else if (key.equals("subsystem")) {
                if (annotatedObject instanceof Reaction) {
                  setSubsystem((Reaction) annotatedObject, value, prefix);
                } else {
                  unknown = true;
                }
              } else if (key.equals("keggId")) {
                addKeggMiriam(annotatedObject, value, prefix);
              } else if (key.equals("fullName")) {
                if (annotatedObject instanceof Element) {
                  setFullName((Element) annotatedObject, value, prefix);
                } else {
                  unknown = true;
                }
              } else if (key.equals("keggorthology")) {
                addKeggMiriam(annotatedObject, value, prefix);
              } else if (key.equals("pubChemId")) {
                addPubchemMiriam(annotatedObject, value);
              } else if (key.equals("hmdb")) {
                addHmdbMiriam(annotatedObject, value);
              } else if (key.equals("cog")) {
                addCogMiriam(annotatedObject, value);
              } else if (key.equals("synonyms")) {
                List<String> synonyms = new ArrayList<>();
                for (String str : value.split(",")) {
                  synonyms.add(str);
                }
                setSynonyms(annotatedObject, synonyms, prefix);
              } else if (key.equals("pdmapName")) {
                // for now we skip it...
                continue;
              } else if (key.equals("reconMap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("reconMap3")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("massbalance")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("isHuman")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("isMicrobe")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("iupac")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("avgmolweight")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("monoisotopicweight")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("miriam")) {
                addMiriam(annotatedObject, value, prefix);
              } else if (key.equals("biggId")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("lmId")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("hepatonetId")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("inchiKey")) {
                if (annotatedObject instanceof Chemical) {
                  setInchiKey((Chemical) annotatedObject, value, prefix);
                }
              } else if (key.equals("smile")) {
                if (annotatedObject instanceof Chemical) {
                  setSmile((Chemical) annotatedObject, value, prefix);
                }
              } else if (key.equals("metanetx")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("seed")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("food_db")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("chemspider")) {
                addChemspider(annotatedObject, value);
              } else if (key.equals("wikipedia")) {
                addWikipedia(annotatedObject, value);
              } else if (key.equals("metlin")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("casRegistry")) {
                addCas(annotatedObject, value);
              } else if (key.equals("epa_id")) {
                // for now we don't handle it
                continue;
              } else {
                unknown = true;
              }
              if (unknown) {
                logger.warn(prefix + "Unknown field in recon annotation: \"" + key + "\" (value: " + value + ")");
              }
            }
          } else {
            logger.warn(prefix + "No recon annotations \"" + id + "\"");
          }
        } else {
          logger.warn(prefix + "No recon annotations \"" + id + "\"");
        }
      } catch (IOException e) {
        throw new AnnotatorException(e);
      }
    }
  }

  @Override
  public String getCommonName() {
    return "Recon annotator";
  }

  @Override
  public String getUrl() {
    return "http://humanmetabolism.org/";
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

}
