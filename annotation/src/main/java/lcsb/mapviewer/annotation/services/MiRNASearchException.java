package lcsb.mapviewer.annotation.services;

/**
 * Exception thrown when there was a problem when searching for a mirna.
 * 
 * @author Piotr Gawron
 * 
 */
public class MiRNASearchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 * 
	 * @param string
	 *          exception message
	 */
	public MiRNASearchException(String string) {
		super(string);
	}

	/**
	 * Default constructor.
	 * 
	 * @param e
	 *          parent exception
	 */
	public MiRNASearchException(Exception e) {
		super(e);
	}

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          exception message
	 * @param e
	 *          source exception
	 */
	public MiRNASearchException(String message, Exception e) {
		super(message, e);
	}
}
