package lcsb.mapviewer.annotation.services.annotators;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Chebi;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

/**
 * This is a class that implements STITCH annotation which is derived from existing ChEBI annotation.
 * 
 * @author David Hoksza
 * 
 */
public class StitchAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Default class logger.
	 */
	private static Logger	logger					= Logger.getLogger(StitchAnnotator.class);
	
	/**
	 * Service used for annotation of entities using {@link MiriamType#STITCH STITCH}.
	 */
	@Autowired
	private ChebiAnnotator		chebiAnnotator;

	/**
	 * Default constructor.
	 */
	public StitchAnnotator() {
		super(StitchAnnotator.class, new Class[] { SimpleMolecule.class }, false);
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		return chebiAnnotator.getServiceStatus();
	}
	
	/**
	 * Returns main layer of the InchKey, * i.e. everything 
	 * before first hyphen: WBYWAXJHAXSJNI-VOTSOKGWSA-N -> WBYWAXJHAXSJNI	   
	 * @param inchiKey Full inchiKey
	 * @return Main layer of the InchiKey
	 */
	private String inchiKeyExtractMainLayer(String inchiKey){
		return inchiKey.replaceFirst("-.*", "");
	}

	@Override
	public void annotateElement(BioEntity object) throws AnnotatorException {
		if (isAnnotatable(object)) {
			
			MiriamData mdChebi = null;
			
			for (MiriamData md : object.getMiriamData()) {
				if (md.getDataType().equals(MiriamType.CHEBI)) {
					mdChebi = md;
				}
				else if (md.getDataType().equals(MiriamType.STITCH)) {
					return;
				}				
			}
			
			if (mdChebi == null) {
				return;
			}
			
			try {
				
				Chebi chebi = chebiAnnotator.getChebiElementForChebiId(mdChebi);
				if (chebi == null) {
					return;
				}
				String inchiKey = chebi.getInchiKey();
				if (inchiKey == null) {
					return;
				}
				
				object.addMiriamData(createMiriamData(MiriamType.STITCH, inchiKeyExtractMainLayer(inchiKey)));				
			} catch(ChebiSearchException exception) {
				logger.warn("No ChEBI element retrieved fro ChEBI ID: " + mdChebi.getResource());
			}			
		}		
	}

	@Override
	public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
		return chebiAnnotator.refreshCacheQuery(query);
	}
	

	@Override
	public String getCommonName() {
		return MiriamType.STITCH.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.STITCH.getDbHomepage();
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}
}
