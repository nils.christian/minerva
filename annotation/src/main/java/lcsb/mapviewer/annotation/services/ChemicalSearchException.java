package lcsb.mapviewer.annotation.services;

/**
 * Exception thrown when there was a problem when searching for a chemical.
 * 
 * @author Piotr Gawron
 * 
 */
public class ChemicalSearchException extends Exception {

	/**
	 * Default constructor.
	 * 
	 * @param string
	 *          exception message
	 */
	public ChemicalSearchException(String string) {
		super(string);
	}

	/**
	 * Default constructor.
	 * 
	 * @param e
	 *          parent exception
	 */
	public ChemicalSearchException(Exception e) {
		super(e);
	}

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          exception message
	 * @param e
	 *          source exception
	 */
	public ChemicalSearchException(String message, Exception e) {
		super(message, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
