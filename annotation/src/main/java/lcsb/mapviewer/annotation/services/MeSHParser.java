package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.SerializationException;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.google.gson.Gson;
import com.google.gson.internal.StringMap;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * Class used for accessing and parsing data from MeSH database.
 * 
 * @author Ayan Rota
 * 
 */
public class MeSHParser extends CachableInterface implements IExternalService {

  /**
   * Url used for searching drugs by name.
   */
  private static final String URL_MESH_DATABASE = "https://meshb.nlm.nih.gov/api/record/ui/";

  /**
   * Url used for searching mesh terms by synonym.
   */
  private static final String URL_SEARCH_BY_SYNONYM = "https://meshb.nlm.nih.gov/api/search/record?searchInField=termDescriptor&sort=&size=20&searchType=exactMatch&searchMethod=FullWord&q=";

  /**
   * Prefix used in the DB to identify the cache entry.
   */
  static final String MESH_PREFIX = "mesh:";

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(MeSHParser.class);

  /**
   * Object that allows to serialize {@link MeSH} elements into xml string and
   * deserialize xml into {@link MeSH} objects.
   */
  private XmlSerializer<MeSH> meshSerializer;

  /**
   * Default constructor.
   */
  public MeSHParser() {
    super(MeSHParser.class);
    meshSerializer = new XmlSerializer<>(MeSH.class);
  }

  @Override
  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String identifier = (String) query;
        if (identifier.startsWith("http")) {
          result = getWebPageContent(identifier);
        } else if (identifier.startsWith(MESH_PREFIX)) {
          String[] ids = identifier.split("\\:");
          if (ids.length == 2) {
            MiriamData meshID = new MiriamData(MiriamType.MESH_2012, ids[1]);
            MeSH mesh = getMeSHByIdFromDB(meshID);
            result = meshSerializer.objectToString(mesh);
          } else {
            throw new InvalidArgumentException("Problematic query: \"" + query + "\"");
          }
        } else {
          throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (IOException e) {
      throw new SourceNotAvailable("Problem with accessing Mesh database", e);
    }
    return result;
  }

  /**
   * @param meshID
   *          miriam type id.
   * @return returns object.
   * @throws AnnotatorException
   *           thrown when there is a problem with accessing mesh database
   */
  public MeSH getMeSH(MiriamData meshID) throws AnnotatorException {
    if (meshID == null || meshID.getResource() == null) {
      throw new InvalidArgumentException("mesh cannot be null ");
    }

    MeSH mesh = null;
    // look for Mesh in the cache
    String id = getIdentifier(meshID);
    Node meshNode = super.getCacheNode(id);
    if (meshNode != null && meshNode.hasChildNodes()) {
      try {
        mesh = meshSerializer.xmlToObject(meshNode);
      } catch (SerializationException e) {
        logger.warn("Problem with processing cached info about mesh: " + meshID);
        mesh = null;
      }
    }
    if (mesh == null) {
      try {
        mesh = getMeSHByIdFromDB(meshID);
      } catch (IOException e) {
        throw new AnnotatorException("Problem with accessing MeSH database", e);
      }
    }
    if (mesh != null) {
      super.setCacheValue(id, this.meshSerializer.objectToString(mesh));
    }

    return mesh;
  }

  /**
   * @param meshID
   *          id as miriam data.
   * @return return the object.
   */
  public String getIdentifier(MiriamData meshID) {
    return MESH_PREFIX + meshID.getResource();
  }

  /**
   * @param meshID
   *          mesh id as Miriam data.
   * @return return as mesh object.
   * @throws IOException
   *           thrown when there is problem with accessing web page
   * @throws AnnotatorException
   *           thrown when there is a problem with accessing mesh db
   */
  private MeSH getMeSHByIdFromDB(MiriamData meshID) throws IOException {
    try {
      MeSH result = new MeSH();
      String page = getWebPageContent(URL_MESH_DATABASE + meshID.getResource());

      Gson gson = new Gson();

      Map<?, ?> gsonObject = new HashMap<String, Object>();
      gsonObject = (Map<?, ?>) gson.fromJson(page, gsonObject.getClass());
      Set<String> synonyms = getSynonyms(gsonObject);
      String name = getName(gsonObject);
      String description = getDescription(gsonObject);
      String id = getId(gsonObject);
      synonyms.remove(name);
      result.addSynonyms(synonyms);
      result.setName(name);
      result.setDescription(description);
      result.setMeSHId(id);

      return result;
    } catch (WrongResponseCodeIOException e) {
      if (e.getResponseCode() == HttpStatus.SC_NOT_FOUND) {
        return null;
      } else {
        throw e;
      }
    }
  }

  /**
   * Extracts name from gson object.
   * 
   * @param gsonObject
   *          gson to process
   * @return name of {@link MeSH} entry
   */
  private String getName(Map<?, ?> gsonObject) {
    StringMap<?> descriptorTag = (StringMap<?>) gsonObject.get("DescriptorName");
    if (descriptorTag == null) {
      descriptorTag = (StringMap<?>) gsonObject.get("SupplementalRecordName");
    }
    return (String) (((StringMap<?>) descriptorTag.get("String")).get("t"));
  }

  /**
   * Extracts Mesh id name from gson object.
   * 
   * @param gsonObject
   *          gson to process
   * @return id of {@link MeSH} entry
   */
  private String getId(Map<?, ?> gsonObject) {
    StringMap<?> descriptorTag = (StringMap<?>) gsonObject.get("DescriptorUI");
    if (descriptorTag == null) {
      descriptorTag = (StringMap<?>) gsonObject.get("SupplementalRecordUI");
    }
    return (String) descriptorTag.get("t");
  }

  /**
   * Extracts Mesh term description from gson object.
   * 
   * @param gsonObject
   *          gson to process
   * @return description of {@link MeSH} entry
   */
  private String getDescription(Map<?, ?> gsonObject) {
    StringMap<?> concepts = (StringMap<?>) gsonObject.get("_generated");
    return (String) concepts.get("PreferredConceptScopeNote");
  }

  /**
   * Extracts list of synonyms from gson object.
   * 
   * @param gsonObject
   *          gson to process
   * @return synonyms of {@link MeSH} entry
   */
  private Set<String> getSynonyms(Map<?, ?> gsonObject) {
    Set<String> synonyms = new HashSet<>();
    StringMap<?> concepts = (StringMap<?>) gsonObject.get("ConceptList");
    ArrayList<?> conceptList = (ArrayList<?>) concepts.get("Concept");
    for (Object object : conceptList) {
      StringMap<?> concept = (StringMap<?>) object;
      ArrayList<?> termList = (ArrayList<?>) ((StringMap<?>) concept.get("TermList")).get("Term");
      for (Object object2 : termList) {
        StringMap<?> term = (StringMap<?>) object2;
        StringMap<?> synonym = (StringMap<?>) term.get("String");
        synonyms.add((String) synonym.get("t"));

      }
    }
    return synonyms;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);
    ExternalServiceStatus status = new ExternalServiceStatus(MiriamType.MESH_2012.getCommonName(),
        MiriamType.MESH_2012.getDbHomepage());
    try {
      status.setStatus(ExternalServiceStatusType.OK);
      MiriamData meshId = new MiriamData(MiriamType.MESH_2012, "D010300");
      MeSH mesh = getMeSH(meshId);
      if (mesh == null || !meshId.getResource().equals(mesh.getMeSHId())) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error(e, e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Checks if the mesh identifier is valid.
   * 
   * @param meshId
   *          mesh id
   * @return <code>true</code> if it's valid
   * @throws AnnotatorException
   *           thrown when there is problem with accessing mesh db
   */
  public boolean isValidMeshId(MiriamData meshId) throws AnnotatorException {
    if (meshId == null) {
      return false;
    }
    return getMeSH(meshId) != null;
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

  public List<MeSH> getMeshBySynonym(String synonym) throws AnnotatorException {
    try {
      List<MeSH> result = new ArrayList<>();
      String page = getWebPageContent(URL_SEARCH_BY_SYNONYM + synonym);
      Gson gson = new Gson();

      Map<?, ?> gsonObject = new HashMap<String, Object>();
      gsonObject = (Map<?, ?>) gson.fromJson(page, gsonObject.getClass());
      Set<MiriamData> synonyms = getIdsBySynonymQuery(gsonObject);
      for (MiriamData meshID : synonyms) {
        result.add(getMeSH(meshID));
      }

      return result;
    } catch (IOException e) {
      throw new AnnotatorException(e);
    }
  }

  private Set<MiriamData> getIdsBySynonymQuery(Map<?, ?> gsonObject) {
    Set<MiriamData> result = new HashSet<>();
    StringMap<?> hits = (StringMap<?>) gsonObject.get("hits");
    ArrayList<?> hitsList = (ArrayList<?>) hits.get("hits");
    for (Object object : hitsList) {
      StringMap<?> hit = (StringMap<?>) object;
      String  id = (String) hit.get("_id");
      result.add(new MiriamData(MiriamType.MESH_2012, id));
    }
    return result;
  }

}
