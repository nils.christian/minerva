package lcsb.mapviewer.annotation.services.genome;

/**
 * Thrown when there is aproblem with accessing reference genome database or
 * there are problems with performing some operation on reference genome
 * connection.
 * 
 * @author Piotr Gawron
 *
 */
public class ReferenceGenomeConnectorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          message associated with exception
	 * @param exception
	 *          original exception
	 */
	public ReferenceGenomeConnectorException(String message, Exception exception) {
		super(message, exception);
	}

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          message associated with exception
	 */
	public ReferenceGenomeConnectorException(String message) {
		super(message);
	}

	/**
	 * Default constructor.
	 * 
	 * @param cause
	 *          original exception
	 */
	public ReferenceGenomeConnectorException(Exception cause) {
		super(cause);
	}

}
