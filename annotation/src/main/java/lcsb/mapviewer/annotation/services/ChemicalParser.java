package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.ChemicalDirectEvidence;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.ProjectDao;

/**
 * This class is a backend to Toxicogenomic Chemical API.
 * 
 * @author Ayan Rota
 * 
 */
public class ChemicalParser extends CachableInterface implements IExternalService {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ChemicalParser.class);

  /**
   * Prefix used for caching elements with chemical identifier as a key.
   */
  static final String CHEMICAL_PREFIX = "chemical:";

  /**
   * Prefix used for caching all chemical names in a disease.
   */
  protected static final String DISEASE_CHEMICALS_PREFIX = "DISEASE_CHEMICALS:";

  /**
   * Prefix used for caching list of suggested queries for project.
   */
  protected static final String PROJECT_SUGGESTED_QUERY_PREFIX = "PROJECT_CHEMICAL_QUERIES:";

  /**
   * Home page of ctd database.
   */
  static final String URL = "https://ctdbase.org/";

  /**
   * URL to get a list of chemicals by disease id.
   */
  public static final String DISEASE_URL = "https://ctdbase.org/detail.go?6578706f7274=1&d-1332398-e=5&view=chem&"
      + "type=disease&acc=MESH%3A";

  /**
   * URL to get a list of chemicals by gene id.
   */
  public static final String DISEASE_GENE_URL = "https://ctdbase.org/detail.go?slimTerm=all&6578706f7274=1&qid=3464576&"
      + "d-1332398-e=5&view=disease&type=gene&assnType=curated&acc=";

  /**
   * Column in results obtained from {@link ChemicalParser#DISEASE_GENE_URL}
   * describing drugs that are related to the gene (gene identifier is provided in
   * the url).
   */
  private static final int DRUG_NAME_COL_IN_GENE_DRUG_LIST = 3;

  /**
   * The location of name column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_NAME_COL = 0;

  /**
   * The location of id column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_ID_COL = 1;

  /**
   * The location of CAS column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_CAS_COL = 2;

  /**
   * The location of disease name column in the parsed tab separated chemicals
   * file.
   */
  public static final Integer CHEMICALS_DISEASE_NAME_COL = 3;

  /**
   * The location of disease id column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_DISEASE_ID_COL = 4;

  /**
   * The location of direct column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_DIRECT_COL = 5;

  /**
   * The location of network of genes column in the parsed tab separated chemicals
   * file.
   */
  public static final Integer CHEMICALS_NETWORK_COL = 6;

  /**
   * The location of score column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_SCORE_COL = 7;

  /**
   * The location of count column in the parsed tab separated chemicals file.
   */
  public static final Integer CHEMICALS_COUNT_COL = 8;

  /**
   * The location of pubmed id column in the publications.
   */
  public static final Integer PUBLICATIONS_ID_COL = 0;

  /**
   * The location of list of genes column that is associated with this publication
   * for particular chemical.
   */
  public static final Integer PUBLICATIONS_ASSOCIATION_COL = 5;

  /**
   * Object that allows to serialize {@link Chemical} elements into xml string and
   * deserialize xml into {@link Chemical} objects.
   */
  private XmlSerializer<Chemical> chemicalSerializer;

  /**
   * The status of the db.
   */
  private ExternalServiceStatus status = new ExternalServiceStatus("ctdbase", URL);

  /**
   * Object used to perform operations on {@link MiriamType#HGNC hgnc
   * annotations}.
   */
  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private MeSHParser meshParser;

  /**
   *
   */
  public ChemicalParser() {
    super(ChemicalParser.class);
    chemicalSerializer = new XmlSerializer<>(Chemical.class);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    GeneralCacheInterface cacheCopy = getCache();
    this.setCache(null);
    status.setStatus(ExternalServiceStatusType.OK);
    try {
      MiriamData diseaseID = new MiriamData(MiriamType.MESH_2012, "D002095");
      MiriamData chemicalID = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, "D000395");
      Chemical chemical = getChemicalFromExternalDatabase(diseaseID, chemicalID);
      if (chemical == null || !chemical.getChemicalId().equals(chemicalID)) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (Exception e) {
      logger.error("ctdbase service unavailable", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;

  }

  /**
   * @param disease
   *          the MESH Id for the disease
   * @return list of chemicals id, name pairs
   * @throws ChemicalSearchException
   *           thrown when there is problem with accessing ctd database
   */
  public Map<MiriamData, String> getChemicalsForDisease(MiriamData disease) throws ChemicalSearchException {
    if (disease == null || disease.getResource() == null) {
      throw new InvalidArgumentException("disease cannot be null");
    }

    // check if we have results in cache
    String chemicalNode = super.getCacheValue(DISEASE_CHEMICALS_PREFIX + disease.getResource());
    if (chemicalNode != null) {
      return deserializeChemicalList(chemicalNode);
    }

    Map<MiriamData, String> chemicals = new HashMap<>();
    try {
      String diseaseID = disease.getResource();
      String query = DISEASE_URL + diseaseID;
      String page = getWebPageContent(query);

      String[] lines = page.split("\n");
      for (int rowNumber = 1; rowNumber < lines.length; rowNumber++) {
        String[] lineValues = lines[rowNumber].split("\t");
        String diseaseId = lineValues[CHEMICALS_DISEASE_ID_COL];
        diseaseId = MiriamData.getIdFromIdentifier(diseaseId);
        if (diseaseId != null && diseaseID.equals(diseaseId) && lineValues[CHEMICALS_ID_COL] != null) {
          chemicals.put(new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, lineValues[CHEMICALS_ID_COL]),
              lineValues[CHEMICALS_NAME_COL]);
        }
      }

    } catch (WrongResponseCodeIOException e) {
      // it means that there are no results for this query
    } catch (IOException e) {
      throw new ChemicalSearchException("ctdbase service unavailable", e);
    }

    // put the results into cache
    String cacheResult = serializeChemicalList(chemicals);
    super.setCacheValue(DISEASE_CHEMICALS_PREFIX + disease.getResource(), cacheResult);

    return chemicals;

  }

  /**
   * Serializes list of chemicalId-chemicalName pairs.
   *
   * @param chemicals
   *          list of objects to serialize
   * @return serialized string
   */
  public String serializeChemicalList(Map<MiriamData, String> chemicals) {
    StringBuilder cacheResult = new StringBuilder();
    for (MiriamData md : chemicals.keySet()) {
      cacheResult.append(md.getResource() + "\t" + chemicals.get(md) + "\n");
    }
    return cacheResult.toString();
  }

  /**
   * Deserializes list of chemicalId-chemicalName pairs.
   *
   * @param chemicalNode
   *          string to deserialize
   * @return list of chemicalId-chemicalName pairs
   */
  public Map<MiriamData, String> deserializeChemicalList(String chemicalNode) {
    Map<MiriamData, String> chemicals = new HashMap<>();

    String[] lines = chemicalNode.split("\n");
    for (String string : lines) {
      if (string.length() > 0) {
        String[] tmp = string.split("\t");
        String id = tmp[0];
        String name = tmp[1];
        chemicals.put(new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, id), name);
      }
    }
    return chemicals;
  }

  /**
   * @param diseaseID
   *          The disease id that the chemical IDs belong to.
   * @param chemicalIDs
   *          The list of the chemical ID required to be retrieved.
   * @return list of chemicals
   * @throws ChemicalSearchException
   *           thrown when there is problem with accessing ctd database
   */
  public List<Chemical> getChemicals(MiriamData diseaseID, Collection<MiriamData> chemicalIDs)
      throws ChemicalSearchException {
    if (diseaseID == null) {
      throw new InvalidArgumentException("disease cannot be null");
    }
    List<Chemical> chemicals = new ArrayList<>();

    for (MiriamData chemicalId : chemicalIDs) {
      Chemical chemical = this.getChemicalFromExternalDatabase(diseaseID, chemicalId);
      if (chemical != null) {
        chemicals.add(chemical);
      }
    }
    return chemicals;
  }

  /**
   * @param diseaseID
   *          Miriam data type for disease id
   * @param chemicalID
   *          Miriam data type for chemical id
   * @return formated resource
   */
  public String getIdentifier(MiriamData diseaseID, MiriamData chemicalID) {
    return CHEMICAL_PREFIX + diseaseID.getResource() + ":" + chemicalID.getResource();
  }

  @Override
  public Object refreshCacheQuery(Object query) throws SourceNotAvailable {
    try {
      Object result = null;
      if (query instanceof String) {
        String identifier = (String) query;
        if (identifier.startsWith(CHEMICAL_PREFIX)) {
          String[] ids = identifier.split("\\:");
          MiriamData diseaseID = new MiriamData(MiriamType.MESH_2012, ids[1]);
          MiriamData chemicalID = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, ids[2]);
          Chemical chemical = getChemicalFromExternalDatabase(diseaseID, chemicalID);
          if (chemical != null) {
            result = chemicalSerializer.objectToString(chemical);
          } else {
            throw new SourceNotAvailable("Could not find the resource with string \"" + query + "\"");
          }
        } else if (identifier.startsWith(DISEASE_CHEMICALS_PREFIX)) {
          String id = identifier.substring(DISEASE_CHEMICALS_PREFIX.length());
          Map<MiriamData, String> resultObj = getChemicalsForDisease(new MiriamData(MiriamType.MESH_2012, id));
          result = serializeChemicalList(resultObj);
        } else if (identifier.startsWith(PROJECT_SUGGESTED_QUERY_PREFIX)) {
          String[] tmp = identifier.split("\n");
          Integer id = Integer.valueOf(tmp[1]);
          MiriamData diseaseId = new MiriamData(MiriamType.MESH_2012, tmp[2]);
          Project project = projectDao.getById(id);
          if (project == null) {
            throw new SourceNotAvailable("Project with given id doesn't exist: " + id);
          }
          List<String> list = getSuggestedQueryListWithoutCache(project, diseaseId);
          result = StringUtils.join(list, "\n");
        } else {
          result = super.refreshCacheQuery(query);
        }
      } else {
        result = super.refreshCacheQuery(query);
      }
      return result;
    } catch (ChemicalSearchException e) {
      throw new SourceNotAvailable("Problem with accessing ctd database", e);
    }
  }

  /**
   * @param diseaseID
   *          the Mesh id of the disease.
   * @param chemicalId
   *          the chemical id
   * @return list of chemical objects.
   * @throws ChemicalSearchException
   *           thrown when there is problem with accessing ctd database
   */
  private Chemical getChemicalFromExternalDatabase(MiriamData diseaseID, MiriamData chemicalId)
      throws ChemicalSearchException {
    Chemical result = null;
    // look for chemical in the cache
    Node chemicalNode = super.getCacheNode(getIdentifier(diseaseID, chemicalId));
    if (chemicalNode != null && chemicalNode.hasChildNodes()) {
      try {
        result = chemicalSerializer.xmlToObject(chemicalNode);
        return result;
      } catch (SerializationException e) {
        logger.error(e, e);
      }
    }
    try {
      String diseaseQuery = DISEASE_URL + diseaseID.getResource();
      String diseasePage = getWebPageContent(diseaseQuery);
      String[] diseaseLines = diseasePage.split("\n");
      for (int rowNumber = 1; rowNumber < diseaseLines.length; rowNumber++) {
        String[] lineValues = diseaseLines[rowNumber].split("\t", -1);
        MiriamData chemID = new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, lineValues[CHEMICALS_ID_COL]);
        String diseaseIdentifier = MiriamData.getIdFromIdentifier(lineValues[CHEMICALS_DISEASE_ID_COL]);
        MiriamData currentDiseaseId = new MiriamData(MiriamType.MESH_2012, diseaseIdentifier);
        if (chemicalId.equals(chemID) && diseaseID.equals(currentDiseaseId)) {
          result = getChemicalFromChemicalLine(lineValues, chemID);
        }
      }
      if (result != null) {
        MeSH mesh = meshParser.getMeSH(result.getChemicalId());
        if (mesh!=null) {
        result.addSynonyms(mesh.getSynonyms());
        } else {
          logger.warn("Problematic mesh id: "+result.getChemicalId());
        }
      }

    } catch (IOException e) {
      throw new ChemicalSearchException("ctdbase service unavailable", e);
    } catch (AnnotatorException e) {
      throw new ChemicalSearchException("problem with accessing mesh db", e);
    }

    // put them in cache
    super.setCacheValue(getIdentifier(diseaseID, chemicalId), chemicalSerializer.objectToString(result));

    return result;
  }

  /**
   * Method that creates chemical from a tab separated row from ctd database. Data
   * looks like (line wrapping added for clarity):
   *
   * <pre>
  1. 	Disease Name	Disease ID	Direct Evidence	Inference Network	Inference Score	Reference Count
  2.	Drug-Induced Liver Injury	MESH:D056486		1-(2-trifluoromethoxyphenyl)-2-nitroethanone|1,4-bis(2-(3,5-dichloropyridyloxy))benzene|
    	2,4-dinitrotoluene|2,6-dinitrotoluene|Acetaminophen|Acetylcysteine|Aflatoxin B1|Azacitidine|Benzo(a)pyrene|Estradiol|Bilirubin|
  		bisphenol A|Buthionine Sulfoximine|Carbamazepine|Chlorodiphenyl (54% Chlorine)|Chlorpromazine|Clodronic Acid|cobaltous chloride|
    	Copper|Curcumin|Cyclosporine|Deferoxamine|Dexamethasone|Cisplatin|Dieldrin|Dietary Fats|Diethylnitrosamine|epigallocatechin gallate|
    	Fenofibrate|Formaldehyde|Genistein|Glutathione|Hydrogen Peroxide|isoliquiritigenin|Lead|Lipopolysaccharides|manganese chloride|
    	Medroxyprogesterone Acetate|Metformin|Methotrexate|methylmercuric chloride|N-Methyl-3,4-methylenedioxyamphetamine|morin|Morphine|
    	Nanotubes, Carbon|nickel chloride|ochratoxin A|Oxygen|Paclitaxel|perfluoro-n-nonanoic acid|perfluorooctanoic acid|Phenobarbital|
    	pirinixic acid|Polyphenols|Polysaccharides|potassium chromate(VI)|propiconazole|Quercetin|resveratrol|rosiglitazone|Rotenone|
    	Streptozocin|Tetrachlorodibenzodioxin|titanium dioxide|Tretinoin|usnic acid|Valproic Acid|Vincristine|Zinc|zinc chloride	190.67	634
   * </pre>
   *
   * @param lineValues
   *          line split by tab separator
   * @param chemID
   *          chemical id
   * @return chemical obtained from input data
   * @throws ChemicalSearchException
   *           thrown when there is a problem accessing ctd database
   */
  Chemical getChemicalFromChemicalLine(String[] lineValues, MiriamData chemID) throws ChemicalSearchException {
    String name = lineValues[CHEMICALS_NAME_COL];
    String casIdentifier = lineValues[CHEMICALS_CAS_COL];
    MiriamData casId = null;
    if ((casIdentifier != null) && !casIdentifier.trim().isEmpty()) {
      casId = new MiriamData(MiriamType.CAS, casIdentifier);
    }

    String directEvidenceStr = lineValues[CHEMICALS_DIRECT_COL];
    ChemicalDirectEvidence directType = null;
    if (directEvidenceStr.equals(ChemicalDirectEvidence.MARKER.getValue())) {
      directType = ChemicalDirectEvidence.MARKER;
    } else if (directEvidenceStr.equals(ChemicalDirectEvidence.THERAPEUTIC.getValue())) {
      directType = ChemicalDirectEvidence.THERAPEUTIC;
    }

    String networkStr = lineValues[CHEMICALS_NETWORK_COL].trim();
    Map<MiriamData, Set<MiriamData>> inferenceNetwork = new HashMap<>();
    List<MiriamData> directEvidencePublication = new ArrayList<>();
    String[] genesIDs = networkStr.split("\\|");
    for (String geneId : genesIDs) {
      if (!geneId.isEmpty()) {
        inferenceNetwork.put(new MiriamData(MiriamType.HGNC_SYMBOL, geneId), new HashSet<>());
      }
    }

    Pattern pattern = Pattern.compile("ixnId=([0-9]+)");
    List<MiriamData> invalidHgnc = new ArrayList<>();
    for (MiriamData md : inferenceNetwork.keySet()) {
      try {
        Set<String> idx = new HashSet<>();
        MiriamData entrez = hgncAnnotator.hgncToEntrez(md);
        // if we cannot find entrez this means the name is invalid hgnc and
        // should be removed from our result list (it might be gene name of
        // other species...)
        if (entrez == null) {
          invalidHgnc.add(md);
        } else {
          String query = "https://ctdbase.org/detail.go?type=gene&view=ixn&chemAcc=" + chemID.getResource() + "&acc="
              + entrez.getResource();
          String referencesPage = getWebPageContent(query);
          Matcher matcher = pattern.matcher(referencesPage);
          while (matcher.find()) {
            idx.add(matcher.group(1));
          }
          for (String string : idx) {
            String query2 = "https://ctdbase.org/detail.go?6578706f7274=1&d-1340579-e=5&type=relationship&ixnId="
                + string;
            String referencesPage2 = getWebPageContent(query2);
            String[] lines = referencesPage2.split("\n");
            for (int i = 1; i < lines.length; i++) {
              String[] cols = lines[i].split("\t");
              MiriamData newMiriam = new MiriamData(MiriamType.PUBMED, cols[0]);
              inferenceNetwork.get(md).add(newMiriam);
            }
          }
        }
      } catch (IOException e) {
        throw new ChemicalSearchException(e);
      } catch (AnnotatorException e) {
        throw new ChemicalSearchException("Problem with accessing hgnc db", e);
      }
    }
    for (MiriamData miriamData : invalidHgnc) {
      inferenceNetwork.remove(miriamData);
    }
    Float score = 0f;
    Integer refCount = 0;
    String scoreStr = lineValues[CHEMICALS_SCORE_COL];
    try {
      score = Float.valueOf(scoreStr);
    } catch (NumberFormatException e) {
      logger.error("problem parsing chemcial file from ctd server, invalid value: " + scoreStr, e);
    }
    try {
      refCount = Integer.valueOf(lineValues[CHEMICALS_COUNT_COL]);
    } catch (NumberFormatException e) {
      logger.error(
          "problem parsing chemcial file from ctd server, score, invalid value: " + lineValues[CHEMICALS_COUNT_COL], e);
    }
    List<Target> geneEntries = new ArrayList<>();

    for (Map.Entry<MiriamData, Set<MiriamData>> geneEntry : inferenceNetwork.entrySet()) {
      Target target = new Target(null, geneEntry.getKey(), geneEntry.getValue());
      target.setName(geneEntry.getKey().getResource());
      geneEntries.add(target);
    }
    return new Chemical(name, chemID, casId, directType, geneEntries, directEvidencePublication, score, refCount);
  }

  /**
   * Returns list of chemicals for given set of target genes related to the
   * disease.
   *
   * @param targets
   *          set of target genes
   * @param diseaseMiriam
   *          identifier of the disease
   * @return list of chemicals for given set of target genes related to the
   *         disease
   * @throws ChemicalSearchException
   *           thrown when there is a problem with accessing ctd database
   */
  public List<Chemical> getChemicalListByTarget(Collection<MiriamData> targets, MiriamData diseaseMiriam)
      throws ChemicalSearchException {
    List<Chemical> result = new ArrayList<>();
    Set<MiriamData> entrez = new HashSet<>();
    for (MiriamData target : targets) {
      if (MiriamType.ENTREZ.equals(target.getDataType())) {
        entrez.add(target);
      } else if (MiriamType.HGNC_SYMBOL.equals(target.getDataType())) {
        try {
          MiriamData entrezTarget = hgncAnnotator.hgncToEntrez(target);
          if (entrezTarget != null) {
            entrez.add(entrezTarget);
          } else {
            logger.warn("Cannot find entrez id for " + target);
          }
        } catch (AnnotatorException e) {
          throw new ChemicalSearchException("Problem with accessing hgnc service", e);
        }
      } else {
        throw new InvalidArgumentException(
            "Only " + MiriamType.HGNC_SYMBOL + ", " + MiriamType.ENTREZ + " types is accepted");
      }
    }
    Map<MiriamData, String> chemicalsMap = getChemicalsForDisease(diseaseMiriam);
    Map<String, MiriamData> nameToId = new HashMap<>();
    for (MiriamData miriamData : chemicalsMap.keySet()) {
      nameToId.put(chemicalsMap.get(miriamData), miriamData);
    }

    Set<MiriamData> chemicalIds = new HashSet<>();
    chemicalIds.addAll(chemicalsMap.keySet());

    Set<MiriamData> geneRelatedChemicals = new HashSet<>();
    for (MiriamData target : entrez) {
      Set<String> geneRelatedChemicalNames = getRelatedChemicalNamesForTarget(diseaseMiriam, target);
      for (String string : geneRelatedChemicalNames) {
        geneRelatedChemicals.add(nameToId.get(string));
      }
    }
    chemicalIds.retainAll(geneRelatedChemicals);
    result = getChemicals(diseaseMiriam, chemicalIds);

    return result;
  }

  /**
   * Return list of chemical names that interact with a target.
   *
   * @param diseaseMiriam
   *          we want to get chemicals in context of the disease
   * @param target
   *          target for which we are looking for chemicals
   * @return list of chemical names that interact with a target
   * @throws ChemicalSearchException
   *           thrown when there is a problem with accessing external resource
   */
  private Set<String> getRelatedChemicalNamesForTarget(MiriamData diseaseMiriam, MiriamData target)
      throws ChemicalSearchException {
    try {
      Set<String> geneRelatedChemicalNames = new HashSet<>();
      String url = DISEASE_GENE_URL + target.getResource();
      String page = getWebPageContent(url);
      if (!page.contains("<!DOCTYPE html>")) {
        String[] lines = page.split("[\n\r]+");
        for (int i = 1; i < lines.length; i++) {
          String[] line = lines[i].split("\t", -1);
          if (line.length > DRUG_NAME_COL_IN_GENE_DRUG_LIST) {
            if (line[1].equalsIgnoreCase("MESH:" + diseaseMiriam.getResource())) {
              String[] drugNames = line[DRUG_NAME_COL_IN_GENE_DRUG_LIST].split("\\|");
              for (String string : drugNames) {
                geneRelatedChemicalNames.add(string);
              }
            }
          } else {
            logger.warn("Problematic line: " + lines[i]);
          }
        }
      }
      return geneRelatedChemicalNames;
    } catch (IOException e) {
      throw new ChemicalSearchException("Problem with accessing ctd database", e);
    }
  }

  /**
   * Returns list of chemicals that interact with a target.
   *
   * @param target
   *          target for which we are looking for chemicals
   * @param disease
   *          we want to get chemicals in context of the disease
   * @return list of chemicals that interact with a target
   * @throws ChemicalSearchException
   *           thrown when there is a problem with accessing chemical database
   */
  public List<Chemical> getChemicalListByTarget(MiriamData target, MiriamData disease) throws ChemicalSearchException {
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);
    return getChemicalListByTarget(targets, disease);
  }

  /**
   * @return the hgncAnnotator
   * @see #hgncAnnotator
   */
  public HgncAnnotator getHgncAnnotator() {
    return hgncAnnotator;
  }

  /**
   * @param hgncAnnotator
   *          the hgncAnnotator to set
   * @see #hgncAnnotator
   */
  public void setHgncAnnotator(HgncAnnotator hgncAnnotator) {
    this.hgncAnnotator = hgncAnnotator;
  }

  @Override
  protected WebPageDownloader getWebPageDownloader() {
    return super.getWebPageDownloader();
  }

  @Override
  protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
    super.setWebPageDownloader(webPageDownloader);
  }

  public List<String> getSuggestedQueryList(Project project, MiriamData diseaseMiriam) throws ChemicalSearchException {
    if (diseaseMiriam == null) {
      return new ArrayList<>();
    }
    String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + project.getId() + "\n" + diseaseMiriam.getResource();
    String cachedData = getCacheValue(cacheQuery);
    List<String> result;
    if (cachedData == null) {
      result = getSuggestedQueryListWithoutCache(project, diseaseMiriam);
      cachedData = StringUtils.join(result, "\n");
      setCacheValue(cacheQuery, cachedData);
    } else {
      result = new ArrayList<>();
      for (String string : cachedData.split("\n")) {
        if (!string.isEmpty()) {
          result.add(string);
        }
      }
    }
    return result;
  }

  private List<String> getSuggestedQueryListWithoutCache(Project project, MiriamData diseaseMiriam)
      throws ChemicalSearchException {
    Set<String> resultSet = new HashSet<>();
    Set<MiriamData> targets = new HashSet<>();
    for (ModelData model : project.getModels()) {
      for (Element element : model.getElements()) {
        MiriamData entrez = null;
        MiriamData hgncSymbol = null;
        for (MiriamData miriam : element.getMiriamData()) {
          if (miriam.getDataType().equals(MiriamType.ENTREZ)) {
            entrez = miriam;
          } else if (miriam.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
            hgncSymbol = miriam;
          }
        }
        if (hgncSymbol != null) {
          targets.add(hgncSymbol);
        } else if (entrez != null) {
          targets.add(entrez);
        } else {
          boolean validClass = false;
          for (Class<?> clazz : MiriamType.HGNC_SYMBOL.getValidClass()) {
            if (clazz.isAssignableFrom(element.getClass())) {
              validClass = true;
            }
          }
          if (validClass) {
            MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
            try {
              if (hgncAnnotator.isValidHgncMiriam(md)) {
                targets.add(md);
              }
            } catch (AnnotatorException e) {
              logger.error("Problem with accessing HGNC database", e);
            }
          }
        }
      }
    }
    List<Chemical> chemicals = getChemicalListByTarget(targets, diseaseMiriam);
    for (Chemical chemical : chemicals) {
      resultSet.add(chemical.getChemicalName());
      resultSet.addAll(chemical.getSynonyms());
    }
    List<String> result = new ArrayList<>();
    result.addAll(resultSet);
    Collections.sort(result);
    return result;
  }

  public List<Chemical> getChemicalsBySynonym(MiriamData diseaseId, String synonym) throws ChemicalSearchException {
    try {
      Collection<MeSH> meshList;
      meshList = meshParser.getMeshBySynonym(synonym);
      List<MiriamData> chemicalIds = new ArrayList<>();
      for (MeSH mesh : meshList) {
        chemicalIds.add(new MiriamData(MiriamType.TOXICOGENOMIC_CHEMICAL, mesh.getMeSHId()));
      }
      return getChemicals(diseaseId, chemicalIds);
    } catch (AnnotatorException e) {
      throw new ChemicalSearchException(e);
    }
  }

}
