package lcsb.mapviewer.annotation.services.annotators;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.EntrezData;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is responsible for connecting to
 * <a href="http://eutils.ncbi.nlm.nih.gov/entrez/">Entrez API</a> and annotate
 * elements with information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public class EntrezAnnotator extends ElementAnnotator implements IExternalService {

	/**
	 * Prefix used in cache key to indicate that cached value contains information
	 * about {@link MiriamType#ENTREZ} to {@link MiriamType#HGNC} conviersion.
	 */
	public static final String				CACHE_HGNC_ID_PREFIX = "HGNC_ID:";

	/**
	 * Address of Entrez API that should be used for retrieving data.
	 */
	private static final String				REST_API_URL				 = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=gene&rettype=xml&id=";

	/**
	 * Prefix used in cache key to indicate that cached value contains
	 * {@link EntrezData} object for a given entrez id.
	 */
	static final String								ENTREZ_DATA_PREFIX	 = "ENTREZ_DATA:";

	/**
	 * Object that allows to serialize {@link EntrezData} elements into xml string
	 * and deserialize xml into {@link EntrezData} objects.
	 */
	private XmlSerializer<EntrezData>	entrezSerializer;

	/**
	 * Default constructor.
	 */
	public EntrezAnnotator() {
		super(EntrezAnnotator.class, new Class[] { Protein.class, Rna.class, Gene.class }, false);
		entrezSerializer = new XmlSerializer<>(EntrezData.class);
	}

	@Override
	public String refreshCacheQuery(Object query) throws SourceNotAvailable {
		String result = null;
		try {
			if (query instanceof String) {
				String name = (String) query;
				if (name.startsWith("http")) {
					result = getWebPageContent(name);
				} else if (name.startsWith(ENTREZ_DATA_PREFIX)) {
					String id = name.replace(ENTREZ_DATA_PREFIX, "");
					result = entrezSerializer.objectToString(getEntrezForMiriamData(createMiriamData(MiriamType.ENTREZ, id), null));
				} else if (name.startsWith(CACHE_HGNC_ID_PREFIX)) {
					String id = name.replace(CACHE_HGNC_ID_PREFIX, "");
					MiriamData md = getHgncIdFromEntrez(createMiriamData(MiriamType.ENTREZ, id));
					if (md != null) {
						result = md.getResource();
					}
				} else {
					throw new InvalidArgumentException("Don't know what to do with input string: " + name);
				}
			} else {
				throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
			}
		} catch (AnnotatorException e) {
			throw new SourceNotAvailable(e);
		} catch (IOException e) {
			throw new SourceNotAvailable(e);
		}
		return result;
	}

	/**
	 * Standard class logger.
	 */
	private final Logger logger = Logger.getLogger(EntrezAnnotator.class);

	@Override
	public String getCommonName() {
		return MiriamType.ENTREZ.getCommonName();
	}

	@Override
	public String getUrl() {
		return MiriamType.ENTREZ.getDbHomepage();
	}

	@Override
	public ExternalServiceStatus getServiceStatus() {
		ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

		GeneralCacheInterface cacheCopy = getCache();
		this.setCache(null);

		try {
			GenericProtein proteinAlias = new GenericProtein("id");
			proteinAlias.addMiriamData(createMiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.ENTREZ, "9999"));
			annotateElement(proteinAlias);

			if (proteinAlias.getFullName() == null || proteinAlias.getFullName().equals("")) {
				status.setStatus(ExternalServiceStatusType.CHANGED);
			} else {
				status.setStatus(ExternalServiceStatusType.OK);
			}
		} catch (Exception e) {
			logger.error(getCommonName() + " is down", e);
			status.setStatus(ExternalServiceStatusType.DOWN);
		}
		this.setCache(cacheCopy);
		return status;
	}

	@Override
	public void annotateElement(BioEntity element) throws AnnotatorException {
		if (isAnnotatable(element)) {
			ElementUtils eu = new ElementUtils();
			String prefix = eu.getElementTag(element);
			MiriamData entrezMiriamData = null;
			for (MiriamData md : element.getMiriamData()) {
				if (MiriamType.ENTREZ.equals(md.getDataType())) {
					if (entrezMiriamData != null) {
						logger.warn(prefix + " More than one " + MiriamType.ENTREZ.getCommonName() + " identifier");
					}
					entrezMiriamData = md;
				}
			}
			if (entrezMiriamData != null) {
				annotateElement(element, entrezMiriamData, prefix);
			} else {
				logger.warn(prefix + " No " + MiriamType.ENTREZ.getCommonName() + " identifier found");
			}
		}
	}

	/**
	 * Annotates element using identifier given in the parameter.
	 * 
	 * @param element
	 *          element that we want to annotate
	 * @param entrezMiriamData
	 *          identifier that should be used for annotation
	 * @param prefix
	 *          prefix used in warnings
	 * @throws AnnotatorException
	 *           thrown when there is a problem with annotating element
	 */
	private void annotateElement(BioEntity element, MiriamData entrezMiriamData, String prefix) throws AnnotatorException {
		EntrezData data = getEntrezForMiriamData(entrezMiriamData, prefix);
		if (data != null) {
			setSymbol(element, data.getSymbol(), prefix);

			setFullName((Element) element, data.getFullName(), prefix);

			element.addMiriamData(data.getMiriamData());
			setSynonyms(element, data.getSynonyms(), prefix);
			setDescription(element, data.getDescription());
		}

	}

	/**
	 * Returns preprocessed data for entrez identifier.
	 * 
	 * @param entrezMiriamData
	 *          {@link MiriamData} with {@link MiriamType#ENTREZ} id
	 * @param prefix
	 *          prefix used for logs
	 * @return {@link EntrezData preprocessed data} for entrez identifier
	 * @throws AnnotatorException
	 *           thrown when there is a problem with obtaining data from entrez
	 *           database
	 */
	EntrezData getEntrezForMiriamData(MiriamData entrezMiriamData, String prefix) throws AnnotatorException {
		EntrezData result = entrezSerializer.xmlToObject(super.getCacheNode(ENTREZ_DATA_PREFIX + entrezMiriamData.getResource()));
		if (result == null) {
			String query = REST_API_URL + entrezMiriamData.getResource();
			logger.debug(query);
			try {
				String content = getWebPageContent(query);
				result = new EntrezData();
				Node xml = getXmlDocumentFromString(content);

				Node response = getNode("Entrezgene-Set", xml.getChildNodes());
				if (response != null) {
					Node errorNode = getNode("Error", response.getChildNodes());
					if (errorNode != null) {
						logger.warn(prefix + " Invalid " + MiriamType.ENTREZ + " identifier: \"" + entrezMiriamData.getResource() + "\"");
						return null;
					}
				} else  {
					logger.warn(prefix + "Problematic entrez response for identifier: \"" + entrezMiriamData.getResource() + "\"");
					return null;
				}

				Node rootNode = getNode("Entrezgene", response.getChildNodes());

				Node annotationNode = getNode("Entrezgene_gene", rootNode.getChildNodes());
				Node annotationGenRefNode = getNode("Gene-ref", annotationNode.getChildNodes());

				Node symbolNode = getNode("Gene-ref_locus", annotationGenRefNode.getChildNodes());
				if (symbolNode != null) {
					String symbol = symbolNode.getTextContent();
					result.setSymbol(symbol);
				}

				Node fullNameNode = getNode("Gene-ref_desc", annotationGenRefNode.getChildNodes());
				if (fullNameNode != null) {
					String fullname = fullNameNode.getTextContent();
					result.setFullName(fullname);
				}

				Node listNode = getNode("Gene-ref_db", annotationGenRefNode.getChildNodes());
				Set<String> synonyms = new HashSet<String>();
				if (listNode != null) {
					NodeList list = getNode("Gene-ref_db", annotationGenRefNode.getChildNodes()).getChildNodes();

					for (int i = 0; i < list.getLength(); i++) {
						Node node = list.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							if (node.getNodeName().equals("Dbtag")) {
								Node dbNameNode = getNode("Dbtag_db", node.getChildNodes());
								String dbName = dbNameNode.getTextContent();

								Node dbIdNode1 = getNode("Dbtag_tag", node.getChildNodes());
								Node dbIdNode2 = getNode("Object-id", dbIdNode1.getChildNodes());
								Node dbIdNode3 = getNode("Object-id_str", dbIdNode2.getChildNodes());
								if (dbIdNode3 == null) {
									dbIdNode3 = getNode("Object-id_id", dbIdNode2.getChildNodes());
								}
								String dbId = null;
								if (dbIdNode3 != null) {
									dbId = dbIdNode3.getTextContent();
								}
								if ("HGNC".equals(dbName)) {
									if (dbId != null && !dbId.isEmpty()) {
										dbId = dbId.replaceAll("HGNC:", "");
										result.addMiriamData(createMiriamData(MiriamType.HGNC, dbId));
									}
								} else if ("Ensembl".equals(dbName)) {
									if (dbId != null && !dbId.isEmpty()) {
										result.addMiriamData(createMiriamData(MiriamType.ENSEMBL, dbId));
									}
								}

							}
						}
					}

				}

				Node synListNode = getNode("Gene-ref_syn", annotationGenRefNode.getChildNodes());
				if (synListNode != null) {
					NodeList list = getNode("Gene-ref_syn", annotationGenRefNode.getChildNodes()).getChildNodes();

					for (int i = 0; i < list.getLength(); i++) {
						Node node = list.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							if (node.getNodeName().equals("Gene-ref_syn_E")) {
								synonyms.add(node.getTextContent());
							}
						}
					}
				}
				if (synonyms.size() > 0) {
					List<String> sortedSynonyms = new ArrayList<String>();
					sortedSynonyms.addAll(synonyms);
					Collections.sort(sortedSynonyms);
					result.setSynonyms(sortedSynonyms);
				}
				Node notesNode = getNode("Entrezgene_summary", rootNode.getChildNodes());
				if (notesNode != null) {
					String notes = notesNode.getTextContent();
					if (notes != null) {
						result.setDescription(notes);
					}
				}
				setCacheValue(ENTREZ_DATA_PREFIX + entrezMiriamData.getResource(), entrezSerializer.objectToString(result));
			} catch (WrongResponseCodeIOException e) {
				if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST) {
					logger.warn(prefix + " Invalid " + MiriamType.ENTREZ.getCommonName() + " identifier found");
				} else {
					throw new AnnotatorException(e);
				}
			} catch (IOException e) {
				throw new AnnotatorException(e);
			} catch (InvalidXmlSchemaException e) {
				throw new AnnotatorException(e);
			}
		}
		return result;
	}

	/**
	 * Converts {@link MiriamType#ENTREZ} identifier into {@link MiriamType#HGNC}
	 * identifier.
	 * 
	 * @param miriamData
	 *          {@link MiriamData} with {@link MiriamType#ENTREZ} identifier
	 * @return {@link MiriamData} with {@link MiriamType#HGNC} or null if such
	 *         identifier doesn't exists
	 * @throws AnnotatorException
	 *           thrown when there was problem with accessing external database or
	 *           any other unknown problem
	 */
	public MiriamData getHgncIdFromEntrez(MiriamData miriamData) throws AnnotatorException {
		if (!MiriamType.ENTREZ.equals(miriamData.getDataType())) {
			throw new InvalidArgumentException("Only " + MiriamType.ENTREZ + " miriam type is accepted.");
		}
		String key = CACHE_HGNC_ID_PREFIX + miriamData.getResource();
		String value = getCacheValue(key);
		if (value == null) {
			GenericProtein proteinAlias = new GenericProtein("some_id");
			annotateElement(proteinAlias, miriamData, "");
			for (MiriamData md : proteinAlias.getMiriamData()) {
				if (MiriamType.HGNC.equals(md.getDataType())) {
					setCacheValue(key, md.getResource());
					return md;
				}
			}
			setCacheValue(key, "null");
			return null;
		} else if (value.equals("null")) {
			return null;
		} else {
			return createMiriamData(MiriamType.HGNC, value);
		}
	}

	@Override
	protected WebPageDownloader getWebPageDownloader() {
		return super.getWebPageDownloader();
	}

	@Override
	protected void setWebPageDownloader(WebPageDownloader webPageDownloader) {
		super.setWebPageDownloader(webPageDownloader);
	}

	/**
	 * @return the entrezSerializer
	 * @see #entrezSerializer
	 */
	protected XmlSerializer<EntrezData> getEntrezSerializer() {
		return entrezSerializer;
	}

	/**
	 * @param entrezSerializer
	 *          the entrezSerializer to set
	 * @see #entrezSerializer
	 */
	protected void setEntrezSerializer(XmlSerializer<EntrezData> entrezSerializer) {
		this.entrezSerializer = entrezSerializer;
	}

}
