package lcsb.mapviewer.annotation.services;

/**
 * Interface that describes annotation that is problematic.
 * 
 * @author Piotr Gawron
 *
 */
public interface ProblematicAnnotation {

	/**
	 * Returns error message.
	 * 
	 * @return problem error message
	 */
	String getMessage();
}
