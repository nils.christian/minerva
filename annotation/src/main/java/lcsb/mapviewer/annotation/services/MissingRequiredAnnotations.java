package lcsb.mapviewer.annotation.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Defines problem with element annotation when there are some required
 * annotations, but none of them can be found in the element.
 * 
 * @author Piotr Gawron
 *
 */
public class MissingRequiredAnnotations implements ProblematicAnnotation {

	/**
	 * BioEntity improperly annotated.
	 */
	private BioEntity				 bioEntity;

	/**
	 * Required miriam type.
	 */
	private List<MiriamType> requiredMiriamType	= new ArrayList<>();

	/**
	 * Constructor that initializes the data with {@link #bioEntity bioEntity} and
	 * list of improper {@link MiriamData}.
	 * 
	 * @param list
	 *          list of missing but required {@link MiriamType}
	 * @param bioEntity
	 *          {@link BioEntity}
	 */
	public MissingRequiredAnnotations(BioEntity bioEntity, Collection<MiriamType> list) {
		if (list.size() == 0) {
			throw new InvalidArgumentException("List of improper annotations cannot be null");
		}
		this.bioEntity = bioEntity;
		requiredMiriamType.addAll(list);
	}

	@Override
	public String getMessage() {
		StringBuilder result = new StringBuilder(new ElementUtils().getElementTag(bioEntity));
		result.append("misses one of the following annotations: ");
		for (MiriamType type : requiredMiriamType) {
			result.append(type.getCommonName() + ", ");
		}
		return result.toString();
	}

	@Override
	public String toString() {
		return getMessage();
	}

}
