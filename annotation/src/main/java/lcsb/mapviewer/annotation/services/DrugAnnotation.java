package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.ProjectDao;

/**
 * Abstract class with some functionalities used by class accessing drug
 * databases.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class DrugAnnotation extends CachableInterface {

  /**
   * Prefix used for caching list of suggested queries for project.
   */
  protected static final String PROJECT_SUGGESTED_QUERY_PREFIX = "PROJECT_DRUG_QUERIES:";

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(DrugAnnotation.class);

  /**
   * Object that allows to serialize {@link Drug} elements into xml string and
   * deserialize xml into {@link Drug} objects.
   */
  private XmlSerializer<Drug> drugSerializer;

  /**
   * Object used to access information about organism taxonomy.
   */
  @Autowired
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Autowired
  private ProjectDao projectDao;

  /**
   * Default constructor. Initializes structures used for transforming
   * {@link Drug} from/to xml.
   * 
   * @param clazz
   *          class that identifies this {@link CachableInterface}
   */
  public DrugAnnotation(final Class<? extends DrugAnnotation> clazz) {
    super(clazz);
    drugSerializer = new XmlSerializer<>(Drug.class);
  }

  /**
   * Returns a drug for given name.
   * 
   * @param name
   *          name of the drug
   * @return drug for a given name
   * @throws DrugSearchException
   *           thrown when there are problems with finding drug
   */
  public abstract Drug findDrug(String name) throws DrugSearchException;

  /**
   * Returns list of drugs that target protein.
   * 
   * @param target
   *          {@link MiriamData} describing target
   * @param organisms
   *          list of organisms to which results should be limited (when no
   *          organisms defined filtering will be turned off)
   * @return list of drugs
   * @throws DrugSearchException
   *           thrown when there are problems with finding drug
   */

  public abstract List<Drug> getDrugListByTarget(MiriamData target, Collection<MiriamData> organisms)
      throws DrugSearchException;

  /**
   * Returns list of drugs that target protein.
   * 
   * @param target
   *          {@link MiriamData} describing target
   * @return list of drugs
   * @throws DrugSearchException
   *           thrown when there are problems with finding drug
   */
  public List<Drug> getDrugListByTarget(MiriamData target) throws DrugSearchException {
    return getDrugListByTarget(target, new ArrayList<>());
  }

  public String refreshCacheQuery(Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith(PROJECT_SUGGESTED_QUERY_PREFIX)) {
          String[] tmp = name.split("\n");
          Integer id = Integer.valueOf(tmp[1]);
          MiriamData diseaseId = new MiriamData(MiriamType.MESH_2012, tmp[2]);
          Project project = projectDao.getById(id);
          if (project == null) {
            throw new SourceNotAvailable("Project with given id doesn't exist: " + id);
          }
          List<String> list = getSuggestedQueryListWithoutCache(project, diseaseId);
          result = StringUtils.join(list, "\n");

        } else if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (DrugSearchException e) {
      throw new SourceNotAvailable(e);
    } catch (IOException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  /**
   * Returns list of drugs that target at least one protein from the parameter
   * list.
   * 
   * @param targets
   *          list of {@link MiriamData} describing targets
   * @param organisms
   *          list of organisms to which results should be limited (when no
   *          organisms defined filtering will be turned off)
   * @return list of drugs
   * @throws DrugSearchException
   *           thrown when there are problems with connection to drug database
   */
  public List<Drug> getDrugListByTargets(Collection<MiriamData> targets, Collection<MiriamData> organisms)
      throws DrugSearchException {
    List<Drug> result = new ArrayList<Drug>();
    Set<String> set = new HashSet<String>();
    Set<MiriamData> searchedResult = new HashSet<MiriamData>();
    for (MiriamData md : targets) {
      if (searchedResult.contains(md)) {
        continue;
      }
      searchedResult.add(md);
      List<Drug> drugs = getDrugListByTarget(md, organisms);
      for (Drug drug : drugs) {
        if (!set.contains(drug.getSources().get(0).getResource())) {
          result.add(drug);
          set.add(drug.getSources().get(0).getResource());
        } else {
          continue;
        }
      }
    }
    return result;
  }

  /**
   * Returns list of drugs that target at least one protein from the parameter
   * list.
   * 
   * @param targets
   *          list of {@link MiriamData} describing targets
   * @return list of drugs
   * @throws DrugSearchException
   *           thrown when there are problems with connection to drug database
   */
  public List<Drug> getDrugListByTargets(Collection<MiriamData> targets) throws DrugSearchException {
    return getDrugListByTargets(targets, new ArrayList<>());
  }

  /**
   * @return the drugSerializer
   * @see #drugSerializer
   */
  protected XmlSerializer<Drug> getDrugSerializer() {
    return drugSerializer;
  }

  /**
   * @param drugSerializer
   *          the drugSerializer to set
   * @see #drugSerializer
   */
  protected void setDrugSerializer(XmlSerializer<Drug> drugSerializer) {
    this.drugSerializer = drugSerializer;
  }

  /**
   * Checks if organism name matches list of organisms.
   * 
   * @param organismName
   *          name of the organism (should be something recognizable by
   *          {@link MiriamType#TAXONOMY} db).
   * @param organisms
   *          list of organisms
   * @return <code>false</code> if organisName couldn't be found in organisms
   *         list. If organismName is null or empty or list of organisms is empty
   *         true will be returned
   */
  protected boolean organismMatch(String organismName, Collection<MiriamData> organisms) {
    if (organismName == null || organismName.isEmpty() || organisms.size() == 0) {
      return true;
    }
    try {
      MiriamData organism = taxonomyBackend.getByName(organismName);
      return organisms.contains(organism);
    } catch (TaxonomySearchException e) {
      logger.error("Problem with taxonomy search for: " + organismName, e);
      return true;
    }
  }

  /**
   * @return the taxonomyBackend
   * @see #taxonomyBackend
   */
  public TaxonomyBackend getTaxonomyBackend() {
    return taxonomyBackend;
  }

  /**
   * @param taxonomyBackend
   *          the taxonomyBackend to set
   * @see #taxonomyBackend
   */
  public void setTaxonomyBackend(TaxonomyBackend taxonomyBackend) {
    this.taxonomyBackend = taxonomyBackend;
  }

  public List<String> getSuggestedQueryList(Project project, MiriamData organism) throws DrugSearchException {
    if (organism == null) {
      return new ArrayList<>();
    }
    String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + project.getId() + "\n" + organism.getResource();
    String cachedData = getCacheValue(cacheQuery);
    List<String> result;
    if (cachedData == null) {
      result = getSuggestedQueryListWithoutCache(project, organism);
      cachedData = StringUtils.join(result, "\n");
      setCacheValue(cacheQuery, cachedData);
    } else {
      result = new ArrayList<>();
      for (String string : cachedData.split("\n")) {
        if (!string.isEmpty()) {
          result.add(string);
        }
      }
    }
    return result;
  }

  private List<String> getSuggestedQueryListWithoutCache(Project project, MiriamData organism)
      throws DrugSearchException {
    Set<String> resultSet = new HashSet<>();
    Set<MiriamData> targets = new HashSet<>();
    for (ModelData model : project.getModels()) {
      for (Element element : model.getElements()) {
        MiriamData uniprot = null;
        MiriamData hgncSymbol = null;
        for (MiriamData miriam : element.getMiriamData()) {
          if (miriam.getDataType().equals(MiriamType.UNIPROT)) {
            uniprot = miriam;
          } else if (miriam.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
            hgncSymbol = miriam;
          }
        }
        if (hgncSymbol != null) {
          targets.add(hgncSymbol);
        } else if (uniprot != null) {
          targets.add(uniprot);
        } else {
          boolean validClass = false;
          for (Class<?> clazz : MiriamType.HGNC_SYMBOL.getValidClass()) {
            if (clazz.isAssignableFrom(element.getClass())) {
              validClass = true;
            }
          }
          if (validClass) {
            MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
            try {
              if (hgncAnnotator.isValidHgncMiriam(md)) {
                targets.add(md);
              }
            } catch (AnnotatorException e) {
              logger.error("Problem with accessing HGNC database", e);
            }
          }
        }
      }
    }
    List<MiriamData> organisms = new ArrayList<>();
    if (organism != null) {
      organisms.add(organism);
    }
    List<Drug> drugs = getDrugListByTargets(targets, organisms);
    for (Drug chemical : drugs) {
      resultSet.add(chemical.getName());
    }
    List<String> result = new ArrayList<>();
    result.addAll(resultSet);
    Collections.sort(result);
    return result;
  }

}
