package lcsb.mapviewer.annotation.data;

import java.io.Serializable;

import uk.ac.ebi.chebi.webapps.chebiWS.model.OntologyDataItem;

/**
 * Describes relation of the {@link Chebi} object into other elements into chebi
 * ontology.
 * 
 * @author Piotr Gawron
 * 
 */
public class ChebiRelation implements Serializable {
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Name of the chebi object pointed by this relation.
	 */
	private String	name;
	
	/**
	 * Type of the relation.
	 */
	private String	type;
	
	/**
	 * Chebi identifier of object pointef by this relation.
	 */
	private String	id;

	/**
	 * Default constructor.
	 */
	public ChebiRelation() {

	}

	/**
	 * Constructor that creates relation from object retrieved by Chebi API.
	 * 
	 * @param item
	 *          object from Chebi API
	 */
	public ChebiRelation(OntologyDataItem item) {
		name = item.getChebiName();
		type = item.getType();
		id = item.getChebiId();
	}

	/**
	 * @return the name
	 * @see #name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 * @see #name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the id
	 * @see #id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(String id) {
		this.id = id;
	}

}
