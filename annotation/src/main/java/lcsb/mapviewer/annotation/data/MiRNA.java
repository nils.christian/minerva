package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Object representing element obtained from Toxigenomic database.
 * 
 * @author Ayan Rota
 * 
 */
@XmlRootElement
public class MiRNA implements Serializable, TargettingStructure {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * ID coming from MiRBase.
   */
  private String name;

  /**
   * Genes interacting with the chemical plus publications.
   */
  private List<Target> targets = new ArrayList<>();

  /**
   * default constructor.
   */
  public MiRNA() {
    super();
  }

  /**
   * Constructor that takes name of mirna as argument.
   * 
   * @param name
   *          name of the mirna
   */
  public MiRNA(String name) {
    super();
    this.name = name;
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("\nmiRBaseID: " + name);
    result.append("\ntargetGenes:");
    for (Target target : getTargets()) {
      result.append(target.toString());
    }

    return result.toString();
  }

  /**
   * @return list of genes plus publications.
   */
  public List<Target> getTargets() {
    return targets;
  }

  /**
   * @param targets
   *          list of genes plus publications.
   */
  public void setTargets(List<Target> targets) {
    this.targets = targets;
  }

  /**
   * @return id form Mirtarbase id.
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          id form MirBase id.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Adds target to mirna.
   * 
   * @param target
   *          target to add.
   */

  public void addTarget(Target target) {
    this.targets.add(target);
  }

  @Override
  public Collection<MiriamData> getSources() {
    return new ArrayList<>();
  }

  /**
   * Comparator of the objects by their name.
   * 
   * @author Piotr Gawron
   * 
   */
  public static class NameComparator implements Comparator<MiRNA> {
    /**
     * Default string comparator.
     */
    private StringComparator stringComparator = new StringComparator();

    @Override
    public int compare(MiRNA arg0, MiRNA arg1) {
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

  }

}
