package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * This class represents <a href="http://www.nlm.nih.gov/cgi/mesh//">MeSH</a>
 * object.
 * 
 * @author Ayan Rota
 * 
 */
@XmlRootElement
public class MeSH implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID = 1L;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger			logger					 = Logger.getLogger(MeSH.class);

	/**
	 * short name.
	 */
	private String						name;

	/**
	 * MeSH identifier.
	 */
	private String						meSHId;

	/**
	 * Detailed description of the MeSH object.
	 */
	private String						description;

	/**
	 * List of synonyms.
	 */
	private List<String>			synonyms				 = new ArrayList<>();

	/**
	 * Default constructor.
	 */
	public MeSH() {

	}

	/**
	 * @param name
	 *          short name.
	 * @param meSHId
	 *          database identifier.
	 * @param description
	 *          long description.
	 * @param synonyms
	 *          list of terms used as names for this object.
	 */
	public MeSH(String meSHId, String name, String description, List<String> synonyms) {
		super();
		this.name = name;
		this.meSHId = meSHId;
		this.description = description;
		this.synonyms = synonyms;
	}

	/**
	 * @return the name
	 * @see #name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 * @see #name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the synonyms
	 * @see #synonyms
	 */
	public List<String> getSynonyms() {
		return synonyms;
	}

	/**
	 * @param synonyms
	 *          the synonyms to set
	 * @see #synonyms
	 */
	public void setSynonyms(List<String> synonyms) {
		this.synonyms = synonyms;
	}

	/**
	 * @return database identifier.
	 */
	public String getMeSHId() {
		return meSHId;
	}

	/**
	 * @return description of the object.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param meSHId
	 *          database identifier
	 */
	public void setMeSHId(String meSHId) {
		this.meSHId = meSHId;
	}

	/**
	 * @param description
	 *          long description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("\nid: " + meSHId);
		result.append("\nname: " + name);
		result.append("\ndescription:" + description);
		result.append("\nsynonyms:" + getSynonymsString());
		return result.toString();
	}

	/**
	 * @return list of synonyms as string.
	 */
	public String getSynonymsString() {
		return StringUtils.join(synonyms, ",");
	}

	/**
	 * Adds synonym to the synonym list.
	 * 
	 * @param synonym
	 *          synonym to add
	 */
	public void addSynonym(String synonym) {
		synonyms.add(synonym);
	}

	/**
	 * Adds synonyms to the synonym list.
	 * 
	 * @param synonymsToAdd
	 *          synonyms to add
	 */
	public void addSynonyms(Set<String> synonymsToAdd) {
		for (String synonym : synonymsToAdd) {
			this.synonyms.add(synonym);
		}

	}

}
