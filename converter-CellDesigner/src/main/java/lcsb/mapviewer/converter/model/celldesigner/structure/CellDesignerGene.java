package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class representing CellDesigner {@link Gene}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerGene extends CellDesignerSpecies<Gene> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of modifications for the Gene.
   */
  private List<CellDesignerModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Constructor that initializes gene with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerGene(CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerGene) {
      CellDesignerGene gene = (CellDesignerGene) species;
      for (CellDesignerModificationResidue mr : gene.getModificationResidues()) {
        addModificationResidue(new CellDesignerModificationResidue(mr));
      }
    }
  }

  @Override
  public void update(CellDesignerSpecies<?> species) {
    super.update(species);
    if (species instanceof CellDesignerGene) {
      CellDesignerGene gene = (CellDesignerGene) species;

      for (CellDesignerModificationResidue mr : gene.getModificationResidues()) {
        addModificationResidue(mr);
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerGene() {
    super();
  }

  @Override
  public CellDesignerGene copy() {
    if (this.getClass() == CellDesignerGene.class) {
      return new CellDesignerGene(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * Adds modification to the gene.
   * 
   * @param modificationResidue
   *          modification to add
   */
  public void addModificationResidue(CellDesignerModificationResidue modificationResidue) {
    for (CellDesignerModificationResidue mr : modificationResidues) {
      if (mr.getIdModificationResidue().equals(modificationResidue.getIdModificationResidue())) {
        mr.update(modificationResidue);
        return;
      }
    }
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);

  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<CellDesignerModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(List<CellDesignerModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  @Override
  public Gene createModelElement(String aliasId) {
    Gene result = new Gene(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

  @Override
  public void updateModelElementAfterLayoutAdded(Species element) {
    for (CellDesignerModificationResidue region : modificationResidues) {
      ((Gene) element).addModificationResidue(region.createModificationResidue(element));
    }
  }

}
