package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class representing CellDesigner {@link Protein} object.
 * 
 * @param <T>
 *          type of a {@link Protein} modeled by this class
 * @author Piotr Gawron
 * 
 */
public class CellDesignerProtein<T extends Protein> extends CellDesignerSpecies<T> {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(CellDesignerProtein.class.getName());

  /**
   * State of the protein.
   */
  private String structuralState = null;

  /**
   * List of modifications for the Protein.
   */
  private List<CellDesignerModificationResidue> modificationResidues = new ArrayList<CellDesignerModificationResidue>();

  /**
   * Constructor that initializes protein with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerProtein(CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerProtein) {
      CellDesignerProtein<?> protein = (CellDesignerProtein<?>) species;
      if (protein.getStructuralState() != null) {
        setStructuralState(new String(protein.getStructuralState()));
      }
      for (CellDesignerModificationResidue mr : protein.getModificationResidues()) {
        addModificationResidue(new CellDesignerModificationResidue(mr));
      }
    }
  }

  @Override
  public void update(CellDesignerSpecies<?> species) {
    super.update(species);
    if (species instanceof CellDesignerProtein) {
      CellDesignerProtein<?> protein = (CellDesignerProtein<?>) species;
      if (getStructuralState() == null || getStructuralState().equals("")) {
        setStructuralState(protein.getStructuralState());
      }
      for (CellDesignerModificationResidue mr : protein.getModificationResidues()) {
        addModificationResidue(mr);
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerProtein() {
    super();
  }

  @Override
  public CellDesignerProtein<T> copy() {
    if (this.getClass().equals(CellDesignerProtein.class)) {
      return new CellDesignerProtein<T>(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented");
    }
  }

  /**
   * Adds modification to the protein.
   * 
   * @param modificationResidue
   *          modification to add
   */
  public void addModificationResidue(CellDesignerModificationResidue modificationResidue) {
    for (CellDesignerModificationResidue mr : modificationResidues) {
      if (mr.getIdModificationResidue().equals(modificationResidue.getIdModificationResidue())) {
        mr.update(modificationResidue);
        return;
      }
    }
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);
  }

  /**
   * @return the structuralState
   * @see #structuralState
   */
  public String getStructuralState() {
    return structuralState;
  }

  /**
   * @param structuralState
   *          the structuralState to set
   * @see #structuralState
   */
  public void setStructuralState(String structuralState) {
    if (this.structuralState != null && !this.structuralState.equals("")
        && !this.structuralState.equals(structuralState)) {
      logger.warn("replacing structural state, Old: " + this.structuralState + " into new: " + structuralState);
    }
    this.structuralState = structuralState;
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<CellDesignerModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(List<CellDesignerModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  @Override
  protected void setModelObjectFields(T result) {
    super.setModelObjectFields(result);
    result.setStructuralState(structuralState);
  }

  @Override
  public void updateModelElementAfterLayoutAdded(Species element) {
    for (CellDesignerModificationResidue mr : modificationResidues) {
      ((Protein) element).addModificationResidue(mr.createModificationResidue(element));
    }
  }

}
