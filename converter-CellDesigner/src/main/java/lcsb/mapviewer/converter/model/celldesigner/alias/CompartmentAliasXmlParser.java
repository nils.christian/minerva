package lcsb.mapviewer.converter.model.celldesigner.alias;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.model.Model;

/**
 * Parser for CellDesigner xml for compartment aliases.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentAliasXmlParser extends AbstractAliasXmlParser<Compartment> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(CompartmentAliasXmlParser.class.getName());

  /**
   * Model for which we are parsing aliases.
   */
  private Model model = null;

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   * @param model
   *          model for which this parser will be used
   */
  public CompartmentAliasXmlParser(CellDesignerElementCollection elements, Model model) {
    this.model = model;
    this.elements = elements;
  }

  @Override
  Compartment parseXmlAlias(Node aliasNode) throws InvalidXmlSchemaException {

    String compartmentId = getNodeAttr("compartment", aliasNode);
    CellDesignerCompartment compartment = elements.getElementByElementId(compartmentId);
    if (compartment == null) {
      throw new InvalidXmlSchemaException("Compartment does not exist in a model: " + compartmentId);
    }
    String aliasId = getNodeAttr("id", aliasNode);
    Compartment result = compartment.createModelElement(aliasId);
    elements.addElement(compartment, aliasId);

    NodeList nodes = aliasNode.getChildNodes();
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:class")) {
          String type = getNodeValue(node);
          if (type.equalsIgnoreCase("SQUARE")) {
            result = new SquareCompartment(result);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_NORTH")) {
            result = new BottomSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_SOUTH")) {
            result = new TopSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_WEST")) {
            result = new RightSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_EAST")) {
            result = new LeftSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("OVAL")) {
            result = new OvalCompartment(result);
          } else {
            throw new InvalidXmlSchemaException("Unknown compartment type: " + type);
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(getNodeAttr("x", node));
          result.setY(getNodeAttr("y", node));
          result.setWidth(getNodeAttr("w", node));
          result.setHeight(getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:doubleLine")) {
          result.setLineThickness(getNodeAttr("thickness", node));
          result.setLineOuterWidth(getNodeAttr("outerWidth", node));
          result.setLineInnerWidth(getNodeAttr("innerWidth", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setColor(getCommonParser().getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
          // not handled
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:point")) {
          if (result instanceof BottomSquareCompartment) {
            result.setY(getNodeAttr("y", node));
          } else if (result instanceof LeftSquareCompartment) {
            result.setX(getNodeAttr("x", node));
          } else if (result instanceof RightSquareCompartment) {
            result.setX(getNodeAttr("x", node));
            result.setWidth(result.getWidth() - result.getX());
          } else if (result instanceof TopSquareCompartment) {
            result.setY(getNodeAttr("y", node));
          } else {
            throw new InvalidXmlSchemaException(
                "Don't know what to do with celldesigner:point for class: " + result.getClass());
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:namePoint")) {
          result.setNamePoint(getCommonParser().getPosition(node));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:compartmentAlias: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  @Override
  public String toXml(Compartment compartment) {
    StringBuilder sb = new StringBuilder("");

    sb.append("<celldesigner:compartmentAlias ");
    sb.append("id=\"" + compartment.getElementId() + "\" ");
    sb.append("compartment=\"" + elements.getElementId(compartment) + "\">\n");

    boolean bounds = true;

    if (compartment.getClass().getName().equals(SquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE</celldesigner:class>\n");
    } else if (compartment.getClass().getName().equals(OvalCompartment.class.getName())) {
      sb.append("<celldesigner:class>OVAL</celldesigner:class>\n");
    } else if (compartment.getClass().getName().equals(BottomSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_NORTH</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"10\" y=\"" + compartment.getY() + "\"/>");
    } else if (compartment.getClass().getName().equals(TopSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_SOUTH</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"10\" y=\"" + compartment.getY() + "\"/>");
    } else if (compartment.getClass().getName().equals(LeftSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_EAST</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"" + compartment.getWidth() + "\" y=\"10\"/>");
    } else if (compartment.getClass().getName().equals(RightSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_WEST</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"" + compartment.getX() + "\" y=\"10\"/>");
    } else {
      throw new NotImplementedException("Unknown compartment class: " + compartment.getClass());
    }
    if (bounds) {
      sb.append("<celldesigner:bounds ");
      sb.append("x=\"" + compartment.getX() + "\" ");
      sb.append("y=\"" + compartment.getY() + "\" ");
      sb.append("w=\"" + compartment.getWidth() + "\" ");
      sb.append("h=\"" + compartment.getHeight() + "\"/>\n");
    }

    sb.append("<celldesigner:namePoint ");
    sb.append("x=\"" + compartment.getNamePoint().getX() + "\" ");
    sb.append("y=\"" + compartment.getNamePoint().getY() + "\"/>\n");

    sb.append("<celldesigner:doubleLine ");
    sb.append("thickness=\"" + compartment.getThickness() + "\" ");
    sb.append("outerWidth=\"" + compartment.getOuterWidth() + "\" ");
    sb.append("innerWidth=\"" + compartment.getInnerWidth() + "\"/>\n");

    sb.append("<celldesigner:paint ");
    sb.append("color=\"" + colorToString(compartment.getColor()) + "\" scheme=\"Color\"/>\n");

    sb.append("</celldesigner:compartmentAlias>\n");

    return sb.toString();
  }

}
