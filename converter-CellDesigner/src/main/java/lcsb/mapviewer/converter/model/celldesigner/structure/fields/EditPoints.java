package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * CellDesigner structure for storing information about base lines in the
 * reaction.
 * 
 * @author Piotr Gawron
 * 
 */
public class EditPoints {

	/**
	 * List of internal points in CellDesigner format. Usually it means that edge
	 * values represents base in which the interanal points are stored. However,
	 * it doesn't have to be true in all case. For more detail analysie usage of
	 * this class in the parser.
	 * 
	 * @see lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory#createPolylineDataFromEditPoints(Point2D,
	 *      Point2D, EditPoints)
	 */
	private List<Point2D>	points;

	/**
	 * This value in reactions with three base reactants+products (reactions that
	 * implements
	 * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoReactantReactionInterface
	 * TwoReactantReactionInterface} or
	 * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoProductReactionInterface
	 * TwoProductReactionInterface}) define where starts the first line in this
	 * strange line description. See also usage of this field in CellDesigner
	 * parser.
	 */

	private int						num0;

	/**
	 * This value in reactions with three base reactants+products (reactions that
	 * implements
	 * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoReactantReactionInterface
	 * TwoReactantReactionInterface} or
	 * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoProductReactionInterface
	 * TwoProductReactionInterface}) define where starts the second line in this
	 * strange line description. See also usage of this field in CellDesigner
	 * parser.
	 */
	private int						num1;

	/**
	 * This value in reactions with three base reactants+products (reactions that
	 * implements
	 * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoReactantReactionInterface
	 * TwoReactantReactionInterface} or
	 * {@link lcsb.mapviewer.db.model.map.reaction.type.TwoProductReactionInterface
	 * TwoProductReactionInterface}) define where starts the third line in this
	 * strange line description. See also usage of this field in CellDesigner
	 * parser.
	 */
	private int						num2;

	/**
	 * This value defines in which segment center of the reaction is put.
	 */
	private Integer				index;

	/**
	 * Default constructor.
	 */
	public EditPoints() {
		points = new ArrayList<Point2D>();
		num0 = 0;
		num1 = 0;
		num2 = 0;
		index = null;
	}

	/**
	 * @return the points
	 * @see #points
	 */
	public List<Point2D> getPoints() {
		return points;
	}

	/**
	 * @param points
	 *          the points to set
	 * @see #points
	 */
	public void setPoints(List<Point2D> points) {
		this.points = points;
	}

	/**
	 * @return the num0
	 * @see #num0
	 */
	public int getNum0() {
		return num0;
	}

	/**
	 * @param num0
	 *          the num0 to set
	 * @see #num0
	 */
	public void setNum0(int num0) {
		this.num0 = num0;
	}

	/**
	 * @return the num1
	 * @see #num1
	 */
	public int getNum1() {
		return num1;
	}

	/**
	 * @param num1
	 *          the num1 to set
	 * @see #num1
	 */
	public void setNum1(int num1) {
		this.num1 = num1;
	}

	/**
	 * @return the num2
	 * @see #num2
	 */
	public int getNum2() {
		return num2;
	}

	/**
	 * @param num2
	 *          the num2 to set
	 * @see #num2
	 */
	public void setNum2(int num2) {
		this.num2 = num2;
	}

	/**
	 * @return the index
	 * @see #index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * @param index
	 *          the index to set
	 * @see #index
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * Returns number of points in the line.
	 * 
	 * @return number of points in the line
	 */
	public int size() {
		return points.size();
	}

}
