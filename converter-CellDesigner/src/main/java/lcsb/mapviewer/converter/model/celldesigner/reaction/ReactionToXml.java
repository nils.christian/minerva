package lcsb.mapviewer.converter.model.celldesigner.reaction;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerLineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerPointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.converter.model.celldesigner.types.OperatorType;
import lcsb.mapviewer.converter.model.celldesigner.types.OperatorTypeUtils;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.SimpleReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This is a part of {@link ReactionXmlParser} class functionality that allows
 * to export reaction into CellDesigner xml node.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionToXml extends XmlParser {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ReactionToXml.class);

  /**
   * Helper object used for manipulation on the point coorinates in CellDesigner
   * format.
   */
  private CellDesignerPointTransformation pointTransformation = new CellDesignerPointTransformation();

  /**
   * Helper object used for manipulation on the line structures in CellDesigner
   * format.
   */
  private CellDesignerLineTransformation lineTransformation = new CellDesignerLineTransformation();

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Helps in providing human readable identifiers of elements for logging.
   */
  private ElementUtils eu = new ElementUtils();

  /**
   * Defines if SBGN standard should be used.
   */
  private boolean sbgn;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param sbgn
   *          Should the converter use SBGN standard
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public ReactionToXml(CellDesignerElementCollection elements, boolean sbgn) {
    this.elements = elements;
    this.sbgn = sbgn;
  }

  /**
   * Transform reaction into CellDesigner xml representation.
   * 
   * @param reaction
   *          reaction to transform
   * @return xml representation of reaction
   * @throws ConverterException
   */
  public String toXml(Reaction reaction) throws InconsistentModelException {
    if (reaction.getReactants().get(0).getElement().equals(reaction.getProducts().get(0).getElement())) {
      throw new SelfReactionException("Reaction " + reaction.getElementId() + " is a self reference for element "
          + reaction.getProducts().get(0).getElement().getElementId());
    }
    StringBuilder sb = new StringBuilder();
    sb.append("<reaction ");
    sb.append("metaid=\"" + reaction.getIdReaction() + "\" ");
    sb.append("id=\"" + reaction.getIdReaction() + "\" ");
    sb.append("name=\"" + reaction.getName() + "\" ");
    sb.append("reversible=\"" + reaction.isReversible() + "\" ");
    sb.append(">\n");
    if (reaction.getNotes() != null && !reaction.getNotes().equals("")) {
      sb.append("<notes>");

      sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
      sb.append(reaction.getNotes());
      sb.append("</body></html>");

      sb.append("</notes>");

    }

    sb.append(getAnnotationXmlStringForReaction(reaction));
    sb.append(getListOfReactantsXmlStringForReaction(reaction));
    sb.append(getListOfProductsXmlStringForReaction(reaction));
    sb.append(getSbmlListOfModificationsXmlStringForReaction(reaction));
    sb.append(getKineticsLawXml(reaction));

    sb.append("</reaction>\n");
    return sb.toString();
  }

  private String getKineticsLawXml(Reaction reaction) throws InconsistentModelException {
    if (reaction.getKinetics() != null) {
      KineticsXmlParser xmlParser = new KineticsXmlParser(reaction.getModel());
      return xmlParser.toXml(reaction.getKinetics(), elements);
    } else {
      return "";
    }
  }

  /**
   * Returns xml node with list of modification.
   * 
   * @param reaction
   *          reaction for which list is generated
   * @return xml node with list of modification
   */
  private String getListOfModificationsXmlStringForReaction(Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:listOfModification>\n");

    // to preserve order of modifier we have to make it a little complicated

    Map<Modifier, NodeOperator> operators = new HashMap<Modifier, NodeOperator>();
    Set<NodeOperator> usedOperators = new HashSet<NodeOperator>();

    for (NodeOperator node : reaction.getOperators()) {
      if (node.isModifierOperator()) {
        for (AbstractNode modNode : node.getInputs()) {
          Modifier modifier = (Modifier) modNode;
          operators.put(modifier, node);
          if (node.getLine().getBeginPoint().distance(modifier.getLine().getEndPoint()) > Configuration.EPSILON) {
            logger.warn("Modifier should end in the same point as operator starts, but: "
                + node.getLine().getBeginPoint() + ", " + modifier.getLine().getEndPoint());
          }
        }
      }
    }
    for (Modifier modifier : reaction.getModifiers()) {
      NodeOperator node = operators.get(modifier);
      if (node != null) {
        if (!usedOperators.contains(node)) {
          usedOperators.add(node);
          sb.append(getModifierXmlString(node));
        }
      }
      sb.append(getModifierXmlString(modifier, node));
    }
    sb.append("</celldesigner:listOfModification>\n");
    return sb.toString();
  }

  /**
   * Returns sbml node with list of modifications.
   * 
   * @param reaction
   *          reaction for which list is generated
   * @return sbml node with list of modifications
   */
  private String getSbmlListOfModificationsXmlStringForReaction(Reaction reaction) {
    if (reaction.getModifiers().size() == 0) {
      return "";
    }
    StringBuilder sb = new StringBuilder();
    sb.append("<listOfModifiers>\n");
    for (Modifier modifier : reaction.getModifiers()) {
      sb.append(getModifierReferenceXmlString(modifier));
    }
    sb.append("</listOfModifiers>\n");
    return sb.toString();
  }

  /**
   * Creates modifierSpeciesReference sbml node for given modifer.
   * 
   * @param modifier
   *          modifier to be transaformed
   * @return modifierSpeciesReference sbml node for given modifer
   */
  private String getModifierReferenceXmlString(Modifier modifier) {
    StringBuilder sb = new StringBuilder();
    Species element = (Species) modifier.getElement();

    Set<Element> parents = new HashSet<>();

    // we need top parent species
    while (element.getComplex() != null) {
      element = element.getComplex();
      if (parents.contains(element)) {
        throw new InvalidArgumentException(eu.getElementTag(element) + " Complex information is cyclic");
      }
      parents.add(element);
    }

    sb.append("<modifierSpeciesReference ");
    sb.append("metaid=\"" + elements.getElementId(element) + "\" ");

    sb.append("species=\"" + elements.getElementId(element) + "\">\n");
    sb.append("<annotation>\n");
    sb.append("<celldesigner:extension>\n");
    sb.append("<celldesigner:alias>" + modifier.getElement().getElementId() + "</celldesigner:alias>\n");
    sb.append("</celldesigner:extension>\n");
    sb.append("</annotation>\n");
    sb.append("</modifierSpeciesReference>\n");

    return sb.toString();
  }

  /**
   * Creates xml node for given modifer operator.
   * 
   * @param modifierOperator
   *          modifier operator to be transaformed
   * @return xml node for given modifer operator
   */
  private String getModifierXmlString(NodeOperator modifierOperator) {
    StringBuilder sb = new StringBuilder();
    OperatorTypeUtils otu = new OperatorTypeUtils();
    ModifierTypeUtils mtu = new ModifierTypeUtils();
    ModifierType modifierType = mtu.getModifierTypeForOperator(modifierOperator);
    sb.append("<celldesigner:modification ");
    String type = otu.getStringTypeByOperator(modifierOperator);
    sb.append(" type=\"" + type + "\" ");
    sb.append(" modificationType=\"" + modifierType.getStringName() + "\" ");
    String modifiers = "";
    String aliases = "";
    for (AbstractNode modifier : modifierOperator.getInputs()) {
      if (modifier instanceof Modifier) {
        Modifier mod = (Modifier) modifier;
        if (modifiers.equals("")) {
          modifiers = elements.getElementId(mod.getElement());
          aliases = mod.getElement().getElementId();
        } else {
          modifiers += "," + elements.getElementId(mod.getElement());
          aliases += "," + mod.getElement().getElementId();
        }

      }
    }
    sb.append(" modifiers=\"" + modifiers + "\" ");
    sb.append(" aliases=\"" + aliases + "\" ");
    sb.append(" targetLineIndex=\"" + mtu.getTargetLineIndexByModifier(modifierOperator) + "\" ");
    PolylineData line = new PolylineData(modifierOperator.getLine());

    line.trimEnd(-modifierType.getTrimLength());

    // TODO
    // check the order (reverse or not, begin or end point)
    line = line.reverse();
    List<Point2D> points = lineTransformation.getPointsFromLine(line);
    sb.append(" editPoints=\"");
    for (int i = 1; i < points.size() - 1; i++) {
      sb.append(points.get(i).getX() + "," + points.get(i).getY() + " ");
    }
    sb.append(line.getEndPoint().getX() + "," + line.getEndPoint().getY());
    sb.append("\"");

    sb.append(">\n");
    PolylineData[] lines = new PolylineData[] { modifierOperator.getLine() };
    sb.append(getConnectSchemeXmlStringForLines(lines));

    sb.append("<celldesigner:line ");
    sb.append("width=\"" + modifierOperator.getLine().getWidth() + "\" ");
    sb.append("color=\"" + colorToString(modifierOperator.getLine().getColor()) + "\" ");
    sb.append("type=\"Straight\" ");
    sb.append("/>\n");

    sb.append("</celldesigner:modification>\n");
    return sb.toString();
  }

  /**
   * Creates xml node for given modifer.
   * 
   * @param modifier
   *          modifier to be transaformed
   * @param gate
   *          operator to which modifier is connected (if any)
   * @return xml node for given modifer
   */
  private String getModifierXmlString(Modifier modifier, NodeOperator gate) {
    StringBuilder sb = new StringBuilder();
    ModifierTypeUtils modifierTypeUtils = new ModifierTypeUtils();
    sb.append("<celldesigner:modification ");
    String type = modifierTypeUtils.getStringTypeByModifier(modifier);
    sb.append(" type=\"" + type + "\" ");
    sb.append(" modifiers=\"" + elements.getElementId(modifier.getElement()) + "\" ");
    sb.append(" aliases=\"" + modifier.getElement().getElementId() + "\" ");
    sb.append(" targetLineIndex=\"" + modifierTypeUtils.getTargetLineIndexByModifier(modifier) + "\" ");
    PolylineData line = new PolylineData(modifier.getLine());

    line.trimEnd(-modifierTypeUtils.getModifierTypeForClazz(modifier.getClass()).getTrimLength());
    if (gate != null) {
      line.setEndPoint(gate.getLine().getBeginPoint());
      CellDesignerAliasConverter converter = new CellDesignerAliasConverter(modifier.getElement(), sbgn);
      CellDesignerAnchor anchor = converter.getAnchorForCoordinates(modifier.getElement(), line.getBeginPoint());
      Point2D start = converter.getPointCoordinates(modifier.getElement(), anchor);
      line.setStartPoint(start);
    }

    List<Point2D> points = lineTransformation.getPointsFromLine(line);
    if (points.size() > 0) {

      boolean first = true;
      sb.append(" editPoints=\"");

      for (Point2D point : points) {
        if (first) {
          first = false;
        } else {
          sb.append(" ");
        }
        sb.append(point.getX() + "," + point.getY());
      }
      sb.append("\"");
    }

    sb.append(">\n");
    PolylineData[] lines = new PolylineData[] { modifier.getLine() };
    sb.append(getConnectSchemeXmlStringForLines(lines));
    sb.append(getLinkTargetXmlString(modifier));

    sb.append("<celldesigner:line ");
    sb.append("width=\"" + modifier.getLine().getWidth() + "\" ");
    sb.append("color=\"" + colorToString(modifier.getLine().getColor()) + "\" ");
    sb.append("type=\"Straight\" ");
    sb.append("/>\n");

    sb.append("</celldesigner:modification>\n");
    return sb.toString();
  }

  /**
   * Gets target link string for given modifier.
   * 
   * @param modifier
   *          modifier to be transformed
   * @return anchor xml node representing modifier connection to reaction
   */
  private String getLinkTargetXmlString(Modifier modifier) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:linkTarget species=\"" + elements.getElementId(modifier.getElement()) + "\" alias=\""
        + modifier.getElement().getElementId() + "\">\n");
    sb.append(getAnchorXml(modifier.getElement(), modifier.getLine().getBeginPoint()));
    sb.append("</celldesigner:linkTarget>\n");

    return sb.toString();
  }

  /**
   * Creates sbml node for list of products.
   * 
   * @param reaction
   *          reaction from which products are taken
   * @return sbml node representing lsit of products
   */
  private String getListOfProductsXmlStringForReaction(Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<listOfProducts>\n");
    for (Product product : reaction.getProducts()) {
      // metaid is missing, maybe it will work ;)

      Species element = (Species) product.getElement();

      Set<Element> parents = new HashSet<>();

      // we need top parent species
      while (element.getComplex() != null) {
        element = element.getComplex();
        if (parents.contains(element)) {
          throw new InvalidArgumentException(eu.getElementTag(element) + " Complex information is cyclic");
        }
        parents.add(element);
      }

      sb.append("<speciesReference species=\"" + elements.getElementId(element) + "\" ");
      if (product.getStoichiometry() != null) {
        sb.append(" stoichiometry=\"" + product.getStoichiometry() + "\" ");
      }
      sb.append(">\n");

      sb.append("<annotation>\n");
      sb.append("<celldesigner:extension>\n");
      sb.append("<celldesigner:alias>" + product.getElement().getElementId() + "</celldesigner:alias>\n");
      sb.append("</celldesigner:extension>\n");
      sb.append("</annotation>\n");
      sb.append("</speciesReference>\n");
    }
    sb.append("</listOfProducts>\n");
    return sb.toString();
  }

  /**
   * Creates sbml node for list of reactants.
   * 
   * @param reaction
   *          reaction from which products are taken
   * @return sbml node representing list of reactants
   */
  private String getListOfReactantsXmlStringForReaction(Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<listOfReactants>\n");
    for (Reactant reactant : reaction.getReactants()) {
      // metaid is missing, maybe it will work ;)
      Species species = (Species) reactant.getElement();

      Set<Element> parents = new HashSet<>();

      // we need top parent species
      while (species.getComplex() != null) {
        species = species.getComplex();
        if (parents.contains(species)) {
          throw new InvalidArgumentException(eu.getElementTag(species) + " Complex information is cyclic");
        }
        parents.add(species);
      }

      sb.append("<speciesReference species=\"" + elements.getElementId(species) + "\" ");
      if (reactant.getStoichiometry() != null) {
        sb.append(" stoichiometry=\"" + reactant.getStoichiometry() + "\" ");
      }
      sb.append(">\n");
      sb.append("<annotation>\n");
      sb.append("<celldesigner:extension>\n");
      sb.append("<celldesigner:alias>" + reactant.getElement().getElementId() + "</celldesigner:alias>\n");
      sb.append("</celldesigner:extension>\n");
      sb.append("</annotation>\n");
      sb.append("</speciesReference>\n");
    }
    sb.append("</listOfReactants>\n");
    return sb.toString();
  }

  /**
   * Creates annotation xml node for reaction.
   * 
   * @param reaction
   *          reaction to be processed
   * @return xml node representing annotation part
   */
  private String getAnnotationXmlStringForReaction(Reaction reaction) {
    ReactionLineData rdl = ReactionLineData.getByReactionType(reaction.getClass());
    if (rdl == null) {
      throw new InvalidArgumentException("Unknown reaction type: " + reaction.getClass());
    }
    String reactionClassString = rdl.getCellDesignerString();
    StringBuilder sb = new StringBuilder();
    sb.append("<annotation>\n");
    sb.append("<celldesigner:extension>\n");
    sb.append("<celldesigner:reactionType>" + reactionClassString + "</celldesigner:reactionType>");
    sb.append(getBaseReactantsXmlStringForReaction(reaction));
    sb.append(getBaseProductsXmlStringForReaction(reaction));
    sb.append(getListOfReactantLinksXmlStringForReaction(reaction));
    sb.append(getListOfProductLinksXmlStringForReaction(reaction));
    sb.append(getConnectSchemeXmlStringForReaction(reaction));
    sb.append(getEditPointsXmlStringForReaction(reaction));
    sb.append(getListOfModificationsXmlStringForReaction(reaction));
    sb.append(getListOfGateMembersXmlStringForReaction(reaction));

    sb.append("<celldesigner:line ");
    sb.append("width=\"" + reaction.getReactants().get(0).getLine().getWidth() + "\" ");
    sb.append("color=\"" + colorToString(reaction.getReactants().get(0).getLine().getColor()) + "\" ");
    sb.append("type=\"Straight\" ");
    sb.append("/>\n");

    XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser();
    sb.append("</celldesigner:extension>\n");
    sb.append(xmlAnnotationParser.dataSetToXmlString(reaction.getMiriamData()));
    sb.append("</annotation>\n");
    return sb.toString();
  }

  /**
   * Returns xml node with a list of gate members for reaction. For now only
   * {@link TwoReactantReactionInterface} is supported.
   * 
   * @param reaction
   *          reaction for which the cml will be returned
   * @return xml string with list of gate members for reaction
   */
  private StringBuilder getListOfGateMembersXmlStringForReaction(Reaction reaction) {
    StringBuilder result = new StringBuilder();
    if (reaction instanceof TwoReactantReactionInterface) {
      Reactant reactant1 = reaction.getReactants().get(0);
      Reactant reactant2 = reaction.getReactants().get(1);
      Product product = reaction.getProducts().get(0);
      NodeOperator operator = reaction.getOperators().get(0);
      OperatorType type = OperatorType.getTypeByClass(operator.getClass());

      // if type is not defined then we are working on something that doesn't
      // use gates
      if (type != null) {
        result.append("<celldesigner:listOfGateMember>\n");
        ReactionLineData lineData = ReactionLineData.getByLineType(reaction.getProducts().get(0).getLine().getType(),
            reaction.getProducts().get(0).getLine().getEndAtd().getArrowType());
        Element alias1 = reactant1.getElement();
        Element alias2 = reactant2.getElement();

        // product line
        result.append("<celldesigner:GateMember type=\"" + type.getStringName() + "\"");
        result.append(" aliases=\"" + alias1.getElementId() + "," + alias2.getElementId() + "\"");
        result.append(" modificationType=\"" + lineData.getCellDesignerString() + "\"");

        CellDesignerLineTransformation clt = new CellDesignerLineTransformation();
        List<Point2D> points = clt.getPointsFromLine(product.getLine());

        result.append(" editPoints=\"");
        for (int i = 0; i < points.size(); i++) {
          result.append(points.get(i).getX() + "," + points.get(i).getY() + " ");
        }
        result.append(product.getLine().getBeginPoint().getX() + "," + product.getLine().getBeginPoint().getY() + " ");
        result.append("\">\n");

        result.append("<celldesigner:connectScheme connectPolicy=\"direct\">\n");
        result.append("<celldesigner:listOfLineDirection>\n");
        for (int i = 0; i < product.getLine().getPoints().size() - 1; i++) {
          result.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
        }
        result.append("</celldesigner:listOfLineDirection>\n");
        result.append("</celldesigner:connectScheme>\n");

        result.append("</celldesigner:GateMember>\n");

        // reactant 1 line

        result.append("<celldesigner:GateMember type=\"" + lineData.getCellDesignerString() + "\"");
        result.append(" aliases=\"" + alias1.getElementId() + "\"");

        points = clt.getPointsFromLine(reactant1.getLine());

        result.append(" editPoints=\"");
        for (int i = 0; i < points.size(); i++) {
          result.append(points.get(i).getX() + "," + points.get(i).getY() + " ");
        }
        result.append("\">\n");

        result.append("<celldesigner:connectScheme connectPolicy=\"direct\">\n");
        result.append("<celldesigner:listOfLineDirection>\n");
        for (int i = 0; i < reactant1.getLine().getPoints().size() - 1; i++) {
          result.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
        }
        result.append("</celldesigner:listOfLineDirection>\n");
        result.append("</celldesigner:connectScheme>\n");

        result.append("</celldesigner:GateMember>\n");

        // reactant 2 line

        result.append("<celldesigner:GateMember type=\"" + lineData.getCellDesignerString() + "\"");
        result.append(" aliases=\"" + alias2.getElementId() + "\"");

        points = clt.getPointsFromLine(reactant2.getLine());

        result.append(" editPoints=\"");
        for (int i = 0; i < points.size(); i++) {
          result.append(points.get(i).getX() + "," + points.get(i).getY() + " ");
        }
        result.append("\">\n");

        result.append("<celldesigner:connectScheme connectPolicy=\"direct\">\n");
        result.append("<celldesigner:listOfLineDirection>\n");
        for (int i = 0; i < reactant2.getLine().getPoints().size() - 1; i++) {
          result.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
        }
        result.append("</celldesigner:listOfLineDirection>\n");
        result.append("</celldesigner:connectScheme>\n");

        result.append("</celldesigner:GateMember>\n");

        result.append("</celldesigner:listOfGateMember>\n");
      }
    }
    return result;
  }

  /**
   * Creates xml node (listOfProductLinks) with list of products.
   * 
   * @param reaction
   *          reaction for which list of products is created
   * @return xml node (listOfProductLinks) with list of products
   */
  private String getListOfProductLinksXmlStringForReaction(Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:listOfProductLinks>\n");
    int firstElement = 1;
    if (reaction instanceof TwoProductReactionInterface) {
      firstElement = 2;
    }
    for (int i = firstElement; i < reaction.getProducts().size(); i++) {
      sb.append(getProductLinkXmlString(reaction.getProducts().get(i)));
    }
    sb.append("</celldesigner:listOfProductLinks>\n");
    return sb.toString();
  }

  /**
   * Creates xml node (listOfReactantLinks) with list of reactants.
   * 
   * @param reaction
   *          reaction for which list of reactants is created
   * @return xml node (listOfReactantLinks) with list of reactants
   */
  private String getListOfReactantLinksXmlStringForReaction(Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:listOfReactantLinks>\n");
    int firstElement = 1;
    if (reaction instanceof TwoReactantReactionInterface) {
      firstElement = 2;
    }
    for (int i = firstElement; i < reaction.getReactants().size(); i++) {
      sb.append(getReactantLinkXmlString(reaction.getReactants().get(i)));
    }
    sb.append("</celldesigner:listOfReactantLinks>\n");
    return sb.toString();
  }

  /**
   * Creates rectantLink xml node for reactant.
   * 
   * @param reactant
   *          reactant to be transformed
   * @return xml node describing reactantLink
   */
  private String getReactantLinkXmlString(Reactant reactant) {
    Element alias = reactant.getElement();
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:reactantLink ");
    sb.append("reactant=\"" + elements.getElementId(alias) + "\" ");
    sb.append("alias=\"" + alias.getElementId() + "\" ");
    // targetLineIndex is missing (maybe it's unimportant :))
    sb.append(">\n");

    sb.append(getAnchorXml(alias, reactant.getLine().getBeginPoint()));

    sb.append("<celldesigner:connectScheme connectPolicy=\"direct\">\n");
    sb.append("<celldesigner:listOfLineDirection>\n");
    for (int i = 0; i < reactant.getLine().getPoints().size() - 1; i++) {
      sb.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
    }
    sb.append("</celldesigner:listOfLineDirection>\n");
    sb.append("</celldesigner:connectScheme>\n");

    // TODO
    sb.append(getEditPointsXmlStringForLine(new PolylineData[] { reactant.getLine() }, 0));
    sb.append("<celldesigner:line ");
    sb.append("width=\"" + reactant.getLine().getWidth() + "\" ");
    sb.append("color=\"" + colorToString(reactant.getLine().getColor()) + "\" ");
    sb.append("type=\"Straight\" ");
    sb.append("/>\n");
    sb.append("</celldesigner:reactantLink>");
    return sb.toString();
  }

  /**
   * Creates productLink xml node for product.
   * 
   * @param product
   *          product to be transformed
   * @return xml node describing productLink
   */
  private String getProductLinkXmlString(Product product) {
    Element alias = product.getElement();
    PolylineData[] lines = new PolylineData[] { product.getLine() };

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:productLink ");
    sb.append("product=\"" + elements.getElementId(alias) + "\" ");
    sb.append("alias=\"" + alias.getElementId() + "\" ");
    // targetLineIndex is missing (maybe it's unimportant :))
    sb.append(">\n");

    sb.append(getAnchorXml(alias, product.getLine().getEndPoint()));

    sb.append(getConnectSchemeXmlStringForLines(lines));

    // TODO
    sb.append(getEditPointsXmlStringForLine(lines, 0));
    sb.append("<celldesigner:line ");
    sb.append("width=\"" + product.getLine().getWidth() + "\" ");
    sb.append("color=\"" + colorToString(product.getLine().getColor()) + "\" ");
    sb.append("type=\"Straight\" ");
    sb.append("/>\n");
    sb.append("</celldesigner:productLink>");
    return sb.toString();
  }

  /**
   * Creates valid xml node with connectScheme for the set of lines.
   * 
   * @param lines
   *          list of lines to be included in connectScheme. As far as I remember
   *          only one or three lines could be provided.
   * @return xml node with connectScheme
   */
  String getConnectSchemeXmlStringForLines(PolylineData[] lines) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:connectScheme ");
    sb.append("connectPolicy=\"direct\" ");
    // where on the line reactangle is positioned (it's not necessary so we skip
    // it)
    // if (rectangleIndex != null) {
    // sb.append("rectangleIndex=\"" + rectangleIndex + "\" ");
    // }
    sb.append(">\n");
    sb.append("<celldesigner:listOfLineDirection>\n");
    int armId = 0;
    for (PolylineData line : lines) {
      String arm = "";
      if (lines.length > 1) {
        arm = " arm\"" + armId + "\" ";
        armId++;
      }
      for (int i = 0; i < line.getPoints().size() - 1; i++) {
        sb.append("<celldesigner:lineDirection " + arm + " index=\"" + i + "\" value=\"unknown\"/>\n");
      }
    }
    sb.append("</celldesigner:listOfLineDirection>\n");
    sb.append("</celldesigner:connectScheme>\n");
    return sb.toString();
  }

  /**
   * Creates
   * {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   * structure representing lines in CellDesigner.
   * 
   * @param lines
   *          lines to be transformed into single
   *          {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   *          structure
   * @param centerIndex
   *          where the central line is positioned
   * @return xml node
   */
  private String getEditPointsXmlStringForLine(PolylineData[] lines, Integer centerIndex) {
    return getEditPointsXmlStringForLine(lines, null, centerIndex);
  }

  /**
   * Creates
   * {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   * structure representing lines in CellDesigner.
   * 
   * @param lines
   *          lines to be transformed into single
   *          {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   *          structure
   * @param centerPoint
   *          central point whene list of lines is greater than 1
   * @param centerIndex
   *          where the central line is positioned
   * @return xml node
   */
  private String getEditPointsXmlStringForLine(PolylineData[] lines, Point2D centerPoint, Integer centerIndex) {
    StringBuilder sb = new StringBuilder();

    sb.append("<celldesigner:editPoints ");
    if (lines.length > 1) {
      for (int i = 0; i < lines.length; i++) {
        sb.append(" num" + i + "=\"" + (lines[i].getPoints().size() - 2) + "\" ");
      }
      if (centerIndex != null) {
        sb.append(" tShapeIndex=\"" + centerIndex + "\"");
      }
    }
    sb.append(">");

    boolean first = true;
    for (PolylineData pd : lines) {
      List<Point2D> points = lineTransformation.getPointsFromLine(pd);

      for (Point2D point : points) {
        if (first) {
          first = false;
        } else {
          sb.append(" ");
        }
        sb.append(point.getX() + "," + point.getY());
      }
    }
    if (centerPoint != null) {
      sb.append(" " + centerPoint.getX() + "," + centerPoint.getY());
    }
    sb.append("</celldesigner:editPoints>\n");

    // when we don't have any points then we cannot create editPoints node (CD
    // throws null pointer exception)
    if (first && centerPoint == null) {
      return "";
    }
    return sb.toString();
  }

  /**
   * Returns xml node representing
   * {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   * structure.
   * 
   * @param reaction
   *          reaction for which xml node is generated
   * @return xml node representing
   *         {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   *         structure
   */
  String getEditPointsXmlStringForReaction(Reaction reaction) {
    if (reaction instanceof SimpleReactionInterface) {
      Product product = reaction.getProducts().get(0);
      Reactant reactant = reaction.getReactants().get(0);
      List<Point2D> points = new ArrayList<Point2D>();
      for (int i = 0; i < reactant.getLine().getPoints().size() - 1; i++) {
        points.add(reactant.getLine().getPoints().get(i));
      }
      for (int i = 1; i < product.getLine().getPoints().size(); i++) {
        points.add(product.getLine().getPoints().get(i));
      }

      PolylineData pd = new PolylineData(points);
      // TODO
      return getEditPointsXmlStringForLine(new PolylineData[] { pd }, reactant.getLine().getPoints().size() - 1);
    } else if (reaction instanceof TwoReactantReactionInterface) {
      Reactant reactant1 = reaction.getReactants().get(0);
      Reactant reactant2 = reaction.getReactants().get(1);
      PolylineData line1 = reactant1.getLine();
      PolylineData line2 = reactant2.getLine();
      List<Point2D> points = new ArrayList<Point2D>();
      for (NodeOperator operator : reaction.getOperators()) {
        if (operator.isReactantOperator()) {
          boolean process = true;
          for (AbstractNode node : operator.getInputs()) {
            if (!(node instanceof Reactant)) {
              process = false;
            }
          }
          if (process) {
            for (int i = 0; i < operator.getLine().getPoints().size() - 1; i++) {
              points.add(operator.getLine().getPoints().get(i));
            }
          }
        }
      }
      Product product = reaction.getProducts().get(0);
      for (int i = 1; i < product.getLine().getPoints().size(); i++) {
        points.add(product.getLine().getPoints().get(i));
      }

      Point2D endPoint = points.get(points.size() - 1);
      CellDesignerAliasConverter converter = new CellDesignerAliasConverter(product.getElement(), sbgn);
      CellDesignerAnchor anchor = converter.getAnchorForCoordinates(product.getElement(), endPoint);
      if (anchor == null) { // when we have no anchor, then end point is set
        // to
        // the center of alias
        points.set(points.size() - 1, product.getElement().getCenter());
      }

      PolylineData line3 = new PolylineData(points);

      Point2D pointC = reactant1.getElement().getCenter();
      Point2D pointA = reactant2.getElement().getCenter();
      Point2D pointB = product.getElement().getCenter();

      Point2D pointO = line3.getBeginPoint();

      Point2D centerPoint = pointTransformation.getCoordinatesInCellDesignerBase(pointA, pointB, pointC, pointO);

      return getEditPointsXmlStringForLine(
          new PolylineData[] { line1.reverse(), line2.reverse(), line3.reverse().reverse() }, centerPoint,
          line3.getPoints().size() - product.getLine().getPoints().size());

    } else if (reaction instanceof TwoProductReactionInterface) {
      List<Point2D> points = new ArrayList<Point2D>();
      Reactant reactant = reaction.getReactants().get(0);
      Product product1 = reaction.getProducts().get(0);
      Product product2 = reaction.getProducts().get(1);
      for (int i = 0; i < reactant.getLine().getPoints().size() - 1; i++) {
        points.add(reactant.getLine().getPoints().get(i));
      }

      Point2D startPoint = points.get(0);
      CellDesignerAliasConverter converter = new CellDesignerAliasConverter(reactant.getElement(), sbgn);
      CellDesignerAnchor anchor = converter.getAnchorForCoordinates(reactant.getElement(), startPoint);
      if (anchor == null) { // when we have no anchor, then end point is set
        // to
        // the center of alias
        points.set(points.size() - 1, reactant.getElement().getCenter());
      }
      int index = points.size();
      for (NodeOperator operator : reaction.getOperators()) {
        if (operator.isProductOperator()) {
          boolean process = true;
          for (AbstractNode node : operator.getOutputs()) {
            if (!(node instanceof Product)) {
              process = false;
            }
          }
          if (process) {
            for (int i = operator.getLine().getPoints().size() - 2; i >= 0; i--) {
              points.add(operator.getLine().getPoints().get(i));
            }
          }
        }
      }
      index = points.size() - index - 1;

      PolylineData line1 = new PolylineData(points);

      PolylineData line2 = product1.getLine();
      PolylineData line3 = product2.getLine();

      Point2D pointA = product1.getElement().getCenter();
      Point2D pointB = product2.getElement().getCenter();
      Point2D pointC = reactant.getElement().getCenter();

      Point2D pointO = line2.getBeginPoint();

      Point2D centerPoint = pointTransformation.getCoordinatesInCellDesignerBase(pointA, pointB, pointC, pointO);

      return getEditPointsXmlStringForLine(new PolylineData[] { line1.reverse(), line2, line3 }, centerPoint, index);
    } else {
      throw new InvalidArgumentException("Unhandled reaction type: " + reaction.getClass());
    }
  }

  /**
   * Creates valid xml node with connectScheme for the reaction.
   * 
   * @param reaction
   *          reaction for which the connectSchema is going to be created
   * @return xml node with connectScheme
   */
  String getConnectSchemeXmlStringForReaction(Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:connectScheme ");
    sb.append("connectPolicy=\"direct\" ");
    int rectangleIndex = -1;
    if (reaction instanceof SimpleReactionInterface) {
      rectangleIndex = reaction.getReactants().get(0).getLine().getPoints().size() - 2;
      sb.append("rectangleIndex=\"" + rectangleIndex + "\" ");
    }
    sb.append(">\n");
    sb.append("<celldesigner:listOfLineDirection>\n");
    if (reaction instanceof SimpleReactionInterface) {
      int size = reaction.getReactants().get(0).getLine().getPoints().size()
          + reaction.getProducts().get(0).getLine().getPoints().size() - 2 - 1;
      for (int i = 0; i < size; i++) {
        sb.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
      }
    } else if (reaction instanceof TwoProductReactionInterface || reaction instanceof TwoReactantReactionInterface) {
      String string = getEditPointsXmlStringForReaction(reaction);
      Pattern p0 = Pattern.compile("num0=\"([0-9]+)");
      Pattern p1 = Pattern.compile("num1=\"([0-9]+)");
      Pattern p2 = Pattern.compile("num2=\"([0-9]+)");
      Matcher m0 = p0.matcher(string);
      Matcher m1 = p1.matcher(string);
      Matcher m2 = p2.matcher(string);
      int num0 = 0;
      int num1 = 0;
      int num2 = 0;

      if (m0.find() && m1.find() && m2.find()) {
        num0 = Integer.parseInt(m0.group(1));
        num1 = Integer.parseInt(m1.group(1));
        num2 = Integer.parseInt(m2.group(1));
      } else {
        throw new InvalidArgumentException("Invalid editPoints string" + string);
      }

      for (int i = 0; i <= num0; i++) {
        sb.append("<celldesigner:lineDirection arm=\"0\" index=\"" + i + "\" value=\"unknown\"/>\n");
      }
      for (int i = 0; i <= num1; i++) {
        sb.append("<celldesigner:lineDirection arm=\"1\" index=\"" + i + "\" value=\"unknown\"/>\n");
      }
      for (int i = 0; i <= num2; i++) {
        sb.append("<celldesigner:lineDirection arm=\"2\" index=\"" + i + "\" value=\"unknown\"/>\n");
      }
    } else {
      throw new InvalidArgumentException("Unknown reaction type: " + reaction.getClass());

    }
    sb.append("</celldesigner:listOfLineDirection>\n");
    sb.append("</celldesigner:connectScheme>\n");
    return sb.toString();
  }

  /**
   * Returns xml node with list of base products.
   * 
   * @param reaction
   *          reaction for which the node is created
   * @return xml node with list of base products
   */
  private String getBaseProductsXmlStringForReaction(Reaction reaction) {
    List<Product> products = new ArrayList<Product>();
    products.add(reaction.getProducts().get(0));
    if (reaction instanceof TwoProductReactionInterface) {
      products.add(reaction.getProducts().get(1));
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:baseProducts>\n");
    for (Product product : products) {
      Element alias = product.getElement();
      sb.append("<celldesigner:baseProduct ");
      sb.append("species=\"" + elements.getElementId(product.getElement()) + "\" ");
      sb.append("alias=\"" + product.getElement().getElementId() + "\" ");
      sb.append(">\n");
      sb.append(getAnchorXml(alias, product.getLine().getEndPoint()));

      sb.append("</celldesigner:baseProduct>\n");
    }
    sb.append("</celldesigner:baseProducts>\n");
    return sb.toString();
  }

  /**
   * Returns xml node with list of base reactants.
   * 
   * @param reaction
   *          reaction for which the node is created
   * @return xml node with list of base reactants
   */
  private String getBaseReactantsXmlStringForReaction(Reaction reaction) {
    List<Reactant> reactants = new ArrayList<Reactant>();
    reactants.add(reaction.getReactants().get(0));
    if (reaction instanceof TwoReactantReactionInterface) {
      reactants.add(reaction.getReactants().get(1));
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:baseReactants>\n");
    for (Reactant reactant : reactants) {
      Element alias = reactant.getElement();
      sb.append("<celldesigner:baseReactant ");
      sb.append("species=\"" + elements.getElementId(reactant.getElement()) + "\" ");
      sb.append("alias=\"" + reactant.getElement().getElementId() + "\" ");
      sb.append(">\n");
      sb.append(getAnchorXml(alias, reactant.getLine().getBeginPoint()));

      sb.append("</celldesigner:baseReactant>\n");
    }
    sb.append("</celldesigner:baseReactants>\n");
    return sb.toString();
  }

  /**
   * Returns xml node that describes anchor point.
   * 
   * @param alias
   *          alias on which we are looking for anchor
   * @param point
   *          point on the alias that should be aligned to the closest anchor
   *          point
   * @return xml node with anchor point
   */
  private String getAnchorXml(Element alias, Point2D point) {
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(alias, sbgn);
    CellDesignerAnchor anchor = converter.getAnchorForCoordinates(alias, point);
    if (anchor != null) {
      return "<celldesigner:linkAnchor position=\"" + anchor + "\"/>\n";
    } else {
      return "";
    }
  }

}
