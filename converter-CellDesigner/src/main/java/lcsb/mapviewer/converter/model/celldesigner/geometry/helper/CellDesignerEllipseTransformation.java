package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;

import lcsb.mapviewer.common.geometry.EllipseTransformation;

import org.apache.log4j.Logger;

/**
 * This class contains basic operators on ellipse used by CellDesigner
 * converters.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerEllipseTransformation extends EllipseTransformation {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger	= Logger.getLogger(CellDesignerEllipseTransformation.class.getName());

	/**
	 * Method returns a cross point between ellipse and a line (from center point
	 * to the anchor point).
	 * 
	 * @param x
	 *          x coordinate of the ellipse
	 * @param y
	 *          y coordinate of the ellipse
	 * @param width
	 *          width of the ellipse
	 * @param height
	 *          height of the ellipse
	 * @param anchor
	 *          anchor on ellipse where we look for the point
	 * @return coordinates on the ellipse described by the anchor point
	 */
	public Point2D getPointOnEllipseByAnchor(double x, double y, double width, double height, CellDesignerAnchor anchor) {

		if (anchor == null || anchor.getAngle() == null) {
			return new Point2D.Double(x + width / 2, y + height / 2);
		} else {
			double angle = anchor.getAngle();
			return new Point2D.Double(x + width / 2 + Math.cos(angle) * width / 2, y + height / 2 + Math.sin(angle) * height / 2);
		}
	}
}
