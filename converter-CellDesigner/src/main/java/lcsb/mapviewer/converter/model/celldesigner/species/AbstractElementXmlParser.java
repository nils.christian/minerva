package lcsb.mapviewer.converter.model.celldesigner.species;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Abstract class with the interface to parse element in CellDesigner file.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          type of the CellDesigner object to parse
 * @param <S>
 *          type of the model object to be obtained
 */
public abstract class AbstractElementXmlParser<T extends CellDesignerElement<?>, S extends Element> extends XmlParser {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(AbstractElementXmlParser.class.getName());

  /**
   * List of special strings in CellDesigner that should be translated into some
   * UTF characters.
   */
  private List<Pair<String, String>> encodedStrings = new ArrayList<Pair<String, String>>();

  /**
   * Parser of the notes that allows to extract additional structured data for the
   * object.
   */
  private RestAnnotationParser rap = new RestAnnotationParser();

  /**
   * Method that parses CellDesigner xml node and translate it into object.
   * Returns a {@link Pair} containing CellDesigner identifier in
   * {@link Pair#left} and element in {@link Pair#right}.
   * 
   * @param node
   *          xml node be parsed
   * @return {@link Pair} containing CellDesigner identifier in {@link Pair#left}
   *         and element parsed from xml node in {@link Pair#right}.
   * @throws InvalidXmlSchemaException
   *           thrown when xml is invalid
   */
  public abstract Pair<String, T> parseXmlElement(Node node) throws InvalidXmlSchemaException;

  /**
   * Default constructor.
   */
  protected AbstractElementXmlParser() {
    encodedStrings.add(new Pair<String, String>("_underscore_", "_"));
    encodedStrings.add(new Pair<String, String>("_BR_", "\n"));
    encodedStrings.add(new Pair<String, String>("_br_", "\n"));
    encodedStrings.add(new Pair<String, String>("_plus_", "+"));
    encodedStrings.add(new Pair<String, String>("_minus_", "-"));
    encodedStrings.add(new Pair<String, String>("_slash_", "/"));
    encodedStrings.add(new Pair<String, String>("_space_", " "));
    encodedStrings.add(new Pair<String, String>("_Alpha_", "Α"));
    encodedStrings.add(new Pair<String, String>("_alpha_", "α"));
    encodedStrings.add(new Pair<String, String>("_Beta_", "Β"));
    encodedStrings.add(new Pair<String, String>("_beta_", "β"));
    encodedStrings.add(new Pair<String, String>("_Gamma_", "Γ"));
    encodedStrings.add(new Pair<String, String>("_gamma_", "γ"));
    encodedStrings.add(new Pair<String, String>("_Delta_", "Δ"));
    encodedStrings.add(new Pair<String, String>("_delta_", "δ"));
    encodedStrings.add(new Pair<String, String>("_Epsilon_", "Ε"));
    encodedStrings.add(new Pair<String, String>("_epsilon_", "ε"));
    encodedStrings.add(new Pair<String, String>("_Zeta_", "Ζ"));
    encodedStrings.add(new Pair<String, String>("_zeta_", "ζ"));
    encodedStrings.add(new Pair<String, String>("_Eta_", "Η"));
    encodedStrings.add(new Pair<String, String>("_eta_", "η"));
    encodedStrings.add(new Pair<String, String>("_Theta_", "Θ"));
    encodedStrings.add(new Pair<String, String>("_theta_", "θ"));
    encodedStrings.add(new Pair<String, String>("_Iota_", "Ι"));
    encodedStrings.add(new Pair<String, String>("_iota_", "ι"));
    encodedStrings.add(new Pair<String, String>("_Kappa_", "Κ"));
    encodedStrings.add(new Pair<String, String>("_kappa_", "κ"));
    encodedStrings.add(new Pair<String, String>("_Lambda_", "Λ"));
    encodedStrings.add(new Pair<String, String>("_lambda_", "λ"));
    encodedStrings.add(new Pair<String, String>("_Mu_", "Μ"));
    encodedStrings.add(new Pair<String, String>("_mu_", "μ"));
    encodedStrings.add(new Pair<String, String>("_Nu_", "Ν"));
    encodedStrings.add(new Pair<String, String>("_nu_", "ν"));
    encodedStrings.add(new Pair<String, String>("_Xi_", "Ξ"));
    encodedStrings.add(new Pair<String, String>("_xi_", "ξ"));
    encodedStrings.add(new Pair<String, String>("_Omicron_", "Ο"));
    encodedStrings.add(new Pair<String, String>("_omicron_", "ο"));
    encodedStrings.add(new Pair<String, String>("_Pi_", "Π"));
    encodedStrings.add(new Pair<String, String>("_pi_", "π"));
    encodedStrings.add(new Pair<String, String>("_Rho_", "Ρ"));
    encodedStrings.add(new Pair<String, String>("_rho_", "ρ"));
    encodedStrings.add(new Pair<String, String>("_Sigma_", "Σ"));
    encodedStrings.add(new Pair<String, String>("_sigma_", "σ"));
    encodedStrings.add(new Pair<String, String>("_Tau_", "Τ"));
    encodedStrings.add(new Pair<String, String>("_tau_", "τ"));
    encodedStrings.add(new Pair<String, String>("_Upsilon_", "Υ"));
    encodedStrings.add(new Pair<String, String>("_upsilon_", "υ"));
    encodedStrings.add(new Pair<String, String>("_Phi_", "Φ"));
    encodedStrings.add(new Pair<String, String>("_phi_", "φ"));
    encodedStrings.add(new Pair<String, String>("_Chi_", "Χ"));
    encodedStrings.add(new Pair<String, String>("_chi_", "χ"));
    encodedStrings.add(new Pair<String, String>("_Psi_", "Ψ"));
    encodedStrings.add(new Pair<String, String>("_psi_", "ψ"));
    encodedStrings.add(new Pair<String, String>("_Omega_", "Ω"));
    encodedStrings.add(new Pair<String, String>("_omega_", "ω"));
  }

  /**
   * Parses CellDEsigner xml node that is given as a plain text into element.
   * Returns a {@link Pair} containing CellDesigner identifier in
   * {@link Pair#left} and element in {@link Pair#right}.
   * 
   * @param xmlString
   *          node to parse
   * @return {@link Pair} containing CellDesigner identifier in {@link Pair#left}
   *         and element parsed from xml node in {@link Pair#right}.
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  public Pair<String, T> parseXmlElement(String xmlString) throws InvalidXmlSchemaException {
    Document doc = getXmlDocumentFromString(xmlString);
    NodeList root = doc.getChildNodes();
    return parseXmlElement(root.item(0));

  }

  /**
   * Method transforms element into CellDesigner xml string.
   * 
   * @param element
   *          object to be transformed
   * @return CellDesigner xml string representing element
   * @throws InconsistentModelException
   *           thrown when xml cannot be generated because structure of objects is
   *           invalid
   */
  public abstract String toXml(S element) throws InconsistentModelException;

  /**
   * This method decodes CellDesigner string with all known CellDesigner special
   * tokens.
   * 
   * @param name
   *          string to be decoded
   * @return decoded string
   */
  protected String decodeName(String name) {
    String result = name;
    for (Pair<String, String> pair : encodedStrings) {
      result = result.replaceAll(pair.getLeft(), pair.getRight());
    }
    return result;
  }

  /**
   * This method encodes {@link String} using CellDesigner special tokens.
   * 
   * @param name
   *          string to be decoded
   * @return decoded string
   */
  protected String encodeName(String name) {
    String result = name;
    for (Pair<String, String> pair : encodedStrings) {
      result = result.replace(pair.getRight(), pair.getLeft());
    }
    return result;
  }

  /**
   * @return the rap
   */
  protected RestAnnotationParser getRap() {
    return rap;
  }
}
