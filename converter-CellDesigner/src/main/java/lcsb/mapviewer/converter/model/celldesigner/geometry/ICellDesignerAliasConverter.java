package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.Point2D;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Interface that defines functions useful in transforming graphical information
 * from objects retrieved from CellDesigner xml into absolute values.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 */
public interface ICellDesignerAliasConverter<T extends Element> {
  /**
   * This method computes which anchor is the most probable for describing the
   * point on the border of alias. There are 16 possible anchors on the border of
   * every alias and one undefined point that will be used if no point can be
   * found (see {@link CellDesignerAnchor} to find information about anchors).
   * 
   * @param alias
   *          object for which we try to find anchor
   * @param point
   *          point on the border of alias for which we try to find anchor
   * @return CellDesigner anchor point that describe the best point on the border
   */
  CellDesignerAnchor getAnchorForCoordinates(T alias, Point2D point);

  /**
   * This method transform CellDesigner anchor point on the alias into x,y
   * coordinates.
   * 
   * @param alias
   *          we want to find coordinates on this alias border
   * @param anchor
   *          CelDesigner anchor that defines point on the border
   * @return point on the border that is defined by the anchor point
   */
  Point2D getPointCoordinates(T alias, CellDesignerAnchor anchor);

  /**
   * This method transform CellDesigner anchor point on the alias into x,y
   * coordinates. If the anchor point doesn't define point then the line parameter
   * is used to find the right angle on the alias border and point on the border
   * with this angle is used as a result.
   * 
   * @param alias
   *          we want to find coordinates on this alias border
   * @param anchor
   *          CelDesigner anchor that defines point on the border
   * @param line
   *          line that starts in the alias
   * @return point on the border that is defined by the anchor point
   */
  Point2D getAnchorPointCoordinates(T alias, CellDesignerAnchor anchor, PolylineData line);

  Point2D getResidueCoordinates(final T species, double angle);

  Double getAngleForPoint(T alias, Point2D position);

  public Double getCellDesignerPositionByCoordinates(ModificationResidue mr);

  /**
   * Gets coordinates on element based on CellDesigner position
   * 
   * @param element
   *          element on which we want to find coordinates
   * @param pos
   *          CellDesigner position (value between 0-1)
   * @return coordinates on the element
   */
  public Point2D getCoordinatesByPosition(Element element, Double pos);

  /**
   * Gets coordinates on element based on CellDesigner position taking into
   * account width of the {@link ModificationResidue} that we want to place.
   * 
   * @param element
   *          element on which we want to find coordinates
   * @param pos
   *          CellDesigner position (value between 0-1)
   * @param modificationWidth
   *          width of the {@link ModificationResidue}
   * @return coordinates on the element
   */
  public Point2D getCoordinatesByPosition(Element element, Double pos, Double modificationWidth);

  public Double getCellDesignerSize(ModificationResidue mr);

  public Double getWidthBySize(Element element, Double size);

}
