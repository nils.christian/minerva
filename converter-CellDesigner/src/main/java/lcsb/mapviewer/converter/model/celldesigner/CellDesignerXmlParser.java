package lcsb.mapviewer.converter.model.celldesigner;

import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.alias.AliasCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.compartment.CompartmentCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.function.FunctionCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.parameter.ParameterCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.reaction.ReactionCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.species.InternalModelSpeciesData;
import lcsb.mapviewer.converter.model.celldesigner.species.SpeciesCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.unit.UnitCollectionXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class is a parser for CellDesigner files. There are two typical use
 * cases:
 * <ul>
 * <li>CellDesigner xml -> our model. To perform this action
 * {@link #createModel(Params)} method should be called.</li>
 * <li>our model -> CellDesigner xml. To perform this action
 * {@link #toXml(Model)} method should be called.</li>
 * </ul>
 * <p/>
 * CellDEsigner format is the extension of <a href="http://sbml.org">SBML</a>.
 * More information about this format could be found <a href=
 * "http://www.celldesigner.org/documents/CellDesigner4ExtensionTagSpecificationE.pdf"
 * >here</a>.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerXmlParser extends XmlParser implements IConverter {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(CellDesignerXmlParser.class.getName());

  /**
   * CellDesigner parser for layers.
   */
  private LayerXmlParser layerParser = new LayerXmlParser();

  /**
   * CellDesigner parser for compartments collections.
   */
  private CompartmentCollectionXmlParser compartmentCollectionXmlParser;

  /**
   * CellDesigner parser for species collections.
   */
  private SpeciesCollectionXmlParser speciesSbmlParser;

  /**
   * CellDesigner parser for alias collections.
   */
  private AliasCollectionXmlParser aliasCollectionParser;

  /**
   * Annotation parser.
   */
  private RestAnnotationParser rap = new RestAnnotationParser();

  @Override
  public Model createModel(ConverterParams params) throws InvalidInputDataExecption {
    CellDesignerElementCollection elements = new CellDesignerElementCollection();
    FunctionCollectionXmlParser functionParser = new FunctionCollectionXmlParser();
    UnitCollectionXmlParser unitParser = new UnitCollectionXmlParser();

    Model model = new ModelFullIndexed(null);

    if (params.getFilename() != null) {
      model.setName(FilenameUtils.getBaseName(params.getFilename()));
    }
    speciesSbmlParser = new SpeciesCollectionXmlParser(elements);
    aliasCollectionParser = new AliasCollectionXmlParser(elements, model);
    compartmentCollectionXmlParser = new CompartmentCollectionXmlParser(elements);

    ParameterCollectionXmlParser parameterParser = new ParameterCollectionXmlParser(model);

    DOMParser parser = new DOMParser();
    try {
      parser.parse(params.getSource());
    } catch (IOException e) {
      throw new InvalidInputDataExecption("IO Problem with a file: " + params.getSource().getSystemId(), e);
    }
    Document doc = parser.getDocument();
    try {

      // Get the document's root XML node
      NodeList root = doc.getChildNodes();

      // Navigate down the hierarchy to get to the CEO node
      Node sbmlNode = getNode("SBML", root);
      if (sbmlNode == null) {
        throw new InvalidInputDataExecption("No SBML node");
      }

      Node modelNode = getNode("model", sbmlNode.getChildNodes());
      if (modelNode == null) {
        throw new InvalidInputDataExecption("No model node in SBML");
      }
      // we ignore metaid - it's useless and obstruct data model
      // model.setMetaId(getNodeAttr("metaId", modelNode));
      model.setIdModel(getNodeAttr("id", modelNode));

      Node compartmentNode = getNode("listOfCompartments", modelNode.getChildNodes());
      if (compartmentNode != null) {
        List<CellDesignerCompartment> compartments = compartmentCollectionXmlParser
            .parseXmlCompartmentCollection(compartmentNode);
        elements.addElements(compartments);
      }

      InternalModelSpeciesData modelData = new InternalModelSpeciesData();

      Node speciesNode = getNode("listOfSpecies", modelNode.getChildNodes());
      if (speciesNode != null) {
        List<Pair<String, ? extends CellDesignerSpecies<?>>> species = speciesSbmlParser
            .parseSbmlSpeciesCollection(speciesNode);
        modelData.updateSpecies(species);
      }
      Node reactionsNode = null;
      Node functionsNode = null;
      Node unitsNode = null;
      Node parametersNode = null;

      NodeList nodes = modelNode.getChildNodes();
      for (int x = 0; x < nodes.getLength(); x++) {
        Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("annotation")) {
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("listOfSpecies")) {
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("listOfReactions")) {
            reactionsNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("listOfCompartments")) {
            // we already parsed compartments
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("notes")) {
            String notes = rap.getNotes(node);
            if (notes != null) {
              notes = StringEscapeUtils.unescapeHtml4(notes);
            }
            model.setNotes(notes);
          } else if (node.getNodeName().equalsIgnoreCase("listOfUnitDefinitions")) {
            unitsNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("listOfFunctionDefinitions")) {
            functionsNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("listOfParameters")) {
            parametersNode = node;
          } else {
            throw new InvalidInputDataExecption("Unknown element of model: " + node.getNodeName());
          }
        }
      }

      if (unitsNode != null) {
        model.addUnits(unitParser.parseXmlUnitCollection(unitsNode));
      }

      if (parametersNode != null) {
        model.addParameters(parameterParser.parseXmlParameterCollection(parametersNode));
      }

      Node annotationNode = getNode("annotation", modelNode.getChildNodes());
      if (annotationNode == null) {
        throw new InvalidInputDataExecption("No annotation node in SBML/model");
      }

      parseAnnotation(model, annotationNode, modelData, elements);

      if (speciesNode != null) {
        List<Pair<String, ? extends CellDesignerSpecies<?>>> species = speciesSbmlParser
            .parseSbmlSpeciesCollection(speciesNode);
        modelData.updateSpecies(species);
      }
      if (functionsNode != null) {
        model.addFunctions(functionParser.parseXmlFunctionCollection(functionsNode));
      }

      if (reactionsNode != null) {
        ReactionCollectionXmlParser reactionCollectionXmlParser = new ReactionCollectionXmlParser(model, elements,
            params.isSbgnFormat());
        List<Reaction> reactions = reactionCollectionXmlParser.parseXmlReactionCollection(reactionsNode);
        model.addReactions(reactions);
      }

      if (params.isSizeAutoAdjust()) {
        Rectangle2D bound = getModelBound(model);
        double width = bound.getWidth() + 2 * (Math.max(0, bound.getX()));
        double height = bound.getHeight() + 2 * (Math.max(0, bound.getY()));

        model.setWidth(width);
        model.setHeight(height);
      }
    } catch (InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    } catch (CellDesignerParserException e) {
      throw new InvalidInputDataExecption(e);
    }

    return model;
  }

  /**
   * Computes bound of the model.
   * 
   * @param model
   *          object for which computaion is done
   * @return bound of the model
   */
  Rectangle2D getModelBound(Model model) {
    double maxX = 0;
    double maxY = 0;
    double minX = model.getWidth();
    double minY = model.getHeight();
    for (Element alias : model.getElements()) {
      maxX = Math.max(maxX, alias.getWidth() + alias.getX());
      maxY = Math.max(maxY, alias.getHeight() + alias.getY());

      minX = Math.min(minX, alias.getX());
      minY = Math.min(minY, alias.getY());

    }

    for (Reaction reaction : model.getReactions()) {
      for (Line2D line : reaction.getLines()) {
        maxX = Math.max(maxX, line.getX1());
        maxX = Math.max(maxX, line.getX2());
        maxY = Math.max(maxY, line.getY1());
        maxY = Math.max(maxY, line.getY2());

        minX = Math.min(minX, line.getX1());
        minX = Math.min(minX, line.getX2());
        minY = Math.min(minY, line.getY1());
        minY = Math.min(minY, line.getY2());
      }
    }

    for (Layer layer : model.getLayers()) {
      for (PolylineData lline : layer.getLines()) {
        for (Line2D line : lline.getLines()) {
          maxX = Math.max(maxX, line.getX1());
          maxX = Math.max(maxX, line.getX2());
          maxY = Math.max(maxY, line.getY1());
          maxY = Math.max(maxY, line.getY2());

          minX = Math.min(minX, line.getX1());
          minX = Math.min(minX, line.getX2());
          minY = Math.min(minY, line.getY1());
          minY = Math.min(minY, line.getY2());
        }
      }
      for (LayerRect rect : layer.getRectangles()) {
        maxX = Math.max(maxX, rect.getX());
        maxX = Math.max(maxX, rect.getX() + rect.getWidth());
        maxY = Math.max(maxY, rect.getY());
        maxY = Math.max(maxY, rect.getY() + rect.getHeight());

        minX = Math.min(minX, rect.getX());
        minX = Math.min(minX, rect.getX() + rect.getWidth());
        minY = Math.min(minY, rect.getY());
        minY = Math.min(minY, rect.getY() + rect.getHeight());
      }
      for (LayerOval oval : layer.getOvals()) {
        maxX = Math.max(maxX, oval.getX());
        maxX = Math.max(maxX, oval.getX() + oval.getWidth());
        maxY = Math.max(maxY, oval.getY());
        maxY = Math.max(maxY, oval.getY() + oval.getHeight());

        minX = Math.min(minX, oval.getX());
        minX = Math.min(minX, oval.getX() + oval.getWidth());
        minY = Math.min(minY, oval.getY());
        minY = Math.min(minY, oval.getY() + oval.getHeight());
      }
      for (LayerText text : layer.getTexts()) {
        maxX = Math.max(maxX, text.getX());
        maxX = Math.max(maxX, text.getX() + text.getWidth());
        maxY = Math.max(maxY, text.getY());
        maxY = Math.max(maxY, text.getY() + text.getHeight());

        minX = Math.min(minX, text.getX());
        minX = Math.min(minX, text.getX() + text.getWidth());
        minY = Math.min(minY, text.getY());
        minY = Math.min(minY, text.getY() + text.getHeight());
      }
    }

    Rectangle2D result = new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    return result;
  }

  /**
   * Parse annotation part of CellDesigner xml.
   * 
   * @param model
   *          model that is parsed (and will be updated)
   * @param modelData
   *          object containing information about species during CellDesigner
   *          parsing
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   * @param annotationNode
   *          xml node to parse
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  private void parseAnnotation(Model model, Node annotationNode, InternalModelSpeciesData modelData,
      CellDesignerElementCollection elements) throws InvalidXmlSchemaException {
    SpeciesCollectionXmlParser parser = new SpeciesCollectionXmlParser(elements);

    Node extensionNode = getNode("celldesigner:extension", annotationNode.getChildNodes());
    if (extensionNode == null) {
      throw new InvalidXmlSchemaException("No celldesigner:extension node in SBML/model/annotation");
    }

    NodeList nodes = extensionNode.getChildNodes();
    Node includedSpecies = null;
    Node listofSpeciesAlias = null;
    Node listOfComplexSpeciesAlias = null;
    Node listOfComparmentAlias = null;
    Node listOfProteins = null;
    Node listOfGenes = null;
    Node listOfRnas = null;
    Node listOfGroups = null;
    Node listOfAntisenseRnas = null;
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:modelVersion")) {
          // we ignore map version (there is no use for us)
          // model.setVersion(getNodeValue(node));
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:modelDisplay")) {
          model.setWidth(getNodeAttr("sizeX", node));
          model.setHeight(getNodeAttr("sizeY", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfSpeciesAliases")) {
          listofSpeciesAlias = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfComplexSpeciesAliases")) {
          listOfComplexSpeciesAlias = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfProteins")) {
          listOfProteins = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfGenes")) {
          listOfGenes = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfRNAs")) {
          listOfRnas = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfAntisenseRNAs")) {
          listOfAntisenseRnas = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfIncludedSpecies")) {
          includedSpecies = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfCompartmentAliases")) {
          listOfComparmentAlias = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfLayers")) {
          model.addLayers(layerParser.parseLayers(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfGroups")) {
          listOfGroups = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfBlockDiagrams")) {
          layerParser.parseBlocks(model, node);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of annotation: " + node.getNodeName());
        }
      }
    }

    if (listOfComparmentAlias != null) {
      List<Compartment> aliases = aliasCollectionParser.parseXmlCompartmentAliasCollection(listOfComparmentAlias);
      for (Element alias : aliases) {
        rap.processNotes(alias);
        model.addElement(alias);
      }
    }

    if (includedSpecies != null) {
      List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser
          .parseIncludedSpeciesCollection(includedSpecies);
      modelData.updateSpecies(species);
    }

    if (listOfProteins != null) {
      List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser.parseXmlProteinCollection(listOfProteins);
      modelData.updateSpecies(species);
    }
    if (listOfGenes != null) {
      List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser.parseXmlGeneCollection(listOfGenes);
      modelData.updateSpecies(species);
    }
    if (listOfRnas != null) {
      List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser.parseXmlRnaCollection(listOfRnas);
      modelData.updateSpecies(species);
    }
    if (listOfAntisenseRnas != null) {
      List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser
          .parseXmlAntisenseRnaCollection(listOfAntisenseRnas);
      modelData.updateSpecies(species);
    }

    for (CellDesignerSpecies<?> species : modelData.getAll()) {
      if (!species.getElementId().equals("")) {
        elements.addElement(species);
      } else {
        logger.warn("Species (class: " + species.getClass().getName() + ", name: " + species.getName()
            + ") exists in CD file, but is never instantiated. It's CellDesigner file problem.");
      }
    }

    if (listOfComplexSpeciesAlias != null) {
      List<Complex> aliases = aliasCollectionParser.parseXmlComplexAliasCollection(listOfComplexSpeciesAlias);
      for (Element alias : aliases) {
        rap.processNotes(alias);
        model.addElement(alias);
      }
    }

    if (listofSpeciesAlias != null) {
      List<Species> aliases = aliasCollectionParser.parseXmlSpeciesAliasCollection(listofSpeciesAlias);
      for (Element alias : aliases) {
        rap.processNotes(alias);
        model.addElement(alias);
      }
    }

    if (includedSpecies != null) {
      parseAnnotationAliasesConnections(model, getNode("celldesigner:listOfSpeciesAliases", nodes));
      parseAnnotationComplexAliasesConnections(model, getNode("celldesigner:listOfComplexSpeciesAliases", nodes));
    }

    if (listOfGroups != null) {
      layerParser.parseGroups(model, listOfGroups);
    }

  }

  /**
   * Parses celldesigner:listOfComplexSpeciesAliases node for annotation part of
   * the CellDEsigner format.
   * 
   * @param model
   *          model that is parsed
   * @param aliasNode
   *          node to parse
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  void parseAnnotationComplexAliasesConnections(Model model, Node aliasNode) throws InvalidXmlSchemaException {
    NodeList nodes = aliasNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:complexSpeciesAlias")) {
          String aliasId = getNodeAttr("id", node);
          String complexId = getNodeAttr("complexSpeciesAlias", node);
          Complex alias = model.getElementByElementId(aliasId);
          if (alias == null) {
            throw new InvalidXmlSchemaException("Alias does not exist " + aliasId);
          }
          Complex parentComplex = model.getElementByElementId(complexId);
          if (parentComplex != null) {
            parentComplex.addSpecies(alias);
            alias.setComplex(parentComplex);
          }
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfComplexSpeciesAliases: " + node.getNodeName());
        }
      }
    }

  }

  /**
   * Parses celldesigner:listOfSpeciesAliases node for annotation part of the
   * CellDesigner format.
   * 
   * @param model
   *          model that is parsed
   * @param aliasNode
   *          node to parse
   * @throws InvalidXmlSchemaException
   *           thrown when xml node contains data that is not supported by xml
   *           schema
   */
  void parseAnnotationAliasesConnections(Model model, Node aliasNode) throws InvalidXmlSchemaException {
    NodeList nodes = aliasNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:speciesAlias")) {
          String aliasId = getNodeAttr("id", node);
          String complexId = getNodeAttr("complexSpeciesAlias", node);
          Element alias = model.getElementByElementId(aliasId);
          if (alias == null) {
            throw new InvalidXmlSchemaException("Alias does not exist " + aliasId);
          } else if (alias instanceof Species) {
            Species species = ((Species) alias);
            Complex complex = model.getElementByElementId(complexId);
            if (complex != null) {
              species.setComplex(complex);
              complex.addSpecies(species);
            }
          }
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfSpeciesAliases: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Transforms model into CellDesigner xml.
   * 
   * @param model
   *          model that should be transformed
   * @return CellDesigner xml string for the model
   * @throws InconsistentModelException
   *           thrown when then model is invalid
   * @throws InconsistentModelException
   */
  public String toXml(Model model) throws InconsistentModelException {
    EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
    try {
      Logger.getRootLogger().addAppender(appender);
      CellDesignerElementCollection elements = new CellDesignerElementCollection();

      SpeciesCollectionXmlParser speciesCollectionXmlParser = new SpeciesCollectionXmlParser(elements);
      ReactionCollectionXmlParser reactionCollectionXmlParser = new ReactionCollectionXmlParser(model, elements, false);
      UnitCollectionXmlParser unitCollectionXmlParser = new UnitCollectionXmlParser();
      FunctionCollectionXmlParser functionCollectionXmlParser = new FunctionCollectionXmlParser();
      ParameterCollectionXmlParser parameterCollectionXmlParser = new ParameterCollectionXmlParser(model);

      aliasCollectionParser = new AliasCollectionXmlParser(elements, model);

      StringBuilder result = new StringBuilder();
      result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
      result.append("<sbml xmlns=\"http://www.sbml.org/sbml/level2/version4\" "
          + "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" level=\"2\" version=\"4\">\n");
      // metaid is a string cell designer id, usually it's model id and as far as
      // we can tell it's not used at all
      result.append("<model metaid=\"" + model.getIdModel() + "\" id=\"" + model.getIdModel() + "\">\n");

      result.append(unitCollectionXmlParser.toXml(model.getUnits()));
      result.append(functionCollectionXmlParser.toXml(model.getFunctions()));
      result.append(parameterCollectionXmlParser.toXml(model.getParameters()));
      result.append(annotationToXml(model, elements));

      compartmentCollectionXmlParser = new CompartmentCollectionXmlParser(elements);

      result.append(compartmentCollectionXmlParser.toXml(model.getCompartments()));

      result.append(speciesCollectionXmlParser.speciesCollectionToSbmlString(model.getSpeciesList()));

      result.append(reactionCollectionXmlParser.reactionCollectionToXmlString(model.getReactions()));
      if (model.getNotes() != null || !appender.getWarnings().isEmpty()) {
        result.append("<notes>");
        if (model.getNotes() != null) {
          result.append(escapeXml(model.getNotes()));
        }
        for (LoggingEvent event : appender.getWarnings()) {
          if (event.getMessage() instanceof String) {
            result.append("\n" + ((String) event.getMessage()));
          }
        }
        result.append("</notes>");
      }

      result.append("</model>");
      result.append("</sbml>");
      return result.toString();
    } finally {
      Logger.getRootLogger().removeAppender(appender);
    }
  }

  /**
   * Generates xml node that should be in annotation part of the model.
   * 
   * @param model
   *          model to transform
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   * @return annotation xml string for the model
   */
  private String annotationToXml(Model model, CellDesignerElementCollection elements) {
    SpeciesCollectionXmlParser speciesCollectionXmlParser = new SpeciesCollectionXmlParser(elements);

    StringBuilder result = new StringBuilder();
    result.append("<annotation>\n");
    result.append("<celldesigner:extension>\n");
    result.append("<celldesigner:modelVersion>4.0</celldesigner:modelVersion>\n");
    result.append("<celldesigner:modelDisplay sizeX=\"" + model.getWidth().intValue() + "\" sizeY=\""
        + model.getHeight().intValue() + "\"/>\n");

    result.append(speciesCollectionXmlParser.speciesCollectionToXmlIncludedString(model.getSpeciesList()));

    result.append(aliasCollectionParser.compartmentAliasCollectionToXmlString(model.getCompartments()));
    result.append(aliasCollectionParser.complexAliasCollectionToXmlString(model.getComplexList()));
    result.append(aliasCollectionParser.speciesAliasCollectionToXmlString(model.getNotComplexSpeciesList()));

    List<Protein> proteins = new ArrayList<>();
    List<Gene> genes = new ArrayList<>();
    List<Rna> rnas = new ArrayList<>();
    List<AntisenseRna> antisenseRnas = new ArrayList<>();
    for (Element element : model.getElements()) {
      if (element instanceof Protein) {
        proteins.add((Protein) element);
      } else if (element instanceof Gene) {
        genes.add((Gene) element);
      } else if (element instanceof AntisenseRna) {
        antisenseRnas.add((AntisenseRna) element);
      } else if (element instanceof Rna) {
        rnas.add((Rna) element);
      }
    }

    result.append(speciesCollectionXmlParser.proteinCollectionToXmlString(proteins));
    result.append(speciesCollectionXmlParser.geneCollectionToXmlString(genes));
    result.append(speciesCollectionXmlParser.rnaCollectionToXmlString(rnas));
    result.append(speciesCollectionXmlParser.antisenseRnaCollectionToXmlString(antisenseRnas));
    result.append(layerParser.layerCollectionToXml(model.getLayers()));

    result.append("</celldesigner:extension>\n");
    result.append("</annotation>\n");
    return result.toString();
  }

  @Override
  public InputStream exportModelToInputStream(Model model) throws InconsistentModelException {
    String exportedString = toXml(model);
    InputStream inputStream = new ByteArrayInputStream(exportedString.getBytes());
    return inputStream;
  }

  @Override
  public File exportModelToFile(Model model, String filePath) throws InconsistentModelException, IOException {
    File file = new File(filePath);
    String exportedString = toXml(model);
    FileWriter fileWriter = new FileWriter(file);
    fileWriter.write(exportedString);
    fileWriter.flush();
    fileWriter.close();

    return file;
  }

  @Override
  public String getCommonName() {
    return "CellDesigner SBML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.SBML;
  }

  @Override
  public String getFileExtension() {
    return "xml";
  }
}
