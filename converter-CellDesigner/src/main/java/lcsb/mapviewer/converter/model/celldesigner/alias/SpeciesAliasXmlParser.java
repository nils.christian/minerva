package lcsb.mapviewer.converter.model.celldesigner.alias;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Parser of CellDesigner xml used for parsing SpeciesAlias.
 * 
 * @author Piotr Gawron
 * 
 * @see Complex
 */
public class SpeciesAliasXmlParser extends AbstractAliasXmlParser<Species> {
  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(SpeciesAliasXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Model for which we parse elements.
   */
  private Model model;

  /**
   * Default constructor with model object for which we parse data.
   * 
   * @param model
   *          model for which we parse elements
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public SpeciesAliasXmlParser(CellDesignerElementCollection elements, Model model) {
    this.elements = elements;
    this.model = model;
  }

  @Override
  Species parseXmlAlias(Node aliasNode) throws InvalidXmlSchemaException {

    String speciesId = getNodeAttr("species", aliasNode);
    String aliasId = getNodeAttr("id", aliasNode);
    CellDesignerSpecies<?> species = elements.getElementByElementId(speciesId);
    if (species == null) {
      throw new InvalidXmlSchemaException("Unknown species for alias (speciesId: " + speciesId + ")");
    }
    if (species instanceof CellDesignerComplexSpecies) {
      logger.warn("[" + speciesId + "," + aliasId
          + "]\tSpecies is defined as a complex, but alias is not a complex. Changing alias to complex.");
    }

    elements.addElement(species, aliasId);
    Species result = species.createModelElement(aliasId);

    String state = "usual";
    NodeList nodes = aliasNode.getChildNodes();
    View usualView = null;
    View briefView = null;
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:activity")) {
          result.setActivity(getNodeValue(node).equalsIgnoreCase("active"));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(getNodeAttr("X", node));
          result.setY(getNodeAttr("Y", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
          result.setFontSize(getNodeAttr("size", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:view")) {
          state = getNodeAttr("state", node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:usualView")) {
          usualView = getCommonParser().getView(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:briefView")) {
          briefView = getCommonParser().getView(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
          processAliasState(node, result);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
          // not handled
          continue;
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:speciesAlias: " + node.getNodeName());
        }
      }
    }

    View view = null;
    if (state.equalsIgnoreCase("usual")) {
      view = usualView;
    } else if (state.equalsIgnoreCase("brief")) {
      view = briefView;
    }

    if (view != null) {
      // inner position defines the position in compartment
      // result.moveBy(view.innerPosition);
      result.setWidth(view.getBoxSize().width);
      result.setHeight(view.getBoxSize().height);
      result.setLineWidth(view.getSingleLine().getWidth());
      result.setColor(view.getColor());
    } else {
      throw new InvalidXmlSchemaException("No view in Alias");
    }
    result.setState(state);
    String compartmentAliasId = getNodeAttr("compartmentAlias", aliasNode);
    if (!compartmentAliasId.isEmpty()) {
      Compartment compartment = model.getElementByElementId(compartmentAliasId);
      if (compartment == null) {
        throw new InvalidXmlSchemaException("CompartmentAlias does not exist: " + compartmentAliasId);
      } else {
        result.setCompartment(compartment);
        compartment.addElement(result);
      }
    }
    String complexAliasId = getNodeAttr("complexSpeciesAlias", aliasNode);
    if (!complexAliasId.isEmpty()) {
      Complex complex = model.getElementByElementId(complexAliasId);
      if (complex == null) {
        throw new InvalidXmlSchemaException(
            "ComplexAlias does not exist: " + complexAliasId + ", current: " + result.getElementId());
      } else {
        result.setComplex(complex);
        complex.addSpecies(result);
      }
    }
    

    species.updateModelElementAfterLayoutAdded(result);
    return result;
  }

  /**
   * Process node with information about alias state and puts data into alias.
   * 
   * @param node
   *          node where information about alias state is stored
   * @param alias
   *          alias object to be modified if necessary
   */
  private void processAliasState(Node node, Species alias) {
    String state = getNodeAttr("state", node);
    if ("open".equalsIgnoreCase(state)) {
      String prefix = getNodeAttr("prefix", node);
      String label = getNodeAttr("label", node);
      alias.setStatePrefix(prefix);
      alias.setStateLabel(label);
    } else if ("empty".equalsIgnoreCase(state)) {
      return;
    } else if (state == null || state.isEmpty()) {
      return;
    } else {
      throw new NotImplementedException("[Alias: " + alias.getElementId() + "] Unkown alias state: " + state);
    }

  }

  @Override
  public String toXml(Species species) {
    Compartment ca = null;
    // artificial compartment aliases should be excluded
    if (species.getCompartment() != null && !(species.getCompartment() instanceof PathwayCompartment)) {
      ca = (Compartment) species.getCompartment();
    } else if (species.getComplex() == null) {
      ModelData model = species.getModelData();
      if (model != null) {
        for (Compartment cAlias : model.getModel().getCompartments()) {
          if (!(cAlias instanceof PathwayCompartment) && cAlias.cross(species)) {
            if (ca == null) {
              ca = cAlias;
            } else if (ca.getSize() > cAlias.getSize()) {
              ca = cAlias;
            }
          }
        }
      }
    }

    Complex complex = species.getComplex();

    String compartmentAliasId = null;
    if (ca != null) {
      compartmentAliasId = ca.getElementId();
    }
    StringBuilder sb = new StringBuilder("");
    sb.append("<celldesigner:speciesAlias ");
    sb.append("id=\"" + species.getElementId() + "\" ");
    sb.append("species=\"" + elements.getElementId(species) + "\" ");
    if (compartmentAliasId != null) {
      sb.append("compartmentAlias=\"" + compartmentAliasId + "\" ");
    }

    if (complex != null) {
      sb.append("complexSpeciesAlias=\"" + complex.getElementId() + "\" ");
    }
    sb.append(">\n");

    if (species.getActivity() != null) {
      if (species.getActivity()) {
        sb.append("<celldesigner:activity>active</celldesigner:activity>\n");
      } else {
        sb.append("<celldesigner:activity>inactive</celldesigner:activity>\n");
      }
    }

    sb.append("<celldesigner:bounds ");
    sb.append("x=\"" + species.getX() + "\" ");
    sb.append("y=\"" + species.getY() + "\" ");
    sb.append("w=\"" + species.getWidth() + "\" ");
    sb.append("h=\"" + species.getHeight() + "\" ");
    sb.append("/>\n");

    sb.append("<celldesigner:font size=\"" + species.getFontSize() + "\"/>\n");

    // TODO to be improved
    sb.append("<celldesigner:view state=\"usual\"/>\n");
    sb.append("<celldesigner:usualView>");
    sb.append("<celldesigner:innerPosition x=\"" + species.getX() + "\" y=\"" + species.getY() + "\"/>");
    sb.append("<celldesigner:boxSize width=\"" + species.getWidth() + "\" height=\"" + species.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + species.getLineWidth() + "\"/>");
    sb.append("<celldesigner:paint color=\"" + colorToString(species.getColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:usualView>\n");
    sb.append("<celldesigner:briefView>");
    sb.append("<celldesigner:innerPosition x=\"" + species.getX() + "\" y=\"" + species.getY() + "\"/>");
    sb.append("<celldesigner:boxSize width=\"" + species.getWidth() + "\" height=\"" + species.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + species.getLineWidth() + "\"/>");
    sb.append("<celldesigner:paint color=\"" + colorToString(species.getColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:briefView>\n");
    if (species.getStateLabel() != null || species.getStatePrefix() != null) {
      sb.append("<celldesigner:info state=\"open\" prefix=\"" + species.getStatePrefix() + "\" label=\""
          + species.getStateLabel() + "\"/>\n");
    }
    sb.append("</celldesigner:speciesAlias>\n");
    return sb.toString();
  }
}
