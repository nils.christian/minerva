package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;

import lcsb.mapviewer.common.geometry.PointTransformation;

import org.apache.log4j.Logger;

/**
 * Class for basic point transformations.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerPointTransformation extends PointTransformation {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger	= Logger.getLogger(CellDesignerPointTransformation.class.getName());

	/**
	 * This method transform coordinates of pointP in celldesginer format (base:
	 * pointA, pointB, pointC) into normal x,y coordinates.
	 * 
	 * In CellDesigner some points are in different base consisted from three
	 * points. In this base vector between pointA and pointC is 1 unit on X axis
	 * (in normal world); and vector between pointB and pointC is 1 unit on Y
	 * axis.
	 * 
	 * @param pointA
	 *          central point of CellDesigner base
	 * @param pointB
	 *          first point of CellDesigner base
	 * @param pointC
	 *          second point of CellDesigner base
	 * @param pointP
	 *          point to be transformed into normal coordinates
	 * @return standard x,y coordinate
	 */
	public Point2D getCoordinatesInNormalBase(final Point2D pointA, final Point2D pointB, final Point2D pointC, final Point2D pointP) {
		double dx1 = pointA.getX() - pointC.getX();
		double dy1 = pointA.getY() - pointC.getY();

		double dx2 = pointB.getX() - pointC.getX();
		double dy2 = pointB.getY() - pointC.getY();

		double x = pointC.getX() + dx1 * pointP.getX() + dx2 * pointP.getY();
		double y = pointC.getY() + dy1 * pointP.getX() + dy2 * pointP.getY();

		Point2D pointO = new Point2D.Double(x, y);

		return pointO;
	}

	/**
	 * This method transform coordinates of pointP in x,y coordinates into
	 * celldesginer format (base: pointA, pointB, pointC)
	 * 
	 * In CellDesigner some points are in different base consisted from three
	 * points. In this base vector between pointA and pointC is 1 unit on X axis
	 * (in normal world); and vector between pointB and pointC is 1 unit on Y
	 * axis.
	 * 
	 * @param pointA
	 *          central point of CellDesigner base
	 * @param pointB
	 *          first point of CellDesigner base
	 * @param pointC
	 *          second point of CellDesigner base
	 * @param pointO
	 *          point to be transformed
	 * @return point in CellDesigner base
	 */
	public Point2D getCoordinatesInCellDesignerBase(final Point2D pointA, final Point2D pointB, final Point2D pointC, final Point2D pointO) {
		double dx1 = pointA.getX() - pointC.getX();
		double dy1 = pointA.getY() - pointC.getY();

		double dx2 = pointB.getX() - pointC.getX();
		double dy2 = pointB.getY() - pointC.getY();

		double y = (dy1 * (pointO.getX() - pointC.getX()) + dx1 * (pointC.getY() - pointO.getY())) / (dx2 * dy1 - dx1 * dy2);
		double x = (dy2 * (pointC.getX() - pointO.getX()) + dx2 * (pointO.getY() - pointC.getY())) / (dx2 * dy1 - dx1 * dy2);

		Point2D pointP = new Point2D.Double(x, y);

		return pointP;
	}

}
