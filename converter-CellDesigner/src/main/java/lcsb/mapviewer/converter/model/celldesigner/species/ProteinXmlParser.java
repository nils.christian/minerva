package lcsb.mapviewer.converter.model.celldesigner.species;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerProtein} object.
 * 
 * @author Piotr Gawron
 * 
 */

public class ProteinXmlParser extends AbstractElementXmlParser<CellDesignerProtein<?>, Protein> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ProteinXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public ProteinXmlParser(CellDesignerElementCollection elements) {
    this.elements = elements;
  }

  @Override
  public Pair<String, CellDesignerProtein<?>> parseXmlElement(Node proteinNode) throws InvalidXmlSchemaException {
    CellDesignerProtein<?> protein = null;
    String value = getNodeAttr("type", proteinNode);
    ProteinMapping mapping = ProteinMapping.getMappingByString(value);
    if (mapping != null) {
      protein = mapping.createProtein();
    } else {
      throw new InvalidXmlSchemaException("Protein node in Sbml model is of unknown type: " + value);
    }

    String identifier = getNodeAttr("id", proteinNode);
    protein.setName(decodeName(getNodeAttr("name", proteinNode)));
    NodeList list = proteinNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:listOfModificationResidues")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:modificationResidue")) {
                protein.addModificationResidue(getModificationResidue(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModificationResidues " + residueNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfBindingRegions")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:bindingRegion")) {
                protein.addModificationResidue(getBindingRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModificationResidues " + residueNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:notes")) {
          protein.setNotes(getRap().getNotes(node));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:protein " + node.getNodeName());
        }
      }
    }

    return new Pair<String, CellDesignerProtein<?>>(identifier, protein);
  }

  @Override
  public String toXml(Protein protein) {
    String attributes = "";
    String result = "";
    attributes += " id=\"p_" + elements.getElementId(protein) + "\"";
    if (protein.getName() != null && !protein.getName().equals("")) {
      attributes += " name=\"" + escapeXml(encodeName(protein.getName())) + "\"";
    }
    String type = null;
    ProteinMapping mapping = ProteinMapping.getMappingByClass(protein.getClass());
    if (mapping != null) {
      type = mapping.getCellDesignerString();
    } else {
      throw new InvalidArgumentException("Invalid protein class type: " + protein.getClass().getName());
    }
    attributes += " type=\"" + type + "\"";
    result += "<celldesigner:protein" + attributes + ">\n";

    List<Residue> residues = new ArrayList<>();
    List<BindingRegion> bindingRegions = new ArrayList<>();
    for (ModificationResidue mr : protein.getModificationResidues()) {
      if (mr instanceof Residue) {
        residues.add((Residue) mr);
      } else if (mr instanceof BindingRegion) {
        bindingRegions.add((BindingRegion) mr);
      } else {
        throw new InvalidArgumentException("Don't know how to handle: " + mr.getClass());
      }
    }
    if (residues.size() > 0) {
      result += "<celldesigner:listOfModificationResidues>";
      for (Residue mr : residues) {
        result += toXml(mr);
      }
      result += "</celldesigner:listOfModificationResidues>\n";
    }
    if (bindingRegions.size() > 0) {
      result += "<celldesigner:listOfBindingRegions>";
      for (BindingRegion mr : bindingRegions) {
        result += toXml(mr);
      }
      result += "</celldesigner:listOfBindingRegions>\n";
    }
    // ignore notes - all notes will be stored in species
    // result +=
    // "<celldesigner:notes>"+protein.getNotes()+"</celldesigner:notes>";
    result += "</celldesigner:protein>\n";
    return result;
  }

  /**
   * Generates CellDesigner xml for {@link Residue}.
   * 
   * @param mr
   *          object to transform into xml
   * @return CellDesigner xml for {@link Residue}
   */
  private String toXml(Residue mr) {
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(mr.getSpecies(), false);

    String result = "";
    String attributes = "";
    if (!mr.getIdModificationResidue().equals("")) {
      attributes += " id=\"" + mr.getIdModificationResidue() + "\"";
    }
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + escapeXml(mr.getName()) + "\"";
    }
    attributes += " angle=\"" + converter.getAngleForPoint(mr.getSpecies(), mr.getPosition()) + "\"";
    result += "<celldesigner:modificationResidue " + attributes + ">";
    result += "</celldesigner:modificationResidue>";

    return result;
  }

  /**
   * Generates CellDesigner xml for {@link BindingRegion}.
   * 
   * @param mr
   *          object to transform into xml
   * @return CellDesigner xml for {@link BindingRegion}
   */
  private String toXml(BindingRegion mr) {

    CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);
    String result = "";
    String attributes = "";
    if (!mr.getIdModificationResidue().equals("")) {
      attributes += " id=\"" + mr.getIdModificationResidue() + "\"";
    }
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + escapeXml(mr.getName()) + "\"";
    }
    attributes += " angle=\"" + cellDesignerModificationResidue.getAngle() + "\"";
    attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    result += "<celldesigner:bindingRegion " + attributes + ">";
    result += "</celldesigner:bindingRegion>";

    return result;
  }

  /**
   * Parses CellDesigner xml node for ModificationResidue.
   * 
   * @param residueNode
   *          xml node to parse
   * @return {@link CellDesignerModificationResidue} object created from the node
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getModificationResidue(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(getNodeAttr("id", residueNode));
    residue.setName(getNodeAttr("name", residueNode));
    residue.setSide(getNodeAttr("side", residueNode));
    residue.setAngle(getNodeAttr("angle", residueNode));
    residue.setModificationType(ModificationType.RESIDUE);
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "Unknown element of celldesigner:modificationResidue " + node.getNodeName());
      }
    }
    return residue;
  }

  /**
   * Parses CellDesigner xml node for ModificationResidue.
   * 
   * @param residueNode
   *          xml node to parse
   * @return {@link CellDesignerModificationResidue} object created from the node
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getBindingRegion(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(getNodeAttr("id", residueNode));
    residue.setName(getNodeAttr("name", residueNode));
    residue.setSize(getNodeAttr("size", residueNode));
    residue.setAngle(getNodeAttr("angle", residueNode));
    residue.setModificationType(ModificationType.BINDING_REGION);
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "Unknown element of celldesigner:modificationResidue " + node.getNodeName());
      }
    }
    return residue;
  }

}
