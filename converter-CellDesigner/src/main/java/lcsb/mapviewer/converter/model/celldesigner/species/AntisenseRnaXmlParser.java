package lcsb.mapviewer.converter.model.celldesigner.species;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;

/**
 * Class that performs parsing of the CellDesigner xml for AntisenseRna object.
 * 
 * @author Piotr Gawron
 * 
 */
public class AntisenseRnaXmlParser extends AbstractElementXmlParser<CellDesignerAntisenseRna, AntisenseRna> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(AntisenseRnaXmlParser.class.getName());

  /**
   * List of {@link CellDesignerElement celldesigner elements} obtained during
   * parsing process.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor.
   * 
   * @param elements
   *          list of {@link CellDesignerElement celldesigner elements} obtained
   *          during parsing process
   */
  public AntisenseRnaXmlParser(CellDesignerElementCollection elements) {
    this.elements = elements;
  }

  @Override
  public Pair<String, CellDesignerAntisenseRna> parseXmlElement(Node antisenseRnaNode)
      throws InvalidXmlSchemaException {
    CellDesignerAntisenseRna antisenseRna = new CellDesignerAntisenseRna();
    String identifier = getNodeAttr("id", antisenseRnaNode);
    antisenseRna.setName(decodeName(getNodeAttr("name", antisenseRnaNode)));
    NodeList list = antisenseRnaNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          antisenseRna.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                antisenseRna.addRegion(getAntisenseRnaRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:antisenseRna " + node.getNodeName());
        }
      }
    }
    Pair<String, CellDesignerAntisenseRna> result = new Pair<String, CellDesignerAntisenseRna>(identifier,
        antisenseRna);
    return result;
  }

  @Override
  public String toXml(AntisenseRna antisenseRna) {
    String attributes = "";
    String result = "";
    attributes += " id=\"ar_" + elements.getElementId(antisenseRna) + "\"";
    if (!antisenseRna.getName().equals("")) {
      attributes += " name=\"" + escapeXml(encodeName(antisenseRna.getName())) + "\"";
    }
    result += "<celldesigner:AntisenseRNA" + attributes + ">";
    result += "<celldesigner:notes>";
    result += "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>";

    RestAnnotationParser rap = new RestAnnotationParser();
    result += rap.createAnnotationString(antisenseRna);
    result += antisenseRna.getNotes();
    result += "</body></html>";
    result += "</celldesigner:notes>";
    if (antisenseRna.getRegions().size() > 0) {
      result += "<celldesigner:listOfRegions>";
      for (ModificationResidue region : antisenseRna.getRegions()) {
        result += toXml(region);
      }
      result += "</celldesigner:listOfRegions>";
    }
    result += "</celldesigner:AntisenseRNA>";
    return result;
  }

  /**
   * Transforms AntisenseRnaRegion into CellDEsigner xml representation.
   * 
   * @param region
   *          object to be transformed
   * @return xml representation of the given region
   */
  private String toXml(ModificationResidue region) {
    CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(region);

    String result = "";
    String attributes = "";
    if (!region.getIdModificationResidue().equals("")) {
      attributes += " id=\"" + region.getIdModificationResidue() + "\"";
    }
    if (!region.getName().equals("")) {
      attributes += " name=\"" + escapeXml(region.getName()) + "\"";
    }
    String type = null;
    if (region instanceof CodingRegion) {
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
      type = "CodingRegion";
    } else if (region instanceof ModificationSite) {
      type = "Modification Site";
    } else if (region instanceof ProteinBindingDomain) {
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
      type = "proteinBindingDomain";
    } else {
      throw new InvalidArgumentException("Unknown modificatin type: " + region.getClass());
    }

    attributes += " pos=\"" + cellDesignerModificationResidue.getPos() + "\"";
    attributes += " type=\"" + type + "\"";
    result += "<celldesigner:region " + attributes + ">";
    result += "</celldesigner:region>";

    return result;
  }

  /**
   * Method that parse xml node into AntisenseRnaRegion element.
   * 
   * @param regionNode
   *          xml node to parse
   * @return AntisenseRnaRegion object from xml node
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getAntisenseRnaRegion(Node regionNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(getNodeAttr("id", regionNode));
    residue.setName(getNodeAttr("name", regionNode));
    residue.setSize(getNodeAttr("size", regionNode));
    residue.setPos(getNodeAttr("pos", regionNode));
    String typeString = getNodeAttr("type", regionNode);
    if (typeString != null) {
      try {
        residue.setModificationType(ModificationType.getByCellDesignerName(typeString));
      } catch (InvalidArgumentException e) {
        throw new InvalidXmlSchemaException(e);
      }
    }

    NodeList list = regionNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

}
