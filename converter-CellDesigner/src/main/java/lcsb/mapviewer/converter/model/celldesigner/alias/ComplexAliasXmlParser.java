package lcsb.mapviewer.converter.model.celldesigner.alias;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Parser of CellDesigner xml used for parsing complex aliases. Important: Only
 * one instance per model should be used.
 * 
 * @author Piotr Gawron
 * 
 * @see Complex
 */
public class ComplexAliasXmlParser extends AbstractAliasXmlParser<Complex> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger												logger								= Logger.getLogger(ComplexAliasXmlParser.class.getName());

	/**
	 * Because of the CellDesigner xml model we have to store information about
	 * all processed Complexes. This infomrmation later on is used for connecting
	 * complexes in hierarchical view.
	 */
	private Map<String, Complex>					complexAliasesMapById	= new HashMap<String, Complex>();

	/**
	 * Because of the CellDesigner xml model we have to store information about
	 * parents of all Complexes. This infomrmation later on is used for connecting
	 * complexes in hierarchical view. We cannot do it immediatelly because some
	 * complexes doesn't exist yet.
	 */
	private Map<String, String>						parents								= new HashMap<String, String>();

	/**
	 * Model for which we are parsing aliases.
	 */
	private Model													model									= null;

	/**
	 * Collection of {@link CellDesignerElement cell designer elements} parsed
	 * from xml.
	 */
	private CellDesignerElementCollection	elements;

	/**
	 * Default constructor with model object for which we parse data.
	 * 
	 * @param model
	 *          model for which we parse elements
	 * @param elements
	 *          collection of {@link CellDesignerElement cell designer elements}
	 *          parsed from xml
	 */
	public ComplexAliasXmlParser(CellDesignerElementCollection elements, Model model) {
		this.model = model;
		this.elements = elements;
	}

	@Override
	Complex parseXmlAlias(Node aliasNode) throws InvalidXmlSchemaException {

		String aliasId = getNodeAttr("id", aliasNode);

		String speciesId = getNodeAttr("species", aliasNode);
		CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) elements.getElementByElementId(speciesId);
		if (species == null) {
			throw new InvalidXmlSchemaException("No species with id=\"" + speciesId + "\" for complex alias \"" + aliasId + "\"");
		}
		Complex result = species.createModelElement(aliasId);
	    elements.addElement(species, aliasId);

		String state = "usual";
		NodeList nodes = aliasNode.getChildNodes();
		View usualView = null;
		View briefView = null;
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("celldesigner:activity")) {
					result.setActivity(getNodeValue(node).equalsIgnoreCase("active"));
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
					result.setX(getNodeAttr("X", node));
					result.setY(getNodeAttr("Y", node));
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
					result.setFontSize(getNodeAttr("size", node));
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:view")) {
					state = getNodeAttr("state", node);
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:usualView")) {
					usualView = getCommonParser().getView(node);
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:briefView")) {
					briefView = getCommonParser().getView(node);
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:backupView")) {
					// not handled
					continue;
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
					processAliasState(node, result);
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:backupSize")) {
					// not handled
					continue;
				} else if (node.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
					// not handled
					continue;
				} else {
					throw new InvalidXmlSchemaException("Unknown element of celldesigner:speciesAlias: " + node.getNodeName());
				}
			}
		}

		View view = null;
		if (state.equalsIgnoreCase("usual")) {
			view = usualView;
		} else if (state.equalsIgnoreCase("brief")) {
			view = briefView;
		} else if (state.equalsIgnoreCase("complexparentbrief")) {
			view = briefView;
		}

		if (view != null) {
			// inner position defines the position in compartment or complexAlias
			// result.moveBy(view.innerPosition);
			result.setWidth(view.getBoxSize().width);
			result.setHeight(view.getBoxSize().height);
			result.setLineWidth(view.getSingleLine().getWidth());
			result.setColor(view.getColor());
		} else if (!state.equalsIgnoreCase("complexnoborder")) {
			throw new InvalidXmlSchemaException("No view (" + state + ") in ComplexAlias for " + result.getElementId());
		}
		result.setState(state);
		String compartmentAliasId = getNodeAttr("compartmentAlias", aliasNode);
		if (!compartmentAliasId.isEmpty()) {
			Compartment ca = model.getElementByElementId(compartmentAliasId);
			if (ca == null) {
				throw new InvalidXmlSchemaException("CompartmentAlias does not exist: " + compartmentAliasId);
			} else {
				result.setCompartment(ca);
				ca.addElement(result);
			}
		}
		String complexSpeciesAliasId = getNodeAttr("complexSpeciesAlias", aliasNode);
		if (!complexSpeciesAliasId.equals("")) {
			parents.put(result.getElementId(), complexSpeciesAliasId);
		}
		complexAliasesMapById.put(result.getElementId(), result);
		return result;
	}

	/**
	 * Process node with information about alias state and puts data into alias.
	 * 
	 * @param node
	 *          node where information about alias state is stored
	 * @param alias
	 *          alias object to be modified if necessary
	 */
	private void processAliasState(Node node, Species alias) {
		String state = getNodeAttr("state", node);
		if ("open".equalsIgnoreCase(state)) {
			String prefix = getNodeAttr("prefix", node);
			String label = getNodeAttr("label", node);
			alias.setStatePrefix(prefix);
			alias.setStateLabel(label);
		} else if ("empty".equalsIgnoreCase(state)) {
			return;
		} else if (state == null || state.isEmpty()) {
			return;
		} else {
			throw new NotImplementedException("[Alias: " + alias.getElementId() + "] Unkown alias state: " + state);
		}

	}

	/**
	 * Adds parent reference for the complexAlias.
	 * 
	 * @param alias
	 *          alias for which we want to add parent information
	 */
	public void addReference(Complex alias) {
		String parentId = parents.get(alias.getElementId());
		if (parentId != null) {
			Complex ca = complexAliasesMapById.get(parentId);
			if (ca == null) {
				throw new InvalidArgumentException("Parent complex alias does not exist: " + parentId + " for alias: " + alias.getElementId());
			} else {
				alias.setComplex(ca);
				ca.addSpecies(alias);
			}
		}
	}

	@Override
	public String toXml(Complex alias) {
		Compartment ca = null;
		// we have to exclude artifitial compartment aliases, becuase they aren't
		// exported to CellDesigner file
		if (alias.getCompartment() != null && !(alias.getCompartment() instanceof PathwayCompartment)) {
			ca = alias.getCompartment();
		} else if (alias.getComplex() == null) {
			ModelData modelData = alias.getModelData();
			if (modelData != null) {
				for (Compartment cAlias : modelData.getModel().getCompartments()) {
					if (!(cAlias instanceof PathwayCompartment) && cAlias.cross(alias)) {
						if (ca == null) {
							ca = cAlias;
						} else if (ca.getSize() > cAlias.getSize()) {
							ca = cAlias;
						}
					}
				}
			}
		}

		Complex complexAlias = alias.getComplex();

		String compartmentAliasId = null;
		if (ca != null) {
			compartmentAliasId = ca.getElementId();
		}
		StringBuilder sb = new StringBuilder("");
		sb.append("<celldesigner:complexSpeciesAlias ");
		sb.append("id=\"" + alias.getElementId() + "\" ");
		sb.append("species=\"" + elements.getElementId(alias) + "\" ");
		if (compartmentAliasId != null) {
			sb.append("compartmentAlias=\"" + compartmentAliasId + "\" ");
		}
		if (complexAlias != null) {
			sb.append("complexSpeciesAlias=\"" + complexAlias.getElementId() + "\"");
		}
		sb.append(">\n");

		if (alias.getActivity() != null) {
			if (alias.getActivity()) {
				sb.append("<celldesigner:activity>active</celldesigner:activity>\n");
			} else {
				sb.append("<celldesigner:activity>inactive</celldesigner:activity>\n");
			}
		}

		sb.append("<celldesigner:bounds ");
		sb.append("x=\"" + alias.getX() + "\" ");
		sb.append("y=\"" + alias.getY() + "\" ");
		sb.append("w=\"" + alias.getWidth() + "\" ");
		sb.append("h=\"" + alias.getHeight() + "\" ");
		sb.append("/>\n");

		sb.append("<celldesigner:font size=\"" + alias.getFontSize() + "\"/>");

		sb.append("<celldesigner:view state=\"usual\"/>\n");
		sb.append("<celldesigner:usualView>");
		sb.append("<celldesigner:innerPosition x=\"" + alias.getX() + "\" y=\"" + alias.getY() + "\"/>");
		sb.append("<celldesigner:boxSize width=\"" + alias.getWidth() + "\" height=\"" + alias.getHeight() + "\"/>");
		sb.append("<celldesigner:singleLine width=\"" + alias.getLineWidth() + "\"/>");
		sb.append("<celldesigner:paint color=\"" + colorToString(alias.getColor()) + "\" scheme=\"Color\"/>");
		sb.append("</celldesigner:usualView>\n");
		sb.append("<celldesigner:briefView>");
		sb.append("<celldesigner:innerPosition x=\"" + alias.getX() + "\" y=\"" + alias.getY() + "\"/>");
		sb.append("<celldesigner:boxSize width=\"" + alias.getWidth() + "\" height=\"" + alias.getHeight() + "\"/>");
		sb.append("<celldesigner:singleLine width=\"" + alias.getWidth() + "\"/>");
		sb.append("<celldesigner:paint color=\"" + colorToString(alias.getColor()) + "\" scheme=\"Color\"/>");
		sb.append("</celldesigner:briefView>\n");

		sb.append("</celldesigner:complexSpeciesAlias>\n");
		return sb.toString();
	}

}
