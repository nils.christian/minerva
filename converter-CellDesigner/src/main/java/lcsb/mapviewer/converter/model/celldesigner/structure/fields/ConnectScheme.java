package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * CellDEsigner structure used for storing some line information.
 * 
 * @author Piotr Gawron
 * 
 */
public class ConnectScheme {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger				logger = Logger.getLogger(ConnectScheme.class.getName());

	/**
	 * Not used in our model... No ide what it means.
	 */
	private String							connectPolicy;

	/**
	 * Defines where the central point is placed.
	 */
	private Integer							connectIndex;

	/**
	 * Not used in our model... No ide what it means.
	 */
	private Map<String, String>	lineDirections;

	/**
	 * Default constructor.
	 */
	public ConnectScheme() {
		connectPolicy = "";
		connectIndex = null;
		lineDirections = new HashMap<String, String>();
	}

	/**
	 * 
	 * @param directions
	 *          the directions to set
	 * @see #directions
	 */
	public void setLineDirections(Map<String, String> directions) {
		lineDirections = directions;
	}

	/**
	 * 
	 * @param text
	 *          the connectIndex to set in String format
	 * @see #connectIndex
	 */
	public void setConnectIndex(String text) {
		if (text == null || "".equals(text)) {
			connectIndex = null;
		} else {
			setConnectIndex(Integer.parseInt(text));
		}

	}

	/**
	 * @return the connectPolicy
	 * @see #connectPolicy
	 */
	public String getConnectPolicy() {
		return connectPolicy;
	}

	/**
	 * @param connectPolicy
	 *          the connectPolicy to set
	 * @see #connectPolicy
	 */
	public void setConnectPolicy(String connectPolicy) {
		this.connectPolicy = connectPolicy;
	}

	/**
	 * @return the connectIndex
	 * @see #connectIndex
	 */
	public Integer getConnectIndex() {
		return connectIndex;
	}

	/**
	 * @param connectIndex
	 *          the connectIndex to set
	 * @see #connectIndex
	 */
	public void setConnectIndex(Integer connectIndex) {
		this.connectIndex = connectIndex;
	}

	/**
	 * @return the lineDirections
	 * @see #lineDirections
	 */
	public Map<String, String> getLineDirections() {
		return lineDirections;
	}
}
