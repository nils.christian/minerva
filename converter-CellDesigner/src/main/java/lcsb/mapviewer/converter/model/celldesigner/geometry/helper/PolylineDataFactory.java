package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * Facory util that creates {@link PolylineData} objects from CellDesigner
 * structure.
 * 
 * @author Piotr Gawron
 * 
 */
public final class PolylineDataFactory {

	/**
	 * Default constructore that prevent instantiation.
	 */
	private PolylineDataFactory() {
	}

	/**
	 * Creates {@link PolylineData} object from two CellDesigner base points and
	 * list of points (in CellDesigner formet).
	 * 
	 * @param baseA
	 *          first base point
	 * @param baseB
	 *          second base point
	 * @param points
	 *          intermediate points in CellDesigner format
	 * @return {@link PolylineData} object representing input data
	 */
	public static PolylineData createPolylineDataFromEditPoints(Point2D baseA, Point2D baseB, List<Point2D> points) {
		List<Point2D> pointsList = new CellDesignerLineTransformation().getLinePointsFromPoints(baseA, baseB, points);
		return new PolylineData(pointsList);
	}

	/**
	 * Creates {@link PolylineData} object from two CellDesigner base points and
	 * list of points (in CellDesigner formet).
	 * 
	 * @param baseA
	 *          first base point
	 * @param baseB
	 *          second base point
	 * @param points
	 *          structure with intermediate points in CellDesigner format
	 * @return {@link PolylineData} object representing input data
	 */
	public static PolylineData createPolylineDataFromEditPoints(Point2D baseA, Point2D baseB, EditPoints points) {
		if (points != null) {
			return createPolylineDataFromEditPoints(baseA, baseB, points.getPoints());
		} else {
			return createPolylineDataFromEditPoints(baseA, baseB, new ArrayList<Point2D>());
		}
	}
}
