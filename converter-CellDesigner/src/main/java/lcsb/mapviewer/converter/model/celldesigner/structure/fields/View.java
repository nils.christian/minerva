package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;

/**
 * This class represents some layout information in CellDesigner format.
 * 
 * @author Piotr Gawron
 * 
 */
public class View {
	
	/**
	 * No idea what this parameter describes.
	 */
	private Point2D			innerPosition;
	
	/**
	 * Size of the element.
	 */
	private Dimension		boxSize;
	
	/**
	 * Width of the line.
	 */
	private SingleLine	singleLine;
	
	/**
	 * Color of the element.
	 */
	private Color				color;

	/**
	 * @return the innerPosition
	 */
	public Point2D getInnerPosition() {
		return innerPosition;
	}

	/**
	 * @param innerPosition
	 *          the innerPosition to set
	 */
	public void setInnerPosition(Point2D innerPosition) {
		this.innerPosition = innerPosition;
	}

	/**
	 * @return the boxSize
	 */
	public Dimension getBoxSize() {
		return boxSize;
	}

	/**
	 * @param boxSize
	 *          the boxSize to set
	 */
	public void setBoxSize(Dimension boxSize) {
		this.boxSize = boxSize;
	}

	/**
	 * @return the singleLine
	 */
	public SingleLine getSingleLine() {
		return singleLine;
	}

	/**
	 * @param singleLine
	 *          the singleLine to set
	 */
	public void setSingleLine(SingleLine singleLine) {
		this.singleLine = singleLine;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *          the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}
}
