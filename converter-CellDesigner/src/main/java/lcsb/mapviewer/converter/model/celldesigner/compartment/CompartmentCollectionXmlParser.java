package lcsb.mapviewer.converter.model.celldesigner.compartment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;

/**
 * CellDEsigner xml parser for collection of compartments.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentCollectionXmlParser extends XmlParser {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger												logger = Logger.getLogger(CompartmentCollectionXmlParser.class.getName());

	/**
	 * CellDesigner xml parser for single compartment.
	 */
	private CompartmentXmlParser					compartmentParser;

	/**
	 * Collection of {@link CellDesignerElement cell designer elements} parsed
	 * from xml.
	 */
	private CellDesignerElementCollection	elements;

	/**
	 * Default constructor.
	 * 
	 * @param elements
	 *          collection of {@link CellDesignerElement cell designer elements}
	 *          parsed from xml
	 */
	public CompartmentCollectionXmlParser(CellDesignerElementCollection elements) {
		this.elements = elements;
		compartmentParser = new CompartmentXmlParser(elements);
	}

	/**
	 * Parse CellDEsigner xml node with collection of compartments.
	 * 
	 * @param compartmentsNode
	 *          xml node to parse
	 * @return list of compartments obtained from xml node
	 * @throws InvalidXmlSchemaException
	 *           thrown when there is a problem with xml
	 */
	public List<CellDesignerCompartment> parseXmlCompartmentCollection(Node compartmentsNode) throws InvalidXmlSchemaException {
		List<CellDesignerCompartment> result = new ArrayList<CellDesignerCompartment>();
		NodeList nodes = compartmentsNode.getChildNodes();
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("compartment")) {
					Pair<String, CellDesignerCompartment> compartmentPair = compartmentParser.parseXmlElement(node);
					result.add(compartmentPair.getRight());
				} else {
					throw new InvalidXmlSchemaException("Unknown element of model/listOfCompartments: " + node.getNodeName());
				}
			}
		}
		return result;
	}

	/**
	 * Creates CellDesigner xml string from set of compartments.
	 * 
	 * @param collection
	 *          collection of compartments to be transformed into xml
	 * @return xml representation of the compartments
	 */
	public String toXml(Collection<Compartment> collection) {
		StringBuilder result = new StringBuilder();
		result.append("<listOfCompartments>\n");
		Set<String> addedIds = new HashSet<>();
		for (Compartment compartment : collection) {
			String sbmlId = elements.getElementId(compartment);
			if (!addedIds.contains(sbmlId) && !(compartment instanceof PathwayCompartment)) {
				addedIds.add(sbmlId);
				result.append(compartmentParser.toXml(compartment));
			}
		}
		Compartment defaultCompartment = new Compartment("default");
		result.append(compartmentParser.toXml(defaultCompartment));
		result.append("</listOfCompartments>\n");
		return result.toString();
	}
}
