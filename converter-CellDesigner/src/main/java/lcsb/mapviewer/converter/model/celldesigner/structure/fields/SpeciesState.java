package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Structure for storing the state of the Species in CellDesigner format.
 * 
 * @author Piotr Gawron
 * 
 */
public class SpeciesState {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(SpeciesState.class.getName());

  /**
   * How many dimers are in the species.
   */
  private int homodimer = 1;

  /**
   * String state description.
   */
  private String structuralState = null;

  /**
   * List of species modification.
   */
  private List<CellDesignerModificationResidue> modifications = new ArrayList<>();

  /**
   * Adds modification to the state.
   * 
   * @param modificationResidue
   *          modification to add
   */
  public void addModificationResidue(CellDesignerModificationResidue modificationResidue) {
    for (CellDesignerModificationResidue modification : modifications) {
      if (modification.getIdModificationResidue().equals(modificationResidue.getIdModificationResidue())) {
        modification.update(modificationResidue);
        return;
      }
    }
    modifications.add(modificationResidue);
  }

  /**
   * Default constructor.
   */
  public SpeciesState() {
  }

  /**
   * Creates species state description from species element.
   * 
   * @param species
   *          object for which description is created
   */
  public SpeciesState(Species species) {
    if (species instanceof Protein) {
      Protein protein = (Protein) species;
      setStructuralState(protein.getStructuralState());
      for (ModificationResidue mr : protein.getModificationResidues()) {
        addModificationResidue(new CellDesignerModificationResidue(mr));
      }

    } else if (species instanceof Complex) {
      Complex complex = (Complex) species;
      setStructuralState(complex.getStructuralState());
    } else if (species instanceof Rna) {
      Rna rna = (Rna) species;
      for (ModificationResidue region : rna.getRegions()) {
        CellDesignerModificationResidue mr = new CellDesignerModificationResidue(region);
        addModificationResidue(mr);
      }
    } else if (species instanceof AntisenseRna) {
      AntisenseRna rna = (AntisenseRna) species;
      for (ModificationResidue region : rna.getRegions()) {
        CellDesignerModificationResidue mr = new CellDesignerModificationResidue(region);
        addModificationResidue(mr);
      }
    } else if (species instanceof Gene) {
      Gene gene = (Gene) species;
      for (ModificationResidue mr : gene.getModificationResidues()) {
        addModificationResidue(new CellDesignerModificationResidue(mr));
      }
    }
    setHomodimer(species.getHomodimer());
  }

  /**
   * 
   * @param homodimer
   *          new {@link #homodimer} value to set (in string format)
   */
  public void setHomodimer(String homodimer) {
    try {
      this.homodimer = Integer.parseInt(homodimer);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid homodir value: " + homodimer);
    }
  }

  /**
   * @return the homodimer
   * @see #homodimer
   */
  public int getHomodimer() {
    return homodimer;
  }

  /**
   * @param homodimer
   *          the homodimer to set
   * @see #homodimer
   */
  public void setHomodimer(int homodimer) {
    this.homodimer = homodimer;
  }

  /**
   * @return the structuralState
   * @see #structuralState
   */
  public String getStructuralState() {
    return structuralState;
  }

  /**
   * @param structuralState
   *          the structuralState to set
   * @see #structuralState
   */
  public void setStructuralState(String structuralState) {
    this.structuralState = structuralState;
  }

  /**
   * @return the modifications
   * @see #modifications
   */
  public List<CellDesignerModificationResidue> getModifications() {
    return modifications;
  }

  /**
   * @param modifications
   *          the modifications to set
   * @see #modifications
   */
  public void setModifications(List<CellDesignerModificationResidue> modifications) {
    this.modifications = modifications;
  }

}
