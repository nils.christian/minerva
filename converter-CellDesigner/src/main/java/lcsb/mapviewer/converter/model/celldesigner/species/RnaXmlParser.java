package lcsb.mapviewer.converter.model.celldesigner.species;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerRna} object.
 * 
 * @author Piotr Gawron
 * 
 */

public class RnaXmlParser extends AbstractElementXmlParser<CellDesignerRna, Rna> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(RnaXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public RnaXmlParser(CellDesignerElementCollection elements) {
    this.elements = elements;
  }

  @Override
  public Pair<String, CellDesignerRna> parseXmlElement(Node rnaNode) throws InvalidXmlSchemaException {
    CellDesignerRna rna = new CellDesignerRna();
    String identifier = getNodeAttr("id", rnaNode);
    rna.setName(decodeName(getNodeAttr("name", rnaNode)));
    NodeList list = rnaNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          rna.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                rna.addRegion(getRnaRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:rna " + node.getNodeName());
        }
      }
    }
    return new Pair<String, CellDesignerRna>(identifier, rna);
  }

  /**
   * Parses CellDesigner xml node for RnaRegion.
   * 
   * @param residueNode
   *          xml node to parse
   * @return {@link CellDesignerRnaRegion} object created from the node
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getRnaRegion(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(getNodeAttr("id", residueNode));
    residue.setSize(getNodeAttr("size", residueNode));
    residue.setPos(getNodeAttr("pos", residueNode));
    String typeString = getNodeAttr("type", residueNode);
    if (typeString != null) {
      residue.setModificationType(ModificationType.getByCellDesignerName(typeString));
    }
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

  @Override
  public String toXml(Rna rna) {
    String attributes = "";
    String result = "";
    attributes += " id=\"r_" + elements.getElementId(rna) + "\"";
    if (!rna.getName().equals("")) {
      attributes += " name=\"" + escapeXml(encodeName(rna.getName())) + "\"";
    }
    result += "<celldesigner:RNA" + attributes + ">";
    result += "<celldesigner:notes>";
    result += "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>";
    RestAnnotationParser rap = new RestAnnotationParser();
    result += rap.createAnnotationString(rna);
    result += rna.getNotes();
    result += "</body></html>";
    result += "</celldesigner:notes>";

    if (rna.getRegions().size() > 0) {
      result += "<celldesigner:listOfRegions>";
      for (ModificationResidue mr : rna.getRegions()) {
        result += toXml(mr);
      }
      result += "</celldesigner:listOfRegions>";
    }

    result += "</celldesigner:RNA>";
    return result;
  }

  /**
   * Generates CellDesigner xml for {@link CellDesignerModificationResidue}.
   * 
   * @param mr
   *          object to transform into xml
   * @return CellDesigner xml for {@link CellDesignerModificationResidue}
   */
  String toXml(ModificationResidue mr) {
    CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);

    String result = "";
    String attributes = "";
    if (!mr.getIdModificationResidue().equals("")) {
      attributes += " id=\"" + mr.getIdModificationResidue() + "\"";
    }
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + escapeXml(mr.getName()) + "\"";
    }
    String type = null;
    if (mr instanceof ModificationSite) {
      type = ModificationType.MODIFICATION_SITE.getCellDesignerName();
    } else if (mr instanceof CodingRegion) {
      type = ModificationType.CODING_REGION.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else if (mr instanceof ProteinBindingDomain) {
      type = ModificationType.PROTEIN_BINDING_DOMAIN.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else {
      throw new InvalidArgumentException("Don't know how to handle: " + mr.getClass());
    }
    attributes += " type=\"" + type + "\"";
    attributes += " pos=\"" + cellDesignerModificationResidue.getPos() + "\"";
    result += "<celldesigner:region " + attributes + ">";
    result += "</celldesigner:region>\n";

    return result;
  }

}
