package lcsb.mapviewer.converter.model.celldesigner.function;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;

public class FunctionCollectionXmlParser extends XmlParser {

  private FunctionXmlParser functionParser = new FunctionXmlParser();

  public Set<SbmlFunction> parseXmlFunctionCollection(Node functionsNode) throws InvalidXmlSchemaException {
    Set<SbmlFunction> result = new HashSet<>();
    for (Node node : super.getNodes("functionDefinition", functionsNode.getChildNodes())) {
      result.add(functionParser.parseFunction(node));
    }
    return result;
  }

  public String toXml(Set<SbmlFunction> functions) {
    StringBuilder result = new StringBuilder();
    result.append("<listOfFunctionDefinitions>\n");
    for (SbmlFunction function : functions) {
      result.append(functionParser.toXml(function));
    }

    result.append("</listOfFunctionDefinitions>\n");
    return result.toString();
  }

}
