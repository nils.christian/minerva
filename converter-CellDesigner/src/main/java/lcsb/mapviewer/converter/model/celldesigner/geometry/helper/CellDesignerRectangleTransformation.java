package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * Class with basic operations on rectangles.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerRectangleTransformation {

	/**
	 * Object used for transformation on the polygons.
	 */
	private CellDesignerPolygonTransformation	polygonTransformation	= new CellDesignerPolygonTransformation();

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger											logger								= Logger.getLogger(CellDesignerRectangleTransformation.class.getName());

	/**
	 * Returns a point on a rectangle for anchor.
	 * 
	 * @param x
	 *          x coordinate of the rectangle
	 * @param y
	 *          y coordinate of the rectangle
	 * @param width
	 *          width of the rectangle
	 * @param height
	 *          height of the rectangle
	 * @param anchor
	 *          point where we are looking for the point
	 * @return coordinates on the rectangle in direction described by anchor
	 */
	public Point2D getPointOnRectangleByAnchor(double x, double y, double width, double height, CellDesignerAnchor anchor) {
		if (anchor == null || anchor.getAngle() == null) {
			return new Point2D.Double(x + width / 2, y + height / 2);
		}
		ArrayList<Point2D> list = new ArrayList<Point2D>();
		list.add(new Point2D.Double(x, y + height / 2));
		list.add(new Point2D.Double(x, y));
		list.add(new Point2D.Double(x + width / 2, y));
		list.add(new Point2D.Double(x + width, y));
		list.add(new Point2D.Double(x + width, y + height / 2));
		list.add(new Point2D.Double(x + width, y + height));
		list.add(new Point2D.Double(x + width / 2, y + height));
		list.add(new Point2D.Double(x, y + height));
		return polygonTransformation.getPointOnPolygonByAnchor(list, anchor);
	}

}
