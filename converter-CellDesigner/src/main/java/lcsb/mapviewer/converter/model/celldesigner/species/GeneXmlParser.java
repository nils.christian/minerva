package lcsb.mapviewer.converter.model.celldesigner.species;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerGene} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneXmlParser extends AbstractElementXmlParser<CellDesignerGene, Gene> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(GeneXmlParser.class.getName());

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public GeneXmlParser(CellDesignerElementCollection elements) {
    this.elements = elements;
  }

  @Override
  public Pair<String, CellDesignerGene> parseXmlElement(Node geneNode) throws InvalidXmlSchemaException {
    CellDesignerGene gene = new CellDesignerGene();
    String identifier = getNodeAttr("id", geneNode);
    gene.setName(decodeName(getNodeAttr("name", geneNode)));
    NodeList list = geneNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          gene.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                gene.addModificationResidue(getModificationResidue(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:gene " + node.getNodeName());
        }
      }
    }
    return new Pair<String, CellDesignerGene>(identifier, gene);
  }

  /**
   * Parse modification for a gene.
   * 
   * @param residueNode
   *          source xml node
   * @return object representing modification
   * @throws InvalidXmlSchemaException
   *           thrown when input xml node doesn't follow defined schema
   */
  CellDesignerModificationResidue getModificationResidue(Node residueNode) throws InvalidXmlSchemaException {
    CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(getNodeAttr("id", residueNode));
    residue.setName(getNodeAttr("name", residueNode));
    residue.setSide(getNodeAttr("side", residueNode));
    residue.setSize(getNodeAttr("size", residueNode));
    residue.setActive(getNodeAttr("active", residueNode));
    residue.setAngle(getNodeAttr("pos", residueNode));
    String type = getNodeAttr("type", residueNode);
    try {
      residue.setModificationType(ModificationType.getByCellDesignerName(type));
    } catch (InvalidArgumentException e) {
      throw new InvalidXmlSchemaException(e);
    }
    NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

  @Override
  public String toXml(Gene gene) {
    String attributes = "";
    String result = "";
    attributes += " id=\"g_" + elements.getElementId(gene) + "\"";
    if (!gene.getName().equals("")) {
      attributes += " name=\"" + escapeXml(encodeName(gene.getName())) + "\"";
    }
    result += "<celldesigner:gene" + attributes + ">";
    result += "<celldesigner:notes>";
    result += "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>";
    RestAnnotationParser rap = new RestAnnotationParser();
    result += rap.createAnnotationString(gene);
    result += gene.getNotes();
    result += "</body></html>";
    result += "</celldesigner:notes>";

    if (gene.getModificationResidues().size() > 0) {
      result += "<celldesigner:listOfRegions>\n";
      for (ModificationResidue mr : gene.getModificationResidues()) {
        result += toXml(mr);
      }
      result += "</celldesigner:listOfRegions>\n";
    }

    result += "</celldesigner:gene>";
    return result;
  }

  /**
   * Generates CellDesigner xml for {@link CellDesignerModificationResidue}.
   * 
   * @param mr
   *          object to transform into xml
   * @return CellDesigner xml for {@link CellDesignerModificationResidue}
   */
  String toXml(ModificationResidue mr) {
    CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);

    String result = "";
    String attributes = "";
    if (!mr.getIdModificationResidue().equals("")) {
      attributes += " id=\"" + mr.getIdModificationResidue() + "\"";
    }
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + escapeXml(mr.getName()) + "\"";
    }
    String type = null;
    if (mr instanceof ModificationSite) {
      type = ModificationType.MODIFICATION_SITE.getCellDesignerName();
    } else if (mr instanceof RegulatoryRegion) {
      type = ModificationType.REGULATORY_REGION.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else if (mr instanceof CodingRegion) {
      type = ModificationType.CODING_REGION.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else if (mr instanceof TranscriptionSite) {
      TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
      if (transcriptionSite.getDirection().equals("RIGHT")) {
        type = ModificationType.TRANSCRIPTION_SITE_RIGHT.getCellDesignerName();
      } else {
        type = ModificationType.TRANSCRIPTION_SITE_LEFT.getCellDesignerName();
      }
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
      attributes += " active=\"" + transcriptionSite.getActive() + "\"";
    } else {
      throw new InvalidArgumentException("Don't know how to handle: " + mr.getClass());
    }
    attributes += " type=\"" + type + "\"";
    attributes += " pos=\"" + cellDesignerModificationResidue.getPos() + "\"";
    result += "<celldesigner:region " + attributes + ">";
    result += "</celldesigner:region>\n";

    return result;
  }

}
