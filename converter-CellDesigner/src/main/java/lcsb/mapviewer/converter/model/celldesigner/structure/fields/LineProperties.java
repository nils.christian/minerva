package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.awt.Color;

/**
 * Structure used in CellDEsigner model to store information about line
 * properties.
 * 
 * @author Piotr Gawron
 * 
 */
public class LineProperties {
	
	/**
	 * Width of the line.
	 */
	private double width;
	
	/**
	 * Color of the line.
	 */
	private Color	 color;

	/**
	 * Line type (no idea what is the format).
	 */
	private String type;

	/**
	 * Default constructor.
	 */
	public LineProperties() {
		width = 1;
		color = Color.BLACK;
		type = "";
	}

	/**
	 * @return the width
	 * @see #width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width
	 *          the width to set
	 * @see #width
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @return the color
	 * @see #color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *          the color to set
	 * @see #color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(String type) {
		this.type = type;
	}

}
