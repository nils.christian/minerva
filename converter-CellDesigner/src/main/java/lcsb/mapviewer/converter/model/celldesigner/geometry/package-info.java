/**
 * Provides classes that transform geometry information from CellDesigner
 * objects into normal x,y coordinates in our model.
 */
package lcsb.mapviewer.converter.model.celldesigner.geometry;

