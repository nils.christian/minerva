package lcsb.mapviewer.converter.model.celldesigner;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SingleLine;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;

import org.w3c.dom.Node;

/**
 * Class with parsers for common CellDesigner objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class CommonXmlParser extends XmlParser {
	/**
	 * Parse xml representation of position into Poin2D object.
	 * 
	 * @param node
	 *          xml node to parse
	 * @return Point2D object
	 */
	public Point2D getPosition(Node node) {
		double x = Double.parseDouble(getNodeAttr("x", node));
		double y = Double.parseDouble(getNodeAttr("y", node));
		return new Point2D.Double(x, y);

	}

	/**
	 * Parse xml representation of dimension.
	 * 
	 * @param node
	 *          xml node to parse
	 * @return dimension (with width and height fields)
	 */
	public Dimension getDimension(Node node) {
		double width = Double.parseDouble(getNodeAttr("width", node));
		double height = Double.parseDouble(getNodeAttr("height", node));
		Dimension result = new Dimension();
		result.setSize(width, height);
		return result;
	}

	/**
	 * Parse xml representation of CellDesigner SingleLine.
	 * 
	 * @param node
	 *          xml node to parse
	 * @return sinle line object (with width field)
	 */
	public SingleLine getSingleLine(Node node) {
		SingleLine result = new SingleLine();
		result.setWidth(Double.parseDouble(getNodeAttr("width", node)));
		return result;
	}

	/**
	 * Parse xml representation of CellDesigner color.
	 * 
	 * @param node
	 *          xml node to parse
	 * @return Color value stored in xml
	 */
	public Color getColor(Node node) {
		String color = getNodeAttr("color", node);
		return stringToColor(color);
	}

	/**
	 * Parse xml representation of CellDesigner view.
	 * 
	 * @param node
	 *          xml node to parse
	 * @return view parsed from xml
	 * @see View
	 */
	public View getView(Node node) {
		View result = new View();
		Node tmpNode = getNode("celldesigner:innerPosition", node.getChildNodes());
		if (tmpNode != null) {
			result.setInnerPosition(getPosition(tmpNode));
		}

		tmpNode = getNode("celldesigner:boxSize", node.getChildNodes());
		if (tmpNode != null) {
			result.setBoxSize(getDimension(tmpNode));
		}

		tmpNode = getNode("celldesigner:singleLine", node.getChildNodes());
		if (tmpNode != null) {
			result.setSingleLine(getSingleLine(tmpNode));
		}

		tmpNode = getNode("celldesigner:paint", node.getChildNodes());
		if (tmpNode != null) {
			result.setColor(getColor(tmpNode));
		}
		return result;
	}

}
