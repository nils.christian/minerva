/**
 * This package contains internal CellDesigner structures describing some fields
 * of the Species elements.
 */
package lcsb.mapviewer.converter.model.celldesigner.structure.fields;
