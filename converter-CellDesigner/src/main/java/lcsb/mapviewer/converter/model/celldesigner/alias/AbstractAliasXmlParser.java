package lcsb.mapviewer.converter.model.celldesigner.alias;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Generic abstract interface for parsing CellDesigner xml nodes with species
 * definition.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          type of the object to parse
 */
public abstract class AbstractAliasXmlParser<T extends Element> extends XmlParser {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger					logger			 = Logger.getLogger(AbstractAliasXmlParser.class.getName());

	/**
	 * Set of common functions used in parsing cell designer xml.
	 */
	private CommonXmlParser	commonParser = new CommonXmlParser();

	/**
	 * Parse object from the xml node.
	 * 
	 * @param node
	 *          xml node to parse
	 * 
	 * @return parsed object
	 * @throws InvalidXmlSchemaException
	 *           thrown when xmlString is invalid
	 */
	abstract T parseXmlAlias(Node node) throws InvalidXmlSchemaException;

	/**
	 * Parse object from the xml string.
	 * 
	 * @param xmlString
	 *          xml string
	 * @return parsed object
	 * @throws InvalidXmlSchemaException
	 *           thrown when xmlString is invalid
	 */
	public T parseXmlAlias(String xmlString) throws InvalidXmlSchemaException {
		Document doc = getXmlDocumentFromString(xmlString);
		NodeList root = doc.getChildNodes();
		return parseXmlAlias(root.item(0));

	}

	/**
	 * Method that transform object into CellDesigner xml.
	 * 
	 * @param alias
	 *          object to be transformed
	 * @return CellDesigner xml representation of the alias
	 */
	abstract String toXml(T alias);

	/**
	 * @return the commonParser
	 */
	CommonXmlParser getCommonParser() {
		return commonParser;
	}

	/**
	 * @param commonParser
	 *          the commonParser to set
	 */
	void setCommonParser(CommonXmlParser commonParser) {
		this.commonParser = commonParser;
	}

}
