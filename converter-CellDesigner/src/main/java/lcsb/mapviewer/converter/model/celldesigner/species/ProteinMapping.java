package lcsb.mapviewer.converter.model.celldesigner.species;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerIonChannelProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerReceptorProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerTruncatedProtein;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.TruncatedProtein;

/**
 * This enum contains information about mapping between CellDesigner xml nodes
 * and classes that extend {@link CellDesignerProtein}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ProteinMapping {

	/**
	 * {@link GenericProtein}.
	 */
	GENERIC_PROTEIN(GenericProtein.class, CellDesignerGenericProtein.class, "GENERIC"),

	/**
	 * {@link IonChannelProtein}.
	 */
	ION_CHANNEL_PROTEIN(IonChannelProtein.class, CellDesignerIonChannelProtein.class, "ION_CHANNEL"),

	/**
	 * {@link ReceptorProtein}.
	 */
	RECEPTOR_PROTEIN(ReceptorProtein.class, CellDesignerReceptorProtein.class, "RECEPTOR"),

	/**
	 * {@link TruncatedProtein}.
	 */
	TRUNCATED_PROTEIN(TruncatedProtein.class, CellDesignerTruncatedProtein.class, "TRUNCATED");

	/**
	 * CellDesigner xml node type.
	 */
	private String																	cellDesignerString;

	/**
	 * Model class that should be used to represent cell designer node.
	 */
	private Class<? extends Protein>								clazz;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger														logger = Logger.getLogger(ProteinMapping.class);

	/**
	 * {@link CellDesignerProtein Celldesigner structure} used to represent the
	 * node.
	 */
	private Class<? extends CellDesignerProtein<?>>	cellDesignerClazz;

	/**
	 * Default constructor.
	 * 
	 * @param cellDesignerClazz
	 *          {@link #clazz}
	 * @param modelClazz
	 *          {@link #clazz class} in model corresponding to celldesigner object
	 * @param cellDesignerString
	 *          {@link #cellDesignerString}
	 */
	ProteinMapping(Class<? extends Protein> modelClazz, Class<? extends CellDesignerProtein<?>> cellDesignerClazz, String cellDesignerString) {
		this.cellDesignerString = cellDesignerString;
		this.clazz = modelClazz;
		this.cellDesignerClazz = cellDesignerClazz;
	}

	/**
	 * @return the cellDesignerName
	 * @see #cellDesignerString
	 */
	public String getCellDesignerString() {
		return cellDesignerString;
	}

	/**
	 * @return the clazz
	 * @see #clazz
	 */
	public Class<? extends Protein> getClazz() {
		return clazz;
	}

	/**
	 * Returns celldesigner class type that represents xml node.
	 * 
	 * @return celldesigner class type that represents xml node
	 */
	public Class<? extends CellDesignerProtein<?>> getCellDesignerClazz() {
		return cellDesignerClazz;
	}

	/**
	 * Returns {@link ProteinMapping} for given CellDesigner xml node name.
	 * 
	 * @param string
	 *          CellDesigner xml node name
	 * @return {@link ProteinMapping} for given CellDesigner xml node name
	 */
	public static ProteinMapping getMappingByString(String string) {
		for (ProteinMapping mapping : ProteinMapping.values()) {
			if (mapping.getCellDesignerString().equals(string)) {
				return mapping;
			}
		}
		return null;
	}

	/**
	 * Returns {@link ProteinMapping} for given {@link CellDesignerProtein} class.
	 * 
	 * @param searchClazz
	 *          {@link CellDesignerProtein} class
	 * @return {@link ProteinMapping} for given {@link CellDesignerProtein} class
	 */
	public static ProteinMapping getMappingByClass(Class<? extends Protein> searchClazz) {
		for (ProteinMapping mapping : ProteinMapping.values()) {
			if (mapping.getClazz().isAssignableFrom(searchClazz)) {
				return mapping;
			}
		}
		return null;
	}

	/**
	 * Creates instance of {@link CellDesignerProtein} specific for this
	 * {@link ProteinMapping}.
	 * 
	 * @return new instance of {@link CellDesignerProtein} specific for this
	 *         {@link ProteinMapping}.
	 */
	public CellDesignerProtein<?> createProtein() {
		try {
			return cellDesignerClazz.getConstructor().newInstance();
		} catch (Exception e) {
			throw new InvalidStateException(e);
		}
	}
}
