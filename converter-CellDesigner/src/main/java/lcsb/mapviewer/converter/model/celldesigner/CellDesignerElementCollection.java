package lcsb.mapviewer.converter.model.celldesigner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * This structure contains information about {@link CellDesignerElement
 * CellDesigner elements} parsed from the file. The information about this
 * elements is used in different places in the parsing process.
 */
public class CellDesignerElementCollection {

  /**
   * Element by element identifier (it's CellDesigner identifier).
   */
  private Map<String, CellDesignerElement<?>> elementById = new HashMap<>();

  /**
   * Returns element by given identifier.
   * 
   * @param speciesId
   *          element identifier
   * @return element by given identifier
   * 
   * @param <T>
   *          type of returned object
   */
  @SuppressWarnings("unchecked")
  public <T extends CellDesignerElement<?>> T getElementByElementId(String speciesId) {
    return (T) elementById.get(speciesId);
  }

  /**
   * 
   */
  private Map<String, String> sbmlIdByElement = new HashMap<>();

  /**
   * Returns element identifier that should be used for model element when
   * creating cell designer xml file.
   * 
   * @param modelElement
   *          model element for which we want to obtain identifier
   * @return identifier of cell designer element that will be exported
   */
  public String getElementId(Element modelElement) {
    if ("default".equals(modelElement.getElementId())) {
      return modelElement.getElementId();
    } else {
      String sbmlId = getSbmlId(modelElement, true);
      if (sbmlIdByElement.get(sbmlId) == null) {
        String id = "s_id_" + modelElement.getElementId();
        if (sbmlIdByElement.values().contains(id)) {
          throw new InvalidArgumentException("id duplicates");
        }
        sbmlIdByElement.put(sbmlId, id);
      }
      return sbmlIdByElement.get(sbmlId);

    }
  }

  /**
   * Creates a String that identifies element as distinct SBML entity.
   * 
   * @param modelElement
   *          element that we want to identify
   * @param useComplex
   *          should we use identifier of a complex. This should be used by
   *          default (because if the complex is different then element should
   *          have different identifier). However, when element asks complex for
   *          id, complex will try to resolve ids of children (because this is
   *          what defines complex identity), and in such situation it should
   *          disable resolving complex, because there will by infinity cyclic
   *          calls and stack overflow error will be thrown.
   * @return unique String for SBML entity
   */
  private String getSbmlId(Element modelElement, boolean useComplex) {
    String compartmenName = "default";
    if (modelElement.getCompartment() != null) {
      compartmenName = modelElement.getCompartment().getName();
    }

    String modifications = "";
    List<ModificationResidue> regions = new ArrayList<>();
    if (modelElement instanceof AntisenseRna) {
      AntisenseRna asAntisenseRna = ((AntisenseRna) modelElement);
      regions.addAll(asAntisenseRna.getRegions());
    } else if (modelElement instanceof Gene) {
      Gene asGene = ((Gene) modelElement);
      regions.addAll(asGene.getModificationResidues());
    } else if (modelElement instanceof Protein) {
      Protein asProtein = ((Protein) modelElement);
      modifications = asProtein.getStructuralState();
      regions.addAll(asProtein.getModificationResidues());
    } else if (modelElement instanceof Rna) {
      Rna asRna = ((Rna) modelElement);
      regions.addAll(asRna.getRegions());
    } else if (modelElement instanceof Complex) {
      Complex asComplex = ((Complex) modelElement);
      modifications = asComplex.getStructuralState();
    }
    for (ModificationResidue region : regions) {
      if (region instanceof AbstractSiteModification) {
        modifications += ((AbstractSiteModification) region).getState();
      }
    }

    String complexId = "";
    String homodimer = "";
    if (modelElement instanceof Species) {
      homodimer = ((Species) modelElement).getHomodimer() + "";
      if (((Species) modelElement).getComplex() != null) {
        if (useComplex) {
          if (!isCyclicNesting(((Species) modelElement).getComplex())) {
            complexId = getElementId(((Species) modelElement).getComplex());
          } else {
            throw new InvalidArgumentException(
                "Cycling nested structure found in element: " + modelElement.getElementId());
          }
        } else {
          complexId = ((Species) modelElement).getComplex().getName();
        }
      }
    }
    String childrenId = "";
    if (modelElement instanceof Complex) {
      Complex asComplex = (Complex) modelElement;
      List<String> childIds = new ArrayList<>();
      for (Species child : asComplex.getAllChildren()) {
        childIds.add(getSbmlId(child, false));
      }
      Collections.sort(childIds);
      for (String string : childIds) {
        childrenId += string + "\n";
      }
    }

    // identifier that distinguish elements in SBML depends only on type,
    // name, compartment, modifications, homodimer, state, complex where it's
    // located,
    // children of the complex
    String sbmlId = compartmenName + "\n" + modelElement.getName() + "\n" + modelElement.getStringType() + "\n"
        + modifications + "\n" + complexId + "\n" + homodimer + "\n" + childrenId;

    return sbmlId;
  }

  /**
   * Checks if complex parenting is cyclic.
   * 
   * @param complex
   *          complex for which data is checked
   * @return true if parent of the complex is also a (grand)child of this complex,
   *         false otherwise
   */
  private boolean isCyclicNesting(Complex complex) {
    Set<Complex> foundComplexes = new HashSet<>();
    while (complex != null) {
      if (foundComplexes.contains(complex)) {
        return true;
      }
      foundComplexes.add(complex);
      complex = complex.getComplex();
    }
    return false;
  }

  /**
   * Adds cell designer structures.
   * 
   * @param elements
   *          list of objects to add
   */
  public void addElements(List<? extends CellDesignerElement<?>> elements) {
    for (CellDesignerElement<?> element : elements) {
      addElement(element);
    }
  }

  /**
   * Adds cell designer object.
   * 
   * @param element
   *          object to add
   */
  public void addElement(CellDesignerElement<?> element) {
    addElement(element, element.getElementId());
  }

  /**
   * Adds CellDesigner element with custom id (instead the one obtained from
   * CellDesigner structure).
   * 
   * @param element
   *          element to be add
   * @param id
   *          id that should be used for identifying element
   */
  public void addElement(CellDesignerElement<?> element, String id) {
    if (elementById.get(id) != null) {
      throw new InvalidArgumentException("[" + element.getClass().getSimpleName() + " " + element.getElementId() + "]\t"
          + "Element with given id alread exists. ID: " + id);
    }
    elementById.put(id, element);
  }

  /**
   * Adds CellDesigner structure in a way that it would be accessed via identifier
   * for model structure. Method used only for unit test.
   * 
   * @param modelElement
   *          model element that will create identifier
   * @param element
   *          element to be added
   */
  public void addModelElement(Element modelElement, CellDesignerElement<?> element) {
    addElement(element, getElementId(modelElement));
    if (getElementByElementId(element.getElementId()) == null) {
      addElement(element);
    }
  }

}
