package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class representing CellDesigner {@link AntisenseRna}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerAntisenseRna extends CellDesignerSpecies<AntisenseRna> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(CellDesignerAntisenseRna.class);

  /**
   * List of antisense rna regions (some rna sequences) in this object.
   */
  private List<CellDesignerModificationResidue> regions = new ArrayList<>();

  /**
   * Constructor that copies the data from species given in the argument.
   * 
   * @param species
   *          parent species from which we want to copy data
   */
  public CellDesignerAntisenseRna(CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerAntisenseRna) {
      CellDesignerAntisenseRna asRna = (CellDesignerAntisenseRna) species;
      for (CellDesignerModificationResidue region : asRna.getRegions()) {
        addRegion(new CellDesignerModificationResidue(region));
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerAntisenseRna() {
    super();
  }

  @Override
  public CellDesignerAntisenseRna copy() {
    if (this.getClass() == CellDesignerAntisenseRna.class) {
      return new CellDesignerAntisenseRna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * Add antisense rna region (part of rna sequence that has some meaning) to the
   * object. If the region with given id exists then the data of this region is
   * copied to the one that is already in the {@link CellDesignerAntisenseRna}.
   * 
   * 
   * @param region
   *          region to add
   */
  public void addRegion(CellDesignerModificationResidue region) {
    for (CellDesignerModificationResidue region2 : regions) {
      if (region2.getIdModificationResidue().equals(region.getIdModificationResidue())) {
        region2.update(region);
        return;
      }
    }
    regions.add(region);
    region.setSpecies(this);
  }

  @Override
  public void update(CellDesignerSpecies<?> sp) {
    super.update(sp);
    if (sp instanceof CellDesignerAntisenseRna) {
      CellDesignerAntisenseRna rna = (CellDesignerAntisenseRna) sp;
      for (CellDesignerModificationResidue region : rna.getRegions()) {
        updateRegion(region);
      }
    }
  }

  /**
   * Method update antisense rna region from the object in params (if the object
   * with the same id already exists). If there is no object with given id then
   * new object is added to antisense rna.
   * 
   * @param param
   *          - object with new data from where update should be performed
   */
  private void updateRegion(CellDesignerModificationResidue param) {
    for (CellDesignerModificationResidue region : regions) {
      if (region.getIdModificationResidue().equals(param.getIdModificationResidue())) {
        region.update(param);
        return;
      }
    }
    regions.add(param);
  }

  /**
   * @return the regions
   * @see #regions
   */
  public List<CellDesignerModificationResidue> getRegions() {
    return regions;
  }

  /**
   * @param regions
   *          the regions to set
   * @see #regions
   */
  public void setRegions(List<CellDesignerModificationResidue> regions) {
    this.regions = regions;
  }

  @Override
  public AntisenseRna createModelElement(String aliasId) {
    AntisenseRna result = new AntisenseRna(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

  @Override
  public void updateModelElementAfterLayoutAdded(Species element) {
    for (CellDesignerModificationResidue region : regions) {
      ((AntisenseRna) element).addRegion(region.createModificationResidue(element));
    }
  }

}
