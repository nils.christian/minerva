package lcsb.mapviewer.converter.model.celldesigner.annotation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class RestAnnotationParserTest extends CellDesignerTestFunctions {
  Logger logger = Logger.getLogger(RestAnnotationParserTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() throws Exception {

    try {
      RestAnnotationParser rap = new RestAnnotationParser();

      String response = readFile("testFiles/annotation/sampleDescription.txt");

      List<String> synonyms = rap.getSynonyms(response);
      assertEquals(3, synonyms.size());
      assertTrue(synonyms.contains("CAMK"));
      assertTrue(synonyms.contains("CAMKIV"));

      List<String> formerSymbols = rap.getFormerSymbols(response);
      assertEquals(0, formerSymbols.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseNotes() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes.xml");
      Species proteinAlias = new GenericProtein("id");
      parser.processNotes(node.getFirstChild(), proteinAlias);
      String notes = proteinAlias.getNotes();
      assertNotNull(notes);
      assertFalse(notes.contains("body"));
      assertTrue(notes.contains("some"));
      // after redoing it should be the same
      parser.processNotes(node.getFirstChild(), proteinAlias);
      notes = proteinAlias.getNotes();
      assertNotNull(notes);
      assertFalse(notes.contains("body"));
      assertTrue(notes.contains("some"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseNotes2() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes3.xml");
      Species proteinAlias = new GenericProtein("id");
      parser.processNotes(node.getFirstChild(), proteinAlias);
      String notes = proteinAlias.getNotes();
      assertNotNull(notes);
      assertFalse(notes.contains("body"));
      assertTrue(notes.contains("some"));
      // after redoing it should be the same
      parser.processNotes(node.getFirstChild(), proteinAlias);
      notes = proteinAlias.getNotes();
      assertNotNull(notes);
      assertFalse(notes.contains("body"));
      assertTrue(notes.contains("some"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseNotes3() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes4.xml");
      Species proteinAlias = new GenericProtein("id");
      proteinAlias.setNotes("text");
      parser.processNotes(node.getFirstChild(), proteinAlias);
      String notes = proteinAlias.getNotes();
      assertNotNull(notes);
      assertFalse(notes.contains("body"));
      assertTrue(notes.contains("some"));
      // after redoing it should be the same
      parser.processNotes(node.getFirstChild(), proteinAlias);
      notes = proteinAlias.getNotes();
      assertNotNull(notes);
      assertFalse(notes.contains("body"));
      assertTrue(notes.contains("some"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseCollisionNotes() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes2.xml");

      GenericProtein protein = new GenericProtein("pr_id1");
      protein.addSynonym("a1");
      protein.addFormerSymbol("a1");
      protein.setFullName("yyy");
      protein.setAbbreviation("aaaa");
      protein.setCharge(9);
      protein.setSymbol("D");
      parser.processNotes(node.getFirstChild(), protein);
      assertEquals(1, protein.getSynonyms().size());
      assertEquals(1, protein.getFormerSymbols().size());

      assertEquals(6, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidNotes1() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes_invalid_charge.xml");

      Species proteinAlias = new GenericProtein("id");
      parser.processNotes(node.getFirstChild(), proteinAlias);

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseCollisionNotes2() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes2.xml");
      Reaction reaction = new Reaction();
      reaction.addSynonym("a1");
      reaction.setAbbreviation("aaaa");
      reaction.setSubsystem("sss");
      reaction.setGeneProteinReaction("str");
      reaction.setFormula("xxx");
      reaction.setMechanicalConfidenceScore(-45);
      reaction.setUpperBound(4.5);
      reaction.setLowerBound(4.5);

      parser.processNotes(node.getFirstChild(), reaction);

      assertEquals(8, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testMoveAnnotationsFromNotes() throws Exception {
    try {
      Model model = getModelForFile("testFiles/copyingAnnotationModel.xml");

      Set<Element> elements = model.getElements();
      for (Element element : elements) {
        if (element.getName().equals("blabla")) {
          assertEquals(2, element.getMiriamData().size());
          element.getMiriamData()
              .add(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "12345"));
          element.getMiriamData()
              .add(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, "333666"));
          assertEquals(2, element.getMiriamData().size());
        }
        assertFalse(element.getNotes().contains("rdf:RDF"));
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessRdfDescription() throws Exception {
    try {
      RestAnnotationParser rap = new RestAnnotationParser();
      Species speciesAlias = new GenericProtein("id");
      speciesAlias.setNotes("begining\n" + "<rdf:RDF>" + "<rdf:Description rdf:about=\"#s2157\">\n" + "<bqmodel:is>\n"
          + "<rdf:Bag>\n" + "<rdf:li rdf:resource=\"urn:miriam:obo.chebi:CHEBI%3A17515\"/>\n" + "</rdf:Bag>\n"
          + "</bqmodel:is>\n" + "</rdf:Description>\n" + "</rdf:RDF>" + "\nend ending");
      rap.processRdfDescription(speciesAlias);
      assertEquals("begining\n\nend ending", speciesAlias.getNotes());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetMiriamDataFromAnnotation() throws Exception {
    try {
      RestAnnotationParser rap = new RestAnnotationParser();
      String response = readFile("testFiles/annotation/sampleDescription.txt");

      Set<MiriamData> set = rap.getMiriamData(response);
      for (MiriamData miriamData : set) {
        if (miriamData.getDataType().equals(MiriamType.KEGG_GENES)) {
          assertTrue(miriamData.getResource().contains("hsa"));
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetMiriamDataFromEmpty() throws Exception {
    try {
      RestAnnotationParser rap = new RestAnnotationParser();

      Set<MiriamData> set = rap.getMiriamData(null);
      assertEquals(0, set.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testCreateAnnotationString() throws Exception {
    try {
      RestAnnotationParser rap = new RestAnnotationParser();

      String former1 = "form1";
      String former2 = "form2";
      GenericProtein element = new GenericProtein("id");
      element.addFormerSymbol(former1);
      element.addFormerSymbol(former2);
      element.setCharge(1);
      String str = rap.createAnnotationString(element, true);
      assertTrue(str.contains(former1));
      assertTrue(str.contains(former2));

      for (NoteField field : NoteField.values()) {
        if (field.getClazz().isAssignableFrom(element.getClass())
            || CellDesignerElement.class.isAssignableFrom(field.getClazz())) {
          assertTrue("Export string doesn't contain info about: " + field.getCommonName(),
              str.indexOf(field.getCommonName()) >= 0);
        }

      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetFormerSymbolsFromInvalidStr() throws Exception {
    try {
      RestAnnotationParser rap = new RestAnnotationParser();

      assertEquals(0, rap.getFormerSymbols(null).size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetNotesInvalid() throws Exception {
    try {
      RestAnnotationParser rap = new RestAnnotationParser();

      Document xmlDoc = new DocumentImpl();
      Node node = xmlDoc.createElement("bla");

      rap.getNotes(node);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testProcessNotesForInvalidElement() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes4.xml");
      BioEntity bioEntity = Mockito.mock(BioEntity.class);
      Mockito.when(bioEntity.getNotes()).thenReturn("");
      parser.processNotes(node.getFirstChild(), bioEntity);
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidNotes() throws Exception {
    try {
      RestAnnotationParser parser = new RestAnnotationParser();
      String str = super.readFile("testFiles/xmlNodeTestExamples/notes5.xml");

      parser.processNotes(str, new GenericProtein("id"));

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessNotes() throws Exception {
    try {
      String newNotes = "new notes";
      String oldNotes = "old notes";
      RestAnnotationParser parser = new RestAnnotationParser();
      String str = "Description: " + newNotes + "\n" + oldNotes;
      GenericProtein protein = new GenericProtein("id");
      parser.processNotes(str, protein);

      assertTrue(protein.getNotes().contains(newNotes));
      assertTrue(protein.getNotes().contains(oldNotes));
      assertFalse(protein.getNotes().contains("Description"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
