package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ProteinTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerGenericProtein());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
			protein.setStructuralState("srt");
			List<CellDesignerModificationResidue> residues = new ArrayList<>();
			residues.add(new CellDesignerModificationResidue());

			protein.setModificationResidues(residues);
			CellDesignerProtein<?> protein2 = new CellDesignerGenericProtein(protein);
			assertNotNull(protein2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdate() {
		try {
			CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
			protein.setStructuralState("");
			CellDesignerProtein<?> protein2 = new CellDesignerGenericProtein();
			protein2.setStructuralState("srt");
			List<CellDesignerModificationResidue> residues = new ArrayList<>();
			residues.add(new CellDesignerModificationResidue());

			protein2.setModificationResidues(residues);

			protein.update(protein2);
			assertEquals(protein2.getStructuralState(), protein.getStructuralState());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddModificationResidue() {
		try {
			CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
			CellDesignerModificationResidue mr = new CellDesignerModificationResidue();
			mr.setIdModificationResidue("id1");

			CellDesignerModificationResidue mr2 = new CellDesignerModificationResidue();
			mr2.setIdModificationResidue("id1");

			protein.addModificationResidue(mr);
			assertEquals(1, protein.getModificationResidues().size());
			protein.addModificationResidue(mr2);
			assertEquals(1, protein.getModificationResidues().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetStructuralState() {
		try {
			CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
			protein.setStructuralState("str");
			protein.setStructuralState("str1");

			assertEquals("str1", protein.getStructuralState());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			CellDesignerProtein<?> protein = new CellDesignerProtein<GenericProtein>().copy();
			assertNotNull(protein);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			CellDesignerProtein<?> mock = Mockito.spy(CellDesignerProtein.class);
			mock.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
