package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

public class CellDesignerXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = Logger.getLogger(CellDesignerXmlParserTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testOpenFromInputStream() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/sample.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      assertNotNull(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromFile() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
      assertNotNull(model);
      assertEquals("sample", model.getName());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidFile() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue(e.getMessage().contains("blablablabla"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidFile2() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample2.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue(e.getMessage().contains("No celldesigner:extension node in SBML/model/annotation"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidFile3() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample10.xml"));
      fail("Exceptin expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("No type information for modification"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseAnnotationComplexAliasesConnections() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.parseAnnotationComplexAliasesConnections(model,
          super.getNodeFromXmlString(readFile("testFiles/invalid/complex_species_alias.xml")));
      fail("Exceptin expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Alias does not exist"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseAnnotationComplexAliasesConnections2() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.parseAnnotationComplexAliasesConnections(model,
          super.getNodeFromXmlString(readFile("testFiles/invalid/complex_species_alias2.xml")));
      fail("Exceptin expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfComplexSpeciesAliases"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseAnnotationAliasesConnections() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.parseAnnotationAliasesConnections(model,
          super.getNodeFromXmlString(readFile("testFiles/invalid/list_of_species_alias.xml")));
      fail("Exceptin expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Alias does not exist"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseAnnotationAliasesConnections2() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.parseAnnotationAliasesConnections(model,
          super.getNodeFromXmlString(readFile("testFiles/invalid/list_of_species_alias2.xml")));
      fail("Exceptin expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:listOfSpeciesAliases"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testEmptyModelToXml() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/empty.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      String xmlString = parser.toXml(model);
      InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is));

      assertNotNull(model2);

      ModelComparator comparator = new ModelComparator();
      assertEquals(0, comparator.compare(model, model2));

      // default compartment definition looks like (but properties order might
      // be different)
      // <compartment metaid="default" id="default" size="1" units="volume"/>

      assertTrue("There is no default compartment", xmlString.indexOf("id=\"default\"") >= 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimpleModelToXml() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/sample.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      String xmlString = parser.toXml(model);

      InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is));

      // we have to replace size of the map because of auto resizing during
      // reading
      model2.setWidth(model.getWidth());
      model2.setHeight(model.getHeight());

      assertNotNull(model2);

      ModelComparator comparator = new ModelComparator();
      assertEquals(0, comparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModifierReactionModelToXml() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/catalysis.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      String xmlString = parser.toXml(model);

      InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
      ModelComparator comparator = new ModelComparator();
      assertEquals(0, comparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAutoMapWidth() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/autoMapWidth.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      assertTrue(model.getWidth() > 700);
      assertTrue(model.getHeight() > 500);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testThrowIOException() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      InputStream fis = Mockito.mock(InputStream.class);
      when(fis.read()).thenThrow(new IOException());

      parser.createModel(new ConverterParams().inputStream(fis));
      fail("Exception expected");
    } catch (InvalidInputDataExecption e) {

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testInvalidInputFile1() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_1.xml");
      parser.createModel(new ConverterParams().inputStream(fis));
      fail("Exception expected");
    } catch (InvalidInputDataExecption e) {

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testInvalidInputFile2() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_2.xml");
      parser.createModel(new ConverterParams().inputStream(fis));
      fail("Exception expected");
    } catch (InvalidInputDataExecption e) {

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testInvalidInputFile3() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_3.xml");
      parser.createModel(new ConverterParams().inputStream(fis));
      fail("Exception expected");
    } catch (InvalidInputDataExecption e) {

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testInvalidInputFile4() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_4.xml");
      parser.createModel(new ConverterParams().inputStream(fis));
      fail("Exception expected");
    } catch (InvalidInputDataExecption e) {

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testInvalidBrokenTypeReaction() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/invalid/broken_type_reaction.xml");
      parser.createModel(new ConverterParams().inputStream(fis));
      fail("Exception expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue(e.getMessage().contains("Unknown CellDesigner class type"));
    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testToXmlAfterAnnotating() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      model.setIdModel("as");
      model.setWidth(10);
      model.setHeight(10);
      Species speciesAlias = new GenericProtein("id1");
      speciesAlias.setName("ROS");
      RestAnnotationParser rap = new RestAnnotationParser();
      rap.processNotes(
          "Symbol: ROS1\r\nName: c-ros oncogene 1 , receptor tyrosine kinase\r\nDescription: RecName: Full=Proto-oncogene tyrosine-protein kinase ROS; EC=2.7.10.1; AltName: Full=Proto-oncogene c-Ros; AltName: Full=Proto-oncogene c-Ros-1; AltName: Full=Receptor tyrosine kinase c-ros oncogene 1; AltName: Full=c-Ros receptor tyrosine kinase; Flags: Precursor;\r\nPrevious Symbols:\r\nSynonyms: ROS, MCF3",
          speciesAlias);
      model.addElement(speciesAlias);

      GenericProtein alias = new GenericProtein("id");
      alias.setName("ROS2");
      model.addElement(alias);

      SimpleMolecule speciesAlias2 = new SimpleMolecule("id2");
      speciesAlias2.setName("PDK1");
      rap.processNotes(
          "Symbol: ROS1\r\nName: c-ros oncogene 1 , receptor tyrosine kinase\r\nDescription: RecName: Full=Proto-oncogene tyrosine-protein kinase ROS; EC=2.7.10.1; AltName: Full=Proto-oncogene c-Ros; AltName: Full=Proto-oncogene c-Ros-1; AltName: Full=Receptor tyrosine kinase c-ros oncogene 1; AltName: Full=c-Ros receptor tyrosine kinase; Flags: Precursor;\r\nPrevious Symbols:\r\nSynonyms: ROS, MCF3",
          speciesAlias2);
      model.addElement(speciesAlias2);

      SimpleMolecule alias2 = new SimpleMolecule("id3");
      alias2.setName("PDK2");
      model.addElement(alias2);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xmlString = parser.toXml(model);

      InputStream is = new ByteArrayInputStream(xmlString.getBytes());
      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      ModelComparator modelComparator = new ModelComparator();

      assertEquals(0, modelComparator.compare(model, model2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelWithNullNotesToXml() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(20);
      model.setHeight(20);
      model.setNotes(null);
      model.setIdModel("id");
      String xmlString = parser.toXml(model);
      InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is));

      assertNotNull(model2);

      ModelComparator comparator = new ModelComparator();
      assertEquals(0, comparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExportXmlForEmptyModel() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(20);
      model.setHeight(20);
      model.setNotes(null);
      model.setIdModel("id");
      Species alias = new GenericProtein("a");
      alias.setName("AA");
      model.addElement(alias);

      parser.toXml(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExportXmlForEmptyModel2() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(20);
      model.setHeight(20);
      model.setNotes(null);
      model.setIdModel("id");

      parser.toXml(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExportImportModelWithSpecialCharacterInNotes() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(20);
      model.setHeight(20);
      model.setNotes(">");
      model.setIdModel("id");
      String xmlString = parser.toXml(model);
      InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is));

      assertNotNull(model2);

      ModelComparator comparator = new ModelComparator();
      assertEquals(0, comparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHomodimerSpecies() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/homodimer.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      for (Element element : model.getElements()) {
        if (element instanceof Species) {
          assertTrue("Homodimer value for class" + element.getClass() + " not upadted",
              ((Species) element).getHomodimer() > 1);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReactionWithNegativeCoords() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      FileInputStream fis = new FileInputStream("testFiles/negativeCoords.xml");
      Model model = parser.createModel(new ConverterParams().inputStream(fis));
      assertTrue(model.getWidth() >= 0);
      assertTrue(model.getHeight() >= 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testaAnnotations() throws Exception {
    try {
      CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
      Model model = cellDesignerXmlParser
          .createModel(new ConverterParams().filename("testFiles/problematic/invalidAlias.xml"));
      assertNotNull(model);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProblematicModification() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser
          .createModel(new ConverterParams().filename("testFiles/problematic/problematic_modification.xml"));
      assertNotNull(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidModifierType() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/problematic/problematic_modifier_type.xml"));
      fail("Exception expected.");
    } catch (InvalidInputDataExecption e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHtmlTagInSymbolName() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(new ConverterParams().filename("testFiles/notes_with_html_coding.xml"));

      Element p = model.getElementByElementId("sa1");

      assertEquals(">symbol<", p.getSymbol());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelBound() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();

      Model model = new ModelFullIndexed(null);
      model.setWidth(0);
      model.setHeight(0);

      Rectangle2D bound = parser.getModelBound(model);
      assertEquals(0, bound.getWidth(), EPSILON);
      assertEquals(0, bound.getHeight(), EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testModelBoundWithLine() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();

      Layer layer = new Layer();
      PolylineData line = new PolylineData(new Point2D.Double(2, 3), new Point2D.Double(20, 30));
      layer.addLayerLine(line);

      Model model = new ModelFullIndexed(null);
      model.addLayer(layer);
      model.setWidth(100);
      model.setHeight(100);

      Rectangle2D bound = parser.getModelBound(model);
      assertEquals(18, bound.getWidth(), EPSILON);
      assertEquals(27, bound.getHeight(), EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testModelBoundWithRectangle() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();

      Layer layer = new Layer();
      LayerRect rect = new LayerRect();
      rect.setX(10.0);
      rect.setY(20.0);
      rect.setWidth(30.0);
      rect.setHeight(45.0);
      layer.addLayerRect(rect);

      Model model = new ModelFullIndexed(null);
      model.addLayer(layer);
      model.setWidth(100);
      model.setHeight(100);

      Rectangle2D bound = parser.getModelBound(model);
      assertEquals(30, bound.getWidth(), EPSILON);
      assertEquals(45, bound.getHeight(), EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testModelBoundWithOval() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();

      Layer layer = new Layer();
      LayerOval oval = new LayerOval();
      oval.setX(11.0);
      oval.setY(21.0);
      oval.setWidth(31.0);
      oval.setHeight(46.0);
      layer.addLayerOval(oval);

      Model model = new ModelFullIndexed(null);
      model.addLayer(layer);
      model.setWidth(100);
      model.setHeight(100);

      Rectangle2D bound = parser.getModelBound(model);
      assertEquals(31, bound.getWidth(), EPSILON);
      assertEquals(46, bound.getHeight(), EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testModelBoundWithText() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();

      Layer layer = new Layer();
      LayerText text = new LayerText();
      text.setX(12.0);
      text.setY(31.0);
      text.setWidth(56.0);
      text.setHeight(52.0);
      layer.addLayerText(text);

      Model model = new ModelFullIndexed(null);
      model.addLayer(layer);
      model.setWidth(100);
      model.setHeight(100);

      Rectangle2D bound = parser.getModelBound(model);
      assertEquals(56, bound.getWidth(), EPSILON);
      assertEquals(52, bound.getHeight(), EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception occured: " + e.getMessage());
    }
  }

  @Test
  public void testGetters() {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      assertNotNull(parser.getCommonName());
      assertNotNull(parser.getMimeType());
      assertNotNull(parser.getFileExtension());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlWithGene() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(100);
      model.setHeight(100);
      Gene gene = new Gene("gene_id_1");
      gene.setName("geneNAME");
      model.addElement(gene);
      String xmlString = parser.toXml(model);
      assertTrue(xmlString.contains("gene_id_1"));
      assertTrue(xmlString.contains("geneNAME"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToInputString() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(100);
      model.setHeight(100);
      InputStream is = parser.exportModelToInputStream(model);
      BufferedReader in = new BufferedReader(new InputStreamReader(is));
      String str = in.readLine();
      assertNotNull(str);
      assertFalse(str.isEmpty());
      is.close();

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToFile() throws Exception {
    String filename = "tmp.xml";
    if (new File(filename).exists()) {
      new File(filename).delete();
    }
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = new ModelFullIndexed(null);
      model.setWidth(100);
      model.setHeight(100);
      parser.exportModelToFile(model, filename);

      File file = new File(filename);
      assertTrue(file.exists());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      if (new File(filename).exists()) {
        new File(filename).delete();
      }
    }
  }

  @Test
  public void testOpenFromInvalidSpeciesStateFile() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample5.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfStructuralStates"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidSpeciesStateFile2() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample6.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:listOfModifications"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidSpeciesStateFile3() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample7.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:state"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidSpeciesStateFile5() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample8.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Unknown modification state"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenFromInvalidSpeciesStateFile6() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      parser.createModel(new ConverterParams().filename("testFiles/invalid/sample9.xml"));
      fail("Exceptin expected");
    } catch (InvalidInputDataExecption e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:modification"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testOpenProblematicFile() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(new ConverterParams().filename("testFiles/problematic/problematic_notes.xml"));
      Element element = model.getElementByElementId("sa2338");
      assertFalse("Element note cannot contain head html tag", element.getNotes().contains("</head>"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testNestedComp() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(new ConverterParams().filename("testFiles/compartment/nested_compartments.xml"));

      assertNotNull(model.getElementByElementId("ca2").getCompartment());
      assertNotNull(model.getElementByElementId("sa1").getCompartment());

      String xml = parser.toXml(model);

      Model model2 = parser.createModel(
          new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));

      ModelComparator comparator = new ModelComparator();

      model.setName(null);
      assertEquals(0, comparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCompartmentWithNotes() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(new ConverterParams().filename("testFiles/compartment_with_notes.xml"));
      model.setName(null);
      assertNotNull(model);
      String str = parser.toXml(model);
      Model model2 = parser.createModel(
          new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
      assertEquals(0, new ModelComparator().compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCompartmentWithSubcompartments() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(
          new ConverterParams().filename("testFiles/compartment/nested_compartments_in_few_compartments.xml"));
      Compartment c3 = model.getElementByElementId("ca3");
      Compartment c4 = model.getElementByElementId("ca4");
      Compartment c1 = model.getElementByElementId("ca1");
      Compartment c2 = model.getElementByElementId("ca2");
      assertEquals("ca1", c3.getCompartment().getElementId());
      assertEquals("ca2", c4.getCompartment().getElementId());

      assertEquals(1, c1.getElements().size());
      assertEquals(1, c2.getElements().size());
      assertEquals(0, c3.getElements().size());
      assertEquals(0, c4.getElements().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReactionWithStochiometry() throws Exception {
    try {
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model model = parser.createModel(new ConverterParams().filename("testFiles/stochiometry.xml"));
      Reaction reaction = model.getReactionByReactionId("re1");

      assertEquals(2.0, reaction.getReactants().get(0).getStoichiometry(), Configuration.EPSILON);
      assertNull(reaction.getProducts().get(0).getStoichiometry());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelWithSelfReactionToXml() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      model.setIdModel("as");
      model.setWidth(10);
      model.setHeight(10);
      Species protein = new GenericProtein("id1");
      protein.setName("ROS");
      model.addElement(protein);

      Reaction reaction = new StateTransitionReaction();
      reaction.setIdReaction("re1");
      Product product = new Product(protein);
      product.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(20, 20)));
      reaction.addProduct(product);
      Reactant reactant = new Reactant(protein);
      reactant.setLine(new PolylineData(new Point2D.Double(20, 20), new Point2D.Double(0, 0)));
      reaction.addReactant(reactant);
      model.addReaction(reaction);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xmlString = parser.toXml(model);

      assertNotNull(xmlString);
      assertTrue(xmlString.contains("omitted"));
      assertTrue(xmlString.contains("re1"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
