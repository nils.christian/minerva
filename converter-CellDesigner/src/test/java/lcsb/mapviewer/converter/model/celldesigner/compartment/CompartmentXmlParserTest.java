package lcsb.mapviewer.converter.model.celldesigner.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.species.ProteinXmlParserTest;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;

public class CompartmentXmlParserTest extends CellDesignerTestFunctions {
	protected Logger							logger					= Logger.getLogger(ProteinXmlParserTest.class.getName());

	CompartmentXmlParser					compartmentParser;
	CellDesignerElementCollection	elements;
	String												testProteinFile	= "testFiles/xmlNodeTestExamples/compartment.xml";

	@Before
	public void setUp() throws Exception {
		elements = new CellDesignerElementCollection();
		compartmentParser = new CompartmentXmlParser(elements);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseXmlCompartment() {
		try {
			String xmlString = readFile(testProteinFile);

			Pair<String, CellDesignerCompartment> result = compartmentParser.parseXmlElement(xmlString);
			CellDesignerCompartment compartment = result.getRight();
			assertNotNull(compartment);
			assertEquals("c1", compartment.getElementId());
			assertEquals(1, compartment.getMiriamData().size());
			assertEquals("c1", compartment.getName());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected exception occured");
		}
	}

	@Test
	public void testToXml() throws Exception {
		try {
			String xmlString = readFile(testProteinFile);

			Pair<String, CellDesignerCompartment> result = compartmentParser.parseXmlElement(xmlString);
			CellDesignerCompartment compartment = result.getRight();
			Compartment alias = compartment.createModelElement("id");
			String transformedXml = compartmentParser.toXml(alias);
			assertNotNull(transformedXml);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(transformedXml));
			Document doc = builder.parse(is);
			NodeList root = doc.getChildNodes();
			assertEquals("compartment", root.item(0).getNodeName());

			Pair<String, CellDesignerCompartment> result2 = compartmentParser.parseXmlElement(compartmentParser.toXml(alias));
			CellDesignerCompartment compartment2 = result2.getRight();
			assertEquals(alias.getMiriamData().size(), compartment2.getMiriamData().size());
			assertEquals(alias.getName(), compartment2.getName());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidCompartment() throws Exception {
		try {
			compartmentParser.parseXmlElement(readFile("testFiles/invalid/compartment.xml"));
			fail("Exception expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of Compartment"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidCompartment2() throws Exception {
		try {
			compartmentParser.parseXmlElement(readFile("testFiles/invalid/compartment2.xml"));
			fail("Exception expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of Compartment/annotation"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidCompartment3() throws Exception {
		try {
			compartmentParser.parseXmlElement(readFile("testFiles/invalid/compartment3.xml"));
			fail("Exception expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of Compartment/annotation/extension"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToXmlWithParent() throws Exception {
		try {
			Compartment child = new Compartment("c_1");
			Compartment parent = new Compartment("p_1");
			child.setCompartment(parent);

			String xml = compartmentParser.toXml(child);
			assertTrue(xml.contains(parent.getElementId()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
