package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionToXmlTest {
  Logger logger = Logger.getLogger(ReactionToXmlTest.class);

  CellDesignerElementCollection elements;
  ReactionToXml toXmlParser;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    toXmlParser = new ReactionToXml(elements, false);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testInvalidModification() throws InconsistentModelException {
    Model model = new ModelFullIndexed(null);
    Species protein1 = new GenericProtein("2");
    Species protein2 = new GenericProtein("3");

    model.addElement(protein1);
    model.addElement(protein2);

    Reaction reaction = new TransportReaction();

    Reactant reactant = new Reactant(protein1);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    Modifier modifier = new Catalysis(protein1);
    modifier.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(30, 0)));

    Modifier modifier2 = new Catalysis(protein1);
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(0, 0));
    points.add(new Point2D.Double(30, 30));
    points.add(new Point2D.Double(20, 20));
    points.add(new Point2D.Double(20, 10));
    points.add(new Point2D.Double(40, 0));
    modifier2.setLine(new PolylineData(points));

    NodeOperator andOperator = new AndOperator();
    andOperator.addInput(modifier);
    andOperator.addInput(modifier2);
    points = new ArrayList<>();
    points.add(new Point2D.Double(1, 0));
    points.add(new Point2D.Double(30, 30));
    points.add(new Point2D.Double(20, 20));
    points.add(new Point2D.Double(20, 10));
    points.add(new Point2D.Double(30, 0));
    andOperator.setLine(new PolylineData(points));

    reaction.addModifier(modifier);
    reaction.addModifier(modifier2);
    reaction.addNode(andOperator);

    EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
    Logger.getRootLogger().addAppender(appender);

    try {
      toXmlParser.toXml(reaction);
      assertEquals(2, appender.getWarnings().size());
    } finally {
      Logger.getRootLogger().removeAppender(appender);
    }

  }

  @Test
  public void testModificationFromInsideComplex() throws InconsistentModelException {

    Model model = new ModelFullIndexed(null);
    Species protein1 = new GenericProtein("2");
    Species protein2 = new GenericProtein("3");

    model.addElement(protein1);
    model.addElement(protein2);

    Complex complex = new Complex("4");
    complex.addSpecies(protein1);

    protein1.setComplex(complex);

    model.addElement(complex);

    Reaction reaction = new TransportReaction();

    Reactant reactant = new Reactant(protein1);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    Modifier modifier = new Catalysis(protein1);
    modifier.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(30, 0)));

    reaction.addModifier(modifier);

    String xml = toXmlParser.toXml(reaction);

    assertTrue(xml.contains("species=\"" + elements.getElementId(complex) + "\""));

  }

  @Test
  public void testInvalidToXml() throws InconsistentModelException {

    Model model = new ModelFullIndexed(null);

    Complex complex = new Complex("4");
    complex.setComplex(complex);
    model.addElement(complex);
    
    Species protein1 = new GenericProtein("2");

    model.addElement(protein1);
    

    Reaction reaction = new TransportReaction();

    Reactant reactant = new Reactant(complex);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein1);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    try {
      toXmlParser.toXml(reaction);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {

    }

  }

  @Test
  public void testInvalidReaction() throws InconsistentModelException {

    Model model = new ModelFullIndexed(null);
    Species protein1 = new GenericProtein("2");
    Species protein2 = new GenericProtein("3");

    model.addElement(protein1);
    model.addElement(protein2);

    Reaction reaction = new Reaction();

    Reactant reactant = new Reactant(protein1);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    try {
      toXmlParser.toXml(reaction);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown reaction type"));
    }

  }

  @Test
  public void testGetEditPointsXmlStringForInvalidReaction() {
    try {
      toXmlParser.getEditPointsXmlStringForReaction(new Reaction());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unhandled reaction type"));
    }

  }

  @Test
  public void testGetConnectSchemeXmlStringForReaction() {
    try {
      toXmlParser.getConnectSchemeXmlStringForReaction(new Reaction());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown reaction type"));
    }

  }

  @Test
  public void testGetConnectSchemeXmlStringForReaction2() {
    try {
      // test internal implementation
      toXmlParser = new ReactionToXml(elements, false) {
        @Override
        String getEditPointsXmlStringForReaction(Reaction reaction) {
          return "";
        }
      };
      toXmlParser.getConnectSchemeXmlStringForReaction(new DissociationReaction());
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid editPoints string"));
    }

  }

  @Test
  public void testGetConnectSchemeXmlStringForLines() {
    // this test checks currently not used part of the code (for test coverage
    // purpose)
    String xml = toXmlParser.getConnectSchemeXmlStringForLines(
        new PolylineData[] { new PolylineData(new Point2D.Double(), new Point2D.Double()), new PolylineData() });
    assertTrue(xml.contains("arm"));

  }

}
