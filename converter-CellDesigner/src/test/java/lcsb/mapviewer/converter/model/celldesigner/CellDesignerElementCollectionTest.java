package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;

public class CellDesignerElementCollectionTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddTheSameElementTwice() {
		try {
			CellDesignerElementCollection collection = new CellDesignerElementCollection();
			CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies();
			complex.setElementId("id");
			collection.addElement(complex);
			collection.addElement(complex);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
