package lcsb.mapviewer.converter.model.celldesigner.species;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ AntisenseRnaXmlParserTest.class, //
		ComplexParserTest.class, //
		GeneXmlParserTest.class, //
		InternalModelSpeciesDataTest.class, //
		ProteinMappingTest.class, //
		ProteinXmlParserTest.class, //
		RnaXmlParserTest.class, //
		SpeciesCollectionTest.class, //
		SpeciesCollectionXmlParserTest.class, //
		SpeciesMappingTest.class, //
		SpeciesSbmlParserTest.class,//
})
public class AllSpeciesTests {

}
