package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;

public class GeneXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = Logger.getLogger(GeneXmlParserTest.class.getName());

  GeneXmlParser geneParser;
  String testGeneFile = "testFiles" + System.getProperty("file.separator") + "xmlNodeTestExamples"
      + System.getProperty("file.separator") + "gene.xml";

  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    geneParser = new GeneXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() {
    try {
      String xmlString = readFile(testGeneFile);
      Pair<String, CellDesignerGene> result = geneParser.parseXmlElement(xmlString);
      CellDesignerGene gene = result.getRight();
      assertEquals("gn3", result.getLeft());
      assertEquals("BCL6", gene.getName());
      assertTrue(gene.getNotes().contains("B-cell CLL/lymphoma 6"));
    } catch (Exception e) {
      e.printStackTrace();
      fail("Unexpected exception occured");
    }
  }

  @Test
  public void testToXml() throws Exception {
    try {
      String xmlString = readFile(testGeneFile);
      Pair<String, CellDesignerGene> result = geneParser.parseXmlElement(xmlString);
      CellDesignerGene gene = result.getRight();
      String transformedXml = geneParser.toXml(gene.createModelElement());
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("celldesigner:gene", root.item(0).getNodeName());

      Pair<String, CellDesignerGene> result2 = geneParser.parseXmlElement(geneParser.toXml(gene.createModelElement()));
      CellDesignerGene gene2 = result2.getRight();
      assertEquals(gene.getName(), gene2.getName());
      assertTrue(gene2.getNotes().trim().contains(gene.getNotes().trim()));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParsePhosphorylatedGene() throws Exception {
    try {
      Model model = getModelForFile("testFiles/problematic/phosphorylated_gene.xml");
      Gene gene = (Gene) model.getElementByElementId("sa1");
      assertEquals(1, gene.getModificationResidues().size());
      ModificationSite residue = (ModificationSite) gene.getModificationResidues().get(0);
      assertEquals(ModificationState.PHOSPHORYLATED, residue.getState());
      assertEquals("some name", residue.getName());

      // check xml transformation
      String xml = geneParser.toXml(gene);
      assertNotNull(xml);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidGeneInput() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_gene_1.xml");
      geneParser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidGeneInput2() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_gene_2.xml");
      geneParser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidModificationResidue() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_modification_residue.xml");
      geneParser.getModificationResidue(getNodeFromXmlString(xmlString));
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModificationResidueToXml() throws Exception {
    try {
      Gene protein = new Gene("id");
      protein.setX(10);
      protein.setY(10);
      protein.setWidth(10);
      protein.setHeight(10);
      ModificationSite mr = new ModificationSite();
      mr.setIdModificationResidue("i");
      mr.setName("a");
      mr.setPosition(new Point2D.Double(3.0, 2.0));
      mr.setSpecies(protein);
      String xmlString = geneParser.toXml(mr);
      assertNotNull(xmlString);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
