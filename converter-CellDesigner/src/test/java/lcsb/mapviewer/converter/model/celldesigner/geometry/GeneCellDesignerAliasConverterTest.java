package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertNotNull;

import java.awt.geom.PathIterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Gene;

public class GeneCellDesignerAliasConverterTest {

  GeneCellDesignerAliasConverter converter = new GeneCellDesignerAliasConverter(false);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetInvalidAliasPointCoordinates() {
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(10);
    alias.setHeight(10);
    assertNotNull(converter.getPointCoordinates(alias, null));
  }

  @Test
  public void testGetPath() {
    Model model = new ModelFullIndexed(null);
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(10);
    alias.setHeight(10);

    alias.setModel(model);
    PathIterator path = converter.getBoundPathIterator(alias);

    assertNotNull(path);
  }

  @Test
  public void testGetPath2() {
    GeneCellDesignerAliasConverter converter = new GeneCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(10);
    alias.setHeight(10);

    alias.setModel(model);
    PathIterator path = converter.getBoundPathIterator(alias);

    assertNotNull(path);
  }

}
