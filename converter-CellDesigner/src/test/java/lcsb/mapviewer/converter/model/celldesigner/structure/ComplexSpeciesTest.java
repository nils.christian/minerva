package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.AntisenseRna;

public class ComplexSpeciesTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerComplexSpecies());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			CellDesignerComplexSpecies degraded = new CellDesignerComplexSpecies(new CellDesignerSpecies<AntisenseRna>());
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
			species.addElement(new CellDesignerSpecies<AntisenseRna>());
			CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies(species);

			Set<CellDesignerElement<?>> elements = new HashSet<>();
			complex.setElements(elements);
			assertEquals(elements, complex.getElements());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddElement() {
		try {
			CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
			species.addElement(new CellDesignerSpecies<AntisenseRna>());
			species.addElement(new CellDesignerSpecies<AntisenseRna>());
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetStructuralState() {
		try {
			CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
			species.setStructuralState("a");
			species.setStructuralState("b");
			assertEquals("b", species.getStructuralState());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAllSimpleChildren() {
		try {
			CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
			species.addElement(new CellDesignerSpecies<AntisenseRna>());
			CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies("a");
			complex.addElement(new CellDesignerSpecies<AntisenseRna>("s"));
			complex.addElement(new CellDesignerSpecies<AntisenseRna>("d"));
			species.addElement(complex);
			assertEquals(3, species.getAllSimpleChildren().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			CellDesignerComplexSpecies degraded = new CellDesignerComplexSpecies().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			CellDesignerComplexSpecies complex = Mockito.spy(CellDesignerComplexSpecies.class);
			complex.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCreateInvalidElement() {
		try {
			CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies();
			complex.addElement(new CellDesignerGene());
			complex.createModelElement();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
