package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class ComplexParserTests extends CellDesignerTestFunctions {
  Logger logger = Logger.getLogger(ComplexParserTests.class);

  ElementUtils eu = new ElementUtils();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseFunctions() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/function.xml");

      assertEquals(1, model.getFunctions().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseParameters() throws Exception {
    try {
      Model model = getModelForFile("testFiles/parameter.xml");

      assertEquals(1, model.getParameters().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseElementKinetics() throws Exception {
    try {
      Model model = getModelForFile("testFiles/elements_with_kinetic_data.xml");

      GenericProtein protein1 = model.getElementByElementId("sa1");
      assertEquals(2.5, protein1.getInitialAmount(), Configuration.EPSILON);
      assertTrue("hasOnlySubstance property is not parsed properly", protein1.hasOnlySubstanceUnits());
      assertTrue("boundaryCondition property is not parsed properly", protein1.isBoundaryCondition());
      assertTrue("constant property is not parsed properly", protein1.isConstant());
      assertEquals(SbmlUnitType.GRAM, protein1.getSubstanceUnits());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseAndSerializeKineticsElements() throws Exception {
    try {
      Model model = getModelForFile("testFiles/elements_with_kinetic_data.xml");
      model.setName(null);

      String xml = new CellDesignerXmlParser().toXml(model);
      ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
      Model model2 = new CellDesignerXmlParser()
          .createModel(new ConverterParams().sizeAutoAdjust(false).inputStream(bais));

      ModelComparator modelComparator = new ModelComparator();
      assertEquals(0, modelComparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseAndSerializeKineticsReaction() throws Exception {
    try {
      Model model = getModelForFile("testFiles/reactions/kinetics.xml");
      model.setName(null);

      String xml = new CellDesignerXmlParser().toXml(model);
      ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
      Model model2 = new CellDesignerXmlParser()
          .createModel(new ConverterParams().sizeAutoAdjust(false).inputStream(bais));

      ModelComparator modelComparator = new ModelComparator();
      assertEquals(0, modelComparator.compare(model, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseUnits() throws Exception {
    try {
      Model model = getModelForFile("testFiles/unit.xml");

      assertEquals(1, model.getUnits().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseCompartmens() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/reactions/centeredAnchorInModifier.xml");

      assertEquals(0, model.getCompartments().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseBubbles() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/bubbles.xml");

      Element species = model.getElementByElementId("sa6");
      assertNotNull(species);
      Protein protein = (Protein) species;
      assertTrue(protein.getModificationResidues().size() > 0);
      assertNotNull(protein.getStructuralState());
      assertFalse(protein.getStructuralState().equals(""));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseCompartmensRelation() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/problematic/elements_in_compartments.xml");

      List<Element> aliases = new ArrayList<>();
      aliases.add(model.getElementByElementId("csa3"));
      aliases.add(model.getElementByElementId("csa4"));
      aliases.add(model.getElementByElementId("csa5"));
      aliases.add(model.getElementByElementId("csa6"));
      aliases.add(model.getElementByElementId("sa3"));
      aliases.add(model.getElementByElementId("sa5"));
      aliases.add(model.getElementByElementId("sa6"));
      aliases.add(model.getElementByElementId("sa7"));
      aliases.add(model.getElementByElementId("sa8"));
      aliases.add(model.getElementByElementId("sa9"));
      aliases.add(model.getElementByElementId("sa10"));
      aliases.add(model.getElementByElementId("sa11"));
      aliases.add(model.getElementByElementId("sa12"));
      aliases.add(model.getElementByElementId("sa13"));
      aliases.add(model.getElementByElementId("sa14"));
      aliases.add(model.getElementByElementId("sa15"));
      aliases.add(model.getElementByElementId("sa16"));
      aliases.add(model.getElementByElementId("sa17"));
      aliases.add(model.getElementByElementId("sa18"));
      aliases.add(model.getElementByElementId("sa19"));
      aliases.add(model.getElementByElementId("sa20"));
      for (Element alias : aliases) {
        assertNotNull(eu.getElementTag(alias) + " does not contain info about compartment", alias.getCompartment());
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseProteins() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/problematic/acetyled_protein.xml");

      Set<ModificationState> residues = new HashSet<>();
      for (Element element : model.getElements()) {
        if (element instanceof Species) {
          Protein protein = (Protein) element;
          for (ModificationResidue mr : protein.getModificationResidues()) {
            if (mr instanceof AbstractSiteModification) {
              residues.add(((AbstractSiteModification) mr).getState());
            }
          }
        }
      }
      // we have a protein which is acetylated and not acetylated so two types
      // of residues
      assertEquals(2, residues.size());

      AbstractSiteModification modification = (AbstractSiteModification) ((Protein) model.getElementByElementId("sa2"))
          .getModificationResidues().get(0);
      assertEquals(ModificationState.ACETYLATED, modification.getState());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testNotesComplexesInNested() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/includedSpecies.xml");
      Element species = model.getElementByElementId("sa1");
      assertNotNull(species);
      assertNotNull(species.getNotes());
      assertTrue("Wrong notes: " + species.getNotes(), species.getNotes().contains("hello world"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHypotheticalComplex() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/problematic/hypothetical_complex.xml");
      Complex species = (Complex) model.getElementByElementId("csa1");
      assertTrue(species.isHypothetical());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProblematicAcetylation() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/problematic/problematic_acetylation.xml");
      Protein p1 = (Protein) model.getElementByElementId("sa73");
      Protein p2 = (Protein) model.getElementByElementId("sa27");
      assertEquals(ModificationState.ACETYLATED, ((Residue) p1.getModificationResidues().get(0)).getState());
      assertFalse(ModificationState.ACETYLATED.equals(((Residue) p2.getModificationResidues().get(0)).getState()));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDuplicateMiriam() throws Exception {
    Model model;
    try {
      model = getModelForFile("testFiles/problematic/duplicated_miriam.xml");
      Protein p1 = (Protein) model.getElementByElementId("sa1");
      Set<String> ids = new HashSet<>();

      assertTrue(p1.getMiriamData().size() > 0);
      for (MiriamData md : p1.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.PUBMED)) {
          assertFalse("Protein contains double pubmed annotation for pubmed id: " + md.getResource(),
              ids.contains(md.getResource()));
          ids.add(md.getResource());
        }
      }
      assertTrue(ids.size() > 0);
      assertEquals(2, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testKappaInDescription() throws Exception {
    try {
      Model model = getModelForFile("testFiles/problematic/kappa_example.xml");

      Element species = model.getElementByElementId("sa1");

      assertFalse(species.getName().toLowerCase().contains("kappa"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testMissingXmlNodes() throws Exception {
    try {
      Model model = getModelForFile("testFiles/missing_xml_nodes.xml");

      assertTrue(((Species) model.getElementByElementId("sa3")).isHypothetical());
      assertTrue(((Species) model.getElementByElementId("sa4")).isHypothetical());
      assertTrue(((Species) model.getElementByElementId("sa5")).isHypothetical());

      assertFalse(((Species) model.getElementByElementId("sa12")).isHypothetical());
      assertFalse(((Species) model.getElementByElementId("sa13")).isHypothetical());
      assertFalse(((Species) model.getElementByElementId("sa14")).isHypothetical());
      assertFalse(((Species) model.getElementByElementId("sa15")).isHypothetical());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParsingSpecialCharactersInName() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      model.setIdModel("as");
      model.setWidth(100);
      model.setHeight(100);
      GenericProtein alias = new GenericProtein("aid");
      alias.setName("name & no-name");
      model.addElement(alias);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String string = parser.toXml(model);

      InputStream stream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));

      ConverterParams params = new ConverterParams().inputStream(stream).sizeAutoAdjust(false);

      Model model2 = parser.createModel(params);

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, model2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParsingEndLineInName() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      model.setIdModel("as");
      model.setWidth(100);
      model.setHeight(100);
      GenericProtein alias = new GenericProtein("aid");
      alias.setName("name\rno-name");
      model.addElement(alias);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String string = parser.toXml(model);

      InputStream stream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));

      ConverterParams params = new ConverterParams().inputStream(stream).sizeAutoAdjust(false);

      Model model2 = parser.createModel(params);

      ModelComparator comparator = new ModelComparator();

      assertEquals(0, comparator.compare(model, model2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testWarningInParser() throws Exception {
    try {
      getModelForFile("testFiles/problematic/invalid_elements_name.xml");
      assertTrue(getWarnings().size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
