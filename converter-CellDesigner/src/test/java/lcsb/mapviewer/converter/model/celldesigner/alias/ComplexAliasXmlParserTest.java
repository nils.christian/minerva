package lcsb.mapviewer.converter.model.celldesigner.alias;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;

public class ComplexAliasXmlParserTest extends CellDesignerTestFunctions {
  Model model;
  private ComplexAliasXmlParser parser;
  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);
    elements = new CellDesignerElementCollection();
    parser = new ComplexAliasXmlParser(elements, model);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testToXml() throws Exception {
    try {
      Complex complex = createComplex();
      String xmlString = parser.toXml(complex);
      assertNotNull(xmlString);

      elements.addElement(new CellDesignerComplexSpecies(elements.getElementId(complex)));
      Complex complexFromXml = parser.parseXmlAlias(xmlString);
      assertEquals(0, getWarnings().size());

      assertEquals(complex.getElementId(), complexFromXml.getElementId());
      assertNotNull(complex.getName());
      assertEquals(complex.getFontSize(), complexFromXml.getFontSize(), Configuration.EPSILON);
      assertEquals(complex.getHeight(), complexFromXml.getHeight(), Configuration.EPSILON);
      assertEquals(complex.getWidth(), complexFromXml.getWidth(), Configuration.EPSILON);
      assertEquals(complex.getX(), complexFromXml.getX(), Configuration.EPSILON);
      assertEquals(complex.getY(), complexFromXml.getY(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Complex createComplex() {
    Complex complex = new Complex("id");
    complex.setFontSize(13.5);
    complex.setHeight(90);
    complex.setWidth(80);
    complex.setName("112");
    complex.setX(32);
    complex.setY(42);
    return complex;
  }

  @Test
  public void testParseXmlAliasNode() throws Exception {
    try {
      elements.addElement(new CellDesignerComplexSpecies("s2597"));

      String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_complex_alias.xml");
      Complex alias = (Complex) parser.parseXmlAlias(xmlString);
      assertEquals(0, getWarnings().size());
      assertEquals(false, alias.getActivity());
      assertEquals("csa1", alias.getElementId());
      assertEquals(12.0, alias.getFontSize(), Configuration.EPSILON);
      assertEquals(120.0, alias.getHeight(), Configuration.EPSILON);
      assertEquals(100.0, alias.getWidth(), Configuration.EPSILON);
      assertEquals(744.0, alias.getX(), Configuration.EPSILON);
      assertEquals(0.0, alias.getY(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
