package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;

public class CellDesignerLineTransformationTest {
	Logger												 logger							 = Logger.getLogger(CellDesignerLineTransformationTest.class);

	CellDesignerLineTransformation lineTransformation	 = new CellDesignerLineTransformation();
	PointTransformation						 pointTransformation = new PointTransformation();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetPointsFromLine() {
		try {
			Point2D startPoint = new Point2D.Double(1, 7);
			Point2D endPoint = new Point2D.Double(3, 5);

			List<Point2D> points = new ArrayList<Point2D>();
			points.add(new Point2D.Double(1.5, 2.5));
			points.add(new Point2D.Double(12, 13));
			points.add(new Point2D.Double(30, 40));

			PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

			List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
			assertNotNull(newPoints);

			assertEquals(points.size(), newPoints.size());

			for (int i = 0; i < points.size(); i++) {
				double dist = points.get(i).distance(newPoints.get(i));
				assertTrue("Distance to big: " + dist + " for points " + points.get(i) + " and " + newPoints.get(i), dist < 1e-6);
				assertTrue(pointTransformation.isValidPoint(newPoints.get(i)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception occured: " + e.getMessage());
		}
	}

	@Test
	public void testGetProblematicPointsFromLine() {
		try {
			Point2D startPoint = new Point2D.Double(1, 7);
			Point2D endPoint = new Point2D.Double(1, 5);

			List<Point2D> points = new ArrayList<Point2D>();
			points.add(new Point2D.Double(1.5, 2.5));
			points.add(new Point2D.Double(12, 13));
			points.add(new Point2D.Double(30, 40));

			PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

			List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
			assertNotNull(newPoints);

			assertEquals(points.size(), newPoints.size());

			for (int i = 0; i < points.size(); i++) {
				assertTrue("Invalid point after transformation: " + newPoints.get(i), pointTransformation.isValidPoint(newPoints.get(i)));
				double dist = points.get(i).distance(newPoints.get(i));
				assertTrue("Distance to big: " + dist + " for points " + points.get(i) + " and " + newPoints.get(i), dist < 1e-6);
				assertTrue(pointTransformation.isValidPoint(newPoints.get(i)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetProblematicPointsFromLine2() {
		try {
			Point2D startPoint = new Point2D.Double(5, 1);
			Point2D endPoint = new Point2D.Double(70, 1);

			List<Point2D> points = new ArrayList<Point2D>();
			points.add(new Point2D.Double(1.5, 2.5));
			points.add(new Point2D.Double(12, 13));
			points.add(new Point2D.Double(-12, -13));
			points.add(new Point2D.Double(30, 40));

			PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

			List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
			assertNotNull(newPoints);

			assertEquals(points.size(), newPoints.size());

			for (int i = 0; i < points.size(); i++) {
				assertTrue("Invalid point after transformation: " + newPoints.get(i), pointTransformation.isValidPoint(newPoints.get(i)));
				double dist = points.get(i).distance(newPoints.get(i));
				assertTrue("Distance to big: " + dist + " for points " + points.get(i) + " and " + newPoints.get(i), dist < 1e-6);
				assertTrue(pointTransformation.isValidPoint(newPoints.get(i)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLinePointsFromInvalidPoints() {
		try {
			Point2D startPoint = new Point2D.Double(Double.POSITIVE_INFINITY, 1);
			Point2D endPoint = new Point2D.Double(70, 1);

			lineTransformation.getLinePointsFromPoints(startPoint, endPoint, new ArrayList<>());
			fail("Excepiton expceted");

		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid start point"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLinePointsFromInvalidPoints2() {
		try {
			Point2D endPoint = new Point2D.Double(Double.POSITIVE_INFINITY, 1);
			Point2D startPoint = new Point2D.Double(70, 1);

			lineTransformation.getLinePointsFromPoints(startPoint, endPoint, new ArrayList<>());
			fail("Excepiton expceted");

		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid end point"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLinePointsFromInvalidPoints3() {
		try {
			Point2D endPoint = new Point2D.Double(70, 2);
			Point2D startPoint = new Point2D.Double(70, 1);
			List<Point2D> list = new ArrayList<>();
			list.add(new Point2D.Double(Double.POSITIVE_INFINITY, 1));

			lineTransformation.getLinePointsFromPoints(startPoint, endPoint, list);
			fail("Excepiton expceted");

		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid point"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetPointsFromInvalidLine() {
		try {

			lineTransformation.getPointsFromLine(null);
			fail("Excepiton expceted");

		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Line cannot be null"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetPointsFromInvalidLine2() {
		try {
			lineTransformation.getPointsFromLine(new PolylineData());
			fail("Excepiton expceted");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid line passed as an argument"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetPointsFromInvalidLine3() {
		try {
			lineTransformation.getPointsFromLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
			fail("Excepiton expceted");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("First and last point in the line must differ but found"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
