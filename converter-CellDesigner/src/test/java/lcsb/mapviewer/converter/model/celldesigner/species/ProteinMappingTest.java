package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.model.map.species.TruncatedProtein;

public class ProteinMappingTest {
	Logger logger = Logger.getLogger(ProteinMappingTest.class);

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAllValues() {
		for (ProteinMapping value : ProteinMapping.values()) {
			assertNotNull(ProteinMapping.valueOf(value.toString()));
		}
	}

	@Test
	public void testCreateInvalidProteinImpl() throws Exception {
		// artificial implementation of Protein that is invalid
		class InvalidProtein extends CellDesignerProtein<TruncatedProtein> {
			private static final long serialVersionUID = 1L;

			@SuppressWarnings("unused")
			public InvalidProtein() {
				throw new NotImplementedException();
			}
		}

		// mopdify one of the elements of ProteinMapping so it will have invalid
		// implementation
		ProteinMapping typeToModify = ProteinMapping.GENERIC_PROTEIN;
		Class<? extends CellDesignerProtein<?>> clazz = typeToModify.getCellDesignerClazz();

		try {
			Field field = typeToModify.getClass().getDeclaredField("cellDesignerClazz");
			field.setAccessible(true);
			field.set(typeToModify, InvalidProtein.class);

			// and check if we catch properly information about problematic
			// implementation
			typeToModify.createProtein();
			fail("Exception expected");
		} catch (InvalidStateException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			// restore correct values for the modified type (if not then other test
			// might fail...)
			Field field = typeToModify.getClass().getDeclaredField("cellDesignerClazz");
			field.setAccessible(true);
			field.set(typeToModify, clazz);
		}

	}

}
