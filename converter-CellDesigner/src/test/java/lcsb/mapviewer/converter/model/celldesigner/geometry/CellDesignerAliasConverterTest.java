package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Species;

public class CellDesignerAliasConverterTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructorWithInvalidArg() {
		try {
			new CellDesignerAliasConverter(null, false);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("element cannot be null"));
		}
	}

	@Test
	public void testConstructorWithInvalidArg2() {
		try {
			Species alias = Mockito.mock(Species.class);
			new CellDesignerAliasConverter(alias, false);
			fail("Exception expected");
		} catch (NotImplementedException e) {
			assertTrue(e.getMessage().contains("Unknown converter for class"));
		}
	}

}
