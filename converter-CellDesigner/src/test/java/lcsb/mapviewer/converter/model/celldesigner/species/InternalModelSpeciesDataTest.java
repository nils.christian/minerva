package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class InternalModelSpeciesDataTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetters() {
		InternalModelSpeciesData data = new InternalModelSpeciesData();
		assertNotNull(data.getAll());
		assertNotNull(data.getAntisenseRnas());
		assertNotNull(data.getGenes());
		assertNotNull(data.getProteins());
		assertNotNull(data.getRnas());
	}

	@Test
	public void testUpdateUnknownSpecies() {
		InternalModelSpeciesData data = new InternalModelSpeciesData();
		try {
			data.updateSpecies(new CellDesignerSpecies<GenericProtein>(), null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Unknown species type"));
		}
	}

}
