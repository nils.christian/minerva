package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.alias.ComplexAliasXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;

public class ComplexParserTest extends CellDesignerTestFunctions {
	Logger												logger									= Logger.getLogger(ComplexParserTest.class);

	String												testSpeciesId						= "s3";
	String												testAliasId							= "sa3";
	String												testCompartmentAliasId	= "ca3";
	String												testCompartmentId				= "c3";
	String												testCompartmentAliasId2	= "ca4";
	String												testCompartmentId2			= "c4";

	CellDesignerElementCollection	elements;

	@Before
	public void setUp() throws Exception {
		elements = new CellDesignerElementCollection();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testComplexState() throws Exception {
		try {
			Model model = getModelForFile("testFiles/problematic/complex_with_state.xml");
			Complex complex = (Complex) model.getElementByElementId("csa1");
			assertEquals("test state", complex.getStructuralState());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidInputNode() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias.xml"));
			fail("Exceptione expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("No species"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidInputNode2() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
			element.setElementId("s3");
			elements.addElement(element);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias2.xml"));
			fail("Exceptione expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("celldesigner:bla"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidInputNode3() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
			element.setElementId("s3");
			elements.addElement(element);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias3.xml"));
			fail("Exceptione expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("unusual"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidInputNode4() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
			element.setElementId("s3");
			elements.addElement(element);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias4.xml"));
			fail("Exceptione expected");

		} catch (NotImplementedException e) {
			assertTrue(e.getMessage().contains("unkown state"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidInputNode5() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
			element.setElementId("s3");
			elements.addElement(element);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias5.xml"));
			fail("Exceptione expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("CompartmentAlias does not exist"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToXmlWithParent() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);

			Compartment compartment = new Compartment(testCompartmentAliasId);

			Complex alias = new Complex(testAliasId);

			alias.setCompartment(compartment);

			model.addElement(alias);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			String xml = parser.toXml(alias);

			assertTrue(xml.contains(testAliasId));
			assertTrue(xml.contains(testCompartmentAliasId));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToXmlWithCompartment() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);

			Compartment ca = new Compartment(testCompartmentAliasId);

			Complex alias = new Complex(testAliasId);

			alias.setCompartment(ca);

			model.addElement(alias);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			String xml = parser.toXml(alias);

			assertTrue(xml.contains(testAliasId));
			assertTrue(xml.contains(testCompartmentAliasId));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToXmlWithUnknownCompartment() throws Exception {
		try {
			Model model = Mockito.mock(ModelFullIndexed.class);
			ModelData md = new ModelData();
			md.setModel(model);

			Compartment ca2 = new Compartment(testCompartmentAliasId2);
			ca2.setX(6);
			ca2.setY(6);
			ca2.setWidth(190);
			ca2.setHeight(190);

			Compartment ca = new Compartment(testCompartmentAliasId);
			ca.setX(5);
			ca.setY(5);
			ca.setWidth(200);
			ca.setHeight(200);

			List<Compartment> list = new ArrayList<>();
			list.add(ca);
			list.add(ca2);

			// ensure that we return list (firts bigger compartment, then smaller)
			when(model.getCompartments()).thenReturn(list);
			when(model.getModelData()).thenReturn(md);

			Complex alias = new Complex(testAliasId);
			alias.setX(10);
			alias.setY(10);
			alias.setWidth(100);
			alias.setHeight(100);
			alias.setModel(model);

			model.addElement(alias);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			String xml = parser.toXml(alias);

			assertTrue(xml.contains(testAliasId));
			assertTrue(xml.contains(testCompartmentAliasId2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToXmlWithComplexParent() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);

			Complex complex = new Complex(testCompartmentAliasId);

			Complex alias = new Complex(testAliasId);

			alias.setComplex(complex);

			model.addElement(alias);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			String xml = parser.toXml(alias);

			assertTrue(xml.contains(testAliasId));
			assertTrue(xml.contains(testCompartmentAliasId));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToXmlWithActiveComplex() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);

			Complex alias = new Complex(testAliasId);

			alias.setActivity(true);

			model.addElement(alias);
			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			String xml = parser.toXml(alias);

			assertTrue(xml.contains(testAliasId));
			assertTrue(xml.contains("<celldesigner:activity>active</celldesigner:activity>"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddInvalidReference() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
			element.setElementId("s2");
			elements.addElement(element);

			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			Complex child = parser.parseXmlAlias(readFile("testFiles/xmlNodeTestExamples/cd_complex_alias_with_parent.xml"));

			parser.addReference(child);
			fail("Exception expected");

		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Parent complex alias does not exist"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseWithEmptyState() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			CellDesignerComplexSpecies element = new CellDesignerComplexSpecies("s2597");
			elements.addElement(element);

			ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

			Complex child = parser.parseXmlAlias(readFile("testFiles/xmlNodeTestExamples/cd_complex_alias_with_empty_state.xml"));

			assertNotNull(child);
			assertNull(child.getStateLabel());
			assertNull(child.getStatePrefix());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
