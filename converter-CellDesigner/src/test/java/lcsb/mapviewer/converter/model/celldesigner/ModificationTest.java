package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

public class ModificationTest extends CellDesignerTestFunctions {
  Logger logger = Logger.getLogger(ModificationTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testBindingRegion() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/protein_with_binding_region.xml");
      Protein protein = model.getElementByElementId("sa1");
      assertEquals(1, protein.getModificationResidues().size());
      ModificationResidue residue = protein.getModificationResidues().get(0);

      assertTrue(residue instanceof BindingRegion);
      BindingRegion bindingRegion = (BindingRegion) residue;
      assertEquals(bindingRegion.getPosition().getX(), protein.getX(), Configuration.EPSILON);
      assertTrue(bindingRegion.getWidth() < bindingRegion.getHeight());

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private void testXmlSerialization(Model model)
      throws InconsistentModelException, UnsupportedEncodingException, InvalidInputDataExecption {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xml = parser.toXml(model);
    InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
    Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

    model.setName(null);
    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testRegulatoryRegion() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/gene_with_regulatory_region.xml");
      Gene gene = model.getElementByElementId("sa1");
      assertEquals(1, gene.getModificationResidues().size());

      ModificationResidue residue = gene.getModificationResidues().get(0);
      assertTrue(residue instanceof RegulatoryRegion);
      RegulatoryRegion bindingRegion = (RegulatoryRegion) residue;
      assertEquals(bindingRegion.getPosition().getY(), gene.getY(), Configuration.EPSILON);
      assertTrue(bindingRegion.getWidth() > bindingRegion.getHeight());

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGeneCodingRegion() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/gene_with_coding_region.xml");
      Gene gene = model.getElementByElementId("sa1");
      assertEquals(1, gene.getModificationResidues().size());

      ModificationResidue residue = gene.getModificationResidues().get(0);
      assertTrue(residue instanceof CodingRegion);
      CodingRegion bindingRegion = (CodingRegion) residue;
      assertEquals(bindingRegion.getPosition().getY(), gene.getY(), Configuration.EPSILON);
      assertTrue(bindingRegion.getWidth() > bindingRegion.getHeight());

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGeneModificationSite() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/gene_with_modification_site.xml");
      Gene gene = model.getElementByElementId("sa1");
      assertEquals(1, gene.getModificationResidues().size());

      ModificationResidue residue = gene.getModificationResidues().get(0);
      assertTrue(residue instanceof ModificationSite);
      ModificationSite bindingRegion = (ModificationSite) residue;
      assertEquals(bindingRegion.getPosition().getY(), gene.getY(), Configuration.EPSILON);

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRegulatoryTranscriptionSiteRight() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/gene_with_transcription_site_right.xml");
      Gene gene = model.getElementByElementId("sa1");
      assertEquals(1, gene.getModificationResidues().size());
      ModificationResidue residue = gene.getModificationResidues().get(0);

      assertTrue(residue instanceof TranscriptionSite);
      TranscriptionSite transcriptionSite = (TranscriptionSite) residue;
      assertEquals(transcriptionSite.getPosition().getY(), gene.getY(), Configuration.EPSILON);
      assertEquals("RIGHT", transcriptionSite.getDirection());
      assertTrue(transcriptionSite.getActive());

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRegulatoryTranscriptionSiteLeft() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/gene_with_transcription_site_left.xml");
      Gene gene = model.getElementByElementId("sa1");
      assertEquals(1, gene.getModificationResidues().size());
      ModificationResidue residue = gene.getModificationResidues().get(0);

      assertTrue(residue instanceof TranscriptionSite);
      TranscriptionSite transcriptionSite = (TranscriptionSite) residue;
      assertEquals(transcriptionSite.getPosition().getY(), gene.getY(), Configuration.EPSILON);
      assertEquals("LEFT", transcriptionSite.getDirection());
      assertFalse(transcriptionSite.getActive());

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProtinWithResidues() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/protein_with_residues.xml");
      Protein protein = (Protein) model.getElementByElementId("sa1");
      assertEquals(14, protein.getModificationResidues().size());

      ModificationResidue residue = protein.getModificationResidues().get(0);

      assertTrue(residue instanceof Residue);

      testXmlSerialization(model);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRnaWithRegion() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/rna_with_region.xml");
      Rna rna = model.getElementByElementId("sa1");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof CodingRegion);

      rna = model.getElementByElementId("sa2");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof ProteinBindingDomain);

      rna = model.getElementByElementId("sa3");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof ModificationSite);

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAntisenseRnaWithRegion() throws Exception {
    try {
      Model model = getModelForFile("testFiles/modifications/antisense_rna_with_region.xml");
      AntisenseRna rna = model.getElementByElementId("sa1");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof CodingRegion);

      rna = model.getElementByElementId("sa2");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof ProteinBindingDomain);

      rna = model.getElementByElementId("sa3");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof ModificationSite);

      rna = model.getElementByElementId("sa4");
      assertEquals(1, rna.getRegions().size());
      assertTrue(rna.getRegions().get(0) instanceof ModificationSite);
      ModificationSite modificationSite = (ModificationSite) rna.getRegions().get(0);
      assertEquals(ModificationState.PHOSPHORYLATED, modificationSite.getState());

      testXmlSerialization(model);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPhosporylatedProteinToXml() throws Exception {
    try {
      Model model = getModelForFile("testFiles/problematic/phosphorylated_protein.xml");

      testXmlSerialization(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
