package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CellDesignerModificationResidueTest.class, //
})
public class AllFieldsTests {

}
