package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Rna;

public class SpeciesTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new CellDesignerSpecies<GenericProtein>());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      String elementId = "51";
      Double initialAmount = 54.0;
      Double initialConcentration = 58.0;
      Integer charge = 59;
      Boolean onlySubstanceUnits = true;

      String trueStr = "true";
      assertFalse(species.isHypothetical());
      species.setHypothetical(trueStr);
      assertTrue(species.isHypothetical());

      species.setElementId(elementId);
      assertEquals(elementId, species.getElementId());

      species.setInitialAmount(initialAmount);
      assertEquals(initialAmount, species.getInitialAmount());

      species.setOnlySubstanceUnits(onlySubstanceUnits);
      assertEquals(onlySubstanceUnits, species.getOnlySubstanceUnits());

      species.setInitialConcentration(initialConcentration);
      assertEquals(initialConcentration, species.getInitialConcentration());

      species.setCharge(charge);

      assertEquals(charge, species.getCharge());

      species.setElementId("");
      assertEquals("", species.getElementId());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      String id = "as_id";
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>(id);
      assertEquals(id, species.getElementId());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      String id = "as_id";
      CellDesignerSpecies<GenericProtein> species = new CellDesignerSpecies<>(id);
      species.setBoundaryCondition(true);
      species.setConstant(true);
      species.setSubstanceUnits(SbmlUnitType.COULUMB);
      CellDesignerSpecies<?> copy = species.copy();
      assertEquals(id, species.getElementId());
      assertEquals(Boolean.TRUE, copy.isBoundaryCondition());
      assertEquals(Boolean.TRUE, copy.isConstant());
      assertEquals(SbmlUnitType.COULUMB, copy.getSubstanceUnit());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetId() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setElementId("");
      species.setElementId("xx");
      try {
        species.setElementId("yy");
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate1() {
    try {
      int warningsCount = getWarnings().size();
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setName("A");
      species.setNotes("XXX");
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.setName("B");
      species.update(species2);
      int warningsCount2 = getWarnings().size();
      assertEquals(1, warningsCount2 - warningsCount);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate2() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setNotes("XXX");
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.addMiriamData(new MiriamData());
      species.update(species2);
      int warningsCount = getWarnings().size();
      assertEquals(0, warningsCount);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate3() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setNotes("XXX");
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.setNotes("xx");
      species.update(species2);
      int warningsCount = getWarnings().size();
      assertEquals(0, warningsCount);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate4() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setNotes("XX");
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.setNotes("xxX");
      species.update(species2);
      int warningsCount = getWarnings().size();
      assertEquals(0, warningsCount);
      assertEquals(3, species.getNotes().length());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate5() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setNotes("XX");
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.setNotes("a as x");
      species2.setHypothetical(true);
      species2.setSymbol("sym");
      species2.setHomodimer(2);
      species.update(species2);
      int warningsCount = getWarnings().size();
      assertEquals(0, warningsCount);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate6() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setSymbol("sym1");
      species.setFullName("a_sym1");
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.setSymbol("sym2");
      species2.setFullName("b_sym1");
      species.update(species2);
      int warningsCount = getWarnings().size();
      assertEquals(2, warningsCount);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate7() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
      species2.addSynonym("syn");
      species2.addFormerSymbol("sym");
      species.update(species2);
      int warningsCount = getWarnings().size();
      assertEquals(0, warningsCount);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetInitialAmount() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setInitialAmount("1");
      assertEquals(1, species.getInitialAmount(), Configuration.EPSILON);
      try {
        species.setInitialAmount("a1");
        fail("Exception expected");
      } catch (InvalidArgumentException e) {
      }
      species.setInitialAmount("1");
      species.setInitialAmount((String) null);
      assertNull(species.getInitialAmount());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetCharge() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setCharge("1");
      assertEquals((Integer) 1, species.getCharge());
      try {
        species.setCharge("a1");
        fail("Exception expected");
      } catch (InvalidArgumentException e) {
      }
      species.setCharge("1");
      species.setCharge((String) null);
      assertNull(species.getCharge());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetOnlySubstanceUnits() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setOnlySubstanceUnits("true");
      assertTrue(species.getOnlySubstanceUnits());
      try {
        species.setOnlySubstanceUnits("a1");
        fail("Exception expected");
      } catch (InvalidArgumentException e) {
      }
      species.setOnlySubstanceUnits("false");
      assertFalse(species.getOnlySubstanceUnits());
      species.setOnlySubstanceUnits((String) null);
      assertNull(species.getOnlySubstanceUnits());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetInitialConcentration() {
    try {
      CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
      species.setInitialConcentration("1");
      assertEquals(1, species.getInitialConcentration(), Configuration.EPSILON);
      try {
        species.setInitialConcentration("a1");
        fail("Exception expected");
      } catch (InvalidArgumentException e) {
      }
      species.setInitialConcentration("1");
      species.setInitialConcentration((String) null);
      assertNull(species.getInitialConcentration());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCreateInvalidElement() {
    try {
      CellDesignerSpecies<?> complex = new CellDesignerSpecies<Rna>();
      complex.createModelElement("id");
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
