package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.IonChannelProtein;

public class IonChannelProteinTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerIonChannelProtein());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			CellDesignerIonChannelProtein species = new CellDesignerIonChannelProtein(new CellDesignerSpecies<IonChannelProtein>());
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy1() {
		try {
			CellDesignerIonChannelProtein species = new CellDesignerIonChannelProtein(new CellDesignerSpecies<IonChannelProtein>()).copy();
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy2() {
		try {
			CellDesignerIonChannelProtein protein = Mockito.spy(CellDesignerIonChannelProtein.class);
			protein.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
