package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class PolylineDataFactoryTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPrivateCotnstructor() throws Exception {
		try {
			Constructor<?> constr = PolylineDataFactory.class.getDeclaredConstructor(new Class<?>[] {});
			constr.setAccessible(true);
			assertNotNull(constr.newInstance(new Object[] {}));
		} catch (Exception e) {
			throw e;
		}
	}

}
