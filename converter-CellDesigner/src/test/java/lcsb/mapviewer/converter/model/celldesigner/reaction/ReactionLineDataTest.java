package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.model.map.reaction.Reaction;

public class ReactionLineDataTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetByLineType() {
    assertNull(ReactionLineData.getByLineType(null, null));
  }

  @Test
  public void test() {
    assertNotNull(ReactionLineData.getByCellDesignerString("UNKNOWN_REDUCED_MODULATION"));
  }

  @Test
  public void testAllValues() {
    for (ReactionLineData value : ReactionLineData.values()) {
      assertNotNull(ReactionLineData.valueOf(value.toString()));
    }
  }

  @Test
  public void testCreateInvalidReaction() throws Exception {
    try {
      ReactionLineData.TRANSPORT.createReaction(Mockito.mock(Reaction.class));
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Problem with creation the new instance of reaction"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
