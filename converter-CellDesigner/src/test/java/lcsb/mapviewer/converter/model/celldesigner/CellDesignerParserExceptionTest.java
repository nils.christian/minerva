package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CellDesignerParserExceptionTest {

	private static class Tmp extends CellDesignerParserException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Tmp(String string) {
			super(string);
		}

		public Tmp() {
			super();
		}

		public Tmp(String string, Exception exception) {
			super(string, exception);
		}

		public Tmp(Exception exception) {
			super(exception);
		}

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		assertNotNull(new Tmp());
		assertNotNull(new Tmp("str"));
		assertNotNull(new Tmp("str2", new Exception()));
		assertNotNull(new Tmp(new Exception()));
	}

}
