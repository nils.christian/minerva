package lcsb.mapviewer.converter.model.celldesigner.annotation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NoteFieldTest.class, //
    RestAnnotationParserTest.class, //
})
public class AllAnnotationTests {

}
