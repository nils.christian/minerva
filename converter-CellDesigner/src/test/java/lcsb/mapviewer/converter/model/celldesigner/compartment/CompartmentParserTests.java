package lcsb.mapviewer.converter.model.celldesigner.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.model.Model;

public class CompartmentParserTests extends CellDesignerTestFunctions {
	Logger logger = Logger.getLogger(CompartmentParserTests.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseCompartmensBottom() {
		Model model;
		try {
			model = getModelForFile("testFiles/compartment/bottom_compartment.xml");

			assertEquals(1, model.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected exception");
		}
	}

	@Test
	public void testParseCompartmensTop() {
		Model model;
		try {
			model = getModelForFile("testFiles/compartment/top_compartment.xml");

			assertEquals(1, model.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected exception");
		}
	}

	@Test
	public void testParseCompartmensRight() {
		Model model;
		try {
			model = getModelForFile("testFiles/compartment/right_compartment.xml");

			assertEquals(1, model.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected exception");
		}
	}

	@Test
	public void testParseCompartmensLeft() {
		Model model;
		try {
			model = getModelForFile("testFiles/compartment/left_compartment.xml");

			assertEquals(1, model.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected exception");
		}
	}

}
