package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerComparator;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerOvalComparator;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerRectComparator;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.layout.graphics.LayerTextComparator;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class LayerXmlParserTest extends CellDesignerTestFunctions {
	Logger				 logger	= Logger.getLogger(LayerXmlParserTest.class);

	LayerXmlParser parser	= new LayerXmlParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseLayers() throws Exception {
		LayerComparator comparator = new LayerComparator();
		try {
			String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_collection.xml");
			Node node = getNodeFromXmlString(xmlString);
			Collection<Layer> layers = parser.parseLayers(node);
			assertNotNull(layers);
			assertEquals(1, layers.size());

			xmlString = parser.layerCollectionToXml(layers);
			node = getNodeFromXmlString(xmlString);
			Collection<Layer> layers2 = parser.parseLayers(node);
			assertNotNull(layers2);
			assertEquals(1, layers2.size());
			assertEquals(0, comparator.compare(layers.iterator().next(), layers2.iterator().next()));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayers() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer_collection.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.parseLayers(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfLayers"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseBlockDiagrams() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			String xmlString = readFile("testFiles/xmlNodeTestExamples/block_diagrams.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.parseBlocks(model, node);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidBlockDiagrams() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			String xmlString = readFile("testFiles/invalid/block_diagrams.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.parseBlocks(model, node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfBlockDiagrams"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidGroups() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			String xmlString = readFile("testFiles/invalid/group_collection.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.parseGroups(model, node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfGroups"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidGroups2() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			String xmlString = readFile("testFiles/invalid/group_collection2.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.parseGroups(model, node);
			fail("Exception expected");
		} catch (InvalidGroupException e) {
			assertTrue(e.getMessage().contains("such alias doesn't exist in the model"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidGroups3() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			GenericProtein alias = new GenericProtein("sa1");
			model.addElement(alias);

			String xmlString = readFile("testFiles/invalid/group_collection3.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.parseGroups(model, node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:group"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLayer() throws Exception {
		LayerComparator comparator = new LayerComparator();
		try {
			String xmlString = readFile("testFiles/xmlNodeTestExamples/layer.xml");
			Node node = getNodeFromXmlString(xmlString);
			Layer layer = parser.getLayer(node);
			assertNotNull(layer);

			xmlString = parser.layerToXml(layer);
			node = getNodeFromXmlString(xmlString);
			Layer layers2 = parser.getLayer(node);
			assertNotNull(layers2);
			assertEquals(0, comparator.compare(layer, layers2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayer() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayer(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfTexts"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayer2() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer2.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayer(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown celldesigner:layerCompartmentAlias type"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayer3() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer3.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayer(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfSquares"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayer4() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer4.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayer(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfFreeLines"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayer5() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer5.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayer(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:layer"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLayerRect() throws Exception {
		try {
			LayerRectComparator comparator = new LayerRectComparator();
			String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_square.xml");
			Node node = getNodeFromXmlString(xmlString);
			LayerRect layer = parser.getLayerRect(node);
			assertNotNull(layer);

			xmlString = parser.layerRectToXml(layer);
			node = getNodeFromXmlString(xmlString);
			LayerRect layers2 = parser.getLayerRect(node);
			assertNotNull(layers2);
			assertEquals(0, comparator.compare(layer, layers2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testParseInvalidLayerRect() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer_square.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayerRect(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:layerCompartmentAlias"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLayerLine() {
		try {
			PolylineDataComparator comparator = new PolylineDataComparator();
			String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_line.xml");
			Node node = getNodeFromXmlString(xmlString);
			PolylineData layer = parser.getLayerLine(node);
			assertNotNull(layer);

			xmlString = parser.layerLineToXml(layer);
			node = getNodeFromXmlString(xmlString);
			PolylineData layers2 = parser.getLayerLine(node);
			assertNotNull(layers2);
			assertEquals(0, comparator.compare(layer, layers2));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testParseInvalidLayerLine() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer_line.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayerLine(node);
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:layerFreeLine"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLayerOval() {
		try {
			LayerOvalComparator comparator = new LayerOvalComparator();
			String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_oval.xml");
			Node node = getNodeFromXmlString(xmlString);
			LayerOval layer = parser.getLayerOval(node);
			assertNotNull(layer);

			xmlString = parser.layerOvalToXml(layer);
			node = getNodeFromXmlString(xmlString);
			LayerOval layers2 = parser.getLayerOval(node);
			assertNotNull(layers2);
			assertEquals(0, comparator.compare(layer, layers2));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testParseInvalidLayerOval() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer_oval.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayerOval(node);
			fail("Exception expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:layerCompartmentAlias"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLayerText() {
		try {
			LayerTextComparator comparator = new LayerTextComparator();
			String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_text.xml");
			Node node = getNodeFromXmlString(xmlString);
			LayerText layer = parser.getLayerText(node);
			assertNotNull(layer);

			xmlString = parser.layerTextToXml(layer);
			node = getNodeFromXmlString(xmlString);
			LayerText layers2 = parser.getLayerText(node);
			assertNotNull(layers2);
			assertEquals(0, comparator.compare(layer, layers2));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testParseInvalidLayerText() throws Exception {
		try {
			String xmlString = readFile("testFiles/invalid/layer_text.xml");
			Node node = getNodeFromXmlString(xmlString);
			parser.getLayerText(node);
			fail("Exception expected");

		} catch (InvalidXmlSchemaException e) {
			assertTrue(e.getMessage().contains("Unknown element of celldesigner:layerSpeciesAlias"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAliasGroup() throws Exception {
		try {
			Model model = new ModelFullIndexed(null);
			Species alias = new GenericProtein("sa1035");
			model.addElement(alias);

			alias = new GenericProtein("sa1036");
			model.addElement(alias);

			alias = new GenericProtein("sa1037");
			model.addElement(alias);

			alias = new GenericProtein("sa1038");
			model.addElement(alias);

			String string = "<celldesigner:group id=\"g74\" members=\"sa1035,sa1036,sa1037,sa1038\"/>";
			Node node = getNodeFromXmlString(string);
			ElementGroup group = parser.getAliasGroup(node, model);
			assertNotNull(group);

			assertEquals(4, group.getElements().size());

			assertEquals("g74", group.getIdGroup());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testLayerTextToXml() throws Exception {
		try {
			LayerText text = new LayerText(new Rectangle2D.Double(1, 2, 3, 4), "DF");
			String xml = parser.layerTextToXml(text);
			assertNotNull(xml);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
