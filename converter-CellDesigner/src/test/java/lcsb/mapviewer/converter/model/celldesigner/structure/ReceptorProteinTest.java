package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.ReceptorProtein;

public class ReceptorProteinTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerReceptorProtein());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			CellDesignerReceptorProtein species = new CellDesignerReceptorProtein(new CellDesignerSpecies<ReceptorProtein>());
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy1() {
		try {
			CellDesignerReceptorProtein species = new CellDesignerReceptorProtein(new CellDesignerSpecies<ReceptorProtein>()).copy();
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy2() {
		try {
			CellDesignerReceptorProtein protein = Mockito.spy(CellDesignerReceptorProtein.class);
			protein.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
