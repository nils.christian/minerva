package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

public class SimpleMoleculeTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerSimpleMolecule());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			CellDesignerSimpleMolecule degraded = new CellDesignerSimpleMolecule(new CellDesignerSpecies<SimpleMolecule>());
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			CellDesignerSimpleMolecule degraded = new CellDesignerSimpleMolecule().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			CellDesignerSimpleMolecule simpleMolecule = Mockito.spy(CellDesignerSimpleMolecule.class);
			simpleMolecule.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
