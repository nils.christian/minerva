package lcsb.mapviewer.converter.model.celldesigner.geometry;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.AllHelperTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AbstractCellDesignerAliasConverterTest.class, //
		AllHelperTests.class, //
		AntisenseRnaCellDesignerAliasConverterTest.class, //
		CellDesignerAliasConverterTest.class, //
		DegradedCellDesignerAliasConverterTest.class, //
		GeneCellDesignerAliasConverterTest.class, //
		IonCellDesignerAliasConverterTest.class, //
		ProteinConverterTest.class, //
		ProteinCellDesignerAliasConverterTest.class, //
		ReactionCellDesignerConverterTest.class, //
		RnaCellDesignerAliasConverterTest.class, //
		SimpleMoleculeCellDesignerAliasConverterTest.class, //
		UnknownCellDesignerAliasConverterTest.class, //
})
public class AllGeometryTests {

}
