package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionFromXmlTest extends CellDesignerTestFunctions {
  ReactionXmlParser parser;
  Model model = new ModelFullIndexed(null);

  CellDesignerElementCollection elements;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    parser = new ReactionXmlParser(elements, false);

    Species alias = new GenericProtein("sa1");
    model.addElement(alias);

    alias = new GenericProtein("sa2");
    model.addElement(alias);

    alias = new GenericProtein("sa3");
    model.addElement(alias);

    alias = new GenericProtein("sa4");
    model.addElement(alias);

    elements.addElement(new CellDesignerGenericProtein("s1"));
    elements.addElement(new CellDesignerGenericProtein("s2"));
    elements.addElement(new CellDesignerGenericProtein("s3"));
    elements.addElement(new CellDesignerGenericProtein("s4"));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testInvalid() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("kineticLaw node doesn't have math subnode"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid3() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction3.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown element of reaction"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid4() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction4.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("No annotation node in reaction"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid5() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction5.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Problem with parsing RDF"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid6() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction6.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown element of reaction/annotation"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid7() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction7.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:baseReactants"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid8() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction8.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:baseProducts"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid9() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction9.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:connectScheme"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid10() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction10.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown element of reaction/celldesigner:extension"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid11() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction11.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("No connectScheme node"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid12() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction12.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown node type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid13() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction13.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Too many gate members"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid14() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction14.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Missing gate member connecting members"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid15() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction15.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Couldn't find type of BOOLEAN_LOGIC_GATE"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid16() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction16.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:listOfReactantLinks"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid17() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction17.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:reactantLink"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid19() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction19.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Alias doesn't exist"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid20() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction20.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:listOfProductLinks"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid21() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction21.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:reactantLink"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid23() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction23.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Alias doesn't exist"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid24() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction24.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:listOfModification"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid26() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction26.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Unknown alias"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid28() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction28.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:connectScheme"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid29() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction29.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of celldesigner:listOfModification"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid30() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction30.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of reaction/celldesigner:baseReactant"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid31() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction31.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Alias with id"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid33() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction33.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of reaction/celldesigner:baseReactant"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid34() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction34.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(), e.getMessage().contains("Alias with id"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid36() throws Exception {
    try {
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/invalid/reaction36.xml")), model);
      fail("Exception expected");
    } catch (ReactionParserException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("Unknown element of reaction/celldesigner:baseProduct"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction() throws Exception {
    try {
      // test situation when createProperTypeReaction returns reaction of
      // unknown type
      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) {
          return result;
        }
      };
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_transport.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Problem with parsing lines. Unknown reaction"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction2() throws Exception {
    try {
      // test situation when createProperTypeReaction returns reaction of
      // unknown type that implements TwoProductReactionInterface

      class NewReactionType extends Reaction implements TwoProductReactionInterface {
        private static final long serialVersionUID = 1L;

        public NewReactionType(Reaction r) {
          super(r);
        }
      }

      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) {
          return new NewReactionType(result);
        }
      };
      parser.getReaction(
          super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Invalid reaction type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction3() throws Exception {
    try {
      // test situation when createOperatorsForTwoProductReaction encounter
      // reaction with two many base reactants

      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
          Reaction r = super.createProperTypeReaction(type, result);
          r.addReactant(new Reactant());
          return r;
        }
      };
      parser.getReaction(
          super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Reaction has more than one reactant"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction4() throws Exception {
    try {
      // test situation when createOperatorsForTwoProductReaction encounter
      // reaction with two many base products

      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
          Reaction r = super.createProperTypeReaction(type, result);
          r.addProduct(new Product());
          return r;
        }
      };
      parser.getReaction(
          super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Too many products"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction5() throws Exception {
    try {
      // test situation when createProperTypeReaction returns reaction of
      // unknown type that implements TwoReactantReactionInterface

      class NewReactionType extends Reaction implements TwoReactantReactionInterface {
        private static final long serialVersionUID = 1L;

        public NewReactionType(Reaction r) {
          super(r);
        }
      }

      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) {
          return new NewReactionType(result);
        }
      };
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Unknown class type with two reactants"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction6() throws Exception {
    try {
      // test situation when createOperatorsForTwoReactantReaction encounter
      // reaction with two many base reactants

      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
          Reaction r = super.createProperTypeReaction(type, result);
          r.addReactant(new Reactant());
          return r;
        }
      };
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Too many reactants"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidReaction7() throws Exception {
    try {
      // test situation when createOperatorsForTwoReactantReaction encounter
      // reaction with two many base products

      ReactionFromXml parser = new ReactionFromXml(null, false) {
        Reaction createProperTypeReaction(String type, Reaction result) throws ReactionParserException {
          Reaction r = super.createProperTypeReaction(type, result);
          r.addProduct(new Product());
          return r;
        }
      };
      parser.getReaction(super.getNodeFromXmlString(readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml")),
          model);
      fail("Exception expected");

    } catch (ReactionParserException e) {
      assertTrue(e.getMessage().contains("Reaction has more than one product"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidEditPointsString() throws Exception {
    try {
      ReactionFromXml parser = new ReactionFromXml(null, false);

      parser.parseEditPointsString("1");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid editPoint string"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidEditPointsString2() throws Exception {
    try {
      ReactionFromXml parser = new ReactionFromXml(null, false);

      parser.parseEditPointsString("1,Infinity");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid point parsed from input string"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
