package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;

public class OperatorTypeUtilsTest {

  OperatorTypeUtils utils = new OperatorTypeUtils();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetUnknownOperatorTypeForClazz() {

    OperatorType result = utils
        .getOperatorTypeForClazz(Mockito.mock(NodeOperator.class, Mockito.CALLS_REAL_METHODS).getClass());

    assertNull(result);
  }

  @Test
  public void testGetUnkownOperatorTypeForStringType() {
    OperatorType result = utils.getOperatorTypeForStringType("unknown");

    assertNull(result);
  }

  @Test
  public void testGetUnknownStringTypeByOperator() {

    String result = utils.getStringTypeByOperator(Mockito.mock(NodeOperator.class, Mockito.CALLS_REAL_METHODS));

    assertNull(result);
  }

  @Test
  public void testCreateUnknownModifierForStringType() {
    try {
      utils.createModifierForStringType("blabla");
      fail("Exceptione expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown modifier type"));
    }
  }

  @Test
  public void testCreateInvalidModifierForStringType() throws Exception {
    // artificial implementation of Modifier that is invalid
    class InvalidModifier extends Modifier {
      private static final long serialVersionUID = 1L;

      @SuppressWarnings("unused")
      public InvalidModifier() {
        throw new NotImplementedException();
      }
    }

    // mopdify one of the elements of OperatorType so it will have invalid
    // implementation
    OperatorType typeToModify = OperatorType.AND_OPERATOR_STRING;
    Class<? extends NodeOperator> clazz = typeToModify.getClazz();

    try {
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, InvalidModifier.class);

      // and check if we catch properly information about problematic
      // implementation
      utils.createModifierForStringType(typeToModify.getStringName());
      fail("Exceptione expected");
    } catch (InvalidStateException e) {
      assertTrue(e.getMessage().contains("Problem with instantiation of NodeOperator class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore correct values for the modified type (if not then other test
      // might fail...)
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, clazz);
    }
  }

}
