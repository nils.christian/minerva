package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class CompartmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerCompartment());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			CellDesignerCompartment original = new CellDesignerCompartment();
			original.addElement(new CellDesignerGenericProtein());
			CellDesignerCompartment compartment = new CellDesignerCompartment(original);
			assertNotNull(compartment);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEquals() {
		try {
			CellDesignerCompartment original = new CellDesignerCompartment();
			original.setName("anme");
			CellDesignerCompartment compartment = new CellDesignerCompartment(original);
			CellDesignerCompartment compartment2 = new CellDesignerCompartment();

			CellDesignerCompartment compartment3 = new CellDesignerCompartment();
			compartment3.setElementId("q");

			assertEquals(0, compartment.compareTo(original));
			assertTrue(compartment.equals(original));

			assertEquals(0, compartment.compareTo(compartment));
			assertTrue(compartment.equals(compartment));
			assertTrue(compartment.compareTo(compartment2) != 0);
			assertFalse(compartment.equals(compartment2));

			assertTrue(compartment.compareTo(compartment3) != 0);

			assertFalse(compartment.equals(new Object()));

			assertTrue(compartment.compareTo(null) != 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			CellDesignerCompartment compartment = new CellDesignerCompartment().copy();
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			String elementId = "id";

			CellDesignerCompartment compartment = new CellDesignerCompartment();

			compartment.setElementId(elementId);
			assertEquals(elementId, compartment.getElementId());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddElements() {
		try {
			CellDesignerGenericProtein protein = new CellDesignerGenericProtein();

			CellDesignerCompartment compartment = new CellDesignerCompartment();

			assertEquals(0, compartment.getElements().size());
			compartment.addElement(protein);
			assertEquals(1, compartment.getElements().size());
			compartment.addElement(protein);
			assertEquals(1, compartment.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testHashCode() {
		try {
			CellDesignerCompartment compartment = new CellDesignerCompartment();
			compartment.setName("name");

			int code = compartment.hashCode();
			compartment.setName("name2");
			int code2 = compartment.hashCode();

			assertTrue(code != code2);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			CellDesignerCompartment compartment = Mockito.spy(CellDesignerCompartment.class);
			compartment.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCreateModelElementWithProblems() {
		try {
			CellDesignerCompartment compartment = new CellDesignerCompartment();
			compartment.addElement(new CellDesignerAntisenseRna());
			compartment.createModelElement();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
