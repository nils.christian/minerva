package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerDegraded;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerDrug;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerIon;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerPhenotype;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSimpleMolecule;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerUnknown;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SpeciesState;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class SpeciesSbmlParserTest extends CellDesignerTestFunctions {
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(SpeciesSbmlParserTest.class.getName());

  SpeciesSbmlParser parser;

  String testDirectory = "testFiles" + System.getProperty("file.separator") + "xmlNodeTestExamples"
      + System.getProperty("file.separator");
  String testGeneFile = "sbml_gene.xml";
  String testDegradedFile = "sbml_degraded.xml";
  String testDrugFile = "sbml_drug.xml";
  String testIonFile = "sbml_ion.xml";
  String testPhenotypeFile = "sbml_phenotype.xml";
  String testProteinFile = "sbml_protein.xml";
  String testRnaFile = "sbml_rna.xml";
  String testSimpleMoleculeFile = "sbml_simple_molecule.xml";
  String testUnknownFile = "sbml_unknown.xml";

  CellDesignerElementCollection elements;

  int idCounter = 0;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    parser = new SpeciesSbmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpeciesAntisenseRna() throws Exception {
    try {
      String xmlString = readFile(testDirectory + "sbml_antisense_rna.xml");
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerAntisenseRna species = (CellDesignerAntisenseRna) result.getRight();
      assertEquals("s2", species.getElementId());
      assertEquals("s3", species.getName());
      assertEquals(2, species.getInitialAmount(), Configuration.EPSILON);
      assertEquals(Boolean.TRUE, species.hasOnlySubstanceUnits());
      assertEquals(new Integer(0), species.getCharge());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesWithKineticsData() throws Exception {
    try {
      String xmlString = readFile("testFiles/kinetics/species_with_kinetics_param.xml");
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();
      assertEquals(3.7, species.getInitialConcentration(), Configuration.EPSILON);
      assertEquals(SbmlUnitType.MOLE, species.getSubstanceUnit());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSpeciesUnitsToXml() throws Exception {
    try {
      GenericProtein protein = new GenericProtein("s1");
      protein.setSubstanceUnits(SbmlUnitType.MOLE);
      String xml = parser.toXml(protein);
      assertTrue("Cannot find substance unit in xml", xml.indexOf("mole") >= 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSpeciesConstantToXml() throws Exception {
    try {
      GenericProtein protein = new GenericProtein("s1");
      protein.setConstant(true);
      String xml = parser.toXml(protein);
      assertTrue("Cannot find constant in xml", xml.indexOf("constant") >= 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSpeciesBoundaryConditionToXml() throws Exception {
    try {
      GenericProtein protein = new GenericProtein("s1");
      protein.setBoundaryCondition(true);
      String xml = parser.toXml(protein);
      assertTrue("Cannot find boundary condition in xml", xml.indexOf("boundaryCondition") >= 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesWithKineticsData2() throws Exception {
    try {
      String xmlString = readFile("testFiles/kinetics/species_with_kinetics_param2.xml");
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();
      assertEquals(2.5, species.getInitialAmount(), Configuration.EPSILON);
      assertEquals(SbmlUnitType.GRAM, species.getSubstanceUnit());
      assertEquals(Boolean.TRUE, species.hasOnlySubstanceUnits());
      assertEquals("Boundary condition wasn't parsed", Boolean.TRUE, species.isBoundaryCondition());
      assertEquals("Constant property wasn't parsed", Boolean.TRUE, species.isConstant());
      assertEquals(new Integer(0), species.getCharge());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlAntisenseRna() throws Exception {
    try {
      String xmlString = readFile(testDirectory + "sbml_antisense_rna.xml");
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerAntisenseRna species = (CellDesignerAntisenseRna) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerAntisenseRna species2 = (CellDesignerAntisenseRna) result2.getRight();

      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getPositionToCompartment(), species2.getPositionToCompartment());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesComplex() throws Exception {
    try {
      String xmlString = readFile(testDirectory + "sbml_complex.xml");
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) result.getRight();
      assertNotNull(species);
      assertEquals("s6549", species.getElementId());
      assertEquals("LC3-II", species.getName());
      assertEquals(0.0, species.getInitialAmount(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlComplex() throws Exception {
    try {
      String xmlString = readFile(testDirectory + "sbml_complex.xml");
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerComplexSpecies species2 = (CellDesignerComplexSpecies) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesDegraded() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testDegradedFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerDegraded species = (CellDesignerDegraded) result.getRight();
      assertNotNull(species);
      assertEquals("s1275", species.getElementId());
      assertEquals("s1275", species.getName());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlDegraded() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testDegradedFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerDegraded species = (CellDesignerDegraded) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerDegraded species2 = (CellDesignerDegraded) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesDrug() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testDrugFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerDrug species = (CellDesignerDrug) result.getRight();
      assertEquals("s6104", species.getElementId());
      assertEquals("geldanamycin", species.getName());
      assertEquals(new Integer(0), species.getCharge());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlDrug() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testDrugFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerDrug species = (CellDesignerDrug) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerDrug species2 = (CellDesignerDrug) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesGene() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testGeneFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerGene species = (CellDesignerGene) result.getRight();

      assertEquals("s5916", species.getElementId());
      assertEquals("Ptgr1", species.getName());
      assertEquals(new Integer(0), species.getCharge());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesGeneWithModelUpdate() throws Exception {
    try {
      CellDesignerSpecies<?> gene = new CellDesignerGene();
      gene.setElementId("s5916");
      InternalModelSpeciesData modelData = new InternalModelSpeciesData();
      modelData.updateSpecies(gene, "");

      SpeciesSbmlParser complexParser = new SpeciesSbmlParser(elements);
      CellDesignerGene oldGene = new CellDesignerGene();
      oldGene.setElementId("s5916");
      oldGene.setName("Ptgr1");
      modelData.updateSpecies(oldGene, "gn95");

      String xmlString = readFile(testDirectory + testGeneFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = complexParser.parseXmlElement(xmlString);
      CellDesignerGene species = (CellDesignerGene) result.getRight();
      modelData.updateSpecies(species, result.getLeft());

      assertEquals("s5916", species.getElementId());
      assertEquals("Ptgr1", species.getName());
      assertTrue(species.getNotes().contains("prostaglandin reductase 1"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlGene() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testGeneFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerGene species = (CellDesignerGene) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerGene species2 = (CellDesignerGene) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertTrue(species2.getNotes().trim().contains(species.getNotes().trim()));
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesIon() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testIonFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerIon species = (CellDesignerIon) result.getRight();

      assertEquals("s6029", species.getElementId());
      assertEquals("Pi", species.getName());
      assertEquals(new Integer(0), species.getCharge());
      assertEquals(0, species.getInitialConcentration(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlIon() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testIonFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerIon species = (CellDesignerIon) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser.parseXmlElement(xmlString);
      CellDesignerIon species2 = (CellDesignerIon) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getElementId(), species2.getElementId());
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
      assertEquals(species.getNotes().trim(), species2.getNotes().trim());
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesPhenotype() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testPhenotypeFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerPhenotype species = (CellDesignerPhenotype) result.getRight();

      assertEquals("s5462", species.getElementId());
      assertEquals("Neuronal damage and death", species.getName());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlPhenotype() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testPhenotypeFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerPhenotype species = (CellDesignerPhenotype) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerPhenotype species2 = (CellDesignerPhenotype) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
      assertEquals(species.getNotes(), species2.getNotes());
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesProtein() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testProteinFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();

      assertEquals("s5456", species.getElementId());
      assertEquals("PTPRC", species.getName());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
      assertEquals(new Integer(0), species.getCharge());
      assertTrue(species.getNotes().contains("protein tyrosine phosphatase, receptor type, C"));
      assertNull(species.getStructuralState());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlProtein() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testProteinFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerGenericProtein species = new CellDesignerGenericProtein(result.getRight());

      String transformedXml = parser.toXml(species.createModelElement("" + idCounter++));
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement("" + idCounter++)));
      CellDesignerProtein<?> species2 = (CellDesignerProtein<?>) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
      assertTrue(species2.getNotes().contains(species.getNotes()));
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesRna() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testRnaFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerRna species = (CellDesignerRna) result.getRight();

      assertEquals("s5914", species.getElementId());
      assertEquals("Fmo3", species.getName());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
      assertEquals(new Integer(0), species.getCharge());
      assertTrue(species.getNotes().contains("Dimethylaniline monooxygenase [N-oxide-forming] 3"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlRna() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testRnaFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerRna species = (CellDesignerRna) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerRna species2 = (CellDesignerRna) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
      assertTrue(species2.getNotes().trim().contains(species.getNotes().trim()));
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesSimpleMolecule() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testSimpleMoleculeFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerSimpleMolecule species = (CellDesignerSimpleMolecule) result.getRight();
      assertEquals("s5463", species.getElementId());
      assertEquals("Peroxides", species.getName());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlSimpleMolecule() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testSimpleMoleculeFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerSimpleMolecule species = (CellDesignerSimpleMolecule) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement("" + idCounter++));
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement("" + idCounter++)));
      CellDesignerSimpleMolecule species2 = (CellDesignerSimpleMolecule) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
      assertEquals(species.getNotes(), species2.getNotes());
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlSpeciesUnknown() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testUnknownFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerUnknown species = (CellDesignerUnknown) result.getRight();
      assertEquals("s1356", species.getElementId());
      assertEquals("unidentified caspase acting on Occludin", species.getName());
      assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
      assertEquals(new Integer(0), species.getCharge());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlUnknown() throws Exception {
    try {
      String xmlString = readFile(testDirectory + testUnknownFile);
      Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
      CellDesignerUnknown species = (CellDesignerUnknown) result.getRight();

      String transformedXml = parser.toXml(species.createModelElement());
      assertNotNull(transformedXml);
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("species", root.item(0).getNodeName());

      Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
          .parseXmlElement(parser.toXml(species.createModelElement()));
      CellDesignerUnknown species2 = (CellDesignerUnknown) result2.getRight();

      assertNotNull(species2);
      assertEquals(species.getName(), species2.getName());
      assertEquals(species.getParent(), species2.getParent());
      assertEquals(species.getInitialAmount(), species2.getInitialAmount());
      assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
      assertEquals(species.getCharge(), species2.getCharge());
      assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
      assertEquals(species.getNotes(), species2.getNotes());
      assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("No annotation node"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid2() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein2.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("bla"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid3() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein3.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("unk_bla"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid4() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein4.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("No celldesigner:extension"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid5() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein5.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("ann_unk"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid6() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein6.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("No celldesigner:speciesIdentity"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid7() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein7.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("unkPos"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid8() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein8.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Species node in Sbml model doesn't contain"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid9() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein9.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("unknown_protein_class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid10() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein10.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Wrong class type for protein reference"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid11() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein11.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Wrong class type for gene reference"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid12() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein12.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Wrong class type for rna reference"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid13() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein13.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Wrong class type for antisense rna reference"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalid14() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/invalid_sbml_protein14.xml");
      parser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("uknNode"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXmlWithDefaultCompartment() throws Exception {
    try {
      CellDesignerGenericProtein species = new CellDesignerGenericProtein();
      String xml = parser.toXml(species.createModelElement("EL_ID"));
      assertTrue(xml.contains("EL_ID"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidSpeciesIdentityToXml() throws Exception {
    try {
      Species species = Mockito.mock(Species.class);
      parser.speciesIdentityToXml(species);
      fail("Excepiton expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid species class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSpeciesIdentityToXml() throws Exception {
    try {
      GenericProtein species = new GenericProtein("xx");
      species.setHypothetical(true);
      String xml = parser.speciesIdentityToXml(species);
      assertTrue(xml.contains("<celldesigner:hypothetical>true</celldesigner:hypothetical>"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSpeciesStateToXml() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.setHomodimer(2);
      state.setStructuralState("xxxState");
      String xml = parser.speciesStateToXml(state);
      assertTrue(xml.contains("xxxState"));
      assertTrue(xml.contains("celldesigner:homodimer"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.addModificationResidue(new CellDesignerModificationResidue());
      CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Modification not supported in Complex"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies2() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.setStructuralState("state");
      CellDesignerGene species = new CellDesignerGene();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("StructuralState not supported in Gene"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies3() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.setStructuralState("state");
      CellDesignerRna species = new CellDesignerRna();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Structural state not supported in RNA"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies4() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.addModificationResidue(new CellDesignerModificationResidue());
      CellDesignerSimpleMolecule species = new CellDesignerSimpleMolecule();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Modification not supported in SimpleMolecule"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies5() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.setStructuralState("state");
      CellDesignerSimpleMolecule species = new CellDesignerSimpleMolecule();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Structural state not supported in SimpleMolecule"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies6() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.setStructuralState("state");
      CellDesignerAntisenseRna species = new CellDesignerAntisenseRna();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Structural state not supported in AntisenseRna"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies7() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.setStructuralState("state");
      CellDesignerSpecies<?> species = new CellDesignerSpecies<Gene>();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Structural state not supported"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessInvalidStateDataInSpecies8() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.addModificationResidue(new CellDesignerModificationResidue());
      CellDesignerSpecies<?> species = new CellDesignerSpecies<Gene>();

      parser.processStateDataInSpecies(species, state);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Modification not supported in"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProcessAntisenseRnaStateDataInSpecies() throws Exception {
    try {
      SpeciesState state = new SpeciesState();
      state.addModificationResidue(new CellDesignerModificationResidue());
      CellDesignerAntisenseRna species = new CellDesignerAntisenseRna();

      parser.processStateDataInSpecies(species, state);

      assertEquals(1, species.getRegions().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
