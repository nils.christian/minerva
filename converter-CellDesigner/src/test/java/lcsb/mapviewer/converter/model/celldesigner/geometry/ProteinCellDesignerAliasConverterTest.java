package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Protein;

public class ProteinCellDesignerAliasConverterTest {

	ProteinCellDesignerAliasConverter converter = new ProteinCellDesignerAliasConverter(false);

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNotImplementedMethod() {
		try {
			Protein alias = Mockito.mock(Protein.class);
			converter.getBoundPathIterator(alias);
			fail("Exception expected");
		} catch (NotImplementedException e) {
			assertTrue(e.getMessage().contains("Not implemented protein converter for type"));
		}
	}
	
	@Test
	public void testGetPointCoordinates() {
		try {
			Protein alias = Mockito.spy(Protein.class);
			alias.setWidth(10);
			alias.setHeight(10);
			converter.getPointCoordinates(alias, CellDesignerAnchor.E);
			fail("Exception expected");
		} catch (NotImplementedException e) {
			assertTrue(e.getMessage().contains("Unknown type"));
		}
	}
	
	

}
