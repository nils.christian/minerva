package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class RnaTest {
  Logger logger = Logger.getLogger(RnaTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new CellDesignerRna());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor1() {
    try {
      CellDesignerRna rna = new CellDesignerRna(new CellDesignerSpecies<Rna>());
      assertNotNull(rna);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      CellDesignerRna rna = new CellDesignerRna(new CellDesignerSpecies<Rna>());
      List<CellDesignerModificationResidue> regions = new ArrayList<>();

      rna.setRegions(regions);

      assertEquals(regions, rna.getRegions());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      CellDesignerRna rna = new CellDesignerRna().copy();
      assertNotNull(rna);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidCopy() {
    try {
      CellDesignerRna rna = Mockito.spy(CellDesignerRna.class);
      rna.copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddRnaRegion() {
    try {
      CellDesignerRna original = new CellDesignerRna();
      CellDesignerModificationResidue region = new CellDesignerModificationResidue("id1");
      original.addRegion(region);

      CellDesignerModificationResidue region2 = new CellDesignerModificationResidue("id1");
      region2.setName("nam");
      original.addRegion(region2);

      assertEquals(1, original.getRegions().size());

      assertEquals("nam", original.getRegions().get(0).getName());

      CellDesignerModificationResidue region3 = new CellDesignerModificationResidue("id2");
      region3.setName("nam");
      original.addRegion(region3);

      assertEquals(2, original.getRegions().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate() {
    try {
      CellDesignerRna original = new CellDesignerRna();
      CellDesignerModificationResidue region2 = new CellDesignerModificationResidue("id1");
      region2.setName("nam");
      original.addRegion(region2);
      CellDesignerModificationResidue region3 = new CellDesignerModificationResidue("id2");
      region3.setName("nam");
      original.addRegion(region3);

      CellDesignerRna copy = new CellDesignerRna(original);
      copy.addRegion(new CellDesignerModificationResidue());
      copy.getRegions().get(0).setState(ModificationState.ACETYLATED);

      logger.debug(copy.getRegions().size());

      original.update(copy);

      boolean acetylatedFound = false;
      for (CellDesignerModificationResidue region : copy.getRegions()) {
        if (ModificationState.ACETYLATED.equals(region.getState())) {
          acetylatedFound = true;
        }
      }
      assertTrue(acetylatedFound);
      assertEquals(3, original.getRegions().size());
      assertEquals(3, copy.getRegions().size());

      original.update(new CellDesignerGenericProtein());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
