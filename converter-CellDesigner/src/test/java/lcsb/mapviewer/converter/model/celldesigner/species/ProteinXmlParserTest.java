package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.model.map.species.Protein;

public class ProteinXmlParserTest extends CellDesignerTestFunctions {
  Logger logger = Logger.getLogger(ProteinXmlParserTest.class.getName());

  ProteinXmlParser proteinParser;
  String testProteinFile = "testFiles" + System.getProperty("file.separator") + "xmlNodeTestExamples"
      + System.getProperty("file.separator") + "protein.xml";

  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    proteinParser = new ProteinXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() throws Exception {
    try {
      String xmlString = readFile(testProteinFile);
      Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
      CellDesignerProtein<?> protein = result.getRight();
      assertEquals("pr23", result.getLeft());
      assertEquals("SDHA", protein.getName());
      assertTrue(protein instanceof CellDesignerGenericProtein);
      assertEquals(1, protein.getModificationResidues().size());
      assertEquals("rs1", protein.getModificationResidues().get(0).getIdModificationResidue());
      assertEquals("S176 bla bla", protein.getModificationResidues().get(0).getName());
      assertEquals("Difference to big", 3.141592653589793, protein.getModificationResidues().get(0).getAngle(), 1e-6);
      assertEquals("none", protein.getModificationResidues().get(0).getSide());
      assertTrue(protein.getNotes().contains("UniProtKB	P31040	SDHA		GO:0005749	GO_REF:0000024"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToXml() throws Exception {
    try {
      String xmlString = readFile(testProteinFile);

      Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
      CellDesignerProtein<?> protein = result.getRight();
      String transformedXml = proteinParser.toXml(protein.createModelElement("id"));
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(transformedXml));
      Document doc = builder.parse(is);
      NodeList root = doc.getChildNodes();
      assertEquals("celldesigner:protein", root.item(0).getNodeName());

      elements = new CellDesignerElementCollection();
      proteinParser = new ProteinXmlParser(elements);
      Protein proteinWithLayout = protein.createModelElement("id");
      proteinWithLayout.setWidth(10);
      proteinWithLayout.setHeight(10);
      protein.updateModelElementAfterLayoutAdded(proteinWithLayout);
      Pair<String, CellDesignerProtein<?>> result2 = proteinParser
          .parseXmlElement(proteinParser.toXml(proteinWithLayout));
      
      CellDesignerProtein<?> protein2 = result2.getRight();
      assertEquals(protein.getName(), protein2.getName());
      assertEquals(protein.getClass(), protein2.getClass());
      assertEquals(protein.getModificationResidues().size(), protein2.getModificationResidues().size());
      assertEquals(protein.getModificationResidues().get(0).getIdModificationResidue(),
          protein2.getModificationResidues().get(0).getIdModificationResidue());
      assertEquals(protein.getModificationResidues().get(0).getName(),
          protein2.getModificationResidues().get(0).getName());
      assertEquals("Difference to big", protein.getModificationResidues().get(0).getAngle(),
          protein2.getModificationResidues().get(0).getAngle(), 1e-6);

      // proteins won't have notes because it makes the xml messy (everything
      // will be in species notes)
      // assertEquals(protein.getNotes().trim(), protein2.getNotes().trim());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidXml() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/protein.xml");
      proteinParser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Protein node in Sbml model is of unknown type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidXml2() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/protein2.xml");
      proteinParser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:listOfModificationResidues"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidXml3() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/protein3.xml");
      proteinParser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:protein"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidXml4() throws Exception {
    try {
      String xmlString = readFile("testFiles/invalid/protein4.xml");
      proteinParser.parseXmlElement(xmlString);
      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:modificationResidue"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidProteinToXml() throws Exception {
    try {
      proteinParser.toXml(Mockito.mock(Protein.class));
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid protein class type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
