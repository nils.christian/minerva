package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class ModificationResidueTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerModificationResidue());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			CellDesignerModificationResidue original = new CellDesignerModificationResidue();
			CellDesignerModificationResidue copy = new CellDesignerModificationResidue(original);
			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdate() {
		try {
			CellDesignerModificationResidue original = new CellDesignerModificationResidue();
			CellDesignerModificationResidue update = new CellDesignerModificationResidue();

			String name = "n";
			String side = "s";
			ModificationState state = ModificationState.ACETYLATED;
			Double angle = 3.9;
			Double size = 5.0;

			update.setName(name);
			update.setSide(side);
			update.setAngle(angle);
			update.setSize(size);
			update.setState(state);

			original.update(update);

			assertEquals(name, original.getName());
			assertEquals(side, original.getSide());
			assertEquals(angle, original.getAngle());
			assertEquals(size, original.getSize());
			assertEquals(state, original.getState());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			CellDesignerModificationResidue original = new CellDesignerModificationResidue();

			String doubleStr = "2.0";
			String invalidDoubleStr = "a2.0";
			String nullStr = null;
			Double angle = 2.0;
			CellDesignerSpecies<?> species = new CellDesignerSpecies<IonChannelProtein>();

			original.setAngle(doubleStr);
			assertEquals(angle, original.getAngle(), Configuration.EPSILON);
			try {
				original.setAngle(invalidDoubleStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			original.setAngle(nullStr);
			assertNull(original.getAngle());

			original.setSize(doubleStr);
			assertEquals(angle, original.getSize(), Configuration.EPSILON);
			try {
				original.setSize(invalidDoubleStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			original.setSize(doubleStr);
			original.setSize(nullStr);
			assertNull(original.getSize());

			original.setSpecies(species);
			assertEquals(species, original.getSpecies());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			CellDesignerModificationResidue degraded = new CellDesignerModificationResidue().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new CellDesignerModificationResidue() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
