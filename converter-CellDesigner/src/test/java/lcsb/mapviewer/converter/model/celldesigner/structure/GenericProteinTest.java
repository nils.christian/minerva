package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class GenericProteinTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerGenericProtein());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			CellDesignerGenericProtein species = new CellDesignerGenericProtein(new CellDesignerSpecies<GenericProtein>());
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy1() {
		try {
			CellDesignerGenericProtein species = new CellDesignerGenericProtein(new CellDesignerSpecies<GenericProtein>()).copy();
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy2() {
		try {
			CellDesignerGenericProtein protein = Mockito.spy(CellDesignerGenericProtein.class);
			protein.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
