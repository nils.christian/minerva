package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class CellDesignerPolygonTransformationTest {
	CellDesignerPolygonTransformation tranformation = new CellDesignerPolygonTransformation();

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetPointOnInvalidPolygonByAnchor() {
		try {
			tranformation.getPointOnPolygonByAnchor(new ArrayList<>(), null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid number of points"));
		}
	}

	@Test
	public void testGetPointOnPolygonByInvalidAnchor() {
		try {
			List<Point2D> list = new ArrayList<>();
			for (int i = 0; i < CellDesignerAnchor.DIFFERENT_ANCHORS / 2; i++) {
				list.add(new Point2D.Double());
			}
			tranformation.getPointOnPolygonByAnchor(list, null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid anchor"));
		}
	}

	@Test
	public void testGetPointOnPolygonByInvalidAnchor2() {
		try {
			List<Point2D> list = new ArrayList<>();
			for (int i = 0; i < CellDesignerAnchor.DIFFERENT_ANCHORS; i++) {
				list.add(new Point2D.Double());
			}
			tranformation.getPointOnPolygonByAnchor(list, null);
			tranformation.getPointOnPolygonByAnchor(new ArrayList<>(), null);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
			assertTrue(e.getMessage().contains("Invalid anchor"));
		}
	}

}
