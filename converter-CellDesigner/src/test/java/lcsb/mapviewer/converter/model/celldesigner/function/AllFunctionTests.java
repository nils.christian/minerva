package lcsb.mapviewer.converter.model.celldesigner.function;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FunctionCollectionXmlParserTest.class, FunctionXmlParserTest.class })
public class AllFunctionTests {

}
