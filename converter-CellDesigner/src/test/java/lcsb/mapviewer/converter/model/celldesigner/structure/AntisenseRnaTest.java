package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class AntisenseRnaTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new CellDesignerAntisenseRna());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor1() {
    try {
      CellDesignerAntisenseRna original = new CellDesignerAntisenseRna();
      original.addRegion(new CellDesignerModificationResidue());
      CellDesignerAntisenseRna aRna = new CellDesignerAntisenseRna(original);
      assertNotNull(aRna);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddRnaRegion() {
    try {
      CellDesignerAntisenseRna original = new CellDesignerAntisenseRna();
      CellDesignerModificationResidue region = new CellDesignerModificationResidue();
      region.setIdModificationResidue("id1");
      original.addRegion(region);

      CellDesignerModificationResidue region2 = new CellDesignerModificationResidue();
      region2.setIdModificationResidue("id1");
      region2.setName("nam");
      original.addRegion(region2);

      assertEquals(1, original.getRegions().size());

      assertEquals("nam", original.getRegions().get(0).getName());

      CellDesignerModificationResidue region3 = new CellDesignerModificationResidue();
      region3.setIdModificationResidue("id2");
      region3.setName("nam");
      original.addRegion(region3);

      assertEquals(2, original.getRegions().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate() {
    try {
      CellDesignerAntisenseRna original = new CellDesignerAntisenseRna();
      CellDesignerModificationResidue region2 = new CellDesignerModificationResidue();
      region2.setIdModificationResidue("id1");
      region2.setName("nam");
      original.addRegion(region2);
      CellDesignerModificationResidue region3 = new CellDesignerModificationResidue();
      region3.setIdModificationResidue("id2");
      region3.setName("nam");
      original.addRegion(region3);

      CellDesignerAntisenseRna copy = new CellDesignerAntisenseRna(original);
      copy.addRegion(new CellDesignerModificationResidue());
      copy.getRegions().get(0).setState(ModificationState.ACETYLATED);

      original.update(copy);

      boolean acetylatedFound = false;
      for (CellDesignerModificationResidue region : copy.getRegions()) {
        if (ModificationState.ACETYLATED.equals(region.getState())) {
          acetylatedFound = true;
        }
      }
      assertTrue(acetylatedFound);
      assertEquals(3, copy.getRegions().size());

      original.update(new CellDesignerGenericProtein());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      CellDesignerAntisenseRna aRna = new CellDesignerAntisenseRna(new CellDesignerSpecies<AntisenseRna>());

      List<CellDesignerModificationResidue> regions = new ArrayList<>();

      aRna.setRegions(regions);

      assertEquals(regions, aRna.getRegions());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      CellDesignerAntisenseRna aRna = new CellDesignerAntisenseRna().copy();
      assertNotNull(aRna);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidCopy() {
    try {
      CellDesignerAntisenseRna antisenseRna = Mockito.spy(CellDesignerAntisenseRna.class);
      antisenseRna.copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
