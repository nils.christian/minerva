package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.lang.reflect.Field;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ModifierTypeUtilsTest {

  ModifierTypeUtils utils = new ModifierTypeUtils();

  Modifier invalidModifier = Mockito.mock(Modifier.class, Mockito.CALLS_REAL_METHODS);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetInvalidModifierTypeForClazz() {
    ModifierType type = utils.getModifierTypeForClazz(invalidModifier.getClass());
    assertNull(type);
  }

  @Test
  public void testGetInvalidStringTypeByModifier() {
    String type = utils.getStringTypeByModifier(invalidModifier);
    assertNull(type);
  }

  @Test
  public void testGetTargetLineIndexByInvalidModifier() {
    try {
      utils.getTargetLineIndexByModifier(invalidModifier);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown modifier class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetTargetLineIndexByInvalidModifier2() {
    try {
      NodeOperator operator = new AndOperator();
      operator.addInput(new Reactant());
      utils.getTargetLineIndexByModifier(operator);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid NodeOperator"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetTargetLineIndexByInvalidModifier3() {
    try {
      NodeOperator operator = new AndOperator();
      operator.addInput(invalidModifier);
      utils.getTargetLineIndexByModifier(operator);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown modifier class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnchorPointOnReactionRectByInvalidType() {
    try {
      Reaction reaction = new Reaction();
      Reactant reactant = new Reactant();
      Product product = new Product();
      reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
      product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
      reaction.addReactant(reactant);
      reaction.addProduct(product);
      Point2D point = utils.getAnchorPointOnReactionRect(reaction, "0,0");
      assertNotNull(point);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnchorPointOnReactionRectByInvalidType2() {
    try {
      Reaction reaction = new Reaction();
      Reactant reactant = new Reactant();
      Product product = new Product();
      reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
      product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
      reaction.addReactant(reactant);
      reaction.addProduct(product);
      utils.getAnchorPointOnReactionRect(reaction, "0");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Invalid targetLineIndex"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAnchorPointOnReactionRectByInvalidType3() {
    try {
      Reaction reaction = new Reaction();
      Reactant reactant = new Reactant();
      Product product = new Product();
      reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
      product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double()));
      reaction.addReactant(reactant);
      reaction.addProduct(product);
      utils.getAnchorPointOnReactionRect(reaction, "10,10");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown targetLineIndex"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateLineEndPointForInvalidModifier() {
    try {
      utils.updateLineEndPoint(invalidModifier);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown modifier class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateLineEndPointForInvalidModifier2() {
    try {
      NodeOperator operator = new AndOperator();
      operator.addInput(new Product());
      operator.addInput(invalidModifier);
      utils.updateLineEndPoint(operator);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown modifier class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateLineEndPointForInvalidModifier3() {
    try {
      NodeOperator operator = new AndOperator();
      operator.addInput(new Product());
      operator.addInput(new Product());
      utils.updateLineEndPoint(operator);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Operator contains invalid input"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCreateInvalidModifierForStringType2() throws Exception {
    // artificial implementation of Modifier that is invalid
    class InvalidModifier extends Modifier {
      private static final long serialVersionUID = 1L;

      @SuppressWarnings("unused")
      public InvalidModifier() {
        throw new NotImplementedException();
      }

      @SuppressWarnings("unused")
      public InvalidModifier(Species alias, CellDesignerElement<?> element) {
        throw new NotImplementedException();
      }
    }

    // mopdify one of the elements of OperatorType so it will have invalid
    // implementation
    ModifierType typeToModify = ModifierType.CATALYSIS;
    Class<? extends Modifier> clazz = typeToModify.getClazz();

    try {
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, InvalidModifier.class);

      // and check if we catch properly information about problematic
      // implementation
      utils.createModifierForStringType(typeToModify.getStringName(), new GenericProtein("id"));
      fail("Exceptione expected");
    } catch (InvalidStateException e) {
      assertTrue(e.getMessage().contains("Problem with instantiation of Modifier class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      // restore correct values for the modified type (if not then other test
      // might fail...)
      Field field = typeToModify.getClass().getDeclaredField("clazz");
      field.setAccessible(true);
      field.set(typeToModify, clazz);
    }
  }

  @Test
  public void testCreateModifierForStringType2() throws Exception {
    try {
      utils.createModifierForStringType("unjkType", null);
      fail("Exceptione expected");
    } catch (InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Unknown modifier type"));
    }
  }

}
