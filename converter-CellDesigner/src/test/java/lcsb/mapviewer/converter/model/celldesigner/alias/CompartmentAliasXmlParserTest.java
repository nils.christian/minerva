package lcsb.mapviewer.converter.model.celldesigner.alias;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class CompartmentAliasXmlParserTest extends CellDesignerTestFunctions {
  static Logger logger = Logger.getLogger(CompartmentAliasXmlParser.class);

  CompartmentAliasXmlParser parser;
  Model model;
  CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);
    elements = new CellDesignerElementCollection();
    parser = new CompartmentAliasXmlParser(elements, model);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testToXml() throws Exception {
    try {
      Compartment compartment = createCompartment();
      String xmlString = parser.toXml(compartment);
      assertNotNull(xmlString);

      elements.addElement(new CellDesignerCompartment(elements.getElementId(compartment)));

      Compartment alias2 = parser.parseXmlAlias(xmlString);
      assertEquals(0, getWarnings().size());

      assertEquals(compartment.getElementId(), alias2.getElementId());
      assertNotNull(compartment.getName());
      assertEquals(compartment.getFontSize(), alias2.getFontSize(), 1e-6);
      assertEquals(compartment.getHeight(), alias2.getHeight(), 1e-6);
      assertEquals(compartment.getWidth(), alias2.getWidth(), 1e-6);
      assertEquals(compartment.getX(), alias2.getX(), 1e-6);
      assertEquals(compartment.getY(), alias2.getY(), 1e-6);
      assertEquals(compartment.getNamePoint().getX(), alias2.getNamePoint().getX(), 1e-6);
      assertEquals(compartment.getNamePoint().getY(), alias2.getNamePoint().getY(), 1e-6);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Compartment createCompartment() {
    Compartment compartment = new SquareCompartment("comp_id");
    compartment.setName("name");
    compartment.setX(13);
    compartment.setY(14);
    compartment.setWidth(100);
    compartment.setHeight(120);
    compartment.setNamePoint(new Point2D.Double(0, 1));

    return compartment;
  }

  @Test
  public void parseInvalidAlias() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      parser = new CompartmentAliasXmlParser(elements, model);
      String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias.xml");
      parser.parseXmlAlias(xmlString);
      fail("Exception expceted");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Compartment does not exist in a model"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void parseInvalidAlias2() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerCompartment comp = new CellDesignerCompartment();
      comp.setElementId("c1");
      elements.addElement(comp);
      parser = new CompartmentAliasXmlParser(elements, model);
      String xmlString = readFile("testFiles/invalid/compartment_alias.xml");
      parser.parseXmlAlias(xmlString);
      fail("Exception expceted");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Unknown compartment type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void parseInvalidAlias3() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerCompartment comp = new CellDesignerCompartment();
      comp.setElementId("c1");
      elements.addElement(comp);
      parser = new CompartmentAliasXmlParser(elements, model);
      String xmlString = readFile("testFiles/invalid/compartment_alias2.xml");
      parser.parseXmlAlias(xmlString);
      fail("Exception expceted");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Don't know what to do with celldesigner:point for class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void parseInvalidAlias4() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      CellDesignerCompartment comp = new CellDesignerCompartment();
      comp.setElementId("c1");
      elements.addElement(comp);
      parser = new CompartmentAliasXmlParser(elements, model);
      String xmlString = readFile("testFiles/invalid/compartment_alias3.xml");
      parser.parseXmlAlias(xmlString);
      fail("Exception expceted");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("Unknown element of celldesigner:compartmentAlias"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseXmlAliasNode() throws Exception {
    try {
      elements.addElement(new CellDesignerCompartment("c1"));

      String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias.xml");
      Compartment alias = (Compartment) parser.parseXmlAlias(xmlString);
      assertEquals(0, getWarnings().size());
      assertNotNull(alias);
      assertEquals("ca1", alias.getElementId());
      assertNotNull(alias.getName());
      assertTrue(alias instanceof SquareCompartment);
      assertEquals(139.0, alias.getX(), 1e-6);
      assertEquals(55.0, alias.getY(), 1e-6);
      assertEquals(522.0, alias.getWidth(), 1e-6);
      assertEquals(392.5, alias.getNamePoint().getX(), 1e-6);
      assertEquals(196.5, alias.getNamePoint().getY(), 1e-6);
      assertEquals(0xffcccc00, alias.getColor().getRGB());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      parser.setCommonParser(null);

      assertNull(parser.getCommonParser());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testLeftToXml() throws Exception {
    try {
      Compartment alias = new LeftSquareCompartment("id");
      String xml = parser.toXml(alias);
      assertNotNull(xml);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testRightToXml() throws Exception {
    try {
      Compartment alias = new RightSquareCompartment("id");
      String xml = parser.toXml(alias);
      assertNotNull(xml);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testTopToXml() throws Exception {
    try {
      Compartment alias = new TopSquareCompartment("id");
      String xml = parser.toXml(alias);
      assertNotNull(xml);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testBotttomToXml() throws Exception {
    try {
      Compartment alias = new BottomSquareCompartment("id");
      String xml = parser.toXml(alias);
      assertNotNull(xml);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testUnknownToXml() throws Exception {
    try {
      Compartment alias = Mockito.mock(BottomSquareCompartment.class);
      Mockito.when(alias.getElementId()).thenReturn("some id");
      parser.toXml(alias);
      fail("Exception expected");
    } catch (NotImplementedException e) {
      assertTrue(e.getMessage().contains("Unknown compartment class"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

}
