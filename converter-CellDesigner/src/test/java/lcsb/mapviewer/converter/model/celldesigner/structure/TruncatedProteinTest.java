package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.TruncatedProtein;

public class TruncatedProteinTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CellDesignerTruncatedProtein());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			CellDesignerTruncatedProtein species = new CellDesignerTruncatedProtein(new CellDesignerSpecies<TruncatedProtein>());
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy1() {
		try {
			CellDesignerTruncatedProtein species = new CellDesignerTruncatedProtein(new CellDesignerSpecies<TruncatedProtein>()).copy();
			assertNotNull(species);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy2() {
		try {
			CellDesignerTruncatedProtein protein = Mockito.spy(CellDesignerTruncatedProtein.class);
			protein.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
