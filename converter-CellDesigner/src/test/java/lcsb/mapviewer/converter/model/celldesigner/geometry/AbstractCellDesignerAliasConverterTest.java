package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerEllipseTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerPolygonTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerRectangleTransformation;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

public class AbstractCellDesignerAliasConverterTest {
  Logger logger = Logger.getLogger(AbstractCellDesignerAliasConverterTest.class);

  AbstractCellDesignerAliasConverter<Species> converter = new AbstractCellDesignerAliasConverter<Species>(false) {
    @Override
    public Point2D getPointCoordinates(Species alias, CellDesignerAnchor anchor) {
      return null;
    }

    @Override
    protected PathIterator getBoundPathIterator(Species alias) {
      return null;
    }
  };

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnchorPointCoordinatesForInvalidImplementation() {
    EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
    Logger.getRootLogger().addAppender(appender);
    try {
      Protein alias = new GenericProtein("id");
      alias.setWidth(1);
      alias.setHeight(1);
      converter.getAnchorPointCoordinates(alias, 0);
      assertEquals(1, appender.getWarnings().size());
    } finally {
      Logger.getRootLogger().removeAppender(appender);
    }
  }

  @Test
  public void testGetters() {
    CellDesignerEllipseTransformation ellipseTransformation = new CellDesignerEllipseTransformation();
    LineTransformation lineTransformation = new LineTransformation();
    CellDesignerPolygonTransformation polygonTransformation = new CellDesignerPolygonTransformation();
    CellDesignerRectangleTransformation rectangleTransformation = new CellDesignerRectangleTransformation();
    converter.setEllipseTransformation(ellipseTransformation);
    converter.setLineTransformation(lineTransformation);
    converter.setPolygonTransformation(polygonTransformation);
    converter.setRectangleTransformation(rectangleTransformation);

    assertEquals(ellipseTransformation, converter.getEllipseTransformation());
    assertEquals(lineTransformation, converter.getLineTransformation());
    assertEquals(polygonTransformation, converter.getPolygonTransformation());
    assertEquals(rectangleTransformation, converter.getRectangleTransformation());
  }

}
