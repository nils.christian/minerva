#! /bin/bash
BACKUP_FILE="/opt/backup/backup.tar.gz"
BACKUP_DIR="/tmp/backup/"
CATALINA_HOME="/var/lib/tomcat7"

rm -f $BACKUP_FILE 
rm -rdf $BACKUP_DIR
mkdir -p $BACKUP_DIR/tmp

cp $CATALINA_HOME/webapps/*.war "$BACKUP_DIR/tmp" 2>/dev/null

if [ -d "$CATALINA_HOME/webapps/map_images" ]
then
    cd $CATALINA_HOME/webapps/
    tar -zcf $BACKUP_DIR/tmp/map_images.tar.gz map_images
fi

export PGPASSWORD="123qweasdzxc"
pg_dump -U map_viewer map_viewer | gzip > "$BACKUP_DIR/tmp/map_viewer_db.gz"

cd $BACKUP_DIR/tmp
tar -zcf $BACKUP_FILE *
chown backup $BACKUP_FILE
cd /
rm -rf $BACKUP_DIR/tmp
