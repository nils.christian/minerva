#! /bin/bash

echo "Dump of MapViewer war file"

TIMESTAMP=$(date +"%F")
BACKUP_DIR="$BACKUP_DIR/$TIMESTAMP"

mkdir -p "$BACKUP_DIR/map_viewer"
cp $CATALINA_HOME/webapps/MapViewer.war "$BACKUP_DIR/map_viewer/"
cp $CATALINA_HOME/webapps/MapViewer.war $RESTORE_DIR
echo "Done"
