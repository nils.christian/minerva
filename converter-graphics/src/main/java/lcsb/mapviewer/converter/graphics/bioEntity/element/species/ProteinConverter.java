package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class defines methods used for drawing {@link Protein} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinConverter extends SpeciesConverter<Protein> {

  /**
   * Width of the ion part in the open channel representation.
   */
  private static final int ION_CHANNEL_WIDTH = 20;

  /**
   * Width of the gap in the open channel representation.
   */
  private static final int OPEN_ION_CHANNEL_WIDTH = 20;

  /**
   * How big should be the arc in rectangle for protein representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 10;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ProteinConverter.class.getName());

  /**
   * Helps in providing human readable identifiers of elements for logging.
   */
  private ElementUtils eu = new ElementUtils();

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public ProteinConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape of {@link Protein}.
   * 
   * @param protein
   *          {@link Protein} for which we are looking for a border
   * @return Shape object defining given alias
   */
  private Shape getGenericShape(final Protein protein) {
    return new RoundRectangle2D.Double(protein.getX(), protein.getY(), protein.getWidth(), protein.getHeight(),
        RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
  }

  @Override
  public void draw(final Protein protein, final Graphics2D graphics, final ConverterParams params) {
    // Local variable setting the SBGN visualization
    boolean sbgnFormat = params.isSbgnFormat();

    // Unit of information text (multimer cardinality and/or ion channel's
    // state)
    String unitOfInformationText = "";
    if (protein.getStatePrefix() != null && protein.getStateLabel() != null) {
      if (protein.getStatePrefix().equals("free input")) {
        unitOfInformationText = protein.getStateLabel();
      } else {
        unitOfInformationText = protein.getStatePrefix() + ":" + protein.getStateLabel();
      }
    }

    int homodir;
    if (sbgnFormat) {
      // If the SBGN display mode is set, multimer is shown as two stacked
      // glyphs
      if (protein.getHomodimer() > 1) {
        homodir = 2;
      } else {
        homodir = 1;
      }
    } else {
      homodir = protein.getHomodimer();
    }

    protein.setWidth(protein.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));
    protein.setHeight(protein.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));

    protein.setX(protein.getX() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));
    protein.setY(protein.getY() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));

    for (int homodimerId = 0; homodimerId < homodir; homodimerId++) {
      protein.setX(protein.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      protein.setY(protein.getY() - SpeciesConverter.HOMODIMER_OFFSET);

      Shape shape = null;
      if (protein instanceof GenericProtein || sbgnFormat) {
        shape = getGenericShape(protein);
        if (protein.getActivity() && !sbgnFormat) {
          drawActivityGenericProtein(protein, graphics);
        }
      } else if (protein instanceof IonChannelProtein) {
        Area a1;
        if (!protein.getActivity()) {
          a1 = new Area(
              new RoundRectangle2D.Double(protein.getX(), protein.getY(), protein.getWidth() - ION_CHANNEL_WIDTH - 1,
                  protein.getHeight(), RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE));
        } else {
          a1 = new Area(new RoundRectangle2D.Double(protein.getX(), protein.getY(),
              protein.getWidth() - ION_CHANNEL_WIDTH - OPEN_ION_CHANNEL_WIDTH - 1, protein.getHeight(),
              RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE));
        }
        Area a2 = new Area(
            new RoundRectangle2D.Double(protein.getX() + protein.getWidth() - ION_CHANNEL_WIDTH, protein.getY(),
                ION_CHANNEL_WIDTH, protein.getHeight(), RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE));
        a1.add(a2);
        shape = a1;
      } else if (protein instanceof TruncatedProtein) {
        shape = getTruncatedShape(protein);
        if (protein.getActivity()) {
          drawActivityTruncatedShape(protein, graphics);
        }
      } else if (protein instanceof ReceptorProtein) {
        shape = getReceptorShape(protein);
        if (protein.getActivity()) {
          drawActivityReceptorProtein(protein, graphics);
        }
      } else {
        logger.warn(eu.getElementTag(protein) + "Unknown shape for protein");
        shape = getDefaultAliasShape(protein);
      }
      Color c = graphics.getColor();
      graphics.setColor(protein.getColor());
      graphics.fill(shape);
      graphics.setColor(c);
      Stroke stroke = graphics.getStroke();
      graphics.setStroke(getBorderLine(protein));
      graphics.draw(shape);
      graphics.setStroke(stroke);

      // SBGN display mode - units of information and state variables are
      // printed on the top element only
      if (!sbgnFormat || (homodimerId == homodir - 1)) {
        for (ModificationResidue mr : protein.getModificationResidues()) {
          // SBGN display mode - print empty state variables
          drawModification(mr, graphics, sbgnFormat);
        }
        if (sbgnFormat) {
          // SBGN display mode - ion channel's state is marked as a unit of
          // information
          if (protein instanceof IonChannelProtein) {
            if (!unitOfInformationText.equals("")) {
              unitOfInformationText = unitOfInformationText.concat("; ");
            }
            if (protein.getActivity()) {
              unitOfInformationText = unitOfInformationText.concat("open");
            } else {
              unitOfInformationText = unitOfInformationText.concat("closed");
            }
          }

          // SBGN display mode - multimers have their cardinality printed as a
          // unit of information
          if (homodir > 1 && (unitOfInformationText == null || !unitOfInformationText.contains("N:"))) {
            if (!unitOfInformationText.equals("")) {
              unitOfInformationText = unitOfInformationText.concat("; ");
            }
            unitOfInformationText = unitOfInformationText.concat("N:").concat(Integer.toString(protein.getHomodimer()));
          }
        }
      }
    }

    if (unitOfInformationText.equals("")) {
      unitOfInformationText = null;
    }

    String text = protein.getStructuralState();
    drawStructuralState(text, protein, graphics);
    drawUnitOfInformation(unitOfInformationText, protein, graphics);
    drawText(protein, graphics, params);
    protein.setWidth(protein.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));
    protein.setHeight(protein.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));
  }

  /**
   * Draws activity border of {@link GenericProtein}.
   * 
   * @param protein
   *          {@link Protein} that will be drawn
   * @param graphics
   *          where we are drawing
   */
  private void drawActivityGenericProtein(final Protein protein, final Graphics2D graphics) {
    int border = ACTIVITY_BORDER_DISTANCE;
    protein.increaseBorder(border);
    Shape shape2 = getGenericShape(protein);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(LineType.DOTTED.getStroke());
    graphics.draw(shape2);
    graphics.setStroke(stroke);
    protein.increaseBorder(-border);
  }

  /**
   * Draws activity border of {@link ReceptorProtein}.
   * 
   * @param protein
   *          {@link Protein} that will be drawn
   * @param graphics
   *          where we are drawing
   */
  public void drawActivityReceptorProtein(final Protein protein, final Graphics2D graphics) {
    int border = ACTIVITY_BORDER_DISTANCE;
    protein.setX(protein.getX() - border);
    protein.setY(protein.getY() - border);
    protein.setWidth(protein.getWidth() + border * 2);
    protein.setHeight(protein.getHeight() + border * 2);
    Shape shape2 = getReceptorShape(protein);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(LineType.DOTTED.getStroke());
    graphics.draw(shape2);
    graphics.setStroke(stroke);
    protein.setX(protein.getX() + border);
    protein.setY(protein.getY() + border);
    protein.setWidth(protein.getWidth() - border * 2);
    protein.setHeight(protein.getHeight() - border * 2);
  }

  /**
   * Draws activity border of {@link TruncatedProtein}.
   * 
   * @param protein
   *          {@link Protein} that will be drawn
   * @param graphics
   *          where we are drawing
   */
  public void drawActivityTruncatedShape(final Protein protein, final Graphics2D graphics) {
    int border = ACTIVITY_BORDER_DISTANCE;
    protein.setX(protein.getX() - border);
    protein.setY(protein.getY() - border);
    protein.setWidth(protein.getWidth() + border * 2);
    protein.setHeight(protein.getHeight() + border * 2);
    Shape shape2 = getTruncatedShape(protein);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(LineType.DOTTED.getStroke());
    graphics.draw(shape2);
    graphics.setStroke(stroke);
    protein.setX(protein.getX() + border);
    protein.setY(protein.getY() + border);
    protein.setWidth(protein.getWidth() - border * 2);
    protein.setHeight(protein.getHeight() - border * 2);
  }

  /**
   * Returns shape of receptor protein.
   * 
   * @param protein
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  protected Shape getReceptorShape(final Protein protein) {
    Shape shape;
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD);
    ArrayList<Point2D> points = getReceptorPoints(protein);
    path.moveTo(points.get(0).getX(), points.get(0).getY());
    for (int i = 1; i < points.size(); i++) {
      path.lineTo(points.get(i).getX(), points.get(i).getY());
    }
    path.closePath();
    shape = path;
    return shape;
  }

  /**
   * Returns shape of truncated protein.
   * 
   * @param protein
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  protected Shape getTruncatedShape(final Protein protein) {
    Shape shape;
    GeneralPath path = new GeneralPath();
    // CHECKSTYLE:OFF
    path.moveTo(protein.getX() + 10, protein.getY());
    path.lineTo(protein.getX() + protein.getWidth(), protein.getY());
    path.lineTo(protein.getX() + protein.getWidth(), protein.getY() + protein.getHeight() * 3 / 5);
    path.lineTo(protein.getX() + protein.getWidth() * 4 / 5, protein.getY() + protein.getHeight() * 2 / 5);
    path.lineTo(protein.getX() + protein.getWidth() * 4 / 5, protein.getY() + protein.getHeight());
    path.lineTo(protein.getX() + 10, protein.getY() + protein.getHeight());
    path.curveTo(protein.getX() + 5, protein.getY() + protein.getHeight() - 2, protein.getX() + 2,
        protein.getY() + protein.getHeight() - 5, protein.getX(), protein.getY() + protein.getHeight() - 10);
    path.lineTo(protein.getX(), protein.getY() + 10);
    path.curveTo(protein.getX() + 2, protein.getY() + 5, protein.getX() + 5, protein.getY() + 2, protein.getX() + 10,
        protein.getY());
    // CHECKSTYLE:ON

    path.closePath();
    shape = path;
    return shape;
  }

  /**
   * Returns shape of receptor protein as a list of points.
   * 
   * @param protein
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  private ArrayList<Point2D> getReceptorPoints(final Protein protein) {
    double x = protein.getX();
    double y = protein.getY();
    double width = protein.getWidth();
    double height = protein.getHeight();
    ArrayList<Point2D> points = new ArrayList<Point2D>();

    // CHECKSTYLE:OFF
    points.add(new Point2D.Double(x, y + height * 2 / 5));
    points.add(new Point2D.Double(x, y));
    points.add(new Point2D.Double(x + width / 2, y + height / 5));
    points.add(new Point2D.Double(x + width, y));
    points.add(new Point2D.Double(x + width, y + height * 2 / 5));
    points.add(new Point2D.Double(x + width, y + height * 4 / 5));
    points.add(new Point2D.Double(x + width / 2, y + height));
    points.add(new Point2D.Double(x, y + height * 4 / 5));
    // CHECKSTYLE:ON

    return points;
  }

  @Override
  public PathIterator getBoundPathIterator(final Protein protein) {
    if (protein instanceof GenericProtein) {
      return getGenericShape(protein).getPathIterator(new AffineTransform());
    } else if (protein instanceof ReceptorProtein) {
      return getReceptorShape(protein).getPathIterator(new AffineTransform());
    } else if (protein instanceof IonChannelProtein) {
      return getGenericShape(protein).getPathIterator(new AffineTransform());
    } else if (protein instanceof TruncatedProtein) {
      return getTruncatedShape(protein).getPathIterator(new AffineTransform());
    } else {
      throw new InvalidArgumentException("Not implemented protein converter for type: " + protein.getClass());
    }
  }

}
