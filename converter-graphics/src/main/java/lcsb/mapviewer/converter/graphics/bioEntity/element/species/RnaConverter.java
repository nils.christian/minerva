/**
 * Default constructor.
 * 
 * @param colorExtractor
 *          Object that helps to convert {@link ColorSchema} values into colors
 *          when drawing {@link Species}
 */
package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * This class defines methods used for drawing {@link Rna} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class RnaConverter extends SpeciesConverter<Rna> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(RnaConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public RnaConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  public void draw(final Rna rna, final Graphics2D graphics, final ConverterParams params) {
    GeneralPath path = getRnaPath(rna);
    Color c = graphics.getColor();
    graphics.setColor(rna.getColor());
    graphics.fill(path);
    graphics.setColor(c);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(rna));
    graphics.draw(path);
    graphics.setStroke(stroke);

    for (ModificationResidue mr : rna.getRegions()) {
      drawModification(mr, graphics, false);
    }

    drawText(rna, graphics, params);
  }

  /**
   * Returns shape of the {@link Rna} as a {@link GeneralPath} object.
   * 
   * @param rna
   *          {@link Rna} for which we are looking for a border
   * @return {@link GeneralPath} object defining border of the given {@link Rna}
   */
  private GeneralPath getRnaPath(final Rna rna) {
    // CHECKSTYLE:OFF
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 4);
    path.moveTo(rna.getX() + rna.getWidth() / 4, rna.getY());
    path.lineTo(rna.getX() + rna.getWidth(), rna.getY());
    path.lineTo(rna.getX() + rna.getWidth() * 3 / 4, rna.getY() + rna.getHeight());
    path.lineTo(rna.getX(), rna.getY() + rna.getHeight());
    // CHECKSTYLE:ON
    path.closePath();
    return path;
  }

  @Override
  public PathIterator getBoundPathIterator(final Rna rna) {
    return getRnaPath(rna).getPathIterator(new AffineTransform());
  }

}
