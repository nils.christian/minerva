package lcsb.mapviewer.converter.graphics.bioEntity.reaction;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioEntity.BioEntityConverter;
import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;

/**
 * Thics class allows to draw reaction on the graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionConverter extends BioEntityConverter<Reaction> {

	/**
	 * When drawing operator this value defines radius of the joining operator
	 * circle.
	 */
	private static final int				DEFAULT_OPERATOR_RADIUS					= 6;

	/**
	 * Size of the rectangle drawn on the central line of the reaction.
	 */
	public static final double			RECT_SIZE												= 10;

	/**
	 * Default font size of reaction description.
	 */
	public static final int					DESCRIPTION_FONT_SIZE						= 10;

	/**
	 * Radius multiplier for bigger logic operators circles in SBGN view.
	 */
	public static final double			SBGN_RADIUS_MULTIPLIER					= 2.5;

	/**
	 * {@link ColorSchema} used for coloring reactions where we have more than one
	 * layout.
	 */
	public static final ColorSchema	DEFAULT_COLOR_SCHEMA						= new GenericColorSchema();

	/**
	 * Line width of #DEFAULT_COLOR_SCHEMA used for coloring reactions where we
	 * have more than one layout.
	 */
	private static final double			DEFAULT_COLOR_SCHEMA_LINE_WIDTH	= 3.0;

	static {
		DEFAULT_COLOR_SCHEMA.setColor(Color.BLACK);
		DEFAULT_COLOR_SCHEMA.setLineWidth(DEFAULT_COLOR_SCHEMA_LINE_WIDTH);
	}

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger				logger							= Logger.getLogger(ReactionConverter.class.getName());

	/**
	 * Font used for drawing reaction description on the map.
	 */
	private Font								descFont						= null;

	/**
	 * Graphical helper object with line transformation functions.
	 */
	private LineTransformation	lineTransformation	= new LineTransformation();

	/**
	 * This objects helps drawing arrows.
	 */
	private ArrowTransformation	arrowTransformation	= new ArrowTransformation();

	/**
	 * Object used to perform transformations on point objects.
	 */
	private PointTransformation	pointTransformation	= new PointTransformation();

	/**
	 * Object that helps to convert {@link ColorSchema} values into colors.
	 */
	private ColorExtractor			colorExtractor;

	/**
	 * Default constructor.
	 * 
	 * @param colorExtractor
	 *          Object that helps to convert {@link ColorSchema} values into
	 *          colors when drawing {@link Reaction}
	 */
	public ReactionConverter(ColorExtractor colorExtractor) {
		super();
		descFont = new Font(Font.SANS_SERIF, Font.BOLD, DESCRIPTION_FONT_SIZE);
		this.colorExtractor = colorExtractor;
	}

	/**
	 * This method draws a rectangle in the center of reaction (defined by line).
	 * 
	 * @param line
	 *          line on which the rectangle is drawn
	 * @param insideType
	 *          type of the rectangle to be drawn
	 * @param graphics
	 *          where the rectangle should be drawn
	 */
	protected void drawRectangleData(final Line2D line, final ReactionRect insideType, final Graphics2D graphics) {
		if (insideType == null) {
			graphics.draw(line);
		} else {
			Point2D startPoint = line.getP1();
			Point2D endPoint = line.getP2();
			double dx = endPoint.getX() - startPoint.getX();
			double dy = endPoint.getY() - startPoint.getY();
			double angle = Math.atan2(dy, dx);

			// find rectangle
			Rectangle2D rect = new Rectangle2D.Double();
			double pointX = (startPoint.getX() + endPoint.getX()) / 2;
			double pointY = (startPoint.getY() + endPoint.getY()) / 2;
			rect.setRect(pointX - RECT_SIZE / 2, pointY - RECT_SIZE / 2, RECT_SIZE, RECT_SIZE);

			// rotate graphics by the angle defined by line (instead of rotating
			// rectangle)
			graphics.rotate(angle, pointX, pointY);
			// fill rect
			Color color = graphics.getColor();
			graphics.setColor(Color.white);
			graphics.fill(rect);
			graphics.setColor(color);
			// draw rect border
			graphics.draw(rect);

			// draw text inside rect
			Font tmpFont = graphics.getFont();
			graphics.setFont(descFont);
			String insideDesc = insideType.getText();
			double textWidth = graphics.getFontMetrics().stringWidth(insideDesc);
			double textHeight = graphics.getFontMetrics().getAscent() - 2;
			graphics.drawString(insideDesc, (int) (pointX - textWidth / 2), (int) (pointY + textHeight / 2));
			graphics.setFont(tmpFont);

			// if we should put bolt character inside then do it
			if (insideType == ReactionRect.RECT_BOLT) {
				GeneralPath path = new GeneralPath();
				// CHECKSTYLE:OFF
				path.moveTo(pointX + 2, pointY - RECT_SIZE / 2 + 1);
				path.lineTo(pointX + 2, pointY + RECT_SIZE / 2 - 3);
				path.lineTo(pointX - 2, pointY - RECT_SIZE / 2 + 3);
				path.lineTo(pointX - 2, pointY + RECT_SIZE / 2 - 1);
				// CHECKSTYLE:ON
				graphics.draw(path);
			}

			// unrotate the graphics
			graphics.rotate(-angle, pointX, pointY);
		}
	}

	@Override
	public void draw(final Reaction reaction, final Graphics2D graphics, final ConverterParams params) {
		Color color = graphics.getColor();
		graphics.setColor(reaction.getReactants().get(0).getLine().getColor());
		// first reactants
		for (Reactant reactant : reaction.getReactants()) {
			if (isVisible(reactant, params)) {
				drawReactant(graphics, reactant);
			}
		}
		// now products
		for (Product product : reaction.getProducts()) {
			if (isVisible(product, params)) {
				drawProduct(graphics, product);
			}
		}
		// draw modifiers
		for (Modifier modifier : reaction.getModifiers()) {
			if (isVisible(modifier, params)) {
				drawModifier(graphics, modifier);
			}
		}

		// and operators
		for (NodeOperator operator : reaction.getOperators()) {
			if (isVisible(operator, params)) {
				drawOperator(graphics, operator, params.isSbgnFormat());
			}
		}

		// in the end we draw rectangle in the middle
		drawRectangleData(reaction.getCenterLine(), reaction.getReactionRect(), graphics);
		graphics.setColor(color);
	}

	@Override
	public void draw(final Reaction reaction, final Graphics2D graphics, final ConverterParams params, List<ColorSchema> visualizedLayoutsColorSchemas) {
		if (visualizedLayoutsColorSchemas.size() == 0) {
			draw(reaction, graphics, params);
		} else if (visualizedLayoutsColorSchemas.size() == 1) {
			if (visualizedLayoutsColorSchemas.get(0) == null) {
				draw(reaction, graphics, params);
			} else {
				List<Pair<AbstractNode, PolylineData>> oldData = new ArrayList<>();
				for (AbstractNode node : reaction.getNodes()) {
					oldData.add(new Pair<AbstractNode, PolylineData>(node, node.getLine()));
				}
				applyColorSchema(reaction, visualizedLayoutsColorSchemas.get(0));
				draw(reaction, graphics, params);
				for (Pair<AbstractNode, PolylineData> pair : oldData) {
					pair.getLeft().setLine(pair.getRight());
				}
			}
		} else {
			int count = 0;
			for (ColorSchema schema : visualizedLayoutsColorSchemas) {
				if (schema != null) {
					count++;
				}
			}
			if (count == 0) {
				draw(reaction, graphics, params);
			} else {
				List<Pair<AbstractNode, PolylineData>> oldData = new ArrayList<>();
				for (AbstractNode node : reaction.getNodes()) {
					oldData.add(new Pair<AbstractNode, PolylineData>(node, node.getLine()));
				}
				applyColorSchema(reaction, DEFAULT_COLOR_SCHEMA);
				draw(reaction, graphics, params);
				for (Pair<AbstractNode, PolylineData> pair : oldData) {
					pair.getLeft().setLine(pair.getRight());
				}
			}
		}
	}

	/**
	 * Modify reaction with data from {@link ColorSchema}.
	 * 
	 * @param reaction
	 *          reaction to modify
	 * @param colorSchema
	 *          {@link ColorSchema} to modify reaction
	 */
	private void applyColorSchema(Reaction reaction, ColorSchema colorSchema) {
		for (AbstractNode node : reaction.getNodes()) {
			PolylineData pd = new PolylineData(node.getLine());
			pd.setColor(colorExtractor.getNormalizedColor(colorSchema));
			if (colorSchema.getLineWidth() != null) {
				pd.setWidth(colorSchema.getLineWidth());
			}
			if (colorSchema.getReverseReaction() != null && colorSchema.getReverseReaction()) {
				ArrowTypeData atd = pd.getBeginAtd();
				pd.setBeginAtd(pd.getEndAtd());
				pd.setEndAtd(atd);
			}
			node.setLine(pd);
		}

	}

	/**
	 * Draw modifier on the graphics2d.
	 * 
	 * @param graphics
	 *          where we want to draw the object
	 * @param modifier
	 *          object to be drawn
	 */
	private void drawModifier(final Graphics2D graphics, final Modifier modifier) {
		// modifier consists only from the arrow
		arrowTransformation.drawLine(modifier.getLine(), graphics);
	}

	/**
	 * Draw operator on the graphics2d.
	 * 
	 * @param graphics
	 *          where we want to draw the object
	 * @param operator
	 *          object to be drawn
	 * @param sbgnFormat
	 *          true if operator is to be drawn in SBGN format
	 */
	private void drawOperator(final Graphics2D graphics, final NodeOperator operator, boolean sbgnFormat) {
		// draw line
		arrowTransformation.drawLine(operator.getLine(), graphics);

		// in SBGN view - draw connecting point only if it's not connected to
		// reactants or products
		if (!sbgnFormat || operator.getLine().getEndAtd().getArrowType() != ArrowType.NONE
				|| (!operator.getOutputs().isEmpty() && operator.getOutputs().get(0) instanceof NodeOperator)) {
			// and now connecting point
			Point2D centerPoint = operator.getLine().getPoints().get(0);
			int radius = DEFAULT_OPERATOR_RADIUS;
			// bigger connecting point circles in SBGN view
			if (sbgnFormat) {
				radius *= SBGN_RADIUS_MULTIPLIER;
			}
			// it's a circle
			Ellipse2D cir = new Ellipse2D.Double(centerPoint.getX() - radius, centerPoint.getY() - radius, 2 * radius, 2 * radius);
			Color color = graphics.getColor();
			graphics.setColor(Color.white);
			graphics.fill(cir);
			graphics.setColor(operator.getLine().getColor());
			graphics.draw(cir);
			// and text defined by operator
			String text;
			if (!sbgnFormat) {
				text = operator.getOperatorText();
			} else {
				text = operator.getSBGNOperatorText();
			}
			Rectangle2D rect = graphics.getFontMetrics().getStringBounds(text, graphics);
			graphics.drawString(text, (int) (centerPoint.getX() - rect.getWidth() / 2 + 1), (int) (centerPoint.getY() + rect.getHeight() / 2) - 2);
			graphics.setColor(color);
		}
	}

	/**
	 * Draw product on the graphics2D.
	 * 
	 * @param graphics
	 *          where we want to draw the object
	 * @param product
	 *          object to be drawn
	 */
	private void drawProduct(final Graphics2D graphics, final Product product) {
		arrowTransformation.drawLine(product.getLine(), graphics);
	}

	/**
	 * Draw reactant on the graphics2D.
	 * 
	 * @param graphics
	 *          where the reactant should be drawn
	 * @param reactant
	 *          object to be drawn on the graphics
	 */
	private void drawReactant(final Graphics2D graphics, final Reactant reactant) {
		arrowTransformation.drawLine(reactant.getLine(), graphics);
	}

	/**
	 * 
	 * @return {@link #descFont}
	 */
	protected Font getDescFont() {
		return descFont;
	}

	/**
	 * 
	 * @param descFont
	 *          new {@link #descFont} value
	 */
	protected void setDescFont(final Font descFont) {
		this.descFont = descFont;
	}

	/**
	 * 
	 * @return {@link #lineTransformation}
	 */
	protected LineTransformation getLineTransformation() {
		return lineTransformation;
	}

	/**
	 * 
	 * @param lineTransformation
	 *          new {@link #lineTransformation}
	 */
	protected void setLineTransformation(final LineTransformation lineTransformation) {
		this.lineTransformation = lineTransformation;
	}

	/**
	 * 
	 * @return {@link #arrowTransformation}
	 */
	protected ArrowTransformation getArrowTransformation() {
		return arrowTransformation;
	}

	/**
	 * 
	 * @param arrowTransformation
	 *          new {@link #arrowTransformation}
	 */
	protected void setArrowTransformation(final ArrowTransformation arrowTransformation) {
		this.arrowTransformation = arrowTransformation;
	}

	/**
	 * 
	 * @return {@link #pointTransformation}
	 */
	protected PointTransformation getPointTransformation() {
		return pointTransformation;
	}

	/**
	 * 
	 * @param pointTransformation
	 *          new {@link #pointTransformation}
	 */
	protected void setPointTransformation(final PointTransformation pointTransformation) {
		this.pointTransformation = pointTransformation;
	}

	@Override
	public void drawText(Reaction bioEntity, Graphics2D graphics, ConverterParams params) throws DrawingException {
	}

}