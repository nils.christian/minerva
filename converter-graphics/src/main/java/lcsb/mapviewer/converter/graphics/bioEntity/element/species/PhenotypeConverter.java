package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Phenotype;

/**
 * This class defines methods used for drawing SpeciesAlias of {@link Phenotype}
 * on the {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PhenotypeConverter extends SpeciesConverter<Phenotype> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(PhenotypeConverter.class.getName());

	/**
	 * Default constructor.
	 * 
	 * @param colorExtractor
	 *          Object that helps to convert {@link ColorSchema} values into
	 *          colors when drawing elements
	 * 
	 */
	public PhenotypeConverter(ColorExtractor colorExtractor) {
		super(colorExtractor);
	}

	@Override
	public void draw(Phenotype phenotype, final Graphics2D graphics, final ConverterParams params) {
		GeneralPath path = getPhenotypePath(phenotype);

		Color c = graphics.getColor();
		graphics.setColor(phenotype.getColor());
		graphics.fill(path);
		graphics.setColor(c);
		Stroke stroke = graphics.getStroke();
		graphics.setStroke(getBorderLine(phenotype));
		graphics.draw(path);
		graphics.setStroke(stroke);
		drawText(phenotype, graphics, params);
	}

	/**
	 * Returns shape of the {@link Phenotype} as a {@link GeneralPath} object.
	 * 
	 * @param phenotype
	 *          {@link Phenotype} for which we are looking for a border
	 * @return {@link GeneralPath} object defining border of the given
	 *         {@link Phenotype}
	 */
	private GeneralPath getPhenotypePath(final Phenotype phenotype) {
		// CHECKSTYLE:OFF
		GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 6);
		path.moveTo(phenotype.getX() + phenotype.getWidth() / 6, phenotype.getY());
		path.lineTo(phenotype.getX() + phenotype.getWidth() * 5 / 6, phenotype.getY());
		path.lineTo(phenotype.getX() + phenotype.getWidth(), phenotype.getY() + phenotype.getHeight() / 2);
		path.lineTo(phenotype.getX() + phenotype.getWidth() * 5 / 6, phenotype.getY() + phenotype.getHeight());
		path.lineTo(phenotype.getX() + phenotype.getWidth() / 6, phenotype.getY() + phenotype.getHeight());
		path.lineTo(phenotype.getX(), phenotype.getY() + phenotype.getHeight() / 2);
		// CHECKSTYLE:ON
		path.closePath();
		return path;
	}

	@Override
	public PathIterator getBoundPathIterator(Phenotype phenotype) {
		return getPhenotypePath(phenotype).getPathIterator(new AffineTransform());
	}

}
