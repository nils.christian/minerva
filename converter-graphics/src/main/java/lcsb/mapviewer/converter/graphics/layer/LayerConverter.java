package lcsb.mapviewer.converter.graphics.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;

import org.apache.log4j.Logger;

/**
 * This class allows to draw layer on Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerConverter {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger				logger							= Logger.getLogger(LayerConverter.class.getName());

	/**
	 * Color used for text frames.
	 */
	private static final Color	FRAME_COLOR					= Color.LIGHT_GRAY;

	/**
	 * Should the border around text be visible.
	 */
	private boolean							visibleTextBorder		= true;

	/**
	 * This objects helps drawing arrows.
	 */
	private ArrowTransformation	arrowTransformation	= new ArrowTransformation();

	/**
	 * Default constructor.
	 * 
	 * @param visibleTextBorder
	 *          should the text hav border
	 */
	public LayerConverter(final boolean visibleTextBorder) {
		this.visibleTextBorder = visibleTextBorder;
	}

	/**
	 * Draw the whole layer on the Graphics2D.
	 * 
	 * @param layer
	 *          object to be drawn
	 * @param graphics
	 *          where we want to draw the object
	 */
	public void drawLayer(final Layer layer, final Graphics2D graphics) {
		if (layer.isVisible()) {
			for (LayerText text : layer.getTexts()) {
				drawLayerText(text, graphics);
			}
			for (LayerRect rect : layer.getRectangles()) {
				drawLayerRect(rect, graphics);
			}
			for (LayerOval oval : layer.getOvals()) {
				drawLayerOval(oval, graphics);
			}
			for (PolylineData line : layer.getLines()) {
				drawLayerLine(line, graphics);
			}
		}

	}

	/**
	 * Draw text on the Graphics2D.
	 * 
	 * @param text
	 *          object to be drawn
	 * @param graphics
	 *          where we want to draw the object
	 */
	private void drawLayerText(final LayerText text, final Graphics2D graphics) {
		if (visibleTextBorder) {
			Color tmpColor = graphics.getColor();
			graphics.setColor(FRAME_COLOR);
			Rectangle2D rect = new Rectangle2D.Double(text.getX(), text.getY(), text.getWidth(), text.getHeight());
			graphics.draw(rect);
			graphics.setColor(tmpColor);
		}
		int x = text.getX().intValue() + 2;
		int y = text.getY().intValue();
		y += graphics.getFontMetrics().getHeight();
		graphics.drawString(text.getNotes(), x, y);

	}

	/**
	 * Draws rectangle on the Graphics2D.
	 * 
	 * @param rect
	 *          object to be drawn
	 * @param graphics
	 *          where we want to draw the object
	 */
	private void drawLayerRect(final LayerRect rect, final Graphics2D graphics) {
		Color tmpColor = graphics.getColor();
		graphics.setColor(rect.getColor());
		Rectangle2D rectangle = new Rectangle2D.Double(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
		graphics.draw(rectangle);
		graphics.setColor(tmpColor);

	}

	/**
	 * Draws line on the Graphics2D.
	 * 
	 * @param line
	 *          object to be drawn
	 * @param graphics
	 *          where we want to draw the object
	 */
	private void drawLayerLine(final PolylineData line, final Graphics2D graphics) {
		Color tmpColor = graphics.getColor();
		graphics.setColor(line.getColor());
		arrowTransformation.drawLine(line, graphics);

		graphics.setColor(tmpColor);

	}

	/**
	 * Draws oval on the Graphics2D.
	 * 
	 * @param oval
	 *          object to be drawn
	 * @param graphics
	 *          where we want to draw the object
	 */
	private void drawLayerOval(final LayerOval oval, final Graphics2D graphics) {
		Color tmpColor = graphics.getColor();
		graphics.setColor(oval.getColor());
		Ellipse2D o = new Ellipse2D.Double(oval.getX(), oval.getY(), oval.getWidth(), oval.getHeight());
		graphics.draw(o);
		graphics.setColor(tmpColor);

	}
}