package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * This class defines methods used for drawing {@link AntisenseRna} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */

public class AntisenseRnaConverter extends SpeciesConverter<AntisenseRna> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(AntisenseRnaConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public AntisenseRnaConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  public void draw(final AntisenseRna antisenseRna, final Graphics2D graphics, final ConverterParams params) {
    GeneralPath path = getAntisenseRnaPath(antisenseRna);
    Color c = graphics.getColor();
    graphics.setColor(antisenseRna.getColor());
    graphics.fill(path);
    graphics.setColor(c);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(antisenseRna));
    graphics.draw(path);
    graphics.setStroke(stroke);

    for (ModificationResidue mr : antisenseRna.getRegions()) {
      drawModification(mr, graphics, false);
    }

    drawText(antisenseRna, graphics, params);
  }

  /**
   * Returns {@link AntisenseRna} border as a {@link GeneralPath} object.
   * 
   * @param antisenseRna
   *          {@link AntisenseRna} for which we want to get border
   * @return border of the {@link AntisenseRna}
   */
  private GeneralPath getAntisenseRnaPath(final AntisenseRna antisenseRna) {
    // CHECKSTYLE:OFF
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 4);
    path.moveTo(antisenseRna.getX(), antisenseRna.getY());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth() * 3 / 4, antisenseRna.getY());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth(), antisenseRna.getY() + antisenseRna.getHeight());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth() / 4, antisenseRna.getY() + antisenseRna.getHeight());
    // CHECKSTYLE:ON
    path.closePath();
    return path;
  }

  @Override
  public PathIterator getBoundPathIterator(final AntisenseRna alias) {
    return getAntisenseRnaPath(alias).getPathIterator(new AffineTransform());
  }

}
