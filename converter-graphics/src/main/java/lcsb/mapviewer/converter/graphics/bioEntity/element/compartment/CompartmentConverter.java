package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.EllipseTransformation;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioEntity.element.ElementConverter;
import lcsb.mapviewer.converter.graphics.geometry.FontFinder;
import lcsb.mapviewer.converter.graphics.geometry.RectangleTooSmallException;
import lcsb.mapviewer.converter.graphics.placefinder.PlaceFinder;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Abstract class responsible for common methods to draw compartmentAliases on
 * Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class for which the comparator is created
 * 
 */
public abstract class CompartmentConverter<T extends Compartment> extends ElementConverter<T> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger					logger										 = Logger.getLogger(CompartmentConverter.class.getName());

	/**
	 * Default font size.
	 */
	private static final int			DEFAULT_FONT_SIZE					 = 10;

	/**
	 * Alpha level for inside of the transparent compartments.
	 */
	public static final int				DEFAULT_ALPHA_LEVEL				 = 8;

	/**
	 * Class used for transformation of lines.
	 */
	private LineTransformation		lineTransformation				 = new LineTransformation();

	/**
	 * Class used for transformation of ellipses.
	 */
	private EllipseTransformation	ellipseTransformation			 = new EllipseTransformation();

	/**
	 * Default alpha level for transparent compartments.
	 */
	private static int						alphaLevel								 = DEFAULT_ALPHA_LEVEL;

	/**
	 * Default alpha level for semi-transparent borders.
	 */
	protected static final int		HIGH_ALPHA_LEVEL					 = 127;

	/**
	 * Class used for finding place to draw desciption of the compartment.
	 */
	private PlaceFinder						placeFinder;

	/**
	 * Object used for synchronization when accessing {@link #placeFinder}.
	 */
	private String								placeFinderSynchronization = "";

	/**
	 * Object that helps to convert {@link ColorSchema} values into colors.
	 */
	private ColorExtractor				colorExtractor;

	/**
	 * Default constructor.
	 * 
	 * @param colorExtractor
	 *          Object that helps to convert {@link ColorSchema} values into
	 *          colors when drawing elements
	 * 
	 */
	protected CompartmentConverter(ColorExtractor colorExtractor) {
		this.colorExtractor = colorExtractor;
	};

	@Override
	public void drawText(final T compartment, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
		if (compartment.getWidth() < Configuration.EPSILON || compartment.getHeight() < Configuration.EPSILON) {
			throw new DrawingException(new ElementUtils().getElementTag(compartment) + "Dimension of the alias must be bigger than 0.");
		}
		boolean textCentered = !isTransparent(compartment, params);
		Rectangle2D border;
		if (textCentered) {
			synchronized (placeFinderSynchronization) {
				if (placeFinder == null || placeFinder.getModel() != compartment.getModelData()) {
					placeFinder = new PlaceFinder(compartment.getModelData().getModel());
				}
				border = placeFinder.getRetangle(compartment, params.getLevel());
			}
		} else {
			border = new Rectangle2D.Double(
					compartment.getNamePoint().getX(), compartment.getNamePoint().getY(),
					compartment.getWidth() - (compartment.getNamePoint().getX() - compartment.getX()),
					compartment.getHeight() - (compartment.getNamePoint().getY() - compartment.getY()));

		}
		double fontSize = DEFAULT_FONT_SIZE * params.getScale();
		if (compartment.getFontSize() != null) {
			fontSize = compartment.getFontSize() * params.getScale();
		}
		String fontName = Font.SANS_SERIF;
		try {
			fontSize = FontFinder.findMaxFontSize((int) Math.round(fontSize), fontName, graphics, border, compartment.getName());
			FontFinder.drawText((int) fontSize, fontName, graphics, border, compartment.getName(), textCentered);
		} catch (RectangleTooSmallException e) {
			// if it's too small then don't draw
			return;
		}
	}

	/**
	 * @return the lineTransformation
	 */
	protected LineTransformation getLineTransformation() {
		return lineTransformation;
	}

	/**
	 * @param lineTransformation
	 *          the lineTransformation to set
	 */
	protected void setLineTransformation(LineTransformation lineTransformation) {
		this.lineTransformation = lineTransformation;
	}

	/**
	 * @return the ellipseTransformation
	 */
	protected EllipseTransformation getEllipseTransformation() {
		return ellipseTransformation;
	}

	/**
	 * @param ellipseTransformation
	 *          the ellipseTransformation to set
	 */
	protected void setEllipseTransformation(EllipseTransformation ellipseTransformation) {
		this.ellipseTransformation = ellipseTransformation;
	}

	/**
	 * @return the alphaLevel
	 */
	public static int getAlphaLevel() {
		return alphaLevel;
	}

	/**
	 * @param alphaLevel
	 *          the alphaLevel to set
	 */
	public static void setAlphaLevel(int alphaLevel) {
		CompartmentConverter.alphaLevel = alphaLevel;
	}

	@Override
	public void draw(T alias, Graphics2D graphics, ConverterParams params, List<ColorSchema> visualizedLayoutsColorSchemas) throws DrawingException {
		draw(alias, graphics, params);

		Color oldColor = graphics.getColor();
		int count = 0;
		double width = alias.getWidth() / visualizedLayoutsColorSchemas.size();
		for (ColorSchema schema : visualizedLayoutsColorSchemas) {
			if (schema != null) {
				double startX = (double) count / (double) visualizedLayoutsColorSchemas.size();
				graphics.setColor(Color.BLACK);

				int x = (int) (startX * alias.getWidth() + alias.getX());
				graphics.drawRect(x, alias.getY().intValue(), (int) width, alias.getHeight().intValue());

				Color color = colorExtractor.getNormalizedColor(schema);
				Color bgAlphaColor = new Color(color.getRed(), color.getGreen(), color.getBlue(), LAYOUT_ALPHA);
				graphics.setColor(bgAlphaColor);
				graphics.fillRect(x, alias.getY().intValue(), (int) width, alias.getHeight().intValue());
			}
			count++;
		}
		graphics.setColor(oldColor);
	}

}
