package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;

import org.apache.log4j.Logger;

/**
 * Class responsible for drawing RightSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class RightSquareCompartmentConverter extends CompartmentConverter<RightSquareCompartment> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(RightSquareCompartmentConverter.class.getName());

	/**
	 * Default constructor.
	 * 
	 * @param colorExtractor
	 *          Object that helps to convert {@link ColorSchema} values into
	 *          colors when drawing {@link Species}
	 */
	public RightSquareCompartmentConverter(ColorExtractor colorExtractor) {
		super(colorExtractor);
	}

	@Override
	public void draw(final RightSquareCompartment compartment, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
		// keep the old values of color and line type
		Color oldColor = graphics.getColor();
		Stroke oldStroke = graphics.getStroke();

		// create shape of the compartment
		Shape s1 = new Line2D.Double(compartment.getX(), compartment.getHeight(), compartment.getX(), 0);
		Shape s3 = new Line2D.Double(compartment.getX() + compartment.getThickness(), compartment.getHeight(), compartment.getX(), 0);
		Area a1 = new Area(new Rectangle2D.Double(compartment.getX(), 0.0, compartment.getWidth(), compartment.getHeight()));

		Color c1 = compartment.getColor();
		Color c2 = new Color(c1.getRed(), c1.getGreen(), c1.getBlue(), HIGH_ALPHA_LEVEL);
		if (c1.equals(Color.WHITE)) {
			c1 = Color.BLACK;
		}

		// fill the background
		boolean fill = !isTransparent(compartment, params);
		if (fill) {
			graphics.setColor(c1);
		} else {
			Color bgAlphaColor = new Color(c1.getRed(), c1.getGreen(), c1.getBlue(), getAlphaLevel());
			graphics.setColor(bgAlphaColor);
		}
		graphics.fill(s1);

		// create borders
		graphics.setColor(c1);
		graphics.setStroke(LineType.SOLID_BOLD.getStroke());
		graphics.draw(s1);
		graphics.setStroke(LineType.SOLID.getStroke());
		graphics.draw(s3);
		graphics.setColor(c2);
		if (fill) {
			graphics.fill(a1);
		}
		// restore color and line type
		graphics.setColor(oldColor);
		graphics.setStroke(oldStroke);

		// three lines below are only temporary fix...
		double x = compartment.getNamePoint().getX() - compartment.getX();
		double y = compartment.getNamePoint().getY();
		compartment.getNamePoint().setLocation(x, y);

		// draw description
		if (fill) {
			Point2D tmpPoint = compartment.getNamePoint();
			compartment.setNamePoint(compartment.getCenter());
			drawText(compartment, graphics, params);
			compartment.setNamePoint(tmpPoint);
		}
		x += compartment.getX();
		compartment.getNamePoint().setLocation(x, y);
	}
}
