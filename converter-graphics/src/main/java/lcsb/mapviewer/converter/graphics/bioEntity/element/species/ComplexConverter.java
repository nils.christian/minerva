package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.geometry.FontFinder;
import lcsb.mapviewer.converter.graphics.geometry.RectangleTooSmallException;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing ComplexAlias on the graphics2d
 * object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexConverter extends SpeciesConverter<Complex> {

	/**
	 * How big is the triangle trimmed part of the complex.
	 */
	private static final int		TRIMMED_CORNER_SIZE	 = 5;

	/**
	 * Describes the distance between border of complex and internal border in
	 * brief view (without children).
	 */
	private static final double	INTERNAL_BORDER_DIST = 5.0;
	/**
	 * Default class logger.
	 */
	private static Logger				logger							 = Logger.getLogger(ComplexConverter.class.getName());

	/**
	 * Default constructor.
	 * 
	 * @param colorExtractor
	 *          Object that helps to convert {@link ColorSchema} values into
	 *          colors when drawing {@link Species}
	 */
	public ComplexConverter(ColorExtractor colorExtractor) {
		super(colorExtractor);
	}

	@Override
	public void draw(final Complex alias, final Graphics2D graphics, final ConverterParams params) {
		if (alias.getState().equalsIgnoreCase("complexnoborder")) {
			return;
		}

		int homodir;
		if (params.isSbgnFormat()) {
			// If the SBGN display mode is set, multimer is shown as two stacked
			// glyphs
			if (alias.getHomodimer() > 1) {
				homodir = 2;
			} else {
				homodir = 1;
			}
		} else {
			homodir = alias.getHomodimer();
		}

		alias.setWidth(alias.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (alias.getHomodimer() - 1));
		alias.setHeight(alias.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (alias.getHomodimer() - 1));

		alias.setX(alias.getX() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));
		alias.setY(alias.getY() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));

		for (int i = 0; i < homodir; i++) {
			alias.setX(alias.getX() - SpeciesConverter.HOMODIMER_OFFSET);
			alias.setY(alias.getY() - SpeciesConverter.HOMODIMER_OFFSET);

			GeneralPath path = getAliasPath(alias);

			Color c = graphics.getColor();
			Stroke stroke = graphics.getStroke();
			graphics.setColor(alias.getColor());
			graphics.fill(path);
			graphics.setColor(c);
			graphics.setStroke(getBorderLine(alias));
			graphics.draw(path);

			if (alias.getState().equals("brief")) {
				alias.increaseBorder(-INTERNAL_BORDER_DIST);
				path = getAliasPath(alias);
				alias.increaseBorder(INTERNAL_BORDER_DIST);
				graphics.setStroke(LineType.DOTTED.getStroke());
				graphics.draw(path);
			}
			if (alias.getActivity() && !params.isSbgnFormat()) {
				alias.increaseBorder(INTERNAL_BORDER_DIST);
				path = getAliasPath(alias);
				alias.increaseBorder(-INTERNAL_BORDER_DIST);
				graphics.setStroke(LineType.DOTTED.getStroke());
				graphics.draw(path);
			}
			graphics.setStroke(stroke);
		}

		alias.setWidth(alias.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (alias.getHomodimer() - 1));
		alias.setHeight(alias.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (alias.getHomodimer() - 1));

		// SBGN view - multimers are displayed with a unit of information containing
		// cardinality
		if (params.isSbgnFormat()) {
			String unitOfInformationText = null;
			if (alias.getStatePrefix() != null && alias.getStateLabel() != null) {
				unitOfInformationText = alias.getStatePrefix() + ":" + alias.getStateLabel();
			}
			if (homodir == 2 && (unitOfInformationText == null || !unitOfInformationText.contains("N:"))) {
				if (unitOfInformationText != null) {
					unitOfInformationText += "; ";
				} else {
					unitOfInformationText = "";
				}
				unitOfInformationText += "N:" + alias.getHomodimer();
			}

			drawUnitOfInformation(unitOfInformationText, alias, graphics);
		}

		String text = alias.getStructuralState();

		drawStructuralState(text, alias, graphics);

		drawText(alias, graphics, params);
	}

	@Override
	public void drawText(final Complex complex, final Graphics2D graphics, final ConverterParams params) {
		if (complex.getElements().size() > 0) {
			if (isTransparent(complex, params)) {
				super.drawText(complex, graphics, params);
				return;
			}
		}
		String text = getText(complex);
		try {
			double fontSize = DEFAULT_SPECIES_FONT_SIZE;
			if (complex.getFontSize() != null) {
				fontSize = complex.getFontSize();
			}
			int size = (int) FontFinder.findMaxFontSize(params.getScale() * fontSize, Font.SANS_SERIF, graphics, complex.getBorder(), text);
			FontFinder.drawText(size, Font.SANS_SERIF, graphics, complex.getBorder(), text);
		} catch (RectangleTooSmallException e) {
			logger.warn("Problem with finding font size", e);
			super.drawText(complex, graphics, params);
		}
	}

	/**
	 * Returns the border of {@link Complex}.
	 * 
	 * @param complex
	 *          exact object for which we want to get a border
	 * @return border of the {@link Complex}
	 */
	private GeneralPath getAliasPath(final Complex complex) {
		GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD);
		path.moveTo(complex.getX() + TRIMMED_CORNER_SIZE, complex.getY());
		path.lineTo(complex.getX() + complex.getWidth() - TRIMMED_CORNER_SIZE, complex.getY());
		path.lineTo(complex.getX() + complex.getWidth(), complex.getY() + TRIMMED_CORNER_SIZE);
		path.lineTo(complex.getX() + complex.getWidth(), complex.getY() + complex.getHeight() - TRIMMED_CORNER_SIZE);
		path.lineTo(complex.getX() + complex.getWidth() - TRIMMED_CORNER_SIZE, complex.getY() + complex.getHeight());
		path.lineTo(complex.getX() + TRIMMED_CORNER_SIZE, complex.getY() + complex.getHeight());
		path.lineTo(complex.getX(), complex.getY() + complex.getHeight() - TRIMMED_CORNER_SIZE);
		path.lineTo(complex.getX(), complex.getY() + TRIMMED_CORNER_SIZE);
		path.closePath();
		return path;
	}

	@Override
	public PathIterator getBoundPathIterator(final Complex complex) {
		return getAliasPath(complex).getPathIterator(new AffineTransform());
	}
}
