package lcsb.mapviewer.converter.graphics.bioEntity.element.compartment;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing SquareCompartment on the {@link Graphics2D}.
 * 
 * @author Piotr Gawron
 * 
 */
public class SquareCompartmentConverter extends CompartmentConverter<SquareCompartment> {
	/**
	 * How big is the arc in the corner of rectangle that represents square
	 * compartment.
	 */
	private static final int RECTANGLE_CORNER_ARC_SIZE = 20;
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger		 logger										 = Logger.getLogger(SquareCompartmentConverter.class.getName());

	/**
	 * Default constructor.
	 * 
	 * @param colorExtractor
	 *          Object that helps to convert {@link ColorSchema} values into
	 *          colors when drawing {@link Species}
	 */
	public SquareCompartmentConverter(ColorExtractor colorExtractor) {
		super(colorExtractor);
	}

	/**
	 * Returns shape representing compartment.
	 * 
	 * @param compartment
	 *          compartment for which we are looking for a {@link Shape}
	 * @return {@link Shape} object that represents compartment
	 */
	private Shape getShape(final Compartment compartment) {
		return new RoundRectangle2D.Double(
				compartment.getX(), compartment.getY(), compartment.getWidth(), compartment.getHeight(), RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
	}

	@Override
	public void draw(final SquareCompartment compartment, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
		// keep the old values of color and line type
		Color oldColor = graphics.getColor();
		Stroke oldStroke = graphics.getStroke();

		// create shape of the compartment
		Shape s1 = getShape(compartment);
		compartment.increaseBorder(-compartment.getThickness());
		Shape s3 = getShape(compartment);
		compartment.increaseBorder(compartment.getThickness());

		Color c1 = compartment.getColor();
		Color c2 = new Color(c1.getRed(), c1.getGreen(), c1.getBlue(), HIGH_ALPHA_LEVEL);
		if (c1.equals(Color.WHITE)) {
			c1 = Color.BLACK;
		}

		Area a1 = new Area(s1);
		a1.subtract(new Area(s3));

		// fill the background
		boolean fill = !isTransparent(compartment, params);
		if (fill) {
			graphics.setColor(c1);
		} else {
			Color bgAlphaColor = new Color(c1.getRed(), c1.getGreen(), c1.getBlue(), getAlphaLevel());
			graphics.setColor(bgAlphaColor);
		}
		graphics.fill(s1);

		// create borders
		graphics.setColor(c1);
		graphics.setStroke(LineType.SOLID_BOLD.getStroke());
		graphics.draw(s1);
		graphics.setStroke(LineType.SOLID.getStroke());
		graphics.draw(s3);
		graphics.setColor(c2);
		graphics.fill(a1);

		// restore color and line type
		graphics.setColor(oldColor);
		graphics.setStroke(oldStroke);

		// draw description
		if (fill) {
			Point2D tmpPoint = compartment.getNamePoint();
			compartment.setNamePoint(compartment.getCenter());
			drawText(compartment, graphics, params);
			compartment.setNamePoint(tmpPoint);
		}
	}
}
