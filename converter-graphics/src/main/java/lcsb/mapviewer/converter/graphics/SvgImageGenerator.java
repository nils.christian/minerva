package lcsb.mapviewer.converter.graphics;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.exception.NotImplementedException;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

/**
 * This class implements SVG generator for the image object.This class uses
 * batik library to create all converters.
 * 
 * @author Piotr Gawron
 * 
 */
public class SvgImageGenerator extends AbstractImageGenerator {

	@Override
	protected void createImageObject(final double width, final double height) {

		// Get a DOMImplementation.
		DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

		// Create an instance of org.w3c.dom.Document.
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);

		// Create an instance of the SVG Generator.
		setGraphics(new SVGGraphics2D(document));

	}

	@Override
	public void saveToFileImplementation(final String fileName) throws SVGGraphics2DIOException {
		((SVGGraphics2D) getGraphics()).stream(fileName);
	}

	/**
	 * Default constructor. Create an image that is described by params. For more
	 * information see
	 * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
	 * params}.
	 * 
	 * @param params
	 *          parameters used for the image creation.
	 * @throws DrawingException
	 *           thrown when there was a problem with drawing a map
	 */
	public SvgImageGenerator(final Params params) throws DrawingException {
		super(params);
	}

	@Override
	protected void closeImageObject() {
	}

	@Override
	public void saveToOutputStreamImplementation(OutputStream os) throws IOException {
		((SVGGraphics2D) getGraphics()).stream(new OutputStreamWriter(os));

	}

	@Override
	public void savePartToFileImplementation(int x, int y, int width, int height, String fileName) throws IOException {
		throw new NotImplementedException("Partial save is not implemented in SVG image generator");
	}

	@Override
	public void savePartToOutputStreamImplementation(int x, int y, int width, int height, OutputStream os) throws IOException {
		throw new NotImplementedException("Partial save is not implemented in SVG image generator");
	}

	@Override
	public String getFormatName() {
		return "SVG image";
	}

	@Override
	public MimeType getMimeType() {
		return MimeType.SVG;
	}

	@Override
	public String getFileExtension() {
		return "svg";
	}

}
