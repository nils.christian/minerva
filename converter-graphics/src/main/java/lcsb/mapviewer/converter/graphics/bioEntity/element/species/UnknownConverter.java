package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;

/**
 * This class defines methods used for drawing {@link Unknown} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownConverter extends SpeciesConverter<Unknown> {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(UnknownConverter.class.getName());

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public UnknownConverter(ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  public void draw(Unknown unknown, final Graphics2D graphics, final ConverterParams params) {
    if (unknown.getActivity()) {
      int border = ACTIVITY_BORDER_DISTANCE;
      unknown.increaseBorder(border);
      Shape shape2 = new Ellipse2D.Double(unknown.getX(), unknown.getY(), unknown.getWidth(), unknown.getHeight());
      Stroke stroke = graphics.getStroke();
      graphics.setStroke(LineType.DOTTED.getStroke());
      graphics.draw(shape2);
      graphics.setStroke(stroke);
      unknown.increaseBorder(-border);
    }

    Shape shape = new Ellipse2D.Double(unknown.getX(), unknown.getY(), unknown.getWidth(), unknown.getHeight());
    Color c = graphics.getColor();
    graphics.setColor(unknown.getColor());
    graphics.fill(shape);
    graphics.setColor(c);
    drawText(unknown, graphics, params);
  }

  @Override
  public PathIterator getBoundPathIterator(Unknown unknown) {
    throw new InvalidStateException("This class doesn't provide boundPath");
  }

}
