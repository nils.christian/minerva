/**
 * This package contains converter from our model into a graphic representation
 * that can be saved in the graphic file. There are two main application:
 * <ul>
 * <li>generation of the single graphic file that represents the map - in this
 * case direct usage of implementation of
 * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator
 * AbstractImageGenerator} is recommended. There are two implementation of this
 * class:
 * <ul>
 * <li> {@link lcsb.mapviewer.converter.graphics.NormalImageGenerator
 * NormalImageGenerator} - allows to generate images in PNG and JPG format.</li>
 * <li> {@link lcsb.mapviewer.converter.graphics.SvgImageGenerator
 * SvgImageGenerator} - allows to generate images in SVG format.</li>
 * </ul>
 * It is crucial to remember that too big maps will create very big files which
 * might cause Out of Memory exception</li>
 * <li>generation of the set of files that represent whole image as set of small
 * non-overlapping parts - in this case
 * {@link lcsb.mapviewer.converter.graphics.MapGenerator MapGenerator} class
 * provides interface that generates these images. The images can be used later
 * on by <a href="https://developers.google.com/maps/">Google Maps API</a>.</li>
 * </ul>
 * 
 */
package lcsb.mapviewer.converter.graphics;

