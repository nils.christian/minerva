package lcsb.mapviewer.converter.graphics.bioEntity.element;

import lcsb.mapviewer.converter.graphics.bioEntity.BioEntityConverter;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This interface defines what operations should be possible to convert
 * {@link Element} into a graphics on Graphics2D object.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class of alias to convert
 */
public abstract class ElementConverter<T extends Element> extends BioEntityConverter<T> {

}
