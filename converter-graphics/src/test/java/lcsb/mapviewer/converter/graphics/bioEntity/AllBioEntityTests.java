package lcsb.mapviewer.converter.graphics.bioEntity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.graphics.bioEntity.element.species.AllSpeciesConverterTests;
import lcsb.mapviewer.converter.graphics.bioEntity.reaction.AllReactionTests;

@RunWith(Suite.class)
@SuiteClasses({ //
		AllSpeciesConverterTests.class, //
		AllReactionTests.class, //
		BioEntityConverterImplTest.class//
})
public class AllBioEntityTests {

}
