package lcsb.mapviewer.converter.graphics;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.mockito.Mockito;

public abstract class GraphicsTestFunctions {
	protected Graphics2D createGraphicsMock() {
		Graphics2D graphics = Mockito.mock(Graphics2D.class);
		FontMetrics fontMetrics = Mockito.mock(FontMetrics.class);
		when(fontMetrics.getStringBounds(any(), any())).thenReturn(new Rectangle2D.Double());
		when(graphics.getFontMetrics()).thenReturn(fontMetrics);
		return graphics;
	}

}
