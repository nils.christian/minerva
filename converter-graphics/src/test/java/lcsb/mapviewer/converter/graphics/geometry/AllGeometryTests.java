package lcsb.mapviewer.converter.graphics.geometry;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FontFinderTest.class, ArrowTransformationTest.class })
public class AllGeometryTests {

}
