package lcsb.mapviewer.converter.graphics;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class AbstractImageGeneratorTest extends GraphicsTestFunctions {
	Logger logger = Logger.getLogger(AbstractImageGeneratorTest.class);

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrawSimpleMap() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();

			Model model = createSimpleModel();

			AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
			gen.draw();

			//3 times for proteins and 4 times for reaction
			verify(graphics, times(7)).draw(any());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawSimpleMapWithNesting() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();

			Model model = createSimpleModel();

			AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
			gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true));
			gen.draw();

			//3 times for proteins and 4 times for reaction
			verify(graphics, times(7)).draw(any());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawSimpleMapWithWhenNestingHidesElement() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();

			Model model = createSimpleModel();
			model.getElementByElementId("s1").setVisibilityLevel(2);

			AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
			gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true).level(0));
			gen.draw();

			//2 times for proteins and 3 times for reaction
			verify(graphics, times(5)).draw(any());
		} catch (Exception e) {
			throw e;
		}
	}

	private Model createSimpleModel() {
		Model model = new ModelFullIndexed(null);
		model.setWidth(100);
		model.setHeight(100);

		GenericProtein protein1 = new GenericProtein("s1");
		protein1.setX(10);
		protein1.setY(10);
		protein1.setWidth(10);
		protein1.setHeight(10);
		model.addElement(protein1);

		GenericProtein protein2 = new GenericProtein("s2");
		protein2.setX(30);
		protein2.setY(10);
		protein2.setWidth(10);
		protein2.setHeight(10);
		model.addElement(protein2);

		GenericProtein protein3 = new GenericProtein("s3");
		protein3.setX(40);
		protein3.setY(10);
		protein3.setWidth(10);
		protein3.setHeight(10);
		model.addElement(protein3);

		Reaction reaction = new Reaction();

		Modifier modifier = new Catalysis(protein1);
		modifier.setLine(new PolylineData(new Point2D.Double(100, 20), new Point2D.Double(100, 80)));
		modifier.getLine().setWidth(1.0);

		Reactant reactant = new Reactant(protein2);
		reactant.setLine(new PolylineData(new Point2D.Double(90, 90), new Point2D.Double(10, 90)));
		reactant.getLine().setWidth(1.0);
		Product product = new Product(protein3);
		product.setLine(new PolylineData(new Point2D.Double(200, 90), new Point2D.Double(110, 90)));
		product.getLine().setWidth(1.0);
		reaction.addModifier(modifier);
		reaction.addProduct(product);
		reaction.addReactant(reactant);

		model.addReaction(reaction);

		return model;
	}

	private AbstractImageGenerator createAbstractImageGeneratorMock(Graphics2D graphics, Model model) throws Exception {
		AbstractImageGenerator result = Mockito.mock(AbstractImageGenerator.class, Mockito.CALLS_REAL_METHODS);
		result.setGraphics(graphics);
		result.setParams(new AbstractImageGenerator.Params().model(model).level(0));
		return result;

	}

}
