package lcsb.mapviewer.converter.graphics.geometry;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.PolylineData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArrowTransformationTest {

	ArrowTransformation	at	= new ArrowTransformation();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrawingArrows() throws Exception {
		try {
			int coordX = 0;
			int coordY = 10;
			int height = 60;

			for (ArrowType type : ArrowType.values()) {
				BufferedImage bi = new BufferedImage((int) 65, (int) 20, BufferedImage.TYPE_INT_ARGB);
				Graphics2D graphics = bi.createGraphics();

//				graphics.fill(new Rectangle2D.Double(0, 0, 100, 40));
				graphics.setColor(Color.BLACK);

				PolylineData pd = new PolylineData(new Point2D.Double(coordX, coordY), new Point2D.Double(coordX + height, coordY));
				pd.getEndAtd().setArrowType(type);
				at.drawLine(pd, graphics);

//				FileOutputStream fos = new FileOutputStream(new File(type.name() + ".png"));
//				ImageIO.write(bi, "PNG", fos);
//				fos.close();
//
//				Desktop.getDesktop().open(new File(type.name() + ".png"));

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
