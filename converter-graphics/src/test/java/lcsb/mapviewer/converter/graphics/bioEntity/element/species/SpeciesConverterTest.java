package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

public class SpeciesConverterTest {
  Logger logger = Logger.getLogger(SpeciesConverterTest.class);

  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawAliasWithLayouts() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();
      ProteinConverter rc = new ProteinConverter(colorExtractor);

      GenericProtein alias = createProtein();
      rc.draw(alias, graphics, new ConverterParams());

      int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

      GenericProtein alias2 = createProtein();

      bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      graphics = bi.createGraphics();

      ColorSchema schema = new GenericColorSchema();
      schema.setColor(Color.RED);
      List<ColorSchema> schemas = new ArrayList<>();
      schemas.add(schema);

      rc.draw(alias2, graphics, new ConverterParams(), schemas);

      int val2 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

      assertTrue(val != val2);

    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testDrawAfterDrawingReactionWithLayouts() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();
      ProteinConverter rc = new ProteinConverter(colorExtractor);

      GenericProtein alias = createProtein();
      rc.draw(alias, graphics, new ConverterParams());

      int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

      GenericProtein alias2 = createProtein();

      bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      graphics = bi.createGraphics();

      ColorSchema schema = new GenericColorSchema();
      schema.setColor(Color.RED);
      List<ColorSchema> schemas = new ArrayList<>();
      schemas.add(schema);

      rc.draw(alias2, graphics, new ConverterParams(), schemas);

      int val2 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

      bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      graphics = bi.createGraphics();

      rc.draw(alias2, graphics, new ConverterParams(), new ArrayList<>());

      int val3 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

      assertTrue(val != val2);
      assertEquals(val, val3);

    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testDrawReactionWithFewLayouts() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();
      ProteinConverter rc = new ProteinConverter(colorExtractor);

      GenericProtein alias = createProtein();
      rc.draw(alias, graphics, new ConverterParams());

      int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

      GenericProtein alias2 = createProtein();

      bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      graphics = bi.createGraphics();

      ColorSchema schema = new GenericColorSchema();
      schema.setColor(Color.RED);
      List<ColorSchema> schemas = new ArrayList<>();
      schemas.add(schema);
      schemas.add(null);
      schema = new GenericColorSchema();
      schema.setColor(Color.BLUE);
      schemas.add(schema);

      rc.draw(alias2, graphics, new ConverterParams(), schemas);

      int val2 = bi.getRGB((int) (alias.getX() + alias.getWidth() / 4), (int) alias.getCenterY());
      int val3 = bi.getRGB((int) (alias.getCenterX()), (int) alias.getCenterY());
      int val4 = bi.getRGB((int) (alias.getX() + 3 * alias.getWidth() / 4), (int) alias.getCenterY());

      assertTrue(val != val2);
      assertEquals(val, val3);
      assertTrue(val != val4);

    } catch (Exception e) {
      throw e;
    }
  }

  @Test
  public void testDrawResidue() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      ModificationResidue residue = new Residue();
      residue.setPosition(new Point2D.Double(10,10));
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(residue, graphics, false);
      verify(converter, times(1)).drawResidue(any(), any(), anyBoolean());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDrawBindingRegion() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      Protein protein = createProtein();
      BindingRegion bindingRegion = new BindingRegion();
      bindingRegion.setPosition(new Point2D.Double(10,10));
      bindingRegion.setWidth(100.0);
      bindingRegion.setHeight(10.0);
      protein.addModificationResidue(bindingRegion);
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(bindingRegion, graphics, false);
      verify(converter, times(1)).drawBindingRegion(any(), any());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDrawModificationSite() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      ModificationSite modificationSite = new ModificationSite();
      modificationSite.setPosition(new Point2D.Double(10,10));
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(modificationSite, graphics, false);
      verify(converter, times(1)).drawModificationSite(any(), any(), anyBoolean());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDrawProteinBindingRegion() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      ProteinBindingDomain proteinBindingRegion = new ProteinBindingDomain();
      proteinBindingRegion.setPosition(new Point2D.Double(10,10));
      proteinBindingRegion.setWidth(100);
      proteinBindingRegion.setHeight(20);
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(proteinBindingRegion, graphics, false);
      verify(converter, times(1)).drawProteinBindingDomain(any(), any());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDrawCodingRegion() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      CodingRegion codingRegion = new CodingRegion();
      codingRegion.setPosition(new Point2D.Double(10,10));
      codingRegion.setWidth(100);
      codingRegion.setHeight(20);
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(codingRegion, graphics, false);
      verify(converter, times(1)).drawCodingRegion(any(), any());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDrawRegulatoryRegion() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      RegulatoryRegion regulatoryRegion = new RegulatoryRegion();
      regulatoryRegion.setPosition(new Point2D.Double(10,10));
      regulatoryRegion.setWidth(100);
      regulatoryRegion.setHeight(20);
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(regulatoryRegion, graphics, false);
      verify(converter, times(1)).drawRegulatoryRegion(any(), any());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDrawTranscriptionSite() throws Exception {
    try {
      BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bi.createGraphics();

      TranscriptionSite transcriptionSite = new TranscriptionSite();
      transcriptionSite.setPosition(new Point2D.Double(10,10));
      transcriptionSite.setWidth(100);
      transcriptionSite.setHeight(20);
      transcriptionSite.setActive(true);
      transcriptionSite.setDirection("LEFT");
      
      ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
      converter.drawModification(transcriptionSite, graphics, false);
      verify(converter, times(1)).drawTranscriptionSite(any(), any());
      
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private GenericProtein createProtein() {
    GenericProtein protein = new GenericProtein("id");
    protein.setName("NAME_OF_THE_ELEMENT");
    protein.setX(10);
    protein.setY(20);
    protein.setWidth(100);
    protein.setHeight(80);
    protein.setColor(Color.WHITE);

    return protein;
  }
}
