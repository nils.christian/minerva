package lcsb.mapviewer.converter.graphics.geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FontFinderTest  {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindFontSize1() throws Exception {
		try {
			String text = "Some text to write";
			int coordX = 20;
			int coordY = 40;
			int height = 20;

			BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

			Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

			double size = FontFinder.findMaxFontSize(13, Font.SANS_SERIF, graphics, border, text);

			assertTrue(size < 13);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testFindFontSize2() throws Exception {
		try {
			String text = "Some text to write";
			int coordX = 20;
			int coordY = 40;
			int height = 60;

			BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

			Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

			double size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, text);

			graphics.setColor(Color.BLACK);
			graphics.draw(border);
			// graphics.setFont(new Font(Font.SANS_SERIF, 0, (int) size));
			FontFinder.drawText((int) size, Font.SANS_SERIF, graphics, border, text);

			// graphics.drawString(text, coordX, coordY + height);
			//
			// FileOutputStream fos = new FileOutputStream(new File("tmp.png"));
			// ImageIO.write(bi, "PNG", fos);
			// fos.close();
			//
			// Desktop.getDesktop().open(new File("tmp.png"));

			//different os compute it differnetly ...
            assertTrue(size>18);
            assertTrue(size<=20);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testFindFontSize3() throws Exception {
		try {
			String text = "Some\ntext\n to write";
			int coordX = 20;
			int coordY = 40;
			int height = 60;

			BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

			Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

			double size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, text);

			graphics.setColor(Color.BLACK);
			graphics.draw(border);
			// graphics.setFont(new Font(Font.SANS_SERIF, 0, (int) size));
			FontFinder.drawText((int) size, Font.SANS_SERIF, graphics, border, text);

			assertTrue(size < 20);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testFindFontSize4() throws Exception {
		try {
			String text = "";
			int coordX = 20;
			int coordY = 40;
			int height = 60;

			BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

			Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

			double size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, text);

			assertEquals(20, (int) size);
			size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, " ");

			assertEquals(20, (int) size);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

}
