package lcsb.mapviewer.converter.graphics.bioEntity.element.species;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({  SpeciesConverterTest.class })
public class AllSpeciesConverterTests {

}
