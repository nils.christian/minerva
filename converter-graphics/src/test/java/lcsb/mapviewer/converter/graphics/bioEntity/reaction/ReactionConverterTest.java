package lcsb.mapviewer.converter.graphics.bioEntity.reaction;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.SplitOperator;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ReactionConverterTest extends GraphicsTestFunctions {

	ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrawReactionWithLayouts() throws Exception {
		try {
			BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(5.0);
			rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", outputStream);
			byte[] output1 = outputStream.toByteArray();

			Reaction reaction2 = createReaction(1.0);

			bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			graphics = bi.createGraphics();

			ColorSchema schema = new GenericColorSchema();
			schema.setColor(Color.BLACK);
			schema.setLineWidth(5.0);
			List<ColorSchema> schemas = new ArrayList<>();
			schemas.add(schema);

			rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), schemas);

			outputStream = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", outputStream);
			byte[] output2 = outputStream.toByteArray();

			// FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
			// FileUtils.writeByteArrayToFile(new File("tmp2.png"), output2);
			//
			// Desktop.getDesktop().open(new File("tmp.png"));
			// Desktop.getDesktop().open(new File("tmp2.png"));

			assertTrue(Arrays.equals(output1, output2));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawAfterDrawingReactionWithLayouts() throws Exception {
		try {
			BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(1.0);
			rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", outputStream);
			byte[] output1 = outputStream.toByteArray();

			Reaction reaction2 = createReaction(1.0);

			bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			graphics = bi.createGraphics();

			ColorSchema schema = new GenericColorSchema();
			schema.setColor(Color.BLACK);
			schema.setLineWidth(5.0);
			List<ColorSchema> schemas = new ArrayList<>();
			schemas.add(schema);

			rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), schemas);
			bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			graphics = bi.createGraphics();
			rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), new ArrayList<>());

			outputStream = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", outputStream);
			byte[] output2 = outputStream.toByteArray();

			// FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
			// FileUtils.writeByteArrayToFile(new File("tmp2.png"), output2);
			//
			// Desktop.getDesktop().open(new File("tmp.png"));
			// Desktop.getDesktop().open(new File("tmp2.png"));

			assertTrue(Arrays.equals(output1, output2));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawReactionWithFewLayouts() throws Exception {
		try {
			BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = bi.createGraphics();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(3.0);
			rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", outputStream);
			byte[] output1 = outputStream.toByteArray();

			Reaction reaction2 = createReaction(1.0);

			bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
			graphics = bi.createGraphics();

			ColorSchema schema = new GenericColorSchema();
			schema.setColor(Color.BLACK);
			schema.setLineWidth(5.0);
			List<ColorSchema> schemas = new ArrayList<>();
			schemas.add(schema);
			schemas.add(schema);

			rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), schemas);

			outputStream = new ByteArrayOutputStream();
			ImageIO.write(bi, "PNG", outputStream);
			byte[] output2 = outputStream.toByteArray();

			// FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
			// FileUtils.writeByteArrayToFile(new File("tmp2.png"), output2);
			//
			// Desktop.getDesktop().open(new File("tmp.png"));
			// Desktop.getDesktop().open(new File("tmp2.png"));

			assertTrue(Arrays.equals(output1, output2));

		} catch (Exception e) {
			throw e;
		}
	}

	private Reaction createReaction(double lineWidth) {
		Reaction result = new Reaction();

		Modifier modifier = new Catalysis(new GenericProtein("s1"));
		modifier.setLine(new PolylineData(new Point2D.Double(100, 20), new Point2D.Double(100, 80)));
		modifier.getLine().setWidth(lineWidth);

		Reactant reactant = new Reactant(new GenericProtein("s2"));
		reactant.setLine(new PolylineData(new Point2D.Double(90, 90), new Point2D.Double(10, 90)));
		reactant.getLine().setWidth(lineWidth);
		Product product = new Product(new GenericProtein("s3"));
		product.setLine(new PolylineData(new Point2D.Double(200, 90), new Point2D.Double(110, 90)));
		product.getLine().setWidth(lineWidth);
		result.addModifier(modifier);
		result.addProduct(product);
		result.addReactant(reactant);
		return result;
	}

	private Reaction createComplexReaction(double lineWidth) {
		Reaction result = new Reaction();

		Modifier modifier1 = new Catalysis(new GenericProtein("s1-1"));
		modifier1.setLine(new PolylineData(new Point2D.Double(80, 20), new Point2D.Double(100, 40)));
		modifier1.getLine().setWidth(lineWidth);

		Modifier modifier2 = new Catalysis(new GenericProtein("s1-2"));
		modifier2.setLine(new PolylineData(new Point2D.Double(120, 20), new Point2D.Double(100, 40)));
		modifier2.getLine().setWidth(lineWidth);

		AndOperator modifierOperator = new AndOperator();
		modifierOperator.addInput(modifier1);
		modifierOperator.addInput(modifier2);
		modifierOperator.setLine(new PolylineData(new Point2D.Double(100, 40), new Point2D.Double(100, 80)));

		Reactant reactant1 = new Reactant(new GenericProtein("s2-1"));
		reactant1.setLine(new PolylineData(new Point2D.Double(60, 90), new Point2D.Double(10, 70)));
		reactant1.getLine().setWidth(lineWidth);
		Reactant reactant2 = new Reactant(new GenericProtein("s2-1"));
		reactant2.setLine(new PolylineData(new Point2D.Double(60, 90), new Point2D.Double(10, 110)));
		reactant2.getLine().setWidth(lineWidth);
		OrOperator reactantOperator = new OrOperator();
		reactantOperator.addInput(reactant1);
		reactantOperator.addInput(reactant2);
		reactantOperator.setLine(new PolylineData(new Point2D.Double(60, 90), new Point2D.Double(90, 90)));

		Product product1 = new Product(new GenericProtein("s3-1"));
		product1.setLine(new PolylineData(new Point2D.Double(200, 70), new Point2D.Double(130, 90)));
		product1.getLine().setWidth(lineWidth);
		Product product2 = new Product(new GenericProtein("s3-2"));
		product2.setLine(new PolylineData(new Point2D.Double(200, 110), new Point2D.Double(130, 90)));
		product2.getLine().setWidth(lineWidth);
		SplitOperator productOperator = new SplitOperator();
		productOperator.addOutput(product1);
		productOperator.addOutput(product2);
		productOperator.setLine(new PolylineData(new Point2D.Double(130, 90), new Point2D.Double(110, 90)));

		result.addModifier(modifier1);
		result.addModifier(modifier2);
		result.addNode(modifierOperator);
		result.addProduct(product1);
		result.addProduct(product2);
		result.addNode(productOperator);
		result.addReactant(reactant1);
		result.addReactant(reactant2);
		result.addNode(reactantOperator);
		return result;
	}

	@Test
	public void testDrawReactionWithSemanticZooming() throws Exception {
		try {
			Graphics2D graphics = Mockito.mock(Graphics2D.class);
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(1.0);
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(4)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawReactionWithSemanticZoomingAndModifierOff() throws Exception {
		try {
			Graphics2D graphics = Mockito.mock(Graphics2D.class);
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(1.0);
			reaction.getModifiers().get(0).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(3)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawReactionWithSemanticZoomingAndReactantOff() throws Exception {
		try {
			Graphics2D graphics = Mockito.mock(Graphics2D.class);
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(1.0);
			reaction.getReactants().get(0).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(3)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawReactionWithSemanticZoomingAndProductOff() throws Exception {
		try {
			Graphics2D graphics = Mockito.mock(Graphics2D.class);
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createReaction(1.0);
			reaction.getProducts().get(0).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(3)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawComplexReactionWithSemanticZoomingAndModiferOff() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createComplexReaction(1.0);
			reaction.getModifiers().get(0).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(12)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawComplexReactionWithSemanticZoomingAndAllModifersOff() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createComplexReaction(1.0);
			reaction.getModifiers().get(0).getElement().setVisibilityLevel("11");
			reaction.getModifiers().get(1).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(9)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawComplexReactionWithSemanticZoomingAndReactantOff() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createComplexReaction(1.0);
			reaction.getReactants().get(0).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(12)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testDrawComplexReactionWithSemanticZoomingAndProductOff() throws Exception {
		try {
			Graphics2D graphics = createGraphicsMock();
			ReactionConverter rc = new ReactionConverter(colorExtractor);

			Reaction reaction = createComplexReaction(1.0);
			reaction.getProducts().get(0).getElement().setVisibilityLevel("11");
			rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

			verify(graphics, times(12)).draw(any(GeneralPath.class));

		} catch (Exception e) {
			throw e;
		}
	}

}
