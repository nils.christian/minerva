package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertFalse;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.bioEntity.element.species.ComplexConverter;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;

public class NormalImageGeneratorTest {
	static Logger logger = Logger.getLogger(NormalImageGenerator.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	Double					scale;
	private Boolean	artifitialCalled;

	public void setScale(Double sc) {
		scale = sc;
	}

	class TmpComplexConverter extends ComplexConverter {
		public TmpComplexConverter(ColorExtractor colorExtractor) {
			super(colorExtractor);
		}

		@Override
		public void drawText(Complex compAlias, Graphics2D graphics, ConverterParams params) {
			setScale(params.getScale());
			super.drawText(compAlias, graphics, params);
		}

	}

	private Model createCompartmentModel() {
		Model model = new ModelFullIndexed(null);
		model.setWidth(526);
		model.setHeight(346);

		LayerText compAlias = new LayerText();
		compAlias.setX(256.0);
		compAlias.setY(79.0);
		compAlias.setWidth(233.0);
		compAlias.setHeight(188.0);
		compAlias.setNotes("asd as");
		compAlias.setColor(Color.BLACK);
		Layer layer = new Layer();
		layer.addLayerText(compAlias);
		layer.setVisible(true);
		model.addLayer(layer);

		layer.addLayerLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(100, 100)));

		Complex alias = new Complex("1");
		alias.setName("a");
		alias.setX(300);
		alias.setY(90);
		alias.setWidth(100);
		alias.setHeight(50);
		model.addElement(alias);

		return model;
	}

	@Test
	public void testArtifitialInNonHierarchicalView() throws Exception {
		scale = null;
		try {
			// TODO fix it
			// AliasConverter.addAliasConverter(PathwayCompartment.class, new
			// TmpArtifitialConverter());

			setArtifitialCalled(false);
			Model model = createCompartmentModel();
			new CreateHierarchyCommand(model, 2, 2).execute();
			new PngImageGenerator(new Params().scale(2).width(600).height(600).model(model));

			assertFalse(getArtifitialCalled());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @return the artifitialCalled
	 * @see #artifitialCalled
	 */
	public Boolean getArtifitialCalled() {
		return artifitialCalled;
	}

	/**
	 * @param artifitialCalled
	 *          the artifitialCalled to set
	 * @see #artifitialCalled
	 */
	public void setArtifitialCalled(Boolean artifitialCalled) {
		this.artifitialCalled = artifitialCalled;
	}

	@Test
	public void testArtifitialInHierarchicalView() throws Exception {
		try {
			Model model = createCompartmentModel();
			Layer layer = model.getLayers().iterator().next();

			layer.addLayerText(new LayerText(new Rectangle2D.Double(10, 10, 500, 500), "X asd"));
			layer.addLayerText(new LayerText(new Rectangle2D.Double(20, 20, 200, 200), "Y qwe ret"));
			layer.addLayerText(new LayerText(new Rectangle2D.Double(100, 100, 199, 220), "Z dgf fsd aaewq ret"));

			new CreateHierarchyCommand(model, 1, 8).execute();
			model.getCompartments().get(1).setTransparencyLevel("2");
			model.getCompartments().get(0).setTransparencyLevel("2");
			model.getCompartments().get(3).setTransparencyLevel("2");
			new PngImageGenerator(new Params().level(2).scale(4).width(600).height(600).model(model).nested(true));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
