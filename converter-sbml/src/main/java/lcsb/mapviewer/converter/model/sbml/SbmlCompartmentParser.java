package lcsb.mapviewer.converter.model.sbml;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.CompartmentGlyph;
import org.sbml.jsbml.ext.layout.Layout;

import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.species.Element;

public class SbmlCompartmentParser extends SbmlElementParser<org.sbml.jsbml.Compartment> {
  Logger logger = Logger.getLogger(SbmlCompartmentParser.class);

  public SbmlCompartmentParser(Layout layout, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(layout, minervaModel);
  }

  @Override
  protected Compartment parse(org.sbml.jsbml.Compartment compartment, Model sbmlModel)
      throws InvalidInputDataExecption {
    Compartment result = new SquareCompartment(compartment.getId());
    assignBioEntityData(compartment, result);
    return result;
  }

  @Override
  protected ListOf<org.sbml.jsbml.Compartment> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfCompartments();
  }

  @Override
  protected List<Element> mergeLayout(List<? extends Element> elements, Layout sbmlLayout, Model sbmlModel)
      throws InvalidInputDataExecption {
    List<Element> result = super.mergeLayout(elements, sbmlLayout, sbmlModel);

    for (Element element : result) {
      Compartment parent = element.getCompartment();
      for (Compartment compartment : minervaModel.getCompartments()) {
        compartment.setNamePoint(compartment.getX() + ApplySimpleLayoutModelCommand.COMPARTMENT_BORDER,
            compartment.getY() + ApplySimpleLayoutModelCommand.COMPARTMENT_BORDER);
        if (parent == null || parent.getSize() > compartment.getSize()) {
          if (compartment.contains(element)) {
            parent = compartment;
          }
        }
      }
      if (parent != null) {
        parent.addElement(element);
      }
    }
    return result;
  }

  @Override
  protected List<Pair<String, AbstractReferenceGlyph>> getGlyphs(Layout sbmlLayout) {
    List<Pair<String, AbstractReferenceGlyph>> result = new ArrayList<>();
    for (CompartmentGlyph glyph : sbmlLayout.getListOfCompartmentGlyphs()) {
      result.add(new Pair<String, AbstractReferenceGlyph>(glyph.getCompartment(), glyph));
    }
    return result;
  }

}
