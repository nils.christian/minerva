package lcsb.mapviewer.converter.model.sbml.reaction;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.LocalParameter;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SimpleSpeciesReference;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.SpeciesReference;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Curve;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LineSegment;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.ReactionGlyph;
import org.sbml.jsbml.ext.layout.SpeciesReferenceGlyph;
import org.sbml.jsbml.ext.layout.SpeciesReferenceRole;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;

public class SbmlReactionExporter extends SbmlBioEntityExporter<Reaction, org.sbml.jsbml.Reaction> {
  Logger logger = Logger.getLogger(SbmlReactionExporter.class);
  private SbmlBioEntityExporter<lcsb.mapviewer.model.map.species.Species, Species> speciesExporter;
  private SbmlBioEntityExporter<Compartment, org.sbml.jsbml.Compartment> compartmentExporter;

  public SbmlReactionExporter(Layout layout, lcsb.mapviewer.model.map.model.Model minervaModel,
      SbmlBioEntityExporter<lcsb.mapviewer.model.map.species.Species, Species> speciesExporter,
      SbmlBioEntityExporter<lcsb.mapviewer.model.map.compartment.Compartment, org.sbml.jsbml.Compartment> compartmentExporter) {
    super(layout, minervaModel);
    this.speciesExporter = speciesExporter;
    this.compartmentExporter = compartmentExporter;
  }

  Map<ReactionNode, SimpleSpeciesReference> speciesReferenceByReactionNode = new HashMap<>();

  @Override
  public org.sbml.jsbml.Reaction createSbmlElement(Reaction reaction) throws InconsistentModelException {
    String reactionId = getReactionId(reaction);
    org.sbml.jsbml.Reaction result = super.getSbmlElementByElementId(reactionId);
    if (result != null) {
      return result;
    }
    result = getSbmlModel().createReaction(reactionId);
    result.setReversible(reaction.isReversible());
    String sboTerm = SBOTermReactionType.getTermByType(reaction.getClass());
    if (sboTerm != null) {
      result.setSBOTerm(sboTerm);
    }
    for (Product product : reaction.getProducts()) {
      Species sbmlSymbol = speciesExporter.getSbmlElementByElementId(product.getElement().getElementId());
      SpeciesReference speciesReference = result.createProduct(sbmlSymbol);
      speciesReferenceByReactionNode.put(product, speciesReference);
    }
    for (Reactant reactant : reaction.getReactants()) {
      Species sbmlSymbol = speciesExporter.getSbmlElementByElementId(reactant.getElement().getElementId());
      SpeciesReference speciesReference = result.createReactant(sbmlSymbol);
      speciesReferenceByReactionNode.put(reactant, speciesReference);
    }
    for (Modifier modifier : reaction.getModifiers()) {
      Species sbmlSymbol = speciesExporter.getSbmlElementByElementId(modifier.getElement().getElementId());
      SimpleSpeciesReference speciesReference = result.createModifier(sbmlSymbol);
      speciesReferenceByReactionNode.put(modifier, speciesReference);
      String term = SBOTermModifierType.getTermByType(modifier.getClass());
      if (term != null) {
        speciesReference.setSBOTerm(term);
      }
    }
    if (reaction.getKinetics() != null) {
      result.setKineticLaw(createKineticLaw(reaction));
    }
    return result;
  }

  private KineticLaw createKineticLaw(Reaction reaction) throws InconsistentModelException {
    SbmlKinetics kinetics = reaction.getKinetics();
    KineticLaw result = new KineticLaw();
    for (SbmlParameter minervaParameter : kinetics.getParameters()) {
      if (getMinervaModel().getParameterById(minervaParameter.getElementId()) == null) {
        LocalParameter parameter = new LocalParameter();
        parameter.setId(minervaParameter.getElementId());
        parameter.setName(minervaParameter.getName());
        parameter.setValue(minervaParameter.getValue());
        parameter.setUnits(minervaParameter.getUnits().getUnitId());
        result.addLocalParameter(parameter);
      }
    }

    try {
      Node node = super.getXmlDocumentFromString(kinetics.getDefinition());
      for (Node ciNode : super.getAllNotNecessirellyDirectChild("ci", node)) {
        String id = super.getNodeValue(ciNode).trim();
        Element element = getMinervaModel().getElementByElementId(id);
        if (element != null) {
          String sbmlId = null;
          Species species = speciesExporter.getSbmlElementByElementId(id);
          if (species != null) {
            sbmlId = species.getId();
          } else {
            sbmlId = compartmentExporter.getSbmlElementByElementId(id).getId();
          }
          ciNode.setTextContent(sbmlId);
        }
      }
      String definition = super.nodeToString(node);
      definition = definition.replace(" xmlns=\"http://www.sbml.org/sbml/level2/version4\"", "");
      definition = definition.replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
      result.setMath(ASTNode.parseMathML(definition));
      return result;
    } catch (InvalidXmlSchemaException e) {
      throw new InconsistentModelException(e);
    }

  }

  private String getReactionId(Reaction reaction) {
    int separatorIndex = reaction.getElementId().indexOf("__");
    if (separatorIndex > 0) {
      return reaction.getElementId().substring(0, separatorIndex);
    }
    return reaction.getElementId();
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(String sbmlElementId, String glyphId) {
    int separatorIndex = glyphId.indexOf("__");
    if (separatorIndex > 0) {
      glyphId = glyphId.substring(separatorIndex + 2);
    }
    ReactionGlyph reactionGlyph;
    try {
      // handle case when input data cannot doesn't come from SBML parser and contains
      // "__" that mess up identifier uniqueness
      reactionGlyph = getLayout().createReactionGlyph(glyphId, sbmlElementId);
    } catch (IllegalArgumentException e) {
      glyphId += "_" + getNextId();
      reactionGlyph = getLayout().createReactionGlyph(glyphId, sbmlElementId);
    }

    return reactionGlyph;
  }

  @Override
  protected Collection<Reaction> getElementList() {
    return getMinervaModel().getReactions();
  }

  @Override
  protected void assignLayoutToGlyph(Reaction reaction, AbstractReferenceGlyph compartmentGlyph) {
    ReactionGlyph reactionGlyph = (ReactionGlyph) compartmentGlyph;
    boolean firstReactant = true;
    reactionGlyph.setCurve(new Curve());

    Point2D reactantEndPoint = null;

    for (Reactant reactant : reaction.getReactants()) {
      SpeciesReferenceGlyph reactantGlyph = createNodeGlyph(reactionGlyph, reactant);
      if (firstReactant) {
        reactantGlyph.setRole(SpeciesReferenceRole.SUBSTRATE);
        for (NodeOperator operator : reaction.getOperators()) {
          if (operator.isReactantOperator()) {
            addOperatorLineToGlyph(reactionGlyph, operator, true);
            if (reactantEndPoint == null) {
              reactantEndPoint = operator.getLine().getEndPoint();
            }
          }
        }
        if (reactantEndPoint == null) {
          reactantEndPoint = reactant.getLine().getEndPoint();
        }
      } else {
        reactantGlyph.setRole(SpeciesReferenceRole.SIDESUBSTRATE);
      }
      firstReactant = false;
    }
    boolean firstProduct = true;
    Point2D productStartPoint = null;
    for (Product product : reaction.getProducts()) {
      SpeciesReferenceGlyph productGlyph = createNodeGlyph(reactionGlyph, product);
      if (firstProduct) {
        productGlyph.setRole(SpeciesReferenceRole.PRODUCT);
        for (NodeOperator operator : reaction.getOperators()) {
          if (operator.isProductOperator()) {
            if (productStartPoint == null) {
              productStartPoint = operator.getLine().getBeginPoint();
              LineSegment segment = new LineSegment();
              segment.setStart(new Point(reactantEndPoint.getX(), reactantEndPoint.getY()));
              segment.setEnd(new Point(productStartPoint.getX(), productStartPoint.getY()));
              reactionGlyph.getCurve().addCurveSegment(segment);
            }
            addOperatorLineToGlyph(reactionGlyph, operator, false);
          }
        }

        if (productStartPoint == null) {
          productStartPoint = product.getLine().getBeginPoint();
          LineSegment segment = new LineSegment();
          segment.setStart(new Point(reactantEndPoint.getX(), reactantEndPoint.getY()));
          segment.setEnd(new Point(productStartPoint.getX(), productStartPoint.getY()));
          reactionGlyph.getCurve().addCurveSegment(segment);
        }

      } else {
        productGlyph.setRole(SpeciesReferenceRole.SIDEPRODUCT);
      }
      firstProduct = false;
    }
    for (Modifier modifier : reaction.getModifiers()) {
      SpeciesReferenceGlyph modifierGlyph = createNodeGlyph(reactionGlyph, modifier);
      if (modifier instanceof Inhibition) {
        modifierGlyph.setRole(SpeciesReferenceRole.INHIBITOR);
      } else if (modifier instanceof Trigger) {
        modifierGlyph.setRole(SpeciesReferenceRole.ACTIVATOR);
      } else {
        modifierGlyph.setRole(SpeciesReferenceRole.MODIFIER);
      }
    }
  }

  private void addOperatorLineToGlyph(ReactionGlyph reactantGlyph, NodeOperator operator, boolean reverse) {
    Curve curve = reactantGlyph.getCurve();
    List<Line2D> lines = operator.getLine().getLines();
    if (reverse) {
      lines = operator.getLine().reverse().getLines();
    }
    for (Line2D line : lines) {
      if (line.getP1().distance(line.getP2()) > Configuration.EPSILON) {
        LineSegment segment = new LineSegment();
        segment.setStart(new Point(line.getX1(), line.getY1()));
        segment.setEnd(new Point(line.getX2(), line.getY2()));
        curve.addCurveSegment(segment);
      }
    }

  }

  private SpeciesReferenceGlyph createNodeGlyph(ReactionGlyph reactionGlyph, ReactionNode node) {
    SpeciesReferenceGlyph reactantGlyph = reactionGlyph.createSpeciesReferenceGlyph("node_" + getNextId());
    reactantGlyph.setSpeciesGlyph(speciesExporter.getSbmlGlyphByElementId(node.getElement().getElementId()).getId());
    reactantGlyph.setCurve(createCurve(node, node instanceof Reactant));
    reactantGlyph.setSpeciesReference(speciesReferenceByReactionNode.get(node));
    return reactantGlyph;
  }

  private Curve createCurve(ReactionNode node, boolean reverse) {
    Curve curve = new Curve();
    List<Line2D> lines = node.getLine().getLines();
    if (reverse) {
      lines = node.getLine().reverse().getLines();
    }
    for (Line2D line : lines) {
      if (line.getP1().distance(line.getP2()) > Configuration.EPSILON) {
        LineSegment segment = new LineSegment();
        segment.setStart(new Point(line.getX1(), line.getY1()));
        segment.setEnd(new Point(line.getX2(), line.getY2()));
        curve.addCurveSegment(segment);
      }
    }
    return curve;
  }

  @Override
  protected void setSbmlModel(Model sbmlModel) {
    super.setSbmlModel(sbmlModel);
  }

  @Override
  protected String getSbmlIdKey(Reaction element) {
    return element.getClass().getSimpleName() + "\n" + element.getElementId();
  }

}
