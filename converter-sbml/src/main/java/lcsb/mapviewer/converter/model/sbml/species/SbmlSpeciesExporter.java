package lcsb.mapviewer.converter.model.sbml.species;

import java.util.List;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Layout;

import lcsb.mapviewer.converter.model.sbml.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlElementExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlSpeciesExporter extends SbmlElementExporter<Species, org.sbml.jsbml.Species> {
  Logger logger = Logger.getLogger(SbmlSpeciesExporter.class);
  private SbmlCompartmentExporter compartmentExporter;

  public SbmlSpeciesExporter(Layout layout, lcsb.mapviewer.model.map.model.Model minervaModel,
      SbmlCompartmentExporter compartmentExporter) {
    super(layout, minervaModel);
    this.compartmentExporter = compartmentExporter;
  }

  @Override
  public org.sbml.jsbml.Species createSbmlElement(Species element) throws InconsistentModelException {
    org.sbml.jsbml.Species result = getSbmlModel().createSpecies("species_" + (getNextId()));
    result.setSBOTerm(SBOTermSpeciesType.getTermByType(element.getClass()));
    if (element.getCompartment() != null) {
      result.setCompartment(compartmentExporter.getSbmlElement(element.getCompartment()));
    }
    return result;
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(String sbmlElementId, String glyphId) {
    AbstractReferenceGlyph speciesGlyph = getLayout().createSpeciesGlyph(glyphId, sbmlElementId);
    return speciesGlyph;
  }

  @Override
  protected List<Species> getElementList() {
    return getMinervaModel().getSpeciesList();
  }

  @Override
  protected String getSbmlIdKey(Species element) {
    String compartmentName = null;
    if (element.getCompartment() != null) {
      compartmentName = element.getCompartment().getName();
    }
    return element.getClass().getSimpleName() + "\n" + element.getName() + "\n" + compartmentName;
  }

}
