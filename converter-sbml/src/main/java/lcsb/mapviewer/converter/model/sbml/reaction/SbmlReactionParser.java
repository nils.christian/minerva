package lcsb.mapviewer.converter.model.sbml.reaction;

import java.awt.geom.Point2D;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.LocalParameter;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ModifierSpeciesReference;
import org.sbml.jsbml.SpeciesReference;
import org.sbml.jsbml.ext.layout.CurveSegment;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.ReactionGlyph;
import org.sbml.jsbml.ext.layout.SpeciesGlyph;
import org.sbml.jsbml.ext.layout.SpeciesReferenceGlyph;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityParser;
import lcsb.mapviewer.converter.model.sbml.SbmlCompartmentParser;
import lcsb.mapviewer.converter.model.sbml.SbmlParameterParser;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.SplitOperator;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class SbmlReactionParser extends SbmlBioEntityParser {
  Logger logger = Logger.getLogger(SbmlReactionParser.class);

  Layout layout;

  lcsb.mapviewer.model.map.model.Model minervaModel;
  ElementUtils eu = new ElementUtils();

  SbmlSpeciesParser speciesParser;
  SbmlCompartmentParser compartmentParser;

  public SbmlReactionParser(Layout sbmlLayout, lcsb.mapviewer.model.map.model.Model minervaModel,
      SbmlSpeciesParser speciesParser, SbmlCompartmentParser compartmentParser) {
    this.layout = sbmlLayout;
    this.minervaModel = minervaModel;
    this.speciesParser = speciesParser;
    this.compartmentParser = compartmentParser;
  }

  public List<Reaction> parseList(Model sbmlModel) throws InvalidInputDataExecption {
    List<Reaction> result = new ArrayList<>();
    for (org.sbml.jsbml.Reaction sbmlElement : getSbmlElementList(sbmlModel)) {
      result.add(parse(sbmlElement, sbmlModel));
    }
    return result;
  }

  protected ListOf<org.sbml.jsbml.Reaction> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfReactions();
  }

  public void mergeLayout(Collection<Reaction> reactions, Layout sbmlLayout, Model sbmlModel)
      throws InvalidInputDataExecption {
    Set<Reaction> used = new HashSet<>();
    Map<String, Reaction> reactionById = new HashMap<>();
    for (Reaction reaction : reactions) {
      if (reactionById.get(reaction.getIdReaction()) != null) {
        throw new InvalidInputDataExecption("Duplicated reaction id: " + reaction.getIdReaction());
      }
      reactionById.put(reaction.getIdReaction(), reaction);
    }

    for (ReactionGlyph glyph : sbmlLayout.getListOfReactionGlyphs()) {
      Reaction source = reactionById.get(glyph.getReaction());
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + glyph.getReaction());
      }
      used.add(source);
      try {
        Reaction reactionWithLayout = source.copy();
        // getId doesn't have to be unique, therefore we concatenate with reaction
        reactionWithLayout.setIdReaction(glyph.getReaction() + "__" + glyph.getId());
        for (SpeciesReferenceGlyph speciesRefernceGlyph : glyph.getListOfSpeciesReferenceGlyphs()) {
          SpeciesGlyph speciesGlyph = layout.getSpeciesGlyph(speciesRefernceGlyph.getSpeciesGlyph());
          ReactionNode minervaNode = null;
          Class<? extends ReactionNode> nodeClass = null;
          if (speciesRefernceGlyph.getRole() != null) {
            switch (speciesRefernceGlyph.getRole()) {
            case ACTIVATOR:
              nodeClass = Trigger.class;
              break;
            case INHIBITOR:
              nodeClass = Inhibition.class;
              break;
            case PRODUCT:
              nodeClass = Product.class;
              break;
            case SIDEPRODUCT:
              nodeClass = Product.class;
              break;
            case SIDESUBSTRATE:
              nodeClass = Reactant.class;
              break;
            case SUBSTRATE:
              nodeClass = Reactant.class;
              break;
            case UNDEFINED:
            case MODIFIER:
              nodeClass = null;
              break;
            }
          }

          if (reactionWithLayout.isReversible() && (nodeClass == Reactant.class || nodeClass == Product.class)) {
            nodeClass = null;
          }
          for (ReactionNode node : reactionWithLayout.getReactionNodes()) {
            if (node.getElement().getElementId().equals(speciesGlyph.getSpecies())) {
              if (nodeClass == null) {
                minervaNode = node;
                nodeClass = node.getClass();
              } else if (node.getClass().isAssignableFrom(nodeClass)) {
                minervaNode = node;
              }
            }
          }
          if (nodeClass == Modifier.class) {
            nodeClass = Modulation.class;
          }
          if (minervaNode == null) {
            throw new InvalidInputDataExecption(
                "Cannot find reaction node for layouted reaction: " + speciesGlyph.getSpecies() + ", " + glyph.getId());
          }
          Element minervaElement = minervaModel.getElementByElementId(speciesGlyph.getId());
          if (minervaElement == null) {
            throw new InvalidInputDataExecption("Cannot find layouted reaction node for layouted reaction: "
                + speciesGlyph.getId() + ", " + glyph.getId());
          }

          PolylineData line = null;
          for (CurveSegment segment : speciesRefernceGlyph.getCurve().getListOfCurveSegments()) {
            Point2D start = new Point2D.Double(segment.getStart().getX(), segment.getStart().getY());
            Point2D end = new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY());
            if (line == null) {
              line = new PolylineData(start, end);
            } else {
              line.addPoint(end);
            }
          }
          if (minervaNode instanceof Reactant) {
            line = line.reverse();
            line.addPoint(line.getEndPoint());
            if (reactionWithLayout.isReversible()) {
              line.getBeginAtd().setArrowType(ArrowType.FULL);
            }
          }
          if (minervaNode instanceof Product) {
            line.addPoint(0, line.getBeginPoint());
            ArrowTypeData atd = new ArrowTypeData();
            atd.setArrowType(ArrowType.FULL);
            line.setEndAtd(atd);
          } else if (minervaNode instanceof Modifier) {
            for (ModifierType mt : ModifierType.values()) {
              if (mt.getClazz().equals(nodeClass)) {
                line.setEndAtd(mt.getAtd());
                line.setType(mt.getLineType());
              }
            }
          }
          updateKinetics(reactionWithLayout, minervaElement.getElementId(), minervaNode.getElement().getElementId());
          if (minervaElement.getCompartment() != null) {
            String newCompartmentId = minervaElement.getCompartment().getElementId();
            String oldCompartmentId = compartmentParser.getSbmlIdByElementId(newCompartmentId);
            updateKinetics(reactionWithLayout, newCompartmentId, oldCompartmentId);
          }
          minervaNode.setLine(line);
          minervaNode.setElement(minervaElement);
          if (nodeClass != minervaNode.getClass()) {
            reactionWithLayout.removeNode(minervaNode);

            try {
              ReactionNode newNode = nodeClass.getConstructor().newInstance();
              newNode.setElement(minervaElement);
              newNode.setLine(line);
              reactionWithLayout.addNode(newNode);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
              throw new InvalidStateException(e);
            }
          }
        }
        if (reactionWithLayout.getReactants().size() > 1 && reactionWithLayout.getProducts().size() > 0) {
          PolylineData line = new PolylineData();
          Point2D p1 = reactionWithLayout.getReactants().get(0).getLine().getEndPoint();
          Point2D p2 = reactionWithLayout.getProducts().get(0).getLine().getBeginPoint();
          Point2D center = new Point2D.Double((p1.getX() + p2.getX()) / 2, (p1.getY() + p2.getY()) / 2);
          line.addPoint(p1);
          line.addPoint(center);
          NodeOperator operator = new AndOperator();
          operator.addInputs(reactionWithLayout.getReactants());
          operator.setLine(line);
          reactionWithLayout.addNode(operator);
        }
        if (reactionWithLayout.getReactants().size() > 0 && reactionWithLayout.getProducts().size() > 1) {
          PolylineData line = new PolylineData();
          Point2D p1 = reactionWithLayout.getReactants().get(0).getLine().getEndPoint();
          Point2D p2 = reactionWithLayout.getProducts().get(0).getLine().getBeginPoint();
          Point2D center = new Point2D.Double((p1.getX() + p2.getX()) / 2, (p1.getY() + p2.getY()) / 2);
          line.addPoint(p2);
          line.addPoint(center);
          NodeOperator operator = new SplitOperator();
          operator.addOutputs(reactionWithLayout.getProducts());
          operator.setLine(line);
          reactionWithLayout.addNode(operator);
        }
        minervaModel.addReaction(reactionWithLayout);
      } catch (InvalidArgumentException e) {
        throw new InvalidInputDataExecption(e);
      }
    }
    Set<Reaction> elementsToRemove = new HashSet<>();
    for (Reaction reaction : reactions) {
      if (!used.contains(reaction)) {
        for (ReactionNode node : reaction.getReactionNodes()) {
          // we might have different elements here, the reason is that single SBML species
          // can be split into two or more (due to layout)
          node.setElement(speciesParser.getAnyElementBySbmlElementId(node.getElement().getElementId()));
        }
      } else {
        elementsToRemove.add(reaction);
      }
    }
    for (Reaction reaction : elementsToRemove) {
      minervaModel.removeReaction(reaction);
    }
  }

  private void updateKinetics(Reaction reactionWithLayout, String newElementId, String oldElementId) {
    SbmlKinetics kinetics = reactionWithLayout.getKinetics();
    if (kinetics != null) {
      String newDefinition = kinetics.getDefinition().replace(">" + oldElementId + "<", ">" + newElementId + "<");
      kinetics.setDefinition(newDefinition);
      Element elementToRemove = null;
      for (Element element : kinetics.getElements()) {
        if (element.getElementId().equals(oldElementId)) {
          elementToRemove = element;
        }
      }
      if (elementToRemove != null) {
        kinetics.removeElement(elementToRemove);
        kinetics.addElement(minervaModel.getElementByElementId(newElementId));
      }
    }

  }

  protected Reaction parse(org.sbml.jsbml.Reaction sbmlReaction, Model sbmlModel) throws InvalidInputDataExecption {
    Reaction reaction = new Reaction();
    assignBioEntityData(sbmlReaction, reaction);
    reaction.setIdReaction(sbmlReaction.getId());
    reaction.setReversible(sbmlReaction.isReversible());
    if (sbmlReaction.getKineticLaw() != null) {
      reaction.setKinetics(createMinervaKinetics(sbmlReaction.getKineticLaw()));
    }
    for (SpeciesReference reactant : sbmlReaction.getListOfReactants()) {
      Species element = minervaModel.getElementByElementId(reactant.getSpecies());
      reaction.addReactant(new Reactant(element));
    }
    for (SpeciesReference product : sbmlReaction.getListOfProducts()) {
      Species element = minervaModel.getElementByElementId(product.getSpecies());
      reaction.addProduct(new Product(element));
    }

    for (ModifierSpeciesReference modifier : sbmlReaction.getListOfModifiers()) {
      Species element = minervaModel.getElementByElementId(modifier.getSpecies());
      Class<? extends Modifier> nodeClass = SBOTermModifierType.getTypeSBOTerm(modifier.getSBOTermID());
      try {
        Modifier newNode = nodeClass.getConstructor(Element.class).newInstance(element);
        reaction.addModifier(newNode);
      } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException e) {
        throw new InvalidInputDataExecption("Problem with creating modifier", e);
      }
    }

    try {
      Class<? extends Reaction> reactionClass = SBOTermReactionType.getTypeSBOTerm(sbmlReaction.getSBOTermID());
      reaction = reactionClass.getConstructor(Reaction.class).newInstance(reaction);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      throw new InvalidInputDataExecption("Problem with creating reaction", e);
    }

    return reaction;
  }

  private SbmlKinetics createMinervaKinetics(KineticLaw kineticLaw) throws InvalidInputDataExecption {
    SbmlKinetics result = new SbmlKinetics();
    result.setDefinition(kineticLaw.getMath().toMathML());

    SbmlParameterParser parameterParser = new SbmlParameterParser(minervaModel);
    result.addParameters(parameterParser.parseList((Collection<LocalParameter>) kineticLaw.getListOfLocalParameters()));

    try {
      Node node = super.getXmlDocumentFromString(result.getDefinition());
      Set<SbmlArgument> elementsUsedInKinetics = new HashSet<>();
      for (Node ciNode : super.getAllNotNecessirellyDirectChild("ci", node)) {
        String id = super.getNodeValue(ciNode).trim();
        SbmlArgument element = minervaModel.getElementByElementId(id);
        if (element == null) {
          element = result.getParameterById(id);
        }
        if (element == null) {
          element = minervaModel.getParameterById(id);
        }
        if (element == null) {
          element = minervaModel.getFunctionById(id);
        }
        if (element != null) {
          ciNode.setTextContent(element.getElementId());
        } else {
          throw new InvalidXmlSchemaException("Unknown symbol in kinetics: " + id);
        }
        elementsUsedInKinetics.add(element);
      }
      result.addArguments(elementsUsedInKinetics);
      result.setDefinition(super.nodeToString(node));
    } catch (InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }

    return result;
  }

  public void validateReactions(Set<Reaction> reactions) throws InvalidInputDataExecption {
    for (Reaction reaction : reactions) {
      if (reaction.getReactants().size() == 0) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "At least one reactant is required for reaction");
      }
      if (reaction.getProducts().size() == 0) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "At least one product is required for reaction");
      }
    }

  }

}
