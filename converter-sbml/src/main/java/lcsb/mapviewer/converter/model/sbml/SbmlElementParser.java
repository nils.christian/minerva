package lcsb.mapviewer.converter.model.sbml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Layout;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.species.Element;

public abstract class SbmlElementParser<T extends org.sbml.jsbml.Symbol> extends SbmlBioEntityParser {
  Logger logger = Logger.getLogger(SbmlElementParser.class);

  public SbmlElementParser(Layout sbmlLayout, lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.layout = sbmlLayout;
    this.minervaModel = minervaModel;
  }

  public List<Element> parseList(Model sbmlModel) throws InvalidInputDataExecption {
    List<Element> result = new ArrayList<>();
    for (T sbmlElement : getSbmlElementList(sbmlModel)) {
      Element element = parse(sbmlElement, sbmlModel);
      result.add(element);
      elementBySbmlId.put(element.getElementId(), element);
    }
    return result;
  }

  protected abstract ListOf<T> getSbmlElementList(Model sbmlModel);

  Map<String, Element> elementBySbmlId = new HashMap<>();

  Map<String, String> sbmlIdByElementId = new HashMap<>();

  public Element getAnyElementBySbmlElementId(String id) {
    return elementBySbmlId.get(id);
  }

  public String getSbmlIdByElementId(String compartmentId) {
    return sbmlIdByElementId.get(compartmentId);
  }

  protected List<Element> mergeLayout(List<? extends Element> elements, Layout sbmlLayout, Model sbmlModel)
      throws InvalidInputDataExecption {
    Set<String> used = new HashSet<>();
    List<Element> result = new ArrayList<>();
    for (Element species : elements) {
      elementBySbmlId.put(species.getElementId(), species);
    }

    for (Pair<String, AbstractReferenceGlyph> idGlyphPair : getGlyphs(sbmlLayout)) {
      String id = idGlyphPair.getLeft();
      Element source = elementBySbmlId.get(id);
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + idGlyphPair.getLeft());
      }
      used.add(source.getElementId());
      AbstractReferenceGlyph glyph = idGlyphPair.getRight();
      Element elementWithLayout = source.copy();
      elementWithLayout.setElementId(glyph.getId());
      elementWithLayout.setX(glyph.getBoundingBox().getPosition().getX());
      elementWithLayout.setY(glyph.getBoundingBox().getPosition().getY());
      elementWithLayout.setWidth(glyph.getBoundingBox().getDimensions().getWidth());
      elementWithLayout.setHeight(glyph.getBoundingBox().getDimensions().getHeight());
      minervaModel.addElement(elementWithLayout);
      result.add(elementWithLayout);
      elementBySbmlId.put(id, elementWithLayout);
      elementBySbmlId.put(elementWithLayout.getElementId(), elementWithLayout);
      sbmlIdByElementId.put(elementWithLayout.getElementId(), id);
    }
    for (Element element : elements) {
      if (!used.contains(element.getElementId())) {
        logger.warn("Layout doesn't contain information about Element: " + element.getElementId());
        result.add(element);
      } else {
        minervaModel.removeElement(element);
      }
    }
    return result;
  }

  protected abstract List<Pair<String, AbstractReferenceGlyph>> getGlyphs(Layout sbmlLayout);

  protected abstract Element parse(T species, Model sbmlModel) throws InvalidInputDataExecption;

}
