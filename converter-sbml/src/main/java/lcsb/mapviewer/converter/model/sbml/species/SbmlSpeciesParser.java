package lcsb.mapviewer.converter.model.sbml.species;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.CompartmentGlyph;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.SpeciesGlyph;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlElementParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlSpeciesParser extends SbmlElementParser<org.sbml.jsbml.Species> {
  Logger logger = Logger.getLogger(SbmlSpeciesParser.class);

  public SbmlSpeciesParser(Layout layout, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(layout, minervaModel);
  }

  @Override
  protected Species parse(org.sbml.jsbml.Species species, Model sbmlModel) throws InvalidInputDataExecption {
    String sboTerm = species.getSBOTermID();
    Class<? extends Species> clazz = SBOTermSpeciesType.getTypeSBOTerm(sboTerm);
    try {
      Species result = clazz.getConstructor(String.class).newInstance(species.getId());
      assignBioEntityData(species, result);
      if (layout == null) {
        assignCompartment(result, species.getCompartment());
      }
      return result;
    } catch (SecurityException | NoSuchMethodException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public List<Element> mergeLayout(List<? extends Element> elements, Layout sbmlLayout, Model sbmlModel)
      throws InvalidInputDataExecption {
    List<Element> result = super.mergeLayout(elements, sbmlLayout, sbmlModel);

    for (Element element : result) {
      String compartmentId = null;
      if (sbmlLayout.getSpeciesGlyph(element.getElementId()) != null) {
        compartmentId = ((org.sbml.jsbml.Species) sbmlLayout.getSpeciesGlyph(element.getElementId())
            .getSpeciesInstance()).getCompartment();
      } else {
        compartmentId = sbmlModel.getSpecies(element.getElementId()).getCompartment();
      }
      assignCompartment(element, compartmentId);
    }
    return result;
  }

  private void assignCompartment(Element element, String compartmentId) {
    Compartment compartment = minervaModel.getElementByElementId(compartmentId);
    if (compartment == null && layout != null) {
      List<Compartment> compartments = new ArrayList<>();
      for (CompartmentGlyph glyph : layout.getListOfCompartmentGlyphs()) {
        if (glyph.getCompartment().equals(compartmentId)) {
          compartments.add(minervaModel.getElementByElementId(glyph.getId()));
        }
      }
      for (Compartment compartment2 : compartments) {
        if (compartment2.contains(element)) {
          compartment = compartment2;
        }
      }

    }
    if (compartment != null) {
      compartment.addElement(element);
    }

  }

  @Override
  protected ListOf<org.sbml.jsbml.Species> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfSpecies();
  }

  @Override
  protected List<Pair<String, AbstractReferenceGlyph>> getGlyphs(Layout sbmlLayout) {
    List<Pair<String, AbstractReferenceGlyph>> result = new ArrayList<>();
    for (SpeciesGlyph glyph : sbmlLayout.getListOfSpeciesGlyphs()) {
      result.add(new Pair<String, AbstractReferenceGlyph>(glyph.getSpecies(), glyph));
    }
    return result;
  }

}
