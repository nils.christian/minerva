package lcsb.mapviewer.converter.model.sbml;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Layout;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;

public abstract class SbmlBioEntityExporter<T extends BioEntity, S extends org.sbml.jsbml.AbstractNamedSBase>
    extends XmlParser {
  Logger logger = Logger.getLogger(SbmlBioEntityExporter.class);

  private Layout layout;

  private lcsb.mapviewer.model.map.model.Model minervaModel;
  private Model sbmlModel;

  private Map<String, S> sbmlElementByElementId = new HashMap<>();
  Map<String, AbstractReferenceGlyph> sbmlGlyphByElementId = new HashMap<>();

  private Map<String, S> sbmlElementByElementNameAndCompartmentName = new HashMap<>();

  private int idCounter = 0;

  public SbmlBioEntityExporter(Layout sbmlLayout, lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.layout = sbmlLayout;
    this.minervaModel = minervaModel;
  }

  public void exportElements(Model model) throws InconsistentModelException {
    sbmlModel = model;
    Collection<T> speciesList = getElementList();
    for (T species : speciesList) {
      S sbmlElement = getSbmlElement(species);

      if (sbmlElementByElementId.get(species.getElementId()) != null) {
        throw new InconsistentModelException("More than one species with id: " + species.getElementId());
      }
      sbmlElementByElementId.put(species.getElementId(), sbmlElement);
    }
    for (T species : speciesList) {
      AbstractReferenceGlyph elementGlyph = createGlyph(species);
      sbmlGlyphByElementId.put(species.getElementId(), elementGlyph);
    }
  }

  protected abstract Collection<T> getElementList();

  public abstract S createSbmlElement(T element) throws InconsistentModelException;

  public S getSbmlElement(T element) throws InconsistentModelException {
    String mapKey = getSbmlIdKey(element);
    if (sbmlElementByElementNameAndCompartmentName.get(mapKey) == null) {
      S sbmlElement = createSbmlElement(element);
      XmlAnnotationParser parser = new XmlAnnotationParser();
      String rdf = parser.dataSetToXmlString(element.getMiriamData());
      try {
        sbmlElement.setAnnotation(rdf);
      } catch (XMLStreamException e1) {
        throw new InconsistentModelException(e1);
      }
      sbmlElement.setName(element.getName());
      try {
        sbmlElement.setNotes(StringEscapeUtils.escapeXml(element.getNotes()));
      } catch (XMLStreamException e) {
        throw new InvalidStateException(e);
      }
      sbmlElementByElementNameAndCompartmentName.put(mapKey, sbmlElement);
    }
    return sbmlElementByElementNameAndCompartmentName.get(mapKey);
  }

  protected abstract String getSbmlIdKey(T element);

  protected abstract void assignLayoutToGlyph(T element, AbstractReferenceGlyph compartmentGlyph);

  protected AbstractReferenceGlyph createGlyph(T element) {
    String sbmlElementId = sbmlElementByElementId.get(element.getElementId()).getId();
    String glyphId = element.getElementId();
    AbstractReferenceGlyph elementGlyph = createElementGlyph(sbmlElementId, glyphId);
    assignLayoutToGlyph(element, elementGlyph);
    return elementGlyph;
  }

  protected abstract AbstractReferenceGlyph createElementGlyph(String sbmlCompartmentId, String glyphId);

  protected String getNextId() {
    return (idCounter++) + "";
  }

  protected Model getSbmlModel() {
    return sbmlModel;
  }

  protected void setSbmlModel(Model sbmlModel) {
    this.sbmlModel = sbmlModel;
  }

  protected Layout getLayout() {
    return layout;
  }

  protected void setLayout(Layout layout) {
    this.layout = layout;
  }

  protected lcsb.mapviewer.model.map.model.Model getMinervaModel() {
    return minervaModel;
  }

  protected void setMinervaModel(lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public S getSbmlElementByElementId(String id) {
    return sbmlElementByElementId.get(id);
  }

  public AbstractReferenceGlyph getSbmlGlyphByElementId(String elementId) {
    return sbmlGlyphByElementId.get(elementId);
  }

}
