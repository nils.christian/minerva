package lcsb.mapviewer.converter.model.sbml;

import java.util.List;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Layout;

import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;

public class SbmlCompartmentExporter extends SbmlElementExporter<Compartment, org.sbml.jsbml.Compartment> {
  Logger logger = Logger.getLogger(SbmlCompartmentExporter.class);

  public SbmlCompartmentExporter(Layout layout, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(layout, minervaModel);
  }

  @Override
  public org.sbml.jsbml.Compartment createSbmlElement(Compartment element) throws InconsistentModelException {
    return getSbmlModel().createCompartment("comp_" + (getNextId()));
  }

  @Override
  protected List<Compartment> getElementList() {
    return getMinervaModel().getCompartments();
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(String sbmlCompartmentId, String glyphId) {
    return getLayout().createCompartmentGlyph(glyphId, sbmlCompartmentId);
  }

  @Override
  protected String getSbmlIdKey(Compartment element) {
    return element.getName();
  }

}
