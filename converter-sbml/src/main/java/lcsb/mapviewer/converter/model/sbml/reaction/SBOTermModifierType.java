package lcsb.mapviewer.converter.model.sbml.reaction;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Modifier;

public enum SBOTermModifierType {
  CATALYSIS(Catalysis.class, new String[] { "SBO:0000013" }), //
  INHIBITION(Inhibition.class, new String[] { "SBO:0000537" }), //
  MODULATION(Modulation.class, new String[] { "SBO:0000594" }), //
  PHYSICAL_STIMULATION(PhysicalStimulation.class, new String[] { "SBO:0000459" }), //
  TRIGGER(Trigger.class, new String[] { "SBO:0000461" }), //
  UNKNOWN_CATALYSIS(UnknownCatalysis.class, new String[] { "SBO:0000462" }), //
  UNKNOWN_INHIBITION(UnknownInhibition.class, new String[] { "SBO:0000536" }), //
  ;

  private static Logger logger = Logger.getLogger(SBOTermModifierType.class);
  private List<String> sboTerms = new ArrayList<>();
  Class<? extends Modifier> clazz;

  private SBOTermModifierType(Class<? extends Modifier> clazz, String[] inputSboTerms) {
    this.clazz = clazz;
    for (String string : inputSboTerms) {
      sboTerms.add(string);
    }
  }

  public static Class<? extends Modifier> getTypeSBOTerm(String sboTerm) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return Modifier.class;
    }
    Class<? extends Modifier> result = null;
    for (SBOTermModifierType term : values()) {
      for (String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
        }
      }
    }
    if (result == null) {
      logger.warn("Don't know how to handle SBOTerm " + sboTerm + " for modifier");
      result = Modifier.class;
    }
    return result;
  }

  public static String getTermByType(Class<? extends Modifier> clazz) {
    for (SBOTermModifierType term : values()) {
      if (clazz.equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  private String getSBO() {
    if (sboTerms.size() != 0) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }
}
