package lcsb.mapviewer.converter.model.sbml;

import java.util.HashSet;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.sbml.jsbml.AbstractNamedSBase;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.ext.layout.Layout;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.sbml.species.ElementColorEnum;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;

public class SbmlBioEntityParser extends XmlParser {
  Logger logger = Logger.getLogger(SbmlBioEntityParser.class);

  protected Layout layout;
  protected lcsb.mapviewer.model.map.model.Model minervaModel;

  private int idCounter = 0;

  public SbmlBioEntityParser() {
    super();
  }

  protected Set<MiriamData> parseAnnotation(Annotation annotation) throws InvalidInputDataExecption {
    XmlAnnotationParser parser = new XmlAnnotationParser();
    Set<MiriamData> result = new HashSet<>();
    if (annotation.getCVTermCount() > 0) {
      try {
        Node node = getXmlDocumentFromString(annotation.getFullAnnotationString());
        Node annotationNode = getNode("annotation", node);
        Node rdfNode = getNode("rdf:RDF", annotationNode);
        result.addAll(parser.parseRdfNode(rdfNode));
      } catch (InvalidXmlSchemaException e) {
        throw new InvalidInputDataExecption(e);
      }
    }
    return result;
  }

  protected void assignBioEntityData(AbstractNamedSBase sbmlElement, BioEntity result)
      throws InvalidInputDataExecption {
    result.addMiriamData(parseAnnotation(sbmlElement.getAnnotation()));
    result.setName(sbmlElement.getName());
    if (result.getName() == null || result.getName().isEmpty()) {
      result.setName(result.getElementId());
    }
    String notes = extractNotes(sbmlElement);
    result.setNotes(notes);

    if (result instanceof Element) {
      Element element = (Element) result;
      element.setColor(ElementColorEnum.getColorByClass(element.getClass()));
    }
  }

  private String extractNotes(AbstractNamedSBase sbmlElement) throws InvalidInputDataExecption {
    String notes = "";
    try {
      notes = sbmlElement.getNotesString();
    } catch (XMLStreamException e) {
      throw new InvalidInputDataExecption(sbmlElement.getId() + " Invalid notes", e);
    }
    if (sbmlElement.getNotes() != null) {
      if (sbmlElement.getNotes().getChildCount() > 1) {
        if (sbmlElement.getNotes().getChild(1).getChildCount() > 1) {
          if (sbmlElement.getNotes().getChild(1).getChild(1).getChildCount() > 0) {
            notes = sbmlElement.getNotes().getChild(1).getChild(1).getChild(0).getCharacters();
          } else {
            notes = sbmlElement.getNotes().getChild(1).getChild(1).getCharacters();
          }
        }
      }
    }
    return notes;
  }

  protected String getNextId() {
    return (idCounter++) + "";
  }

}