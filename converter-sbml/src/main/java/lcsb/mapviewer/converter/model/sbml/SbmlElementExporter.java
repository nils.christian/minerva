package lcsb.mapviewer.converter.model.sbml;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.Point;

import lcsb.mapviewer.model.map.species.Element;

public abstract class SbmlElementExporter<T extends Element, S extends org.sbml.jsbml.Symbol>
    extends SbmlBioEntityExporter<T, S> {
  Logger logger = Logger.getLogger(SbmlElementExporter.class);

  public SbmlElementExporter(Layout sbmlLayout, lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlLayout, minervaModel);
  }

  protected void assignLayoutToGlyph(T element, AbstractReferenceGlyph speciesGlyph) {
    BoundingBox boundingBox = new BoundingBox();

    boundingBox.setPosition(new Point(element.getX(), element.getY()));
    Dimensions dimensions = new Dimensions();
    dimensions.setWidth(element.getWidth());
    dimensions.setHeight(element.getHeight());
    boundingBox.setDimensions(dimensions);

    speciesGlyph.setBoundingBox(boundingBox);
  }

}
