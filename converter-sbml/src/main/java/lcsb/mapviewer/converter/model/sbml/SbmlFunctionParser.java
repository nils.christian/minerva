package lcsb.mapviewer.converter.model.sbml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;

public class SbmlFunctionParser extends SbmlBioEntityParser {
  Logger logger = Logger.getLogger(SbmlFunctionParser.class);

  public SbmlFunctionParser(lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  protected SbmlFunction parse(org.sbml.jsbml.FunctionDefinition unitDefinition, Model sbmlModel) throws InvalidInputDataExecption {
    try {
      SbmlFunction result = new SbmlFunction(unitDefinition.getId());
      result.setName(unitDefinition.getName());
      Node node = super.getXmlDocumentFromString(unitDefinition.getMath().toMathML());
      Node mathDefinition = getNode("math", node);
      String definition = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">"
          + nodeToString(mathDefinition).replace(" xmlns=\"http://www.w3.org/1998/Math/MathML\"", "") + "</math>";
      result.setDefinition(definition);
      result.setArguments(parseArgumentsFromMath(mathDefinition, result.getFunctionId()));
      return result;
    } catch (InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }

  }

  private List<String> parseArgumentsFromMath(Node mathDefinition, String functionId) throws InvalidXmlSchemaException {
    List<String> result = new ArrayList<>();
    Node lambdaDefinition = getNode("lambda", mathDefinition);
    if (lambdaDefinition == null) {
      throw new InvalidXmlSchemaException("Function " + functionId + " doesn't contain valid MathML lambda node");
    }
    List<Node> variables = super.getNodes("bvar", lambdaDefinition.getChildNodes());
    for (Node node : variables) {
      result.add(getNodeValue(getNode("ci", node)).trim());
    }
    return result;
  }

  protected ListOf<org.sbml.jsbml.FunctionDefinition> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfFunctionDefinitions();
  }

  public Collection<SbmlFunction> parseList(Model sbmlModel) throws InvalidInputDataExecption {
    List<SbmlFunction> result = new ArrayList<>();
    for (org.sbml.jsbml.FunctionDefinition unitDefinition : getSbmlElementList(sbmlModel)) {
      result.add(parse(unitDefinition, sbmlModel));
    }
    return result;
  }
}
