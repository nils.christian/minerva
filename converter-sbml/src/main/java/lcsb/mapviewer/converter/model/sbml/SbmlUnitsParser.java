package lcsb.mapviewer.converter.model.sbml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;

public class SbmlUnitsParser extends SbmlBioEntityParser {
  Logger logger = Logger.getLogger(SbmlUnitsParser.class);

  public SbmlUnitsParser(lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  protected SbmlUnit parse(org.sbml.jsbml.UnitDefinition unitDefinition, Model sbmlModel) {
    SbmlUnit result = new SbmlUnit(unitDefinition.getId());
    result.setName(unitDefinition.getName());
    return result;
  }

  protected ListOf<org.sbml.jsbml.UnitDefinition> getSbmlElementList(Model sbmlModel) {
    return sbmlModel.getListOfUnitDefinitions();
  }

  public Collection<SbmlUnit> parseList(Model sbmlModel) {
    List<SbmlUnit> result = new ArrayList<>();
    for (org.sbml.jsbml.UnitDefinition unitDefinition : getSbmlElementList(sbmlModel)) {
      result.add(parse(unitDefinition, sbmlModel));
    }
    return result;
  }
}
