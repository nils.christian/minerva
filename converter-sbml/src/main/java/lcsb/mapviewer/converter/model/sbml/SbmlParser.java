package lcsb.mapviewer.converter.model.sbml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLException;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.ext.SBasePlugin;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.util.NotImplementedException;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.reaction.SbmlReactionParser;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

public class SbmlParser implements IConverter {

  /**
   * Default class logger.
   */
  Logger logger = Logger.getLogger(SbmlParser.class);

  @Override
  public Model createModel(ConverterParams params) throws InvalidInputDataExecption {
    try {
      Model model = new ModelFullIndexed(null);

      if (params.getFilename() != null) {
        model.setName(FilenameUtils.getBaseName(params.getFilename()));
      }
      SBMLDocument sbmlDocument = SBMLReader.read(params.getInputStream());
      org.sbml.jsbml.Model sbmlModel = sbmlDocument.getModel();
      model.setIdModel(sbmlModel.getId());
      model.setName(sbmlModel.getName());

      Layout layout = getSbmlLayout(sbmlModel);
      boolean layoutExists = layout != null;

      SbmlCompartmentParser compartmentParser = new SbmlCompartmentParser(layout, model);
      SbmlSpeciesParser speciesParser = new SbmlSpeciesParser(layout, model);
      SbmlReactionParser reactionParser = new SbmlReactionParser(layout, model, speciesParser, compartmentParser);
      SbmlUnitsParser unitParser = new SbmlUnitsParser(model);
      SbmlParameterParser parameterParser = new SbmlParameterParser(model);
      SbmlFunctionParser functionParser = new SbmlFunctionParser(model);

      Set<MiriamData> annotations = compartmentParser.parseAnnotation(sbmlModel.getAnnotation());
      if (annotations.size() > 0) {
        throw new NotImplementedException("Annotations not implemented for model");
      }

      model.addUnits(unitParser.parseList(sbmlModel));
      model.addParameters(parameterParser.parseList(sbmlModel));
      model.addFunctions(functionParser.parseList(sbmlModel));

      model.addElements(compartmentParser.parseList(sbmlModel));
      model.addElements(speciesParser.parseList(sbmlModel));
      model.addReactions(reactionParser.parseList(sbmlModel));

      if (layoutExists) {
        if (layout.getDimensions() != null) {
          model.setWidth(layout.getDimensions().getWidth());
          model.setHeight(layout.getDimensions().getHeight());

        }
        compartmentParser.mergeLayout(model.getCompartments(), layout, sbmlModel);
        speciesParser.mergeLayout(model.getSpeciesList(), layout, sbmlModel);
        reactionParser.mergeLayout(model.getReactions(), layout, sbmlModel);
      }
      reactionParser.validateReactions(model.getReactions());

      if (sbmlModel.getConstraintCount() > 0) {
        throw new NotImplementedException("Constraints not implemented for model");
      }
      if (sbmlModel.getConversionFactorInstance() != null) {
        throw new NotImplementedException("ConversionFactor not implemented for model");
      }
      if (sbmlModel.getCVTermCount() > 0) {
        throw new NotImplementedException("CVTerms not implemented for model");
      }
      if (sbmlModel.getEventCount() > 0) {
        throw new NotImplementedException("Events not implemented for model");
      }
      if (sbmlModel.getEventAssignmentCount() > 0) {
        throw new NotImplementedException("EventAssignemnts not implemented for model");
      }
      if (sbmlModel.getInitialAssignmentCount() > 0) {
        throw new NotImplementedException("InitialAssignment not implemented for model");
      }
      if (sbmlModel.getRuleCount() > 0) {
        throw new NotImplementedException("Rule not implemented for model");
      }
      createLayout(model, layout, params.isSizeAutoAdjust());
      return model;
    } catch (XMLStreamException e) {
      throw new InvalidInputDataExecption(e);
    }
  }

  private void createLayout(Model model, Layout layout, boolean resize) throws InvalidInputDataExecption {
    if (model.getWidth() == null) {
      double maxY = 0;
      double maxX = 0;
      for (Element element : model.getElements()) {
        maxY = Math.max(maxY, element.getY() + element.getHeight() + 10);
        maxX = Math.max(maxX, element.getX() + element.getWidth() + 10);
      }
      if (resize) {
        model.setWidth(maxX);
        model.setHeight(maxY);
      }
    }
    Collection<BioEntity> bioEntitesRequiringLayout = new HashSet<>();

    for (Element element : model.getElements()) {
      if (element.getWidth() == 0 || element.getHeight() == 0) {
        bioEntitesRequiringLayout.add(element);
      }
    }
    for (Reaction reaction : model.getReactions()) {
      if (!hasLayout(reaction)) {
        updateModifierTypes(reaction);
        bioEntitesRequiringLayout.add(reaction);
      }
    }
    try {
      if (bioEntitesRequiringLayout.size() > 0) {
        new ApplySimpleLayoutModelCommand(model, bioEntitesRequiringLayout, true).execute();
      }
    } catch (CommandExecutionException e) {
      throw new InvalidInputDataExecption("Problem with generating layout", e);
    }
  }

  private void updateModifierTypes(Reaction reaction) {
    Set<Modifier> modifiersToBeRemoved = new HashSet<>();
    Set<Modifier> modifiersToBeAdded = new HashSet<>();
    for (Modifier modifier : reaction.getModifiers()) {
      if (modifier.getClass() == Modifier.class) {
        modifiersToBeRemoved.add(modifier);
        modifiersToBeAdded.add(new Modulation(modifier.getElement()));
      }
    }
    for (Modifier modifier : modifiersToBeRemoved) {
      reaction.removeModifier(modifier);
    }
    for (Modifier modifier : modifiersToBeAdded) {
      reaction.addModifier(modifier);
    }
  }

  private boolean hasLayout(Reaction reaction) {
    for (AbstractNode node : reaction.getNodes()) {
      if (node.getLine() == null) {
        return false;
      } else if (node.getLine().length() == 0) {
        return false;
      }
    }
    return true;
  }

  private Layout getSbmlLayout(org.sbml.jsbml.Model sbmlModel) {
    Layout layout = null;

    if (sbmlModel.getExtensionCount() > 0) {
      for (SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (plugin.getClass().equals(org.sbml.jsbml.ext.layout.LayoutModelPlugin.class)) {
          LayoutModelPlugin layoutPlugin = (LayoutModelPlugin) plugin;
          if (layoutPlugin.getLayoutCount() == 0) {
            logger.warn("Layout plugin available but no layouts defined");
          } else if (layoutPlugin.getLayoutCount() > 1) {
            logger.warn(layoutPlugin.getLayoutCount() + " layouts defined. Using first one.");
            layout = layoutPlugin.getLayout(0);
          } else {
            layout = layoutPlugin.getLayout(0);
          }
        } else {
          logger.warn("Unknown sbml plugin: " + plugin);
        }
      }
    }
    return layout;
  }

  @Override
  public InputStream exportModelToInputStream(Model model) throws ConverterException, InconsistentModelException {
    String exportedString = toXml(model);
    InputStream inputStream = new ByteArrayInputStream(exportedString.getBytes());
    return inputStream;
  }

  private String toXml(Model model) throws ConverterException {
    try {
      return new SbmlExporter().toXml(model);
    } catch (SBMLException | XMLStreamException | InconsistentModelException e) {
      throw new ConverterException(e);
    }
  }

  @Override
  public File exportModelToFile(Model model, String filePath)
      throws ConverterException, InconsistentModelException, IOException {
    File file = new File(filePath);
    String exportedString = toXml(model);
    FileWriter fileWriter = new FileWriter(file);
    fileWriter.write(exportedString);
    fileWriter.flush();
    fileWriter.close();
    return file;
  }

  @Override
  public String getCommonName() {
    return "SBML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.SBML;
  }

  @Override
  public String getFileExtension() {
    return "xml";
  }

}
