package lcsb.mapviewer.converter.model.sbml;

import org.apache.log4j.Logger;
import org.sbml.jsbml.UnitDefinition;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlUnitExporter {
  Logger logger = Logger.getLogger(SbmlUnitExporter.class);
  private Model minervaModel;

  public SbmlUnitExporter(lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public void exportUnits(org.sbml.jsbml.Model result) {
    for (SbmlUnit unit : minervaModel.getUnits()) {
      result.addUnitDefinition(createUnitDefinition(unit));
    }
  }

  private UnitDefinition createUnitDefinition(SbmlUnit unit) {
    UnitDefinition result = new UnitDefinition();
    result.setId(unit.getUnitId());
    result.setName(unit.getName());
    return result;
  }

}
