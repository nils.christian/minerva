package lcsb.mapviewer.converter.model.sbml;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLException;
import org.sbml.jsbml.SBMLWriter;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;

import lcsb.mapviewer.converter.model.sbml.reaction.SbmlReactionExporter;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlExporter {
  Logger logger = Logger.getLogger(SbmlExporter.class);
  public String toXml(lcsb.mapviewer.model.map.model.Model model)
      throws SBMLException, XMLStreamException, InconsistentModelException {
    SBMLDocument doc = new SBMLDocument(3, 1);
    Model result = doc.createModel(model.getIdModel());
    result.setName(model.getName());
    Layout layout = createSbmlLayout(model, result);

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(layout, model);
    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> speciesExporter = new SbmlSpeciesExporter(layout, model,
        compartmentExporter);
    SbmlReactionExporter reactionExporter = new SbmlReactionExporter(layout, model, speciesExporter,
        compartmentExporter);
    SbmlUnitExporter unitExporter = new SbmlUnitExporter(model);
    SbmlParameterExporter parameterExporter = new SbmlParameterExporter(model);
    SbmlFunctionExporter functionExporter = new SbmlFunctionExporter(model);

    compartmentExporter.exportElements(result);
    speciesExporter.exportElements(result);
    reactionExporter.exportElements(result);
    unitExporter.exportUnits(result);
    parameterExporter.exportParameter(result);
    functionExporter.exportFunction(result);

    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    SBMLWriter.write(doc, stream, "minerva", "1.0");
    return stream.toString();
  }

  private Layout createSbmlLayout(lcsb.mapviewer.model.map.model.Model model, Model result) {
    LayoutModelPlugin layoutPlugin = new LayoutModelPlugin(result);
    Layout layout = new Layout();
    Dimensions dimensions = new Dimensions();
    if (model.getHeight() != null) {
      dimensions.setHeight(model.getHeight());
    } else {
      dimensions.setHeight(640);
    }
    if (model.getWidth() != null) {
      dimensions.setWidth(model.getWidth());
    } else {
      dimensions.setWidth(480);
    }
    layout.setDimensions(dimensions);
    layoutPlugin.add(layout);
    result.addExtension("layout", layoutPlugin);
    return layout;
  }
}
