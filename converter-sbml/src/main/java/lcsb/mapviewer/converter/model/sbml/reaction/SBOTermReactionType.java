package lcsb.mapviewer.converter.model.sbml.reaction;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;

public enum SBOTermReactionType {
  DISSOCIATION(DissociationReaction.class, new String[] { "SBO:0000180" }), //
  HETERODIMER_ASSOCIATION(HeterodimerAssociationReaction.class, new String[] { "SBO:0000177" }), //
  KNOWN_TRANSITION_OMITTED(KnownTransitionOmittedReaction.class, new String[] { "SBO:0000205" }), //
  NEGATIVE_INFLUENCE(NegativeInfluenceReaction.class, new String[] { "SBO:0000407" }), //
  POSITIVE_INFLUENCE(PositiveInfluenceReaction.class, new String[] { "SBO:0000171" }), //
  REDUCED_MODULATION(ReducedModulationReaction.class, new String[] { "SBO:0000632" }), //
  REDUCED_PHYSICAL_STIMULATION(ReducedPhysicalStimulationReaction.class, new String[] { "SBO:0000411" }), //
  REDUCED_TRIGGER(ReducedTriggerReaction.class, new String[] { "SBO:0000461" }), //
  STATE_TRANSITION(StateTransitionReaction.class, new String[] { "SBO:0000176" }), //
  TRANSCRIPTION(TranscriptionReaction.class, new String[] { "SBO:0000183" }), //
  TRANSLATION(TranslationReaction.class, new String[] { "SBO:0000184" }), //
  TRANSPORT(TransportReaction.class, new String[] { "SBO:0000185" }), //
  TRUNCATION(TruncationReaction.class, new String[] { "SBO:0000178" }), //
  UNKNOWN_NEGATIVE_INFLUENCE(UnknownNegativeInfluenceReaction.class, new String[] { "SBO:0000169" }), //
  UNKNOWN_POSITIVE_INFLUENCE(UnknownPositiveInfluenceReaction.class, new String[] { "SBO:0000172" }), //
  UNKNOWN_REDUCED_MODULATION(UnknownReducedModulationReaction.class, new String[] { "SBO:0000631" }), //
  UNKNOWN_REDUCED_PHYSICAL_STIMULATION(UnknownReducedPhysicalStimulationReaction.class, new String[] { "SBO:0000170" }), //
  UNKNOWN_REDUCED_TRIGGER(UnknownReducedTriggerReaction.class, new String[] { "SBO:0000533" }), //
  UNKNOWN_TRANSITION(UnknownTransitionReaction.class, new String[] { "SBO:0000396" }), //
  ;

  private static Logger logger = Logger.getLogger(SBOTermReactionType.class);
  private List<String> sboTerms = new ArrayList<>();
  Class<? extends Reaction> clazz;

  private SBOTermReactionType(Class<? extends Reaction> clazz, String[] inputSboTerms) {
    this.clazz = clazz;
    for (String string : inputSboTerms) {
      sboTerms.add(string);
    }
  }

  public static Class<? extends Reaction> getTypeSBOTerm(String sboTerm) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return StateTransitionReaction.class;
    }
    Class<? extends Reaction> result = null;
    for (SBOTermReactionType term : values()) {
      for (String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
        }
      }
    }
    if (result == null) {
      logger.warn("Don't know how to handle SBOTerm " + sboTerm + " for reaction");
      result = StateTransitionReaction.class;
    }
    return result;
  }

  public static String getTermByType(Class<? extends Reaction> clazz) {
    for (SBOTermReactionType term : values()) {
      if (clazz.equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  private String getSBO() {
    if (sboTerms.size() != 0) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }
}
