package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Desktop;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.NormalImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlExporterTest {
  Logger logger = Logger.getLogger(SbmlExporterTest.class);
  SbmlParser parser = new SbmlParser();
  SbmlExporter exporter = new SbmlExporter();

  @Test
  public void testExportCompartment() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml");
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    Compartment compartment = model.getElementByElementId("CompartmentGlyph_1");
    assertNotNull(compartment);
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
  }

  private Model getModelAfterSerializing(String filename) throws Exception {
    Model originalModel = parser.createModel(new ConverterParams().filename(filename));
    return getModelAfterSerializing(originalModel);
  }

  private Model getModelAfterSerializing(Model originalModel) throws Exception {
    String xml = exporter.toXml(originalModel);
    ByteArrayInputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
    Model result = parser.createModel(new ConverterParams().inputStream(stream));
    // showImage(originalModel);
    // showImage(result);
    return result;
  }

  protected void showImage(Model model) throws Exception {
    String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();
    AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    NormalImageGenerator nig = new PngImageGenerator(params);
    String pathWithouExtension = dir + "/" + model.getName();
    String pngFilePath = pathWithouExtension.concat(".png");
    nig.saveToFile(pngFilePath);
    Desktop.getDesktop().open(new File(pngFilePath));

  }

  @Test
  public void testExportSpecies() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/SpeciesGlyph_Example_level2_level3.xml");
    assertNotNull(model);
    assertEquals(1, model.getElements().size());
    Species glucoseSpecies = model.getElementByElementId("SpeciesGlyph_Glucose");
    assertNotNull(glucoseSpecies.getX());
    assertNotNull(glucoseSpecies.getY());
    assertTrue(glucoseSpecies.getWidth() > 0);
    assertTrue(glucoseSpecies.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= glucoseSpecies.getX() + glucoseSpecies.getWidth());
    assertTrue(model.getHeight() >= glucoseSpecies.getY() + glucoseSpecies.getHeight());
    assertFalse(glucoseSpecies.getClass().equals(Species.class));
  }

  @Test
  public void testExportSpeciesInCompartments() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/SpeciesGlyph_Example.xml");
    assertNotNull(model);
    assertEquals(2, model.getElements().size());
    Compartment compartment = model.getElementByElementId("Yeast");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());

    assertNotNull(compartment.getX() >= 0);
    assertNotNull(compartment.getY() >= 0);
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
    assertTrue(compartment.getElements().size() > 0);
  }

  @Test
  public void testExportReaction() throws Exception {
    Model model = getModelAfterSerializing("testFiles/layoutExample/Complete_Example.xml");
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    for (ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());
  }

  @Test
  public void testExportModelId() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setIdModel("Test123");
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(model.getIdModel(), deserializedModel.getIdModel());
  }

  @Test
  public void testExportModelDimension() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.setWidth(200);
    model.setHeight(300);
    Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(model.getWidth(), deserializedModel.getWidth());
    assertEquals(model.getHeight(), deserializedModel.getHeight());
  }

  @Test
  public void testExportEmptyModel() throws Exception {
    Model model = new ModelFullIndexed(null);
    Model deserializedModel = getModelAfterSerializing(model);

    assertNotNull(deserializedModel);
  }

  @Test
  public void testExportReactionWithLayout() throws Exception {
    Model model = createModelWithReaction();
    Reaction reaction = model.getReactions().iterator().next();

    Model deserializedModel = getModelAfterSerializing(model);
    Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getReactants().get(0).getLine().length(),
        deserializedReaction.getReactants().get(0).getLine().length(), Configuration.EPSILON);
    assertEquals(reaction.getProducts().get(0).getLine().length(),
        deserializedReaction.getProducts().get(0).getLine().length(), Configuration.EPSILON);

  }

  private Model createModelWithReaction() {
    Model model = new ModelFullIndexed(null);
    GenericProtein p1 = new GenericProtein("s1");
    p1.setWidth(100);
    p1.setHeight(20);
    p1.setX(10);
    p1.setY(10);
    model.addElement(p1);
    GenericProtein p2 = new GenericProtein("s2");
    p2.setWidth(100);
    p2.setHeight(20);
    p2.setX(10);
    p2.setY(50);
    model.addElement(p2);
    Reaction reaction = new StateTransitionReaction();
    Reactant reactant = new Reactant(p1);
    Product product = new Product(p2);
    PolylineData reactantLine = new PolylineData();
    reactantLine.addPoint(new Point2D.Double(50, 20));
    reactantLine.addPoint(new Point2D.Double(50, 30));
    reactant.setLine(reactantLine);
    PolylineData productLine = new PolylineData();
    productLine.addPoint(new Point2D.Double(50, 40));
    productLine.addPoint(new Point2D.Double(50, 60));
    product.setLine(productLine);
    reaction.addReactant(reactant);
    reaction.addProduct(product);
    model.addReaction(reaction);
    return model;
  }

  @Test
  public void testExportReactionWithNotes() throws Exception {
    Model model = createModelWithReaction();
    Reaction reaction = model.getReactions().iterator().next();
    reaction.setNotes("XYZ");

    Model deserializedModel = getModelAfterSerializing(model);
    Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getNotes(), deserializedReaction.getNotes());

  }

  @Test
  public void testExportModelWithReaction() throws Exception {
    IConverter converter = new SbmlParser();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    model.setName(null);

    converter.exportModelToFile(model, "tmp.xml");

    Model model2 = converter.createModel(new ConverterParams().filename("tmp.xml"));
    model2.setName(null);

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));

  }

  @Test
  public void testExportModelName() throws Exception {
    IConverter converter = new SbmlParser();

    Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    model.setName("UNKNOWN DISEASE MAP");
    model.setIdModel("id1");

    converter.exportModelToFile(model, "tmp.xml");

    Model model2 = converter.createModel(new ConverterParams().filename("tmp.xml"));

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));

  }
  
  @Test
  public void testExportProblematicNotes() throws Exception {
    Model model = createModelWithReaction();
    Reaction reaction = model.getReactions().iterator().next();
    reaction.setNotes("X=Y<Z");

    Model deserializedModel = getModelAfterSerializing(model);
    Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getNotes(), deserializedReaction.getNotes());

  }
}
