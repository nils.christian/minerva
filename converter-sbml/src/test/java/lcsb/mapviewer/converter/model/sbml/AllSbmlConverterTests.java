package lcsb.mapviewer.converter.model.sbml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.converter.model.sbml.reaction.AllSbmlReactionParserTests;
import lcsb.mapviewer.converter.model.sbml.species.AllSbmlSpeciesTests;

@RunWith(Suite.class)
@SuiteClasses({ AllSbmlReactionParserTests.class, //
    AllSbmlSpeciesTests.class, //
    GenericSbmlParserTest.class, //
    GenericSbmlToXmlParserTest.class, //
    SbmlExporterTest.class, //
    SbmlPareserForInvalidReactionTest.class, //
    SbmlParserTest.class, //
})
public class AllSbmlConverterTests {

}
