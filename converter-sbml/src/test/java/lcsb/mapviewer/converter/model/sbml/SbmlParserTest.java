package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlParserTest {
  Logger logger = Logger.getLogger(SbmlParserTest.class);
  SbmlParser parser = new SbmlParser();

  @Test
  public void testParseCompartment() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml"));
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    Compartment compartment = model.getElementByElementId("CompartmentGlyph_1");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
  }

  @Test
  public void testParseUnits() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml"));
    assertNotNull(model);
    assertTrue("Units weren't parsed", model.getUnits().size() > 0);
  }

  @Test
  public void testParseKinetics() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/SBML.xml"));
    for (Reaction reaction : model.getReactions()) {
      assertNotNull("Kinetics is not parsed", reaction.getKinetics());
      assertNotNull("No math definition for kinetic law", reaction.getKinetics().getDefinition());
      assertTrue("There should be a kinetic parameter defined", reaction.getKinetics().getParameters().size() > 0);
      assertTrue("Elements used by kinetics are not available", reaction.getKinetics().getElements().size() > 0);
    }
  }

  @Test
  public void testParseGlobalParameters() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/kinetic_global_paramter.xml"));
    assertTrue("There should be a kinetic parameter defined", model.getParameters().size() > 0);
  }

  @Test
  public void testParseFunctions() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/kinetic_function.xml"));
    assertTrue("There should be a kinetic function defined", model.getFunctions().size() > 0);
  }

  @Test
  public void testParseSpecies() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/SpeciesGlyph_Example_level2_level3.xml"));
    assertNotNull(model);
    assertEquals(1, model.getElements().size());
    Species glucoseSpecies = model.getElementByElementId("SpeciesGlyph_Glucose");
    assertNotNull(glucoseSpecies.getX());
    assertNotNull(glucoseSpecies.getY());
    assertTrue(glucoseSpecies.getWidth() > 0);
    assertTrue(glucoseSpecies.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= glucoseSpecies.getX() + glucoseSpecies.getWidth());
    assertTrue(model.getHeight() >= glucoseSpecies.getY() + glucoseSpecies.getHeight());
    assertFalse(glucoseSpecies.getClass().equals(Species.class));
  }

  @Test
  public void testParseSpeciesInCompartments() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/SpeciesGlyph_Example.xml"));
    assertNotNull(model);
    assertEquals(2, model.getElements().size());
    Compartment compartment = model.getElementByElementId("Yeast");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
    assertTrue(compartment.getElements().size() > 0);
  }

  @Test
  public void testParseReaction() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/Complete_Example.xml"));
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    for (ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());

  }

  @Test
  public void testReactionWithoutLayout() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/Complete_Example_level2.xml"));
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    for (ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());

  }

  @Test
  public void testAnnotationsInComp() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/compartment_with_annotation.xml"));
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    assertEquals(1, model.getCompartments().iterator().next().getMiriamData().size());
  }

}
