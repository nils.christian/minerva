package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;

import lcsb.mapviewer.converter.model.sbml.SbmlCompartmentExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class SbmlSpeciesExporterTest {

  Logger logger = Logger.getLogger(SbmlSpeciesExporterTest.class);

  SbmlCompartmentExporter compartmentExporter;

  org.sbml.jsbml.Model sbmlModel;
  Layout sbmlLayout;

  @Test
  public void testOneElementWithTwoAliases() throws InconsistentModelException {
    Element protein1 = new GenericProtein("sa1");
    protein1.setName("SNCA");
    Element protein2 = new GenericProtein("sa2");
    protein2.setName("SNCA");
    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);

    SbmlSpeciesExporter exporter = createExporter(model);

    exporter.exportElements(sbmlModel);

    assertEquals(1, sbmlModel.getSpeciesCount());
  }

  @Test
  public void testOneElementButInTwoCompartments() throws InconsistentModelException {
    Compartment compartment = new Compartment("c1");
    compartment.setName("test");
    Element protein1 = new GenericProtein("sa1");
    protein1.setName("SNCA");
    Element protein2 = new GenericProtein("sa2");
    protein2.setName("SNCA");
    protein2.setCompartment(compartment);
    
    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addElement(compartment);

    SbmlSpeciesExporter exporter = createExporter(model);

    exporter.exportElements(sbmlModel);

    assertEquals(2, sbmlModel.getSpeciesCount());
  }

  private SbmlSpeciesExporter createExporter(Model model) throws InconsistentModelException {
    SBMLDocument doc = new SBMLDocument(3, 1);
    sbmlModel = doc.createModel(model.getIdModel());

    LayoutModelPlugin layoutPlugin = new LayoutModelPlugin(sbmlModel);
    sbmlLayout = new Layout();
    layoutPlugin.add(sbmlLayout);
    sbmlModel.addExtension("layout", layoutPlugin);

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(sbmlLayout, model);
    compartmentExporter.exportElements(sbmlModel);
    SbmlSpeciesExporter result = new SbmlSpeciesExporter(sbmlLayout, model, compartmentExporter);
    return result;
  }

}
