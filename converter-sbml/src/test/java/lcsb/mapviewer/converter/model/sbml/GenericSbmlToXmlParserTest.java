package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;

@RunWith(Parameterized.class)
public class GenericSbmlToXmlParserTest {

  static Logger logger = Logger.getLogger(GenericSbmlToXmlParserTest.class.getName());

  private Path filePath;

  public GenericSbmlToXmlParserTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/layoutExample")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")) {
        data.add(new Object[] { fPath });
      }
    });
    Files.walk(Paths.get("testFiles/small")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test
  public void toXmlModelTest() throws Exception {
    try {

      String dir = Files.createTempDirectory("sbgn-temp-images-dir").toFile().getAbsolutePath();

      IConverter converter = new SbmlParser();

      Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
      model.setName(null);

      String pathWithouExtension = dir + "/"
          + filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".xml"));
      String xmlFilePath = pathWithouExtension.concat(".xml");
      converter.exportModelToFile(model, xmlFilePath);

      Model model2 = converter.createModel(new ConverterParams().filename(xmlFilePath).sizeAutoAdjust(false));
      model2.setName(null);

      assertNotNull(model2);
      ModelComparator comparator = new ModelComparator(1.0);
      assertEquals(0, comparator.compare(model, model2));
      FileUtils.deleteDirectory(new File(dir));

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw e;
    }
  }

}
