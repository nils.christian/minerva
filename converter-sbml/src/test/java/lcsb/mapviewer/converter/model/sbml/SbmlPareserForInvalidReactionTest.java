package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;

@RunWith(Parameterized.class)
public class SbmlPareserForInvalidReactionTest {

  static Logger logger = Logger.getLogger(SbmlPareserForInvalidReactionTest.class.getName());

  private Path filePath;

  public SbmlPareserForInvalidReactionTest(Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/invalidReaction")).forEach(fPath -> {
      if (Files.isRegularFile(fPath) && fPath.toString().endsWith(".xml")) {
        data.add(new Object[] { fPath });
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    try {
      IConverter converter = new SbmlParser();

      converter.createModel(new ConverterParams().filename(filePath.toString()));
      fail("Exception expected when parsing file: " + filePath.toString());
    } catch (InvalidInputDataExecption e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
