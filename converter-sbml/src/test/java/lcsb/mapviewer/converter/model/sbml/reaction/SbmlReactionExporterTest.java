package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;

import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlReactionExporterTest {

  @Test
  public void testTriggerReactionToSbml() throws InconsistentModelException {
    ModelFullIndexed model = new ModelFullIndexed(null);

    SbmlReactionExporter exporter = createExporter(model);
    org.sbml.jsbml.Reaction result = exporter.createSbmlElement(new TriggerReaction());
    assertNotNull(result);
  }

  private SbmlReactionExporter createExporter(ModelFullIndexed model) {
    SBMLDocument doc = new SBMLDocument(3, 1);
    Model result = doc.createModel(model.getIdModel());
    result.setName(model.getName());
    LayoutModelPlugin plugin = new LayoutModelPlugin(result);

    Layout layout = new Layout();
    Dimensions dimensions = new Dimensions();
    if (model.getHeight() != null) {
      dimensions.setHeight(model.getHeight());
    } else {
      dimensions.setHeight(640);
    }
    if (model.getWidth() != null) {
      dimensions.setWidth(model.getWidth());
    } else {
      dimensions.setWidth(480);
    }
    layout.setDimensions(dimensions);
    plugin.add(layout);
    result.addExtension("layout", plugin);

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(layout, model);
    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> speciesExporter = new SbmlSpeciesExporter(layout, model,
        compartmentExporter);

    SbmlReactionExporter exporter = new SbmlReactionExporter(layout, model, speciesExporter, compartmentExporter);
    exporter.setSbmlModel(result);
    return exporter;
  }

}
