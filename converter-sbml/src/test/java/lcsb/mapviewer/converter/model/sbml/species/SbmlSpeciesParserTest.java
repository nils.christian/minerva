package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Unknown;

public class SbmlSpeciesParserTest {
  Logger logger = Logger.getLogger(SbmlSpeciesParserTest.class);
  SbmlParser parser = new SbmlParser();

  ColorParser colorParser = new ColorParser();

  @Test
  public void testParseAntisenseRna() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/antisense_rna.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof AntisenseRna);
    assertEquals(ElementColorEnum.ANTISENSE_RNA.getColor(), element.getColor());
  }

  @Test
  public void testParseComplex() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/complex.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Complex);
    assertEquals(ElementColorEnum.COMPLEX.getColor(), element.getColor());
  }

  @Test
  public void testParseDegraded() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/degraded.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Degraded);
    assertEquals(ElementColorEnum.DEGRADED.getColor(), element.getColor());
  }

  @Test
  public void testParseDrug() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/drug.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Drug);
    assertEquals(ElementColorEnum.DRUG.getColor(), element.getColor());
  }

  @Test
  public void testParseGene() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/gene.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Gene);
    assertEquals(ElementColorEnum.GENE.getColor(), element.getColor());
  }

  @Test
  public void testParseGenericProtein() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/generic_protein.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof GenericProtein);
    assertEquals(ElementColorEnum.GENERIC_PROTEIN.getColor(), element.getColor());
  }

  @Test
  public void testParseIonChannel() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/ion_channel.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof IonChannelProtein);
    assertEquals(ElementColorEnum.ION_CHANNEL.getColor(), element.getColor());
  }

  @Test
  public void testParseIon() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/ion.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Ion);
    assertEquals(ElementColorEnum.ION.getColor(), element.getColor());
  }

  @Test
  public void testParsePhenotype() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/phenotype.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Phenotype);
    assertEquals(ElementColorEnum.PHENOTYPE.getColor(), element.getColor());
  }

  @Test
  public void testParseReceptor() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/receptor.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof ReceptorProtein);
    assertEquals(ElementColorEnum.RECEPTOR.getColor(), element.getColor());
  }

  @Test
  public void testParseRna() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/rna.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Rna);
    assertEquals(ElementColorEnum.RNA.getColor(), element.getColor());
  }

  @Test
  public void testParseSimpleMolecule() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/small_molecule.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof SimpleMolecule);
    assertEquals(ElementColorEnum.SIMPLE_MOLECULE.getColor(), element.getColor());
  }

  @Test
  public void testParseSimpleMoleculeWithAlternativeSBOTerm() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/small_molecule2.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof SimpleMolecule);
  }

  @Test
  public void testParseUnknown() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/unknown_species.xml"));
    Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Unknown);
    assertEquals(ElementColorEnum.UNKNOWN.getColor(), element.getColor());
  }
}
