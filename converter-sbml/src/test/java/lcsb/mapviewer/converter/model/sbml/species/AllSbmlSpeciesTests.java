package lcsb.mapviewer.converter.model.sbml.species;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SbmlSpeciesExporterTest.class, SbmlSpeciesParserTest.class })
public class AllSbmlSpeciesTests {

}
