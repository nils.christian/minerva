package lcsb.mapviewer.converter.model.sbml.reaction;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SbmlReactionExporterTest.class, //
    SbmlReactionParserTest.class //
})
public class AllSbmlReactionParserTests {

}
