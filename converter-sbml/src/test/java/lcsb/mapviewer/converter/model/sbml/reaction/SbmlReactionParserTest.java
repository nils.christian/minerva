package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;

public class SbmlReactionParserTest {
  Logger logger = Logger.getLogger(SbmlReactionParserTest.class);
  SbmlParser parser = new SbmlParser();

  @Test
  public void testParseCatalysis() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/catalysis.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof Catalysis);
  }

  @Test
  public void testParseInhibition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/inhibition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof Inhibition);
  }

  @Test
  public void testParseUnknownCatalysis() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/unknown_catalysis.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof UnknownCatalysis);
  }

  @Test
  public void testParseUnknownInhibition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/modifier/unknown_inhibition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction.getModifiers().iterator().next() instanceof UnknownInhibition);
  }

  @Test
  public void testParseStateTransition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/state_transition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof StateTransitionReaction);
  }

  @Test
  public void testParseTranscription() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/transcription.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TranscriptionReaction);
  }

  @Test
  public void testParseTranslation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/translation.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TranslationReaction);
  }

  @Test
  public void testParseTransport() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/transport.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TransportReaction);
  }

  @Test
  public void testParseKnownTransitionOmitted() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/known_transition_omitted.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof KnownTransitionOmittedReaction);
  }

  @Test
  public void testParseUnknownTransition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/unknown_transition.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof UnknownTransitionReaction);
  }

  @Test
  public void testParseHeterodimerAssociation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/heterodimer_association.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
  }

  @Test
  public void testParseDissociation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/dissociation.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof DissociationReaction);
  }

  @Test
  public void testParseTruncation() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/truncation.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof TruncationReaction);
  }

  @Test
  public void testParsePositiveInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/positive_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof PositiveInfluenceReaction);
  }

  @Test
  public void testParseUnknownPositiveInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/unknown_positive_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue("Invalid class type: " + reaction.getClass(), reaction instanceof UnknownPositiveInfluenceReaction);
  }

  @Test
  public void testParseNegativeInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/reaction/negative_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof NegativeInfluenceReaction);
  }

  @Test
  public void testParseUnknownNegativeInfluence() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/reaction/unknown_negative_influence.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction);
    assertTrue(reaction instanceof UnknownNegativeInfluenceReaction);
  }

}
