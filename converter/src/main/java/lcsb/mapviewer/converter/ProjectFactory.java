package lcsb.mapviewer.converter;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;

import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;

public class ProjectFactory {
	
	/**
	 * Deafult class clogger.
	 */
	@SuppressWarnings("unused")
	private Logger							logger = Logger.getLogger(ProjectFactory.class);
	
	private ComplexZipConverter	converter;

	public ProjectFactory(ComplexZipConverter converter) {
		this.converter = converter;
	}

	public Project create(ComplexZipConverterParams params) throws InvalidInputDataExecption {
		return create(params, new Project());
	}

	public Project create(ComplexZipConverterParams params, Project project) throws InvalidInputDataExecption {
		Model model = converter.createModel(params);

		Set<Model> models = new HashSet<>();
		models.add(model);
		models.addAll(model.getSubmodels());

		project.addModel(model);
		ZipFile zipFile = params.getZipFile();
		Enumeration<? extends ZipEntry> entries;

		entries = zipFile.entries();
		List<ImageZipEntryFile> imageEntries = new ArrayList<>();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if (!entry.isDirectory()) {
				ZipEntryFile zef = params.getEntry(entry.getName());
				if (zef instanceof ImageZipEntryFile) {
					imageEntries.add((ImageZipEntryFile) zef);
				}
			}
		}

		if (imageEntries.size() > 0) {
			OverviewParser parser = new OverviewParser();
			project.addOverviewImages(parser.parseOverviewLinks(models, imageEntries, params.getVisualizationDir(), zipFile));
		}
		return project;
	}
}
