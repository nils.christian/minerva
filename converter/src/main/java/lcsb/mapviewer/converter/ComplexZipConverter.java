package lcsb.mapviewer.converter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class allows to create complex {@link Model} that contains submaps. It's
 * written in generic way and use {@link IConverter} as a class to parse single
 * submap.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexZipConverter {

  /**
   * Size of the buffer used for accessing single chunk of data from input stream.
   */
  private static final int BUFFER_SIZE = 1024;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ComplexZipConverter.class);

  /**
   * Class used to create single submap from a file.
   */
  private Class<? extends IConverter> converterClazz;

  /**
   * Default constructor. Checks if the class given in the parameter is proper
   * {@link IConverter} implementation.
   * 
   * @param clazz
   *          {@link IConverter} class used for creation of the single submap
   */
  public ComplexZipConverter(Class<? extends IConverter> clazz) {
    if (Modifier.isAbstract(clazz.getModifiers())) {
      throw new InvalidClassException("Param class cannot be abstract");
    }

    if (Modifier.isInterface(clazz.getModifiers())) {
      throw new InvalidClassException("Param class cannot be an interface");
    }

    converterClazz = clazz;
  }

  /**
   * Creates complex {@link Model} that contains submaps.
   * 
   * @param params
   *          {@link ComplexZipConverterParams object} with information about data
   *          from which result is going to be created
   * @return complex {@link Model} created from input data
   * @throws InvalidInputDataExecption
   *           thrown when there is a problem with accessing input data
   */
  public Model createModel(ComplexZipConverterParams params) throws InvalidInputDataExecption {
    try {
      ZipFile zipFile = params.getZipFile();
      Enumeration<? extends ZipEntry> entries;
      String mapping = validateSubmodelInformation(params, zipFile);

      IConverter converter = createConverterInstance();
      Map<String, Model> filenameModelMap = new HashMap<>();
      Map<String, Model> nameModelMap = new HashMap<>();

      entries = zipFile.entries();
      Model result = null;
      int overlayOrder = 1;
      List<Layout> layouts = new ArrayList<>();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          ZipEntryFile zef = params.getEntry(entry.getName());
          if (zef instanceof ModelZipEntryFile) {
            ModelZipEntryFile modelEntryFile = (ModelZipEntryFile) zef;
            InputStream is = zipFile.getInputStream(entry);
            ConverterParams cParams = new ConverterParams();
            cParams.inputStream(is);
            Model model = converter.createModel(cParams);
            model.setName(modelEntryFile.getName());
            filenameModelMap.put(entry.getName(), model);
            nameModelMap.put(FilenameUtils.getBaseName(modelEntryFile.getFilename()).toLowerCase(), model);
            if (modelEntryFile.isRoot()) {
              result = model;
            }
          } else if (zef instanceof LayoutZipEntryFile) {
            layouts.add(layoutZipEntryFileToLayout(params, zipFile, entry, (LayoutZipEntryFile) zef, overlayOrder++));
          } else if (zef instanceof ImageZipEntryFile) {
            continue;
            // imageEntries.add((ImageZipEntryFile) zef);
          } else if (!isIgnoredFile(entry.getName())) {
            throw new NotImplementedException("Unknwon entry type: " + zef.getClass());
          }
        }
      }

      for (Entry<String, Model> entry : filenameModelMap.entrySet()) {
        String filename = entry.getKey();
        Model model = entry.getValue();
        ZipEntryFile zef = params.getEntry(filename);
        if (zef instanceof ModelZipEntryFile) {
          ModelZipEntryFile modelEntryFile = (ModelZipEntryFile) zef;
          if (!modelEntryFile.isRoot() && !modelEntryFile.isMappingFile()) {
            ModelSubmodelConnection submodel = new ModelSubmodelConnection(model, modelEntryFile.getType());
            submodel.setName(modelEntryFile.getName());
            result.addSubmodelConnection(submodel);
          }
        }
      }
      for (Layout layout : layouts) {
        Layout topLayout = new Layout(layout);
        result.addLayout(topLayout);
        int modelId = 0;
        topLayout.setDirectory(topLayout.getDirectory() + modelId);
        modelId++;
        for (ModelSubmodelConnection connection : result.getSubmodelConnections()) {
          Layout childLayout = new Layout(layout);
          // we need to set separate directory names for different submodels
          childLayout.setDirectory(childLayout.getDirectory() + modelId);
          modelId++;
          childLayout.setParentLayout(topLayout);
          connection.getSubmodel().addLayout(childLayout);
        }
      }
      Model mappingModel = filenameModelMap.get(mapping);
      if (mappingModel != null) {
        for (Reaction reaction : mappingModel.getReactions()) {
          processReaction(mapping, nameModelMap, result, reaction);
        }
      }
      return result;
    } catch (IOException e) {
      throw new InvalidArgumentException(e);
    }
  }

  protected boolean isIgnoredFile(String name) {
    if (name == null) {
      return true;
    } else if (name.isEmpty()) {
      return true;
    } else if (name.startsWith(".DS_Store")) {
      return true;
    } else if (name.startsWith("__MACOSX")) {
      return true;
    }
    return false;
  }

  /**
   * Process a single reaction in mapping file (transfomring reaction into
   * connection between submodels).
   * 
   * @param mapping
   *          name of the mapping file
   * @param nameModelMap
   *          mapping between file names and models
   * @param topModel
   *          top model
   * @param reaction
   *          reaction to transform into connection
   */
  public void processReaction(String mapping, Map<String, Model> nameModelMap, Model topModel, Reaction reaction) {
    if (reaction.getReactants().size() > 1) {
      logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
          + ") contains too many reactants. Skipped");
    } else if (reaction.getProducts().size() > 1) {
      logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
          + ") contains too many products. Skipped");
    } else if (reaction.getModifiers().size() > 0) {
      logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
          + ") contains modifiers. Skipped");
    } else {
      Element fromAlias = reaction.getReactants().get(0).getElement();
      Element toAlias = reaction.getProducts().get(0).getElement();
      if (!(fromAlias instanceof Species)) {
        logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
            + ") contains doesn't start in species. Skipped");
      } else if (!(toAlias instanceof Species)) {
        logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
            + ") contains doesn't end in species. Skipped");
      } else {
        Complex complexFrom = ((Species) fromAlias).getComplex();
        Complex complexTo = ((Species) toAlias).getComplex();
        if (complexFrom == null) {
          logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
              + ") contains doesn't start inside complex. Skipped");
        } else if (complexTo == null && (!(toAlias instanceof Complex))) {
          logger.warn("[SUBMODEL MAPPING] Reaction " + reaction.getIdReaction() + " in mapping file (" + mapping
              + ") contains doesn't end inside complex. Skipped");
        } else {
          if (complexTo == null) {
            complexTo = (Complex) toAlias;
          }
          String fromName = complexFrom.getName().toLowerCase();
          String toName = complexTo.getName().toLowerCase();
          Model fromModel = nameModelMap.get(fromName);
          Model toModel = nameModelMap.get(toName);
          if (fromModel == null) {
            throw new InvalidArgumentException(
                "Mapping file references to " + fromName + " submodel. But such model doesn't exist");
          } else if (toModel == null) {
            throw new InvalidArgumentException(
                "Mapping file references to " + toName + " submodel. But such model doesn't exist");
          }
          Element source = fromModel.getElementByElementId(fromAlias.getName());
          if (source == null) {
            throw new InvalidArgumentException("Mapping file references to element with alias: " + fromAlias.getName()
                + ". But such element doesn't exist");
          }
          Element dest = null;
          if (!(toAlias instanceof Complex)) {
            dest = fromModel.getElementByElementId(toAlias.getName());
          }
          SubmodelType type = SubmodelType.UNKNOWN;
          if (fromAlias instanceof Protein) {
            type = SubmodelType.DOWNSTREAM_TARGETS;
          } else if (fromAlias instanceof Phenotype) {
            type = SubmodelType.PATHWAY;
          }
          ElementSubmodelConnection connection = new ElementSubmodelConnection(toModel, type);
          connection.setFromElement(source);
          connection.setToElement(dest);
          connection.setName(toModel.getName());
          source.setSubmodel(connection);
        }
      }
    }
  }

  /**
   * This method validates if information about model and submodels in the params
   * are sufficient. If not then appropriate exception will be thrown.
   * 
   * @param params
   *          parameters to validate
   * @param zipFile
   *          original {@link ZipFile}
   * @return name of the file containing mapping between submodels
   */
  protected String validateSubmodelInformation(ComplexZipConverterParams params, ZipFile zipFile) {
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    Set<String> processed = new HashSet<>();

    String root = null;
    String mapping = null;
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        String name = entry.getName();
        ZipEntryFile zef = params.getEntry(name);
        if (zef == null && !isIgnoredFile(name)) {
          throw new InvalidArgumentException("No information found in params about file: " + name);
        }
        if (zef instanceof ModelZipEntryFile) {
          ModelZipEntryFile modelEntryFile = (ModelZipEntryFile) zef;
          if (modelEntryFile.isRoot()) {
            if (root != null) {
              throw new InvalidArgumentException("Two roots found: " + name + ", " + root + ". There can be only one.");
            }
            root = name;
          }
          if (modelEntryFile.isMappingFile()) {
            if (mapping != null) {
              throw new InvalidArgumentException(
                  "Two mapping files found: " + name + ", " + mapping + ". There can be only one.");
            }
            mapping = name;
          }
        }
        processed.add(name.toLowerCase());
      }
    }
    if (root == null) {
      throw new InvalidArgumentException("No root map found.");
    }
    for (String entryName : params.getFilenames()) {
      if (!processed.contains(entryName)) {
        throw new InvalidArgumentException("Entry " + entryName + " doesn't exists in the zip file.");
      }
    }
    return mapping;
  }

  /**
   * Transforms {@link LayoutZipEntryFile} into {@link Layout}.
   * 
   * @param zipFile
   *          original {@link ZipFile}
   * @param entry
   *          entry in a zip file
   * @param params
   *          parameters used to make general conversion (we use directory where
   *          layout should be stored)
   * @param layoutEntry
   *          {@link LayoutZipEntryFile} to transform
   * @return {@link LAyout} for a given {@link LayoutZipEntryFile}
   * @throws IOException
   *           thrown when there is a problem with accessing {@link ZipFile}
   */
  protected Layout layoutZipEntryFileToLayout(ComplexZipConverterParams params, ZipFile zipFile, ZipEntry entry,
      LayoutZipEntryFile layoutEntry, int order) throws IOException {
    Layout layout = new Layout();
    layout.setDescription(layoutEntry.getDescription());
    layout.setDirectory(params.getVisualizationDir() + "/" + layoutEntry.getName().replaceAll(" ", "_"));
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(zipFile.getInputStream(entry)));
    fileEntry.setOriginalFileName(entry.getName());
    fileEntry.setLength(fileEntry.getFileContent().length);
    layout.setInputData(fileEntry);
    layout.setPublicLayout(true);
    layout.setTitle(layoutEntry.getName());
    layout.setOrderIndex(order);
    return layout;
  }

  /**
   * Copy file from zip entry into temporary file.
   * 
   * @param zipFile
   *          input zip file
   * @param entry
   *          entry in the zip file
   * @return {@link File} with a copy of data from zip entry
   * @throws IOException
   *           thrown when there is a problem with input or output file
   */
  protected File saveFileFromZipFile(ZipFile zipFile, ZipEntry entry) throws IOException {
    InputStream is = zipFile.getInputStream(entry);
    File result = File.createTempFile("temp-file-name", ".png");
    FileOutputStream fos = new FileOutputStream(result);
    byte[] bytes = new byte[BUFFER_SIZE];
    int length;
    while ((length = is.read(bytes)) >= 0) {
      fos.write(bytes, 0, length);
    }
    fos.close();
    return result;
  }

  /**
   * Creates inctance of {@link IConverter} used as a template parameter for this
   * class instatiation.
   * 
   * @return inctance of {@link IConverter}
   */
  protected IConverter createConverterInstance() {
    IConverter converter;
    try {
      converter = converterClazz.newInstance();
    } catch (InstantiationException e) {
      throw new InvalidClassException("Problem with instantation of the class: " + converterClazz, e);
    } catch (IllegalAccessException e) {
      throw new InvalidClassException("Problem with instantation of the class: " + converterClazz, e);
    }
    return converter;
  }
}
