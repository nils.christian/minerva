package lcsb.mapviewer.converter.annotation;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * This class allow to parse xml nodes responsible for species annotations in
 * CellDesigner.
 * 
 * @author Piotr Gawron
 * 
 */
public class XmlAnnotationParser extends XmlParser {

	/**
	 * Deafult logger.
	 */
	private static Logger logger = Logger.getLogger(XmlAnnotationParser.class.getName());

	/**
	 * Default constructor.
	 */
	public XmlAnnotationParser() {
	}

	/**
	 * This method parse the xml string passed as an argument. All information
	 * obtained by the method are stored in local variables: miriamDataSet,
	 * speciesId and can be accessed later on.
	 * 
	 * @param data
	 *          - xml string to be parsed
	 * 
	 * @return collection of miriam objects that were obtained from the xml node
	 * @throws InvalidXmlSchemaException
	 *           thrown when there is a problem with xml
	 */
	public Set<MiriamData> parse(String data) throws InvalidXmlSchemaException {
		// start from creating a DOM parser and parse the whole document
		Document doc = getXmlDocumentFromString(data);

		NodeList root = doc.getChildNodes();

		// process the whole schema
		return parseRdfNode(root);
	}

	/**
	 * Retrieves set of Miriam annotation from xml rdf node.
	 * 
	 * @param root
	 *          xml node list
	 * @return set of miriam annotations.
	 * @throws InvalidXmlSchemaException
	 *           thrown when there is a problem with xml
	 */
	public Set<MiriamData> parseRdfNode(NodeList root) throws InvalidXmlSchemaException {
		Node rdf = getNode("rdf:RDF", root);
		return parseRdfNode(rdf);
	}

	/**
	 * Retrieves set of Miriam annotation from xml rdf node.
	 * 
	 * @param rdf
	 *          xml node
	 * @return set of miriam annotations.
	 * @throws InvalidXmlSchemaException
	 *           thrown when there is a problem with xml
	 */
	public Set<MiriamData> parseRdfNode(Node rdf) throws InvalidXmlSchemaException {
		Set<MiriamData> miriamDataSet = new HashSet<MiriamData>();
		if (rdf != null) {
			Node description = getNode("rdf:Description", rdf.getChildNodes());
			if (description != null) {
				NodeList list = description.getChildNodes();
				for (int i = 0; i < list.getLength(); i++) {
					Node node = list.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						miriamDataSet.addAll(parseMiriamNode(node));
					}
				}
			} else {
				throw new InvalidXmlSchemaException("rdf:Description node not found");
			}
		} else {
			throw new InvalidXmlSchemaException("rdf:Rdf node not found");
		}
		return miriamDataSet;
	}

	/**
	 * This method converts a xml node into MiriamData object (annotation of a
	 * species).
	 * 
	 * @param node
	 *          - xml node that contains representation of a single annotation of
	 *          a species
	 * @return MiriamData object that represents annotation of a species
	 * @throws InvalidXmlSchemaException
	 *           thrown when there is a problem with xml
	 */
	public Set<MiriamData> parseMiriamNode(Node node) throws InvalidXmlSchemaException {
		Set<MiriamData> result = new HashSet<MiriamData>();
		NodeList list = node.getChildNodes();
		String relationTypeString = node.getNodeName();
		MiriamRelationType relationType = MiriamRelationType.getTypeByStringRepresentation(relationTypeString);
		Node bag = getNode("rdf:Bag", list);
		if (bag == null) {
			throw new InvalidXmlSchemaException("No rdf:Bag node found");
		}
		list = bag.getChildNodes();
		List<Node> nodes = getNodes("rdf:li", list);
		for (Node li : nodes) {
			String dataTypeUri = getNodeAttr("rdf:resource", li);
			if (dataTypeUri == null || dataTypeUri.isEmpty()) {
				throw new InvalidXmlSchemaException("rdf:li does not have a rdf:resource attribute");
			}

			try {
				MiriamData md = MiriamType.getMiriamByUri(dataTypeUri);
				if (relationType == null) {
					logger.warn("Unknown relation type: " + relationTypeString + ". For miriam uri: " + dataTypeUri + ".");
				} else {
					if (!MiriamType.PUBMED.equals(md.getDataType())) {
						md.setRelationType(relationType);
					}
					result.add(md);
				}
			} catch (InvalidArgumentException e) {
				logger.warn(e.getMessage());
			}
		}
		return result;
	}

	/**
	 * This method converts a set of MiriamData into xml string that can be put to
	 * CellDesigner schema.
	 * 
	 * @param data
	 *          - a set of MiriamData to be converted.
	 * @return xml string representation of the input data
	 */
	public String dataSetToXmlString(Collection<MiriamData> data) {
		StringBuilder result = new StringBuilder("");
		result.append(
				"<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" " + "xmlns:dc=\"http://purl.org/dc/elements/1.1/\" "
						+ "xmlns:dcterms=\"http://purl.org/dc/terms/\" " + "xmlns:vCard=\"http://www.w3.org/2001/vcard-rdf/3.0#\" "
						+ "xmlns:bqbiol=\"http://biomodels.net/biology-qualifiers/\" " + "xmlns:bqmodel=\"http://biomodels.net/model-qualifiers/\">\n");
		result.append("<rdf:Description rdf:about=\"#\">\n");
		for (MiriamData miriamData : data) {
			result.append(miriamDataToXmlString(miriamData));
		}
		result.append("</rdf:Description>\n");
		result.append("</rdf:RDF>\n");
		return result.toString();
	}

	/**
	 * This method converts a single MiriamData into xml string that can be put to
	 * CellDesigner schema.
	 * 
	 * @param data
	 *          - a MiriamData to be converted.
	 * @return xml string representation of the input data
	 */
	public String miriamDataToXmlString(MiriamData data) {
		StringBuilder result = new StringBuilder("");
		result.append("<" + data.getRelationType().getStringRepresentation() + ">\n");
		result.append("<rdf:Bag>\n");
		result.append("<rdf:li rdf:resource=\"" + data.getDataType().getUris().get(0) + ":" + data.getResource().replaceAll(":", "%3A") + "\"/>\n");
		result.append("</rdf:Bag>\n");
		result.append("</" + data.getRelationType().getStringRepresentation() + ">\n");

		return result.toString();
	}

}
