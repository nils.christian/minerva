package lcsb.mapviewer.converter;

/**
 * Generic exception thrown when implementation of {@link IConverter} encounter
 * a problem.
 * 
 * @author Piotr Gawron
 * 
 */
public class ConverterException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Default constructor with exception message.
	 * 
	 * @param message
	 *          error message
	 */
	public ConverterException(String message) {
		super(message);
	}

	/**
	 * Default constructor with super exception as a source.
	 * 
	 * @param e
	 *          super exception
	 */
	public ConverterException(Exception e) {
		super(e);
	}

	/**
	 * Default constructor - initializes instance variable to unknown.
	 */

	public ConverterException() {
		super(); // call superclass constructor
	}

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          exception message
	 * @param e
	 *          super exception
	 */
	public ConverterException(String message, Exception e) {
		super(message, e);
	}
}