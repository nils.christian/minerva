package lcsb.mapviewer.converter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;

/**
 * Interface used for reading data from file and putting it into {@link Model}
 * object.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IConverter {

	/**
	 * Parse input source and transformes it into a model object.
	 * 
	 * @param params
	 *          input params used for reading data
	 * @return model obtained from inut source
	 * @throws InvalidInputDataExecption
	 *           thrown when input parameters are invalid
	 */

	Model createModel(ConverterParams params) throws InvalidInputDataExecption;

	/**
	 * Export model to {@link InputStream}.
	 * 
	 * @param model
	 *          model to be exported
	 * @return {@link InputStream} with exported data
	 * @throws InconsistentModelException
	 *           thrown when given model is inconsistent and unable to be exported
	 * @throws ConverterException
	 *           thrown when there is unknown problem in a converter
	 */
	InputStream exportModelToInputStream(Model model) throws ConverterException, InconsistentModelException;

	/**
	 * Export model to {@link File}.
	 * 
	 * @param model
	 *          model to be exported
	 * @param filePath
	 *          exported file path
	 * @return {@link File} with exported data
	 * @throws InconsistentModelException
	 *           thrown when given model is inconsistent and unable to be exported
	 * @throws ConverterException
	 *           thrown when there is unknown problem in a converter
	 * @throws IOException thrown when there is a problem with writing to file
	 */
	File exportModelToFile(Model model, String filePath) throws ConverterException, InconsistentModelException, IOException;

	/**
	 * Returns a common name of data format used in the converter.
	 * 
	 * @return common name of data format used in the converter
	 */
	String getCommonName();

	/**
	 * Returns {@link MimeType} of the exported data.
	 * 
	 * @return {@link MimeType} of the exported data
	 */
	MimeType getMimeType();

	/**
	 * Returns extension of the exported file.
	 * 
	 * @return extension of the exported file
	 */
	String getFileExtension();

}
