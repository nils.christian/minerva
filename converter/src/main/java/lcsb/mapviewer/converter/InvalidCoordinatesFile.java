package lcsb.mapviewer.converter;


/**
 * Exception thrown when the {@link OverviewParser#COORDINATES_FILENAME image
 * link coordinates file} in {@link lcsb.mapviewer.converter.zip.InputFileType#OVERLAY_IMAGE_DATA input file}
 * is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidCoordinatesFile extends InvalidOverviewFile {

	/**
	 * Default constructor.
	 * 
	 * @param e
	 *          parent exception (reason)
	 */
	public InvalidCoordinatesFile(Exception e) {
		super(e);
	}

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          exception message
	 * @param e
	 *          parent exception (reason)
	 */
	public InvalidCoordinatesFile(String message, Exception e) {
		super(message, e);
	}

	/**
	 * Default constructor.
	 * 
	 * @param message
	 *          exception message
	 */
	public InvalidCoordinatesFile(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

}
