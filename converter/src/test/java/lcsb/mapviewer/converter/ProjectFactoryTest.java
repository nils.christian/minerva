package lcsb.mapviewer.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.ComplexZipConverterTest.MockConverter;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.model.ModelData;

public class ProjectFactoryTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testOverviewImageLink() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ProjectFactory projectFactory = new ProjectFactory(converter);
      ZipFile zipFile = new ZipFile("testFiles/complex_model_with_img.zip");
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(zipFile);

      ZipEntryFileFactory factory = new ZipEntryFileFactory();
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          params.entry(factory.createZipEntryFile(entry, zipFile));
        }
      }

      Project project = projectFactory.create(params);
      assertNotNull(project);
      ModelData model = project.getModels().iterator().next();
      assertEquals("main", model.getName());

      List<OverviewImage> result = project.getOverviewImages();

      assertNotNull(result);
      assertEquals(1, result.size());

      OverviewImage img = result.get(0);

      assertEquals("test.png", img.getFilename());
      assertEquals((Integer) 639, img.getHeight());
      assertEquals((Integer) 963, img.getWidth());
      assertEquals(2, img.getLinks().size());

      OverviewLink link = img.getLinks().get(0);
      List<Point2D> polygon = link.getPolygonCoordinates();
      assertEquals(4, polygon.size());

      assertTrue(link instanceof OverviewModelLink);

      OverviewModelLink mLink = (OverviewModelLink) link;
      assertEquals((Integer) 10, mLink.getxCoord());
      assertEquals((Integer) 10, mLink.getyCoord());
      assertEquals((Integer) 3, mLink.getZoomLevel());
      assertEquals(model, mLink.getLinkedModel());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testOverviewImageLinkToSubmapPath() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ProjectFactory projectFactory = new ProjectFactory(converter);
      ZipFile zipFile = new ZipFile("testFiles/complex_model_with_images_path.zip");
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(zipFile);

      ZipEntryFileFactory factory = new ZipEntryFileFactory();
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          params.entry(factory.createZipEntryFile(entry, zipFile));
        }
      }

      Project project = projectFactory.create(params);
      assertNotNull(project);
      ModelData model = project.getModels().iterator().next();
      assertEquals("main", model.getName());

      List<OverviewImage> result = project.getOverviewImages();

      assertNotNull(result);
      assertEquals(2, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

}
