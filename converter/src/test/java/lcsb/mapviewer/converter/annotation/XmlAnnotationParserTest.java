package lcsb.mapviewer.converter.annotation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

public class XmlAnnotationParserTest extends ConverterTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseRdf() throws Exception {
    try {
      XmlAnnotationParser xap = new XmlAnnotationParser();

      String xml = readFile("testFiles/annotation/rdf.xml");

      Set<MiriamData> set = xap.parse(xml);
      assertEquals(2, set.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testMiriamDataToXmlAndBack() throws Exception {
    try {
      MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "e:f");
      XmlAnnotationParser parser = new XmlAnnotationParser();
      String xml = parser.miriamDataToXmlString(md);
      assertNotNull(xml);
      Set<MiriamData> set = parser.parseMiriamNode(super.getNodeFromXmlString(xml));
      assertNotNull(set);
      assertEquals(1, set.size());
      MiriamData md1 = set.iterator().next();
      assertNotNull(md1);
      assertEquals(md.getDataType(), md1.getDataType());
      assertEquals(md.getRelationType(), md1.getRelationType());
      assertEquals(md.getResource(), md1.getResource());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidRdf() throws Exception {
    try {
      XmlAnnotationParser xap = new XmlAnnotationParser();

      xap.parseRdfNode((Node) null);
      fail("Exception expected");

    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("rdf:Rdf node not found"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidRdf2() throws Exception {
    try {
      XmlAnnotationParser xap = new XmlAnnotationParser();

      String xml = readFile("testFiles/annotation/invalid_rdf.xml");

      xap.parse(xml);

      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue(e.getMessage().contains("rdf:Description node not found"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidRdf3() throws Exception {
    try {
      XmlAnnotationParser xap = new XmlAnnotationParser();

      String xml = readFile("testFiles/annotation/invalid_rdf2.xml");

      xap.parse(xml);

      fail("Exception expected");
    } catch (InvalidXmlSchemaException e) {
      assertTrue("Invalid message: " + e.getMessage(),
          e.getMessage().contains("rdf:li does not have a rdf:resource attribute"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testParseInvalidRdf4() throws Exception {
    EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
    Logger.getRootLogger().addAppender(appender);
    try {
      XmlAnnotationParser xap = new XmlAnnotationParser();

      String xml = readFile("testFiles/annotation/invalid_rdf3.xml");

      xap.parse(xml);

      assertEquals(1, appender.getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      Logger.getRootLogger().removeAppender(appender);
    }
  }

  @Test
  public void testParseInvalidRdf5() throws Exception {
    EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
    Logger.getRootLogger().addAppender(appender);
    try {
      XmlAnnotationParser xap = new XmlAnnotationParser();

      String xml = readFile("testFiles/annotation/invalid_rdf4.xml");

      xap.parse(xml);

      assertEquals(1, appender.getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      Logger.getRootLogger().removeAppender(appender);
    }
  }

}
