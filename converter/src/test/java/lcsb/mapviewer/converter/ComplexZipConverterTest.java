package lcsb.mapviewer.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Species;

public class ComplexZipConverterTest {

  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(ComplexZipConverterTest.class);

  private abstract class AbstractConverter implements IConverter {

  }

  private interface InterfaceConverter extends IConverter {

  }

  public static class MockConverter implements IConverter {

    @Override
    public Model createModel(ConverterParams params) {
      Model result = new ModelFullIndexed(null);

      Species sa1 = new GenericProtein("sa1");
      result.addElement(sa1);

      Species sa2 = new GenericProtein("sa2");
      result.addElement(sa2);

      Species sa3 = new GenericProtein("sa3");
      result.addElement(sa3);

      Species sa4 = new Phenotype("sa4");
      result.addElement(sa4);

      Complex ca1 = new Complex("ca1");
      ca1.setName("main");
      result.addElement(ca1);
      Species sa5 = new GenericProtein("sa5");
      sa5.setName("sa1");
      result.addElement(sa5);
      ca1.addSpecies(sa5);
      Species sa6 = new Phenotype("sa6");
      sa6.setName("sa4");
      result.addElement(sa6);
      ca1.addSpecies(sa6);

      Complex ca2 = new Complex("ca2");
      ca2.setName("s1");
      result.addElement(ca2);
      Species sa7 = new GenericProtein("sa7");
      sa7.setName("sa1");
      result.addElement(sa7);
      ca2.addSpecies(sa7);

      Complex ca3 = new Complex("cs3");
      ca3.setName("s2");
      result.addElement(ca3);

      Complex ca4 = new Complex("cs4");
      ca4.setName("s3");
      result.addElement(ca4);

      Reaction r1 = new TransportReaction("re1");
      r1.addReactant(new Reactant(sa5));
      r1.addProduct(new Product(sa7));
      result.addReaction(r1);

      Reaction r2 = new TransportReaction("re2");
      r2.addReactant(new Reactant(sa6));
      r2.addProduct(new Product(ca3));
      result.addReaction(r2);

      Reaction r3 = new TransportReaction("re3");
      r3.addReactant(new Reactant(sa7));
      r3.addProduct(new Product(ca4));
      result.addReaction(r3);
      return result;
    }

    @Override
    public InputStream exportModelToInputStream(Model model) throws InconsistentModelException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public File exportModelToFile(Model model, String filePath) throws InconsistentModelException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public String getCommonName() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public MimeType getMimeType() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public String getFileExtension() {
      // TODO Auto-generated method stub
      return null;
    }

  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() throws Exception {
    try {
      new ComplexZipConverter(AbstractConverter.class);
      fail("Exception expected");

    } catch (InvalidClassException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testConstructor2() throws Exception {
    try {
      new ComplexZipConverter(InterfaceConverter.class);
      fail("Exception expected");

    } catch (InvalidClassException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testConstructor3() throws Exception {
    try {
      new ComplexZipConverter(MockConverter.class);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testParamsOk() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      Model model = converter.createModel(params);
      assertNotNull(model);
      assertEquals("main", model.getName());
      assertEquals(3, model.getSubmodelConnections().size());

      Model s1Model = null;
      Model s2Model = null;
      Model s3Model = null;

      for (ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
        if (submodel.getName().equals("s1")) {
          s1Model = submodel.getSubmodel().getModel();
        }
        if (submodel.getName().equals("s2")) {
          s2Model = submodel.getSubmodel().getModel();
        }
        if (submodel.getName().equals("s3")) {
          s3Model = submodel.getSubmodel().getModel();
        }
      }
      assertNotNull(s1Model);
      assertNotNull(s2Model);
      assertNotNull(s3Model);

      Element al1 = model.getElementByElementId("sa1");
      assertNotNull(al1.getSubmodel());
      assertEquals(SubmodelType.DOWNSTREAM_TARGETS, al1.getSubmodel().getType());
      assertEquals(s1Model, al1.getSubmodel().getSubmodel().getModel());

      Element al2 = model.getElementByElementId("sa2");
      assertNull(al2.getSubmodel());

      Element al4 = model.getElementByElementId("sa4");
      assertNotNull(al4.getSubmodel());
      assertEquals(SubmodelType.PATHWAY, al4.getSubmodel().getType());
      assertEquals(s2Model, al4.getSubmodel().getSubmodel().getModel());

      Element s1_al1 = s1Model.getElementByElementId("sa1");
      assertNotNull(s1_al1.getSubmodel());
      assertEquals(SubmodelType.DOWNSTREAM_TARGETS, s1_al1.getSubmodel().getType());
      assertEquals(s3Model, s1_al1.getSubmodel().getSubmodel().getModel());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testChangeModelZipEntry() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s12", false, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      Model model = converter.createModel(params);
      assertNotNull(model);
      assertEquals("main", model.getName());
      assertEquals(3, model.getSubmodelConnections().size());

      Model s1Model = null;
      Model s2Model = null;
      Model s3Model = null;

      for (ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
        if (submodel.getName().equals("s12")) {
          s1Model = submodel.getSubmodel().getModel();
        }
        if (submodel.getName().equals("s2")) {
          s2Model = submodel.getSubmodel().getModel();
        }
        if (submodel.getName().equals("s3")) {
          s3Model = submodel.getSubmodel().getModel();
        }
      }
      assertNotNull(s1Model);
      assertNotNull(s2Model);
      assertNotNull(s3Model);

      Element al1 = model.getElementByElementId("sa1");
      assertNotNull(al1.getSubmodel());
      assertEquals(SubmodelType.DOWNSTREAM_TARGETS, al1.getSubmodel().getType());
      assertEquals(s1Model, al1.getSubmodel().getSubmodel().getModel());

      Element al2 = model.getElementByElementId("sa2");
      assertNull(al2.getSubmodel());

      Element al4 = model.getElementByElementId("sa4");
      assertNotNull(al4.getSubmodel());
      assertEquals(SubmodelType.PATHWAY, al4.getSubmodel().getType());
      assertEquals(s2Model, al4.getSubmodel().getSubmodel().getModel());

      Element s1_al1 = s1Model.getElementByElementId("sa1");
      assertNotNull(s1_al1.getSubmodel());
      assertEquals(SubmodelType.DOWNSTREAM_TARGETS, s1_al1.getSubmodel().getType());
      assertEquals(s3Model, s1_al1.getSubmodel().getSubmodel().getModel());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testParamsMissing() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      try {
        converter.createModel(params);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testParamsInvalid1() throws Exception {
    try {
      // two mains
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", true, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      try {
        converter.createModel(params);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testParamsInvalid2() throws Exception {
    try {
      // zero mains
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", false, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      try {
        converter.createModel(params);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testParamsInvalid3() throws Exception {
    try {
      // two mappings
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", false, true, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      try {
        converter.createModel(params);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testParamsMissingMapping() throws Exception {
    try {
      // zero mappings (should be ok)
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", "", false, false, null));
      Model model = converter.createModel(params);

      assertNotNull(model);

      assertEquals(4, model.getSubmodelConnections().size());

      for (ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
        if (submodel.getName().equals("s1")) {
          assertEquals(SubmodelType.DOWNSTREAM_TARGETS, submodel.getType());
        } else if (submodel.getName().equals("s2")) {
          assertEquals(SubmodelType.PATHWAY, submodel.getType());
        } else if (submodel.getName().equals("s3")) {
          assertEquals(SubmodelType.UNKNOWN, submodel.getType());
        } else if (submodel.getName().equals("mapping")) {
          assertEquals(SubmodelType.UNKNOWN, submodel.getType());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testTooManyParams() throws Exception {
    try {
      // zero mappings (should be ok)
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/complex_model.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
      params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
      params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
      params.entry(new ModelZipEntryFile("mapping.xml", "", false, false, null));
      params.entry(new ModelZipEntryFile("blabla.xml", "s3", false, false, SubmodelType.UNKNOWN));
      try {
        converter.createModel(params);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidMappingFile() throws Exception {
    try {
      // zero mappings (should be ok)
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      ComplexZipConverterParams params = new ComplexZipConverterParams();
      params.zipFile(new ZipFile("testFiles/invalid_mapping.zip"));
      params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
      params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
      try {
        converter.createModel(params);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {

      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsIgnoredFileForMac() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      assertTrue(converter.isIgnoredFile("__MACOSX/.desc"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsIgnoredFileForOldMacEntries() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      assertTrue(converter.isIgnoredFile(".DS_Store/.desc"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIsIgnoredFileForValidFiles() throws Exception {
    try {
      ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
      assertFalse(converter.isIgnoredFile("mapping.xml"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testLayoutZipEntryFileToLayoutOrder() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    ZipFile file = Mockito.mock(ZipFile.class);
    params.zipFile(file);
    LayoutZipEntryFile entry1 = new LayoutZipEntryFile("overlay1.txt", "name", "desc1");
    LayoutZipEntryFile entry2 = new LayoutZipEntryFile("overlay2.txt", "name", "desc1");
    params.entry(entry1);
    params.entry(entry2);
    ZipEntry entry = Mockito.mock(ZipEntry.class);
    Mockito.when(file.getInputStream(entry)).thenReturn(new ByteArrayInputStream("".getBytes()));
    
    Layout overlay1 = converter.layoutZipEntryFileToLayout(params, file, entry, entry1, 1);
    Layout overlay2 = converter.layoutZipEntryFileToLayout(params, file, entry, entry2, 2);
    assertTrue(overlay1.getOrderIndex() > 0);
    assertTrue(overlay2.getOrderIndex() > overlay1.getOrderIndex());
  }

}
