package lcsb.mapviewer.wikipathway.model.biopax;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BiopaxDataTest.class, BiopaxOpenControlledVocabularyTest.class, BiopaxPublicationTest.class })
public class AllBiopaxTests {

}
