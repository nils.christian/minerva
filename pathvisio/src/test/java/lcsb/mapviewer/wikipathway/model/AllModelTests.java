package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.wikipathway.model.biopax.AllBiopaxTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AllBiopaxTests.class,//
		DataNodeTest.class,//
		EdgeTest.class,//
		GraphTest.class,//
		GroupTest.class,//
		InteractionTest.class,//
		LabelTest.class,//
		PointDataTest.class,//
		ShapeTest.class,//
		StateTest.class })
public class AllModelTests {

}
