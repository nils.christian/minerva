package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

public class ReactionGpmlToModelTest extends WikipathwaysTestFunctions{
	/**
	 * Default class logger.
	 */
	static Logger						logger	= Logger.getLogger(ReactionGpmlToModelTest.class);

	private ModelComparator	mc			= new ModelComparator(1.0);

	@Test
	public void LineInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_line.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ArrowInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_arrow.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof StateTransitionReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void DashedArrowInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_arrow.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void DashedLineInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_line.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BindingInteractionTest() throws Exception {
		try {
			// invalid mim_binding (contains only one reactant)
			String fileName = "testFiles/reactions/interaction_mim_binding.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BindingInteractionTest2() throws Exception {
		try {
			// proper mim_binding (contains two reactants)
			String fileName = "testFiles/reactions/interaction_mim_binding2.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(getWarnings().toString(), 0, getWarnings().size());
			assertTrue(reaction instanceof HeterodimerAssociationReaction);
			assertFalse(reaction.isReversible());
			assertEquals(2, reaction.getReactants().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingLeftInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_branching_left.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingRightInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_branching_right.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CatalystInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_catalyst.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof PositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CleavageInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_cleavage.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof StateTransitionReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ConversionInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_conversion.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof StateTransitionReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CovalentBondInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_covalent_bond.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void GapInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_gap.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void InhibitionInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_inhibition.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof NegativeInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ModificationInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_modification.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof PositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void NecessaryStimulationInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_necessary_stimulation.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof ReducedPhysicalStimulationReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void StimulationInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_stimulation.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof ReducedPhysicalStimulationReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TranscriptionTranslationInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_transcription_translation.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof PositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TbarTranslationInteractionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_tbar.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof NegativeInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void LineAsInteractionTest2() throws Exception {
		try {
			// proper mim_binding (contains two reactants)
			String fileName = "testFiles/reactions/line_as_interaction.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BindingInteractionDashedTest() throws Exception {
		try {
			// invalid mim_binding (contains only one reactant)
			String fileName = "testFiles/reactions/interaction_dashed_mim_binding.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingLeftInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_branching_left.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingRightInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_branching_right.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CatalystInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_catalyst.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CleavageInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_cleavage.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ConversionInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_conversion.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CovalentBondInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_covalent_bond.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void GapInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_gap.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownTransitionReaction);
			assertTrue(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void InhibitionInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_inhibition.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownNegativeInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ModificationInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_modification.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(0, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void NecessaryStimulationInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_necessary_stimulation.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void StimulationInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_stimulation.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownPositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TranscriptionTranslationInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_transcription_translation.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof PositiveInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TbarTranslationInteractionDashedTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_tbar.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, getWarnings().size());
			assertTrue(reaction instanceof UnknownNegativeInfluenceReaction);
			assertFalse(reaction.isReversible());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void lineLineCombinedReactionsTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/line_line_combined_reaction.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(4, model1.getReactions().size());
			
			assertEquals(4, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
