package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ReactionGpmlInputToModelTest extends WikipathwaysTestFunctions {
	/**
	 * Default class logger.
	 */
	static Logger						logger	= Logger.getLogger(ReactionGpmlInputToModelTest.class);

	private ModelComparator	mc			= new ModelComparator(1.0);

	@Test
	public void LineInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_line_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void ArrowInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_arrow_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void DashedLineInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_line_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void DashedArrowInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_arrow_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void TBarInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_tbar_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Inhibition);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void NecessaryStimulationInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_necessary_stimulation_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof PhysicalStimulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void BindingInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_binding_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void ConversionInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_conversion_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void StimulationInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_stimulation_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof PhysicalStimulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void ModificationInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_modification_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void CatalystInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_catalyst_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Catalysis);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void InhibitionInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_inhibition_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Inhibition);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void CleavageInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_cleavage_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void CovalentBondInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_covalent_bond_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void BranchingLeftInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_branching_left_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void BranchingRightInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_branching_right_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void TranscriptionTranslationInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_transcription_translation_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void GapInteractionInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_gap_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void TBarInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_tbar_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof UnknownInhibition);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void NecessaryStimulationInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_necessary_stimulation_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void BindingInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_binding_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void ConversionInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_conversion_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void StimulationInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_stimulation_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void ModificationInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_modification_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void CatalystInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_catalyst_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof UnknownCatalysis);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void InhibitionInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_inhibition_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof UnknownInhibition);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void CleavageInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_cleavage_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void CovalentBondInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_covalent_bond_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void BranchingLeftInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_branching_left_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void BranchingRightInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_branching_right_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void TranscriptionTranslationInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_transcription_translation_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void GapInteractionDashedInputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_gap_input.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNull(product);
			assertNotNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
