package lcsb.mapviewer.wikipathway;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public abstract class WikipathwaysTestFunctions {
	private Logger					logger	= Logger.getLogger(WikipathwaysTestFunctions.class);

	protected static double	EPSILON	= Configuration.EPSILON;
	
	private DocumentBuilder	db;


	private EventStorageLoggerAppender appender;

	@Before
	public final void _setUp() throws Exception {
		Logger.getRootLogger().removeAppender(appender);
		appender = new EventStorageLoggerAppender();
		Logger.getRootLogger().addAppender(appender);
		db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	}

	@After
	public final void _tearDown() throws Exception {
		Logger.getRootLogger().removeAppender(appender);
	}

	protected List<LoggingEvent> getWarnings() {
		return appender.getWarnings();
	}

	protected String readFile(String file) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		try {
			String line = null;
			String ls = System.getProperty("line.separator");

			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
		} finally {
			reader.close();
		}

		return stringBuilder.toString();
	}

	protected Node getNodeFromXmlString(String text) throws InvalidXmlSchemaException {
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(text));
		return getXmlDocumentFromInputSource(is).getChildNodes().item(0);
	}

	protected Document getXmlDocumentFromFile(String fileName) throws InvalidXmlSchemaException, IOException {
		File file = new File(fileName);
		InputStream inputStream = new FileInputStream(file);
		Reader reader = null;
		try {
			reader = new InputStreamReader(inputStream, "UTF-8");
			InputSource is = new InputSource(reader);

			Document result = getXmlDocumentFromInputSource(is);
			inputStream.close();
			return result;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	protected Document getXmlDocumentFromInputSource(InputSource stream) throws InvalidXmlSchemaException {
		DocumentBuilder db;
		try {
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new InvalidXmlSchemaException("Problem with xml parser");
		}
		Document doc = null;
		try {
			doc = db.parse(stream);
		} catch (SAXException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		return doc;
	}

	protected String createTmpFileName() {
		try {
			File f = File.createTempFile("prefix", ".txt");
			String filename = f.getName();
			f.delete();
			return filename;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected String nodeToString(Node node) {
		return nodeToString(node, false);
	}

	protected String nodeToString(Node node, boolean includeHeadNode) {
		if (node == null)
			return null;
		StringWriter sw = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty(OutputKeys.METHOD, "xml");

			NodeList list = node.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node element = list.item(i);
				t.transform(new DOMSource(element), new StreamResult(sw));
			}
		} catch (TransformerException te) {
			logger.debug("nodeToString Transformer Exception");
		}
		if (includeHeadNode) {
			return "<" + node.getNodeName() + ">" + sw.toString() + "</" + node.getNodeName() + ">";
		}
		return sw.toString();
	}

	protected boolean equalFiles(String fileA, String fileB) throws IOException {
		int BLOCK_SIZE = 65536;
		FileInputStream inputStreamA = new FileInputStream(fileA);
		FileInputStream inputStreamB = new FileInputStream(fileB);
		// vary BLOCK_SIZE to suit yourself.
		// it should probably a factor or multiple of the size of a disk
		// sector/cluster.
		// Note that your max heap size may need to be adjused
		// if you have a very big block size or lots of these comparators.

		// assume inputStreamA and inputStreamB are streams from your two files.
		byte[] streamABlock = new byte[BLOCK_SIZE];
		byte[] streamBBlock = new byte[BLOCK_SIZE];
		boolean match = true;
		int bytesReadA = 0;
		int bytesReadB = 0;
		do {
			bytesReadA = inputStreamA.read(streamABlock);
			bytesReadB = inputStreamB.read(streamBBlock);
			match = ((bytesReadA == bytesReadB) && Arrays.equals(streamABlock, streamBBlock));
		} while (match && (bytesReadA > -1));
		inputStreamA.close();
		inputStreamB.close();
		return match;
	}

	public File createTempDirectory() throws IOException {
		final File temp;

		temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

		if (!(temp.delete())) {
			throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
		}

		if (!(temp.mkdir())) {
			throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
		}

		return (temp);
	}

	protected String getWebpage(String accessUrl) throws IOException {
		String inputLine;
		StringBuilder tmp = new StringBuilder();
		URL url = new URL(accessUrl);
		URLConnection urlConn = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			tmp.append(inputLine);
		}
		in.close();
		return tmp.toString();
	}
	
	protected Element fileToNode(String filename) throws SAXException, IOException {
		InputStream is = new FileInputStream(new File(filename));
		Document doc = null;
		doc = db.parse(is);
		return doc.getDocumentElement();
	}
	
}
