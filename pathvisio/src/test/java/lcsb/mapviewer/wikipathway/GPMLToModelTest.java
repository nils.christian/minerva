package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

public class GPMLToModelTest extends WikipathwaysTestFunctions {
  /**
   * Default class logger.
   */
  static Logger logger = Logger.getLogger(GPMLToModelTest.class);

  private ModelComparator mc = new ModelComparator(1.0);

  @Test
  public void DopamineTest() throws Exception {
    try {
      String pathToFile = "testFiles/wikipathways/";
      String fileName = "Dopamine.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(22, getWarnings().size());
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP1403_75220Test() throws Exception {
    try {
      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP1403_75220.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(22, getWarnings().size());
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP528_76269Test() throws Exception {
    try {

      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP528_76269.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(15, getWarnings().size());
      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP550_73391Test() throws Exception {
    try {
      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP550_73391.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(16, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP197_69902Test() throws Exception {
    try {
      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP197_69902.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(3, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP179_70629Test() throws Exception {
    try {
      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP179_70629.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(38, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP306_71714Test() throws Exception {
    try {

      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP306_71714.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(41, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void WP481_72080Test() throws Exception {
    try {

      String pathToFile = "testFiles/wikipathways/";
      String fileName = "WP481_72080.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(22, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testTwoReactants() throws Exception {
    try {

      String pathToFile = "testFiles/small/";
      String fileName = "two_reactant.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile + fileName);

      assertEquals(0, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);

      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testTwoPubmeds() throws Exception {
    try {

      String pathToFile = "testFiles/small/reaction_with_two_pubmeds.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile);
      assertEquals(2, model1.getReactions().iterator().next().getMiriamData().size());

      assertEquals(0, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);

      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComment() throws Exception {
    try {

      String pathToFile = "testFiles/small/comment.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile);
      assertTrue(model1.getNotes().contains("Metabolic Process"));

      assertEquals(0, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);

      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGstp() throws Exception {
    try {

      String pathToFile = "testFiles/wikipathways/gstp.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile);

      assertEquals(3, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);

      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testMissingAliasesInCompartment() throws Exception {
    try {

      String pathToFile = "testFiles/small/missing_aliases_in_compartment.gpml";

      Model model1 = new GPMLToModel().getModel(pathToFile);

      assertEquals(3, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);

      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelLines() throws Exception {
    try {

      String fileName = "testFiles/small/model_with_line.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      assertEquals(0, getWarnings().size());

      assertEquals(1, model1.getLayers().iterator().next().getLines().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelShapes() throws Exception {
    try {
      String fileName = "testFiles/small/shapes.gpml";
      new GPMLToModel().getModel(fileName);

      assertEquals(11, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testHypotheticlaComplex() throws Exception {
    try {
      String fileName = "testFiles/complex/hypothetical_complex.gpml";
      Model model = new GPMLToModel().getModel(fileName);
      for (Complex species : model.getComplexList()) {
        assertTrue("Complex parsed from gpml should be hypothetical", species.isHypothetical());
      }

      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testNonHypotheticlaComplex() throws Exception {
    try {
      String fileName = "testFiles/complex/nonhypothetical_complex.gpml";
      Model model = new GPMLToModel().getModel(fileName);
      for (Complex species : model.getComplexList()) {
        assertFalse("Complex parsed from gpml should be hypothetical", species.isHypothetical());
      }
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComplexName() throws Exception {
    try {
      String fileName = "testFiles/complex/complex_with_name.gpml";
      Model model = new GPMLToModel().getModel(fileName);
      boolean nameFound = false;
      for (Complex species : model.getComplexList()) {
        if ("p70 S6 Kinases".equals(species.getName())) {
          nameFound = true;
        }
      }
      assertTrue("Complex parsed from gpml should have a valid name", nameFound);
      assertEquals(1, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCompartmentName() throws Exception {
    try {
      String fileName = "testFiles/compartment/compartment_name.gpml";
      Model model = new GPMLToModel().getModel(fileName);
      for (Compartment compartment : model.getCompartments()) {
        assertEquals("Compartment parsed from gpml should have a valid name", "Label", compartment.getName());
      }
      assertEquals(0, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 30000)
  @Ignore
  public void test() throws Exception {
    try {
      String fileName = "D:/WP3408_81714.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().model(model1).width(model1.getWidth())
          .height(model1.getHeight());
      PngImageGenerator pig = new PngImageGenerator(params);
      pig.saveToFile("D:/tes1.png");

      Desktop.getDesktop().open(new File("D:/tes1.png"));

      params = new AbstractImageGenerator.Params().model(model2).width(model2.getWidth()).height(model2.getHeight());
      pig = new PngImageGenerator(params);
      pig.saveToFile("D:/tes2.png");

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComplexInCompartment() throws Exception {
    try {
      String fileName = "testFiles/compartment/complex_in_compartment.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));

      assertEquals(2, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReactionToComplex() throws Exception {
    try {
      String fileName = "testFiles/small/reaction_to_complex.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));

      assertEquals(54, getWarnings().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testColorfullComplexReaction() throws Exception {
    try {
      String fileName = "testFiles/reactions/complex_color_reaction.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testColorfullComplexReaction2() throws Exception {
    try {
      String fileName = "testFiles/reactions/complex_color_reaction2.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProteinWithModification() throws Exception {
    try {
      String fileName = "testFiles/small/protein_with_modification.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);
      assertEquals(0, getWarnings().size());

      Gene protein = (Gene) model1.getElementByElementId("be3de");
      assertNotNull(protein);
      assertEquals(2, protein.getModificationResidues().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProteinWithModification2() throws Exception {
    try {
      String fileName = "testFiles/small/protein_with_modification_2.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);
      assertEquals(0, getWarnings().size());

      Gene protein = (Gene) model1.getElementByElementId("be3de");
      assertNotNull(protein);
      assertEquals(2, protein.getModificationResidues().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModifierPosition() throws Exception {
    try {
      String fileName = "testFiles/small/modifier_misaligned.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      Reaction reaction = model1.getReactions().iterator().next();

      double distance = reaction.getCenterPoint().distance(reaction.getModifiers().get(0).getLine().getEndPoint());
      assertTrue("Modifier is too far from center point: " + distance,
          distance < ReactionCellDesignerConverter.RECT_SIZE);

      assertEquals(1, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReactionToLabel() throws Exception {
    try {
      String fileName = "testFiles/small/reaction_to_label.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      assertEquals(1, getWarnings().size());

      assertEquals(1, model1.getReactions().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModifierAsLabel() throws Exception {
    try {
      String fileName = "testFiles/small/modifier_to_label.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      assertEquals(1, model1.getReactions().size());
      assertEquals(3, model1.getReactions().iterator().next().getNodes().size());

      assertEquals(1, getWarnings().size());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModifierAdsLabel() throws Exception {
    try {
      String fileName = "testFiles/small/protein_with_state.gpml";
      Model model1 = new GPMLToModel().getModel(fileName);

      Protein protein = (Protein) model1.getElementsByName("Protein").get(0);
      assertEquals(0, protein.getModificationResidues().size());
      assertEquals("GTP", protein.getStructuralState());

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      String xml = parser.toXml(model1);
      InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

      Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

      assertEquals(0, mc.compare(model1, model2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
