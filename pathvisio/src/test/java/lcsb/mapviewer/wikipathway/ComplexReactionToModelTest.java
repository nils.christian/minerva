package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ComplexReactionToModelTest extends WikipathwaysTestFunctions{
	/**
	 * Default class logger.
	 */
	static Logger	logger	= Logger.getLogger(ComplexReactionToModelTest.class);

	@Test
	public void ComplexReactionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/two_segment_reaction.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, reaction.getReactants().size());
			assertEquals(1, reaction.getProducts().size());

			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ComplexReactantReactionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/two_reactants_reaction.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(2, reaction.getReactants().size());
			assertEquals(1, reaction.getProducts().size());

			assertEquals(1, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ComplexSplitReactionTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/split_two_segment_reaction.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			assertEquals(1, reaction.getReactants().size());
			assertEquals(2, reaction.getProducts().size());

			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void problematicReactantTest() throws Exception {
		try {
			ModelComparator mc = new ModelComparator(1.0);

			String fileName = "testFiles/small/problematic_reactant.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
			assertEquals("File " + fileName + " different after transformation", 0, mc.compare(model1, model2));

			assertEquals(7, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void problematicReactant2Test() throws Exception {
		try {
			ModelComparator mc = new ModelComparator(1.0);

			String fileName = "testFiles/small/problematic_reactant_2.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
			assertEquals("File " + fileName + " different after transformation", 0, mc.compare(model1, model2));

			assertEquals(3, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void missingNodesInReactionTest() throws Exception {
		try {
			ModelComparator mc = new ModelComparator(1.0);

			String fileName = "testFiles/small/missing_nodes_in_reaction.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);

			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals("File " + fileName + " different after transformation", 0, mc.compare(model1, model2));

			assertEquals(5, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testColorReaction() throws Exception {
		try {
			String fileName = "testFiles/small/color_reaction.gpml";
			Model model = new GPMLToModel().getModel(fileName);

			assertEquals(1, model.getReactions().size());

			Reaction reaction = model.getReactions().iterator().next();

			assertFalse(Color.BLACK.equals(reaction.getReactants().get(0).getLine().getColor()));

			Element redAlias = model.getElementByElementId("d9620");
			Element blackAlias = model.getElementByElementId("d046f");
			assertFalse(Color.BLACK.equals(redAlias.getColor()));
			assertTrue(Color.WHITE.equals(blackAlias.getColor()));

			assertEquals(1, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void testReactionWithTwoSegments() throws Exception {
		try {
			String fileName = "testFiles/small/color_reaction.gpml";
			Model model = new GPMLToModel().getModel(fileName);

			Reaction reaction = model.getReactions().iterator().next();

			int lineCount = 0;
			for (AbstractNode node : reaction.getNodes()) {
				lineCount += node.getLine().getLines().size();
			}

			assertEquals(4, lineCount);

			assertEquals(1, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
