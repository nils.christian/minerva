package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxData;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxPublication;

public class BiopaxParserTest extends WikipathwaysTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParse() throws Exception {
		try {
			BiopaxParser parser = new BiopaxParser();
			Document document = getXmlDocumentFromFile("testFiles/biopax/small.xml");
			BiopaxData data = parser.parse(document.getFirstChild());
			assertNotNull(data);
			assertEquals(2, data.getPublications().size());
			BiopaxPublication publication = data.getPublicationByReference("cf2");
			assertEquals("23456", publication.getId());
			assertEquals("PubMed", publication.getDb());
			assertEquals("[Radical resection of foci in tuberculosis of the shoulder joint].", publication.getTitle());
			assertEquals("Magy Traumatol Orthop Helyreallito Seb", publication.getSource());
			assertEquals("1977", publication.getYear());
			assertEquals("Udvarhelyi I", publication.getAuthors());
			assertEquals("cf2", publication.getReferenceId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testIncorrectPumeds() throws Exception {
		try {
			BiopaxParser parser = new BiopaxParser();
			Document document = getXmlDocumentFromFile("testFiles/biopax/invalid_pubmed_reference.xml");
			BiopaxData data = parser.parse(document.getFirstChild());
			assertNotNull(data);
			assertTrue(getWarnings().size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
