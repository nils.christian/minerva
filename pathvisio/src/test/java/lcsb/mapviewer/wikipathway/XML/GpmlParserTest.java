package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.Graph;
import lcsb.mapviewer.wikipathway.model.Interaction;

public class GpmlParserTest extends WikipathwaysTestFunctions {
	Logger logger = Logger.getLogger(GpmlParserTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEdgeAttributes() throws Exception {
		try {

			String fileName = "testFiles/small/missing_aliases_in_compartment.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);
			assertNotNull(graph);
			assertEquals(3, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCycliReactions() throws Exception {
		try {

			String fileName = "testFiles/small/cyclic_reactions.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);
			assertNotNull(graph);
			assertEquals(3, getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testBiopaxVocabulary() throws Exception {
		try {

			String fileName = "testFiles/small/opencontrolledvocabulary.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);

			assertEquals(1, graph.getBiopaxData().getOpenControlledVocabularies().size());
			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModelAttribute() throws Exception {
		try {

			String fileName = "testFiles/small/model_attribute.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);

			assertEquals("428359", graph.getAttributes().get("reactome_id"));
			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModelAnnotations() throws Exception {
		try {

			String fileName = "testFiles/small/model_annotations.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);

			assertEquals(6, graph.getBiopaxReferences().size());
			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModelLines() throws Exception {
		try {

			String fileName = "testFiles/small/model_with_line.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);

			assertEquals(1, graph.getLines().size());
			PolylineData pd = graph.getLines().get(0);
			assertEquals(3, pd.getPoints().size());
			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimpleReaction() throws Exception {
		try {

			String fileName = "testFiles/small/simple_reaction.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			Graph graph = new GpmlParser().createGraph(fis);

			Interaction interaction = graph.getInteractions().iterator().next();
			PolylineData pd = interaction.getLine();
			assertTrue(pd.getPoints().get(0).distance(476.0, 351.0) < EPSILON);
			assertTrue(pd.getPoints().get(1).distance(476.0, 286.0) < EPSILON);
			assertTrue(pd.getPoints().get(2).distance(846.0, 287.0) < EPSILON);
			assertTrue(pd.getPoints().get(3).distance(846.0, 354.0) < EPSILON);

			assertEquals(0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
