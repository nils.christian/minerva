package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class BugTest extends WikipathwaysTestFunctions {
	Logger logger = Logger.getLogger(BugTest.class);

	@Test
	public void testBug319() throws Exception {
		try {
			String fileName = "testFiles/bugs/error_319.gpml";
			FileInputStream fis = new FileInputStream(fileName);
			new GpmlParser().createGraph(fis);
			assertEquals(1, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private ModelComparator mc = new ModelComparator(1.0);

	@Test
	public void testBug328() throws Exception {
		try {
			String filename = "testFiles/bugs/error_328.gpml";
			Model model1 = new GPMLToModel().getModel(filename);

			assertEquals(7, getWarnings().size());

			int complexes = 0;
			for (Element alias : model1.getElements()) {
				if (alias instanceof Complex) {
					complexes++;
				}
			}
			assertEquals("Invalid number of complexes", 1, complexes);

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals("File " + filename + " different after transformation", 0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
