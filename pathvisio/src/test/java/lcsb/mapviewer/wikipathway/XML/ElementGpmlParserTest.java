package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;

public class ElementGpmlParserTest {

	ElementGpmlParser<Object> parser = new ElementGpmlParser<Object>() {
		@Override
		public String toXml(Collection<Object> list) throws ConverterException {
			throw new NotImplementedException();
		}

		@Override
		public String toXml(Object object) throws ConverterException {
			throw new NotImplementedException();
		}

		@Override
		public Object parse(Element node) throws ConverterException {
			throw new NotImplementedException();
		}
	};

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseTransparentColor() {
		try {
			Color color = parser.hexStringToColor("Transparent");
			assertNotNull(color);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
