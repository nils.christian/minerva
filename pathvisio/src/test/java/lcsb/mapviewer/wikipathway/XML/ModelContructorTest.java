package lcsb.mapviewer.wikipathway.XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.graphics.PolylineData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ModelContructorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			Point2D p1 = new Point2D.Double(1455.5988605049254, 562.0578193221613);
			Point2D p2 = new Point2D.Double(1455.5988605049254, 562.0578193221613);
			PolylineData pd = new PolylineData(p1, p2);
			
			ModelContructor mc = new ModelContructor();
			Pair<PolylineData,PolylineData> result = mc.splitPolyline(pd);
			assertNotNull(result);
			assertNotNull(result.getLeft());
			assertNotNull(result.getRight());
			assertEquals(2, result.getLeft().getPoints().size());
			assertEquals(2, result.getRight().getPoints().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
