package lcsb.mapviewer.wikipathway.XML;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BiopaxParserTest.class,//
		DataNodeParserTest.class,//
		BugTest.class,//
		EdgeLineParserTest.class,//
		GpmlParserTest.class,//
		LabelParserTest.class,//
		ReferenceParserTest.class,//
		ModelToGPMLTest.class,//
		ModelContructorTest.class,//
		ShapeParserTest.class,//
		StateParserTest.class,//
})
public class AllXmlTests {

}
