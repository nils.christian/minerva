package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ReactionGpmlOutputToModelTest extends WikipathwaysTestFunctions{
	/**
	 * Default class logger.
	 */
	static Logger						logger	= Logger.getLogger(ReactionGpmlOutputToModelTest.class);

	private ModelComparator	mc			= new ModelComparator(1.0);

	@Test
	public void LineInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_line_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ArrowInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_arrow_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void DashedLineInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_line_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void DashedArrowInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_arrow_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TBarInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_tbar_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Inhibition);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void NecessaryStimulationInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_necessary_stimulation_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof PhysicalStimulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BindingInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_binding_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ConversionInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_conversion_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void StimulationInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_stimulation_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof PhysicalStimulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ModificationInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_modification_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CatalystInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_catalyst_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Catalysis);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void InhibitionInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_inhibition_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Inhibition);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CleavageInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_cleavage_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CovalentBondInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_covalent_bond_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingLeftInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_branching_left_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingRightInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_branching_right_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(0, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TranscriptionTranslationInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_transcription_translation_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void GapInteractionOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_mim_gap_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(1, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TBarInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_tbar_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Inhibition);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void NecessaryStimulationInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_necessary_stimulation_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof PhysicalStimulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BindingInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_binding_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ConversionInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_conversion_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void StimulationInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_stimulation_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof PhysicalStimulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void ModificationInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_modification_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CatalystInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_catalyst_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Catalysis);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void InhibitionInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_inhibition_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Inhibition);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CleavageInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_cleavage_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void CovalentBondInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_covalent_bond_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingLeftInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_branching_left_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void BranchingRightInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_branching_right_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modifier);
			}

			assertNotNull(reactant);
			assertNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void TranscriptionTranslationInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_transcription_translation_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void GapInteractionDashedOutputTest() throws Exception {
		try {
			String fileName = "testFiles/reactions/interaction_dashed_mim_gap_output.gpml";

			Model model1 = new GPMLToModel().getModel(fileName);

			assertEquals(1, model1.getReactions().size());

			Reaction reaction = model1.getReactions().iterator().next();

			Product product = null;
			if (reaction.getProducts().size() > 1) {
				product = reaction.getProducts().get(1);
			}
			Reactant reactant = null;
			if (reaction.getReactants().size() > 1) {
				reactant = reaction.getReactants().get(1);
			}
			Modifier modifier = null;
			if (reaction.getModifiers().size() > 0) {
				modifier = reaction.getModifiers().get(0);
				assertTrue(modifier instanceof Modulation);
			}

			assertNull(reactant);
			assertNotNull(product);
			assertNull(modifier);

			assertEquals(2, getWarnings().size());

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model1);
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

			assertEquals(0, mc.compare(model1, model2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
