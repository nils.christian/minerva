package lcsb.mapviewer.wikipathway.utils;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * This class has some geometric utils.
 * 
 * @author Jan Badura
 * 
 */
public final class Geo {

	/**
	 * Max distance betwen point and line that still allows to match intersection.
	 */
	private static final int	DISTANCE_PROXIMITY	= 10;

	/**
	 * Default class constructor. Prevents instatiation.
	 */
	private Geo() {

	}

	/**
	 * This function calculates point on line that is closest to given point.
	 * 
	 * @param line
	 *          - line has to be parallel to oX or oY
	 * @param point
	 *          point
	 * @return point on line that is closest to given point
	 */
	private static Point2D closestPointOnLine(Line2D line, Point2D point) {
		Point2D s = line.getP1();
		Point2D e = line.getP2();
		if (s.equals(e)) {
			return (Point2D) s.clone();
		}

		if (s.getX() == e.getX()) {
			if (point.getY() > Math.min(s.getY(), e.getY()) && point.getY() < Math.max(s.getY(), e.getY())) {
				return new Point2D.Double(s.getX(), point.getY());
			} else {
				if (point.distance(s) < point.distance(e)) {
					return (Point2D) s.clone();
				} else {
					return (Point2D) e.clone();
				}
			}
		} else if (s.getY() == e.getY()) {
			if (point.getX() > Math.min(s.getX(), e.getX()) && point.getX() < Math.max(s.getX(), e.getX())) {
				return new Point2D.Double(point.getX(), s.getY());
			} else {
				if (point.distance(s) < point.distance(e)) {
					return (Point2D) s.clone();
				} else {
					return (Point2D) e.clone();
				}
			}
		} else {
			throw new InvalidArgumentException("Line is invalid");
		}
	}

	/**
	 * This function return value from <0,1>, that is the position of given point
	 * on given Polyline.
	 * 
	 * @param mainLine
	 *          line on which we are looking for a point
	 * @param point
	 *          point
	 * @return double position, 0 <= position <= 1
	 */
	public static double distanceOnPolyline(PolylineData mainLine, Point2D point) {
		double length = 0;
		double tmp = 0;
		for (Line2D line : mainLine.getLines()) {
			length += lineLen(line);
		}

		for (Line2D line : mainLine.getLines()) {
			if (line.ptSegDist(point) < DISTANCE_PROXIMITY) {
				tmp += line.getP1().distance(point);
				break;
			} else {
				tmp += lineLen(line);
			}
		}
		double position = tmp / length;
		if (position < 0 || position > 1) {
			throw new InvalidStateException();
		}
		return position;
	}

	/**
	 * Returns length of the line.
	 * 
	 * @param line
	 *          line
	 * @return length of the line
	 */
	public static double lineLen(Line2D line) {
		return line.getP1().distance(line.getP2());
	}

	/**
	 * Returns distance between point and rectangle border.
	 * 
	 * @param point
	 *          point
	 * @param rect
	 *          rectangle border
	 * @return distance between point and rectangle border
	 */
	public static double distance(Point2D point, Rectangle2D rect) {
		double res = Double.MAX_VALUE;

		Point2D p1 = new Point2D.Double(rect.getX(), rect.getY());
		Point2D p2 = new Point2D.Double(rect.getX(), rect.getY() + rect.getHeight());
		Point2D p3 = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight());
		Point2D p4 = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY());
		res = Math.min(res, new Line2D.Double(p1, p2).ptSegDist(point));
		res = Math.min(res, new Line2D.Double(p2, p3).ptSegDist(point));
		res = Math.min(res, new Line2D.Double(p3, p4).ptSegDist(point));
		res = Math.min(res, new Line2D.Double(p4, p1).ptSegDist(point));

		return res;
	}

	/**
	 * This function returns point on the middle of rectangle side, that is
	 * closest to the given point.
	 * 
	 * @param rect
	 *          rectangle on which we look for a point
	 * @param point
	 *          point to which result should be as close as possible
	 * @return point on the middle of rectangle side that is as close as possible
	 *         to the input point
	 */
	public static Point2D pointOnRectangle(Rectangle2D rect, Point2D point) {

		Point2D p1 = new Point2D.Double(rect.getX(), rect.getY());
		Point2D p2 = new Point2D.Double(rect.getX(), rect.getY() + rect.getHeight());
		Point2D p3 = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight());
		Point2D p4 = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY());

		Point2D p12 = new Point2D.Double((p1.getX() + p2.getX()) / 2, (p1.getY() + p2.getY()) / 2);
		Point2D p23 = new Point2D.Double((p2.getX() + p3.getX()) / 2, (p2.getY() + p3.getY()) / 2);
		Point2D p34 = new Point2D.Double((p3.getX() + p4.getX()) / 2, (p3.getY() + p4.getY()) / 2);
		Point2D p41 = new Point2D.Double((p4.getX() + p1.getX()) / 2, (p4.getY() + p1.getY()) / 2);

		double dis1, dis2, dis3, dis4, min;
		dis1 = point.distance(p12);
		dis2 = point.distance(p23);
		dis3 = point.distance(p34);
		dis4 = point.distance(p41);

		min = Math.min(dis1, dis2);
		min = Math.min(min, dis3);
		min = Math.min(min, dis4);

		if (min == dis1) {
			return p12;
		} else if (min == dis2) {
			return p23;
		} else if (min == dis3) {
			return p34;
		} else {
			return p41;
		}
	}

	/**
	 * This function returns point on rectangle that is closest to given point.
	 * 
	 * @param rect
	 *          rectangle on which the result poiint will be placed
	 * @param point
	 *          point for which we look the closest reference on rectangle
	 * @return the closest possible point on rectangle
	 */
	public static Point2D closestPointOnRectangle(Rectangle2D rect, Point2D point) {
		Point2D p1 = new Point2D.Double(rect.getX(), rect.getY());
		Point2D p2 = new Point2D.Double(rect.getX(), rect.getY() + rect.getHeight());
		Point2D p3 = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight());
		Point2D p4 = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY());

		double dis1, dis2, dis3, dis4, min;
		dis1 = new Line2D.Double(p1, p2).ptSegDist(point);
		dis2 = new Line2D.Double(p2, p3).ptSegDist(point);
		dis3 = new Line2D.Double(p3, p4).ptSegDist(point);
		dis4 = new Line2D.Double(p4, p1).ptSegDist(point);
		min = Math.min(dis1, dis2);
		min = Math.min(min, dis3);
		min = Math.min(min, dis4);

		if (min == dis1) {
			return closestPointOnLine(new Line2D.Double(p1, p2), point);
		} else if (min == dis2) {
			return closestPointOnLine(new Line2D.Double(p2, p3), point);
		} else if (min == dis3) {
			return closestPointOnLine(new Line2D.Double(p3, p4), point);
		} else {
			return closestPointOnLine(new Line2D.Double(p4, p1), point);
		}
	}

	/**
	 * Returns point that is outside given rectangle, but still close to the given
	 * point. If the given point is inside rec then the given point is returned.
	 * 
	 * @param rec
	 *          rectangle
	 * @param point
	 *          point
	 * @return point that is outside given rectangle, but still close to the given
	 *         point
	 */
	public static Point2D pointRightOutsideRec(Rectangle2D rec, Point2D point) {
		if (!rec.contains(point)) {
			return (Point2D) point.clone();
		} else {
			double lx = rec.getX();
			double rx = lx + rec.getWidth();
			double uy = rec.getY();
			double dy = uy + rec.getHeight();

			double dis1 = Math.abs(point.getX() - lx);
			double dis2 = Math.abs(point.getX() - rx);
			double dis3 = Math.abs(point.getY() - uy);
			double dis4 = Math.abs(point.getY() - dy);

			double min = Math.min(dis1, dis2);
			min = Math.min(min, dis3);
			min = Math.min(min, dis4);

			if (min == dis1) {
				return new Point2D.Double(lx - 1.0, point.getY());
			} else if (min == dis2) {
				return new Point2D.Double(rx + 1.0, point.getY());
			} else if (min == dis3) {
				return new Point2D.Double(point.getX(), uy - 1.0);
			} else {
				return new Point2D.Double(point.getX(), uy + 1.0);
			}
		}
	}

	/**
	 * Creates a {@link PolylineData} that is identical to parameter line, but
	 * doesn't contain duplicate points.
	 * 
	 * @param line
	 *          line that is transformed
	 * @return input line without duplicate points
	 */
	public static PolylineData removeRedundantPoints(PolylineData line) {
		PolylineData res = new PolylineData(line);

		for (int i = 2; i < res.getPoints().size(); i++) {
			if (new Line2D.Double(res.getPoints().get(i), res.getPoints().get(i - 2)).ptSegDist(res.getPoints().get(i - 1)) < DISTANCE_PROXIMITY) {
				res.getPoints().remove(i - 1);
				i--;
			}
		}
		return res;
	}

	/**
	 * Returns distance between point and line.
	 * 
	 * @param point
	 *          point
	 * @param mainLine
	 *          line
	 * @return distance between point and line
	 */
	public static double distanceFromPolyline(Point2D point, PolylineData mainLine) {
		double min = Double.MAX_VALUE;
		for (Line2D line : mainLine.getLines()) {
			min = Math.min(min, line.ptSegDist(point));
		}
		return min;
	}

	/**
	 * Returns point on line that is as closse as possible to point.
	 * 
	 * @param point
	 *          point
	 * @param mainLine
	 *          line
	 * @return point on line that is as closse as possible to point
	 */
	public static Point2D closestPointOnPolyline(PolylineData mainLine, Point2D point) {
		Point2D res = null;
		double distance = distanceOnPolyline(mainLine, point);
		double lenght = 0.0;
		for (Line2D line : mainLine.getLines()) {
			lenght += lineLen(line);
		}
		lenght *= distance;
		for (Line2D line : mainLine.getLines()) {
			if (lenght - lineLen(line) > 0) {
				lenght -= lineLen(line);
			} else {
				double tmp = lenght / lineLen(line);
				double x = line.getX1() + tmp * (line.getX2() - line.getX1());
				double y = line.getY1() + tmp * (line.getY2() - line.getY1());
				res = new Point2D.Double(x, y);
			}
		}
		return res;
	}

}
