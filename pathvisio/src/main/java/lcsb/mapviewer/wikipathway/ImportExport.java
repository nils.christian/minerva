package lcsb.mapviewer.wikipathway;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.LoggingEvent;
import org.pathvisio.core.model.ConverterException;
import org.pathvisio.core.model.Pathway;
import org.pathvisio.core.model.PathwayExporter;
import org.pathvisio.core.model.PathwayImporter;
import org.pathvisio.desktop.PvDesktop;
import org.pathvisio.desktop.plugin.Plugin;

import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.wikipathway.XML.GPMLToModel;
import lcsb.mapviewer.wikipathway.XML.ModelToGPML;

/**
 * Pathvisio plugin that allows to import/export CellDesigner file.
 * 
 * @author Piotr Gawron
 * 
 */
public class ImportExport implements Plugin {
	/**
	 * Default class logger.
	 */
	private static Logger	 logger						= Logger.getLogger(ImportExport.class);
	/**
	 * List of extensions supported by import plugin.
	 */
	private final String[] importExtensions	= new String[] { "xml" };
	/**
	 * List of extensions supported by export plugin.
	 */
	private final String[] exportExtensions	= new String[] { "cell" };

	@Override
	public void init(PvDesktop desktop) {
		try {
			Properties props = new Properties();
			props.load(new FileInputStream("log4j.properties"));
			PropertyConfigurator.configure(props);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ExportCellDesigner exportPlugin = new ExportCellDesigner();
		ImportCellDesigner importPlugin = new ImportCellDesigner();
		desktop.getSwingEngine().getEngine().addPathwayImporter(importPlugin);
		desktop.getSwingEngine().getEngine().addPathwayExporter(exportPlugin);
	}

	@Override
	public void done() {
	}

	/**
	 * Implementation of {@link PathwayExporter} that allows PathVisio to import
	 * from CellDesigner xml file.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	protected class ImportCellDesigner implements PathwayImporter {

		/**
		 * List of warnings that occured during conversion.
		 */
		private List<LoggingEvent> warnings = new ArrayList<>();

		/**
		 * Default constructor.
		 */
		public ImportCellDesigner() {
		}

		@Override
		public String[] getExtensions() {
			return importExtensions;
		}

		@Override
		public String getName() {
			return "CellDesigner";
		}

		@Override
		public Pathway doImport(File file) throws ConverterException {
			EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
			try {
				Logger.getRootLogger().addAppender(appender);
				Pathway pathway = new Pathway();
				String fileName = file.getPath();

				CellDesignerXmlParser parser = new CellDesignerXmlParser();
				Model model = (Model) parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
				String tmp = new ModelToGPML().getGPML(model);
				InputStream stream = new ByteArrayInputStream(tmp.getBytes(StandardCharsets.UTF_8));
				Boolean validate = false;
				pathway.readFromXml(stream, validate);

				Logger.getRootLogger().removeAppender(appender);
				warnings.addAll(appender.getWarnings());
				return pathway;
			} catch (Exception e) {
				logger.error(e, e);
				throw new ConverterException(e);
			} finally {
				Logger.getRootLogger().removeAppender(appender);
			}
		}

		@Override
		public boolean isCorrectType(File arg0) {
			return true;
		}

		@Override
		public List<String> getWarnings() {
			List<String> result = new ArrayList<>();
			for (LoggingEvent event : warnings) {
				if (event.getMessage() instanceof String) {
					result.add((String) event.getMessage());
				}
			}
			return result;
		}
	}

	/**
	 * Implementation of {@link PathwayExporter} that allows PathVisio to export
	 * into CellDesigner xml file.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	protected class ExportCellDesigner implements PathwayExporter {

		/**
		 * {@link Model} that was created using this {@link PathwayExporter}.
		 */
		private Model				 model		= null;

		/**
		 * List of export warnings.
		 */
		private List<String> warnings	= new ArrayList<>();

		@Override
		public String[] getExtensions() {
			return exportExtensions;
		}

		@Override
		public String getName() {
			return "CellDesigner";
		}

		@Override
		public List<String> getWarnings() {
			return warnings;
		}

		@Override
		public void doExport(File file, Pathway pathway) throws ConverterException {
			EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
			try {
				Logger.getRootLogger().addAppender(appender);
				pathway.writeToXml(new File("tmp.gpml"), false);
				model = new GPMLToModel().getModel("tmp.gpml");

				Logger.getRootLogger().removeAppender(appender);
				warnings = createWarnings(appender);

				CellDesignerXmlParser parser = new CellDesignerXmlParser();
				String xml = parser.toXml(model);
				PrintWriter writer = new PrintWriter(file.getPath(), "UTF-8");
				writer.println(xml);
				writer.close();

				warnings.add("Please manually change extension of saved file from .cell to .xml");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new ConverterException(e);
			} finally {
				Logger.getRootLogger().removeAppender(appender);
			}
		}

		/**
		 * Creates list of warnings from log4j appender data.
		 * 
		 * @param appender
		 *          appender with the logs
		 * @return list of warnings from log4j appender data
		 */
		private List<String> createWarnings(EventStorageLoggerAppender appender) {
			List<String> warnings = new ArrayList<>();
			for (LoggingEvent event : appender.getWarnings()) {
				if (event.getMessage() instanceof String) {
					warnings.add(((String) event.getMessage()).replaceAll("\n", "_NEW_LINE_"));
				} else {
					logger.warn("Unknown message class: " + event.getClass());
				}
			}
			return warnings;
		}
	}
}
