/**
 * Model elements obtained from gpml file. There are few basic classes in this
 * package:
 * <ul>
 * <li> {@link lcsb.mapviewer.wikipathway.model.Graph Graph} - description of the
 * whole model imported from gpml file,</li>
 * <li> {@link lcsb.mapviewer.wikipathway.model.Interaction Interaction} -
 * description of single interaction in the model,</li>
 * <li> {@link lcsb.mapviewer.wikipathway.model.PathwayElement PathwayElement} -
 * single element in the model.</li>
 * </ul>
 * 
 */
package lcsb.mapviewer.wikipathway.model;

