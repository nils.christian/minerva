package lcsb.mapviewer.wikipathway.model;

/**
 * Class used to store data from Label from .gpml.
 * 
 * @author Jan Badura
 * 
 */
public class Label extends GraphicalPathwayElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ???
	 */
	private String						textLabel;

	/**
	 * Font weight (like italic, bold) that should be used.
	 */
	private String						fontWeight;

	/**
	 * Shape associated with the label border.
	 */
	private String						shape;

	/**
	 * How description should be aligned.
	 */
	private String						vAlign;

	/**
	 * Where this node belongs to.
	 */
	private String						groupRef;

	/**
	 * Sometimes {@link Label labels} are connected to reactions. This field
	 */
	private boolean					 treatAsNode			= false;

	/**
	 * Default constructor.
	 * 
	 * @param graphId
	 *          {@link PathwayElement#graphId}
	 */
	public Label(String graphId) {
		super(graphId);
		setTextLabel(null);
		setGroupRef(null);
	}

	/**
	 * Empty constructor that should be used only by serialization tools and
	 * subclasses.
	 */
	protected Label() {
	}

	@Override
	public String getName() {
		return getTextLabel();
	}

	/**
	 * @return the textLabel
	 * @see #textLabel
	 */
	public String getTextLabel() {
		return textLabel;
	}

	/**
	 * @param textLabel
	 *          the textLabel to set
	 * @see #textLabel
	 */
	public void setTextLabel(String textLabel) {
		this.textLabel = textLabel;
	}

	/**
	 * @return the groupRef
	 * @see #groupRef
	 */
	public String getGroupRef() {
		return groupRef;
	}

	/**
	 * @param groupRef
	 *          the groupRef to set
	 * @see #groupRef
	 */
	public void setGroupRef(String groupRef) {
		this.groupRef = groupRef;
	}

	/**
	 * @return the treatAsNode
	 * @see #treatAsNode
	 */
	public boolean isTreatAsNode() {
		return treatAsNode;
	}

	/**
	 * @param treatAsNode
	 *          the treatAsNode to set
	 * @see #treatAsNode
	 */
	public void setTreatAsNode(boolean treatAsNode) {
		this.treatAsNode = treatAsNode;
	}

	/**
	 * @return the vAlign
	 * @see #vAlign
	 */
	public String getvAlign() {
		return vAlign;
	}

	/**
	 * @param vAlign
	 *          the vAlign to set
	 * @see #vAlign
	 */
	public void setvAlign(String vAlign) {
		this.vAlign = vAlign;
	}

	/**
	 * @return the fontWeight
	 * @see #fontWeight
	 */
	public String getFontWeight() {
		return fontWeight;
	}

	/**
	 * @param fontWeight
	 *          the fontWeight to set
	 * @see #fontWeight
	 */
	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
	}

	/**
	 * @return the shape
	 * @see #shape
	 */
	public String getShape() {
		return shape;
	}

	/**
	 * @param shape
	 *          the shape to set
	 * @see #shape
	 */
	public void setShape(String shape) {
		this.shape = shape;
	}

}
