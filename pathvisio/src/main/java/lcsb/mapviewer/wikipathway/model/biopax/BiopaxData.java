package lcsb.mapviewer.wikipathway.model.biopax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

import org.apache.log4j.Logger;

/**
 * Element containg all biopax information parsed from gpml file.
 * 
 * @author Piotr Gawron
 * 
 */
public class BiopaxData implements Serializable {

	/**
	 * 
	 */
	private static final long										serialVersionUID					 = 1L;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final transient Logger							 logger										 = Logger.getLogger(BiopaxData.class);

	/**
	 * List of {@link BiopaxPublication publications}.
	 */
	private Map<String, BiopaxPublication>			 publications							 = new HashMap<String, BiopaxPublication>();

	/**
	 * List of {@link BiopaxOpenControlledVocabulary biopax vocabulary}.
	 */
	private List<BiopaxOpenControlledVocabulary> openControlledVocabularies = new ArrayList<BiopaxOpenControlledVocabulary>();

	/**
	 * @return the publications
	 * @see #publications
	 */
	public Collection<BiopaxPublication> getPublications() {
		return publications.values();
	}

	/**
	 * @param publication
	 *          the publication to add
	 * @see #publications
	 */
	public void addPublication(BiopaxPublication publication) {
		String id = publication.getReferenceId();
		if (this.publications.get(publication.getReferenceId()) != null) {
			throw new InvalidArgumentException("Biopax publication with " + id + " already exists.");
		}
		this.publications.put(id, publication);
	}

	/**
	 * Returns publication identified by reference id.
	 * 
	 * @param ref
	 *          reference identifier
	 * @return {@link BiopaxPublication} corresponding to the ref
	 */
	public BiopaxPublication getPublicationByReference(String ref) {
		return publications.get(ref);
	}

	/**
	 * @return the openControlledVocabularies
	 * @see #openControlledVocabularies
	 */
	public List<BiopaxOpenControlledVocabulary> getOpenControlledVocabularies() {
		return openControlledVocabularies;
	}

	/**
	 * @param openControlledVocabulary
	 *          the openControlledVocabulary to add
	 * @see #openControlledVocabularies
	 */
	public void addOpenControlledVocabulary(BiopaxOpenControlledVocabulary openControlledVocabulary) {
		this.openControlledVocabularies.add(openControlledVocabulary);
	}

}
