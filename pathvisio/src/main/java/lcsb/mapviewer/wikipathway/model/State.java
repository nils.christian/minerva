package lcsb.mapviewer.wikipathway.model;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.ModificationState;

/**
 * Class used to store data about {@link DataNode} state from .gpml.
 * 
 * @author Piotr Gawron
 * 
 */
public class State extends GraphicalPathwayElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Shape of the element.
	 */
	private String						shape;

	/**
	 * References for given edge.
	 */
	private List<MiriamData>	references			 = new ArrayList<>();

	/**
	 * Object where this state is placed.
	 */
	private String						graphRef;

	/**
	 * State correspondig to {@link Protein#structuralState protein structural state}.
	 */
	private String						structuralState;

	/**
	 * Type of the modification.
	 */
	private ModificationState type;

	/**
	 * X position on {@link #graphRef} element.
	 */
	private Double						relX;

	/**
	 * Y position on {@link #graphRef} element.
	 */
	private Double						relY;

	/**
	 * Width of the element.
	 */
	private Double						width;

	/**
	 * Heightof the element.
	 */
	private Double						height;

	/**
	 * Default constructor.
	 * 
	 * @param graphId
	 *          {@link PathwayElement#graphId}
	 */
	public State(String graphId) {
		super(graphId);
	}

	/**
	 * Empty constructor that should be used only by serialization tools and
	 * subclasses.
	 */
	protected State() {
	}

	@Override
	String getName() {
		throw new NotImplementedException();
	}

	@Override
	public Rectangle2D getRectangle() {
		throw new NotImplementedException();
	}

	/**
	 * @return the shape
	 * @see #shape
	 */
	public String getShape() {
		return shape;
	}

	/**
	 * @param shape
	 *          the shape to set
	 * @see #shape
	 */
	public void setShape(String shape) {
		this.shape = shape;
	}

	/**
	 * @return the relX
	 * @see #relX
	 */
	public Double getRelX() {
		return relX;
	}

	/**
	 * @param relX
	 *          the relX to set
	 * @see #relX
	 */
	public void setRelX(Double relX) {
		this.relX = relX;
	}

	/**
	 * @return the relY
	 * @see #relY
	 */
	public Double getRelY() {
		return relY;
	}

	/**
	 * @param relY
	 *          the relY to set
	 * @see #relY
	 */
	public void setRelY(Double relY) {
		this.relY = relY;
	}

	/**
	 * @return the width
	 * @see #width
	 */
	public Double getWidth() {
		return width;
	}

	/**
	 * @param width
	 *          the width to set
	 * @see #width
	 */
	public void setWidth(Double width) {
		this.width = width;
	}

	/**
	 * @return the height
	 * @see #height
	 */
	public Double getHeight() {
		return height;
	}

	/**
	 * @param height
	 *          the height to set
	 * @see #height
	 */
	public void setHeight(Double height) {
		this.height = height;
	}

	/**
	 * @return the graphRef
	 * @see #graphRef
	 */
	public String getGraphRef() {
		return graphRef;
	}

	/**
	 * @param graphRef
	 *          the graphRef to set
	 * @see #graphRef
	 */
	public void setGraphRef(String graphRef) {
		this.graphRef = graphRef;
	}

	/**
	 * @return the references
	 * @see #references
	 */
	public List<MiriamData> getReferences() {
		return references;
	}

	/**
	 * @param references
	 *          the references to set
	 * @see #references
	 */
	public void setReferences(List<MiriamData> references) {
		this.references = references;
	}

	/**
	 * Adds reference to object.
	 * 
	 * @param reference
	 *          reference to add
	 */
	public void addReference(MiriamData reference) {
		this.references.add(reference);
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public ModificationState getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(ModificationState type) {
		this.type = type;
	}

	/**
	 * @return the structuralState
	 * @see #structuralState
	 */
	public String getStructuralState() {
		return structuralState;
	}

	/**
	 * @param structuralState the structuralState to set
	 * @see #structuralState
	 */
	public void setStructuralState(String structuralState) {
		this.structuralState = structuralState;
	}

}
