package lcsb.mapviewer.wikipathway.model;

import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for pathway elements. It defines common functionalities for
 * all elements in the model. There are two known subclasses:
 * <ul>
 * <li> {@link GraphicalPathwayElement}, representing elemnts with some graphical
 * representation</li>
 * <li> {@link Group}, representing just groups of elements</li>
 * </ul>
 * 
 * @author Jan Badura
 * 
 */
public abstract class PathwayElement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Comment of the element.
	 */
	private List<String>			comments				 = new ArrayList<>();

	/**
	 * Z order of the element.
	 */
	private Integer					 zOrder;

	/**
	 * Identifier of the element in model.
	 */
	private String						graphId;

	/**
	 * 
	 * Reference to biopax node with references about this element.
	 */
	private List<String>			biopaxReferences = new ArrayList<>();

	/**
	 * Default constructor.
	 * 
	 * @param graphId
	 *          {@link #graphId} value
	 */
	public PathwayElement(String graphId) {
		this.graphId = graphId;
	}

	/**
	 * Empty constructor that should be used only by serialization tools and
	 * subclasses.
	 */
	protected PathwayElement() {
	}

	/**
	 * 
	 * @return {@link #graphId}
	 */
	public String getGraphId() {
		return this.graphId;
	}

	/**
	 * Returns name of the element.
	 * 
	 * @return name of the element
	 */
	abstract String getName();

	/**
	 * Return boundary of the element.
	 * 
	 * @return boundary of the element
	 */
	abstract Rectangle2D getRectangle();

	/**
	 * @param graphId
	 *          the graphId to set
	 * @see #graphId
	 */
	void setGraphId(String graphId) {
		this.graphId = graphId;
	}

	/**
	 * Returns prefix that should be used in logger for warnings about this
	 * element.
	 * 
	 * @return prefix that should be used in logger for warnings about this
	 *         element
	 */
	public String getWarningPrefix() {
		return "[" + this.getClass().getSimpleName() + ", " + getGraphId() + "]\t";
	}

	/**
	 * @return the biopaxReference
	 * @see #biopaxReference
	 */
	public List<String> getBiopaxReference() {
		return biopaxReferences;
	}

	/**
	 * @param biopaxReferences
	 *          the biopaxReference to set
	 * @see #biopaxReference
	 */
	public void setBiopaxReference(List<String> biopaxReferences) {
		this.biopaxReferences = biopaxReferences;
	}

	/**
	 * @return the zOrder
	 * @see #zOrder
	 */
	public Integer getzOrder() {
		return zOrder;
	}

	/**
	 * @param zOrder
	 *          the zOrder to set
	 * @see #zOrder
	 */
	public void setzOrder(Integer zOrder) {
		this.zOrder = zOrder;
	}

	/**
	 * Adds reference to {@link #biopaxReferences}.
	 * 
	 * @param biopaxString
	 *          reference to add
	 */
	public void addBiopaxReference(String biopaxString) {
		biopaxReferences.add(biopaxString);
	}

	/**
	 * @return the comments
	 * @see #comments
	 */
	public List<String> getComments() {
		return comments;
	}

	/**
	 * @param comment
	 *          the comment to set
	 * @see #comment
	 */
	public void addComment(String comment) {
		this.comments.add(comment);
	}
}
