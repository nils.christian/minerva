package lcsb.mapviewer.wikipathway.model;

import java.awt.geom.Point2D;
import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * Class that defines infgomration about point stored in gpml structures.
 * 
 * @author Piotr Gawron
 *
 */
public class PointData implements Serializable {

	/**
	 * 
	 */
	private static final long			serialVersionUID = 1L;

	/**
	 * Defdault clas logger.
	 */
	private final transient Logger logger					 = Logger.getLogger(PointData.class);

	/**
	 * X coordinate.
	 */
	private Double								 x;

	/**
	 * Y coordinate.
	 */
	private Double								 y;

	/**
	 * String identifing anchor point on the map.
	 */
	private String								 graphRef;

	/**
	 * Where the point is placed on the {@link Shape} object (it's a value between
	 * -1,1).
	 */
	private String								 relX;

	/**
	 * Where the point is placed on the {@link Shape} object (it's a value between
	 * -1,1).
	 */
	private String								 relY;

	/**
	 * Type of line asociated with line that ends in this point.
	 */
	private GpmlInteractionType		type;

	/**
	 * @return the x
	 * @see #x
	 */
	public Double getX() {
		return x;
	}

	/**
	 * @param x
	 *          the x to set
	 * @see #x
	 */
	public void setX(Double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 * @see #y
	 */
	public Double getY() {
		return y;
	}

	/**
	 * @param y
	 *          the y to set
	 * @see #y
	 */
	public void setY(Double y) {
		this.y = y;
	}

	/**
	 * @param relX
	 *          the relX to set
	 * @see #relX
	 */
	public void setRelX(String relX) {
		this.relX = relX;
	}

	/**
	 * @param relY
	 *          the relY to set
	 * @see #relY
	 */
	public void setRelY(String relY) {
		this.relY = relY;
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public GpmlInteractionType getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(GpmlInteractionType type) {
		this.type = type;
	}

	/**
	 * @return the graphRef
	 * @see #graphRef
	 */
	public String getGraphRef() {
		return graphRef;
	}

	/**
	 * @param graphRef
	 *          the graphRef to set
	 * @see #graphRef
	 */
	public void setGraphRef(String graphRef) {
		this.graphRef = graphRef;
	}

	/**
	 * Transform {@link PointData} into standard {@link Point2D point}.
	 * 
	 * @return {@link Point2D point} represented by this structure
	 */
	public Point2D toPoint() {
		return new Point2D.Double(x, y);
	}

	/**
	 * Returns <code>true</code> if point is connected some defined point on the
	 * map, <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if point is connected some defined point on the
	 *         map, <code>false</code> otherwise.
	 */
	public boolean hasGraphRef() {
		return graphRef != null && !graphRef.isEmpty();
	}

	/**
	 * Returns {@link Direction direction} from which this point is placed on
	 * {@link Shape} to which the point is connected.
	 * 
	 * @return {@link Direction direction} from which this point is placed on
	 *         {@link Shape}
	 */
	public Direction getDirection() {
		if (relX == null || relY == null) {
			return null;
		} else if (relX.equals("1.0")) {
			return Direction.EAST;
		} else if (relX.equals("-1.0")) {
			return Direction.WEST;
		} else if (relY.equals("1.0")) {
			return Direction.SOUTH;
		} else if (relY.equals("-1.0")) {
			return Direction.NORTH;
		} else {
			logger.warn("Cannot determine connection direction from values: relX=" + relX + "; relY=" + relY + ". Estimating...");
			double xVal = Double.valueOf(relX);
			double yVal = Double.valueOf(relY);
			if (Math.abs(xVal) > Math.abs(yVal)) {
				if (xVal > 0) {
					return Direction.EAST;
				} else {
					return Direction.WEST;
				}
			} else {
				if (yVal > 0) {
					return Direction.NORTH;
				} else {
					return Direction.SOUTH;
				}
			}
		}
	}
}
