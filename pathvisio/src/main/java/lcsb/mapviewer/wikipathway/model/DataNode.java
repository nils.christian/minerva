package lcsb.mapviewer.wikipathway.model;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class used to store data from DataNode from .gpml.
 * 
 * @author Jan Badura
 * 
 */
public class DataNode extends GraphicalPathwayElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * References.
	 */
	private List<MiriamData>	references			 = new ArrayList<>();

	/**
	 * ???
	 */
	private String						type;

	/**
	 * Type o line used to draw element.
	 */
	private LineType					lineType				 = LineType.SOLID;

	/**
	 * ???
	 */
	private String						textLabel;

	/**
	 * Where this node belongs to.
	 */
	private String						groupRef;

	/**
	 * Font weight (like italic, bold) that should be used.
	 */
	private String						fontWeight;

	/**
	 * Align of the description.
	 */
	private String						vAlign;

	/**
	 * Default constructor.
	 * 
	 * @param graphId
	 *          {@link PathwayElement#graphId}
	 */
	public DataNode(String graphId) {
		super(graphId);
	}

	/**
	 * Empty constructor that should be used only by serialization tools and
	 * subclasses.
	 */
	protected DataNode() {
		super();
	}

	@Override
	public String toString() {
		return textLabel + " " + getGraphId() + " " + groupRef + " " + type;
	}

	@Override
	public String getName() {
		return getTextLabel();
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the textLabel
	 * @see #textLabel
	 */
	public String getTextLabel() {
		return textLabel;
	}

	/**
	 * @param textLabel
	 *          the textLabel to set
	 * @see #textLabel
	 */
	public void setTextLabel(String textLabel) {
		this.textLabel = textLabel;
	}

	/**
	 * @return the groupRef
	 * @see #groupRef
	 */
	public String getGroupRef() {
		return groupRef;
	}

	/**
	 * @param groupRef
	 *          the groupRef to set
	 * @see #groupRef
	 */
	public void setGroupRef(String groupRef) {
		this.groupRef = groupRef;
	}

	/**
	 * @return the references
	 * @see #references
	 */
	public List<MiriamData> getReferences() {
		return references;
	}

	/**
	 * @param references
	 *          the references to set
	 * @see #references
	 */
	public void setReferences(List<MiriamData> references) {
		this.references = references;
	}

	/**
	 * @return the vAlign
	 * @see #vAlign
	 */
	public String getvAlign() {
		return vAlign;
	}

	/**
	 * @param vAlign
	 *          the vAlign to set
	 * @see #vAlign
	 */
	public void setvAlign(String vAlign) {
		this.vAlign = vAlign;
	}

	/**
	 * Adds reference to node.
	 * 
	 * @param md
	 *          objet to add
	 */
	public void addReference(MiriamData md) {
		references.add(md);
	}

	/**
	 * @return the lineType
	 * @see #lineType
	 */
	public LineType getLineType() {
		return lineType;
	}

	/**
	 * @param lineType
	 *          the lineType to set
	 * @see #lineType
	 */
	public void setLineType(LineType lineType) {
		this.lineType = lineType;
	}

	/**
	 * @return the fontWeight
	 * @see #fontWeight
	 */
	public String getFontWeight() {
		return fontWeight;
	}

	/**
	 * @param fontWeight
	 *          the fontWeight to set
	 * @see #fontWeight
	 */
	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
	}

}
