package lcsb.mapviewer.wikipathway.XML;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.wikipathway.model.GpmlInteractionType;
import lcsb.mapviewer.wikipathway.model.PointData;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link PointData} objects from Xml {@link Element
 * node}.
 * 
 * @author Piotr Gawron
 *
 */
public class PointDataParser extends ElementGpmlParser<PointData> {

	/**
	 * Default class logger.
	 */
	private final Logger logger = Logger.getLogger(PointDataParser.class);

	@Override
	public PointData parse(Element element) throws UnknownTypeException {
		PointData result = new PointData();
		for (Pair<String, String> entry : getAttributes(element)) {
			switch (entry.getLeft()) {
				case ("X"):
					result.setX(Double.valueOf(entry.getRight()));
					break;
				case ("Y"):
					result.setY(Double.valueOf(entry.getRight()));
					break;
				case ("GraphRef"):
					result.setGraphRef(entry.getRight());
					break;
				case ("ArrowHead"):
					result.setType(GpmlInteractionType.getTypeByGpmlString(entry.getRight()));
					break;
				case ("RelX"):
					result.setRelX(entry.getRight());
					break;
				case ("RelY"):
					result.setRelY(entry.getRight());
					break;
				default:
					logger.warn("Unknown point attribute: " + entry.getLeft());
			}
		}
		return result;
	}

	@Override
	public String toXml(PointData node) throws ConverterException {
		throw new NotImplementedException();
	}

	@Override
	public String toXml(Collection<PointData> list) throws ConverterException {
		throw new NotImplementedException();
	}
}
