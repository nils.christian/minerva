package lcsb.mapviewer.wikipathway.XML;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxData;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxOpenControlledVocabulary;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxPublication;

/**
 * Parser of Biopax data from the gpml file.
 * 
 * @author Piotr Gawron
 * 
 */
public class BiopaxParser extends XmlParser {

	/**
	 * Default class logger.
	 */
	private static Logger						logger		 = Logger.getLogger(BiopaxParser.class);

	/**
	 * Hash used for next {@link BiopaxPublication} processed by parser.
	 */
	private Integer									hash			 = 1;

	/**
	 * Mapping between {@link MiriamData} and hash value.
	 */
	private Map<MiriamData, String>	miriamHash = new HashMap<>();

	/**
	 * Creates data structure from biopax xml node.
	 * 
	 * @param biopax
	 *          xml node
	 * @return {@link BiopaxData} structure containing biopax data
	 */
	public BiopaxData parse(Node biopax) {
		BiopaxData result = new BiopaxData();
		NodeList nodes = biopax.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if ("bp:PublicationXref".equalsIgnoreCase(node.getNodeName())) {
					BiopaxPublication publication = parsePublication(node);
					if (publication.getId() == null || publication.getId().isEmpty()) {
						logger.warn("No pubmed identifier defined for publication: title: " + publication.getTitle());
					}
					result.addPublication(publication);
				} else if ("bp:openControlledVocabulary".equalsIgnoreCase(node.getNodeName())) {
					result.addOpenControlledVocabulary(parseOpenControlledVocabulary(node));
				} else {
					logger.warn("Unknown biopax node: " + node.getNodeName());
				}
			}
		}
		return result;
	}

	/**
	 * Method which parse biopax vocabulary xml node.
	 * 
	 * @param biopaxNode
	 *          xml node
	 * @return {@link BiopaxOpenControlledVocabulary}
	 */
	private BiopaxOpenControlledVocabulary parseOpenControlledVocabulary(Node biopaxNode) {
		BiopaxOpenControlledVocabulary result = new BiopaxOpenControlledVocabulary();
		NodeList nodes = biopaxNode.getChildNodes();

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if ("bp:ID".equalsIgnoreCase(node.getNodeName())) {
					result.setId(node.getTextContent());
				} else if ("bp:TERM".equalsIgnoreCase(node.getNodeName())) {
					result.setTerm(node.getTextContent());
				} else if ("bp:Ontology".equalsIgnoreCase(node.getNodeName())) {
					result.setOntology(node.getTextContent());
				} else {
					logger.warn("Unknown biopax node: " + node.getNodeName());
				}
			}
		}
		return result;
	}

	/**
	 * Method which parse biopax publication xml node.
	 * 
	 * @param publication
	 *          xml node
	 * @return {@link BiopaxPublication}
	 */
	protected BiopaxPublication parsePublication(Node publication) {
		BiopaxPublication result = new BiopaxPublication();
		NodeList nodes = publication.getChildNodes();

		result.setReferenceId(getNodeAttr("rdf:id", publication));
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if ("bp:ID".equalsIgnoreCase(node.getNodeName())) {
					result.setId(node.getTextContent());
				} else if ("bp:DB".equalsIgnoreCase(node.getNodeName())) {
					result.setDb(node.getTextContent());
				} else if ("bp:TITLE".equalsIgnoreCase(node.getNodeName())) {
					result.setTitle(node.getTextContent());
				} else if ("bp:SOURCE".equalsIgnoreCase(node.getNodeName())) {
					result.setSource(node.getTextContent());
				} else if ("bp:YEAR".equalsIgnoreCase(node.getNodeName())) {
					result.setYear(node.getTextContent());
				} else if ("bp:AUTHORS".equalsIgnoreCase(node.getNodeName())) {
					result.setAuthors(node.getTextContent());
				} else {
					logger.warn("Unknown biopax node: " + node.getNodeName());
				}
			}
		}
		return result;
	}

	/**
	 * Returns xml string representing biopax reference to set of
	 * {@link MiriamData} form parameters.
	 * 
	 * @param miriamData
	 *          set of {@link MiriamData} for which we want to obtain biopax
	 *          references
	 * @return xml string representing biopax reference to set of
	 *         {@link MiriamData} form parameters. Only references to
	 *         {@link MiriamType#PUBMED} are included.
	 */
	public String toReferenceXml(Set<MiriamData> miriamData) {
		StringBuilder sb = new StringBuilder("");
		for (MiriamData md : miriamData) {
			if (md.getDataType().equals(MiriamType.PUBMED)) {
				sb.append("<BiopaxRef>" + getHash(md) + "</BiopaxRef>\n");
			}
		}
		return sb.toString();
	}

	/**
	 * Returns unique hash for the {@link MiriamData} that can be used as a key in
	 * Biopax xml.
	 * 
	 * @param md
	 *          {@link MiriamData} for which we want to have unique hash value
	 * @return unique hash for the {@link MiriamData} that can be used as a key in
	 *         Biopax xml
	 */
	private String getHash(MiriamData md) {
		if (miriamHash.get(md) == null) {
			miriamHash.put(md, "ann" + hash);
			hash++;
		}
		return miriamHash.get(md);
	}

	/**
	 * Converts collection of {@link MiriamData} into an xml {@link String}
	 * representing this collection as a biopax data.
	 * 
	 * @param miriamData
	 *          collection of {@link MiriamData}
	 * @return xml {@link String} representing this collection as a biopax data
	 */
	public String toXml(Collection<MiriamData> miriamData) {
		StringBuilder sb = new StringBuilder("");
		sb.append("<Biopax>\n");
		for (MiriamData md : miriamData) {
			if (md.getDataType().equals(MiriamType.PUBMED)) {
				sb.append(toXml(md));
			}
		}
		sb.append("</Biopax>\n");

		return sb.toString();
	}

	/**
	 * Converts {@link MiriamData} into xml string in biopax format.
	 * 
	 * @param md
	 *          {@link MiriamData} to transform
	 * @return xml string in biopax format representing {@link MiriamData}
	 */
	public String toXml(MiriamData md) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"<bp:PublicationXref xmlns:bp=\"http://www.biopax.org/release/biopax-level3.owl#\" "
						+ "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" rdf:id=\"" + getHash(md) + "\">\n");
		sb.append("  <bp:ID rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">" + md.getResource() + "</bp:ID>\n");
		sb.append("  <bp:DB rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">PubMed</bp:DB>\n");
		sb.append("</bp:PublicationXref>\n");
		return sb.toString();
	}

	/**
	 * Creates {@link MiriamData annotation} from {@link BiopaxPublication}.
	 * 
	 * @param publication
	 *          input biopax structure
	 * @return {@link MiriamData annotation}
	 */
	protected MiriamData createMiriamData(BiopaxPublication publication) {
		if ("PubMed".equals(publication.getDb())) {
			if (publication.getId() == null || publication.getId().equals("")) {
				return null;
			} else {
				return new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.PUBMED, publication.getId());
			}
		} else {
			throw new InvalidArgumentException("Unknown biopax database: " + publication.getDb());
		}
	}

	/**
	 * Returns list of {@link MiriamData} that are refernced in {@link BiopaxData}
	 * with identifier given in biopaxReference.
	 * 
	 * @param biopaxData
	 *          {@link BiopaxData} where annotations are stored
	 * @param biopaxReference
	 *          list of refrences (to data in {@link BiopaxData}) that we want to
	 *          convert into {@link MiriamData}
	 * @return list of {@link MiriamData} that are refernced in {@link BiopaxData}
	 *         with identifier given in biopaxReference.
	 */
	public Collection<MiriamData> getMiriamData(BiopaxData biopaxData, List<String> biopaxReference) {
		List<MiriamData> result = new ArrayList<>();
		for (String string : biopaxReference) {
			BiopaxPublication bp = biopaxData.getPublicationByReference(string);
			if (bp != null) {
				MiriamData md = createMiriamData(bp);
				if (md != null) {
					result.add(md);
				} else {
					logger.warn("[" + string + "]\tBiopax publication is invalid.");
				}
			} else {
				logger.warn("[" + string + "]\tBiopax publication doesn't exist.");
			}
		}
		return result;
	}

}
