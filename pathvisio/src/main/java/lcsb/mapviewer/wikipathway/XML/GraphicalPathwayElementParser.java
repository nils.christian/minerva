package lcsb.mapviewer.wikipathway.XML;

import java.awt.font.TextAttribute;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.geometry.TextAlignment;
import lcsb.mapviewer.wikipathway.model.GpmlLineType;
import lcsb.mapviewer.wikipathway.model.GraphicalPathwayElement;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser used for extracting common data (from gpml file) for all elements
 * extending {@link GraphicalPathwayElement} class.
 * 
 * @author Piotr Gawron
 *
 * @param <T>
 */
public abstract class GraphicalPathwayElementParser<T extends GraphicalPathwayElement> extends ElementGpmlParser<T> {

	/**
	 * Default class logger.
	 */
	private final Logger logger = Logger.getLogger(GraphicalPathwayElementParser.class);

	/**
	 * Parse font attributes that might appear.
	 * 
	 * @param element
	 *          element where data should be put
	 * @param attribute
	 *          attribute to parse
	 * @return true if element was parsed properly
	 * @throws UnknownTypeException
	 *           thrown when some elements contains unknown values
	 */
	protected boolean parseCommonGraphicAttributes(GraphicalPathwayElement element, Pair<String, String> attribute) throws UnknownTypeException {
		switch (attribute.getLeft()) {
			case ("LineThickness"):
				try {
					Double value = Double.valueOf(attribute.getRight());
					element.setLineThickness(value);
				} catch (NumberFormatException e) {
					logger.warn("Invalid LineThickness: " + attribute.getRight());
				}
				break;
			case ("LineStyle"):
				element.setLineType(GpmlLineType.getByGpmlName(attribute.getRight()).getCorrespondingGlobalLineType());
				break;
			case ("FontStyle"):
				element.addFontAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);
				break;
			case ("FontName"):
				element.setFontName(attribute.getRight());
				break;
			case ("Align"):
				switch (attribute.getRight()) {
					case ("Right"):
						element.setTextAlignment(TextAlignment.RIGHT);
						break;
					case ("Left"):
						element.setTextAlignment(TextAlignment.LEFT);
						break;
					default:
						logger.warn("Unknown value of attribute: " + attribute.getLeft() + " - " + attribute.getRight());
						break;
				}
				break;
			default:
				return false;
		}
		return true;
	}

}
