package lcsb.mapviewer.wikipathway.XML;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.model.ReferenceMapping;

/**
 * Parser used to extract {@link MiriamData refernces} in miriam format from
 * GPML data.
 * 
 * @author Piotr Gawron
 *
 */
public class ReferenceParser extends ElementGpmlParser<MiriamData> {

	/**
	 * Default class logger.
	 */
	private final Logger logger = Logger.getLogger(ReferenceParser.class);

	/**
	 * This function creates MiriamData from database name and id.
	 * 
	 * @param id
	 *          - resource identifier
	 * @param db
	 *          -database type
	 * @return {@link MiriamData} object referenceing to the resource
	 */
	protected MiriamData createMiriamData(String id, String db) {
		if (db == null || db.equals("")) {
			throw new InvalidArgumentException("Invalid db type: " + db);
		} else if (id == null || id.trim().equals("")) {
			throw new InvalidArgumentException("Invalid db resource value: " + id);
		}
		ReferenceMapping mapping = ReferenceMapping.getMappingByGpmlString(db);
		MiriamType type = null;
		if (mapping != null) {
			type = mapping.getType();
		} else {
			for (MiriamType t : MiriamType.values()) {
				if (t.getCommonName().equalsIgnoreCase(db)) {
					type = t;
				} else {
					for (String uri : t.getUris()) {
						if (uri.endsWith(db)) {
							type = t;
						}
					}
				}
			}
		}
		if (type != null) {
			return new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, type, id);
		} else {
			throw new NotImplementedException("This database type is not implemented yet: " + db + "; resource: " + id);
		}
	}

	@Override
	public MiriamData parse(Element node) {
		String id = null;
		String db = null;
		for (Pair<String, String> entry : getAttributes(node)) {
			switch (entry.getLeft()) {
				case ("ID"):
					id = entry.getRight();
					break;
				case ("Database"):
					db = entry.getRight();
					break;
				default:
					logger.warn("Unknown attribute of " + node.getNodeName() + " node: " + entry.getLeft());
					break;
			}
		}

		NodeList tmpList = node.getChildNodes();
		for (int j = 0; j < tmpList.getLength(); j++) {
			Node tmpNode = tmpList.item(j);
			if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eTmp = (Element) tmpNode;
				switch (eTmp.getNodeName()) {
					default:
						logger.warn("Unknown sub-node of " + node.getNodeName() + " node: " + eTmp.getNodeName());
						break;
				}
			}
		}

		if (id != null && !id.isEmpty()) {
			if (db == null || db.isEmpty()) {
				logger.warn("Reference is invalid. Database identifier exists (" + id + "), but no database type is set.");
			} else {
				MiriamData md = createMiriamData(id, db);
				return md;
			}
		}
		return null;
	}

	@Override
	public String toXml(MiriamData md) throws ConverterException {
		if (md == null) {
			return "<Xref ID=\"\" Database=\"\"/>\n";
		} else if (MiriamType.PUBMED.equals(md.getDataType())) {
			throw new InvalidArgumentException("Pubmed cannot be exported to XRef node.");
		} else {
			ReferenceMapping mapping = ReferenceMapping.getMappingByMiriamType(md.getDataType());
			if (mapping == null) {
				throw new InvalidArgumentException("Don't know how to export " + md.getDataType() + " annotation.");
			} else if (mapping.getGpmlString() == null) {
				logger.warn(md.getDataType().getCommonName() + " annotation is not supported by GPML");
				return "";
			} else {
				return "<Xref ID=\"" + md.getResource() + "\" Database=\"" + mapping.getGpmlString() + "\"/>\n";
			}
		}
	}

	@Override
	public String toXml(Collection<MiriamData> miriamData) throws ConverterException {
		StringBuilder result = new StringBuilder("");
		int counter = 0;
		for (MiriamData md : miriamData) {
			if (!md.getDataType().equals(MiriamType.PUBMED)) {
				counter++;
				if (counter == 1) {
					EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
					try {
						Logger.getRootLogger().addAppender(appender);
						result.append(toXml(md));
						if (appender.getWarnings().size() > 0) {
							counter--;
						}
					} finally {
						Logger.getRootLogger().removeAppender(appender);
					}
				} else {
					logger.warn("Annotation ommited - gpml support only one annotation per element: " + md.getDataType() + ": " + md.getResource());
				}
			}
		}
		if (counter == 0) {
			return toXml((MiriamData) null);
		}
		return result.toString();
	}
}
