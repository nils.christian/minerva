package lcsb.mapviewer.wikipathway.XML;

import lcsb.mapviewer.converter.ConverterException;

/**
 * Exception that shold be thrown when edges create a cycle. Consider an example
 * which has three edges:
 * <ul>
 * <li>edge_A - starts in edge_B and ends in some protein,</li>
 * <li>edge_B - starts in edge_C and ends in some protein,</li>
 * <li>edge_C - starts in edge_A and ends in some protein,</li>
 * </ul>
 * During resolving how it should be maintained there are two options - either
 * merge it (but in more complex example it's almost impossible to do it
 * properly) or report an errow. This exception will be thrown in such
 * situations.
 * 
 * @author Piotr Gawron
 * 
 */
public class CyclicEdgeException extends ConverterException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor with message passed in the argument.
	 * 
	 * @param string
	 *          message of this exception
	 */
	public CyclicEdgeException(String string) {
		super(string);
	}

}
