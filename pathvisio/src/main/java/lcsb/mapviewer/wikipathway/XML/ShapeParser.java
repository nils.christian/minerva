package lcsb.mapviewer.wikipathway.XML;

import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.wikipathway.model.Shape;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link Shape} objects from Xml {@link Element node}
 * .
 * 
 * @author Piotr Gawron
 *
 */
public class ShapeParser extends GraphicalPathwayElementParser<Shape> {

	/**
	 * Default class logger.
	 */
	private final Logger logger = Logger.getLogger(ShapeParser.class);

	@Override
	public Shape parse(Element eElement) throws ConverterException {
		if (!eElement.getNodeName().equals("Shape")) {
			throw new InvalidArgumentException(ShapeParser.class.getSimpleName() + " can parse only Shape xml nodes");
		}
		Shape shape = new Shape(eElement.getAttribute("GraphId"));
		for (Pair<String, String> entry : getAttributes(eElement)) {
			switch (entry.getLeft()) {
				case ("GraphId"):
					break;
				case ("TextLabel"):
					shape.setTextLabel(entry.getRight());
					break;
				case ("GroupRef"):
					shape.setGroupRef(entry.getRight());
					break;
				default:
					logger.warn("Unknown attribute of " + eElement.getNodeName() + " node: " + entry.getLeft());
					break;
			}
		}

		NodeList tmpList = eElement.getChildNodes();
		for (int j = 0; j < tmpList.getLength(); j++) {
			Node tmpNode = tmpList.item(j);
			if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eTmp = (Element) tmpNode;
				switch (eTmp.getNodeName()) {
					case ("Graphics"):
						parseGraphics(eTmp, shape);
						break;
					case ("Attribute"):
						parseAttribute(eTmp, shape);
						break;
					case ("Comment"):
						shape.addComment(eTmp.getTextContent());
						break;
					case ("BiopaxRef"):
						shape.addBiopaxReference(eTmp.getTextContent());
						break;
					default:
						logger.warn("Unknown sub-node of " + eElement.getNodeName() + " node: " + eTmp.getNodeName());
						break;
				}
			}
		}
		return shape;
	}

	/**
	 * Method that parses {@link Shape} xml attribute.
	 * 
	 * @param eTmp
	 *          xml node with attribute
	 * @param shape
	 *          shape where data should be added
	 */
	private void parseAttribute(Element eTmp, Shape shape) {
		String key = eTmp.getAttribute("Key");
		String value = eTmp.getAttribute("Value");
		switch (key) {
			case ("org.pathvisio.CellularComponentProperty"):
				shape.setCompartment(true);
				break;
			case ("org.pathvisio.DoubleLineProperty"):
				switch (value) {
					case ("Double"):
						shape.setLineType(LineType.DOUBLE);
						break;
					default:
						logger.warn(shape.getWarningPrefix() + "Unknown line type: " + value);
						break;
				}
				break;
			default:
				logger.warn(shape.getWarningPrefix() + "Unknown attribute of node. Key:" + key + "; value: " + value);
				break;
		}
	}

	/**
	 * Parse graphics xml node in the shape node.
	 * 
	 * @param eTmp
	 *          xml node with graphics
	 * @param shape
	 *          shape where data should be added
	 * @throws UnknownTypeException
	 *           thrown when some types in the xml node are unknown
	 */
	private void parseGraphics(Element eTmp, Shape shape) throws UnknownTypeException {
		Double centerX = null;
		Double centerY = null;
		Double width = null;
		Double height = null;
		for (Pair<String, String> entry : getAttributes(eTmp)) {
			if (!parseCommonGraphicAttributes(shape, entry)) {
				switch (entry.getLeft()) {
					case ("CenterX"):
						centerX = Double.valueOf(entry.getRight());
						break;
					case ("CenterY"):
						centerY = Double.valueOf(entry.getRight());
						break;
					case ("Width"):
						width = Double.valueOf(entry.getRight());
						break;
					case ("Height"):
						height = Double.valueOf(entry.getRight());
						break;
					case ("ShapeType"):
						shape.setShape(entry.getRight());
						break;
					case ("Color"):
						shape.setColor(hexStringToColor(entry.getRight()));
						break;
					case ("FillColor"):
						shape.setFillColor(hexStringToColor(entry.getRight()));
						break;
					case ("ZOrder"):
						shape.setzOrder(Integer.valueOf(entry.getRight()));
						break;
					case ("FontSize"):
						shape.setFontSize(Double.valueOf(entry.getRight()));
						break;
					case ("LineThickness"):
						shape.setLineThickness(Double.valueOf(entry.getRight()));
						break;
					case ("Valign"):
						shape.setvAlign(entry.getRight());
						break;
					case ("FontWeight"):
						switch (entry.getRight()) {
							case ("Bold"):
								shape.addFontAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
								break;
							default:
								logger.warn("Unknown value of attribute: " + entry.getLeft() + " - " + entry.getRight());
								break;
						}
						break;
					case ("Rotation"):
						shape.setRotation(Double.valueOf(entry.getRight()));
						break;
					default:
						logger.warn("Unknown attribute of " + eTmp.getNodeName() + " node: " + entry.getLeft());
						break;
				}
			}
		}
		shape.setRectangle(new Rectangle2D.Double(centerX - width / 2, centerY - height / 2, width, height));
	}

	@Override
	public String toXml(Shape node) throws ConverterException {
		throw new NotImplementedException();
	}

	@Override
	public String toXml(Collection<Shape> list) throws ConverterException {
		throw new NotImplementedException();
	}
}
