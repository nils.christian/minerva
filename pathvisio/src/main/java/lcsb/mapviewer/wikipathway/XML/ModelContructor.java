package lcsb.mapviewer.wikipathway.XML;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.reaction.ReactionLineData;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.AssociationOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.SplitOperator;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedNotation;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.wikipathway.model.DataNode;
import lcsb.mapviewer.wikipathway.model.Edge;
import lcsb.mapviewer.wikipathway.model.Graph;
import lcsb.mapviewer.wikipathway.model.GraphicalPathwayElement;
import lcsb.mapviewer.wikipathway.model.Group;
import lcsb.mapviewer.wikipathway.model.Interaction;
import lcsb.mapviewer.wikipathway.model.InteractionMapping;
import lcsb.mapviewer.wikipathway.model.Label;
import lcsb.mapviewer.wikipathway.model.PathwayElement;
import lcsb.mapviewer.wikipathway.model.Shape;
import lcsb.mapviewer.wikipathway.model.State;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxPublication;
import lcsb.mapviewer.wikipathway.utils.Geo;

/**
 * Class contains methods for GraphToModel conversion.
 * 
 * @author Jan Badura
 * 
 */
public class ModelContructor {

  /**
   * CellDesigner util class used for retrieving information about modifier
   * graphics.
   */
  private ModifierTypeUtils mtu = new ModifierTypeUtils();

  /**
   * Default color used by complexes.
   */
  private static final Color DEFAULT_COMPLEX_ALIAS_COLOR = new Color(102, 255, 255);

  /**
   * How much of central line is used by and operator that joins reactants.
   */
  private static final int AND_OPERATOR_CENTRAL_LINE_RATIO = 10;

  /**
   * How much of central line is used by split operator that split products.
   */
  private static final int SPLIT_OPERATOR_CENTRAL_LINE_RATIO = 10;

  /**
   * Epsilon used for double comparison.
   */
  private static final double EPSILON = 1e-6;

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ModelContructor.class);

  /**
   * Parser used for extracting {@link MiriamData references} from GPML model.
   */
  private BiopaxParser biopaxParser = new BiopaxParser();

  /**
   * List of {@link Shape#shape shapes} that are not supported to be part of a
   * {@link Complex complex}.
   */
  private static final Set<String> INALID_COMPLEX_SHAPE_CHILDREN = new HashSet<>();

  static {
    INALID_COMPLEX_SHAPE_CHILDREN.add("Rectangle");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Oval");
    INALID_COMPLEX_SHAPE_CHILDREN.add("RoundedRectangle");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Golgi Apparatus");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Brace");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Sarcoplasmic Reticulum");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Mitochondria");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Endoplasmic Reticulum");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Arc");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Triangle");
    INALID_COMPLEX_SHAPE_CHILDREN.add("Hexagon");
  }

  /**
   * This function splits {@link PolylineData} into two parts. It also assumes
   * that last segment on the left part is equal to the length of the first
   * segment on the right side.
   * 
   * @param pd
   *          line to split
   * @return pair of lines into which parameter was split to
   */
  protected Pair<PolylineData, PolylineData> splitPolyline(PolylineData pd) {
    PolylineData p1 = new PolylineData();
    PolylineData p2 = new PolylineData();
    p1.setColor(pd.getColor());
    p2.setColor(pd.getColor());

    List<Line2D> lines = pd.getLines();
    double lenght = 0.0;
    for (Line2D line : lines) {
      lenght += Geo.lineLen(line);
    }
    if (lenght > Configuration.EPSILON) {
      double tmp = 0.0;
      boolean first = true;
      for (Line2D line : lines) {
        if (tmp + Geo.lineLen(line) > lenght / 2) {
          if (first) {
            double a = (lenght / 2 - tmp) / Geo.lineLen(line);
            double x = line.getX1() + a * (line.getX2() - line.getX1());
            double y = line.getY1() + a * (line.getY2() - line.getY1());
            p1.addPoint((Point2D) line.getP1().clone());
            p1.addPoint(new Point2D.Double(x, y));
            p2.addPoint(new Point2D.Double(x, y));
            first = false;
          }
          p2.addPoint((Point2D) line.getP2().clone());
          tmp += Geo.lineLen(line);
        } else {
          p1.addPoint((Point2D) line.getP1().clone());
          tmp += Geo.lineLen(line);
        }
      }
    } else { // we have line with empty length
      p1.addPoint((Point2D) pd.getBeginPoint().clone());
      p1.addPoint((Point2D) pd.getBeginPoint().clone());
      p2.addPoint((Point2D) pd.getBeginPoint().clone());
      p2.addPoint((Point2D) pd.getBeginPoint().clone());
    }

    // and now make sure that we have equal distances from left and right
    double leftDist = p1.getEndPoint().distance(p1.getPoints().get(p1.getPoints().size() - 2));
    double rightDist = p2.getEndPoint().distance(p2.getPoints().get(p2.getPoints().size() - 2));
    if (Math.abs(leftDist - rightDist) > Configuration.EPSILON) {
      if (leftDist > rightDist) {
        double ratio = rightDist / leftDist;
        double dx = p1.getPoints().get(p1.getPoints().size() - 1).getX()
            - p1.getPoints().get(p1.getPoints().size() - 2).getX();
        double dy = p1.getPoints().get(p1.getPoints().size() - 1).getY()
            - p1.getPoints().get(p1.getPoints().size() - 2).getY();
        double x = p1.getPoints().get(p1.getPoints().size() - 2).getX() + dx * ratio;
        double y = p1.getPoints().get(p1.getPoints().size() - 2).getY() + dy * ratio;
        Point2D newPoint = new Point2D.Double(x, y);
        p1.addPoint(p1.getPoints().size() - 1, newPoint);
      } else {
        double ratio = leftDist / rightDist;
        double dx = p2.getPoints().get(0).getX() - p2.getPoints().get(1).getX();
        double dy = p2.getPoints().get(0).getY() - p2.getPoints().get(1).getY();
        double x = p2.getPoints().get(1).getX() + dx * ratio;
        double y = p2.getPoints().get(1).getY() + dy * ratio;
        Point2D newPoint = new Point2D.Double(x, y);
        p2.addPoint(1, newPoint);
      }
    }
    return new Pair<PolylineData, PolylineData>(p1, p2);
  }

  /**
   * Support class to send less parameters in functions.
   * 
   * @author Jan Badura
   */
  private final class Data {
    /**
     * Map between graphId and aliases created from gpml elements.
     */
    private Map<String, Element> id2alias;

    /**
     * Default layer.
     */
    private Layer layer;

    /**
     * Default constructor.
     */
    private Data() {
      id2alias = new HashMap<String, Element>();
      layer = new Layer();
      layer.setVisible(true);
      layer.setLayerId("1");
      layer.setName("defaultLayer");
    }
  }

  /**
   * This function creates Species from DataNode.
   * 
   * @param dataNode
   *          object from which species is created
   * @param data
   *          ...
   * @return {@link Species} created from input {@link DataNode}
   */
  protected Species createAlias(DataNode dataNode, Data data) {
    Species res = null;
    String type = dataNode.getType();
    if (type == null || type.equals("")) {
      type = null;
    }

    if (type != null && type.equalsIgnoreCase("Metabolite")) {
      res = new SimpleMolecule(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("GeneProduct")) {
      res = new Gene(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Pathway")) {
      res = new Phenotype(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Rna")) {
      res = new Rna(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Protein")) {
      res = new GenericProtein(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Complex")) {
      res = new Complex(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("None")) {
      res = new Unknown(dataNode.getGraphId());
    } else {
      logger.warn("[" + dataNode.getGraphId() + "]\tUnknown species type: " + type + ". Using Unknown");
      res = new Unknown(dataNode.getGraphId());
    }

    res.addMiriamData(dataNode.getReferences());
    res.setName(dataNode.getName());
    return res;
  }

  /**
   * This function adds ComplexSpecies to model from graph. ComplexName is set as
   * groupId from .gpml
   * 
   * @param model
   *          to this model complexes will be added
   * @param graph
   *          gpml data model
   * @param data
   *          ...
   */
  protected void addComplexSpecies(Model model, Graph graph, Data data) {
    for (Group group : graph.getGroups()) {
      Complex alias = new Complex(group.getGraphId());
      alias.setName(group.getGraphId());
      if ("Complex".equalsIgnoreCase(group.getStyle())) {
        alias.setHypothetical(false);
      } else {
        alias.setHypothetical(true);
      }

      Rectangle2D rec = group.getRectangle();
      if (rec == null) {
        rec = new Rectangle2D.Double(0, 0, 0, 0);
      }
      alias.setX(rec.getX());
      alias.setY(rec.getY());
      alias.setWidth((int) rec.getWidth());
      alias.setHeight((int) rec.getHeight());
      alias.setColor(DEFAULT_COMPLEX_ALIAS_COLOR);

      data.id2alias.put(group.getGraphId(), alias);
      model.addElement(alias);
    }
  }

  /**
   * This function adds Species, TextLabels, Compartments and Shapes from graph.
   * 
   * @param model
   *          model to which species will be added
   * @param graph
   *          gpml data model
   * @param data
   *          ...
   */
  protected void addElement(Model model, Graph graph, Data data) {
    for (DataNode dataNode : graph.getDataNodes()) {
      Species species = createAlias(dataNode, data);
      species.addMiriamData(biopaxParser.getMiriamData(graph.getBiopaxData(), dataNode.getBiopaxReference()));

      Element alias = updateAlias(dataNode, species);

      data.id2alias.put(dataNode.getGraphId(), alias);
      model.addElement(alias);
    }
    for (Label label : graph.getLabels()) {
      if (label.isTreatAsNode()) {
        Species species = createSpecies(label);
        species.addMiriamData(biopaxParser.getMiriamData(graph.getBiopaxData(), label.getBiopaxReference()));

        Element alias = updateAlias(label, species);

        data.id2alias.put(label.getGraphId(), alias);
        model.addElement(alias);
      } else {
        LayerText text = createText(label);
        data.layer.addLayerText(text);
      }
    }
    for (Shape shape : graph.getShapes()) {
      if (shape.isTreatAsNode()) {
        Species species = createSpecies(shape);
        species.addMiriamData(biopaxParser.getMiriamData(graph.getBiopaxData(), shape.getBiopaxReference()));

        Element alias = updateAlias(shape, species);

        data.id2alias.put(shape.getGraphId(), alias);
        model.addElement(alias);

      } else {
        if (!shape.isCompartment()) {
          if (shape.getShape().equalsIgnoreCase("oval")) {
            LayerOval oval = createOval(shape);
            data.layer.addLayerOval(oval);
          } else {
            LayerRect rect = createRectangle(shape);
            data.layer.addLayerRect(rect);
          }
        } else {
          Compartment compartment = new Compartment(shape.getGraphId());
          Element cmpAl = updateAlias(shape, compartment);
          model.addElement(cmpAl);
        }
      }
    }

    for (State state : graph.getStates()) {
      // TODO this might not work
      Element species = data.id2alias.get(state.getGraphRef());
      if (state.getType() != null) {
        AbstractSiteModification mr = null;

        if (species instanceof Protein) {
          mr = new Residue();
          ((Protein) species).addModificationResidue(mr);
        } else if (species instanceof Gene) {
          mr = new ModificationSite();
          ((Gene) species).addModificationResidue(mr);
        } else {
          logger.warn(
              state.getWarningPrefix() + "state for " + species.getClass().getSimpleName() + " is not supported.");
        }
        if (mr != null) {
          mr.setIdModificationResidue(state.getGraphId());
          mr.setState(state.getType());

          Double x = state.getRelX() * species.getWidth() / 2 + species.getCenterX();
          Double y = state.getRelY() * species.getHeight() / 2 + species.getCenterY();
          mr.setPosition(new Point2D.Double(x, y));
          adjustModificationCoordinates(species);
        }
      } else if (state.getStructuralState() != null) {
        if (species instanceof Protein) {
          Protein protein = ((Protein) species);
          if (protein.getStructuralState() == null) {
            protein.setStructuralState(state.getStructuralState());
          } else {
            logger.warn(state.getWarningPrefix() + " tries to override another state: " + protein.getStructuralState());
          }
        } else {
          logger.warn(state.getWarningPrefix() + "structural state for " + species.getClass().getSimpleName()
              + " is not supported.");
        }
      }
    }
  }

  /**
   * {@link ModificationResidue} in element might have slightly off coordinates
   * (due to different symbol shapes). For that we need to align them to match our
   * model.
   * 
   * @param species
   */
  protected void adjustModificationCoordinates(Element species) {
    if (species instanceof Protein) {
      Protein protein = (Protein) species;
      if (protein.getModificationResidues().size() > 0) {
        CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, true);
        for (ModificationResidue mr : protein.getModificationResidues()) {
          double angle = converter.getAngleForPoint(species, mr.getPosition());
          mr.setPosition(converter.getResidueCoordinates(species, angle));
        }
      }
    } else if (species instanceof Gene) {
      Gene gene = (Gene) species;
      if (gene.getModificationResidues().size() > 0) {
        CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, true);
        for (ModificationResidue mr : gene.getModificationResidues()) {
          double angle = converter.getAngleForPoint(species, mr.getPosition());
          mr.setPosition(converter.getResidueCoordinates(species, angle));
        }
      }

    }
  }

  /**
   * Creates {@link LayerRect} object from {@link Shape}.
   * 
   * @param shape
   *          source gpml object to be transformed
   * @return {@link LayerRect} obtained from {@link Shape} object
   */
  public LayerRect createRectangle(Shape shape) {
    LayerRect rect = new LayerRect();
    Rectangle2D rec = shape.getRectangle();
    rect.setX(rec.getX());
    rect.setY(rec.getY());
    rect.setHeight(rec.getHeight());
    rect.setWidth(rec.getWidth());
    rect.setColor(shape.getColor());
    return rect;
  }

  /**
   * Creates {@link LayerOval} object from {@link Shape}.
   * 
   * @param shape
   *          source gpml object to be transformed
   * @return {@link LayerOval} obtained from {@link Shape} object
   */
  public LayerOval createOval(Shape shape) {
    LayerOval oval = new LayerOval();
    Rectangle2D rec = shape.getRectangle();
    oval.setX(rec.getX());
    oval.setY(rec.getY());
    oval.setHeight(rec.getHeight());
    oval.setWidth(rec.getWidth());
    oval.setColor(shape.getColor());
    return oval;
  }

  /**
   * Creates {@link LayerText} from {@link Label}.
   * 
   * @param label
   *          object from which result will be created
   * @return {@link LayerText} from {@link Label}
   */
  public LayerText createText(Label label) {
    Rectangle2D rec = label.getRectangle();

    LayerText text = new LayerText();
    text.setX(rec.getX());
    text.setY(rec.getY());
    text.setHeight(rec.getHeight());
    text.setWidth(rec.getWidth());
    text.setColor(label.getColor());
    if (text.getColor().equals(Color.WHITE)) {
      text.setColor(Color.BLACK);
    }
    text.setNotes(label.getName());
    return text;
  }

  /**
   * Creates alias for {@link GraphicalPathwayElement}. Type of the alias is
   * defined by the parameter {@link Species}
   * 
   * @param gpmlElement
   *          object from which alias will be create
   * @param alias
   *          specie for which alias will be created
   * @return {@link Species} created from input {@link Label}
   */
  public Element updateAlias(GraphicalPathwayElement gpmlElement, Element alias) {
    if (alias instanceof Compartment) {
      if (((Shape) gpmlElement).getShape().equalsIgnoreCase("oval")) {
        alias = new OvalCompartment((Compartment) alias);
      } else {
        alias = new SquareCompartment((Compartment) alias);
      }
      String name = ((Shape) gpmlElement).getTextLabel();
      if (name == null) {
        name = "";
      }
      alias.setName(name);
    }

    Rectangle2D rec = gpmlElement.getRectangle();
    alias.setX(rec.getX());
    alias.setY(rec.getY());
    alias.setWidth((int) rec.getWidth());
    alias.setHeight((int) rec.getHeight());
    alias.setColor(gpmlElement.getFillColor());
    return alias;
  }

  /**
   * Creates {@link Unknown species} from {@link Label}.
   * 
   * @param label
   *          original label from which output should be created
   * @return {@link Unknown} object created from input {@link Label}
   */
  private Species createSpecies(Label label) {
    logger.warn(label.getWarningPrefix() + " Label cannot be part of reaction. Tranforming to Unknown");

    Species res = new Unknown(label.getGraphId());
    res.setName(label.getName());
    return res;
  }

  /**
   * Creates {@link Unknown species} from {@link Shape}.
   * 
   * @param shape
   *          original label from which output should be created
   * @return {@link Unknown} object created from input {@link Label}
   */
  private Species createSpecies(Shape shape) {
    logger.warn(shape.getWarningPrefix() + " Shape can not be part of reaction. Tranforming to Unknown");

    Species res = new Unknown(shape.getGraphId());
    res.setName(shape.getName());
    return res;
  }

  /**
   * This function add Species to right Complexes.
   * 
   * @param graph
   *          gpml data model
   * @param data
   *          ...
   * @throws UnknownChildClassException
   *           thrown when complex contain invalid child
   */
  protected void putSpeciesIntoComplexes(Graph graph, Data data) throws UnknownChildClassException {
    for (Group group : graph.getGroups()) {

      Complex complexSpecies = (Complex) data.id2alias.get(group.getGraphId());
      for (PathwayElement pe : group.getNodes()) {
        Element species = data.id2alias.get(pe.getGraphId());
        if (species != null) {
          if (species instanceof Species) {

            Species speciesAlias = (Species) species;
            speciesAlias.setComplex(complexSpecies);
            complexSpecies.addSpecies(speciesAlias);
          }
          // we have label
        } else if (graph.getLabelByGraphId(pe.getGraphId()) != null) {
          Label label = graph.getLabelByGraphId(pe.getGraphId());
          // if complex has generic name then change it into label
          if (complexSpecies.getName().equals(group.getGraphId())) {
            complexSpecies.setName(label.getTextLabel());
          } else {
            // if there are more labels than one then merge labels
            complexSpecies.setName(complexSpecies.getName() + "\n" + label.getTextLabel());
          }
        } else if (graph.getShapeByGraphId(pe.getGraphId()) != null) {
          Shape shape = graph.getShapeByGraphId(pe.getGraphId());
          if (INALID_COMPLEX_SHAPE_CHILDREN.contains(shape.getShape())) {
            logger.warn(shape.getWarningPrefix() + shape.getShape() + " cannot be part of a complex. Skipping.");
          } else {
            throw new UnknownChildClassException("Unknown class type with id \"" + pe.getGraphId() + "\": "
                + shape.getShape() + ". Group id: " + group.getGraphId());
          }
        } else {
          throw new UnknownChildClassException(
              "Cannot find child with id: " + pe.getGraphId() + ". Group id: " + group.getGraphId());
        }
      }
    }
  }

  /**
   * This function creates {@link Reaction} from {@link Interaction} from graph.
   * 
   * @param interaction
   *          gpml interaction
   * @param graph
   *          gpml data model
   * @param data
   *          ...
   * @return reaction created from gpml {@link Interaction}
   */
  protected Reaction createReactionFromInteraction(Interaction interaction, Graph graph, Data data) {
    InteractionMapping mapping = InteractionMapping.getInteractionMapping(interaction.getType(),
        interaction.getLine().getType());
    if (mapping == null) {
      throw new InvalidArgumentException("Unknown interaction type: " + interaction.getType());
    }
    Reaction reaction = createReactionByType(mapping.getModelReactionType());
    reaction.setIdReaction(interaction.getGraphId());
    reaction.setReversible(mapping.isReversible());

    reaction.addMiriamData(interaction.getReferences());
    for (String ref : interaction.getBiopaxReferences()) {
      BiopaxPublication publication = graph.getBiopaxData().getPublicationByReference(ref);
      MiriamData md = biopaxParser.createMiriamData(publication);
      if (md != null) {
        reaction.addMiriamData(md);
      }
    }

    PolylineData pd = interaction.getLine();

    Pair<PolylineData, PolylineData> pair = splitPolyline(pd);
    PolylineData pdfirstpart = pair.getLeft();
    PolylineData pdsecondpart = pair.getRight();

    Reactant reactant = new Reactant();
    reactant.setElement(data.id2alias.get(interaction.getStart()));
    reactant.setLine(pdfirstpart);
    reaction.addReactant(reactant);

    Product product = new Product();
    product.setElement(data.id2alias.get(interaction.getEnd()));
    product.setLine(pdsecondpart);
    reaction.addProduct(product);

    for (Edge e : interaction.getReactants()) {
      Reactant reac = createReactantFromEdge(data, e);
      reaction.addReactant(reac);
    }

    for (Edge e : interaction.getProducts()) {
      Product pro = createProductFromEdge(data, e);
      reaction.addProduct(pro);
    }

    double centralLength = pdfirstpart.getPoints().get(pdfirstpart.getPoints().size() - 2)
        .distance(pdfirstpart.getEndPoint()) * 2;

    if (reaction.getReactants().size() > 1) {
      PolylineData operatorLine = new PolylineData();
      operatorLine.setColor(pdfirstpart.getColor());
      operatorLine.addPoint((Point2D) pdfirstpart.getEndPoint().clone());
      operatorLine.addPoint((Point2D) pdfirstpart.getEndPoint().clone());
      pdfirstpart.trimEnd(centralLength / AND_OPERATOR_CENTRAL_LINE_RATIO);
      operatorLine.setStartPoint((Point2D) pdfirstpart.getEndPoint().clone());
      NodeOperator andOperator;
      // heterodimer association use association operator
      if (reaction instanceof HeterodimerAssociationReaction) {
        andOperator = new AssociationOperator();
        // all other reaction just force and operator between inputs
      } else {
        andOperator = new AndOperator();
      }
      andOperator.addInput(reactant);
      andOperator.setLine(operatorLine);
      for (int i = 1; i < reaction.getReactants().size(); i++) {
        Reactant r = reaction.getReactants().get(i);
        r.getLine().addPoint((Point2D) pdfirstpart.getEndPoint().clone());
        andOperator.addInput(r);
      }
      reaction.addNode(andOperator);
    }

    if (reaction.getProducts().size() > 1) {
      PolylineData operatorLine = new PolylineData();
      operatorLine.setColor(pdsecondpart.getColor());

      operatorLine.addPoint((Point2D) pdsecondpart.getBeginPoint().clone());
      pdsecondpart.trimBegin(centralLength / SPLIT_OPERATOR_CENTRAL_LINE_RATIO);
      operatorLine.addPoint((Point2D) pdsecondpart.getBeginPoint().clone());
      SplitOperator splitOperator = new SplitOperator();
      splitOperator.addOutput(product);
      splitOperator.setLine(operatorLine);
      for (int i = 1; i < reaction.getProducts().size(); i++) {
        Product r = reaction.getProducts().get(i);
        r.getLine().addPoint(0, (Point2D) pdsecondpart.getBeginPoint().clone());
        splitOperator.addOutput(r);
      }
      reaction.addNode(splitOperator);
    }

    for (Edge e : interaction.getModifiers()) {
      Modifier mod = createModifierFromEdge(data, e);
      reaction.addModifier(mod);
    }
    if (reaction instanceof TwoReactantReactionInterface && reaction.getReactants().size() < 2) {
      logger.warn("Reaction should contain at least 2 reactants. GraphId: " + interaction.getGraphId());
      reaction = new UnknownTransitionReaction(reaction);
    }
    if (reaction instanceof ReducedNotation && ((reaction.getReactants().size() > 1)
        || reaction.getProducts().size() > 1 || reaction.getModifiers().size() > 0)) {
      logger.warn("Reaction should contain only one reactant and one product. GraphId: " + interaction.getGraphId());
      reaction = new UnknownTransitionReaction(reaction);
    }
    ReactionLineData rld = ReactionLineData.getByReactionType(reaction.getClass());
    if (rld == null) {
      throw new InvalidArgumentException("Don't know how to process reaction type: " + reaction.getClass());
    }
    for (AbstractNode node : reaction.getNodes()) {
      if (!(node instanceof Modifier)) {
        node.getLine().setType(rld.getLineType());
      }
    }
    for (Product p : reaction.getProducts()) {
      p.getLine().getEndAtd().setArrowType(rld.getProductArrowType());
      if (rld.getProductLineTrim() > 0) {
        p.getLine().trimEnd(rld.getProductLineTrim());
      }

    }
    if (reaction.isReversible()) {
      for (Reactant r : reaction.getReactants()) {
        r.getLine().getBeginAtd().setArrowType(rld.getProductArrowType());
        if (rld.getProductLineTrim() > 0) {
          r.getLine().trimBegin(rld.getProductLineTrim());
        }
      }
    }

    ModifierTypeUtils mtu = new ModifierTypeUtils();
    for (Modifier m : reaction.getModifiers()) {
      m.getLine().setEndPoint(m.getLine().getPoints().get(m.getLine().getPoints().size() - 2));
      m.getLine().setEndPoint(mtu.getAnchorPointOnReactionRect(reaction, mtu.getTargetLineIndexByModifier(m)));
    }

    return reaction;
  }

  /**
   * Creates {@link Reactant} from gpml edge.
   * 
   * @param data
   *          ...
   * @param e
   *          edge
   * @return {@link Reactant} from gpml edge
   */
  protected Reactant createReactantFromEdge(Data data, Edge e) {
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This reactant is invalid");
    }

    Reactant reac = new Reactant();
    reac.setElement(data.id2alias.get(id));
    reac.setLine(e.getLine());

    // if somebody drew the reaction in reverse order (starting from reaction
    // and going to the product) then reverse the order of points: order should
    // be from reactant to the product
    if (id.equals(e.getEnd())) {
      reac.setLine(reac.getLine().reverse());
    }
    return reac;
  }

  /**
   * Creates {@link Product} from gpml edge.
   * 
   * @param data
   *          ...
   * @param e
   *          edge
   * @return {@link Product} from gpml edge
   */
  protected Product createProductFromEdge(Data data, Edge e) {
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This product is invalid");
    }
    Product pro = new Product();
    pro.setElement(data.id2alias.get(id));
    pro.setLine(e.getLine());
    // if somebody drew the reaction in reverse order (starting from reaction
    // and going to the product) then reverse the order of points: order should
    // be from reactant to the product
    if (id.equals(e.getStart())) {
      pro.setLine(pro.getLine().reverse());
    }
    return pro;
  }

  /**
   * Creates {@link Modifier} from gpml edge.
   * 
   * @param data
   *          ...
   * @param e
   *          edge
   * @return {@link Modifier} from gpml edge
   */
  protected Modifier createModifierFromEdge(Data data, Edge e) {
    InteractionMapping modifierMapping = InteractionMapping.getInteractionMapping(e.getType(), e.getLine().getType());
    if (modifierMapping == null) {
      throw new InvalidArgumentException("Unknown interaction type: " + e.getType());
    }

    Class<? extends ReactionNode> modifierType = null;
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      modifierType = modifierMapping.getModelInputReactionNodeType();
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      modifierType = modifierMapping.getModelOutputReactionNodeType();
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This modifier is invalid");
    }

    Modifier mod = createModifierByType(modifierType, (Species) data.id2alias.get(id));

    ModifierType mt = mtu.getModifierTypeForClazz(mod.getClass());

    e.getLine().setEndAtd(mt.getAtd());
    e.getLine().setType(mt.getLineType());
    mod.setLine(e.getLine());
    return mod;
  }

  /**
   * Creates {@link Reaction} for a given class type.
   * 
   * @param reactionType
   *          type of reaction to create
   * @return new instance of the reactionType
   */
  private Reaction createReactionByType(Class<? extends Reaction> reactionType) {
    try {
      Reaction result = reactionType.getConstructor().newInstance();
      return result;
    } catch (Exception e) {
      throw new InvalidArgumentException("Cannot create reaction.", e);
    }
  }

  /**
   * Creates {@link Modifier} for a given class type.
   * 
   * @param modifierType
   *          type of modifier in reaction to create
   * @param alias
   *          {@link Species alias } to which modifier is attached
   * @return new instance of the modifierType
   */
  private Modifier createModifierByType(Class<? extends ReactionNode> modifierType, Species alias) {
    try {
      Modifier result = (Modifier) modifierType.getConstructor(Element.class).newInstance(alias);
      return result;
    } catch (Exception e) {
      throw new InvalidArgumentException("Cannot create modifier.", e);
    }
  }

  /**
   * This function creates {@link Model} from {@link Graph}.
   * 
   * @param graph
   *          object with data obtained from gpml
   * @return {@link Model} object representing gpml
   * @throws ConverterException
   *           exception thrown when conversion from graph couldn't be performed
   * 
   */
  public Model getModel(Graph graph) throws ConverterException {
    try {
      Data data = new Data();
      Model model = new ModelFullIndexed(null);
      model.setWidth(graph.getBoardWidth());
      model.setHeight(graph.getBoardHeight());

      model.setIdModel("1");
      model.getLayers().add(data.layer);

      addComplexSpecies(model, graph, data);
      addElement(model, graph, data);
      putSpeciesIntoComplexes(graph, data);

      for (Interaction interaction : graph.getInteractions()) {
        Reaction reaction = createReactionFromInteraction(interaction, graph, data);
        model.addReaction(reaction);
      }

      removeEmptyComplexes(model);

      fixCompartmentAliases(model);

      StringBuilder tmp = new StringBuilder();
      for (String string : graph.getComments()) {
        tmp.append(string + "\n");
      }
      String notes = tmp.toString();
      boolean different = false;
      do {
        String newNotes = StringEscapeUtils.unescapeHtml4(notes);
        different = newNotes.compareTo(notes) != 0;
        notes = newNotes;
      } while (different);
      if (notes != null) {
        notes = StringEscapeUtils.unescapeHtml4(notes);
      }
      model.setNotes(notes);
      StringBuilder references = new StringBuilder("");
      for (String string : graph.getBiopaxReferences()) {
        references.append(string + ", ");
      }
      if (references.length() > 0) {
        logger.warn("[Biopax, " + references.toString() + "]\tModel annotations are not supported.");
      }

      assignNamesToComplexes(model);
      assignNamesToCompartments(model);

      data.layer.addLayerLines(createLines(graph));

      putAliasesIntoCompartments(model);

      return model;
    } catch (UnknownChildClassException e) {
      throw new ConverterException(e);
    }
  }

  /**
   * Method that put {@link Species aliases} that are not assigned into any
   * {@link Compartment} into a proper compartment.
   * 
   * @param model
   *          model where aliases will be modifed
   */
  private void putAliasesIntoCompartments(Model model) {
    for (Element alias : model.getElements()) {
      if (alias.getCompartment() == null) {
        if (alias instanceof Species) {
          Compartment selectedAlias = null;
          for (Compartment cAlias : model.getCompartments()) {
            if (cAlias.cross(alias)) {
              if (selectedAlias == null) {
                selectedAlias = cAlias;
              } else if (selectedAlias.getSize() > cAlias.getSize()) {
                selectedAlias = cAlias;
              }
            }
          }
          if (selectedAlias != null) {
            selectedAlias.addElement(alias);
          }
        }
      }
    }
  }

  /**
   * Removes empty complexes (with size 0) from model.
   * 
   * @param model
   *          model where operation is performed
   */
  private void removeEmptyComplexes(Model model) {
    Set<Element> aliasesInReaction = new HashSet<>();
    for (Reaction reaction : model.getReactions()) {
      for (ReactionNode node : reaction.getReactionNodes()) {
        aliasesInReaction.add(node.getElement());
      }
    }
    List<Element> toRemove = new ArrayList<>();
    ElementUtils eu = new ElementUtils();
    for (Element alias : model.getElements()) {
      if (alias instanceof Complex) {
        Complex cAlias = (Complex) alias;
        if (cAlias.getSize() <= EPSILON && cAlias.getAllChildren().size() == 0) {
          if (aliasesInReaction.contains(alias)) {
            logger.warn(eu.getElementTag(alias) + "Empty element is invalid, but it's a part of reaction.");
          } else {
            toRemove.add(alias);
            logger.warn(eu.getElementTag(alias) + "Empty element is invalid");
          }
        }
      }
    }
    for (Element alias : toRemove) {
      model.removeElement(alias);
    }
  }

  /**
   * Tries to find a name to assign to complexes if complexes don't contain them.
   * 
   * @param model
   *          model where complexes are placed
   */
  private void assignNamesToComplexes(Model model) {
    for (Element alias : model.getElements()) {
      if (alias instanceof Complex) {
        if (alias.getName() == null || (alias.getName().isEmpty())) {
          for (Layer layer : model.getLayers()) {
            LayerText toRemove = null;
            for (LayerText lt : layer.getTexts()) {
              if (alias.contains(lt)) {
                alias.setName(lt.getNotes());
                toRemove = lt;
                break;
              }
            }
            if (toRemove != null) {
              layer.removeLayerText(toRemove);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Tries to find a name to assign to compartments if comparments don't contain
   * them.
   * 
   * @param model
   *          model where compartments are placed
   */
  private void assignNamesToCompartments(Model model) {
    for (Element alias : model.getElements()) {
      if (alias instanceof Compartment) {
        if (alias.getName() == null || alias.getName().isEmpty()) {
          for (Layer layer : model.getLayers()) {
            LayerText toRemove = null;
            for (LayerText lt : layer.getTexts()) {
              if (alias.contains(lt)) {
                alias.setName(lt.getNotes());
                toRemove = lt;
                break;
              }
            }
            if (toRemove != null) {
              layer.removeLayerText(toRemove);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Creates list of {@link LayerLine} in the model from gpml model.
   * 
   * @param graph
   *          gpml model
   * @return list of {@link LayerLine}
   */
  private Collection<PolylineData> createLines(Graph graph) {
    List<PolylineData> result = new ArrayList<PolylineData>();
    for (PolylineData pd : graph.getLines()) {
      result.add(pd);
    }
    return result;
  }

  /**
   * By default gpml doesn't offer information about compartments structure. This
   * function fixes assignments to compartment and compartment aliases.
   * 
   * @param model
   *          model where assignments are fixed.
   */
  private void fixCompartmentAliases(Model model) {
    List<Compartment> aliases = model.getCompartments();
    // clear all assignments
    for (Compartment compartmentAlias : aliases) {
      compartmentAlias.getElements().clear();
    }

    for (Element alias : model.getElements()) {
      // elements inside complexes shouldn't be considered
      if (alias instanceof Species) {
        if (((Species) alias).getComplex() != null) {
          continue;
        }
      }
      Compartment parentAlias = null;
      for (Compartment compartmentAlias : aliases) {
        if (compartmentAlias.contains(alias)) {
          if (parentAlias == null) {
            parentAlias = compartmentAlias;
          } else if (parentAlias.getSize() > compartmentAlias.getSize()) {
            parentAlias = compartmentAlias;
          }
        }
      }
      if (parentAlias != null) {
        parentAlias.addElement(alias);
        alias.setCompartment(parentAlias);
      }
    }
  }
}
