<?xml version="1.0" encoding="UTF-8"?>
<Pathway xmlns="http://pathvisio.org/GPML/2013a" Name="ACE Inhibitor Pathway" Organism="Homo sapiens" License="CC BY 2.0">
  <Comment Source="WikiPathways-category">Physiological Process</Comment>
  <Comment Source="WikiPathways-description">The core of this pathway was elucidated over a century ago and involves the conversion of angiotensinogen to angiotensin I (Ang I) by renin, its subsequent conversion to angiotensin II (Ang II) by angiotensin converting enzyme. Ang II activates the angiotensin II receptor type 1 to induce aldosterone synthesis, increasing water and salt resorption and potassium excretion in the kidney and increasing blood pressure.

Source: PharmGKB (http://www.pharmgkb.org/do/serve?objId=PA2023&amp;objCls=Pathway)</Comment>
  <BiopaxRef>b93</BiopaxRef>
  <Graphics BoardWidth="850.0" BoardHeight="503.66666666666663" />
  <DataNode TextLabel="Start&#xA;" GraphId="d9620" Type="Metabolite">
    <Attribute Key="org.pathvisio.model.BackpageHead" Value="ACE Inhibitor" />
    <Graphics CenterX="505.5" CenterY="369.66666666666663" Width="91.0" Height="20.0" ZOrder="32768" FontSize="10" Valign="Middle" Color="cc0000" />
    <Xref Database="ChEBI" ID="CHEBI:35457" />
  </DataNode>
  <DataNode TextLabel="Stop&#xA;" GraphId="d046f" Type="GeneProduct">
    <Attribute Key="org.pathvisio.model.BackpageHead" Value="ACE | angiotensin I converting enzyme (peptidyl..." />
    <Graphics CenterX="360.0" CenterY="463.66666666666663" Width="60.0" Height="20.0" ZOrder="32770" FontSize="10" Valign="Middle" />
    <Xref Database="Entrez Gene" ID="1636" />
  </DataNode>
  <Interaction GraphId="d5bc3">
    <Graphics ConnectorType="Elbow" ZOrder="32771" LineThickness="1.0" Color="ff0000">
      <Point X="460.0" Y="369.66666666666663" GraphRef="d9620" RelX="-1.0" RelY="0.0" />
      <Point X="345.0" Y="453.66666666666663" GraphRef="d046f" RelX="-0.5" RelY="-1.0" ArrowHead="mim-inhibition" />
    </Graphics>
    <Xref Database="" ID="" />
  </Interaction>
  <InfoBox CenterX="1.6666666666666667" CenterY="38.333333333333336" />
  <Biopax>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="b40">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">17376010</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Angiotensin II reactivation and aldosterone escape phenomena in renin-angiotensin-aldosterone system blockade: is oral renin inhibition the solution?</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Expert Opin Pharmacother</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2007</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Athyros VG</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Mikhailidis DP</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kakafika AI</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Tziomalos K</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Karagiannis A</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:openControlledVocabulary xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#">
      <bp:TERM xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ACE inhibitor drug pathway</bp:TERM>
      <bp:ID xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PW:0001228</bp:ID>
      <bp:Ontology xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Pathway Ontology</bp:Ontology>
    </bp:openControlledVocabulary>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="b5d">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">18729003</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Mineralocorticoid receptor antagonists and endothelial function.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Curr Opin Investig Drugs</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2008</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Maron BA</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Leopold JA</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="eeb">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">16679330</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Systematic review and meta-analysis of ethnic differences in risks of adverse reactions to drugs used in cardiovascular medicine.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">BMJ</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2006</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">McDowell SE</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Coleman JJ</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Ferner RE</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="b93">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">17464936</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">The emerging role of ACE2 in physiology and disease.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">J Pathol</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2007</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Hamming I</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Cooper ME</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Haagmans BL</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Hooper NM</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Korstanje R</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Osterhaus AD</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Timens W</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Turner AJ</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Navis G</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">van Goor H</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:openControlledVocabulary xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#">
      <bp:TERM xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">angiotensin signaling pathway</bp:TERM>
      <bp:ID xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PW:0000245</bp:ID>
      <bp:Ontology xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Pathway Ontology</bp:Ontology>
    </bp:openControlledVocabulary>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="bd1">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">15174896</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Pharmacogenetics of antihypertensive drug responses.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Am J Pharmacogenomics</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2004</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Schwartz GL</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Turner ST</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="ce3">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">15134803</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">The regulation of aldosterone synthase expression.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Mol Cell Endocrinol</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2004</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Bassett MH</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">White PC</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Rainey WE</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="af8">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">16374430</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Renin increases mesangial cell transforming growth factor-beta1 and matrix proteins through receptor-mediated, angiotensin II-independent mechanisms.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kidney Int</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2006</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Huang Y</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Wongamorntham S</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kasting J</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">McQuillan D</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Owens RT</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Yu L</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Noble NA</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Border W</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="bc9">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">18035185</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Effects of renin-angiotensin system inhibition on end-organ protection: can we do better?</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Clin Ther</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2007</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Weir MR</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="c74">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">18449520</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Are we poised to target ACE2 for the next generation of antihypertensives?</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">J Mol Med</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2008</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Ferreira AJ</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Raizada MK</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="c16">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">18417113</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">The (pro)renin receptor: a new addition to the renin-angiotensin system?</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Eur J Pharmacol</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2008</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Batenburg WW</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Jan Danser AH</bp:AUTHORS>
    </bp:PublicationXref>
  </Biopax>
</Pathway>

