package lcsb.mapviewer.reactome;

import lcsb.mapviewer.reactome.utils.AllReactomeUtilsTests;
import lcsb.mapviewer.reactome.xml.AllReactomeXmlTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AllReactomeUtilsTests.class,//
		AllReactomeXmlTests.class,//
})
public class AllReactomeTests {

}
