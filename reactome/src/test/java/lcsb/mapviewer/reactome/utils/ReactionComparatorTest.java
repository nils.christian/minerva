package lcsb.mapviewer.reactome.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult.MatchStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ReactionComparatorTest extends ReactomeTestFunctions {
	Logger logger = Logger.getLogger(ReactionComparatorTest.class);

	@Autowired
	ReactionComparator comparator;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore("Something changed in reactome")
	public void testComparisonReaction1152() {
		try {
			Model model = getModelForFile("testFiles/pd_full/PD_130712_3.xml",true);
			Reaction reaction = model.getReactionByReactionId("re1152");
			
			ReactomeReactionlikeEvent reactomeReaction =(ReactomeReactionlikeEvent) rc.getFullObjectForDbId(109339);

			//this test doens't pass beacause something went wrong in reactome db
			
			assertEquals(MatchStatus.OK,comparator.compareReactions(reaction, reactomeReaction).getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception occured: " + e.getMessage());
		}
	}


}
