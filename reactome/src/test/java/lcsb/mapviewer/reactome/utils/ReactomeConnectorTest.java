package lcsb.mapviewer.reactome.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeInstanceEdit;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomeReaction;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

public class ReactomeConnectorTest extends ReactomeTestFunctions {
	Logger												logger	= Logger.getLogger(ReactomeConnectorTest.class);

	CacheType											reactomeCacheType;

	@Autowired
	private GeneralCacheInterface	cache;

	@Before
	public void setUp() throws Exception {
		reactomeCacheType = rc.getCacheType();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetEntities() throws Exception {
		try {
			List<ReactomePhysicalEntity> list = rc.getEntitiesForName("GAK", true);
			assertNotNull(list);
			assertTrue("Too few elements on the list: " + list.size(), list.size() > 1);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetReactions() throws Exception {
		try {
			List<ReactomeReactionlikeEvent> list = rc.getReactionsForEntityId(162704);
			assertNotNull(list);
			assertTrue("Too few elements on the list: " + list.size(), list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetNodeForId() {
		try {
			Node node = rc.getFullNodeForDbId(1385634);
			assertNotNull(node);
			assertEquals("referenceDatabase", node.getNodeName());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testGetTypeForId() {
		try {
			Class<?> clazz = rc.getTypeForDbId(109624);
			assertNotNull(clazz);
			assertEquals(clazz.getName(), ReactomeReaction.class.getName());

			clazz = rc.getTypeForDbId(70154);
			assertNotNull(clazz);
			assertEquals(clazz.getName(), ReactomeInstanceEdit.class.getName());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testGetObjectsForIds() {
		try {
			List<Integer> ids = new ArrayList<Integer>();
			ids.add(3896801);
			ids.add(549119);

			Map<Integer, ReactomeDatabaseObject> objects = rc.getFullObjectsForDbIds(ids);
			assertEquals(2, objects.keySet().size());
			ReactomeDatabaseObject object = objects.get(3896801);
			assertNotNull(object);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testGetObjectsForIds2() {
		try {
			List<Integer> ids = new ArrayList<Integer>();
			ids.add(2671814);
			ids.add(2562550);
			ids.add(2562550);
			ids.add(2562550);

			Map<Integer, ReactomeDatabaseObject> objects = rc.getFullObjectsForDbIds(ids);
			assertEquals(2, objects.keySet().size());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testGetEntitiesForChebiId() throws Exception {
		try {
			Set<MiriamData> ids = new HashSet<MiriamData>();
			ids.add(new MiriamData(MiriamType.CHEBI, "CHEBI:15378"));
			List<ReactomePhysicalEntity> list = rc.getEntitiesForChebiId(ids, false);
			assertNotNull(list);
			assertTrue(list.size() > 0);
			assertTrue(list.get(0) instanceof ReactomeSimpleEntity);
			ReactomeSimpleEntity entity = (ReactomeSimpleEntity) list.get(0);
			assertNotNull(entity);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSimpleObjectListByQuery() throws Exception {
		try {
			String query = "name=GAK";
			List<ReactomeDatabaseObject> list = rc.getSimpleObjectListByQuery(query, ReactomePhysicalEntity.class);
			assertNotNull(list);
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test(timeout = 15000)
	public void testCachableInterfaceByParams() throws Exception {
		String url = "http://reactome.org/ReactomeRESTfulAPI/RESTfulWS/listByQuery/DatabaseObjects";
		String query = "identifier=2562550\n" + url;
		String newRes = "hello";
		try {
			waitForRefreshCacheQueueToEmpty();

			cache.setCachedQuery(query, reactomeCacheType, newRes);
			cache.invalidateByQuery(query, reactomeCacheType);

			waitForRefreshCacheQueueToEmpty();

			String res = cache.getStringByQuery(query, reactomeCacheType);

			assertNotNull(res);

			assertFalse("Value wasn't refreshed from db", newRes.equals(res));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
