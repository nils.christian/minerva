package lcsb.mapviewer.reactome.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.xml.ReactomeCatalystActivityParser;
import lcsb.mapviewer.reactome.xml.ReactomeDefinedSetParser;
import lcsb.mapviewer.reactome.xml.ReactomeReactionParser;
import lcsb.mapviewer.reactome.xml.ReactomeSimpleEntityParser;

public class ReactomeQueryUtilTest extends ReactomeTestFunctions {

	Model									model;

	static Logger										logger	= Logger.getLogger(ReactomeQueryUtilTest.class);
	@Autowired
	ReactionComparator							reactionComparator;
	@Autowired
	ReactomeSimpleEntityParser			seParser;
	@Autowired
	ReactomeDefinedSetParser				dsParser;
	@Autowired
	ReactomeReactionParser					rParser;
	@Autowired
	ReactomeCatalystActivityParser	caParser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testGetReactionsBetweenSpecies() {
		try {
			// based on reaction re1107 from PD_130712_3.xml
			SimpleMolecule input = new SimpleMolecule("id1");
			input.setName("coenzyme A");
			SimpleMolecule output = new SimpleMolecule("id2");
			output.setName("acetyl-CoA");
			List<Element> list = new ArrayList<>();
			list.add(input);
			list.add(output);

			List<ReactomeReactionlikeEvent> reactions = rcu.getReactionsBetweenSpecies(list);
			assertNotNull(reactions);
			assertTrue(reactions.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void checkFindSimilarReaction() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re1113.xml",true);
			Reaction reaction = model.getReactionByReactionId("re2");
			ReactomeReactionlikeEvent reactomeReaction = rcu.getSimilarReaction(reaction);
			assertNotNull(reactomeReaction);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void checkFindSimilarReaction2() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re105.xml",true);
			Reaction reaction = model.getReactionByReactionId("re1");
			rcu.getSimilarReaction(reaction);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	@Ignore("doesn't work anymore")
	public void checkFindSimilarReaction3() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re1170.xml",true);
			// problematic reaction with ReactomeGenomeEncodedEntity
			Reaction reaction = model.getReactionByReactionId("re1");
			assertNotNull(rcu.getSimilarReaction(reaction));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void checkFindSimilarReaction5() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re1175.xml",true);
			// problematic reaction with candidate set of proteins
			Reaction reaction = model.getReactionByReactionId("re2");
			assertNotNull(rcu.getSimilarReaction(reaction));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void checkFindSimilarReaction6() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re501.xml",true);
			// problematic reaction with multi edge from a protein
			Reaction reaction = model.getReactionByReactionId("re1");
			assertNotNull(rcu.getSimilarReaction(reaction));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void checkFindSimilarReaction7() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re514.xml",true);
			// problematic reaction with unknown element
			Reaction reaction = model.getReactionByReactionId("re1");
			assertNotNull(rcu.getSimilarReaction(reaction));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Ignore("Very long test...")
	@Test
	public void checkFindSimilarReaction8() throws Exception {
		try {
			Model model = getModelForFile("testFiles/pd_full/PD_130712_3.xml",true);
			// problematic reaction with complex of complex of molecule
			Reaction reaction = model.getReactionByReactionId("re966");
			assertNotNull(rcu.getSimilarReaction(reaction));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	@Ignore("reaction for this stable identifier doesn't exist anymore")
	public void testGetNeighboruingReactionsForKnownReaction() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/re1113.xml",true);
			Reaction reaction = model.getReactionByReactionId("re2");
			ReactomeReactionlikeEvent obj = (ReactomeReactionlikeEvent) rc.getFullObjectForStableIdentifier(rcu.getReactomeIdentifierForReaction(reaction));
			Set<ReactomeReactionlikeEvent> reactions = rcu.getNeighboruingReactionsForKnownReaction(obj, true);
			assertNotNull(reactions);
			assertTrue(reactions.size() > 0);
			assertFalse(reactions.contains(obj));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactionsForPubmedId() throws Exception {
		try {
			Set<ReactomeReactionlikeEvent> list = rcu.getReactionsForPubmedId(9481670);
			assertNotNull(list);
			assertTrue(list.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactionsForPubmedIdLisWithKnowReactions() throws Exception {
		try {
			ReactomeReactionlikeEvent reaction = (ReactomeReactionlikeEvent) rc.getFullObjectForDbId(432162);
			Set<ReactomeReactionlikeEvent> reactions = new HashSet<ReactomeReactionlikeEvent>();
			List<String> pubmedIds = new ArrayList<String>();
			pubmedIds.add("9481670");

			List<PredictionResult> list = rcu.getExtendedReactionsForPubmedPublicationsWithTabuReaction(pubmedIds, reactions);
			assertNotNull(list);
			assertTrue(list.size() > 0);

			reactions.add(reaction);
			list = rcu.getExtendedReactionsForPubmedPublicationsWithTabuReaction(pubmedIds, reactions);
			assertNotNull(list);
			assertEquals(0, list.size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
