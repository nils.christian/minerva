package lcsb.mapviewer.reactome.utils;

import lcsb.mapviewer.reactome.utils.comparators.NodeComparatorTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NodeComparatorTest.class, //
		ReactionComparatorTest.class,//
		ReactomeConnectorTest.class, //
		ReactomeQueryUtilTest.class, //
})
public class AllReactomeUtilsTests {

}
