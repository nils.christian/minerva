package lcsb.mapviewer.reactome.utils.comparators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.utils.ReactomeQueryUtil;
import lcsb.mapviewer.reactome.xml.ReactomeCatalystActivityParser;
import lcsb.mapviewer.reactome.xml.ReactomeDefinedSetParser;
import lcsb.mapviewer.reactome.xml.ReactomeReactionParser;
import lcsb.mapviewer.reactome.xml.ReactomeSimpleEntityParser;

public class NodeComparatorTest extends ReactomeTestFunctions {

	Model													 model;

	static Logger									 logger	= Logger.getLogger(NodeComparatorTest.class);
	@Autowired
	ReactomeQueryUtil							 rcu;
	@Autowired
	ReactomeSimpleEntityParser		 seParser;
	@Autowired
	ReactomeDefinedSetParser			 dsParser;
	@Autowired
	ReactomeReactionParser				 rParser;
	@Autowired
	ReactomeCatalystActivityParser caParser;
	@Autowired
	NodeComparator								 nc;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNodeMatch1() throws Exception {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element speciesL_glu = model.getElementByElementId("sa6");
			ReactomePhysicalEntity reactomeL_glu = seParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeInput1.xml").getChildNodes().item(0));

			// these nodes doesn't match according to chebi identifiers that could be
			// retrieved from data
			// somebody probably made a mistake in porting reactions between reactome
			// and PD_MAP (or maybe there is another reason for that)
			assertFalse(nc.compareNodes(speciesL_glu, reactomeL_glu));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNodeMatch2() {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element speciesNADP_PLUS = model.getElementByElementId("sa5");
			ReactomeDefinedSet reactomeNAD_P_PLUS = dsParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeInput2.xml").getChildNodes().item(0));

			// these nodes match, because in reactome we have set of possibilites
			assertTrue(nc.compareNodes(speciesNADP_PLUS, reactomeNAD_P_PLUS));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testNodeMatch3() {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element h2o = model.getElementByElementId("sa7");
			ReactomePhysicalEntity reactomeH2O = seParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeInput3.xml").getChildNodes().item(0));

			// these nodes match
			assertTrue(nc.compareNodes(h2o, reactomeH2O));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testNodeMatch4() {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element oxoglutarate = model.getElementByElementId("sa1");
			ReactomePhysicalEntity reactomeOxoglutaric = seParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeOutput1.xml").getChildNodes().item(0));

			// these nodes match
			assertFalse(nc.compareNodes(oxoglutarate, reactomeOxoglutaric));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testNodeMatch5() {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element nadph = model.getElementByElementId("sa2");
			ReactomeDefinedSet reactomeNadph = dsParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeOutput3.xml").getChildNodes().item(0));

			// these nodes match
			assertTrue(nc.compareNodes(nadph, reactomeNadph));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testNodeMatch6() {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element hydron = model.getElementByElementId("sa3");
			ReactomePhysicalEntity reactomeHydron = seParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeOutput4.xml").getChildNodes().item(0));

			// these nodes match
			assertTrue(nc.compareNodes(hydron, reactomeHydron));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testNodeMatch7() throws Exception {
		try {
			Model model = getModelForFile("testFiles/reactome/cell_designer_references/reaction.xml", true);

			Element hydron = model.getElementByElementId("sa4");
			ReactomeCatalystActivity reactomeHydron = caParser
					.parseObject(getXmlDocumentFromFile("testFiles/reactome/cell_designer_references/reactomeCatalyst1.xml").getChildNodes().item(0));

			// these nodes match
			assertTrue(nc.compareNodes(hydron, reactomeHydron));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNodeMatchInReaction495_a() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/tbid_bax_complex.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(168850);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNodeMatchInReaction495_b() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/bax.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(139907);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNodeMatchInReaction495_c() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/bid.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(139953);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifierMatchInReaction400() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/fumarate_hydratase.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(70981);

			// these nodes match
			boolean status = nc.compareNodes(species, reactomeObject);
			assertTrue(status);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifierMatchInReaction1152() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/acadl.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(77258);

			// these nodes match
			boolean status = nc.compareNodes(species, reactomeObject);
			assertTrue(status);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testProtductMatchInReaction1145() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/phosphate_ion.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(113548);

			// these nodes match
			boolean status = nc.compareNodes(species, reactomeObject);
			assertTrue(status);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactantMatchInReaction1708_a() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/dna.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(29428);

			// these nodes match
			nc.compareNodes(species, reactomeObject);
			// assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactantMatchInReaction898() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/atp.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(416325);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testProductMatchInReaction490() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/bcl2l11_2.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(140526);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testComplexMatchInReaction471() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/diablo_xiap_casp9.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(114318);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUnknownMatchInReaction516() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/unknown.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(500676);

			// these nodes don't match after update of reactome
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testComplexMatchInReaction1145() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/pcca_pccb_complex.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(71030);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testProteinMatchInReaction1461() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/dffb.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(211238);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifierMatchInReaction605() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/caspase_7.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(202853);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Ignore("Object 548839 doesn't exist anymore")
	@Test
	public void testModifierMatchInReaction1170() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/coa.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(548839);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifierMatchInReaction392_a() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/pdp1_complex.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(204160);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifierMatchInReaction392_b() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/ca2.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(204160);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactantMatchInReaction1708_b() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/dna2.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(266214);

			// these nodes match
			assertFalse(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactantMatchInReaction590() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/caspase_3.xml", true);

			Element species = model.getElementByElementId("csa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(114327);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCatalystMatchInReaction1172() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/gpd1.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(1500607);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifierMatchInReaction931() throws Exception {
		try {
			Model model = getModelForFile("testFiles/small/prkacb.xml", true);

			Element species = model.getElementByElementId("sa1");
			ReactomeDatabaseObject reactomeObject = rc.getFullObjectForDbId(443469);

			// these nodes match
			assertTrue(nc.compareNodes(species, reactomeObject));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
