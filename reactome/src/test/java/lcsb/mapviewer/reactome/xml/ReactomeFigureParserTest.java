package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeFigure;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeFigureParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeFigureParserTest.class);

	@Autowired
	ReactomeFigureParser parser ;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/figure.xml");
			ReactomeFigure res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer)109689,res.getCreated().getDbId());
			assertEquals("/figures/unsatfattyacidbetao_2.jpg",res.getDisplayName());
			assertEquals("/figures/unsatfattyacidbetao_2.jpg",res.getUrl());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/figure.xml");
			ReactomeFigure res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
