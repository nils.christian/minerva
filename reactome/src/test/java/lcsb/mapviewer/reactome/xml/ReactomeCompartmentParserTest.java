package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeCompartment;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeCompartmentParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeCompartmentParserTest.class);

	@Autowired
	ReactomeCompartmentParser parser = new ReactomeCompartmentParser();
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/compartment.xml");
			ReactomeCompartment res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			
			assertEquals((Integer)160533,res.getModified().get(0).getDbId());
			assertEquals((Integer)350,res.getDbId());
			assertEquals("0005730",res.getAccession());
			assertEquals("A small, dense body one or more of which are present in the nucleus of eukaryotic cells. It is rich in RNA and protein, is not bounded by a limiting membrane, and is not seen during mitosis. Its prime function is the transcription of the nucleolar DNA into 45S ribosomal-precursor RNA, the processing of this RNA into 5.8S, 18S, and 28S components of ribosomal RNA, and the association of these components with 5S RNA and proteins synthesized outside the nucleolus. This association results in the formation of ribonucleoprotein precursors; these pass into the cytoplasm and mature into the 40S and 60S subunits of the ribosome.",res.getDefinition());
			assertEquals((Integer)1,res.getReferenceDatabase().getDbId());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/compartment.xml");
			ReactomeCompartment res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
}
