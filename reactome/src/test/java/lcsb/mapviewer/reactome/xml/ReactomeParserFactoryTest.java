package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ReactomeParserFactoryTest extends ReactomeTestFunctions{

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetParserByClassName() {
		try {
			ReactomeNodeParser<?> parser = ReactomeParserFactory.getParserForClass(ReactomeDatabaseObject.class);
			assertNotNull(parser);
			assertTrue(parser  instanceof ReactomeDatabaseObjectParser);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unkonwn exception occurred");
		}

	}

	@Test
	public void testGetInvalidParserByClassName() {
		try {
			ReactomeNodeParser<?> parser = ReactomeParserFactory.getParserForClass(Object.class);
			fail("Exception expected");
			assertNull(parser);
		} catch (Exception e) {
		}
	}

}
