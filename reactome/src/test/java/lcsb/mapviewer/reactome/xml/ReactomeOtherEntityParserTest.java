package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

public class ReactomeOtherEntityParserTest extends ReactomeTestFunctions {
	Logger										logger = Logger.getLogger(ReactomeOtherEntityParserTest.class);

	@Autowired
	ReactomeOtherEntityParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/otherEntity.xml");
			ReactomeOtherEntity res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);

			assertEquals((Integer) 29428, res.getDbId());
			assertEquals("DNA [nucleoplasm]", res.getDisplayName());
			assertEquals((Integer) 77027, res.getCreated().getDbId());
			assertEquals((Integer) 368619, res.getStableIdentifier().getDbId());
			assertEquals((Integer) 7660, res.getCompartments().get(0).getDbId());
			assertEquals((Integer) 212290, res.getCrossReferences().get(0).getDbId());
			assertEquals(2, res.getNames().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/otherEntity.xml");
			ReactomeOtherEntity res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCrossReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
