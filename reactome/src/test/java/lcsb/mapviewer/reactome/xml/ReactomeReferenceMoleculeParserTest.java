package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeReferenceMoleculeParserTest extends ReactomeTestFunctions {
	Logger													logger	= Logger.getLogger(ReactomeReferenceMoleculeParserTest.class);

	@Autowired
	ReactomeReferenceMoleculeParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceMolecule.xml");
			ReactomeReferenceMolecule res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 29860, res.getDbId());
			assertEquals((Integer) 141749, res.getModified().get(0).getDbId());
			assertEquals((Integer) 29861, res.getCrossReferences().get(0).getDbId());
			assertEquals("17925", res.getIdentifier());
			assertEquals("alpha-D-Glucose [ChEBI:17925]", res.getDisplayName());
			assertEquals("alpha-D-Glucose", res.getNames().get(0));
			assertEquals((Integer) 114984, res.getReferenceDatabase().getDbId());
			assertEquals(
					"C6H12O6\\nBBtclserve02090411462D 0   0.00000     0.00000263\\n \\n 24 24  0  0  0  0  0  0  0  0999 V2000\\n   -0.5448    0.5276    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\\n   -0.5448   -0.3000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\\n    0.1690    0.9414    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\\n   -1.2621    0.9414    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\\n    0.1690   -0.7138    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\\n   -1.2621   -0.7138    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\\n    0.8897    0.5276    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\\n   -1.8897    0.4034    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\\n    0.8897   -0.3000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\\n    0.1690   -1.5414    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\\n    1.6035    0.9414    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\\n    1.6035   -0.7138    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\\n   -0.5453    1.0405    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n   -0.9890   -0.0434    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n   -0.9573    1.3540    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n   -1.6377    1.2907    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n   -0.2755   -0.9699    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n   -1.7064   -0.9701    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n    1.3342    0.2715    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n   -2.2791    0.0696    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n    1.3342   -0.0439    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n    0.1690   -2.0543    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n    2.0473    1.1987    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n    2.0473   -0.9711    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n  1  2  1  0  0  0  0\\n  1  3  1  0  0  0  0\\n  1  4  1  1  0  0  0\\n  2  5  1  0  0  0  0\\n  2  6  1  6  0  0  0\\n  3  7  1  0  0  0  0\\n  4  8  1  0  0  0  0\\n  5  9  1  0  0  0  0\\n  5 10  1  1  0  0  0\\n  7 11  1  6  0  0  0\\n  9 12  1  6  0  0  0\\n  7  9  1  0  0  0  0\\n  1 13  1  0  0  0  0\\n  2 14  1  0  0  0  0\\n  4 15  1  0  0  0  0\\n  4 16  1  0  0  0  0\\n  5 17  1  0  0  0  0\\n  6 18  1  0  0  0  0\\n  7 19  1  0  0  0  0\\n  8 20  1  0  0  0  0\\n  9 21  1  0  0  0  0\\n 10 22  1  0  0  0  0\\n 11 23  1  0  0  0  0\\n 12 24  1  0  0  0  0\\nM  END\\n$$$$\\n",
					res.getAtomicConnectivity());
			assertEquals("C6H12O6", res.getFormula());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceMolecule.xml");
			ReactomeReferenceMolecule res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCrossReferences().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
