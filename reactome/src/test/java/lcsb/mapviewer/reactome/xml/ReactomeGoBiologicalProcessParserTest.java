package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeGoBiologicalProcess;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeGoBiologicalProcessParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeGoBiologicalProcessParserTest.class);

	@Autowired
	ReactomeGoBiologicalProcessParser parser = new ReactomeGoBiologicalProcessParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/goBiologicalProcess.xml");
			ReactomeGoBiologicalProcess res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer)140,res.getDbId());
			assertEquals((Integer)160533,res.getModified().get(0).getDbId());
			assertEquals("0007166",res.getAccession());
			assertEquals("A series of molecular signals initiated by activation of a receptor on the surface of a cell. The pathway begins with binding of an extracellular ligand to a cell surface receptor, or for receptors that signal in the absence of a ligand, by ligand-withdrawal or the activity of a constitutively active receptor. The pathway ends with regulation of a downstream cellular process, e.g. transcription.",res.getDefinition());
			assertEquals((Integer)1,res.getReferenceDatabase().getDbId());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/goBiologicalProcess.xml");
			ReactomeGoBiologicalProcess res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
