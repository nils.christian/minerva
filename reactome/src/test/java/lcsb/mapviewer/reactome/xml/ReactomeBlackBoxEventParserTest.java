package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeBlackBoxEvent;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

public class ReactomeBlackBoxEventParserTest extends ReactomeTestFunctions {
	Logger											logger = Logger.getLogger(ReactomeBlackBoxEventParserTest.class);

	@Autowired
	ReactomeBlackBoxEventParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/blackBoxEvent.xml");
			ReactomeBlackBoxEvent res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 139947, res.getCreated().getDbId());
			assertEquals((Integer) 139952, res.getDbId());
			assertEquals((Integer) 141163, res.getModified().get(1).getDbId());
			assertEquals((Integer) 202124, res.getEntityOnOtherCells().get(0).getDbId());
			assertEquals((Integer) 358238, res.getStableIdentifier().getDbId());
			assertEquals((Integer) 876, res.getCompartments().get(1).getDbId());
			assertEquals((Integer) 3508822, res.getEdited().get(0).getDbId());
			assertEquals(false, res.getIsInDisease());
			assertEquals(false, res.getIsInferred());
			assertEquals(true, res.getDoRelease());
			assertEquals((Integer) 1267836, res.getLiteratureReferences().get(2).getDbId());

			assertEquals((Integer) 450358, res.getOrthologousEvents().get(0).getDbId());
			assertEquals((Integer) 141156, res.getPrecedingEvents().get(0).getDbId());
			assertEquals((Integer) 2594924, res.getReviewed().get(0).getDbId());
			assertEquals((Integer) 48887, res.getSpecies().getDbId());
			assertEquals((Integer) 178769, res.getSummations().get(0).getDbId());
			assertEquals((Integer) 139951, res.getCatalystActivities().get(0).getDbId());
			assertEquals((Integer) 2671813, res.getInputs().get(0).getDbId());
			assertEquals((Integer) 2562550, res.getOutputs().get(1).getDbId());
			assertEquals("Caspase-8 processing ", res.getDisplayName());
			assertEquals("Caspase-8 processing ", res.getNames().get(0));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/blackBoxEvent.xml");
			ReactomeBlackBoxEvent res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);

			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getEdited().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getLiteratureReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getOrthologousEvents().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getPrecedingEvents().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReviewed().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSummations().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInputs().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getOutputs().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCatalystActivities().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
