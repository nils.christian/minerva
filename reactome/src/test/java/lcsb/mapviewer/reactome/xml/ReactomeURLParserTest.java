package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeStatus;
import lcsb.mapviewer.reactome.model.ReactomeURL;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeURLParserTest extends ReactomeTestFunctions {
	Logger						logger	= Logger.getLogger(ReactomeURLParserTest.class);

	@Autowired
	ReactomeURLParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/url.xml");
			ReactomeURL res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);

			assertEquals((Integer) 2858999, res.getDbId());
			assertEquals((Integer) 2859000, res.getCreated().getDbId());
			assertEquals((Integer) 2858997, res.getAuthors().get(0).getDbId());
			assertEquals("Catalyst Sequence Similarity Project", res.getTitle());
			assertEquals("http://wiki.reactome.org/index.php/Catalyst_Sequence_Similarity_Project", res.getUniformResourceLocator());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/url.xml");
			ReactomeURL res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getAuthors().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
