package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDisease;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeDiseaseParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeDiseaseParserTest.class);

	@Autowired
	ReactomeDiseaseParser parser ;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/disease.xml");
			ReactomeDisease res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 1660557, res.getCreated().getDbId());
			assertEquals((Integer) 1660556, res.getDbId());
			assertEquals("tuberculosis", res.getDisplayName());
			assertEquals("tuberculosis", res.getNames().get(0));
			assertEquals("A primary bacterial infectious disease that is located_in lungs, located_in lymph nodes, located_in pericardium, located_in brain, located_in pleura and located_in gastrointestinal tract, has_material_basis_in Mycobacterium tuberculosis, which is transmitted_by droplets released into the air when an infected person coughs or sneezes.", res.getDefinition());
			assertEquals("Tuberculoma (finding)", res.getSynonyms().get(1));
			assertEquals((Integer) 399, res.getIdentifier());
			assertEquals((Integer) 1247631, res.getReferenceDatabase().getDbId());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
}
