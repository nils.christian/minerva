package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeSequenceDomain;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeSequenceDomainParserTest extends ReactomeTestFunctions {
	Logger												logger	= Logger.getLogger(ReactomeSequenceDomainParserTest.class);

	@Autowired
	ReactomeSequenceDomainParser	parser	;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/sequenceDomain.xml");
			ReactomeSequenceDomain res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 1598217, res.getCreated().getDbId());
			assertEquals((Integer) 169873, res.getDbId());
			assertEquals((Integer) 70154, res.getModified().get(0).getDbId());
			assertEquals("SH2 domain", res.getDisplayName());
			assertEquals((Integer) (-1), res.getEndCoordinate());
			assertEquals((Integer) 1, res.getStartCoordinate());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
