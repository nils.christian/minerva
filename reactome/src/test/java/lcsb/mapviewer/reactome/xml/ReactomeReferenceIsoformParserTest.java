package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReferenceIsoform;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeReferenceIsoformParserTest extends ReactomeTestFunctions {
	Logger													logger	= Logger.getLogger(ReactomeReferenceIsoformParserTest.class);

	@Autowired
	ReactomeReferenceIsoformParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceIsoform.xml");
			ReactomeReferenceIsoform res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 143527, res.getModified().get(1).getDbId());
			assertEquals((Integer) 58407, res.getDbId());
			assertEquals((Integer) 29805, res.getCrossReferences().get(0).getDbId());
			assertEquals((Integer) 2, res.getReferenceDatabase().getDbId());
			assertEquals("false", res.getIsSequenceChanged());
			assertEquals((Integer) 48887, res.getSpecies().getDbId());
			assertEquals((Integer) 10030875, res.getReferenceGenes().get(0).getDbId());
			assertEquals((Integer) 192971, res.getReferenceTranscripts().get(0).getDbId());
			assertEquals((Integer) 402286, res.getIsoformParents().get(0).getDbId());
			assertEquals("UniProt:P02545-1 LMNA", res.getDisplayName());
			assertEquals("P02545", res.getIdentifier());
			assertEquals("LMNA", res.getNames().get(0));
			assertEquals("ENSG00000160789", res.getOtherIdentifier().get(0));
			assertEquals("E0855F7699F0318B", res.getChecksum());
			assertEquals(
					"FUNCTION Lamins are components of the nuclear lamina, a fibrous layer on the nucleoplasmic side of the inner nuclear membrane, which is thought to provide a framework for the nuclear envelope and may also interact with chromatin. Lamin A and C are present in equal amounts in the lamina of mammals. Plays an important role in nuclear assembly, chromatin organization, nuclear membrane and telomere dynamics.FUNCTION Prelamin-A/C can accelerate smooth muscle cell senescence. It acts to disrupt mitosis and induce DNA damage in vascular smooth muscle cells (VSMCs), leading to mitotic failure, genomic instability, and premature senescence.SUBUNIT Homodimer of lamin A and lamin C. Interacts with lamin-associated polypeptides IA, IB and TMPO-alpha, RB1 and with emerin. Interacts with SREBF1, SREBF2, SUN2 and TMEM43 (By similarity). Proteolytically processed isoform A interacts with NARF. Interacts with SUN1. Prelamin-A/C interacts with EMD. Interacts with MLIP; may regulate MLIP localization to the nucleus envelope. Interacts with DMPK; may regulate nuclear envelope stability.TISSUE SPECIFICITY In the arteries, prelamin-A/C accumulation is not observed in young healthy vessels but is prevalent in medial vascular smooth muscle cells (VSMCs) from aged individuals and in atherosclerotic lesions, where it often colocalizes with senescent and degenerate VSMCs. Prelamin-A/C expression increases with age and disease. In normal aging, the accumulation of prelamin-A/C is caused in part by the down-regulation of ZMPSTE24/FACE1 in response to oxidative stress.PTM Increased phosphorylation of the lamins occurs before envelope disintegration and probably plays a role in regulating lamin associations.PTM Proteolytic cleavage of the C-terminal of 18 residues of prelamin-A/C results in the production of lamin-A/C. The prelamin-A/C maturation pathway includes farnesylation of CAAX motif, ZMPSTE24/FACE1 mediated cleavage of the last three amino acids, methylation of the C-terminal cysteine and endoproteolytic removal of the last 15 C-terminal amino acids. Proteolytic cleavage requires prior farnesylation and methylation, and absence of these blocks cleavage.PTM Sumoylation is necessary for the localization to the nuclear envelope.PTM Farnesylation of prelamin-A/C facilitates nuclear envelope targeting.DISEASE Emery-Dreifuss muscular dystrophy 2, autosomal dominant (EDMD2) [MIM:181350]: A degenerative myopathy characterized by weakness and atrophy of muscle without involvement of the nervous system, early contractures of the elbows, Achilles tendons and spine, and cardiomyopathy associated with cardiac conduction defects. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Emery-Dreifuss muscular dystrophy 3, autosomal recessive (EDMD3) [MIM:181350]: A degenerative myopathy characterized by weakness and atrophy of muscle without involvement of the nervous system, early contractures of the elbows, Achilles tendons and spine, and cardiomyopathy associated with cardiac conduction defects. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Cardiomyopathy, dilated 1A (CMD1A) [MIM:115200]: A disorder characterized by ventricular dilation and impaired systolic function, resulting in congestive heart failure and arrhythmia. Patients are at risk of premature death. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Familial partial lipodystrophy 2 (FPLD2) [MIM:151660]: A disorder characterized by the loss of subcutaneous adipose tissue in the lower parts of the body (limbs, buttocks, trunk). It is accompanied by an accumulation of adipose tissue in the face and neck causing a double chin, fat neck, or cushingoid appearance. Adipose tissue may also accumulate in the axillae, back, labia majora, and intraabdominal region. Affected patients are insulin-resistant and may develop glucose intolerance and diabetes mellitus after age 20 years, hypertriglyceridemia, and low levels of high density lipoprotein cholesterol. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Limb-girdle muscular dystrophy 1B (LGMD1B) [MIM:159001]: An autosomal dominant degenerative myopathy with age-related atrioventricular cardiac conduction disturbances, dilated cardiomyopathy, and the absence of early contractures. Characterized by slowly progressive skeletal muscle weakness of the hip and shoulder girdles. Muscle biopsy shows mild dystrophic changes. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Charcot-Marie-Tooth disease 2B1 (CMT2B1) [MIM:605588]: A recessive axonal form of Charcot-Marie-Tooth disease, a disorder of the peripheral nervous system, characterized by progressive weakness and atrophy, initially of the peroneal muscles and later of the distal muscles of the arms. Charcot-Marie-Tooth disease is classified in two main groups on the basis of electrophysiologic properties and histopathology: primary peripheral demyelinating neuropathies (designated CMT1 when they are dominantly inherited) and primary peripheral axonal neuropathies (CMT2). Neuropathies of the CMT2 group are characterized by signs of axonal regeneration in the absence of obvious myelin alterations, normal or slightly reduced nerve conduction velocities, and progressive distal muscle weakness and atrophy. Nerve conduction velocities are normal or slightly reduced. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Hutchinson-Gilford progeria syndrome (HGPS) [MIM:176670]: Rare genetic disorder characterized by features reminiscent of marked premature aging. Note=The disease is caused by mutations affecting the gene represented in this entry. HGPS is caused by the toxic accumulation of a mutant form of lamin-A/C. This mutant protein, called progerin, acts to deregulate mitosis and DNA damage signaling, leading to premature cell death and senescence. Progerin lacks the conserved ZMPSTE24/FACE1 cleavage site and therefore remains permanently farnesylated. Thus, although it can enter the nucleus and associate with the nuclear envelope, it cannot incorporate normally into the nuclear lamina.DISEASE Cardiomyopathy, dilated, with hypergonadotropic hypogonadism (CMDHH) [MIM:212112]: A disorder characterized by the association of genital anomalies, hypergonadotropic hypogonadism and dilated cardiomyopathy. Patients can present other variable clinical manifestations including mental retardation, skeletal anomalies, scleroderma-like skin, graying and thinning of hair, osteoporosis. Dilated cardiomyopathy is characterized by ventricular dilation and impaired systolic function, resulting in congestive heart failure and arrhythmia. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Mandibuloacral dysplasia with type A lipodystrophy (MADA) [MIM:248370]: A disorder characterized by mandibular and clavicular hypoplasia, acroosteolysis, delayed closure of the cranial suture, progeroide appearance, partial alopecia, soft tissue calcinosis, joint contractures, and partial lipodystrophy with loss of subcutaneous fat from the extremities. Adipose tissue in the face, neck and trunk is normal or increased. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Lethal tight skin contracture syndrome (LTSCS) [MIM:275210]: Rare disorder mainly characterized by intrauterine growth retardation, tight and rigid skin with erosions, prominent superficial vasculature and epidermal hyperkeratosis, facial features (small mouth, small pinched nose and micrognathia), sparse/absent eyelashes and eyebrows, mineralization defects of the skull, thin dysplastic clavicles, pulmonary hypoplasia, multiple joint contractures and an early neonatal lethal course. Liveborn children usually die within the first week of life. The overall prevalence of consanguineous cases suggested an autosomal recessive inheritance. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Heart-hand syndrome Slovenian type (HHS-Slovenian) [MIM:610140]: Heart-hand syndrome (HHS) is a clinically and genetically heterogeneous disorder characterized by the co-occurrence of a congenital cardiac disease and limb malformations. Note=The disease is caused by mutations affecting the gene represented in this entry.DISEASE Muscular dystrophy congenital LMNA-related (MDCL) [MIM:613205]: A form of congenital muscular dystrophy. Patients present at birth, or within the first few months of life, with hypotonia, muscle weakness and often with joint contractures. Note=The disease is caused by mutations affecting the gene represented in this entry.MISCELLANEOUS There are three types of lamins in human cells: A, B, and C.MISCELLANEOUS The structural integrity of the lamina is strictly controlled by the cell cycle, as seen by the disintegration and formation of the nuclear envelope in prophase and telophase, respectively.SIMILARITY Belongs to the intermediate filament family.",
					res.getComments().get(0));
			assertEquals(
					"recommendedName: Prelamin-A/C  component recommendedName: Lamin-A/C  alternativeName: 70 kDa lamin  alternativeName: Renal carcinoma antigen NY-REN-32  /component",
					res.getDescriptions().get(0));
			assertEquals("LMN1", res.getGeneNames().get(1));
			assertEquals("3D-structure", res.getKeywords().get(0));
			assertEquals("LMNA_HUMAN", res.getSecondaryIdentifiers().get(0));
			assertEquals("P02545-1", res.getVariantIdentifier());
			assertEquals((Integer) 664, res.getSequenceLength());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceIsoform.xml");
			ReactomeReferenceIsoform res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCrossReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceGenes().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceTranscripts().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getIsoformParents().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
