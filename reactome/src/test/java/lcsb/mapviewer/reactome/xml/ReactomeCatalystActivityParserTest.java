package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

public class ReactomeCatalystActivityParserTest extends ReactomeTestFunctions {
	Logger												 logger	= Logger.getLogger(ReactomeCatalystActivityParserTest.class);

	@Autowired
	ReactomeCatalystActivityParser parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/catalystActivity.xml");
			ReactomeCatalystActivity res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 8347, res.getActivity().getDbId());
			assertEquals((Integer) 2160866, res.getPhysicalEntity().getDbId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/catalystActivity.xml");
			ReactomeCatalystActivity res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getActivity().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getPhysicalEntity().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
