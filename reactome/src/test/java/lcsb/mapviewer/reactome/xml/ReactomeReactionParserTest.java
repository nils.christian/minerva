package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReaction;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeReactionParserTest extends ReactomeTestFunctions {
	Logger									logger	= Logger.getLogger(ReactomeReactionParserTest.class);

	@Autowired
	ReactomeReactionParser	parser	= new ReactomeReactionParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/reaction.xml");
			ReactomeReaction res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 199678, res.getLiteratureReferences().get(1).getDbId());
			assertEquals((Integer) 453329, res.getRevised().get(0).getDbId());
			assertEquals((Integer) 73099, res.getCrossReferences().get(0).getDbId());
			assertEquals((Integer) 70589, res.getReverseReaction().getDbId());
			assertEquals((Integer) 111451, res.getAuthoreds().get(0).getDbId());
			assertEquals((Integer) 109985, res.getFigures().get(0).getDbId());
			assertEquals((Integer) 201589, res.getEdited().get(0).getDbId());
			assertEquals((Integer) 210329, res.getReviewed().get(0).getDbId());
			assertEquals((Integer) 20426, res.getGoBiologicalProcess().getDbId());
			assertEquals((Integer) 1660556, res.getDiseases().get(0).getDbId());
			assertEquals((Integer) 198621, res.getNormalReactions().get(0).getDbId());
			assertEquals((Integer) 2473605, res.getRequiredInputComponents().get(0).getDbId());

			assertEquals("NEW", res.getReleaseStatus());
			assertEquals(false, res.getIsChimeric());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/reaction.xml");
			ReactomeReaction res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCatalystActivities().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInputs().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getOutputs().get(1).getStatus());
//skip it as there is a bug in reactome			
//			assertEquals(ReactomeStatus.FULL, res.getEvidenceType().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInferredFroms().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getOrthologousEvents().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCrossReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getPrecedingEvents().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getAuthoreds().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReviewed().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getFigures().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getEdited().get(1).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReverseReaction().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getGoBiologicalProcess().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
