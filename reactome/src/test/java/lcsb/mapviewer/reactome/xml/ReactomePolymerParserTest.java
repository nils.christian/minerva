package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomePolymer;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomePolymerParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomePolymerParserTest.class);

	@Autowired
	ReactomePolymerParser parser ;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/polymer.xml");
			ReactomePolymer res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			
			assertEquals((Integer)350274, res.getDbId());
			assertEquals("DFF40 homooligomer [nucleoplasm]", res.getDisplayName());
			assertEquals((Integer)350279, res.getCreated().getDbId());
			assertEquals((Integer)350312, res.getModified().get(0).getDbId());
			assertEquals((Integer)364826, res.getStableIdentifier().getDbId());
			assertEquals((Integer)7660, res.getCompartments().get(0).getDbId());
			assertEquals((Integer)2160866, res.getInferredTos().get(0).getDbId());
			assertEquals("DFF40 homooligomer", res.getNames().get(0));
			assertEquals((Integer)4, res.getMinUnitCount());
			assertEquals((Integer)211195, res.getRepeatedUnits().get(0).getDbId());
			assertEquals((Integer)48887, res.getSpecies().getDbId());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/polymer.xml");
			ReactomePolymer res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInferredTos().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getRepeatedUnits().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
