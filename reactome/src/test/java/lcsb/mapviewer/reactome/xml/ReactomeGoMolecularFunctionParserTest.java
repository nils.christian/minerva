package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeGoMolecularFunction;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeGoMolecularFunctionParserTest extends ReactomeTestFunctions {
	Logger															logger	= Logger.getLogger(ReactomeGoMolecularFunctionParserTest.class);

	@Autowired
	ReactomeGoMolecularFunctionParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/goMolecularFunction.xml");
			ReactomeGoMolecularFunction res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 8347, res.getDbId());
			assertEquals((Integer) 3446290, res.getModified().get(49).getDbId());
			assertEquals("0004135", res.getAccession());
			assertEquals(
					"Catalysis of the hydrolysis of (1->6)-alpha-D-glucosidic branch linkages in glycogen phosphorylase limit dextrin. Limit dextrin is the highly branched core that remains after exhaustive treatment of glycogen with glycogen phosphorylase. It is formed because these enzymes cannot hydrolyze the (1->6) glycosidic linkages present.",
					res.getDefinition());
			assertEquals("3.2.1.33", res.getEcNumbers().get(0));
			assertEquals("amylo-alpha-1,6-glucosidase activity", res.getNames().get(0));
			assertEquals((Integer) 1, res.getReferenceDatabase().getDbId());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/goMolecularFunction.xml");
			ReactomeGoMolecularFunction res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
