package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ReactomeEntityWithAccessionedSequenceParserTest extends ReactomeTestFunctions {

	@Autowired
	ReactomeEntityWithAccessionedSequenceParser parser ;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/entityWithAccessionedSequence.xml");
			ReactomeEntityWithAccessionedSequence res = parser.parseObject(document.getChildNodes().item(0));
			
			assertEquals((Integer) 1660557, res.getCreated().getDbId());
			assertEquals((Integer) 3920050, res.getDbId());
			assertEquals("GAK(1-1311) [cytoplasmic vesicle membrane]", res.getDisplayName());
			assertEquals((Integer) 437051, res.getCompartments().get(0).getDbId());
			assertEquals((Integer) 434203, res.getInferredFroms().get(0).getDbId());
			assertEquals(6, res.getNames().size());
			assertEquals((Integer) 48895, res.getSpecies().getDbId());
			assertEquals((Integer) 1311, res.getEndCoordinate());
			assertEquals((Integer) 2161523, res.getReferenceEntity().getDbId());
			assertEquals((Integer) 1, res.getStartCoordinate());
			assertEquals((Integer) 199756, res.getLiteratureReferences().get(0).getDbId());
			assertEquals((Integer) 1500575, res.getDiseases().get(1).getDbId());
			assertEquals("AAA+ ATPase (26S proteasome)",res.getDefinition());
			

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testSimpleParser() {
		try {
			Node node = getNodeFromXmlString(readFile("testFiles/reactome/SimpleObject.xml"));
			ReactomeEntityWithAccessionedSequence object = parser.parseSimplifiedObject(node);
			assertEquals(ReactomeStatus.ONLY_ID, object.getStatus());
			assertEquals(434340, (int) object.getDbId());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/entityWithAccessionedSequence.xml");
			ReactomeEntityWithAccessionedSequence res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInferredFroms().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getLiteratureReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceEntity().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}


}
