package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeComplexParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeComplexParserTest.class);

	@Autowired
	ReactomeComplexParser parser ;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/complex.xml");
			ReactomeComplex res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			
			assertEquals((Integer)139947,res.getCreated().getDbId());
			assertEquals((Integer)3896793,res.getDbId());
			assertEquals("((1,6)-alpha-glucosyl)poly((1,4)-alpha-glucosyl)glycogenin-1 dimer (name copied from entity in Homo sapiens) [cytosol]",res.getDisplayName());
			assertEquals((Integer)70101,res.getCompartments().get(0).getDbId());
			assertEquals((Integer)453350,res.getInferredFroms().get(1).getDbId());
			assertEquals("((1,6)-alpha-glucosyl)poly((1,4)-alpha-glucosyl)glycogenin-1 dimer (name copied from entity in Homo sapiens)",res.getNames().get(0));
			assertEquals((Integer)143353,res.getSummations().get(0).getDbId());
			assertEquals((Integer)29418,res.getHasComponents().get(2).getDbId());
			assertEquals((Integer)48895,res.getSpecies().getDbId());
			assertEquals((Integer)48895,res.getSpecies().getDbId());
			assertEquals((Integer)70101,res.getIncludedLocations().get(0).getDbId());
			assertEquals((Integer)1470028,res.getGoCellularComponent().getDbId());
			assertEquals((Integer)450856,res.getReviseds().get(0).getDbId());
			assertEquals((Integer)1527321,res.getRevieweds().get(0).getDbId());
			
			
			assertEquals(true,res.getIsChimeric());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/complex.xml");
			ReactomeComplex res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInferredFroms().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSummations().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getHasComponents().get(2).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getIncludedLocations().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getGoCellularComponent().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
}
