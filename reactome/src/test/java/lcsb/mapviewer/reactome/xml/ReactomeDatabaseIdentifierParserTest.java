package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseIdentifier;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

public class ReactomeDatabaseIdentifierParserTest extends ReactomeTestFunctions {

	@Autowired
	ReactomeDatabaseIdentifierParser parser = new ReactomeDatabaseIdentifierParser();
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSimpleParser() {
		try {
			Node node = getNodeFromXmlString(readFile("testFiles/reactome/databaseIdentifier.xml"));
			ReactomeDatabaseIdentifier object = parser.parseObject(node);
			
			assertNotNull(object);
			assertEquals((Integer)72811, object.getCreated().getDbId());
			assertEquals((Integer)73099, object.getDbId());
			assertEquals("NCBI_taxonomy:10116", object.getDisplayName());
			assertEquals("10116", object.getIdentifier());
			assertEquals((Integer)72810, object.getReferenceDatabase().getDbId());
			assertEquals("http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=10116", object.getUrl());
			
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Node node = getNodeFromXmlString(readFile("testFiles/reactome/databaseIdentifier.xml"));
			ReactomeDatabaseIdentifier res = parser.parseObject(node);
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	
}
