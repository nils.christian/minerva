package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertFalse;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class GeneralTests extends ReactomeTestFunctions {
	Logger logger = Logger.getLogger(GeneralTests.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFragmentInsertionModification() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2318717);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFragmentDeletionModification() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2063905);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInterChainCrosslinkedResidue() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(4419898);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testIntraChainCrosslinkedResidue() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(3777079);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGroupModifiedResidue() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2396027);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testModifiedResidue() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(140630);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	@Ignore("Reactome server returns Internal Server error")
	public void testSequenceDomain() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(173802);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEntityFunctionalStatus() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(3080617);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPathway() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(168276);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testBlackBoxEvent() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2454113);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDepolymerisation() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(173642);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPolymerisation() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(936986);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReaction() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(71593);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEvidenceType() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(4546411);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCellType() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2473605);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDisease() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(1625206);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPsiMod() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(448185);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSequenceOntology() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2318494);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEntityCompartment() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(1470028);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGO_MolecularFunction() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(77954);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	@Ignore("In reactome there is no object of this type")
	public void testPathwayCoordinates() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(5477003);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFigure() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(157815);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFrontPage() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(4545014);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testFunctionalStatusType() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2426108);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInstanceEdit() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(1598217);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPathwayDiagram() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2162173);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPathwayVertex() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(10834395);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactionVertex() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(10834441);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCandidateSet() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(198809);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testOpenSet() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(162704);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDefinedSet() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(453350);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testOtherEntity() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(162704);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPolymer() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(2160866);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimpleEntity() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(868632);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testLiteratureReference() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(182836);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testURL() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(1170534);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReferenceDatabase() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(1385634);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReactionCoordinates() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(142110);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReferenceGroup() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(164405);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReferenceMolecule() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(142110);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReferenceIsoform() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(59007);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReferenceRNASequence() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(188854);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testNegativeRegulation() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(5229351);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRequirement() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(72781);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPositiveRegulation() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(5623897);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSummation() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(143360);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testTaxon() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(72968);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReferenceDNASequence() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(10030875);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSpecies() throws Exception {
		try {
			ReactomeDatabaseObject object = rc.getFullObjectForDbId(181384);
			assertFalse(object.getClass().getName().contains(ReactomeDatabaseObject.class.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
