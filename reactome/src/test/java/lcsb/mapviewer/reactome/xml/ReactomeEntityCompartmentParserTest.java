package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeEntityCompartment;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeEntityCompartmentParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeEntityCompartmentParserTest.class);


	@Autowired
	ReactomeEntityCompartmentParser parser = new ReactomeEntityCompartmentParser();
@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/entityCompartment.xml");
			ReactomeEntityCompartment res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			
			assertEquals((Integer)70101, res.getDbId());
			assertEquals("cytosol", res.getDisplayName());
			assertEquals((Integer)3445953, res.getModified().get(30).getDbId());
			assertEquals("0005829", res.getAccession());
			assertEquals("The part of the cytoplasm that does not contain organelles but which does contain other particulate matter, such as protein complexes.", res.getDefinition());
			assertEquals((Integer)1, res.getReferenceDatabase().getDbId());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/entityCompartment.xml");
			ReactomeEntityCompartment res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
