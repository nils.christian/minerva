package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeSimpleEntityParserTest extends ReactomeTestFunctions {
	Logger											logger	= Logger.getLogger(ReactomeSimpleEntityParserTest.class);

	@Autowired
	ReactomeSimpleEntityParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/simpleEntity.xml");
			ReactomeSimpleEntity res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);

			assertEquals((Integer) 70113, res.getDbId());
			assertEquals("Glc [cytosol]", res.getDisplayName());
			assertEquals((Integer) 1884039, res.getModified().get(2).getDbId());
			assertEquals((Integer) 362359, res.getStableIdentifier().getDbId());
			assertEquals((Integer) 70101, res.getCompartments().get(0).getDbId());
			assertEquals((Integer) 29861, res.getCrossReferences().get(0).getDbId());
			assertEquals(2, res.getNames().size());
			assertEquals((Integer) 29860, res.getReferenceEntities().get(0).getDbId());
			assertEquals((Integer) 111218, res.getFigures().get(0).getDbId());
			assertEquals((Integer) 48887, res.getSpecies().getDbId());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/simpleEntity.xml");
			ReactomeSimpleEntity res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCrossReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceEntities().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getFigures().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
