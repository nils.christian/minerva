package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomePathway;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomePathwayParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomePathwayParserTest.class);

	@Autowired
	ReactomePathwayParser parser ;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/pathway.xml");
			ReactomePathway res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer)111451,res.getCreated().getDbId());
			assertEquals((Integer)360839,res.getStableIdentifier().getDbId());
			assertEquals((Integer)70101,res.getCompartments().get(0).getDbId());
			assertEquals(false,res.getIsInDisease());
			assertEquals(false,res.getIsInferred());
			assertEquals(false,res.getHasDiagram());
			assertEquals(true,res.getDoRelease());
			assertEquals((Integer)141212,res.getLiteratureReferences().get(0).getDbId());
			assertEquals((Integer)450358,res.getOrthologousEvents().get(0).getDbId());
			assertEquals((Integer)114284,res.getPrecedingEvents().get(0).getDbId());
			assertEquals((Integer)48887,res.getSpecies().getDbId());
			assertEquals((Integer)212974,res.getSummations().get(0).getDbId());
			assertEquals((Integer)114254,res.getHasEvents().get(0).getDbId());
			assertEquals("Formation of apoptosome", res.getDisplayName());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAnotherObject(){
		try {
			 rc.getFullObjectForDbId(71336);
		} catch (Exception e){
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/pathway.xml");
			ReactomePathway res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getLiteratureReferences().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getOrthologousEvents().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getPrecedingEvents().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSummations().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getHasEvents().get(0).getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


}
