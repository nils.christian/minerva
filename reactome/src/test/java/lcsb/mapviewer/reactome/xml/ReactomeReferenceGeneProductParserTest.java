package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeReferenceGeneProductParserTest extends ReactomeTestFunctions {
	Logger															logger	= Logger.getLogger(ReactomeReferenceGeneProductParserTest.class);

	@Autowired
	ReactomeReferenceGeneProductParser	parser	= new ReactomeReferenceGeneProductParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceGeneProduct.xml");
			ReactomeReferenceGeneProduct res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 4546409, res.getCreated().getDbId());
			assertEquals((Integer) 4556528, res.getDbId());
			assertEquals("Ensembl:DDB0184541", res.getDisplayName());
			assertEquals("DDB0184541", res.getIdentifier());
			assertEquals("ENSG00000148672", res.getOtherIdentifier().get(0));
			assertEquals("A7319A840F57FBB2", res.getChecksum());
			assertEquals(
					"CATALYTIC ACTIVITY L-glutamate + H(2)O + NAD(P)(+) = 2-oxoglutarate + NH(3) + NAD(P)H.ENZYME REGULATION Subject to allosteric regulation. Activated by ADP. Inhibited by GTP and ATP. ADP can occupy the NADH binding site and activate the enzyme.SUBUNIT Homohexamer.PTM Stoichiometry shows that ADP-ribosylation occurs in one subunit per catalytically active homohexamer.DISEASE Familial hyperinsulinemic hypoglycemia 6 (HHF6) [MIM:606762]: Familial hyperinsulinemic hypoglycemia [MIM:256450], also referred to as congenital hyperinsulinism, nesidioblastosis, or persistent hyperinsulinemic hypoglycemia of infancy (PPHI), is the most common cause of persistent hypoglycemia in infancy and is due to defective negative feedback regulation of insulin secretion by low glucose levels. In HHF6 elevated oxidation rate of glutamate to alpha-ketoglutarate stimulates insulin secretion in the pancreatic beta cells, while they impair detoxification of ammonium in the liver. Note=The disease is caused by mutations affecting the gene represented in this entry.SIMILARITY Belongs to the Glu/Leu/Phe/Val dehydrogenases family.",
					res.getComments().get(0));
			assertEquals("recommendedName: Glutamate dehydrogenase 1, mitochondrial shortName:GDH 1 ecNumber1.4.1.3/ecNumber ", res.getDescriptions().get(0));
			assertEquals("GLUD1", res.getGeneNames().get(0));
			assertEquals("3D-structure", res.getKeywords().get(0));
			assertEquals("Q5TBU3", res.getSecondaryIdentifiers().get(0));
			assertEquals("false", res.getIsSequenceChanged());
			assertEquals((Integer) 558, res.getSequenceLength());

			assertEquals((Integer) 2, res.getReferenceDatabase().getDbId());
			assertEquals((Integer) 48895, res.getSpecies().getDbId());
			assertEquals((Integer) 10030875, res.getReferenceGenes().get(0).getDbId());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceGeneProduct.xml");
			ReactomeReferenceGeneProduct res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceDatabase().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getReferenceGenes().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
