package lcsb.mapviewer.reactome.xml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReactomeBlackBoxEventParserTest.class,//
		ReactomeCatalystActivityParserTest.class,//
		ReactomeCandidateSetParserTest.class,//
		ReactomeCompartmentParserTest.class,//
		ReactomeComplexParserTest.class,//
		ReactomeDatabaseIdentifierParserTest.class,//
		ReactomeDatabaseObjectParserTest.class,//
		ReactomeDefinedSetParserTest.class,//
		ReactomeDiseaseParserTest.class,//
		ReactomeEntityCompartmentParserTest.class,//
		ReactomeEntityWithAccessionedSequenceParserTest.class,//
		ReactomeFigureParserTest.class,//
		ReactomeGenomeEncodedEntityParserTest.class,//
		ReactomeGoBiologicalProcessParserTest.class,//
		ReactomeGoMolecularFunctionParserTest.class,//
		ReactomeInstanceEditParserTest.class,//
		ReactomeLiteratureReferenceParserTest.class,//
		ReactomeOtherEntityParserTest.class,//
		ReactomeParserFactoryTest.class,//
		ReactomePathwayParserTest.class,//
		ReactomePersonParserTest.class,//
		ReactomePolymerParserTest.class,//
		ReactomeReactionParserTest.class,//
		ReactomeReferenceDatabaseParserTest.class,//
		ReactomeReferenceGeneProductParserTest.class,//
		ReactomeReferenceIsoformParserTest.class,//
		ReactomeReferenceMoleculeParserTest.class,//
		ReactomeSimpleEntityParserTest.class,//
		ReactomeSpeciesParserTest.class,//
		ReactomeStableIdentifierParserTest.class,//
		ReactomeSummationParserTest.class,//
		ReactomeURLParserTest.class,//
		GeneralTests.class,//

})
public class AllReactomeXmlTests {

}
