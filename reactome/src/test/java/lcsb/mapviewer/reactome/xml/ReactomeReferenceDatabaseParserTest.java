package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeReferenceDatabase;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeReferenceDatabaseParserTest extends ReactomeTestFunctions {
	Logger													logger	= Logger.getLogger(ReactomeReferenceDatabaseParserTest.class);

	@Autowired
	ReactomeReferenceDatabaseParser	parser	= new ReactomeReferenceDatabaseParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceDatabase.xml");
			ReactomeReferenceDatabase res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 72811, res.getCreated().getDbId());
			assertEquals((Integer) 72810, res.getDbId());
			assertEquals("http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=###ID###", res.getAccessUrl());
			assertEquals("NCBI_taxonomy", res.getNames().get(1));
			assertEquals("http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html", res.getUrl());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/referenceDatabase.xml");
			ReactomeReferenceDatabase res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
