package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

public class ReactomeDefinedSetParserTest extends ReactomeTestFunctions {
	Logger									 logger	= Logger.getLogger(ReactomeDefinedSetParserTest.class);

	@Autowired
	ReactomeDefinedSetParser parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/definedSet.xml");
			ReactomeDefinedSet res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 453352, res.getCreated().getDbId());
			assertEquals((Integer) 453350, res.getDbId());
			assertEquals((Integer) 453376, res.getModified().get(0).getDbId());
			assertEquals((Integer) 1107637, res.getStableIdentifier().getDbId());
			assertEquals((Integer) 70101, res.getCompartments().get(0).getDbId());
			assertEquals((Integer) 9178869, res.getInferredTos().get(0).getDbId());
			assertEquals((Integer) 453360, res.getHasMembers().get(0).getDbId());
			assertEquals((Integer) 48887, res.getSpecies().get(0).getDbId());
			assertEquals((Integer) 445353, res.getAuthored().getDbId());

			assertEquals("((1,6)-alpha-glucosyl)poly((1,4)-alpha-glucosyl)glycogenin dimer", res.getNames().get(0));
			assertEquals("((1,6)-alpha-glucosyl)poly((1,4)-alpha-glucosyl)glycogenin dimer [cytosol]", res.getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/definedSet.xml");
			ReactomeDefinedSet res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInferredTos().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getHasMembers().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getAuthored().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
