package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomePerson;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomePersonParserTest extends ReactomeTestFunctions {
	Logger								logger	= Logger.getLogger(ReactomePersonParserTest.class);

	@Autowired
	ReactomePersonParser	parser	= new ReactomePersonParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/person.xml");
			ReactomePerson res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 73925, res.getModified().get(0).getDbId());
			assertEquals((Integer) 70169, res.getDbId());
			assertEquals("Schmidt, EE", res.getDisplayName());
			assertEquals("Esther", res.getFirstname());
			assertEquals("EE", res.getInitial());
			assertEquals("Schmidt", res.getSurname());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/person.xml");
			ReactomePerson res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
