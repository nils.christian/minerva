package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeLiteratureReference;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeLiteratureReferenceParserTest extends ReactomeTestFunctions {
	Logger														logger	= Logger.getLogger(ReactomeLiteratureReferenceParserTest.class);

	@Autowired
	ReactomeLiteratureReferenceParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/literatureReference.xml");
			ReactomeLiteratureReference res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);

			assertEquals((Integer) 71546, res.getDbId());
			assertEquals("Glycogen debranching enzyme: purification, antibody characterization, and immunoblot analyses of type III glycogen storage disease.",
					res.getDisplayName());
			assertEquals((Integer) 3230535, res.getModified().get(0).getDbId());
			assertEquals((Integer) 71549, res.getAuthors().get(3).getDbId());
			assertEquals("Glycogen debranching enzyme: purification, antibody characterization, and immunoblot analyses of type III glycogen storage disease.",
					res.getTitle());
			assertEquals("Am J Hum Genet", res.getJournal());
			assertEquals("1002-15", res.getPages());
			assertEquals((Integer) 41, res.getVolume());
			assertEquals((Integer) 2961257, res.getPubMedIdentifier());
			assertEquals((Integer) 1988, res.getYear());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/literatureReference.xml");
			ReactomeLiteratureReference res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getAuthors().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
