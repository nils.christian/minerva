package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeStableIdentifier;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeStableIdentifierParserTest extends ReactomeTestFunctions {
	Logger													logger	= Logger.getLogger(ReactomeStableIdentifierParserTest.class);

	@Autowired
	ReactomeStableIdentifierParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/stableIdentifier.xml");
			ReactomeStableIdentifier res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 4529165, res.getDbId());
			assertEquals("REACT_83785.9", res.getDisplayName());
			assertEquals("REACT_83785", res.getIdentifier());
			assertEquals("9", res.getIdentifierVersion());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/stableIdentifier.xml");
			ReactomeStableIdentifier res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
