package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeStatus;
import lcsb.mapviewer.reactome.model.ReactomeSummation;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeSummationParserTest extends ReactomeTestFunctions {
	Logger									logger	= Logger.getLogger(ReactomeSummationParserTest.class);

	@Autowired
	ReactomeSummationParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/summation.xml");
			ReactomeSummation res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 584171, res.getCreated().getDbId());
			assertEquals((Integer) 3639721, res.getDbId());
			assertEquals((Integer) 213418, res.getLiteratureReferences().get(0).getDbId());

			assertEquals("This complex/polymer has been computationally inferred (base...", res.getDisplayName());
			assertEquals(
					"This complex/polymer has been computationally inferred (based on Ensembl Compara) from a complex/polymer involved in an event that has been demonstrated in another species.",
					res.getText());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testUpdateObjectFromDb() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/summation.xml");
			ReactomeSummation res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getLiteratureReferences().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
