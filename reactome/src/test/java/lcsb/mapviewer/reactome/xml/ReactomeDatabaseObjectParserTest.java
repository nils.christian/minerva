package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;

public class ReactomeDatabaseObjectParserTest extends ReactomeTestFunctions {

	@Autowired
	ReactomeDatabaseObjectParser	parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSimpleParser() {
		try {
			Node node = getNodeFromXmlString(readFile("testFiles/reactome/SimpleObject.xml"));
			ReactomeDatabaseObject object = parser.parseSimplifiedObject(node);
			assertEquals(ReactomeStatus.ONLY_ID, object.getStatus());
			assertEquals(434340, (int) object.getDbId());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
