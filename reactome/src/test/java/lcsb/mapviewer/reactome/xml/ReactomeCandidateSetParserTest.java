package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

public class ReactomeCandidateSetParserTest extends ReactomeTestFunctions {
	Logger										 logger	= Logger.getLogger(ReactomeCandidateSetParserTest.class);

	@Autowired
	ReactomeCandidateSetParser parser;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/candidateSet.xml");
			ReactomeCandidateSet res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 549096, res.getCreated().getDbId());
			assertEquals((Integer) 549113, res.getDbId());
			assertEquals((Integer) 549119, res.getModified().get(0).getDbId());
			assertEquals((Integer) 1106869, res.getStableIdentifier().getDbId());
			assertEquals((Integer) 17906, res.getCompartments().get(0).getDbId());
			assertEquals((Integer) 198809, res.getInferredTos().get(0).getDbId());
			assertEquals((Integer) 61855, res.getHasMembers().get(0).getDbId());
			assertEquals((Integer) 549076, res.getHasCandidates().get(0).getDbId());
			assertEquals((Integer) 48887, res.getSpecies().get(0).getDbId());
			assertEquals((Integer) 3508822, res.getEdited().get(0).getDbId());

			assertEquals("GPAM or GPAT2", res.getNames().get(0));
			assertEquals("GPAM or GPAT2 [mitochondrial outer membrane]", res.getDisplayName());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/candidateSet.xml");
			ReactomeCandidateSet res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getInferredTos().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getHasMembers().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getHasCandidates().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getEdited().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().get(0).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
