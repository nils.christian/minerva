package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeInstanceEdit;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeInstanceEditParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeInstanceEditParserTest.class);

	@Autowired
	ReactomeInstanceEditParser parser ;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/instanceEdit.xml");
			ReactomeInstanceEdit res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer)3895160,res.getDbId());
			assertEquals("Schmidt, EE",res.getDisplayName());
			assertEquals((Integer)70169,res.getAuthor().getDbId());
			assertEquals("inferred events based on ensembl compara",res.getNote());
			assertEquals((Integer)3895160,res.getDbId());
			Calendar time = res.getDateTime();
			assertEquals(2013,time.get(Calendar.YEAR));
			assertEquals(6,time.get(Calendar.DATE));
			//indexed from 0...
			assertEquals(5,time.get(Calendar.MONTH));
			assertEquals(17,time.get(Calendar.HOUR_OF_DAY));
			assertEquals(45,time.get(Calendar.MINUTE));
			assertEquals(31,time.get(Calendar.SECOND));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}
	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/instanceEdit.xml");
			ReactomeInstanceEdit res = parser.parseObject(document.getChildNodes().item(0));
			
			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getAuthor().getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
