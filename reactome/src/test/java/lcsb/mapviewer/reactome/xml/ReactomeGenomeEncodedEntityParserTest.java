package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;
import lcsb.mapviewer.reactome.model.ReactomeStatus;

public class ReactomeGenomeEncodedEntityParserTest extends ReactomeTestFunctions {
	Logger														logger = Logger.getLogger(ReactomeGenomeEncodedEntityParserTest.class);

	@Autowired
	ReactomeGenomeEncodedEntityParser	parser = new ReactomeGenomeEncodedEntityParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/genomeEncodedEntity.xml");
			ReactomeGenomeEncodedEntity res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
			assertEquals((Integer) 500674, res.getDbId());
			assertEquals((Integer) 500675, res.getCreated().getDbId());
			assertEquals((Integer) 500696, res.getModified().get(0).getDbId());
			assertEquals((Integer) 1106207, res.getStableIdentifier().getDbId());
			assertEquals((Integer) 876, res.getCompartments().get(0).getDbId());
			assertEquals((Integer) 48887, res.getSpecies().getDbId());
			assertEquals("unidentified caspase acting on ZO-2", res.getNames().get(0));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testUpdateObjectFromDb() throws Exception {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/genomeEncodedEntity.xml");
			ReactomeGenomeEncodedEntity res = parser.parseObject(document.getChildNodes().item(0));

			rc.updateOnlyIdFields(res);
			assertEquals(ReactomeStatus.FULL, res.getCreated().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getModified().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getStableIdentifier().getStatus());
			assertEquals(ReactomeStatus.FULL, res.getCompartments().get(0).getStatus());
			assertEquals(ReactomeStatus.FULL, res.getSpecies().getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
