package lcsb.mapviewer.reactome.xml;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import lcsb.mapviewer.reactome.ReactomeTestFunctions;
import lcsb.mapviewer.reactome.model.ReactomeDepolymerisation;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

public class ReactomeDepolymerisationParserTest extends ReactomeTestFunctions{
	Logger logger = Logger.getLogger(ReactomeDepolymerisationParserTest.class);

	@Autowired
	ReactomeDepolymerisationParser parser ;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseObject() {
		try {
			Document document = getXmlDocumentFromFile("testFiles/reactome/depolymerisation.xml");
			ReactomeDepolymerisation res = parser.parseObject(document.getChildNodes().item(0));
			assertNotNull(res);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
}
