/**
 * Provides structures for reactome model. More information about reactome model
 * can be found in the following links:
 * <ul>
 * <li><a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">Glossary
 * Data Model</a></li>
 * <li><a href="http://www.reactome.org/cgi-bin/classbrowser">Reactom class
 * browser</a></li>
 * </ul>
 */
package lcsb.mapviewer.reactome.model;

