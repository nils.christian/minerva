package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceEntity"
 * >ReferenceEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceEntity extends ReactomeDatabaseObject {
	/**
	 * Some identifier param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String														identifier;
	/**
	 * ReferenceDatabase param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceDatabase					referenceDatabase;
	/**
	 * List of DatabaseIdentifier params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDatabaseIdentifier>	crossReferences	= new ArrayList<ReactomeDatabaseIdentifier>();
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>											names						= new ArrayList<String>();
	/**
	 * List of other identifiers in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>											otherIdentifier	= new ArrayList<String>();

	/**
	 * Adds reference to {@link #crossReferences} list.
	 * 
	 * @param crossReference
	 *          object to add
	 */
	public void addCrossReferences(ReactomeDatabaseIdentifier crossReference) {
		this.crossReferences.add(crossReference);
	}

	/**
	 * Adds name to {@link #names} list.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		this.names.add(name);
	}

	/**
	 * Adds other identifier to {@link #otherIdentifier} list.
	 * 
	 * @param otherIdentifier
	 *          object to add
	 */
	public void addOtherIdentifier(String otherIdentifier) {
		this.otherIdentifier.add(otherIdentifier);
	}

	/**
	 * @return the identifier
	 * @see #identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier
	 *          the identifier to set
	 * @see #identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the referenceDatabase
	 * @see #referenceDatabase
	 */
	public ReactomeReferenceDatabase getReferenceDatabase() {
		return referenceDatabase;
	}

	/**
	 * @param referenceDatabase
	 *          the referenceDatabase to set
	 * @see #referenceDatabase
	 */
	public void setReferenceDatabase(ReactomeReferenceDatabase referenceDatabase) {
		this.referenceDatabase = referenceDatabase;
	}

	/**
	 * @return the crossReferences
	 * @see #crossReferences
	 */
	public List<ReactomeDatabaseIdentifier> getCrossReferences() {
		return crossReferences;
	}

	/**
	 * @param crossReferences
	 *          the crossReferences to set
	 * @see #crossReferences
	 */
	public void setCrossReferences(List<ReactomeDatabaseIdentifier> crossReferences) {
		this.crossReferences = crossReferences;
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @return the otherIdentifier
	 * @see #otherIdentifier
	 */
	public List<String> getOtherIdentifier() {
		return otherIdentifier;
	}

	/**
	 * @param otherIdentifier
	 *          the otherIdentifier to set
	 * @see #otherIdentifier
	 */
	public void setOtherIdentifier(List<String> otherIdentifier) {
		this.otherIdentifier = otherIdentifier;
	}

}
