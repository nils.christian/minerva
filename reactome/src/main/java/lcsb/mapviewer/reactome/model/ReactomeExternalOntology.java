package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ExternalOntology"
 * >ExternalOntology</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeExternalOntology extends ReactomeDatabaseObject {
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>							names			= new ArrayList<String>();
	/**
	 * List of synonyms in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>							synonyms	= new ArrayList<String>();
	/**
	 * Definition param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String										definition;
	/**
	 * Identifier param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer										identifier;
	/**
	 * ReferenceDatabase param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceDatabase	referenceDatabase;

	/**
	 * Adds name to {@link #names}.
	 * 
	 * @param name
	 *          name to add
	 */
	public void addName(String name) {
		this.names.add(name);
	}

	/**
	 * Adds synonym to {@link #synonyms}.
	 * 
	 * @param synonym
	 *          synonym to add
	 */
	public void addSynonym(String synonym) {
		this.synonyms.add(synonym);
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @return the synonyms
	 * @see #synonyms
	 */
	public List<String> getSynonyms() {
		return synonyms;
	}

	/**
	 * @param synonyms
	 *          the synonyms to set
	 * @see #synonyms
	 */
	public void setSynonyms(List<String> synonyms) {
		this.synonyms = synonyms;
	}

	/**
	 * @return the definition
	 * @see #definition
	 */
	public String getDefinition() {
		return definition;
	}

	/**
	 * @param definition
	 *          the definition to set
	 * @see #definition
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	/**
	 * @return the identifier
	 * @see #identifier
	 */
	public Integer getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier
	 *          the identifier to set
	 * @see #identifier
	 */
	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the referenceDatabase
	 * @see #referenceDatabase
	 */
	public ReactomeReferenceDatabase getReferenceDatabase() {
		return referenceDatabase;
	}

	/**
	 * @param referenceDatabase
	 *          the referenceDatabase to set
	 * @see #referenceDatabase
	 */
	public void setReferenceDatabase(ReactomeReferenceDatabase referenceDatabase) {
		this.referenceDatabase = referenceDatabase;
	}
}
