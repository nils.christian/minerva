package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionCoordinates"
 * >ReactionCoordinates</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionCoordinates extends ReactomeDatabaseObject {

	/**
	 * Source x param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	sourceX;
	/**
	 * Source y param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	sourceY;
	/**
	 * Target x param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	targetX;
	/**
	 * Target x param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	targetY;

	/**
	 * @return the sourceX
	 * @see #sourceX
	 */
	public Integer getSourceX() {
		return sourceX;
	}

	/**
	 * @param sourceX
	 *          the sourceX to set
	 * @see #sourceX
	 */
	public void setSourceX(Integer sourceX) {
		this.sourceX = sourceX;
	}

	/**
	 * @return the sourceY
	 * @see #sourceY
	 */
	public Integer getSourceY() {
		return sourceY;
	}

	/**
	 * @param sourceY
	 *          the sourceY to set
	 * @see #sourceY
	 */
	public void setSourceY(Integer sourceY) {
		this.sourceY = sourceY;
	}

	/**
	 * @return the targetX
	 * @see #targetX
	 */
	public Integer getTargetX() {
		return targetX;
	}

	/**
	 * @param targetX
	 *          the targetX to set
	 * @see #targetX
	 */
	public void setTargetX(Integer targetX) {
		this.targetX = targetX;
	}

	/**
	 * @return the targetY
	 * @see #targetY
	 */
	public Integer getTargetY() {
		return targetY;
	}

	/**
	 * @param targetY
	 *          the targetY to set
	 * @see #targetY
	 */
	public void setTargetY(Integer targetY) {
		this.targetY = targetY;
	}

}
