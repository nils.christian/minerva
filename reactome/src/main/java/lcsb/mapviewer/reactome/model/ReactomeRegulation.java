package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Regulation"
 * >Regulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeRegulation extends ReactomeDatabaseObject {
	
	/**
	 * Event param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeEvent					 regulatedEntity;
	
	/**
	 * DatabaseObject param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeDatabaseObject regulator;

	/**
	 * If a reaction is regulated in different ways in different pathway contexts,
	 * the containedInPathway allows curator to specify this. More information can
	 * be found <a href=
	 * "http://wiki.reactome.org/index.php/Glossary_Data_Model#Regulation">here
	 * </a>.
	 */
	private ReactomePathway				 containedInPathway;

	/**
	 * @return the regulatedEntity
	 * @see #regulatedEntity
	 */
	public ReactomeEvent getRegulatedEntity() {
		return regulatedEntity;
	}

	/**
	 * @param regulatedEntity
	 *          the regulatedEntity to set
	 * @see #regulatedEntity
	 */
	public void setRegulatedEntity(ReactomeEvent regulatedEntity) {
		this.regulatedEntity = regulatedEntity;
	}

	/**
	 * @return the regulator
	 * @see #regulator
	 */
	public ReactomeDatabaseObject getRegulator() {
		return regulator;
	}

	/**
	 * @param regulator
	 *          the regulator to set
	 * @see #regulator
	 */
	public void setRegulator(ReactomeDatabaseObject regulator) {
		this.regulator = regulator;
	}

	/**
	 * @return the containedInPathway
	 * @see #containedInPathway
	 */
	public ReactomePathway getContainedInPathway() {
		return containedInPathway;
	}

	/**
	 * @param containedInPathway
	 *          the containedInPathway to set
	 * @see #containedInPathway
	 */
	public void setContainedInPathway(ReactomePathway containedInPathway) {
		this.containedInPathway = containedInPathway;
	}

}
