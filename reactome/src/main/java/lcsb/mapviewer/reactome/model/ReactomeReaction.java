package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Reaction"
 * >Reaction</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReaction extends ReactomeReactionlikeEvent {
	/**
	 * Reaction param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReaction	reverseReaction;

	/**
	 * @return the reverseReaction
	 * @see #reverseReaction
	 */
	public ReactomeReaction getReverseReaction() {
		return reverseReaction;
	}

	/**
	 * @param reverseReaction
	 *          the reverseReaction to set
	 * @see #reverseReaction
	 */
	public void setReverseReaction(ReactomeReaction reverseReaction) {
		this.reverseReaction = reverseReaction;
	}

}
