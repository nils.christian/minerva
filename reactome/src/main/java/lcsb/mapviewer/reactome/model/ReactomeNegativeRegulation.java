package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=NegativeRegulation"
 * >NegativeRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeNegativeRegulation extends ReactomeRegulation {
}
