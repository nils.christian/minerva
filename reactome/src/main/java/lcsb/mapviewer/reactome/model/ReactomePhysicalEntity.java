package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PhysicalEntity"
 * >PhysicalEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePhysicalEntity extends ReactomeDatabaseObject {

	/**
	 * List of EntityCompartment params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEntityCompartment>		compartments					= new ArrayList<ReactomeEntityCompartment>();
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>			inferredTos						= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>			inferredFroms					= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * InstanceEdit param in the reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeInstanceEdit							authored;
	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>				reviseds							= new ArrayList<ReactomeInstanceEdit>();
	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>				revieweds							= new ArrayList<ReactomeInstanceEdit>();
	/**
	 * List of Figure params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeFigure>							figures								= new ArrayList<ReactomeFigure>();
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>											names									= new ArrayList<String>();
	/**
	 * List of Summation params in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeSummation>						summations						= new ArrayList<ReactomeSummation>();
	/**
	 * List of DatabaseIdentifier params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDatabaseIdentifier>	crossReferences				= new ArrayList<ReactomeDatabaseIdentifier>();
	/**
	 * List of Publication params in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePublication>					literatureReferences	= new ArrayList<ReactomePublication>();
	/**
	 * GO_CellularComponent param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeGoCellularComponent				goCellularComponent;
	/**
	 * List of InstanceEdit params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>				edited								= new ArrayList<ReactomeInstanceEdit>();
	/**
	 * List of Disease params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDisease>							diseases							= new ArrayList<ReactomeDisease>();
	/**
	 * Definition param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String														definition;
	
	/**
	 * Definition param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String														systematicName;

	/**
	 * Adds compartment to {@link #compartments} list.
	 * 
	 * @param compartment
	 *          object to add
	 */
	public void addCompartment(ReactomeEntityCompartment compartment) {
		compartments.add(compartment);
	}

	/**
	 * Adds PhysicalEntity to {@link #inferredTos} list.
	 * 
	 * @param inferredTo
	 *          object to add
	 */
	public void addInferredTo(ReactomePhysicalEntity inferredTo) {
		inferredTos.add(inferredTo);
	}

	/**
	 * Adds PhysicalEntity to {@link #inferredFroms} list.
	 * 
	 * @param inferredFrom
	 *          inferredFroms to add
	 */
	public void addInferredFrom(ReactomePhysicalEntity inferredFrom) {
		inferredFroms.add(inferredFrom);
	}

	/**
	 * Adds name to the {@link #names} list.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		names.add(name);
	}

	/**
	 * Adds sumamtion to {@link #summations} list.
	 * 
	 * @param summation
	 *          object to add
	 */
	public void addSummation(ReactomeSummation summation) {
		summations.add(summation);
	}

	/**
	 * Adds reference to {@link #crossReferences} list.
	 * 
	 * @param crossReference
	 *          object to add
	 */
	public void addCrossReference(ReactomeDatabaseIdentifier crossReference) {
		crossReferences.add(crossReference);
	}

	/**
	 * Adds reference to {@link #literatureReferences} list.
	 * 
	 * @param literatureReference
	 *          object to add
	 */
	public void addLiteratureReference(ReactomePublication literatureReference) {
		this.literatureReferences.add(literatureReference);
	}

	/**
	 * Adds element to {@link #edited} list.
	 * 
	 * @param edited
	 *          object to add
	 */
	public void addEdited(ReactomeInstanceEdit edited) {
		this.edited.add(edited);
	}

	/**
	 * Adds element to {@link #reviseds} list.
	 * 
	 * @param revised
	 *          object to add
	 */
	public void addRevised(ReactomeInstanceEdit revised) {
		this.reviseds.add(revised);
	}

	/**
	 * Adds figure to {@link #figures} list.
	 * 
	 * @param figure
	 *          object to add
	 */
	public void addFigure(ReactomeFigure figure) {
		this.figures.add(figure);
	}

	/**
	 * Adds disease to {@link #diseases} list.
	 * 
	 * @param disease
	 *          object to add
	 */
	public void addDisease(ReactomeDisease disease) {
		this.diseases.add(disease);
	}

	/**
	 * Adds element to {@link #revieweds} list.
	 * 
	 * @param reviewed
	 *          object to add
	 */
	public void addReviewed(ReactomeInstanceEdit reviewed) {
		this.revieweds.add(reviewed);
	}

	/**
	 * @return the compartments
	 * @see #compartments
	 */
	public List<ReactomeEntityCompartment> getCompartments() {
		return compartments;
	}

	/**
	 * @param compartments
	 *          the compartments to set
	 * @see #compartments
	 */
	public void setCompartments(List<ReactomeEntityCompartment> compartments) {
		this.compartments = compartments;
	}

	/**
	 * @return the inferredTos
	 * @see #inferredTos
	 */
	public List<ReactomePhysicalEntity> getInferredTos() {
		return inferredTos;
	}

	/**
	 * @param inferredTos
	 *          the inferredTos to set
	 * @see #inferredTos
	 */
	public void setInferredTos(List<ReactomePhysicalEntity> inferredTos) {
		this.inferredTos = inferredTos;
	}

	/**
	 * @return the inferredFroms
	 * @see #inferredFroms
	 */
	public List<ReactomePhysicalEntity> getInferredFroms() {
		return inferredFroms;
	}

	/**
	 * @param inferredFroms
	 *          the inferredFroms to set
	 * @see #inferredFroms
	 */
	public void setInferredFroms(List<ReactomePhysicalEntity> inferredFroms) {
		this.inferredFroms = inferredFroms;
	}

	/**
	 * @return the authored
	 * @see #authored
	 */
	public ReactomeInstanceEdit getAuthored() {
		return authored;
	}

	/**
	 * @param authored
	 *          the authored to set
	 * @see #authored
	 */
	public void setAuthored(ReactomeInstanceEdit authored) {
		this.authored = authored;
	}

	/**
	 * @return the reviseds
	 * @see #reviseds
	 */
	public List<ReactomeInstanceEdit> getReviseds() {
		return reviseds;
	}

	/**
	 * @param reviseds
	 *          the reviseds to set
	 * @see #reviseds
	 */
	public void setReviseds(List<ReactomeInstanceEdit> reviseds) {
		this.reviseds = reviseds;
	}

	/**
	 * @return the revieweds
	 * @see #revieweds
	 */
	public List<ReactomeInstanceEdit> getRevieweds() {
		return revieweds;
	}

	/**
	 * @param revieweds
	 *          the revieweds to set
	 * @see #revieweds
	 */
	public void setRevieweds(List<ReactomeInstanceEdit> revieweds) {
		this.revieweds = revieweds;
	}

	/**
	 * @return the figures
	 * @see #figures
	 */
	public List<ReactomeFigure> getFigures() {
		return figures;
	}

	/**
	 * @param figures
	 *          the figures to set
	 * @see #figures
	 */
	public void setFigures(List<ReactomeFigure> figures) {
		this.figures = figures;
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @return the summations
	 * @see #summations
	 */
	public List<ReactomeSummation> getSummations() {
		return summations;
	}

	/**
	 * @param summations
	 *          the summations to set
	 * @see #summations
	 */
	public void setSummations(List<ReactomeSummation> summations) {
		this.summations = summations;
	}

	/**
	 * @return the crossReferences
	 * @see #crossReferences
	 */
	public List<ReactomeDatabaseIdentifier> getCrossReferences() {
		return crossReferences;
	}

	/**
	 * @param crossReferences
	 *          the crossReferences to set
	 * @see #crossReferences
	 */
	public void setCrossReferences(List<ReactomeDatabaseIdentifier> crossReferences) {
		this.crossReferences = crossReferences;
	}

	/**
	 * @return the literatureReferences
	 * @see #literatureReferences
	 */
	public List<ReactomePublication> getLiteratureReferences() {
		return literatureReferences;
	}

	/**
	 * @param literatureReferences
	 *          the literatureReferences to set
	 * @see #literatureReferences
	 */
	public void setLiteratureReferences(List<ReactomePublication> literatureReferences) {
		this.literatureReferences = literatureReferences;
	}

	/**
	 * @return the goCellularComponent
	 * @see #goCellularComponent
	 */
	public ReactomeGoCellularComponent getGoCellularComponent() {
		return goCellularComponent;
	}

	/**
	 * @param goCellularComponent
	 *          the goCellularComponent to set
	 * @see #goCellularComponent
	 */
	public void setGoCellularComponent(ReactomeGoCellularComponent goCellularComponent) {
		this.goCellularComponent = goCellularComponent;
	}

	/**
	 * @return the edited
	 * @see #edited
	 */
	public List<ReactomeInstanceEdit> getEdited() {
		return edited;
	}

	/**
	 * @param edited
	 *          the edited to set
	 * @see #edited
	 */
	public void setEdited(List<ReactomeInstanceEdit> edited) {
		this.edited = edited;
	}

	/**
	 * @return the diseases
	 * @see #diseases
	 */
	public List<ReactomeDisease> getDiseases() {
		return diseases;
	}

	/**
	 * @param diseases
	 *          the diseases to set
	 * @see #diseases
	 */
	public void setDiseases(List<ReactomeDisease> diseases) {
		this.diseases = diseases;
	}

	/**
	 * @return the definition
	 * @see #definition
	 */
	public String getDefinition() {
		return definition;
	}

	/**
	 * @param definition
	 *          the definition to set
	 * @see #definition
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	/**
	 * @return the systematicName
	 * @see #systematicName
	 */
	public String getSystematicName() {
		return systematicName;
	}

	/**
	 * @param systematicName the systematicName to set
	 * @see #systematicName
	 */
	public void setSystematicName(String systematicName) {
		this.systematicName = systematicName;
	}
}
