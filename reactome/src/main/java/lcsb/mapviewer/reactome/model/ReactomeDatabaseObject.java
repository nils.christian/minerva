package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DatabaseObject"
 * >DatabaseObject</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDatabaseObject {
	/**
	 * Database identifier of the object in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer											dbId;
	/**
	 * What is the local status of the object. There are three possibillities:
	 * <ul>
	 * <li> {@link ReactomeStatus#UNINITIALIZED UNINITIALIZED} - when object was
	 * created but it wasn't connected to any object in reactome db</li>
	 * <li> {@link ReactomeStatus#ONLY_ID ONLY_ID} - when object was created and
	 * basic information from reactome db has been retrieved</li>
	 * <li> {@link ReactomeStatus#FULL FULL} - when full information about object
	 * was retrieved from reactome db</li>
	 * </ul>
	 * 
	 */
	private ReactomeStatus							status		= ReactomeStatus.UNINITIALIZED;
	/**
	 * InstanceEdit param in the reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeInstanceEdit				created;
	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>	modified	= new ArrayList<ReactomeInstanceEdit>();
	/**
	 * Displayed name of the object in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String											displayName;
	/**
	 * String describing reactome class of the object. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String											schemaClass;
	/**
	 * Stable Identifier (publicly available identifier) of the object. More
	 * information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeStableIdentifier		stableIdentifier;

	/**
	 * Adds modified into {@link #modified} list.
	 * 
	 * @param modified
	 *          object to add
	 */
	public void addModified(ReactomeInstanceEdit modified) {
		this.modified.add(modified);
	}

	/**
	 * @return the dbId
	 * @see #dbId
	 */
	public Integer getDbId() {
		return dbId;
	}

	/**
	 * @param dbId
	 *          the dbId to set
	 * @see #dbId
	 */
	public void setDbId(Integer dbId) {
		this.dbId = dbId;
	}

	/**
	 * @return the status
	 * @see #status
	 */
	public ReactomeStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *          the status to set
	 * @see #status
	 */
	public void setStatus(ReactomeStatus status) {
		this.status = status;
	}

	/**
	 * @return the created
	 * @see #created
	 */
	public ReactomeInstanceEdit getCreated() {
		return created;
	}

	/**
	 * @param created
	 *          the created to set
	 * @see #created
	 */
	public void setCreated(ReactomeInstanceEdit created) {
		this.created = created;
	}

	/**
	 * @return the displayName
	 * @see #displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName
	 *          the displayName to set
	 * @see #displayName
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the stableIdentifier
	 * @see #stableIdentifier
	 */
	public ReactomeStableIdentifier getStableIdentifier() {
		return stableIdentifier;
	}

	/**
	 * @param stableIdentifier
	 *          the stableIdentifier to set
	 * @see #stableIdentifier
	 */
	public void setStableIdentifier(ReactomeStableIdentifier stableIdentifier) {
		this.stableIdentifier = stableIdentifier;
	}

	/**
	 * @return the schemaClass
	 * @see #schemaClass
	 */
	public String getSchemaClass() {
		return schemaClass;
	}

	/**
	 * @param schemaClass the schemaClass to set
	 * @see #schemaClass
	 */
	public void setSchemaClass(String schemaClass) {
		this.schemaClass = schemaClass;
	}

	/**
	 * @return the modified
	 * @see #modified
	 */
	public List<ReactomeInstanceEdit> getModified() {
		return modified;
	}

	/**
	 * @param modified the modified to set
	 * @see #modified
	 */
	public void setModified(List<ReactomeInstanceEdit> modified) {
		this.modified = modified;
	}

}
