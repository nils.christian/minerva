package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=URL"
 * >URL</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeURL extends ReactomePublication {
	/**
	 * Url param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	uniformResourceLocator;

	/**
	 * @return the uniformResourceLocator
	 * @see #uniformResourceLocator
	 */
	public String getUniformResourceLocator() {
		return uniformResourceLocator;
	}

	/**
	 * @param uniformResourceLocator the uniformResourceLocator to set
	 * @see #uniformResourceLocator
	 */
	public void setUniformResourceLocator(String uniformResourceLocator) {
		this.uniformResourceLocator = uniformResourceLocator;
	}

}
