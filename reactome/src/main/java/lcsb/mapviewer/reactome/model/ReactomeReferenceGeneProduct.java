package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceGeneProduct"
 * >ReferenceGeneProduct</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceGeneProduct extends ReactomeReferenceSequence {
	/**
	 * List of ReferenceDNASequence params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeReferenceDNASequence>	referenceGenes				= new ArrayList<ReactomeReferenceDNASequence>();
	/**
	 * List of ReferenceRNASequence params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeReferenceRNASequence>	referenceTranscripts	= new ArrayList<ReactomeReferenceRNASequence>();

	/**
	 * Adds reference gene to {@link #referenceGenes} list.
	 * 
	 * @param referenceGene
	 *          object to add
	 */
	public void addReferenceGene(ReactomeReferenceDNASequence referenceGene) {
		referenceGenes.add(referenceGene);
	}

	/**
	 * Adds reference rna to {@link #referenceTranscripts} list.
	 * 
	 * @param referenceTranscript
	 *          objet to add
	 */
	public void addReferenceTranscript(ReactomeReferenceRNASequence referenceTranscript) {
		referenceTranscripts.add(referenceTranscript);
	}

	/**
	 * @return the referenceGenes
	 * @see #referenceGenes
	 */
	public List<ReactomeReferenceDNASequence> getReferenceGenes() {
		return referenceGenes;
	}

	/**
	 * @param referenceGenes
	 *          the referenceGenes to set
	 * @see #referenceGenes
	 */
	public void setReferenceGenes(List<ReactomeReferenceDNASequence> referenceGenes) {
		this.referenceGenes = referenceGenes;
	}

	/**
	 * @return the referenceTranscripts
	 * @see #referenceTranscripts
	 */
	public List<ReactomeReferenceRNASequence> getReferenceTranscripts() {
		return referenceTranscripts;
	}

	/**
	 * @param referenceTranscripts
	 *          the referenceTranscripts to set
	 * @see #referenceTranscripts
	 */
	public void setReferenceTranscripts(List<ReactomeReferenceRNASequence> referenceTranscripts) {
		this.referenceTranscripts = referenceTranscripts;
	}

}
