package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionlikeEvent"
 * >ReactionlikeEvent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionlikeEvent extends ReactomeEvent {
	/**
	 * List of CatalystActivity params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeCatalystActivity>				catalystActivities				= new ArrayList<ReactomeCatalystActivity>();
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>					inputs										= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>					outputs										= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * Boolean is chimeric param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean																isChimeric;
	/**
	 * List of PhysicalEntity params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>					entityOnOtherCells				= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * List of EntityFunctionalStatus params in reactome model. More information
	 * can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEntityFunctionalStatus>	entityFunctionalStatuses	= new ArrayList<ReactomeEntityFunctionalStatus>();
	/**
	 * List of ReactionlikeEvent params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeReactionlikeEvent>				normalReactions						= new ArrayList<ReactomeReactionlikeEvent>();
	/**
	 * List of DatabaseObject params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDatabaseObject>					requiredInputComponents		= new ArrayList<ReactomeDatabaseObject>();

	/**
	 * Adds catalyst activity to {@link #catalystActivities} list.
	 * 
	 * @param catalystActivity
	 *          object to add
	 */
	public void addCatalystActivity(ReactomeCatalystActivity catalystActivity) {
		this.catalystActivities.add(catalystActivity);
	}

	/**
	 * Adds physical entity to {@link #inputs} list.
	 * 
	 * @param input
	 *          object to add
	 */
	public void addInput(ReactomePhysicalEntity input) {
		inputs.add(input);
	}

	/**
	 * Adds physical entity to {@link #outputs} list.
	 * 
	 * @param output
	 *          object to add
	 */
	public void addOutput(ReactomePhysicalEntity output) {
		outputs.add(output);
	}

	/**
	 * Adds physical entity to {@link #entityOnOtherCells} list.
	 * 
	 * @param entityOnOtherCell
	 *          object to add
	 */
	public void addEntityOnOtherCell(ReactomePhysicalEntity entityOnOtherCell) {
		this.entityOnOtherCells.add(entityOnOtherCell);
	}

	/**
	 * Adds status to {@link #entityFunctionalStatuses} list.
	 * 
	 * @param entityFunctionalStatus
	 *          object to add
	 */
	public void addEntityFunctionalStatus(ReactomeEntityFunctionalStatus entityFunctionalStatus) {
		this.entityFunctionalStatuses.add(entityFunctionalStatus);
	}

	/**
	 * Adds reaction to {@link #normalReactions} list.
	 * 
	 * @param normalReaction
	 *          object to add
	 */
	public void addNormalReactions(ReactomeReactionlikeEvent normalReaction) {
		this.normalReactions.add(normalReaction);
	}

	/**
	 * Adds object to {@link #requiredInputComponents} list.
	 * 
	 * @param requiredInputComponent
	 *          object to add
	 */
	public void addRequiredInputComponents(ReactomeDatabaseObject requiredInputComponent) {
		this.requiredInputComponents.add(requiredInputComponent);
	}

	/**
	 * @return the catalystActivities
	 * @see #catalystActivities
	 */
	public List<ReactomeCatalystActivity> getCatalystActivities() {
		return catalystActivities;
	}

	/**
	 * @param catalystActivities
	 *          the catalystActivities to set
	 * @see #catalystActivities
	 */
	public void setCatalystActivities(List<ReactomeCatalystActivity> catalystActivities) {
		this.catalystActivities = catalystActivities;
	}

	/**
	 * @return the inputs
	 * @see #inputs
	 */
	public List<ReactomePhysicalEntity> getInputs() {
		return inputs;
	}

	/**
	 * @param inputs
	 *          the inputs to set
	 * @see #inputs
	 */
	public void setInputs(List<ReactomePhysicalEntity> inputs) {
		this.inputs = inputs;
	}

	/**
	 * @return the outputs
	 * @see #outputs
	 */
	public List<ReactomePhysicalEntity> getOutputs() {
		return outputs;
	}

	/**
	 * @param outputs
	 *          the outputs to set
	 * @see #outputs
	 */
	public void setOutputs(List<ReactomePhysicalEntity> outputs) {
		this.outputs = outputs;
	}

	/**
	 * @return the isChimeric
	 * @see #isChimeric
	 */
	public Boolean getIsChimeric() {
		return isChimeric;
	}

	/**
	 * @param isChimeric
	 *          the isChimeric to set
	 * @see #isChimeric
	 */
	public void setIsChimeric(Boolean isChimeric) {
		this.isChimeric = isChimeric;
	}

	/**
	 * @return the entityOnOtherCells
	 * @see #entityOnOtherCells
	 */
	public List<ReactomePhysicalEntity> getEntityOnOtherCells() {
		return entityOnOtherCells;
	}

	/**
	 * @param entityOnOtherCells
	 *          the entityOnOtherCells to set
	 * @see #entityOnOtherCells
	 */
	public void setEntityOnOtherCells(List<ReactomePhysicalEntity> entityOnOtherCells) {
		this.entityOnOtherCells = entityOnOtherCells;
	}

	/**
	 * @return the entityFunctionalStatuses
	 * @see #entityFunctionalStatuses
	 */
	public List<ReactomeEntityFunctionalStatus> getEntityFunctionalStatuses() {
		return entityFunctionalStatuses;
	}

	/**
	 * @param entityFunctionalStatuses
	 *          the entityFunctionalStatuses to set
	 * @see #entityFunctionalStatuses
	 */
	public void setEntityFunctionalStatuses(List<ReactomeEntityFunctionalStatus> entityFunctionalStatuses) {
		this.entityFunctionalStatuses = entityFunctionalStatuses;
	}

	/**
	 * @return the normalReactions
	 * @see #normalReactions
	 */
	public List<ReactomeReactionlikeEvent> getNormalReactions() {
		return normalReactions;
	}

	/**
	 * @param normalReactions
	 *          the normalReactions to set
	 * @see #normalReactions
	 */
	public void setNormalReactions(List<ReactomeReactionlikeEvent> normalReactions) {
		this.normalReactions = normalReactions;
	}

	/**
	 * @return the requiredInputComponents
	 * @see #requiredInputComponents
	 */
	public List<ReactomeDatabaseObject> getRequiredInputComponents() {
		return requiredInputComponents;
	}

	/**
	 * @param requiredInputComponents
	 *          the requiredInputComponents to set
	 * @see #requiredInputComponents
	 */
	public void setRequiredInputComponents(List<ReactomeDatabaseObject> requiredInputComponents) {
		this.requiredInputComponents = requiredInputComponents;
	}
}
