package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Taxon"
 * >Taxon</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeTaxon extends ReactomeDatabaseObject {
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>											names						= new ArrayList<String>();
	/**
	 * List of DatabaseIdentifier params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDatabaseIdentifier>	crossReferences	= new ArrayList<ReactomeDatabaseIdentifier>();
	/**
	 * Taxon param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeTaxon											superTaxon;

	/**
	 * Adds reference to {@link #crossReferences} list.
	 * 
	 * @param crossReference
	 *          object to add
	 */
	public void addCrossReference(ReactomeDatabaseIdentifier crossReference) {
		crossReferences.add(crossReference);
	}

	/**
	 * Adds name to {@link #names} list.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		names.add(name);
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @return the crossReferences
	 * @see #crossReferences
	 */
	public List<ReactomeDatabaseIdentifier> getCrossReferences() {
		return crossReferences;
	}

	/**
	 * @param crossReferences
	 *          the crossReferences to set
	 * @see #crossReferences
	 */
	public void setCrossReferences(List<ReactomeDatabaseIdentifier> crossReferences) {
		this.crossReferences = crossReferences;
	}

	/**
	 * @return the superTaxon
	 * @see #superTaxon
	 */
	public ReactomeTaxon getSuperTaxon() {
		return superTaxon;
	}

	/**
	 * @param superTaxon
	 *          the superTaxon to set
	 * @see #superTaxon
	 */
	public void setSuperTaxon(ReactomeTaxon superTaxon) {
		this.superTaxon = superTaxon;
	}
}
