package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FunctionalStatusType"
 * >FunctionalStatusType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFunctionalStatusType extends ReactomeDatabaseObject {
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>	names	= new ArrayList<String>();

	/**
	 * Adds name to {@link #names} list.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		this.names.add(name);
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}
}
