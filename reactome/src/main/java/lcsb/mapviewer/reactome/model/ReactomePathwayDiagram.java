package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayDiagram"
 * >PathwayDiagram</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathwayDiagram extends ReactomePathwayDiagramItem {
	/**
	 * Height in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer								height;
	/**
	 * Width in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer								width;
	/**
	 * List of Pathawy params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePathway>	representedPathways	= new ArrayList<ReactomePathway>();

	/**
	 * Sterd at xml param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String								storedATXML;

	/**
	 * Adds pathway to {@link #representedPathways} list.
	 * 
	 * @param representedPathway
	 *          object to add
	 */
	public void addRepresentedPathway(ReactomePathway representedPathway) {
		this.representedPathways.add(representedPathway);
	}

	/**
	 * @return the height
	 * @see #height
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * @param height
	 *          the height to set
	 * @see #height
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

	/**
	 * @return the width
	 * @see #width
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @param width
	 *          the width to set
	 * @see #width
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * @return the representedPathways
	 * @see #representedPathways
	 */
	public List<ReactomePathway> getRepresentedPathways() {
		return representedPathways;
	}

	/**
	 * @param representedPathways
	 *          the representedPathways to set
	 * @see #representedPathways
	 */
	public void setRepresentedPathways(List<ReactomePathway> representedPathways) {
		this.representedPathways = representedPathways;
	}

	/**
	 * @return the storedATXML
	 * @see #storedATXML
	 */
	public String getStoredATXML() {
		return storedATXML;
	}

	/**
	 * @param storedATXML
	 *          the storedATXML to set
	 * @see #storedATXML
	 */
	public void setStoredATXML(String storedATXML) {
		this.storedATXML = storedATXML;
	}
}
