package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentModification"
 * >FragmentModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFragmentModification extends ReactomeGeneticallyModifiedResidue {
	/**
	 * Start position in reference sequence in reactome model. More information
	 * can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	startPositionInReferenceSequence;
	/**
	 * End position in reference sequence in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	endPositionInReferenceSequence;

	/**
	 * @return the startPositionInReferenceSequence
	 * @see #startPositionInReferenceSequence
	 */
	public Integer getStartPositionInReferenceSequence() {
		return startPositionInReferenceSequence;
	}

	/**
	 * @param startPositionInReferenceSequence
	 *          the startPositionInReferenceSequence to set
	 * @see #startPositionInReferenceSequence
	 */
	public void setStartPositionInReferenceSequence(Integer startPositionInReferenceSequence) {
		this.startPositionInReferenceSequence = startPositionInReferenceSequence;
	}

	/**
	 * @return the endPositionInReferenceSequence
	 * @see #endPositionInReferenceSequence
	 */
	public Integer getEndPositionInReferenceSequence() {
		return endPositionInReferenceSequence;
	}

	/**
	 * @param endPositionInReferenceSequence
	 *          the endPositionInReferenceSequence to set
	 * @see #endPositionInReferenceSequence
	 */
	public void setEndPositionInReferenceSequence(Integer endPositionInReferenceSequence) {
		this.endPositionInReferenceSequence = endPositionInReferenceSequence;
	}

}
