package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=StableIdentifier"
 * >StableIdentifier</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeStableIdentifier extends ReactomeDatabaseObject {

	/**
	 * Identifier in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	identifier;

	/**
	 * Identifier version in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	identifierVersion;

	/**
	 * Old identifier used in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	oldIdentifier;

	/**
	 * Old identifier version used in reactome model. More information can be
	 * found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	oldIdentifierVersion;

	/**
	 * Undocumented by reactome...
	 */
	private boolean	released;

	/**
	 * @return the identifier
	 * @see #identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier
	 *          the identifier to set
	 * @see #identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the identifierVersion
	 * @see #identifierVersion
	 */
	public String getIdentifierVersion() {
		return identifierVersion;
	}

	/**
	 * @param identifierVersion
	 *          the identifierVersion to set
	 * @see #identifierVersion
	 */
	public void setIdentifierVersion(String identifierVersion) {
		this.identifierVersion = identifierVersion;
	}

	/**
	 * @return the oldIdentifier
	 * @see #oldIdentifier
	 */
	public String getOldIdentifier() {
		return oldIdentifier;
	}

	/**
	 * @param oldIdentifier
	 *          the oldIdentifier to set
	 * @see #oldIdentifier
	 */
	public void setOldIdentifier(String oldIdentifier) {
		this.oldIdentifier = oldIdentifier;
	}

	/**
	 * @return the oldIdentifierVersion
	 * @see #oldIdentifierVersion
	 */
	public String getOldIdentifierVersion() {
		return oldIdentifierVersion;
	}

	/**
	 * @param oldIdentifierVersion
	 *          the oldIdentifierVersion to set
	 * @see #oldIdentifierVersion
	 */
	public void setOldIdentifierVersion(String oldIdentifierVersion) {
		this.oldIdentifierVersion = oldIdentifierVersion;
	}

	/**
	 * @return the released
	 * @see #released
	 */
	public boolean isReleased() {
		return released;
	}

	/**
	 * @param released
	 *          the released to set
	 * @see #released
	 */
	public void setReleased(boolean released) {
		this.released = released;
	}

}
