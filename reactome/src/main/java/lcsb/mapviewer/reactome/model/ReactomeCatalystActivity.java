package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CatalystActivity"
 * >CatalystActivity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCatalystActivity extends ReactomeDatabaseObject {
	/**
	 * GO_MolecularFunction param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeGoMolecularFunction		activity;

	/**
	 * PhysicalEntity params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>	activeUnits						= new ArrayList<>();

	/**
	 * Publicactions params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePublication>			literatureReferences	= new ArrayList<>();

	/**
	 * PhysicalEntity param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomePhysicalEntity				physicalEntity;

	/**
	 * @return the activity
	 * @see #activity
	 */
	public ReactomeGoMolecularFunction getActivity() {
		return activity;
	}

	/**
	 * @param activity
	 *          the activity to set
	 * @see #activity
	 */
	public void setActivity(ReactomeGoMolecularFunction activity) {
		this.activity = activity;
	}

	/**
	 * @return the physicalEntity
	 * @see #physicalEntity
	 */
	public ReactomePhysicalEntity getPhysicalEntity() {
		return physicalEntity;
	}

	/**
	 * @param physicalEntity
	 *          the physicalEntity to set
	 * @see #physicalEntity
	 */
	public void setPhysicalEntity(ReactomePhysicalEntity physicalEntity) {
		this.physicalEntity = physicalEntity;
	}

	/**
	 * @return the activeUnits
	 * @see #activeUnits
	 */
	public List<ReactomePhysicalEntity> getActiveUnits() {
		return activeUnits;
	}

	/**
	 * @param activeUnits
	 *          the activeUnits to set
	 * @see #activeUnits
	 */
	public void setActiveUnits(List<ReactomePhysicalEntity> activeUnits) {
		this.activeUnits = activeUnits;
	}

	/**
	 * @return the literatureReferences
	 * @see #literatureReferences
	 */
	public List<ReactomePublication> getLiteratureReferences() {
		return literatureReferences;
	}

	/**
	 * @param literatureReferences
	 *          the literatureReferences to set
	 * @see #literatureReferences
	 */
	public void setLiteratureReferences(List<ReactomePublication> literatureReferences) {
		this.literatureReferences = literatureReferences;
	}

	/**
	 * Adds new element to {@link #activeUnits}.
	 * 
	 * @param object
	 *          new {@link ReactomePhysicalEntity} to add
	 */
	public void addActiveUnit(ReactomePhysicalEntity object) {
		activeUnits.add(object);
	}

	/**
	 * Adds new element to {@link #literatureReferences}.
	 * 
	 * @param object
	 *          new {@link ReactomePublication} to add
	 */
	public void addLiteratureReference(ReactomePublication object) {
		literatureReferences.add(object);
	}

}
