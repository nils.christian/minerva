package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CandidateSet"
 * >CandidateSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCandidateSet extends ReactomeEntitySet {
	/**
	 * HasCandidates param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>	hasCandidates	= new ArrayList<ReactomePhysicalEntity>();

	/**
	 * Adds {@link ReactomePhysicalEntity} to {@link #hasCandidates}.
	 * 
	 * @param hasCandidate
	 *          object to add
	 */
	public void addHasCandidate(ReactomePhysicalEntity hasCandidate) {
		this.hasCandidates.add(hasCandidate);
	}

	/**
	 * @return the hasCandidates
	 * @see #hasCandidates
	 */
	public List<ReactomePhysicalEntity> getHasCandidates() {
		return hasCandidates;
	}

	/**
	 * @param hasCandidates
	 *          the hasCandidates to set
	 * @see #hasCandidates
	 */
	public void setHasCandidates(List<ReactomePhysicalEntity> hasCandidates) {
		this.hasCandidates = hasCandidates;
	}
}
