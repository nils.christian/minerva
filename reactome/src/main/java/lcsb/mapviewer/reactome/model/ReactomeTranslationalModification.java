package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=TranslationalModification"
 * >TranslationalModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeTranslationalModification extends ReactomeAbstractModifiedResidue {
	/**
	 * Coordinate param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer					coordinate;
	/**
	 * Psi mod param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomePsiMod	psiMod;

	/**
	 * @return the coordinate
	 * @see #coordinate
	 */
	public Integer getCoordinate() {
		return coordinate;
	}

	/**
	 * @param coordinate
	 *          the coordinate to set
	 * @see #coordinate
	 */
	public void setCoordinate(Integer coordinate) {
		this.coordinate = coordinate;
	}

	/**
	 * @return the psiMod
	 * @see #psiMod
	 */
	public ReactomePsiMod getPsiMod() {
		return psiMod;
	}

	/**
	 * @param psiMod
	 *          the psiMod to set
	 * @see #psiMod
	 */
	public void setPsiMod(ReactomePsiMod psiMod) {
		this.psiMod = psiMod;
	}

}
