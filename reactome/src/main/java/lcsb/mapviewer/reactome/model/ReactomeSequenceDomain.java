package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SequenceDomain"
 * >SequenceDomain</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSequenceDomain extends ReactomeDomain {
	/**
	 * End coordinate param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	endCoordinate;
	/**
	 * Start coordinate param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	startCoordinate;

	/**
	 * @return the endCoordinate
	 * @see #endCoordinate
	 */
	public Integer getEndCoordinate() {
		return endCoordinate;
	}

	/**
	 * @param endCoordinate
	 *          the endCoordinate to set
	 * @see #endCoordinate
	 */
	public void setEndCoordinate(Integer endCoordinate) {
		this.endCoordinate = endCoordinate;
	}

	/**
	 * @return the startCoordinate
	 * @see #startCoordinate
	 */
	public Integer getStartCoordinate() {
		return startCoordinate;
	}

	/**
	 * @param startCoordinate
	 *          the startCoordinate to set
	 * @see #startCoordinate
	 */
	public void setStartCoordinate(Integer startCoordinate) {
		this.startCoordinate = startCoordinate;
	}

}
