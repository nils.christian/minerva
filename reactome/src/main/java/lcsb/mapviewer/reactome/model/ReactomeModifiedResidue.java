package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ModifiedResidue"
 * >ModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeModifiedResidue extends ReactomeTranslationalModification {
}
