package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentInsertionModification"
 * >FragmentInsertionModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFragmentInsertionModification extends ReactomeFragmentModification {
	/**
	 * Coordinate param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	coordinate;

	/**
	 * @return the coordinate
	 * @see #coordinate
	 */
	public Integer getCoordinate() {
		return coordinate;
	}

	/**
	 * @param coordinate the coordinate to set
	 * @see #coordinate
	 */
	public void setCoordinate(Integer coordinate) {
		this.coordinate = coordinate;
	}

}
