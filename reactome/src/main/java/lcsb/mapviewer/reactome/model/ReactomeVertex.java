package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Vertex"
 * >Vertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeVertex extends ReactomePathwayDiagramItem {
	/**
	 * X coordinate in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	x;
	/**
	 * Y coordinate in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	y;
	/**
	 * Width in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	width;
	/**
	 * Height in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	height;

	/**
	 * @return the x
	 * @see #x
	 */
	public Integer getX() {
		return x;
	}

	/**
	 * @param x
	 *          the x to set
	 * @see #x
	 */
	public void setX(Integer x) {
		this.x = x;
	}

	/**
	 * @return the y
	 * @see #y
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * @param y
	 *          the y to set
	 * @see #y
	 */
	public void setY(Integer y) {
		this.y = y;
	}

	/**
	 * @return the width
	 * @see #width
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @param width
	 *          the width to set
	 * @see #width
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * @return the height
	 * @see #height
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * @param height
	 *          the height to set
	 * @see #height
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

}
