package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Event"
 * >Event</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEvent extends ReactomeDatabaseObject {

	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>			 authoreds						= new ArrayList<ReactomeInstanceEdit>();

	/**
	 * List of EntityCompartment params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEntityCompartment>	compartments				 = new ArrayList<ReactomeEntityCompartment>();

	/**
	 * List of DatabaseIdentifier params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDatabaseIdentifier> crossReferences			= new ArrayList<ReactomeDatabaseIdentifier>();

	/**
	 * Definition param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String													 definition;

	/**
	 * List of Disease params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDisease>						diseases						 = new ArrayList<ReactomeDisease>();

	/**
	 * Boolean do release param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean													doRelease;

	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>			 edited							 = new ArrayList<ReactomeInstanceEdit>();

	/**
	 * EvidenceType param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeEvidenceType						 evidenceType;

	/**
	 * List of Figure params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeFigure>						 figures							= new ArrayList<ReactomeFigure>();

	/**
	 * GO_BiologicalProcess param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeGoBiologicalProcess			goBiologicalProcess;

	/**
	 * List of Event params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEvent>							inferredFroms				= new ArrayList<ReactomeEvent>();

	/**
	 * Boolean is in disease param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean													isInDisease;

	/**
	 * Boolean is inferred param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean													isInferred;

	/**
	 * List of Publication params in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePublication>				literatureReferences = new ArrayList<ReactomePublication>();

	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>										 names								= new ArrayList<String>();

	/**
	 * List of Event params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEvent>							orthologousEvents		= new ArrayList<ReactomeEvent>();

	/**
	 * List of Event params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEvent>							precedingEvents			= new ArrayList<ReactomeEvent>();

	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeSpecies>						relatedSpecies			 = new ArrayList<>();

	/**
	 * Release date param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Calendar												 releaseDate;

	/**
	 * Release status param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String													 releaseStatus;

	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>			 revised							= new ArrayList<ReactomeInstanceEdit>();

	/**
	 * List of InstanceEdit params in the reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInstanceEdit>			 reviewed						 = new ArrayList<ReactomeInstanceEdit>();

	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeSpecies									species;

	/**
	 * Species name param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String													 speciesName;

	/**
	 * List of Summation params in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeSummation>					summations					 = new ArrayList<ReactomeSummation>();

	/**
	 * Adds compartment to {@link #compartments}.
	 * 
	 * @param compartment
	 *          object to add
	 */
	public void addCompartment(ReactomeEntityCompartment compartment) {
		compartments.add(compartment);
	}

	/**
	 * Adds name to {@link #names}.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		names.add(name);
	}

	/**
	 * Adds event to {@link #inferredFroms} list.
	 * 
	 * @param inferredFrom
	 *          object to add
	 */
	public void addInferredFrom(ReactomeEvent inferredFrom) {
		inferredFroms.add(inferredFrom);
	}

	/**
	 * Adds event to {@link #orthologousEvents} list.
	 * 
	 * @param orthologousEvent
	 *          object to add
	 */
	public void addOrthologousEvent(ReactomeEvent orthologousEvent) {
		orthologousEvents.add(orthologousEvent);
	}

	/**
	 * Adds event to {@link #precedingEvents} list.
	 * 
	 * @param precedingEvent
	 *          object to add
	 */
	public void addPrecedingEvent(ReactomeEvent precedingEvent) {
		precedingEvents.add(precedingEvent);
	}

	/**
	 * Adds summation to {{@link #summations} list.
	 * 
	 * @param summation
	 *          object to add
	 */
	public void addSummation(ReactomeSummation summation) {
		summations.add(summation);
	}

	/**
	 * Adds literature reference to {@link #literatureReferences} list.
	 * 
	 * @param literatureReference
	 *          object to add
	 */
	public void addLiteratureReferences(ReactomePublication literatureReference) {
		this.literatureReferences.add(literatureReference);
	}

	/**
	 * Adds element into {@link #revised} list.
	 * 
	 * @param revised
	 *          object to add
	 */
	public void addRevised(ReactomeInstanceEdit revised) {
		this.revised.add(revised);
	}

	/**
	 * Adds reference to {@link #crossReferences} list.
	 * 
	 * @param crossReference
	 *          object to add
	 */
	public void addCrossReferences(ReactomeDatabaseIdentifier crossReference) {
		this.crossReferences.add(crossReference);
	}

	/**
	 * Adds element to {@link #authoreds} list.
	 * 
	 * @param authored
	 *          object to add
	 */
	public void addAuthored(ReactomeInstanceEdit authored) {
		this.authoreds.add(authored);
	}

	/**
	 * Adds figure to {@link #figures} list.
	 * 
	 * @param figure
	 *          object to add
	 */
	public void addFigure(ReactomeFigure figure) {
		this.figures.add(figure);
	}

	/**
	 * Adds element to {@link #edited} list.
	 * 
	 * @param edited
	 *          object to add
	 */
	public void addEdited(ReactomeInstanceEdit edited) {
		this.edited.add(edited);
	}

	/**
	 * Adds object to {@link #reviewed} list.
	 * 
	 * @param reviewed
	 *          object to add
	 */
	public void addReviewed(ReactomeInstanceEdit reviewed) {
		this.reviewed.add(reviewed);
	}

	/**
	 * Adds disease to {@link #diseases} list.
	 * 
	 * @param disease
	 *          object to add
	 */
	public void addDisease(ReactomeDisease disease) {
		this.diseases.add(disease);
	}

	/**
	 * @return the compartments
	 * @see #compartments
	 */
	public List<ReactomeEntityCompartment> getCompartments() {
		return compartments;
	}

	/**
	 * @param compartments
	 *          the compartments to set
	 * @see #compartments
	 */
	public void setCompartments(List<ReactomeEntityCompartment> compartments) {
		this.compartments = compartments;
	}

	/**
	 * @return the evidenceType
	 * @see #evidenceType
	 */
	public ReactomeEvidenceType getEvidenceType() {
		return evidenceType;
	}

	/**
	 * @param evidenceType
	 *          the evidenceType to set
	 * @see #evidenceType
	 */
	public void setEvidenceType(ReactomeEvidenceType evidenceType) {
		this.evidenceType = evidenceType;
	}

	/**
	 * @return the inferredFroms
	 * @see #inferredFroms
	 */
	public List<ReactomeEvent> getInferredFroms() {
		return inferredFroms;
	}

	/**
	 * @param inferredFroms
	 *          the inferredFroms to set
	 * @see #inferredFroms
	 */
	public void setInferredFroms(List<ReactomeEvent> inferredFroms) {
		this.inferredFroms = inferredFroms;
	}

	/**
	 * @return the orthologousEvents
	 * @see #orthologousEvents
	 */
	public List<ReactomeEvent> getOrthologousEvents() {
		return orthologousEvents;
	}

	/**
	 * @param orthologousEvents
	 *          the orthologousEvents to set
	 * @see #orthologousEvents
	 */
	public void setOrthologousEvents(List<ReactomeEvent> orthologousEvents) {
		this.orthologousEvents = orthologousEvents;
	}

	/**
	 * @return the precedingEvents
	 * @see #precedingEvents
	 */
	public List<ReactomeEvent> getPrecedingEvents() {
		return precedingEvents;
	}

	/**
	 * @param precedingEvents
	 *          the precedingEvents to set
	 * @see #precedingEvents
	 */
	public void setPrecedingEvents(List<ReactomeEvent> precedingEvents) {
		this.precedingEvents = precedingEvents;
	}

	/**
	 * @return the revised
	 * @see #revised
	 */
	public List<ReactomeInstanceEdit> getRevised() {
		return revised;
	}

	/**
	 * @param revised
	 *          the revised to set
	 * @see #revised
	 */
	public void setRevised(List<ReactomeInstanceEdit> revised) {
		this.revised = revised;
	}

	/**
	 * @return the figures
	 * @see #figures
	 */
	public List<ReactomeFigure> getFigures() {
		return figures;
	}

	/**
	 * @param figures
	 *          the figures to set
	 * @see #figures
	 */
	public void setFigures(List<ReactomeFigure> figures) {
		this.figures = figures;
	}

	/**
	 * @return the authoreds
	 * @see #authoreds
	 */
	public List<ReactomeInstanceEdit> getAuthoreds() {
		return authoreds;
	}

	/**
	 * @param authoreds
	 *          the authoreds to set
	 * @see #authoreds
	 */
	public void setAuthoreds(List<ReactomeInstanceEdit> authoreds) {
		this.authoreds = authoreds;
	}

	/**
	 * @return the edited
	 * @see #edited
	 */
	public List<ReactomeInstanceEdit> getEdited() {
		return edited;
	}

	/**
	 * @param edited
	 *          the edited to set
	 * @see #edited
	 */
	public void setEdited(List<ReactomeInstanceEdit> edited) {
		this.edited = edited;
	}

	/**
	 * @return the reviewed
	 * @see #reviewed
	 */
	public List<ReactomeInstanceEdit> getReviewed() {
		return reviewed;
	}

	/**
	 * @param reviewed
	 *          the reviewed to set
	 * @see #reviewed
	 */
	public void setReviewed(List<ReactomeInstanceEdit> reviewed) {
		this.reviewed = reviewed;
	}

	/**
	 * @return the isInDisease
	 * @see #isInDisease
	 */
	public Boolean getIsInDisease() {
		return isInDisease;
	}

	/**
	 * @param isInDisease
	 *          the isInDisease to set
	 * @see #isInDisease
	 */
	public void setIsInDisease(Boolean isInDisease) {
		this.isInDisease = isInDisease;
	}

	/**
	 * @return the releaseDate
	 * @see #releaseDate
	 */
	public Calendar getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @param releaseDate
	 *          the releaseDate to set
	 * @see #releaseDate
	 */
	public void setReleaseDate(Calendar releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the isInferred
	 * @see #isInferred
	 */
	public Boolean getIsInferred() {
		return isInferred;
	}

	/**
	 * @param isInferred
	 *          the isInferred to set
	 * @see #isInferred
	 */
	public void setIsInferred(Boolean isInferred) {
		this.isInferred = isInferred;
	}

	/**
	 * @return the doRelease
	 * @see #doRelease
	 */
	public Boolean getDoRelease() {
		return doRelease;
	}

	/**
	 * @param doRelease
	 *          the doRelease to set
	 * @see #doRelease
	 */
	public void setDoRelease(Boolean doRelease) {
		this.doRelease = doRelease;
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public ReactomeSpecies getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(ReactomeSpecies species) {
		this.species = species;
	}

	/**
	 * @return the summations
	 * @see #summations
	 */
	public List<ReactomeSummation> getSummations() {
		return summations;
	}

	/**
	 * @param summations
	 *          the summations to set
	 * @see #summations
	 */
	public void setSummations(List<ReactomeSummation> summations) {
		this.summations = summations;
	}

	/**
	 * @return the diseases
	 * @see #diseases
	 */
	public List<ReactomeDisease> getDiseases() {
		return diseases;
	}

	/**
	 * @param diseases
	 *          the diseases to set
	 * @see #diseases
	 */
	public void setDiseases(List<ReactomeDisease> diseases) {
		this.diseases = diseases;
	}

	/**
	 * @return the literatureReferences
	 * @see #literatureReferences
	 */
	public List<ReactomePublication> getLiteratureReferences() {
		return literatureReferences;
	}

	/**
	 * @param literatureReferences
	 *          the literatureReferences to set
	 * @see #literatureReferences
	 */
	public void setLiteratureReferences(List<ReactomePublication> literatureReferences) {
		this.literatureReferences = literatureReferences;
	}

	/**
	 * @return the crossReferences
	 * @see #crossReferences
	 */
	public List<ReactomeDatabaseIdentifier> getCrossReferences() {
		return crossReferences;
	}

	/**
	 * @param crossReferences
	 *          the crossReferences to set
	 * @see #crossReferences
	 */
	public void setCrossReferences(List<ReactomeDatabaseIdentifier> crossReferences) {
		this.crossReferences = crossReferences;
	}

	/**
	 * @return the goBiologicalProcess
	 * @see #goBiologicalProcess
	 */
	public ReactomeGoBiologicalProcess getGoBiologicalProcess() {
		return goBiologicalProcess;
	}

	/**
	 * @param goBiologicalProcess
	 *          the goBiologicalProcess to set
	 * @see #goBiologicalProcess
	 */
	public void setGoBiologicalProcess(ReactomeGoBiologicalProcess goBiologicalProcess) {
		this.goBiologicalProcess = goBiologicalProcess;
	}

	/**
	 * @return the releaseStatus
	 * @see #releaseStatus
	 */
	public String getReleaseStatus() {
		return releaseStatus;
	}

	/**
	 * @param releaseStatus
	 *          the releaseStatus to set
	 * @see #releaseStatus
	 */
	public void setReleaseStatus(String releaseStatus) {
		this.releaseStatus = releaseStatus;
	}

	/**
	 * @return the definition
	 * @see #definition
	 */
	public String getDefinition() {
		return definition;
	}

	/**
	 * @param definition
	 *          the definition to set
	 * @see #definition
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	/**
	 * @return the relatedSpecies
	 * @see #relatedSpecies
	 */
	public List<ReactomeSpecies> getRelatedSpecies() {
		return relatedSpecies;
	}

	/**
	 * @param relatedSpecies
	 *          the relatedSpecies to set
	 * @see #relatedSpecies
	 */
	public void setRelatedSpecies(List<ReactomeSpecies> relatedSpecies) {
		this.relatedSpecies = relatedSpecies;
	}

	/**
	 * Adds new object to {@link #relatedSpecies}.
	 * 
	 * @param object
	 *          new {@link ReactomeSpecies} to add
	 */
	public void addRelatedSpecies(ReactomeSpecies object) {
		relatedSpecies.add(object);
	}

	/**
	 * @return the speciesName
	 * @see #speciesName
	 */
	public String getSpeciesName() {
		return speciesName;
	}

	/**
	 * @param speciesName
	 *          the speciesName to set
	 * @see #speciesName
	 */
	public void setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
	}

}
