package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Figure"
 * >Figure</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFigure extends ReactomeDatabaseObject {
	/**
	 * Url in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	url;

	/**
	 * @return the url
	 * @see #url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *          the url to set
	 * @see #url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
