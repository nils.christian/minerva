package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DatabaseIdentifier"
 * >DatabaseIdentifier</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDatabaseIdentifier extends ReactomeDatabaseObject {
	/**
	 * String identifier in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String										identifier;
	/**
	 * ReferenceDatabase param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceDatabase	referenceDatabase;
	/**
	 * Url param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String										url;

	/**
	 * @return the identifier
	 * @see #identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier
	 *          the identifier to set
	 * @see #identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the referenceDatabase
	 * @see #referenceDatabase
	 */
	public ReactomeReferenceDatabase getReferenceDatabase() {
		return referenceDatabase;
	}

	/**
	 * @param referenceDatabase
	 *          the referenceDatabase to set
	 * @see #referenceDatabase
	 */
	public void setReferenceDatabase(ReactomeReferenceDatabase referenceDatabase) {
		this.referenceDatabase = referenceDatabase;
	}

	/**
	 * @return the url
	 * @see #url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *          the url to set
	 * @see #url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
