package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceIsoform"
 * >ReferenceIsoform</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceIsoform extends ReactomeReferenceGeneProduct {
	/**
	 * List of ReferenceGeneProduct params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeReferenceGeneProduct>	isoformParents	= new ArrayList<ReactomeReferenceGeneProduct>();

	/**
	 * Viariant identifier param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String															variantIdentifier;

	/**
	 * Adds element to {@link #isoformParents} list.
	 * 
	 * @param isoformParent
	 *          object to add
	 */
	public void addIsoformParent(ReactomeReferenceGeneProduct isoformParent) {
		this.isoformParents.add(isoformParent);
	}

	/**
	 * @return the variantIdentifier
	 * @see #variantIdentifier
	 */
	public String getVariantIdentifier() {
		return variantIdentifier;
	}

	/**
	 * @param variantIdentifier
	 *          the variantIdentifier to set
	 * @see #variantIdentifier
	 */
	public void setVariantIdentifier(String variantIdentifier) {
		this.variantIdentifier = variantIdentifier;
	}

	/**
	 * @return the isoformParents
	 * @see #isoformParents
	 */
	public List<ReactomeReferenceGeneProduct> getIsoformParents() {
		return isoformParents;
	}

	/**
	 * @param isoformParents
	 *          the isoformParents to set
	 * @see #isoformParents
	 */
	public void setIsoformParents(List<ReactomeReferenceGeneProduct> isoformParents) {
		this.isoformParents = isoformParents;
	}
}
