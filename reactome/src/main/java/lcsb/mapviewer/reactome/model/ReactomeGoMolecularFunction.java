package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_MolecularFunction"
 * >GoMolecularFunction</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGoMolecularFunction extends ReactomeDatabaseObject {
	/**
	 * Accession param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String														accession;
	/**
	 * Definition param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String														definition;
	/**
	 * List of ec numbers in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>											ecNumbers	= new ArrayList<String>();
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>											names			= new ArrayList<String>();
	/**
	 * List of GO_BiologicalProcess params in reactome model. More information can
	 * be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeGoBiologicalProcess>	regulates	= new ArrayList<ReactomeGoBiologicalProcess>();
	/**
	 * ReferenceDatabase param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceDatabase					referenceDatabase;

	/**
	 * Adds ec numner to {@link #ecNumbers} list.
	 * 
	 * @param ecNumber
	 *          object to add
	 */
	public void addEcNumber(String ecNumber) {
		this.ecNumbers.add(ecNumber);
	}

	/**
	 * Adds name to {@link #names} list.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		this.names.add(name);
	}

	/**
	 * Adds object to {@link #regulates} list.
	 * 
	 * @param regulate
	 *          object to add
	 */
	public void addRegulate(ReactomeGoBiologicalProcess regulate) {
		this.regulates.add(regulate);
	}

	/**
	 * @return the accession
	 * @see #accession
	 */
	public String getAccession() {
		return accession;
	}

	/**
	 * @param accession
	 *          the accession to set
	 * @see #accession
	 */
	public void setAccession(String accession) {
		this.accession = accession;
	}

	/**
	 * @return the definition
	 * @see #definition
	 */
	public String getDefinition() {
		return definition;
	}

	/**
	 * @param definition
	 *          the definition to set
	 * @see #definition
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	/**
	 * @return the ecNumbers
	 * @see #ecNumbers
	 */
	public List<String> getEcNumbers() {
		return ecNumbers;
	}

	/**
	 * @param ecNumbers
	 *          the ecNumbers to set
	 * @see #ecNumbers
	 */
	public void setEcNumbers(List<String> ecNumbers) {
		this.ecNumbers = ecNumbers;
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}

	/**
	 * @return the regulates
	 * @see #regulates
	 */
	public List<ReactomeGoBiologicalProcess> getRegulates() {
		return regulates;
	}

	/**
	 * @param regulates
	 *          the regulates to set
	 * @see #regulates
	 */
	public void setRegulates(List<ReactomeGoBiologicalProcess> regulates) {
		this.regulates = regulates;
	}

	/**
	 * @return the referenceDatabase
	 * @see #referenceDatabase
	 */
	public ReactomeReferenceDatabase getReferenceDatabase() {
		return referenceDatabase;
	}

	/**
	 * @param referenceDatabase
	 *          the referenceDatabase to set
	 * @see #referenceDatabase
	 */
	public void setReferenceDatabase(ReactomeReferenceDatabase referenceDatabase) {
		this.referenceDatabase = referenceDatabase;
	}
}
