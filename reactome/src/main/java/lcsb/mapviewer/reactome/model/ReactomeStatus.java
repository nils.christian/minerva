package lcsb.mapviewer.reactome.model;

/**
 * Defines status of the object retrieved from reactome database.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ReactomeStatus {
	/**
	 * Full object was retrieved from reactome db.
	 * 
	 */
	FULL,
	/**
	 * Object was created and basic information from reactome db has been
	 * retrieved.
	 */
	ONLY_ID,
	/**
	 * object was created but it wasn't connected to any object in reactome db.
	 */
	UNINITIALIZED
}
