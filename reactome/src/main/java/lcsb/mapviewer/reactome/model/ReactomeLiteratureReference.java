package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=LiteratureReference"
 * >LiteratureReference</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeLiteratureReference extends ReactomePublication {
	/**
	 * Journal param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	journal;
	/**
	 * Pages param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	pages;
	/**
	 * Volume param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	volume;
	/**
	 * Pubmed identifier param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	pubMedIdentifier;
	/**
	 * Year param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	year;

	/**
	 * @return the journal
	 * @see #journal
	 */
	public String getJournal() {
		return journal;
	}

	/**
	 * @param journal
	 *          the journal to set
	 * @see #journal
	 */
	public void setJournal(String journal) {
		this.journal = journal;
	}

	/**
	 * @return the pages
	 * @see #pages
	 */
	public String getPages() {
		return pages;
	}

	/**
	 * @param pages
	 *          the pages to set
	 * @see #pages
	 */
	public void setPages(String pages) {
		this.pages = pages;
	}

	/**
	 * @return the volume
	 * @see #volume
	 */
	public Integer getVolume() {
		return volume;
	}

	/**
	 * @param volume
	 *          the volume to set
	 * @see #volume
	 */
	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	/**
	 * @return the pubMedIdentifier
	 * @see #pubMedIdentifier
	 */
	public Integer getPubMedIdentifier() {
		return pubMedIdentifier;
	}

	/**
	 * @param pubMedIdentifier
	 *          the pubMedIdentifier to set
	 * @see #pubMedIdentifier
	 */
	public void setPubMedIdentifier(Integer pubMedIdentifier) {
		this.pubMedIdentifier = pubMedIdentifier;
	}

	/**
	 * @return the year
	 * @see #year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year
	 *          the year to set
	 * @see #year
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

}
