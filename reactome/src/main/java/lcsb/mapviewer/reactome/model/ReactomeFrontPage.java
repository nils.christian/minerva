package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FrontPage"
 * >FrontPage</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFrontPage extends ReactomeDatabaseObject {
	/**
	 * List of Event params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEvent>	frontPageItems	= new ArrayList<ReactomeEvent>();

	/**
	 * Adds element to {@link #frontPageItems}.
	 * 
	 * @param frontPageItem
	 *          object to add
	 */
	public void addFrontPageItem(ReactomeEvent frontPageItem) {
		this.frontPageItems.add(frontPageItem);
	}

	/**
	 * @return the frontPageItems
	 * @see #frontPageItems
	 */
	public List<ReactomeEvent> getFrontPageItems() {
		return frontPageItems;
	}

	/**
	 * @param frontPageItems
	 *          the frontPageItems to set
	 * @see #frontPageItems
	 */
	public void setFrontPageItems(List<ReactomeEvent> frontPageItems) {
		this.frontPageItems = frontPageItems;
	}
}
