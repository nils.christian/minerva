package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Summation"
 * >Summation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSummation extends ReactomeDatabaseObject {
	/**
	 * Text of summation in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String										text;
	/**
	 * List of Publication params in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePublication>	literatureReferences	= new ArrayList<ReactomePublication>();

	/**
	 * Adds reference to {@link #literatureReferences} list.
	 * @param literatureReference
	 *          object to add
	 */
	public void addLiteratureReferences(ReactomePublication literatureReference) {
		this.literatureReferences.add(literatureReference);
	}

	/**
	 * @return the text
	 * @see #text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *          the text to set
	 * @see #text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the literatureReferences
	 * @see #literatureReferences
	 */
	public List<ReactomePublication> getLiteratureReferences() {
		return literatureReferences;
	}

	/**
	 * @param literatureReferences
	 *          the literatureReferences to set
	 * @see #literatureReferences
	 */
	public void setLiteratureReferences(List<ReactomePublication> literatureReferences) {
		this.literatureReferences = literatureReferences;
	}
}
