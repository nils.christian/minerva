package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Person"
 * >Person</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePerson extends ReactomeDatabaseObject {
	/**
	 * Affilation.
	 */
	private ReactomeAffiliation					affilation;
	/**
	 * ???
	 */
	private ReactomeDatabaseIdentifier	crosssReference;
	/**
	 * Email address.
	 */
	private String											eMailAddress;
	/**
	 * Figure.
	 */
	private ReactomeFigure							figure;
	/**
	 * Project.
	 */
	private String											project;
	/**
	 * Url.
	 */
	private String											url;

	/**
	 * First name.
	 */
	private String											firstname;
	/**
	 * Family name.
	 */
	private String											surname;
	/**
	 * Initials.
	 */
	private String											initial;

	/**
	 * @return the firstname
	 * @see #firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname
	 *          the firstname to set
	 * @see #firstname
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the surname
	 * @see #surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *          the surname to set
	 * @see #surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the initial
	 * @see #initial
	 */
	public String getInitial() {
		return initial;
	}

	/**
	 * @param initial
	 *          the initial to set
	 * @see #initial
	 */
	public void setInitial(String initial) {
		this.initial = initial;
	}

	/**
	 * @return the affilation
	 * @see #affilation
	 */
	public ReactomeAffiliation getAffilation() {
		return affilation;
	}

	/**
	 * @param affilation
	 *          the affilation to set
	 * @see #affilation
	 */
	public void setAffilation(ReactomeAffiliation affilation) {
		this.affilation = affilation;
	}

	/**
	 * @return the crosssReference
	 * @see #crosssReference
	 */
	public ReactomeDatabaseIdentifier getCrosssReference() {
		return crosssReference;
	}

	/**
	 * @param crosssReference
	 *          the crosssReference to set
	 * @see #crosssReference
	 */
	public void setCrosssReference(ReactomeDatabaseIdentifier crosssReference) {
		this.crosssReference = crosssReference;
	}

	/**
	 * @return the eMailAddress
	 * @see #eMailAddress
	 */
	public String geteMailAddress() {
		return eMailAddress;
	}

	/**
	 * @param eMailAddress
	 *          the eMailAddress to set
	 * @see #eMailAddress
	 */
	public void seteMailAddress(String eMailAddress) {
		this.eMailAddress = eMailAddress;
	}

	/**
	 * @return the figure
	 * @see #figure
	 */
	public ReactomeFigure getFigure() {
		return figure;
	}

	/**
	 * @param figure
	 *          the figure to set
	 * @see #figure
	 */
	public void setFigure(ReactomeFigure figure) {
		this.figure = figure;
	}

	/**
	 * @return the project
	 * @see #project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project
	 *          the project to set
	 * @see #project
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the url
	 * @see #url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *          the url to set
	 * @see #url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
