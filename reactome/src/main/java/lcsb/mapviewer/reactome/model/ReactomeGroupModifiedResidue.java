package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GroupModifiedResidue"
 * >GroupModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGroupModifiedResidue extends ReactomeTranslationalModification {
	/**
	 * DatabaseObject param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeDatabaseObject	modification;

	/**
	 * @return the modification
	 * @see #modification
	 */
	public ReactomeDatabaseObject getModification() {
		return modification;
	}

	/**
	 * @param modification the modification to set
	 * @see #modification
	 */
	public void setModification(ReactomeDatabaseObject modification) {
		this.modification = modification;
	}

}
