package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CrosslinkedResidue"
 * >CrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCrosslinkedResidue extends ReactomeTranslationalModification {
	/**
	 * List of DatabaseObject params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeDatabaseObject>	modifications			= new ArrayList<ReactomeDatabaseObject>();
	/**
	 * List of Coordinates params in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<Integer>									secondCoordinates	= new ArrayList<Integer>();

	/**
	 * Adds modification to {@link #modifications}.
	 * 
	 * @param modification
	 *          modification to add
	 */
	public void addModification(ReactomeDatabaseObject modification) {
		this.modifications.add(modification);
	}

	/**
	 * Adds coordinate to {@link #secondCoordinates}.
	 * 
	 * @param secondCoordinate
	 *          coordinate to add
	 */
	public void addSecondCoordinate(Integer secondCoordinate) {
		this.secondCoordinates.add(secondCoordinate);
	}

	/**
	 * @return the secondCoordinates
	 * @see #secondCoordinates
	 */
	public List<Integer> getSecondCoordinates() {
		return secondCoordinates;
	}

	/**
	 * @param secondCoordinates
	 *          the secondCoordinates to set
	 * @see #secondCoordinates
	 */
	public void setSecondCoordinates(List<Integer> secondCoordinates) {
		this.secondCoordinates = secondCoordinates;
	}

	/**
	 * @return the modifications
	 * @see #modifications
	 */
	public List<ReactomeDatabaseObject> getModifications() {
		return modifications;
	}

	/**
	 * @param modifications
	 *          the modifications to set
	 * @see #modifications
	 */
	public void setModifications(List<ReactomeDatabaseObject> modifications) {
		this.modifications = modifications;
	}
}
