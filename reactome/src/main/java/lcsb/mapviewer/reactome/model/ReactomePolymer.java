package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Polymer"
 * >Polymer</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePolymer extends ReactomePhysicalEntity {
	/**
	 * Minimum unit count param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer												minUnitCount;
	/**
	 * Maximum unit count param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer												maxUnitCount;

	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeSpecies								species;
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>	repeatedUnits	= new ArrayList<ReactomePhysicalEntity>();

	/**
	 * Adds element to {@link #repeatedUnits} list.
	 * 
	 * @param repeatedUnit
	 *          object to add
	 */
	public void addRepeatedUnit(ReactomePhysicalEntity repeatedUnit) {
		this.repeatedUnits.add(repeatedUnit);
	}

	/**
	 * @return the minUnitCount
	 * @see #minUnitCount
	 */
	public Integer getMinUnitCount() {
		return minUnitCount;
	}

	/**
	 * @param minUnitCount
	 *          the minUnitCount to set
	 * @see #minUnitCount
	 */
	public void setMinUnitCount(Integer minUnitCount) {
		this.minUnitCount = minUnitCount;
	}

	/**
	 * @return the maxUnitCount
	 * @see #maxUnitCount
	 */
	public Integer getMaxUnitCount() {
		return maxUnitCount;
	}

	/**
	 * @param maxUnitCount
	 *          the maxUnitCount to set
	 * @see #maxUnitCount
	 */
	public void setMaxUnitCount(Integer maxUnitCount) {
		this.maxUnitCount = maxUnitCount;
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public ReactomeSpecies getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(ReactomeSpecies species) {
		this.species = species;
	}

	/**
	 * @return the repeatedUnits
	 * @see #repeatedUnits
	 */
	public List<ReactomePhysicalEntity> getRepeatedUnits() {
		return repeatedUnits;
	}

	/**
	 * @param repeatedUnits
	 *          the repeatedUnits to set
	 * @see #repeatedUnits
	 */
	public void setRepeatedUnits(List<ReactomePhysicalEntity> repeatedUnits) {
		this.repeatedUnits = repeatedUnits;
	}
}