package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionVertex"
 * >ReactionVertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionVertex extends ReactomeVertex {

	/**
	 * Point coordinates in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	pointCoordinates;

	/**
	 * @return the pointCoordinates
	 * @see #pointCoordinates
	 */
	public String getPointCoordinates() {
		return pointCoordinates;
	}

	/**
	 * @param pointCoordinates
	 *          the pointCoordinates to set
	 * @see #pointCoordinates
	 */
	public void setPointCoordinates(String pointCoordinates) {
		this.pointCoordinates = pointCoordinates;
	}
}
