package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Pathway"
 * >Pathway</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathway extends ReactomeEvent {
	/**
	 * Has diagrams param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean							hasDiagram;
	/**
	 * List of Event params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEvent>	hasEvents	= new ArrayList<ReactomeEvent>();
	/**
	 * Doi param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String							doi;

	/**
	 * IsCanonical param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean							isCanonical;

	/**
	 * Normal pathwat param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomePathway			normalPathway;

	/**
	 * Adds event to {@link #hasEvents}.
	 * 
	 * @param hasEvent
	 *          object to add
	 */
	public void addHasEvent(ReactomeEvent hasEvent) {
		this.hasEvents.add(hasEvent);
	}

	/**
	 * @return the hasDiagram
	 * @see #hasDiagram
	 */
	public Boolean getHasDiagram() {
		return hasDiagram;
	}

	/**
	 * @param hasDiagram
	 *          the hasDiagram to set
	 * @see #hasDiagram
	 */
	public void setHasDiagram(Boolean hasDiagram) {
		this.hasDiagram = hasDiagram;
	}

	/**
	 * @return the hasEvents
	 * @see #hasEvents
	 */
	public List<ReactomeEvent> getHasEvents() {
		return hasEvents;
	}

	/**
	 * @param hasEvents
	 *          the hasEvents to set
	 * @see #hasEvents
	 */
	public void setHasEvents(List<ReactomeEvent> hasEvents) {
		this.hasEvents = hasEvents;
	}

	/**
	 * @return the doi
	 * @see #doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi
	 *          the doi to set
	 * @see #doi
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the isCanonical
	 * @see #isCanonical
	 */
	public Boolean getIsCanonical() {
		return isCanonical;
	}

	/**
	 * @param isCanonical the isCanonical to set
	 * @see #isCanonical
	 */
	public void setIsCanonical(Boolean isCanonical) {
		this.isCanonical = isCanonical;
	}

	/**
	 * @return the normalPathway
	 * @see #normalPathway
	 */
	public ReactomePathway getNormalPathway() {
		return normalPathway;
	}

	/**
	 * @param normalPathway the normalPathway to set
	 * @see #normalPathway
	 */
	public void setNormalPathway(ReactomePathway normalPathway) {
		this.normalPathway = normalPathway;
	}
}
