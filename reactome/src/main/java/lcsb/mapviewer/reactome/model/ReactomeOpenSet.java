package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=OpenSet"
 * >OpenSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeOpenSet extends ReactomeEntitySet {
	/**
	 * ReferenceEntity param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceEntity	referenceEntity;

	/**
	 * @return the referenceEntity
	 * @see #referenceEntity
	 */
	public ReactomeReferenceEntity getReferenceEntity() {
		return referenceEntity;
	}

	/**
	 * @param referenceEntity the referenceEntity to set
	 * @see #referenceEntity
	 */
	public void setReferenceEntity(ReactomeReferenceEntity referenceEntity) {
		this.referenceEntity = referenceEntity;
	}
}
