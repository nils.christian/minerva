package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceMolecule"
 * >ReferenceMolecule</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceMolecule extends ReactomeReferenceEntity {
	/**
	 * Formula param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	formula;
	/**
	 * Atomic connectivity param in reactome model. More information can be found
	 * <a href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String	atomicConnectivity;

	/**
	 * @return the atomicConnectivity
	 * @see #atomicConnectivity
	 */
	public String getAtomicConnectivity() {
		return atomicConnectivity;
	}

	/**
	 * @param atomicConnectivity
	 *          the atomicConnectivity to set
	 * @see #atomicConnectivity
	 */
	public void setAtomicConnectivity(String atomicConnectivity) {
		this.atomicConnectivity = atomicConnectivity;
	}

	/**
	 * @return the formula
	 * @see #formula
	 */
	public String getFormula() {
		return formula;
	}

	/**
	 * @param formula
	 *          the formula to set
	 * @see #formula
	 */
	public void setFormula(String formula) {
		this.formula = formula;
	}

}
