package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityWithAccessionedSequence"
 * >EntityWithAccessionedSequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntityWithAccessionedSequence extends ReactomeGenomeEncodedEntity {
	/**
	 * End coordinate param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer																endCoordinate;
	/**
	 * Start coordinate param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer																startCoordinate;
	/**
	 * ReferenceEntity param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceEntity								referenceEntity;
	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeSpecies												species;
	/**
	 * List of AbstractModifiedResidue params in reactome model. More information
	 * can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeAbstractModifiedResidue>	hasModifiedResidues	= new ArrayList<ReactomeAbstractModifiedResidue>();

	/**
	 * Adds modified residue to {@link #hasModifiedResidues}.
	 * 
	 * @param hasModifiedResidue
	 *          object to add
	 */
	public void addHasModifiedResidue(ReactomeAbstractModifiedResidue hasModifiedResidue) {
		this.hasModifiedResidues.add(hasModifiedResidue);
	}

	/**
	 * @return the hasModifiedResidues
	 * @see #hasModifiedResidues
	 */
	public List<ReactomeAbstractModifiedResidue> getHasModifiedResidues() {
		return hasModifiedResidues;
	}

	/**
	 * @param hasModifiedResidues
	 *          the hasModifiedResidues to set
	 * @see #hasModifiedResidues
	 */
	public void setHasModifiedResidues(List<ReactomeAbstractModifiedResidue> hasModifiedResidues) {
		this.hasModifiedResidues = hasModifiedResidues;
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public ReactomeSpecies getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(ReactomeSpecies species) {
		this.species = species;
	}

	/**
	 * @return the referenceEntity
	 * @see #referenceEntity
	 */
	public ReactomeReferenceEntity getReferenceEntity() {
		return referenceEntity;
	}

	/**
	 * @param referenceEntity
	 *          the referenceEntity to set
	 * @see #referenceEntity
	 */
	public void setReferenceEntity(ReactomeReferenceEntity referenceEntity) {
		this.referenceEntity = referenceEntity;
	}

	/**
	 * @return the startCoordinate
	 * @see #startCoordinate
	 */
	public Integer getStartCoordinate() {
		return startCoordinate;
	}

	/**
	 * @param startCoordinate
	 *          the startCoordinate to set
	 * @see #startCoordinate
	 */
	public void setStartCoordinate(Integer startCoordinate) {
		this.startCoordinate = startCoordinate;
	}

	/**
	 * @return the endCoordinate
	 * @see #endCoordinate
	 */
	public Integer getEndCoordinate() {
		return endCoordinate;
	}

	/**
	 * @param endCoordinate
	 *          the endCoordinate to set
	 * @see #endCoordinate
	 */
	public void setEndCoordinate(Integer endCoordinate) {
		this.endCoordinate = endCoordinate;
	}

}
