package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=InterChainCrosslinkedResidue"
 * >InterChainCrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeInterChainCrosslinkedResidue extends ReactomeCrosslinkedResidue {
	/**
	 * List of InterChainCrosslinkedResidue params in reactome model. More
	 * information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeInterChainCrosslinkedResidue>	equivalentTos							= new ArrayList<ReactomeInterChainCrosslinkedResidue>();
	/**
	 * List of ReferenceSequence params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeReferenceSequence>							secondReferenceSequences	= new ArrayList<ReactomeReferenceSequence>();

	/**
	 * Adds object to {@link #equivalentTos} list.
	 * 
	 * @param equivalentTo
	 *          object to add
	 */
	public void addEquivalentTo(ReactomeInterChainCrosslinkedResidue equivalentTo) {
		this.equivalentTos.add(equivalentTo);
	}

	/**
	 * Adds object to {@link #secondReferenceSequences} list.
	 * 
	 * @param secondReferenceSequence
	 *          object to add
	 */
	public void addSecondReferenceSequence(ReactomeReferenceSequence secondReferenceSequence) {
		this.secondReferenceSequences.add(secondReferenceSequence);
	}

	/**
	 * @return the equivalentTos
	 * @see #equivalentTos
	 */
	public List<ReactomeInterChainCrosslinkedResidue> getEquivalentTos() {
		return equivalentTos;
	}

	/**
	 * @param equivalentTos
	 *          the equivalentTos to set
	 * @see #equivalentTos
	 */
	public void setEquivalentTos(List<ReactomeInterChainCrosslinkedResidue> equivalentTos) {
		this.equivalentTos = equivalentTos;
	}

	/**
	 * @return the secondReferenceSequences
	 * @see #secondReferenceSequences
	 */
	public List<ReactomeReferenceSequence> getSecondReferenceSequences() {
		return secondReferenceSequences;
	}

	/**
	 * @param secondReferenceSequences
	 *          the secondReferenceSequences to set
	 * @see #secondReferenceSequences
	 */
	public void setSecondReferenceSequences(List<ReactomeReferenceSequence> secondReferenceSequences) {
		this.secondReferenceSequences = secondReferenceSequences;
	}

}
