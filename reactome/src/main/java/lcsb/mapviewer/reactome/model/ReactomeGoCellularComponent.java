package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_CellularComponent"
 * >GoCellularComponent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGoCellularComponent extends ReactomeDatabaseObject {
	/**
	 * Accession param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String										accession;
	/**
	 * Definition param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String										definition;
	/**
	 * ReferenceDatabase param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceDatabase	referenceDatabase;

	/**
	 * @return the accession
	 * @see #accession
	 */
	public String getAccession() {
		return accession;
	}

	/**
	 * @param accession
	 *          the accession to set
	 * @see #accession
	 */
	public void setAccession(String accession) {
		this.accession = accession;
	}

	/**
	 * @return the definition
	 * @see #definition
	 */
	public String getDefinition() {
		return definition;
	}

	/**
	 * @param definition
	 *          the definition to set
	 * @see #definition
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	/**
	 * @return the referenceDatabase
	 * @see #referenceDatabase
	 */
	public ReactomeReferenceDatabase getReferenceDatabase() {
		return referenceDatabase;
	}

	/**
	 * @param referenceDatabase
	 *          the referenceDatabase to set
	 * @see #referenceDatabase
	 */
	public void setReferenceDatabase(ReactomeReferenceDatabase referenceDatabase) {
		this.referenceDatabase = referenceDatabase;
	}

}
