package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceDatabase"
 * >ReferenceDatabase</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceDatabase extends ReactomeDatabaseObject {
	/**
	 * Access url in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String				accessUrl;
	/**
	 * Url in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String				url;
	/**
	 * List of names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>	names	= new ArrayList<String>();

	/**
	 * Adds name to {@link #names} list.
	 * 
	 * @param name
	 *          object to add
	 */
	public void addName(String name) {
		names.add(name);
	}

	/**
	 * @return the accessUrl
	 * @see #accessUrl
	 */
	public String getAccessUrl() {
		return accessUrl;
	}

	/**
	 * @param accessUrl
	 *          the accessUrl to set
	 * @see #accessUrl
	 */
	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

	/**
	 * @return the url
	 * @see #url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *          the url to set
	 * @see #url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the names
	 * @see #names
	 */
	public List<String> getNames() {
		return names;
	}

	/**
	 * @param names
	 *          the names to set
	 * @see #names
	 */
	public void setNames(List<String> names) {
		this.names = names;
	}
}
