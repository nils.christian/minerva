package lcsb.mapviewer.reactome.model;

import java.util.Calendar;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=InstanceEdit"
 * >InstanceEdit</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeInstanceEdit extends ReactomeDatabaseObject {
	/**
	 * Person param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomePerson	author;
	/**
	 * Note param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String					note;
	/**
	 * Date and time param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Calendar				dateTime;

	/**
	 * @return the author
	 * @see #author
	 */
	public ReactomePerson getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *          the author to set
	 * @see #author
	 */
	public void setAuthor(ReactomePerson author) {
		this.author = author;
	}

	/**
	 * @return the note
	 * @see #note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *          the note to set
	 * @see #note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the dateTime
	 * @see #dateTime
	 */
	public Calendar getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime
	 *          the dateTime to set
	 * @see #dateTime
	 */
	public void setDateTime(Calendar dateTime) {
		this.dateTime = dateTime;
	}
}
