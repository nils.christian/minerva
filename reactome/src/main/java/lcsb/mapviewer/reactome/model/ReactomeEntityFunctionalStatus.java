package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityFunctionalStatus"
 * >EntityFunctionalStatus</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntityFunctionalStatus extends ReactomeDatabaseObject {
	/**
	 * FunctionalStatus param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeFunctionalStatus>	functionalStatuses	= new ArrayList<ReactomeFunctionalStatus>();
	/**
	 * PhysicalEntity param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomePhysicalEntity					physicalEntity;

	/**
	 * Adds status to {@link #functionalStatuses}.
	 * 
	 * @param functionalStatus
	 *          object to add
	 */
	public void addFunctionalStatus(ReactomeFunctionalStatus functionalStatus) {
		this.functionalStatuses.add(functionalStatus);
	}

	/**
	 * @return the functionalStatuses
	 * @see #functionalStatuses
	 */
	public List<ReactomeFunctionalStatus> getFunctionalStatuses() {
		return functionalStatuses;
	}

	/**
	 * @param functionalStatuses
	 *          the functionalStatuses to set
	 * @see #functionalStatuses
	 */
	public void setFunctionalStatuses(List<ReactomeFunctionalStatus> functionalStatuses) {
		this.functionalStatuses = functionalStatuses;
	}

	/**
	 * @return the physicalEntity
	 * @see #physicalEntity
	 */
	public ReactomePhysicalEntity getPhysicalEntity() {
		return physicalEntity;
	}

	/**
	 * @param physicalEntity
	 *          the physicalEntity to set
	 * @see #physicalEntity
	 */
	public void setPhysicalEntity(ReactomePhysicalEntity physicalEntity) {
		this.physicalEntity = physicalEntity;
	}
}
