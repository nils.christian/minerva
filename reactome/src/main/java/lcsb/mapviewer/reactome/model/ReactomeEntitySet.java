package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntitySet"
 * >EntitySet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntitySet extends ReactomePhysicalEntity {
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>	hasMembers	= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeSpecies>					species			= new ArrayList<ReactomeSpecies>();

	/**
	 * Adds physical entity to {@link #hasMembers}.
	 * 
	 * @param hasMember
	 *          object to add
	 */
	public void addHasMembers(ReactomePhysicalEntity hasMember) {
		this.hasMembers.add(hasMember);
	}

	/**
	 * Adds secies to {@link #species} list.
	 * 
	 * @param species
	 *          object to add
	 */
	public void addSpecies(ReactomeSpecies species) {
		this.species.add(species);
	}

	/**
	 * @return the hasMembers
	 * @see #hasMembers
	 */
	public List<ReactomePhysicalEntity> getHasMembers() {
		return hasMembers;
	}

	/**
	 * @param hasMembers
	 *          the hasMembers to set
	 * @see #hasMembers
	 */
	public void setHasMembers(List<ReactomePhysicalEntity> hasMembers) {
		this.hasMembers = hasMembers;
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public List<ReactomeSpecies> getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(List<ReactomeSpecies> species) {
		this.species = species;
	}
}
