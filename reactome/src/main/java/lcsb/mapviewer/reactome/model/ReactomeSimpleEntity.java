package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SimpleEntity"
 * >SimpleEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSimpleEntity extends ReactomePhysicalEntity {
	/**
	 * List of ReferenceMolecule params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeReferenceMolecule>	referenceEntities	= new ArrayList<ReactomeReferenceMolecule>();
	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeSpecies									species;

	/**
	 * Adds entity to {@link #referenceEntities} list.
	 * 
	 * @param referenceEntity
	 *          object to add
	 */
	public void addReferenceEntity(ReactomeReferenceMolecule referenceEntity) {
		referenceEntities.add(referenceEntity);
	}

	/**
	 * @return the referenceEntities
	 * @see #referenceEntities
	 */
	public List<ReactomeReferenceMolecule> getReferenceEntities() {
		return referenceEntities;
	}

	/**
	 * @param referenceEntities
	 *          the referenceEntities to set
	 * @see #referenceEntities
	 */
	public void setReferenceEntities(List<ReactomeReferenceMolecule> referenceEntities) {
		this.referenceEntities = referenceEntities;
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public ReactomeSpecies getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(ReactomeSpecies species) {
		this.species = species;
	}

}
