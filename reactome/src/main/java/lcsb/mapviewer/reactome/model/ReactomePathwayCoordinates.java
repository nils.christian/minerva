package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayCoordinates"
 * >PathwayCoordinates</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathwayCoordinates extends ReactomeDatabaseObject {
	/**
	 * Max x coordinate in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	maxX;
	/**
	 * Max y coordinate in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	maxY;
	/**
	 * Min x coordinate in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	minX;
	/**
	 * Min y coordinate in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer	minY;

	/**
	 * @return the maxX
	 * @see #maxX
	 */
	public Integer getMaxX() {
		return maxX;
	}

	/**
	 * @param maxX
	 *          the maxX to set
	 * @see #maxX
	 */
	public void setMaxX(Integer maxX) {
		this.maxX = maxX;
	}

	/**
	 * @return the maxY
	 * @see #maxY
	 */
	public Integer getMaxY() {
		return maxY;
	}

	/**
	 * @param maxY
	 *          the maxY to set
	 * @see #maxY
	 */
	public void setMaxY(Integer maxY) {
		this.maxY = maxY;
	}

	/**
	 * @return the minX
	 * @see #minX
	 */
	public Integer getMinX() {
		return minX;
	}

	/**
	 * @param minX
	 *          the minX to set
	 * @see #minX
	 */
	public void setMinX(Integer minX) {
		this.minX = minX;
	}

	/**
	 * @return the minY
	 * @see #minY
	 */
	public Integer getMinY() {
		return minY;
	}

	/**
	 * @param minY
	 *          the minY to set
	 * @see #minY
	 */
	public void setMinY(Integer minY) {
		this.minY = minY;
	}
}
