package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Complex"
 * >Complex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeComplex extends ReactomePhysicalEntity {
	/**
	 * List of PhysicalEntity param in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePhysicalEntity>		hasComponents			= new ArrayList<ReactomePhysicalEntity>();
	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeSpecies									species;
	/**
	 * Boolean param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Boolean													isChimeric;
	/**
	 * List of EntityCompartment params in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomeEntityCompartment>	includedLocations	= new ArrayList<ReactomeEntityCompartment>();

	/**
	 * Adds component to {@link #hasComponents}.
	 * 
	 * @param component
	 *          component to add
	 */
	public void addHasComponent(ReactomePhysicalEntity component) {
		hasComponents.add(component);
	}

	/**
	 * Adds EntityCompartment to {@link #includedLocations}.
	 * 
	 * @param includedLocation
	 *          compartment to add
	 */
	public void addIncludedLocation(ReactomeEntityCompartment includedLocation) {
		this.includedLocations.add(includedLocation);
	}

	/**
	 * @return the hasComponents
	 * @see #hasComponents
	 */
	public List<ReactomePhysicalEntity> getHasComponents() {
		return hasComponents;
	}

	/**
	 * @param hasComponents
	 *          the hasComponents to set
	 * @see #hasComponents
	 */
	public void setHasComponents(List<ReactomePhysicalEntity> hasComponents) {
		this.hasComponents = hasComponents;
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public ReactomeSpecies getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(ReactomeSpecies species) {
		this.species = species;
	}

	/**
	 * @return the isChimeric
	 * @see #isChimeric
	 */
	public Boolean getIsChimeric() {
		return isChimeric;
	}

	/**
	 * @param isChimeric
	 *          the isChimeric to set
	 * @see #isChimeric
	 */
	public void setIsChimeric(Boolean isChimeric) {
		this.isChimeric = isChimeric;
	}

	/**
	 * @return the includedLocations
	 * @see #includedLocations
	 */
	public List<ReactomeEntityCompartment> getIncludedLocations() {
		return includedLocations;
	}

	/**
	 * @param includedLocations
	 *          the includedLocations to set
	 * @see #includedLocations
	 */
	public void setIncludedLocations(List<ReactomeEntityCompartment> includedLocations) {
		this.includedLocations = includedLocations;
	}

}
