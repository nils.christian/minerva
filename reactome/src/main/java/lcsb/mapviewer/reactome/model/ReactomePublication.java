package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Publication"
 * >Publication</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePublication extends ReactomeDatabaseObject {
	/**
	 * List of Person params in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<ReactomePerson>	authors	= new ArrayList<ReactomePerson>();

	/**
	 * Title of publication.
	 */
	private String								title;

	/**
	 * Adds author to {@link #authors} list.
	 * 
	 * @param author
	 *          object to add
	 */
	public void addAuthor(ReactomePerson author) {
		this.authors.add(author);
	}

	/**
	 * @return the authors
	 * @see #authors
	 */
	public List<ReactomePerson> getAuthors() {
		return authors;
	}

	/**
	 * @param authors
	 *          the authors to set
	 * @see #authors
	 */
	public void setAuthors(List<ReactomePerson> authors) {
		this.authors = authors;
	}

	/**
	 * @return the title
	 * @see #title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *          the title to set
	 * @see #title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
