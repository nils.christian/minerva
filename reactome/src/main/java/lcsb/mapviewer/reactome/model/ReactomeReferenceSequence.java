package lcsb.mapviewer.reactome.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceSequence"
 * >ReferenceSequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceSequence extends ReactomeReferenceEntity {
	/**
	 * Species param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeSpecies	species;
	/**
	 * Checksum in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String					checksum;
	/**
	 * List of keywords in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>		keywords							= new ArrayList<String>();
	/**
	 * List of gene names in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>		geneNames							= new ArrayList<String>();
	/**
	 * List of comments in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>		comments							= new ArrayList<String>();
	/**
	 * List of descriptions in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>		descriptions					= new ArrayList<String>();
	/**
	 * List of secondary identifiers in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private List<String>		secondaryIdentifiers	= new ArrayList<String>();
	/**
	 * Is sequence changed information in reactome model. More information can be
	 * found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private String					isSequenceChanged;
	/**
	 * Length of the seqeuence in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private Integer					sequenceLength;

	/**
	 * Adds keyword to {@link #keywords} list.
	 * 
	 * @param keyword
	 *          object to add
	 */
	public void addKeyword(String keyword) {
		this.keywords.add(keyword);
	}

	/**
	 * Adds gene name to {@link #geneNames} list.
	 * 
	 * @param geneName
	 *          object to add
	 */
	public void addGeneName(String geneName) {
		this.geneNames.add(geneName);
	}

	/**
	 * Adds comment to {@link #comments} list.
	 * 
	 * @param comment
	 *          object to add
	 */
	public void addComment(String comment) {
		this.comments.add(comment);
	}

	/**
	 * Adds description to {@link #descriptions} list.
	 * 
	 * @param description
	 *          object to add
	 */
	public void addDescription(String description) {
		this.descriptions.add(description);
	}

	/**
	 * Adds identifier to {@link #secondaryIdentifiers} list.
	 * 
	 * @param secondaryIdentifier
	 *          object to add
	 */
	public void addSecondaryIdentifier(String secondaryIdentifier) {
		this.secondaryIdentifiers.add(secondaryIdentifier);
	}

	/**
	 * @return the species
	 * @see #species
	 */
	public ReactomeSpecies getSpecies() {
		return species;
	}

	/**
	 * @param species
	 *          the species to set
	 * @see #species
	 */
	public void setSpecies(ReactomeSpecies species) {
		this.species = species;
	}

	/**
	 * @return the checksum
	 * @see #checksum
	 */
	public String getChecksum() {
		return checksum;
	}

	/**
	 * @param checksum
	 *          the checksum to set
	 * @see #checksum
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	/**
	 * @return the keywords
	 * @see #keywords
	 */
	public List<String> getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords
	 *          the keywords to set
	 * @see #keywords
	 */
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the geneNames
	 * @see #geneNames
	 */
	public List<String> getGeneNames() {
		return geneNames;
	}

	/**
	 * @param geneNames
	 *          the geneNames to set
	 * @see #geneNames
	 */
	public void setGeneNames(List<String> geneNames) {
		this.geneNames = geneNames;
	}

	/**
	 * @return the comments
	 * @see #comments
	 */
	public List<String> getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *          the comments to set
	 * @see #comments
	 */
	public void setComments(List<String> comments) {
		this.comments = comments;
	}

	/**
	 * @return the descriptions
	 * @see #descriptions
	 */
	public List<String> getDescriptions() {
		return descriptions;
	}

	/**
	 * @param descriptions
	 *          the descriptions to set
	 * @see #descriptions
	 */
	public void setDescriptions(List<String> descriptions) {
		this.descriptions = descriptions;
	}

	/**
	 * @return the secondaryIdentifiers
	 * @see #secondaryIdentifiers
	 */
	public List<String> getSecondaryIdentifiers() {
		return secondaryIdentifiers;
	}

	/**
	 * @param secondaryIdentifiers
	 *          the secondaryIdentifiers to set
	 * @see #secondaryIdentifiers
	 */
	public void setSecondaryIdentifiers(List<String> secondaryIdentifiers) {
		this.secondaryIdentifiers = secondaryIdentifiers;
	}

	/**
	 * @return the isSequenceChanged
	 * @see #isSequenceChanged
	 */
	public String getIsSequenceChanged() {
		return isSequenceChanged;
	}

	/**
	 * @param isSequenceChanged
	 *          the isSequenceChanged to set
	 * @see #isSequenceChanged
	 */
	public void setIsSequenceChanged(String isSequenceChanged) {
		this.isSequenceChanged = isSequenceChanged;
	}

	/**
	 * @return the sequenceLength
	 * @see #sequenceLength
	 */
	public Integer getSequenceLength() {
		return sequenceLength;
	}

	/**
	 * @param sequenceLength
	 *          the sequenceLength to set
	 * @see #sequenceLength
	 */
	public void setSequenceLength(Integer sequenceLength) {
		this.sequenceLength = sequenceLength;
	}

}
