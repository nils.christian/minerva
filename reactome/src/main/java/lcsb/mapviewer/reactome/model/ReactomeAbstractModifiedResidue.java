package lcsb.mapviewer.reactome.model;

/**
 * Object representing Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=AbstractModifiedResidue"
 * >AbstractModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeAbstractModifiedResidue extends ReactomeDatabaseObject {
	/**
	 * ReferenceSequence param in reactome model. More information can be found <a
	 * href="http://wiki.reactome.org/index.php/Glossary_Data_Model">here</a>.
	 */
	private ReactomeReferenceSequence	referenceSequence;

	/**
	 * @return the referenceSequence
	 * @see #referenceSequence
	 */
	public ReactomeReferenceSequence getReferenceSequence() {
		return referenceSequence;
	}

	/**
	 * @param referenceSequence
	 *          the referenceSequence to set
	 * @see #referenceSequence
	 */
	public void setReferenceSequence(ReactomeReferenceSequence referenceSequence) {
		this.referenceSequence = referenceSequence;
	}

}
