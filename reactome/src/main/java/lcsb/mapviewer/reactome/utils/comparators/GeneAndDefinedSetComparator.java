package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;

/**
 * This class allows to compare {@link Gene} element (internal
 * representation) and {@link ReactomeDefinedSet} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneAndDefinedSetComparator extends ANodeComparator<Gene, ReactomeDefinedSet> {

	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(GeneAndDefinedSetComparator.class);

	/**
	 * Default constructor.
	 */
	public GeneAndDefinedSetComparator() {
		super(Gene.class, ReactomeDefinedSet.class);
	}

	@Override
	public boolean compareNodes(Gene species, ReactomeDefinedSet rSpecies) {
		logger.warn(getElementTag(species) + " and " + rSpecies.getClass() + ", " + rSpecies.getDbId() + " should be comparable, but how??? Assuming FALSE");
		return false;
	}

}
