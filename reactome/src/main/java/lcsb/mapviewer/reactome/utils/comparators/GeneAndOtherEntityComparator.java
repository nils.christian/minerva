package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;

/**
 * This class allows to compare {@link Gene} element (internal
 * representation) and {@link ReactomeOtherEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneAndOtherEntityComparator extends ANodeComparator<Gene, ReactomeOtherEntity> {

	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(GeneAndOtherEntityComparator.class);

	/**
	 * Default constructor.
	 */
	public GeneAndOtherEntityComparator() {
		super(Gene.class, ReactomeOtherEntity.class);
	}

	@Override
	public boolean compareNodes(Gene species, ReactomeOtherEntity rSpecies) {
		logger.warn(getElementTag(species) + " and " + rSpecies.getClass() + ", " + rSpecies.getDbId() + " should be comparable, but how??? Assuming FALSE");
		return false;
	}

}
