package lcsb.mapviewer.reactome.utils.comparators;

import java.util.List;

import lcsb.mapviewer.annotation.services.annotators.ChebiSearchException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Ion} element (internal representation)
 * and {@link ReactomeComplex} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class IonAndComplexComparator extends ANodeComparator<Ion, ReactomeComplex> {

  /**
   * Default constructor.
   */
  public IonAndComplexComparator() {
    super(Ion.class, ReactomeComplex.class);
  }

  @Override
  public boolean compareNodes(Ion species, ReactomeComplex entity) throws ComparatorException {
    try {
      List<MiriamData> chebi1 = getChebiBackend().getOntologyChebiIdsForChebiName(species.getName());
      String chebi2 = null;
      if (chebi1.size() == 0) { // if we don't have id then return false (even
                                // with
        // two null values we cannot claim that they are
        // equal)
        return false;
      }
      int matched = 0;
      for (ReactomePhysicalEntity entity2 : entity.getHasComponents()) {
        if (entity2 instanceof ReactomeSimpleEntity) {
          for (ReactomeReferenceEntity reference : ((ReactomeSimpleEntity) entity2).getReferenceEntities()) {
            if (reference instanceof ReactomeReferenceMolecule) {
              boolean isChebi = false;
              if (reference.getReferenceDatabase() != null) {
                for (String name : reference.getReferenceDatabase().getNames()) {
                  if (name.equalsIgnoreCase("ChEBI")) {
                    isChebi = true;
                  }
                }
              }
              if (isChebi) {
                chebi2 = "CHEBI:" + reference.getIdentifier();
                if (chebi1.contains(new MiriamData(MiriamType.CHEBI, chebi2))) {
                  matched++;
                } // in other case we cannot claim that there are different
                  // (maybe
                  // there are more molecule references and other is going to
                  // point to correct chebi id)
              }
            }
          }
        } else if (entity2 instanceof ReactomeDefinedSet) {
          if (getGlobalComparator().compareNodes(species, (ReactomeDefinedSet) entity2)) {
            matched++;
          }
        } else if (entity2 instanceof ReactomeComplex) {
          if (getGlobalComparator().compareNodes(species, (ReactomeComplex) entity2)) {
            matched++;
          }
        } else if (entity2 instanceof ReactomeEntityWithAccessionedSequence) {
          return false;
        } else {
          throw new InvalidArgumentException("Cannot determine the way of comparison for provided class types: "
              + species.getClass() + ", " + entity2.getClass());
        }
      }
      return matched == entity.getHasComponents().size();
    } catch (ChebiSearchException e) {
      throw new ComparatorException(e);
    }
  }

}
