package lcsb.mapviewer.reactome.utils;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult.MatchStatus;
import lcsb.mapviewer.reactome.utils.comparators.NodeComparator;

/**
 * This class allows to comapre reaction from our model with the reaction from
 * reactome database.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class ReactionComparator {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	 logger	= Logger.getLogger(ReactionComparator.class);

	/**
	 * Comparator that allows to compare nodes in our format with nodes in
	 * reactome format.
	 */
	@Autowired
	private NodeComparator nc;

	/**
	 * Compares two reaction and return {@link MatchResult result} of this
	 * comparison.
	 * 
	 * @param reaction
	 *          reaction in our format.
	 * @param reactomeReaction
	 *          reactome reaction
	 * @return resul of reaction comparison. For more details see:
	 *         {@link MatchResult}
	 * @throws ComparatorException
	 *           thrown when there is a problem with comparison {@link Reaction}
	 *           with Reactome object
	 * 
	 */
	public MatchResult compareReactions(Reaction reaction, ReactomeReactionlikeEvent reactomeReaction) throws ComparatorException {
		MatchResult result = new MatchResult();
		result.setLocalReaction(reaction);
		result.setReactomeReaction(reactomeReaction);
		if (reactomeReaction == null) {
			result.setStatus(MatchStatus.INVALID_REACTOME_ID);
			return result;
		}

		for (ReactionNode node : reaction.getReactionNodes()) {
			if (node.getElement() instanceof Phenotype) {
				result.setStatus(MatchStatus.INVALID_REACTION_WITH_PHEONTYPE);
				return result;
			}
		}

		int matchingElements = 0;
		Set<Element> invalidReactionModifier = new HashSet<>();
		Set<Element> invalidReactionInput = new HashSet<>();
		Set<Element> invalidReactionOutput = new HashSet<>();

		Set<ReactomeDatabaseObject> validReactomeCatalyst = new HashSet<ReactomeDatabaseObject>();
		Set<ReactomeDatabaseObject> validReactomeInput = new HashSet<ReactomeDatabaseObject>();
		Set<ReactomeDatabaseObject> validReactomeOutput = new HashSet<ReactomeDatabaseObject>();

		for (ReactionNode node : reaction.getModifiers()) {
			boolean verified = false;
			Element species = node.getElement();

			for (ReactomeCatalystActivity catalystActivity : reactomeReaction.getCatalystActivities()) {
				if (nc.compareNodes(species, catalystActivity)) {
					verified = true;
					validReactomeCatalyst.add(catalystActivity);
				}
			}
			if (!verified) {
				invalidReactionModifier.add(species);
			} else {
				matchingElements++;
			}
		}
		for (ReactionNode node : reaction.getReactants()) {
			Element species = node.getElement();
			boolean verified = false;
			for (ReactomePhysicalEntity input : reactomeReaction.getInputs()) {
				if (nc.compareNodes(species, input)) {
					verified = true;
					validReactomeInput.add(input);
				}
			}
			if (!verified) {
				invalidReactionInput.add(species);
			} else {
				matchingElements++;
			}
		}
		for (ReactionNode node : reaction.getProducts()) {
			Element species = node.getElement();
			boolean verified = false;
			for (ReactomePhysicalEntity input : reactomeReaction.getOutputs()) {
				if (nc.compareNodes(species, input)) {
					verified = true;
					validReactomeOutput.add(input);
				}
			}
			if (!verified) {
				invalidReactionOutput.add(species);
			} else {
				matchingElements++;
			}
		}

		int totalElements = matchingElements;

		for (Element species : invalidReactionInput) {
			result.addInvalidLocalInput(species);
			totalElements++;
		}
		for (Element species : invalidReactionModifier) {
			result.addInvalidLocalModifier(species);
			totalElements++;
		}
		for (Element species : invalidReactionOutput) {
			result.addInvalidLocalOutput(species);
			totalElements++;
		}

		for (ReactomePhysicalEntity input : reactomeReaction.getInputs()) {
			if (!validReactomeInput.contains(input)) {
				result.addInvalidReactomeInput(input);
				totalElements++;
			}
		}
		for (ReactomeCatalystActivity catalystActivity : reactomeReaction.getCatalystActivities()) {
			if (!validReactomeCatalyst.contains(catalystActivity)) {
				result.addInvalidReactomeModifier(catalystActivity);
				totalElements++;
			}
		}
		for (ReactomePhysicalEntity output : reactomeReaction.getOutputs()) {
			if (!validReactomeOutput.contains(output)) {
				result.addInvalidReactomeOutput(output);
				totalElements++;
			}
		}

		result.setScore(((double) matchingElements) / ((double) totalElements));

		if (totalElements > matchingElements) {
			result.setStatus(MatchStatus.MISMATCH);
		} else {
			result.setStatus(MatchStatus.OK);
		}
		return result;

	}

	/**
	 * @return the nc
	 * @see #nc
	 */
	public NodeComparator getNc() {
		return nc;
	}

	/**
	 * @param nc
	 *          the nc to set
	 * @see #nc
	 */
	public void setNc(NodeComparator nc) {
		this.nc = nc;
	}
}
