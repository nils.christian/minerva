package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;

/**
 * This class allows to compare {@link Unknown} element (internal
 * representation) and {@link ReactomeGenomeEncodedEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownAndGenomeEncodedEntityComparator extends ANodeComparator<Unknown, ReactomeGenomeEncodedEntity> {

	/**
	 * Default constructor.
	 */
	public UnknownAndGenomeEncodedEntityComparator() {
		super(Unknown.class, ReactomeGenomeEncodedEntity.class);
	}

	@Override
	public boolean compareNodes(Unknown species, ReactomeGenomeEncodedEntity entity) {
		// we can compare the objects only using names...
		for (String name : entity.getNames()) {
			if (name.trim().equalsIgnoreCase(species.getName().trim())) {
				return true;
			}
		}
		return false;
	}

}
