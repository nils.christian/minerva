package lcsb.mapviewer.reactome.utils.comparators;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeCandidateSet} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndCandidateSetComparator extends ANodeComparator<Protein, ReactomeCandidateSet> {

	/**
	 * Default constructor.
	 */
	public ProteinAndCandidateSetComparator() {
		super(Protein.class, ReactomeCandidateSet.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeCandidateSet rSpecies) throws ComparatorException {
		boolean result = false;
		Set<ReactomePhysicalEntity> objects = new HashSet<ReactomePhysicalEntity>();
		objects.addAll(rSpecies.getHasMembers());
		objects.addAll(rSpecies.getHasCandidates());
		for (ReactomePhysicalEntity entity : objects) {
			if (entity instanceof ReactomeComplex) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeComplex) entity);
			} else if (entity instanceof ReactomeEntityWithAccessionedSequence) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeEntityWithAccessionedSequence) entity);
			} else {
				throw new InvalidArgumentException(
						"Cannot determine the way of comparison for provided class types: " + species.getClass() + ", " + entity.getClass());
			}
		}
		return result;
	}

}
