package lcsb.mapviewer.reactome.utils.comparators;

import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.utils.ComparatorException;
import lcsb.mapviewer.reactome.utils.ElementUtil;
import lcsb.mapviewer.reactome.utils.ReactomeQueryUtil;

/**
 * This abstract class define interface for comparison between object in interal
 * representation (model package) and object from the reactome database.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class type of the object in internal representation
 * @param <U>
 *          class type of the object in reactome representation
 */

// TODO should implements comparable<T,U>
@Transactional(value = "txManager")
public abstract class ANodeComparator<T extends Element, U extends ReactomeDatabaseObject> {
	/**
	 * Default class logger.
	 */
	private static Logger																		 logger	= Logger.getLogger(ANodeComparator.class);

	/**
	 * Service used for annotation of proteins (and finding annotation of
	 * proteins).
	 */
	@Autowired
	private HgncAnnotator																		 hgncAnnotator;

	/**
	 * Util object used for processing elements.
	 */
	@Autowired
	private ElementUtil																			 elementUtil;

	/**
	 * Service accessing chebi database.
	 */
	@Autowired
	private ChebiAnnotator																	 chebiBackend;

	/**
	 * This object allows to query reactome database for missing elements.
	 */
	@Autowired
	private ReactomeQueryUtil																 rcu;

	/**
	 * Comparator that allows to compare everything with everything else.
	 */
	private ANodeComparator<Element, ReactomeDatabaseObject> globalComparator;

	/**
	 * Type of the object in internal representation for which the comparator is
	 * designed for.
	 */
	private Class<T>																				 speciesClass;

	/**
	 * Type of the object in reactome representation for which the comparator is
	 * designed for.
	 */
	private Class<U>																				 reactomeSpeciesClass;

	/**
	 * Default constructor.
	 * 
	 * @param clazzA
	 *          Type of the object in internal representation for which the
	 *          comparator is designed for.
	 * @param clazzB
	 *          Type of the object in reactome representation for which the
	 *          comparator is designed for.
	 */
	public ANodeComparator(Class<T> clazzA, Class<U> clazzB) {
		speciesClass = clazzA;
		reactomeSpeciesClass = clazzB;
	}

	/**
	 * Method that compares two species.
	 * 
	 * @param species
	 *          object in internal representation to compare
	 * @param reactomeSpecies
	 *          object in reactome representation to compare
	 * @return <i>true</i> if objects can be considered as equal,<br/>
	 *         <i>false</i> otherwise
	 * @throws ComparatorException
	 *           thrown when there is a problem during comparison
	 */
	public boolean compareNodes(T species, U reactomeSpecies) throws ComparatorException {
		logger.warn("Don't know how to compare: " + species.getClass() + " and " + reactomeSpecies.getClass() + ". Assuming false.");
		return false;
	}

	/**
	 * 
	 * @return the speciesClass object
	 */
	public Class<T> getSpeciesClass() {
		return speciesClass;
	}

	/**
	 * 
	 * @return the reactomeSpeciesClass object
	 */
	public Class<U> getReactomeSpeciesClass() {
		return reactomeSpeciesClass;
	}

	/**
	 * Method that compares two sets of strings.
	 * 
	 * @param localSet
	 *          set of strings and subsets of strings
	 * @param reactomeSet
	 *          second set of strings
	 * @return <i>true</i> if two sets contain the same strings,<br/>
	 *         <i>false</i> otherwise
	 */
	protected boolean compareSets(Set<MiriamData> localSet, Set<MiriamData> reactomeSet) {
		if (localSet.size() != reactomeSet.size()) {
			return false;
		}
		for (MiriamData md : reactomeSet) {
			if (!localSet.contains(md)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @return the rcu object
	 */
	public ReactomeQueryUtil getRcu() {
		return rcu;
	}

	/**
	 * 
	 * @param rcu
	 *          the rcu to set
	 */
	public void setRcu(ReactomeQueryUtil rcu) {
		this.rcu = rcu;
	}

	/**
	 * @return the globalComparator
	 */
	protected ANodeComparator<Element, ReactomeDatabaseObject> getGlobalComparator() {
		return globalComparator;
	}

	/**
	 * @param globalComparator
	 *          the globalComparator to set
	 */
	protected void setGlobalComparator(ANodeComparator<Element, ReactomeDatabaseObject> globalComparator) {
		this.globalComparator = globalComparator;
	}

	/**
	 * @return the chebiBackend
	 * @see #chebiBackend
	 */
	public ChebiAnnotator getChebiBackend() {
		return chebiBackend;
	}

	/**
	 * @param chebiBackend
	 *          the chebiBackend to set
	 * @see #chebiBackend
	 */
	public void setChebiBackend(ChebiAnnotator chebiBackend) {
		this.chebiBackend = chebiBackend;
	}

	/**
	 * @return the elementUtil
	 * @see #elementUtil
	 */
	protected ElementUtil getElementUtil() {
		return elementUtil;
	}

	/**
	 * @param elementUtil
	 *          the elementUtil to set
	 * @see #elementUtil
	 */
	protected void setElementUtil(ElementUtil elementUtil) {
		this.elementUtil = elementUtil;
	}

	/**
	 * @return the hgncAnnotator
	 * @see #hgncAnnotator
	 */
	public HgncAnnotator getHgncAnnotator() {
		return hgncAnnotator;
	}

	/**
	 * @param hgncAnnotator
	 *          the hgncAnnotator to set
	 * @see #hgncAnnotator
	 */
	public void setHgncAnnotator(HgncAnnotator hgncAnnotator) {
		this.hgncAnnotator = hgncAnnotator;
	}

	/**
	 * Returns short string description identifing element.
	 * 
	 * @param element
	 *          element to be transformed into string id
	 * @return short string description identifing element
	 */
	public String getElementTag(BioEntity element) {
		return new ElementUtils().getElementTag(element);
	}

}
