package lcsb.mapviewer.reactome.utils.comparators;

import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeEntityWithAccessionedSequence} (reactome
 * model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndEntityWithAccessionedSequenceComparator extends ANodeComparator<Protein, ReactomeEntityWithAccessionedSequence> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ProteinAndEntityWithAccessionedSequenceComparator.class);

	/**
	 * Default constructor.
	 */
	public ProteinAndEntityWithAccessionedSequenceComparator() {
		super(Protein.class, ReactomeEntityWithAccessionedSequence.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeEntityWithAccessionedSequence entity) throws ComparatorException {
		try {
			Set<MiriamData> uniprotId1 = getElementUtil().getMiriamByType(species, MiriamType.UNIPROT, true);

			MiriamData uniprotId2;

			ReactomeReferenceEntity reference = entity.getReferenceEntity();
			if (reference instanceof ReactomeReferenceGeneProduct) {
				uniprotId2 = new MiriamData(MiriamType.UNIPROT, reference.getIdentifier());
			} else {
				throw new InvalidArgumentException("Unknown class type (" + reference.getClass() + "). Don't know what to do");
			}
			return uniprotId1.contains(uniprotId2);
		} catch (Exception e1) {
			throw new ComparatorException(e1);
		}
	}

}
