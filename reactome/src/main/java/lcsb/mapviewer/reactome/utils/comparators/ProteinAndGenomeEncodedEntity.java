package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeGenomeEncodedEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndGenomeEncodedEntity extends ANodeComparator<Protein, ReactomeGenomeEncodedEntity> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger	= Logger.getLogger(ProteinAndGenomeEncodedEntity.class);

	/**
	 * Default constructor.
	 */
	public ProteinAndGenomeEncodedEntity() {
		super(Protein.class, ReactomeGenomeEncodedEntity.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeGenomeEncodedEntity entity) {
		for (String name : entity.getNames()) {
			if (name.trim().equalsIgnoreCase(name.trim())) {
				return true;
			}
		}
		return false;
	}

}
