package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeSimpleEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndSimpleEntityComparator extends ANodeComparator<Protein, ReactomeSimpleEntity> {

	/**
	 * Default constructor.
	 */
	public ProteinAndSimpleEntityComparator() {
		super(Protein.class, ReactomeSimpleEntity.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeSimpleEntity cSpecies) {
		return false;
	}

}
