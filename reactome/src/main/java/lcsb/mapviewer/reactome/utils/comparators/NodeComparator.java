package lcsb.mapviewer.reactome.utils.comparators;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.persist.SpringApplicationContext;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * Comparator that allows to compare nodes in our format with nodes in reactome
 * format.
 * 
 * @author Piotr Gawron
 * 
 */
public class NodeComparator extends ANodeComparator<Element, ReactomeDatabaseObject> {
	/**
	 * Default class logger.
	 */
	private static Logger											 logger			 = Logger.getLogger(NodeComparator.class);

	/**
	 * This map contains implementation of all known comparators between local
	 * node representation and reactome node representation.
	 */
	private Map<String, ANodeComparator<?, ?>> comparators = new HashMap<String, ANodeComparator<?, ?>>();

	/**
	 * Default constructor. Initializes {@link #comparators} with every known
	 * implementation of {@link ANodeComparator}.
	 */
	public NodeComparator() {
		super(Element.class, ReactomeDatabaseObject.class);
	}

	/**
	 * Adds comparator to the list of known comparators.
	 * 
	 * @param comparator
	 *          comparator to add
	 */
	private void addComparator(ANodeComparator<?, ?> comparator) {
		comparators.put(comparator.getSpeciesClass().getName() + "," + comparator.getReactomeSpeciesClass().getName(), comparator);
		comparator.setGlobalComparator(this);
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean compareNodes(Element species, ReactomeDatabaseObject reactomeSpecies) throws ComparatorException {
		String speciesName = species.getClass().getName();
		if (species instanceof Protein) {
			speciesName = Protein.class.getName();
		}
		String serializedClassComparator = speciesName + "," + reactomeSpecies.getClass().getName();
		if (serializedClassComparator.indexOf("_$$_javassist_") >= 0) {
			serializedClassComparator = serializedClassComparator.substring(0, serializedClassComparator.indexOf("_$$_javassist_"));
		}
		ANodeComparator comparator = getComparator(serializedClassComparator);
		if (comparator != null) {
			return comparator.compareNodes(species, reactomeSpecies);
		} else {
			logger.debug("Debug information for exception: ");
			logger.debug("Species: " + getElementTag(species));
			logger.debug("Reactome object: " + reactomeSpecies.getDbId());
			throw new InvalidArgumentException("Unknown comparator for classes: " + serializedClassComparator);
		}

	}

	/**
	 * Initializes node comparators. We cannot do in constructor, because
	 * {@link #applicationContext} is not set yet.
	 */
	private void init() {
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndCandidateSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndCatalystComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndComplexComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndDefinedSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndEntityWithAccessionedSequenceComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndOtherEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ComplexAndSimpleEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(DrugAndCatalystComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(GeneAndDefinedSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(GeneAndOtherEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(IonAndCatalystComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(IonAndComplexComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(IonAndDefinedSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(IonAndEntityWithAccessionedSequenceComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(IonAndSimpleEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(MoleculeAndCatalystComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(MoleculeAndComplexComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(MoleculeAndDefinedSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(MoleculeAndEntityWithAccessionedSequenceComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(MoleculeAndSimpleEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndCandidateSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndCatalystComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndComplexComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndDefinedSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndDefinedSetComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndEntityWithAccessionedSequenceComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndGenomeEncodedEntity.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndPolymerComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndSimpleEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(ProteinAndOtherEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(UnknownAndCatalystComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(UnknownAndGenomeEncodedEntityComparator.class.getSimpleName()));
		addComparator((ANodeComparator<?, ?>) SpringApplicationContext.getBean(DegradedAndEntityWithAccessionedSequenceComparator.class.getSimpleName()));
	}

	/**
	 * Returns a comparator for nodes represented by the string.
	 * 
	 * @param serializedClassComparator
	 *          string with two class names separated by a coma
	 * @return comparator for given node classes
	 */
	@SuppressWarnings("rawtypes")
	private ANodeComparator getComparator(String serializedClassComparator) {
		if (comparators.size() == 0) {
			init();
		}
		return comparators.get(serializedClassComparator);
	}

}
