package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;

/**
 * This class allows to compare {@link Drug} element (internal
 * representation) and {@link ReactomeCatalystActivity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class DrugAndCatalystComparator extends ANodeComparator<Drug, ReactomeCatalystActivity> {
	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(DrugAndCatalystComparator.class);

	/**
	 * Default constructor.
	 */
	public DrugAndCatalystComparator() {
		super(Drug.class, ReactomeCatalystActivity.class);
	}

	@Override
	public boolean compareNodes(Drug species, ReactomeCatalystActivity cSpecies) {
		logger.warn("Don't know how to compare: " + species.getClass() + " and " + cSpecies.getClass() + ". Assuming false.");
		return false;
	}

}
