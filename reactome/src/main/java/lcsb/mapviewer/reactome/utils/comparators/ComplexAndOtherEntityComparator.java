package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;

/**
 * This class allows to compare {@link Complex} element (internal
 * representation) and {@link ReactomeOtherEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexAndOtherEntityComparator extends ANodeComparator<Complex, ReactomeOtherEntity> {

	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(ComplexAndOtherEntityComparator.class);

	/**
	 * Default constructor.
	 */
	public ComplexAndOtherEntityComparator() {
		super(Complex.class, ReactomeOtherEntity.class);
	}

	@Override
	public boolean compareNodes(Complex species, ReactomeOtherEntity rSpecies) {
		logger.warn(getElementTag(species) + " and " + rSpecies.getClass() + ", " + rSpecies.getDbId() + " should be comparable, but how??? Assuming FALSE");
		return false;
	}

}
