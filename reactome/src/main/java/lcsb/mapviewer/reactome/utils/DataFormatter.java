package lcsb.mapviewer.reactome.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult;

/**
 * This class contains some methods that helps to present in readable way data
 * received from the comparison of Reactome and PD map.
 * 
 * @author Piotr Gawron
 * 
 */
public class DataFormatter {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger			logger = Logger.getLogger(DataFormatter.class);

	/**
	 * Connector used for accessing data from miriam registry.
	 */
	@Autowired
	private MiriamConnector		mc;

	/**
	 * Util class that allows to query reactome database.
	 */
	@Autowired
	private ReactomeQueryUtil	rcu;

	/**
	 * Transforms list of Species (from local model) into readable string. Parts
	 * of the string contains links to external resources.
	 * 
	 * @param list
	 *          Species to transform
	 * @return human readable String representation of the linput Species list
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	private String speciesListToString(List<Element> list) throws AnnotatorException {
		String result = "";
		for (Element species : list) {
			if (!result.equals("")) {
				result += ",<hr/>";
			}
			result += species.getName() + "<br/>";
			Set<MiriamData> ids = null;
			if (species instanceof Complex) {
				ids = rcu.getIdsForComplex((Complex) species);
			} else {
				ids = rcu.getIdsForSpecies(species);
			}
			for (MiriamData md : ids) {
				result += "<a href=\"" + mc.getUrlString(md) + "\">" + md.getDataType().getCommonName() + "(" + md.getResource() + ")</a>,";
			}
		}
		return result;
	}

	/**
	 * Transforms list of reactome objects into readable string. Parts of the
	 * string contains links to external resources.
	 * 
	 * @param list
	 *          reactome objects to transform
	 * @return human readable String representation of the linput objects
	 */
	private String reactomeListToString(List<ReactomeDatabaseObject> list) {
		String result = "";
		for (ReactomeDatabaseObject species : list) {
			if (!result.equals("")) {
				result += ",<hr/>";
			}
			result += species.getDisplayName() + "<br/>";
			ReactomeDatabaseObject species2 = species;
			if (species instanceof ReactomeCatalystActivity) {
				species2 = ((ReactomeCatalystActivity) species).getPhysicalEntity();
			}
			Set<MiriamData> ids = new HashSet<MiriamData>();
			ids.addAll(rcu.getIdentifiersForReactomeEntity((ReactomePhysicalEntity) species2));
			for (MiriamData md : ids) {
				result += "<a href=\"" + mc.getUrlString(md) + "\">" + md.getDataType().getCommonName() + "(" + md.getResource() + ")</a>,";
			}
		}
		return result;
	}

	/**
	 * Returns String representing invalid local inputs from result.
	 * 
	 * @param result
	 *          MatchResult to process.
	 * @return human readable String representing invalid local inputs
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	public String getInvalidLocalInputString(MatchResult result) throws AnnotatorException {
		return speciesListToString(result.getInvalidLocalInput());
	}

	/**
	 * Returns String representing invalid local output from result.
	 * 
	 * @param result
	 *          MatchResult to process.
	 * @return human readable String representing invalid local output
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	public String getInvalidLocalOutputString(MatchResult result) throws AnnotatorException {
		return speciesListToString(result.getInvalidLocalOutput());
	}

	/**
	 * Returns String representing invalid local modifiers from result.
	 * 
	 * @param result
	 *          MatchResult to process.
	 * @return human readable String representing invalid local modifiers
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	public String getInvalidLocalModifierString(MatchResult result) throws AnnotatorException {
		return speciesListToString(result.getInvalidLocalModifier());
	}

	/**
	 * Returns String representing invalid reactome inputs from result.
	 * 
	 * @param result
	 *          MatchResult to process.
	 * @return human readable String representing invalid reactome inputs
	 */
	public String getInvalidReactomeInputString(MatchResult result) {
		return reactomeListToString(result.getInvalidReactomeInput());
	}

	/**
	 * Returns String representing invalid reactome modifiers from result.
	 * 
	 * @param result
	 *          MatchResult to process.
	 * @return human readable String representing invalid reactome modifiers
	 */
	public String getInvalidReactomeModifierString(MatchResult result) {
		return reactomeListToString(result.getInvalidReactomeModifier());
	}

	/**
	 * Returns String representing invalid reactome output from result.
	 * 
	 * @param result
	 *          MatchResult to process.
	 * @return human readable String representing invalid reactome output
	 */
	public String getInvalidReactomeOutputString(MatchResult result) {
		return reactomeListToString(result.getInvalidReactomeOutput());
	}

	/**
	 * @return the mc
	 * @see #mc
	 */
	public MiriamConnector getMc() {
		return mc;
	}

	/**
	 * @param mc
	 *          the mc to set
	 * @see #mc
	 */
	public void setMc(MiriamConnector mc) {
		this.mc = mc;
	}

}
