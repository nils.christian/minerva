package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Ion} element (internal
 * representation) and {@link ReactomeDefinedSet} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class IonAndDefinedSetComparator extends ANodeComparator<Ion, ReactomeDefinedSet> {

	/**
	 * Default constructor.
	 */
	public IonAndDefinedSetComparator() {
		super(Ion.class, ReactomeDefinedSet.class);
	}

	@Override
	public boolean compareNodes(Ion species, ReactomeDefinedSet rSpecies) throws ComparatorException {
		boolean result = false;
		for (ReactomePhysicalEntity entity : rSpecies.getHasMembers()) {
			if (entity instanceof ReactomeSimpleEntity) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeSimpleEntity) entity);
			} else if (entity instanceof ReactomeComplex) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeComplex) entity);
			} else if (entity instanceof ReactomeEntityWithAccessionedSequence) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeEntityWithAccessionedSequence) entity);
			} else {
				throw new InvalidArgumentException(
						"Cannot determine the way of comparison for provided class types: " + species.getClass() + ", " + entity.getClass());
			}
		}
		return result;
	}

}
