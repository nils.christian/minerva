package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomePolymer;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeDefinedSet} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndDefinedSetComparator extends ANodeComparator<Protein, ReactomeDefinedSet> {

	/**
	 * Default constructor.
	 */
	public ProteinAndDefinedSetComparator() {
		super(Protein.class, ReactomeDefinedSet.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeDefinedSet rSpecies) throws ComparatorException {
		boolean result = false;
		for (ReactomePhysicalEntity entity : rSpecies.getHasMembers()) {
			if (entity instanceof ReactomeComplex) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeComplex) entity);
			} else if (entity instanceof ReactomeEntityWithAccessionedSequence) {
				result |= getGlobalComparator().compareNodes(species, (ReactomeEntityWithAccessionedSequence) entity);
			} else if (entity instanceof ReactomePolymer) {
				result |= getGlobalComparator().compareNodes(species, (ReactomePolymer) entity);
			} else {
				throw new InvalidArgumentException(
						"Cannot determine the way of comparison for provided class types: " + species.getClass() + ", " + entity.getClass());
			}
		}
		return result;
	}

}
