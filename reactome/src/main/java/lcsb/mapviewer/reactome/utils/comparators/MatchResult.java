package lcsb.mapviewer.reactome.utils.comparators;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;

/**
 * This class represent result of the comparison between reaction in our model
 * and reaction in reactome format.
 * 
 * @author Piotr Gawron
 * 
 */
public class MatchResult {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger								 logger									 = Logger.getLogger(MatchResult.class);

	/**
	 * Local reaction to be compared.
	 */
	private Reaction										 localReaction;
	/**
	 * Reactome reaction to be compared.
	 */
	private ReactomeReactionlikeEvent		 reactomeReaction;

	/**
	 * How similar are the reactions (value between 0.0 and 1.0).
	 */
	private double											 score;

	/**
	 * What the status of the comparison.
	 */
	private MatchStatus									 status;

	/**
	 * Which local reactants couldn't be matched into reactome nodes.
	 */
	private List<Element>									 invalidLocalInput			 = new ArrayList<>();
	/**
	 * Which local products couldn't be matched into reactome nodes.
	 */
	private List<Element>									 invalidLocalOutput			 = new ArrayList<>();
	/**
	 * Which local modifiers couldn't be matched into reactome nodes.
	 */
	private List<Element>									 invalidLocalModifier		 = new ArrayList<>();

	/**
	 * Which reactome reactants couldn't be matched into local reaction nodes.
	 */
	private List<ReactomeDatabaseObject> invalidReactomeInput		 = new ArrayList<>();
	/**
	 * Which reactome products couldn't be matched into local reaction nodes.
	 */
	private List<ReactomeDatabaseObject> invalidReactomeOutput	 = new ArrayList<>();
	/**
	 * Which reactome modifiers couldn't be matched into local reaction nodes.
	 */
	private List<ReactomeDatabaseObject> invalidReactomeModifier = new ArrayList<>();

	/**
	 * General outcome of the reactions comparision.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	public enum MatchStatus {
		/**
		 * Reactions are the same.
		 */
		OK,
		/**
		 * There are some mismatches between rections.
		 */
		MISMATCH,
		/**
		 * Reactome identifier that annotates local reaction was invalid.
		 */
		INVALID_REACTOME_ID,
		/**
		 * We don't know how to compare reactions with pehontypes.
		 */
		INVALID_REACTION_WITH_PHEONTYPE,
		/**
		 * Local reaction wasn't annotated by reactome, but similar reaction in
		 * reactome was found.
		 */
		SIMILAR_REACTION_FOUND
	}

	/**
	 * Adds species to mismatched local reactants.
	 * 
	 * @param invalidLocalInput
	 *          species to be added
	 */
	public void addInvalidLocalInput(Element invalidLocalInput) {
		this.invalidLocalInput.add(invalidLocalInput);
	}

	/**
	 * Adds species to mismatched local products.
	 * 
	 * @param invalidLocalOutput
	 *          species to be added
	 */
	public void addInvalidLocalOutput(Element invalidLocalOutput) {
		this.invalidLocalOutput.add(invalidLocalOutput);
	}

	/**
	 * Adds species to mismatched local modifier.
	 * 
	 * @param invalidLocalModifier
	 *          species to be added
	 */
	public void addInvalidLocalModifier(Element invalidLocalModifier) {
		this.invalidLocalModifier.add(invalidLocalModifier);
	}

	/**
	 * Adds species to mismatched reactome reactants.
	 * 
	 * @param invalidReactomeInput
	 *          species to be added
	 */
	public void addInvalidReactomeInput(ReactomeDatabaseObject invalidReactomeInput) {
		this.invalidReactomeInput.add(invalidReactomeInput);
	}

	/**
	 * Adds species to mismatched reactome products.
	 * 
	 * @param invalidReactomeOutput
	 *          species to be added
	 */
	public void addInvalidReactomeOutput(ReactomeDatabaseObject invalidReactomeOutput) {
		this.invalidReactomeOutput.add(invalidReactomeOutput);
	}

	/**
	 * Adds species to mismatched reactome modifiers.
	 * 
	 * @param invalidReactomeModifier
	 *          species to be added
	 */
	public void addInvalidReactomeModifier(ReactomeDatabaseObject invalidReactomeModifier) {
		this.invalidReactomeModifier.add(invalidReactomeModifier);
	}

	/**
	 * @return the localReaction
	 * @see #localReaction
	 */
	public Reaction getLocalReaction() {
		return localReaction;
	}

	/**
	 * @param localReaction
	 *          the localReaction to set
	 * @see #localReaction
	 */
	public void setLocalReaction(Reaction localReaction) {
		this.localReaction = localReaction;
	}

	/**
	 * @return the reactomeReaction
	 * @see #reactomeReaction
	 */
	public ReactomeReactionlikeEvent getReactomeReaction() {
		return reactomeReaction;
	}

	/**
	 * @param reactomeReaction
	 *          the reactomeReaction to set
	 * @see #reactomeReaction
	 */
	public void setReactomeReaction(ReactomeReactionlikeEvent reactomeReaction) {
		this.reactomeReaction = reactomeReaction;
	}

	/**
	 * @return the score
	 * @see #score
	 */
	public double getScore() {
		return score;
	}

	/**
	 * @param score
	 *          the score to set
	 * @see #score
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * @return the status
	 * @see #status
	 */
	public MatchStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *          the status to set
	 * @see #status
	 */
	public void setStatus(MatchStatus status) {
		this.status = status;
	}

	/**
	 * @return the invalidLocalInput
	 * @see #invalidLocalInput
	 */
	public List<Element> getInvalidLocalInput() {
		return invalidLocalInput;
	}

	/**
	 * @param invalidLocalInput
	 *          the invalidLocalInput to set
	 * @see #invalidLocalInput
	 */
	public void setInvalidLocalInput(List<Element> invalidLocalInput) {
		this.invalidLocalInput = invalidLocalInput;
	}

	/**
	 * @return the invalidLocalOutput
	 * @see #invalidLocalOutput
	 */
	public List<Element> getInvalidLocalOutput() {
		return invalidLocalOutput;
	}

	/**
	 * @param invalidLocalOutput
	 *          the invalidLocalOutput to set
	 * @see #invalidLocalOutput
	 */
	public void setInvalidLocalOutput(List<Element> invalidLocalOutput) {
		this.invalidLocalOutput = invalidLocalOutput;
	}

	/**
	 * @return the invalidLocalModifier
	 * @see #invalidLocalModifier
	 */
	public List<Element> getInvalidLocalModifier() {
		return invalidLocalModifier;
	}

	/**
	 * @param invalidLocalModifier
	 *          the invalidLocalModifier to set
	 * @see #invalidLocalModifier
	 */
	public void setInvalidLocalModifier(List<Element> invalidLocalModifier) {
		this.invalidLocalModifier = invalidLocalModifier;
	}

	/**
	 * @return the invalidReactomeInput
	 * @see #invalidReactomeInput
	 */
	public List<ReactomeDatabaseObject> getInvalidReactomeInput() {
		return invalidReactomeInput;
	}

	/**
	 * @param invalidReactomeInput
	 *          the invalidReactomeInput to set
	 * @see #invalidReactomeInput
	 */
	public void setInvalidReactomeInput(List<ReactomeDatabaseObject> invalidReactomeInput) {
		this.invalidReactomeInput = invalidReactomeInput;
	}

	/**
	 * @return the invalidReactomeOutput
	 * @see #invalidReactomeOutput
	 */
	public List<ReactomeDatabaseObject> getInvalidReactomeOutput() {
		return invalidReactomeOutput;
	}

	/**
	 * @param invalidReactomeOutput
	 *          the invalidReactomeOutput to set
	 * @see #invalidReactomeOutput
	 */
	public void setInvalidReactomeOutput(List<ReactomeDatabaseObject> invalidReactomeOutput) {
		this.invalidReactomeOutput = invalidReactomeOutput;
	}

	/**
	 * @return the invalidReactomeModifier
	 * @see #invalidReactomeModifier
	 */
	public List<ReactomeDatabaseObject> getInvalidReactomeModifier() {
		return invalidReactomeModifier;
	}

	/**
	 * @param invalidReactomeModifier
	 *          the invalidReactomeModifier to set
	 * @see #invalidReactomeModifier
	 */
	public void setInvalidReactomeModifier(List<ReactomeDatabaseObject> invalidReactomeModifier) {
		this.invalidReactomeModifier = invalidReactomeModifier;
	}

}
