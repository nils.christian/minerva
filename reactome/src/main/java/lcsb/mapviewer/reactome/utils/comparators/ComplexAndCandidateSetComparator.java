package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;

/**
 * This class allows to compare {@link Complex} element (internal
 * representation) and {@link ReactomeCandidateSet} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexAndCandidateSetComparator extends ANodeComparator<Complex, ReactomeCandidateSet> {

	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(ComplexAndCandidateSetComparator.class);

	/**
	 * Default constructor.
	 */
	public ComplexAndCandidateSetComparator() {
		super(Complex.class, ReactomeCandidateSet.class);
	}

	@Override
	public boolean compareNodes(Complex species, ReactomeCandidateSet rSpecies) {
		logger.warn(getElementTag(species) + " and " + rSpecies.getClass() + ", " + rSpecies.getDbId() + " should be comparable, but how??? Assuming FALSE");
		return false;
	}

}
