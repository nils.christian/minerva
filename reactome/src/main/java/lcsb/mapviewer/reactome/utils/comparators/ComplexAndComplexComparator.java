package lcsb.mapviewer.reactome.utils.comparators;

import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Complex} element (internal
 * representation) and {@link ReactomeComplex} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexAndComplexComparator extends ANodeComparator<Complex, ReactomeComplex> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ComplexAndComplexComparator.class);

	/**
	 * Default constructor.
	 */
	public ComplexAndComplexComparator() {
		super(Complex.class, ReactomeComplex.class);
	}

	@Override
	public boolean compareNodes(Complex complex, ReactomeComplex reactomeComplex) throws ComparatorException {
		try {
			Set<MiriamData> complex1Ids = getRcu().getIdsForComplex(complex);
			if (complex1Ids == null || complex1Ids.size() == 0) {
				return false;
			}

			Set<MiriamData> uniprotIds2 = getRcu().getIdentifiersForReactomeEntity(reactomeComplex);
			return compareSets(complex1Ids, uniprotIds2);
		} catch (Exception e) {
			throw new ComparatorException(e);
		}
	}

}
