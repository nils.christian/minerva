package lcsb.mapviewer.reactome.utils.comparators;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeComplex} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndComplexComparator extends ANodeComparator<Protein, ReactomeComplex> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ProteinAndComplexComparator.class);

	/**
	 * Default constructor.
	 */
	public ProteinAndComplexComparator() {
		super(Protein.class, ReactomeComplex.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeComplex complex) throws ComparatorException {
		try {
			Set<MiriamData> uniprotIds1 = getElementUtil().getMiriamByType(species, MiriamType.UNIPROT, true);
			Set<MiriamData> uniprotIds2 = new HashSet<MiriamData>();
			uniprotIds2.addAll(getRcu().getIdentifiersForReactomeEntity(complex));
			if (uniprotIds1.size() != uniprotIds2.size()) {
				return false;
			}
			for (MiriamData string : uniprotIds2) {
				if (!uniprotIds1.contains(string)) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			throw new ComparatorException(e);
		}
	}

}
