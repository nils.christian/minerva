package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;

/**
 * This class allows to compare {@link Complex} element (internal
 * representation) and {@link ReactomeDefinedSet} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexAndDefinedSetComparator extends ANodeComparator<Complex, ReactomeDefinedSet> {

	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(ComplexAndDefinedSetComparator.class);

	/**
	 * Default constructor.
	 */
	public ComplexAndDefinedSetComparator() {
		super(Complex.class, ReactomeDefinedSet.class);
	}

	@Override
	public boolean compareNodes(Complex species, ReactomeDefinedSet rSpecies) {
		logger.warn(getElementTag(species) + " and " + rSpecies.getClass() + ", " + rSpecies.getDbId() + " should be comparable, but how??? Assuming FALSE");
		return false;
	}

}
