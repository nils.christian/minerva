package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Unknown} element (internal
 * representation) and {@link ReactomeCatalystActivity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownAndCatalystComparator extends ANodeComparator<Unknown, ReactomeCatalystActivity> {

	/**
	 * Default constructor.
	 */
	public UnknownAndCatalystComparator() {
		super(Unknown.class, ReactomeCatalystActivity.class);
	}

	@Override
	public boolean compareNodes(Unknown species, ReactomeCatalystActivity cSpecies) throws ComparatorException {
		ReactomePhysicalEntity pEntity = cSpecies.getPhysicalEntity();
		if (pEntity instanceof ReactomeGenomeEncodedEntity) {
			return getGlobalComparator().compareNodes(species, (ReactomeGenomeEncodedEntity) pEntity);
		} else {
			throw new InvalidArgumentException("Cannot determine the way of comparison for provided class types: " + species.getClass() + ", " + pEntity.getClass());
		}
	}

}
