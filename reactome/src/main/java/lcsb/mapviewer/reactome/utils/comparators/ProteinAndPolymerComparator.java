package lcsb.mapviewer.reactome.utils.comparators;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomePolymer;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomePolymer} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndPolymerComparator extends ANodeComparator<Protein, ReactomePolymer> {
	/**
	 * Default class logger.
	 */
	private static Logger	logger	= Logger.getLogger(ProteinAndPolymerComparator.class);

	/**
	 * Default constructor.
	 */
	public ProteinAndPolymerComparator() {
		super(Protein.class, ReactomePolymer.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomePolymer polymer) throws ComparatorException {
		Set<MiriamData> uniprotIds1 = new HashSet<MiriamData>();
		Set<MiriamData> uniprotIds2 = new HashSet<MiriamData>();
		for (MiriamData md : species.getMiriamData()) {
			if (MiriamType.UNIPROT.equals(md.getDataType())) {
				uniprotIds1.add(md);
			}
		}
		if (uniprotIds1.size() == 0) {
			try {
				for (MiriamData md : species.getMiriamData()) {
					if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
						List<MiriamData> uniprot = getHgncAnnotator().hgncToUniprot(md);
						uniprotIds1.addAll(uniprot);
					}
				}
				if (uniprotIds1.size() == 0) {
					logger.warn("Cannot find uniprot identifier for: " + species.getName());
					return false;
				}
			} catch (Exception e) {
				throw new InvalidArgumentException("Problem with converting ids: " + e.getMessage());
			}
		}

		for (ReactomePhysicalEntity entity : polymer.getRepeatedUnits()) {
			if (entity instanceof ReactomeEntityWithAccessionedSequence) {
				ReactomeReferenceEntity reference = ((ReactomeEntityWithAccessionedSequence) entity).getReferenceEntity();
				if (reference instanceof ReactomeReferenceGeneProduct) {
					uniprotIds2.add(new MiriamData(MiriamType.UNIPROT, reference.getIdentifier()));
				} else {
					throw new InvalidArgumentException("Unknown class type (" + reference.getClass() + "). Don't know what to do");
				}
			} else if (entity instanceof ReactomeDefinedSet) {
				logger.warn("Too complicated nesting... Ommiting...");
			} else if (entity instanceof ReactomeSimpleEntity) {
				// cannot compare protein and chemical
				return false;
			} else if (entity instanceof ReactomeOtherEntity) {
				// cannot compare protein and other entities
				return false;
			} else if (entity instanceof ReactomeComplex) {
				// if subcomplex match then add appropriate ids
				if (getGlobalComparator().compareNodes(species, (ReactomeComplex) entity)) {
					uniprotIds2.addAll(uniprotIds1);
				}
			} else {
				throw new InvalidArgumentException("Unknown class type (" + entity.getClass() + "). Don't know what to do");
			}
		}
		if (uniprotIds1.size() != uniprotIds2.size()) {
			return false;
		}
		for (MiriamData string : uniprotIds2) {
			if (!uniprotIds1.contains(string)) {
				return false;
			}
		}
		return true;
	}

}
