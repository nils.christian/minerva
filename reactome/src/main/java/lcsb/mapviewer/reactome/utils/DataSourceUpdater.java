package lcsb.mapviewer.reactome.utils;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;

import org.w3c.dom.Node;

/**
 * Interface used for accessing data from reactome.
 * 
 * @author Piotr Gawron
 * 
 */
public interface DataSourceUpdater extends IExternalService {
	/**
	 * Returns xml node prepresenting reactome object for reactome database
	 * identifier.
	 * 
	 * @param id
	 *          database identifier of the reactome object
	 * @return xml node for the given reactome object
	 * @throws InvalidXmlSchemaException 
	 *           thrown when there is a problem with xml 
	 */
	Node getFullNodeForDbId(Integer id) throws InvalidXmlSchemaException;

	/**
	 * Returns class type of the reactome object identifier by the database
	 * identifier given as a parameter.
	 * 
	 * @param id
	 *          reactome database identifier
	 * @return class type of the object identified by the reactome database id
	 * @throws ClassNotFoundException
	 *           thrown when the class cannot be found in the system
	 * @throws InvalidXmlSchemaException 
	 */
	Class<?> getTypeForDbId(Integer id) throws ClassNotFoundException, InvalidXmlSchemaException;

	/**
	 * Returns object that represents reactome object for given identifier.
	 * 
	 * @param id
	 *          database identifier of the object to be found
	 * @return reactome object for a give identifier
	 */
	ReactomeDatabaseObject getFullObjectForDbId(Integer id);

	/**
	 * Returns a map with reactome objects mapped to the identifiers given in the
	 * parameter.
	 * 
	 * @param ids
	 *          list of reactome database identifiers
	 * @return map with objects for identifiers given in the parameter
	 * @throws ClassNotFoundException
	 *           thrown when at least class of the one object cannot be found
	 * @throws IOException
	 *           if there are problems with accessing data
	 */
	Map<Integer, ReactomeDatabaseObject> getFullObjectsForDbIds(List<Integer> ids) throws ClassNotFoundException, IOException;

	/**
	 * Returns list of elements that correspond to the set of chebi identifiers.
	 * 
	 * @param chebiId
	 *          chebi identifier
	 * @param deepSearch
	 *          are we also interested in complex that contains object with chebi
	 *          id
	 * @return list of reactome objects for given chebi identifiers
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	List<ReactomePhysicalEntity> getEntitiesForChebiId(Set<MiriamData> chebiId, boolean deepSearch) throws IOException;

	/**
	 * Returns list of elements that correspond to the uniprot identifiers.
	 * 
	 * @param uniprot
	 *          set of uniprot identifiers
	 * @return list of reactome objects for given uniprot identifier
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	List<ReactomePhysicalEntity> getEntitiesForUniprotId(Collection<MiriamData> uniprot) throws IOException;

	/**
	 * Returns list of elements that correspond to the given name.
	 * 
	 * @param name
	 *          name of the object
	 * @return list of reactome objects for given name
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	List<ReactomePhysicalEntity> getEntitiesForName(String name) throws IOException;

	/**
	 * Returns list of elements that correspond to at least one identifier given
	 * in the parameter.
	 * 
	 * @param identifiers
	 *          set of identifiers, it's possible to put in the set another set
	 * @return list of reactome objects for given identifiers
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	List<ReactomePhysicalEntity> getEntitiesForSetOfIds(Set<MiriamData> identifiers) throws IOException;

	/**
	 * Returns list of reaction that are connected to the object with given
	 * identifier.
	 * 
	 * @param dbId
	 *          identifier of the object
	 * @return list of reactome reactions
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	List<ReactomeReactionlikeEvent> getReactionsForEntityId(Integer dbId) throws IOException;

	/**
	 * This method perform a query to database for objects of type clazz.
	 * 
	 * @param query
	 *          reactome query to the reactome database
	 * @param clazz
	 *          class of which we want to find object
	 * @return list of objects from reactome database that match to the query
	 * @throws IOException
	 *           exception thrown when there is a problem with reactome database
	 *           connection
	 */
	List<ReactomeDatabaseObject> getSimpleObjectListByQuery(String query, Class<? extends ReactomeDatabaseObject> clazz) throws IOException;

	/**
	 * Returns reactome object for stable identifier in the form
	 * 'identifier.version'.
	 * 
	 * @param identifier
	 *          identifier of the object
	 * @param version
	 *          version of the object
	 * @return reactome object for stable identifier
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	ReactomeDatabaseObject getFullObjectForStableIdentifier(String identifier, String version) throws IOException;

	/**
	 * Returns reactome object for stable identifier.
	 * 
	 * @param stableIdentifier
	 *          stable identifier of the object
	 * @return reactome object for stable identifier
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	ReactomeDatabaseObject getFullObjectForStableIdentifier(String stableIdentifier) throws IOException;

	/**
	 * Returns list of elements that correspond to the given name. 'full' flag
	 * determines if we want to retrieve full information about the object or
	 * maybe only standard short list of parameters.
	 * 
	 * @param name
	 *          name of the object
	 * @param full
	 *          determines if we want to retrieve full information about the
	 *          object or simplified information
	 * @return list of reactome objects for given name
	 * @throws IOException
	 *           thrown when there is a problem with accessing data
	 */
	List<ReactomePhysicalEntity> getEntitiesForName(String name, boolean full) throws IOException;

	/**
	 * Updates object fields that are reactome objects.
	 * 
	 * @param res
	 *          object to be updated
	 */
	void updateOnlyIdFields(ReactomeDatabaseObject res);

	/**
	 * Returns type of cached elements used by this interface.
	 * 
	 * @return type of cached elements used by this interface
	 */
	CacheType getCacheType();
}
