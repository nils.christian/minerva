package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;

/**
 * This class allows to compare {@link Degraded} element (internal
 * representation) and {@link ReactomeEntityWithAccessionedSequence} (reactome
 * model).
 * 
 * @author Piotr Gawron
 * 
 */
public class DegradedAndEntityWithAccessionedSequenceComparator extends ANodeComparator<Degraded, ReactomeEntityWithAccessionedSequence> {

	/**
	 * Default constructor.
	 */
	public DegradedAndEntityWithAccessionedSequenceComparator() {
		super(Degraded.class, ReactomeEntityWithAccessionedSequence.class);
	}
}
