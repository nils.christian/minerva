package lcsb.mapviewer.reactome.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ChebiSearchException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Util class used to get information about elements.
 * 
 * @author Piotr Gawron
 * 
 */
public class ElementUtil {

	/**
	 * Returns human readable id of the element from paramter.
	 * 
	 * @param element
	 *          element to be identified
	 * @return human readable id of the element from paramter
	 */
	public String getElementTag(BioEntity element) {
		return new ElementUtils().getElementTag(element);
	}

	/**
	 * Hgnc annotator used to resolve some identifier conversion.
	 */
	@Autowired
	private HgncAnnotator	 hgncAnnotator;

	/**
	 * Service accessing chebi database.
	 */
	@Autowired
	private ChebiAnnotator chebiBackend;

	/**
	 * Returns set of annotations by given type for given element (using local or
	 * remote information).
	 * 
	 * @param element
	 *          element for which annotation is looked for
	 * @param mt
	 *          {@link MiriamType} type of the annotation
	 * @param queryServer
	 *          if annotation is not found locally, should the method query
	 *          external server
	 * @return set of annotations
	 * @throws AnnotatorException
	 *           thrown when there is a problem with accessing external resource
	 */
	public Set<MiriamData> getMiriamByType(Element element, MiriamType mt, boolean queryServer) throws AnnotatorException {
		Set<MiriamData> result = new HashSet<MiriamData>();
		for (MiriamData md : element.getMiriamData()) {
			if (mt.equals(md.getDataType())) {
				result.add(md);
			}
		}
		if (result.size() == 0 && queryServer) {
			if (mt.equals(MiriamType.UNIPROT)) {
				result.addAll(getUniprot(element));
			} else if (MiriamType.CHEBI.equals(mt)) {
				try {
					MiriamData md = chebiBackend.getChebiForChebiName(element.getName());
					if (md != null) {
						result.add(md);
					}
				} catch (ChebiSearchException e) {
					throw new AnnotatorException("Problem with getting information about chebi", e);
				}
			} else {
				throw new InvalidStateException(getElementTag(element) + "Don't know how to query server for " + mt.getCommonName() + " in element");
			}
		}
		return result;
	}

	/**
	 * Returns set of {@link MiriamType#UNIPROT} annotations for the element.
	 * 
	 * @param element
	 *          object for which annotations are looked for
	 * @return set of {@link MiriamType#UNIPROT} annotations for the element
	 * @throws AnnotatorException
	 *           thrown when there is a problem with accessing external server
	 */
	private Collection<? extends MiriamData> getUniprot(Element element) throws AnnotatorException {
		Set<MiriamData> result = new HashSet<MiriamData>();
		for (MiriamData md : element.getMiriamData()) {
			if (MiriamType.HGNC_SYMBOL.equals(md.getDataType()) || MiriamType.HGNC.equals(md.getDataType())) {
				List<MiriamData> uniprot = hgncAnnotator.hgncToUniprot(md);
				result.addAll(uniprot);
			}
		}
		return result;
	}
}
