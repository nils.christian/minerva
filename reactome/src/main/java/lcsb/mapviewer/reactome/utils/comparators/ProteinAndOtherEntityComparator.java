package lcsb.mapviewer.reactome.utils.comparators;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeOtherEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndOtherEntityComparator extends ANodeComparator<Protein, ReactomeOtherEntity> {

	/**
	 * Default class logger.
	 */
	private static Logger logger = Logger.getLogger(ProteinAndOtherEntityComparator.class);

	/**
	 * Default constructor.
	 */
	public ProteinAndOtherEntityComparator() {
		super(Protein.class, ReactomeOtherEntity.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeOtherEntity rSpecies) {
		logger.warn(getElementTag(species) + " and " + rSpecies.getClass() + ", " + rSpecies.getDbId() + " should be comparable, but how??? Assuming FALSE");
		return false;
	}

}
