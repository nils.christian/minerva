package lcsb.mapviewer.reactome.utils.comparators;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Complex} element (internal
 * representation) and {@link ReactomeEntityWithAccessionedSequence} (reactome
 * model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexAndEntityWithAccessionedSequenceComparator
    extends ANodeComparator<Complex, ReactomeEntityWithAccessionedSequence> {

  /**
   * Default constructor.
   */
  public ComplexAndEntityWithAccessionedSequenceComparator() {
    super(Complex.class, ReactomeEntityWithAccessionedSequence.class);
  }

  @Override
  public boolean compareNodes(Complex complex, ReactomeEntityWithAccessionedSequence entity)
      throws ComparatorException {
    Set<MiriamData> complex1Ids;
    try {
      complex1Ids = getRcu().getIdsForComplex(complex);
    } catch (Exception e) {
      throw new ComparatorException(e);
    }
    Set<String> uniprotIds2 = new HashSet<String>();

    ReactomeReferenceEntity reference = entity.getReferenceEntity();
    if (reference instanceof ReactomeReferenceGeneProduct) {
      uniprotIds2.add(reference.getIdentifier());
    } else {
      throw new InvalidArgumentException("Unknown class type (" + reference.getClass() + "). Don't know what to do");
    }
    if (complex1Ids.size() != uniprotIds2.size()) {
      return false;
    }
    for (String string : uniprotIds2) {
      if (!complex1Ids.contains(new MiriamData(MiriamType.UNIPROT, string))) {
        return false;
      }
    }
    return true;
  }

}
