package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Protein} element (internal
 * representation) and {@link ReactomeCatalystActivity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinAndCatalystComparator extends ANodeComparator<Protein, ReactomeCatalystActivity> {

	/**
	 * Default constructor.
	 */
	public ProteinAndCatalystComparator() {
		super(Protein.class, ReactomeCatalystActivity.class);
	}

	@Override
	public boolean compareNodes(Protein species, ReactomeCatalystActivity cSpecies) throws ComparatorException {
		ReactomePhysicalEntity pEntity = cSpecies.getPhysicalEntity();
		if (pEntity instanceof ReactomeComplex) {
			return getGlobalComparator().compareNodes(species, (ReactomeComplex) pEntity);
		} else if (pEntity instanceof ReactomeEntityWithAccessionedSequence) {
			return getGlobalComparator().compareNodes(species, (ReactomeEntityWithAccessionedSequence) pEntity);
		} else if (pEntity instanceof ReactomeCandidateSet) {
			return getGlobalComparator().compareNodes(species, (ReactomeCandidateSet) pEntity);
		} else if (pEntity instanceof ReactomeGenomeEncodedEntity) {
			return getGlobalComparator().compareNodes(species, (ReactomeGenomeEncodedEntity) pEntity);
		} else if (pEntity instanceof ReactomeDefinedSet) {
			return getGlobalComparator().compareNodes(species, (ReactomeDefinedSet) pEntity);
		} else {
			throw new InvalidArgumentException("Cannot determine the way of comparison for provided class types: " + species.getClass() + ", " + pEntity.getClass());
		}
	}

}
