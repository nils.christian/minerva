package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;

/**
 * This class allows to compare {@link SimpleMolecule} element (internal
 * representation) and {@link ReactomeEntityWithAccessionedSequence} (reactome
 * model).
 * 
 * @author Piotr Gawron
 * 
 */
public class MoleculeAndEntityWithAccessionedSequenceComparator extends ANodeComparator<SimpleMolecule, ReactomeEntityWithAccessionedSequence> {

	/**
	 * Default constructor.
	 */
	public MoleculeAndEntityWithAccessionedSequenceComparator() {
		super(SimpleMolecule.class, ReactomeEntityWithAccessionedSequence.class);
	}

	@Override
	public boolean compareNodes(SimpleMolecule species, ReactomeEntityWithAccessionedSequence simpleEntity) {
		return false;
	}

}
