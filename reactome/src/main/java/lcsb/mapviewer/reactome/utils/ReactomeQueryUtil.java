package lcsb.mapviewer.reactome.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseIdentifier;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntitySet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomeEvent;
import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;
import lcsb.mapviewer.reactome.model.ReactomeLiteratureReference;
import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomePolymer;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.model.ReactomeReferenceDNASequence;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.PredictionResult.PredictionStatus;

/**
 * Util class used for accessing objects in reactome database. They are filtered
 * based on different set of parameters.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeQueryUtil {
	/**
	 * Default class logger.
	 */
	private static Logger			logger = Logger.getLogger(ReactomeQueryUtil.class);

	/**
	 * Interface used for accessing data in the reactome database.
	 */
	@Autowired
	private DataSourceUpdater	rc;

	/**
	 * Util object used for processing elements.
	 */
	@Autowired
	private ElementUtil				elementUtil;

	/**
	 * Service accessing chebi database.
	 */
	@Autowired
	private ChebiAnnotator		chebiBackend;

	/**
	 * Provides human readable id of element.
	 * 
	 * @param element
	 *          element for which we want id string.
	 * @return human readable id of element
	 */
	public String getElementTag(BioEntity element) {
		return new ElementUtils().getElementTag(element);
	}

	/**
	 * Returns list of reactome reactions that contais all species given in the
	 * parameter.
	 * 
	 * @param speciesList
	 *          list of species
	 * @return list of reactome reactions that contais all species given in the
	 *         parameter
	 */
	public List<ReactomeReactionlikeEvent> getReactionsBetweenSpecies(List<Element> speciesList) {
		return getReactionsBetweenSpecies(speciesList, false);
	}

	/**
	 * Returns list of reactome reactions that contais all species given in the
	 * parameter.
	 * 
	 * @param speciesList
	 *          list of species
	 * @param exact
	 *          should the list of reaction participants be axactly the same as in
	 *          the speciesList parameter
	 * @return list of reactome reactions that contais all species given in the
	 *         parameter
	 */
	public List<ReactomeReactionlikeEvent> getReactionsBetweenSpecies(List<Element> speciesList, boolean exact) {
		List<ReactomeReactionlikeEvent> result = getReactionsBetweenSpecies(speciesList, exact, false);
		if (result.size() == 0) {
			result = getReactionsBetweenSpecies(speciesList, exact, true);
		}
		return result;
	}

	/**
	 * Returns list of reactome reactions that contais all species given in the
	 * parameter.
	 * 
	 * @param speciesList
	 *          list of species
	 * @param deepSearch
	 *          should the complexes/sets containing species be also considered
	 * @param exact
	 *          should the list of reaction participants be axactly the same as in
	 *          the speciesList parameter
	 * @return list of reactome reactions that contais all species given in the
	 *         parameter
	 */
	public List<ReactomeReactionlikeEvent> getReactionsBetweenSpecies(List<Element> speciesList, boolean exact, boolean deepSearch) {
		try {

			List<ReactomeReactionlikeEvent> result = new ArrayList<ReactomeReactionlikeEvent>();
			if (speciesList.size() <= 0) {
				return result;
			}

			List<List<ReactomePhysicalEntity>> entities = new ArrayList<List<ReactomePhysicalEntity>>();
			int shortestIndex = -1;
			boolean notMolecule = true;
			for (Element species : speciesList) {
				if (species instanceof Chemical) {
					Set<MiriamData> chebiIds = new HashSet<MiriamData>();
					chebiIds.add(chebiBackend.getChebiForChebiName(species.getName()));
					List<ReactomePhysicalEntity> entityList = rc.getEntitiesForChebiId(chebiIds, deepSearch);
					entities.add(entityList);
					// logger.debug(species.getName() + " - " + entityList.size());
					if (notMolecule) {
						if (shortestIndex < 0) {
							shortestIndex = entities.size() - 1;
						} else if (entityList.size() < entities.get(shortestIndex).size()) {
							shortestIndex = entities.size() - 1;
						}
					}
				} else if (species instanceof Protein) {
					Set<MiriamData> uniprotId = elementUtil.getMiriamByType(species, MiriamType.UNIPROT, true);
					List<ReactomePhysicalEntity> entityList;
					if (uniprotId.size() > 0) {
						entityList = rc.getEntitiesForUniprotId(uniprotId);
					} else {
						entityList = rc.getEntitiesForName(species.getName());
					}

					entities.add(entityList);
					// logger.debug(species.getName() + " - " + entityList.size());
					if (shortestIndex < 0) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					} else if (entityList.size() < entities.get(shortestIndex).size()) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					} else if (notMolecule) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					}
				} else if (species instanceof Complex) {
					Set<MiriamData> identifiers = getIdsForComplex((Complex) species);
					if (identifiers == null) {
						return result;
					}
					List<ReactomePhysicalEntity> entityList = rc.getEntitiesForSetOfIds(identifiers);
					entities.add(entityList);
					// logger.debug(species.getName() + " - " + entityList.size());
					if (shortestIndex < 0) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					} else if (entityList.size() < entities.get(shortestIndex).size()) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					} else if (notMolecule) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					}
				} else if (species instanceof Unknown) {
					// try to find by name
					List<ReactomePhysicalEntity> entityList = rc.getEntitiesForName(species.getName());
					entities.add(entityList);
					// logger.debug(species.getName() + " - " + entityList.size());
					if (shortestIndex < 0) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					} else if (entityList.size() < entities.get(shortestIndex).size()) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					} else if (notMolecule) {
						shortestIndex = entities.size() - 1;
						notMolecule = false;
					}
				} else if (species instanceof Phenotype) {
					// we cannot do anything with phenotype
					return result;
				} else {
					logger.warn("Unhandled class type: " + species.getClass());
					return result;
				}
			}

			List<ReactomePhysicalEntity> shortestList = entities.get(shortestIndex);
			for (ReactomePhysicalEntity entity : shortestList) {
				List<ReactomeReactionlikeEvent> reactions = rc.getReactionsForEntityId(entity.getDbId());
				for (ReactomeReactionlikeEvent reactomeReaction : reactions) {
					boolean correctReaction = checkReaction(exact, entities, reactomeReaction);
					if (correctReaction) {
						result.add(reactomeReaction);
					}
				}

			}

			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Checks if reactome reaction matches the set of entities given in the
	 * parameter.
	 * 
	 * @param exact
	 *          should the match be exact:
	 *          entities.size()==reaction.participants.size(). <br/>
	 *          Or maybe<br/>
	 *          entities.size()<=reaction.participants.size()
	 * @param entities
	 *          list of entities to check
	 * @param reactomeReaction
	 *          reaction to check
	 * @return <code>true</code> if reactome reaction matches the set of entities
	 *         given in the parameter, <code>false</code> otherwise
	 */
	private boolean checkReaction(boolean exact, List<List<ReactomePhysicalEntity>> entities, ReactomeReactionlikeEvent reactomeReaction) {
		boolean correctReaction = true;
		Set<Integer> matcheIds = new HashSet<Integer>();
		for (List<ReactomePhysicalEntity> list : entities) {
			boolean foundMatch = false;
			for (ReactomePhysicalEntity reactomePhysicalEntity : list) {
				for (ReactomeDatabaseObject object : reactomeReaction.getInputs()) {
					if (reactomePhysicalEntity.getDbId() == object.getDbId()) {
						if (exact) {
							matcheIds.add(object.getDbId());
						}
						foundMatch = true;
					}
				}
				for (ReactomeDatabaseObject object : reactomeReaction.getOutputs()) {
					if (reactomePhysicalEntity.getDbId() == object.getDbId()) {
						if (exact) {
							matcheIds.add(object.getDbId());
						}
						foundMatch = true;
					}
				}
				for (ReactomeCatalystActivity catalystActivity : reactomeReaction.getCatalystActivities()) {
					if (catalystActivity.getPhysicalEntity() != null) {
						if (reactomePhysicalEntity.getDbId() == catalystActivity.getPhysicalEntity().getDbId()) {
							if (exact) {
								matcheIds.add(catalystActivity.getPhysicalEntity().getDbId());
							}
							foundMatch = true;
						}
					}
				}
			}
			if (!foundMatch) {
				correctReaction = false;
			}
		}
		if (exact) {
			boolean ok = true;
			for (ReactomeDatabaseObject object : reactomeReaction.getInputs()) {
				if (!matcheIds.contains(object.getDbId())) {
					ok = false;
				}
			}
			for (ReactomeDatabaseObject object : reactomeReaction.getOutputs()) {
				if (!matcheIds.contains(object.getDbId())) {
					ok = false;
				}
			}
			for (ReactomeCatalystActivity catalystActivity : reactomeReaction.getCatalystActivities()) {
				if (catalystActivity.getPhysicalEntity() != null) {
					if (!matcheIds.contains(catalystActivity.getPhysicalEntity().getDbId())) {
						ok = false;
					}
				}
			}
			if (!ok) {
				correctReaction = false;
			}
		}
		return correctReaction;
	}

	/**
	 * Returns reactome stable identifier (see also
	 * {@link lcsb.mapviewer.reactome.model.ReactomeStableIdentifier}) for the
	 * given species.
	 * 
	 * @param species
	 *          species to be checked
	 * @return reactome stable identifier
	 */
	public String getReactomeIdentifierForSpecies(Element species) {
		String stableIdentifier = null;
		for (MiriamData md : species.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.REACTOME)) {
				stableIdentifier = md.getResource();
			}
		}
		return stableIdentifier;
	}

	/**
	 * Returns reactome stable identifier (see also
	 * {@link lcsb.mapviewer.reactome.model.ReactomeStableIdentifier}) for the
	 * given reaction.
	 * 
	 * @param reaction
	 *          reaction to check
	 * @return reactome stable identifier
	 */
	public String getReactomeIdentifierForReaction(Reaction reaction) {
		String stableIdentifier = null;
		for (MiriamData md : reaction.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.REACTOME)) {
				stableIdentifier = md.getResource();
			}
		}
		return stableIdentifier;
	}

	/**
	 * Returns list of reactome stable identifiers (see also
	 * {@link lcsb.mapviewer.reactome.model.ReactomeStableIdentifier}) for the
	 * given reaction.
	 * 
	 * @param reaction
	 *          reaction to check
	 * @return list of reactome stable identifiers
	 */
	public List<String> getReactomeIdentifiersForReaction(Reaction reaction) {
		List<String> stableIdentifiers = new ArrayList<String>();
		for (MiriamData md : reaction.getMiriamData()) {
			if (md.getDataType().equals(MiriamType.REACTOME)) {
				stableIdentifiers.add(md.getResource());
			}
		}
		return stableIdentifiers;
	}

	/**
	 * Return list of identifiers for reactome Simple Entity.
	 * 
	 * @param simpleEntity
	 *          object for which identifiers are looked for
	 * @return list of known identifiers
	 */
	protected Set<MiriamData> getIdentifiersForSimpleEntity(ReactomeSimpleEntity simpleEntity) {
		Set<MiriamData> result = new HashSet<MiriamData>();
		for (ReactomeReferenceEntity reference : simpleEntity.getReferenceEntities()) {
			if (reference instanceof ReactomeReferenceMolecule) {
				boolean isChebi = false;
				if (reference.getReferenceDatabase() != null) {
					for (String name : reference.getReferenceDatabase().getNames()) {
						if (name.equalsIgnoreCase("ChEBI")) {
							isChebi = true;
						}
					}
				}
				if (isChebi) {
					String chebi = "CHEBI:" + reference.getIdentifier();
					result.add(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, chebi));
				} else {
					throw new InvalidArgumentException("Unknown database type: " + reference.getReferenceDatabase().getDisplayName());
				}
			} else {
				throw new InvalidArgumentException("Unknown reference type: " + reference.getClass());
			}
		}
		return result;
	}

	/**
	 * Return set of identifiers for reactome object.
	 * 
	 * @param entity
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 */
	public Set<MiriamData> getIdentifiersForReactomeEntity(ReactomePhysicalEntity entity) {
		if (entity instanceof ReactomeSimpleEntity) {
			return getIdentifiersForSimpleEntity((ReactomeSimpleEntity) entity);
		} else if (entity instanceof ReactomeComplex) {
			return getIdentifiersForComplex((ReactomeComplex) entity);
		} else if (entity instanceof ReactomeEntityWithAccessionedSequence) {
			return getIdentifiersForEntityWithAccessionedSequence((ReactomeEntityWithAccessionedSequence) entity);
		} else if (entity instanceof ReactomeDefinedSet) {
			return getIdentifiersForEntitySet((ReactomeEntitySet) entity);
		} else if (entity instanceof ReactomeOtherEntity) {
			return getIdentifiersForOtherEntity((ReactomeOtherEntity) entity);
		} else if (entity instanceof ReactomeGenomeEncodedEntity) {
			// there are not identifiers
			return new HashSet<MiriamData>();
		} else if (entity instanceof ReactomePolymer) {
			logger.warn("What kind of id for polymer: " + entity.getDisplayName());
			return new HashSet<MiriamData>();
		} else if (entity instanceof ReactomeCandidateSet) {
			return getIdentifiersForEntitySet((ReactomeEntitySet) entity);
		} else {
			throw new InvalidArgumentException("Don't know how to process class: " + entity.getClass());
		}
	}

	/**
	 * Return set of identifiers for reactome object.
	 * 
	 * @param entity
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 */
	private Set<MiriamData> getIdentifiersForOtherEntity(ReactomeOtherEntity entity) {
		Set<MiriamData> result = new HashSet<MiriamData>();
		for (ReactomeDatabaseIdentifier reference : entity.getCrossReferences()) {
			boolean isChebi = false;
			if (reference.getReferenceDatabase() != null) {
				for (String name : reference.getReferenceDatabase().getNames()) {
					if (name.equalsIgnoreCase("ChEBI")) {
						isChebi = true;
					}
				}
			}
			if (isChebi) {
				String chebi = "CHEBI:" + reference.getIdentifier();
				result.add(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, chebi));
			} else {
				throw new InvalidArgumentException("Unknown database type: " + reference.getReferenceDatabase().getDisplayName());
			}
		}
		return result;
	}

	/**
	 * Return set of identifiers for reactome object.
	 * 
	 * @param set
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 */
	private Set<MiriamData> getIdentifiersForEntitySet(ReactomeEntitySet set) {
		Set<MiriamData> result = new HashSet<MiriamData>();
		for (ReactomePhysicalEntity entity : set.getHasMembers()) {
			result.addAll(getIdentifiersForReactomeEntity(entity));
		}
		return result;
	}

	/**
	 * Return set of identifiers for reactome object.
	 * 
	 * @param complex
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 */
	protected Set<MiriamData> getIdentifiersForComplex(ReactomeComplex complex) {
		Set<MiriamData> result = new HashSet<MiriamData>();
		for (ReactomePhysicalEntity entity : complex.getHasComponents()) {
			result.addAll(getIdentifiersForReactomeEntity(entity));
		}
		return result;

	}

	/**
	 * Return set of identifiers for reactome object.
	 * 
	 * @param entity
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 */
	private Set<MiriamData> getIdentifiersForEntityWithAccessionedSequence(ReactomeEntityWithAccessionedSequence entity) {
		ReactomeReferenceEntity reference = ((ReactomeEntityWithAccessionedSequence) entity).getReferenceEntity();
		if (reference instanceof ReactomeReferenceGeneProduct) {
			Set<MiriamData> result = new HashSet<MiriamData>();
			// result.add(reference.getIdentifier());
			// return result;
			if (reference.getReferenceDatabase().getDisplayName().equals("UniProt")) {
				result.add(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.UNIPROT, reference.getIdentifier()));
				return result;
			} else {
				throw new NotImplementedException("Dont know what to do: " + reference.getIdentifier() + ";" + reference.getReferenceDatabase().getDisplayName());
			}
		} else if (reference instanceof ReactomeReferenceDNASequence) {
			// Set<MiriamData> result = new HashSet<MiriamData>();
			// result.add(reference.getIdentifier());
			// return result;
			throw new NotImplementedException("Dont know what to do: " + reference.getIdentifier() + ";" + reference.getReferenceDatabase().getDisplayName());
		} else {
			throw new InvalidArgumentException("Unknown class type (" + reference.getClass() + "). Don't know what to do");
		}
	}

	/**
	 * Return set of identifiers for complex object.
	 * 
	 * @param complex
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 * 
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	public Set<MiriamData> getIdsForComplex(Complex complex) throws AnnotatorException {
		Set<MiriamData> result = new HashSet<>();
		for (Species species : complex.getAllSimpleChildren()) {
			result.addAll(getIdsForSpecies(species));
		}
		return result;
	}

	/**
	 * Return set of identifiers for species object.
	 * 
	 * @param species
	 *          object for which identifiers are looked for
	 * @return set of known identifiers
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	public Set<MiriamData> getIdsForSpecies(Element species) throws AnnotatorException {
		Set<MiriamData> result = new HashSet<>();
		if (species instanceof Protein) {
			result.addAll(elementUtil.getMiriamByType(species, MiriamType.UNIPROT, true));
		} else if (species instanceof Phenotype) {
			return Collections.emptySet();
		} else if (species instanceof Chemical) {
			result.addAll(elementUtil.getMiriamByType(species, MiriamType.CHEBI, true));
		} else {
			logger.warn("Don't know how to handle " + species.getClass() + " in a complex comparison.");
			return result;
		}
		return result;
	}

	/**
	 * Returns url for the reactome stable identifier.
	 * 
	 * @param reactomId
	 *          reactome stabel identifier
	 * @return url for the reactome stable identifier
	 */
	public String getReactomeUrlForStableIdentifier(String reactomId) {
		return "http://minerva.uni.lu/MapViewer/miriam.xhtml?type=urn:miriam:reactome&resource=" + reactomId;
	}

	/**
	 * Returns list of ReactomeLiteratureReference for pubmed identifier.
	 * 
	 * @param id
	 *          pubmed identifier
	 * @return list of ReactomeLiteratureReference for pubmed identifier
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	public List<ReactomeDatabaseObject> getLiteratureReferencesByPubMedId(Integer id) throws IOException {
		String query = "pubMedIdentifier=" + id;
		return rc.getSimpleObjectListByQuery(query, ReactomeLiteratureReference.class);
	}

	/**
	 * Returns one, random ReactomeLiteratureReference for pubmed identifier.
	 * 
	 * @param id
	 *          pubmed identifier
	 * @return ReactomeLiteratureReference for pubmed identifier
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	public ReactomeDatabaseObject getLiteratureReferenceByPubMedId(Integer id) throws IOException {
		List<ReactomeDatabaseObject> objects = getLiteratureReferencesByPubMedId(id);
		if (objects.size() > 0) {
			return objects.get(0);
		}
		return null;
	}

	/**
	 * Returns reactome reaction that is similar to the reaction given in the
	 * parametr.
	 * 
	 * @param reaction
	 *          reaction for which we are looking for similar one
	 * @return reactome reaction that is similar to the reaction given in the
	 *         parametr.
	 */
	public ReactomeReactionlikeEvent getSimilarReaction(Reaction reaction) {
		List<Element> list = new ArrayList<>();
		for (ReactionNode node : reaction.getReactionNodes()) {
			list.add(node.getElement());
		}
		List<ReactomeReactionlikeEvent> reactions = getReactionsBetweenSpecies(list, true);
		// logger.debug("Reactions: " + reactions.size());
		if (reactions.size() > 0) {

			int minSize = reactions.get(0).getInputs().size() + reactions.get(0).getOutputs().size();
			ReactomeReactionlikeEvent result = reactions.get(0);
			for (ReactomeReactionlikeEvent newReaction : reactions) {
				int newSize = newReaction.getInputs().size() + newReaction.getOutputs().size();
				if (newSize < minSize) {
					minSize = newSize;
					result = newReaction;
				}
			}
			return result;
		} else {
			return null;
		}
	}

	/**
	 * Returns reactions that neighbour the reaction in the parameter.
	 * 
	 * @param originalReaction
	 *          original reactome reaction
	 * @param deep
	 *          should we perform deep search (TODO to be checked)
	 * @return reactions that neighbour the reaction in the parameter
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	public Set<ReactomeReactionlikeEvent> getNeighboruingReactionsForKnownReaction(ReactomeReactionlikeEvent originalReaction, boolean deep) throws IOException {
		Set<ReactomeReactionlikeEvent> result = new HashSet<ReactomeReactionlikeEvent>();
		for (ReactomePhysicalEntity input : originalReaction.getInputs()) {
			if (!(input instanceof ReactomeSimpleEntity) || deep) {
				result.addAll(rc.getReactionsForEntityId(input.getDbId()));
			}
		}

		for (ReactomePhysicalEntity output : originalReaction.getOutputs()) {
			if (!(output instanceof ReactomeSimpleEntity) || deep) {
				result.addAll(rc.getReactionsForEntityId(output.getDbId()));
			}
		}
		for (ReactomeCatalystActivity catalystActivity : originalReaction.getCatalystActivities()) {
			if (catalystActivity.getPhysicalEntity() != null) {
				if (!(catalystActivity.getPhysicalEntity() instanceof ReactomeSimpleEntity) || deep) {
					result.addAll(rc.getReactionsForEntityId(catalystActivity.getPhysicalEntity().getDbId()));
				}
			}
		}
		result.remove(originalReaction);
		return result;
	}

	/**
	 * REturns reactions that connect reactions given in the parameter.
	 * 
	 * @param knownReactions
	 *          list of known reactions
	 * @param threshold
	 *          how many reactions should touch the result reactions
	 * @return reactions that connect reactions given in the parameter
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	public Set<PredictionResult> getExtendedReactionsForKnownReactions(Collection<ReactomeReactionlikeEvent> knownReactions, double threshold)
			throws IOException {

		Map<ReactomeReactionlikeEvent, Integer> reactionCounter = new HashMap<ReactomeReactionlikeEvent, Integer>();
		for (ReactomeReactionlikeEvent reactomeReactionlikeEvent : knownReactions) {
			Set<ReactomeReactionlikeEvent> newReactions = getNeighboruingReactionsForKnownReaction(reactomeReactionlikeEvent, false);
			for (ReactomeReactionlikeEvent reaction : newReactions) {
				if (knownReactions.contains(reaction)) {
					continue;
				}
				if (reactionCounter.get(reaction) == null) {
					reactionCounter.put(reaction, 1);
				} else {
					reactionCounter.put(reaction, reactionCounter.get(reaction) + 1);
				}
			}
		}
		Set<PredictionResult> result = new HashSet<PredictionResult>();
		for (Entry<ReactomeReactionlikeEvent, Integer> entry : reactionCounter.entrySet()) {
			Set<ReactomeReactionlikeEvent> neighbourgs = getNeighboruingReactionsForKnownReaction(entry.getKey(), false);
			int known = entry.getValue();
			int total = neighbourgs.size();
			if (known >= threshold) {
				PredictionResult prediction = new PredictionResult();
				prediction.setReaction(entry.getKey());
				prediction.setScore(((double) known) / ((double) total));
				prediction.setStatus(PredictionStatus.STRUCTURE);
				result.add(prediction);
			}
		}

		return result;
	}

	/**
	 * Returns list of reaction that are annotated by pubmed publications, but
	 * aren't on the tabu list.
	 * 
	 * @param pubmedIds
	 *          list of pubmed identifiers
	 * @param reactions
	 *          reactions that should be excluded from the result
	 * @return list of reaction that are annotated by pubmed publications, but
	 *         aren't on the tabu list.
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	public List<PredictionResult> getExtendedReactionsForPubmedPublicationsWithTabuReaction(List<String> pubmedIds, Set<ReactomeReactionlikeEvent> reactions)
			throws IOException {
		Set<ReactomeReactionlikeEvent> newReactions = new HashSet<ReactomeReactionlikeEvent>();
		for (String pubmedId : pubmedIds) {
			Set<ReactomeReactionlikeEvent> tmp = getReactionsForPubmedId(Integer.parseInt(pubmedId));
			for (ReactomeReactionlikeEvent reactomeReactionlikeEvent : tmp) {
				if (!reactions.contains(reactomeReactionlikeEvent)) {
					newReactions.add(reactomeReactionlikeEvent);
				}
			}
		}
		List<PredictionResult> result = new ArrayList<PredictionResult>();
		for (ReactomeReactionlikeEvent reactomeReactionlikeEvent : newReactions) {
			PredictionResult pResult = new PredictionResult();
			pResult.setReaction(reactomeReactionlikeEvent);
			pResult.setStatus(PredictionStatus.DATA_MINIG);
			result.add(pResult);
		}

		return result;
	}

	/**
	 * Returns reactions that are annotated with pubmed identifier.
	 * 
	 * @param pubmedId
	 *          pubmed identifier
	 * @return reactions that are annotated with pubmed identifier
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	Set<ReactomeReactionlikeEvent> getReactionsForPubmedId(int pubmedId) throws IOException {
		Set<ReactomeReactionlikeEvent> reactions = new HashSet<ReactomeReactionlikeEvent>();
		List<ReactomeDatabaseObject> references = getLiteratureReferencesByPubMedId(pubmedId);
		for (ReactomeDatabaseObject reference : references) {
			List<ReactomeDatabaseObject> objects = getEventsForLiteratureReference(reference);
			for (ReactomeDatabaseObject object : objects) {
				if (object instanceof ReactomeReactionlikeEvent) {
					reactions.add((ReactomeReactionlikeEvent) object);
				}
			}
		}
		return reactions;
	}

	/**
	 * Returns reactions that are referenced by the parameter.
	 * 
	 * @param reference
	 *          result reactions should be referenced by this element
	 * @return reactions that are referenced by the parameter
	 * @throws IOException
	 *           thrown then there is a problem with accessing reactome db
	 */
	private List<ReactomeDatabaseObject> getEventsForLiteratureReference(ReactomeDatabaseObject reference) throws IOException {
		List<ReactomeDatabaseObject> result = new ArrayList<ReactomeDatabaseObject>();
		String query = "literatureReference=" + reference.getDbId();
		for (ReactomeDatabaseObject obj : rc.getSimpleObjectListByQuery(query, ReactomeEvent.class)) {
			result.add(rc.getFullObjectForDbId(obj.getDbId()));
		}
		return result;
	}

	/**
	 * @return the rc
	 * @see #rc
	 */
	public DataSourceUpdater getRc() {
		return rc;
	}

	/**
	 * @param rc
	 *          the rc to set
	 * @see #rc
	 */
	public void setRc(DataSourceUpdater rc) {
		this.rc = rc;
	}

	/**
	 * @return the chebiBackend
	 * @see #chebiBackend
	 */
	public ChebiAnnotator getChebiBackend() {
		return chebiBackend;
	}

	/**
	 * @param chebiBackend
	 *          the chebiBackend to set
	 * @see #chebiBackend
	 */
	public void setChebiBackend(ChebiAnnotator chebiBackend) {
		this.chebiBackend = chebiBackend;
	}

}
