package lcsb.mapviewer.reactome.utils;

import java.util.Comparator;

import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;

/**
 * Describes reactome reaction that possibly should be included in our model.
 * 
 * @author Piotr Gawron
 * 
 */
public class PredictionResult {
	/**
	 * Type of the analysis from which the result comes from.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	public enum PredictionStatus {
		/**
		 * Structure analysis.
		 */
		STRUCTURE,
		/**
		 * Data mining.
		 */
		DATA_MINIG
	}

	/**
	 * Comparator that compares two results based on the quality score.
	 */
	public static final Comparator<PredictionResult>	SCORE_COMPARATOR	= new Comparator<PredictionResult>() {

																																				@Override
																																				public int compare(PredictionResult arg0, PredictionResult arg1) {
																																					Double a = arg0.getScore();
																																					Double b = arg1.getScore();
																																					return b.compareTo(a);
																																				}
																																			};

	/**
	 * Reactome reaction to which this result refer to.
	 */
	private ReactomeReactionlikeEvent									reaction;
	/**
	 * Quality score of the prediction.
	 */
	private double																		score;
	/**
	 * Type of analysis from which prediction comes from.
	 */
	private PredictionStatus													status;

	/**
	 * @return the reaction
	 * @see #reaction
	 */
	public ReactomeReactionlikeEvent getReaction() {
		return reaction;
	}

	/**
	 * @param reaction
	 *          the reaction to set
	 * @see #reaction
	 */
	public void setReaction(ReactomeReactionlikeEvent reaction) {
		this.reaction = reaction;
	}

	/**
	 * @return the score
	 * @see #score
	 */
	public double getScore() {
		return score;
	}

	/**
	 * @param score
	 *          the score to set
	 * @see #score
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * @return the status
	 * @see #status
	 */
	public PredictionStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *          the status to set
	 * @see #status
	 */
	public void setStatus(PredictionStatus status) {
		this.status = status;
	}

}
