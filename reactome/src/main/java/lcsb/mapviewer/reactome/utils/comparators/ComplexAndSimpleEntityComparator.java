package lcsb.mapviewer.reactome.utils.comparators;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link Complex} element (internal
 * representation) and {@link ReactomeSimpleEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexAndSimpleEntityComparator extends ANodeComparator<Complex, ReactomeSimpleEntity> {

  /**
   * Default constructor.
   */
  public ComplexAndSimpleEntityComparator() {
    super(Complex.class, ReactomeSimpleEntity.class);
  }

  @Override
  public boolean compareNodes(Complex complex, ReactomeSimpleEntity simpleEntity) throws ComparatorException {
    Set<MiriamData> complex1Ids;
    try {
      complex1Ids = getRcu().getIdsForComplex(complex);
    } catch (Exception e) {
      throw new ComparatorException(e);
    }

    Set<MiriamData> entityIds2 = new HashSet<MiriamData>();

    for (ReactomeReferenceEntity reference : simpleEntity.getReferenceEntities()) {
      if (reference instanceof ReactomeReferenceMolecule) {
        boolean isChebi = false;
        if (reference.getReferenceDatabase() != null) {
          for (String name : reference.getReferenceDatabase().getNames()) {
            if (name.equalsIgnoreCase("ChEBI")) {
              isChebi = true;
            }
          }
        }
        if (isChebi) {
          MiriamData chebi = new MiriamData(MiriamType.CHEBI, "CHEBI:" + reference.getIdentifier());
          if (complex1Ids.contains(chebi)) {
            entityIds2.add(chebi);
          }
        }
      }
    }
    return compareSets(complex1Ids, entityIds2);
  }

}
