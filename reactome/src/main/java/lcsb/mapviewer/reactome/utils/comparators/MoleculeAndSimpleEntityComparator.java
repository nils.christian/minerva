package lcsb.mapviewer.reactome.utils.comparators;

import java.util.List;
import java.util.Set;

import lcsb.mapviewer.annotation.services.annotators.ChebiSearchException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link SimpleMolecule} element (internal
 * representation) and {@link ReactomeSimpleEntity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class MoleculeAndSimpleEntityComparator extends ANodeComparator<SimpleMolecule, ReactomeSimpleEntity> {

	/**
	 * Default constructor.
	 */
	public MoleculeAndSimpleEntityComparator() {
		super(SimpleMolecule.class, ReactomeSimpleEntity.class);
	}

	@Override
	public boolean compareNodes(SimpleMolecule species, ReactomeSimpleEntity simpleEntity) throws ComparatorException {
		try {
			List<MiriamData> chebi1 = getChebiBackend().getOntologyChebiIdsForChebiName(species.getName());
			if (chebi1.size() == 0) { // if we don't have id then return false (even
																// with two null values we cannot claim that
																// they
																// are equal)
				return false;
			}
			Set<MiriamData> chebiIds2 = getRcu().getIdentifiersForReactomeEntity(simpleEntity);
			// no match was found, so lets assume that they are different
			for (MiriamData string : chebi1) {
				if (chebiIds2.contains(string)) {
					return true;
				}
			}
			return false;
		} catch (ChebiSearchException e) {
			throw new ComparatorException(e);
		}
	}

}
