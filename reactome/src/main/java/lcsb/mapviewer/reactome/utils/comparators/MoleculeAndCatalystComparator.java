package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.utils.ComparatorException;

/**
 * This class allows to compare {@link SimpleMolecule} element (internal
 * representation) and {@link ReactomeCatalystActivity} (reactome model).
 * 
 * @author Piotr Gawron
 * 
 */
public class MoleculeAndCatalystComparator extends ANodeComparator<SimpleMolecule, ReactomeCatalystActivity> {

	/**
	 * Default constructor.
	 */
	public MoleculeAndCatalystComparator() {
		super(SimpleMolecule.class, ReactomeCatalystActivity.class);
	}

	@Override
	public boolean compareNodes(SimpleMolecule species, ReactomeCatalystActivity cSpecies) throws ComparatorException {
		ReactomePhysicalEntity pEntity = cSpecies.getPhysicalEntity();
		if (pEntity instanceof ReactomeComplex) {
			return getGlobalComparator().compareNodes(species, (ReactomeComplex) pEntity);
		} else if (pEntity instanceof ReactomeEntityWithAccessionedSequence) {
			return getGlobalComparator().compareNodes(species, (ReactomeEntityWithAccessionedSequence) pEntity);
		} else if (pEntity instanceof ReactomeSimpleEntity) {
			return getGlobalComparator().compareNodes(species, (ReactomeSimpleEntity) pEntity);
		} else if (pEntity instanceof ReactomeDefinedSet) {
			return getGlobalComparator().compareNodes(species, (ReactomeDefinedSet) pEntity);
		} else {
			throw new InvalidArgumentException("Cannot determine the way of comparison for provided class types: " + species.getClass() + ", " + pEntity.getClass());
		}
	}

}
