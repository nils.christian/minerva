package lcsb.mapviewer.reactome.utils;

/**
 * Exception thrown when there is a problem with comparison of elements between
 * our model and reactome.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComparatorException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Default constructor.
	 * 
	 * @param e
	 *          exception caught and forwarded to this exception
	 */
	public ComparatorException(Exception e) {
		super(e);
	}

}
