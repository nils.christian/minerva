package lcsb.mapviewer.reactome.utils.comparators;

import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;

/**
 * This class allows to compare {@link Ion} element (internal
 * representation) and {@link ReactomeEntityWithAccessionedSequence} (reactome
 * model).
 * 
 * @author Piotr Gawron
 * 
 */
public class IonAndEntityWithAccessionedSequenceComparator extends ANodeComparator<Ion, ReactomeEntityWithAccessionedSequence> {

	/**
	 * Default constructor.
	 */
	public IonAndEntityWithAccessionedSequenceComparator() {
		super(Ion.class, ReactomeEntityWithAccessionedSequence.class);
	}

	@Override
	public boolean compareNodes(Ion species, ReactomeEntityWithAccessionedSequence simpleEntity) {
		return false;
	}

}
