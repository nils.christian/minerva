package lcsb.mapviewer.reactome;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeStatus;
import lcsb.mapviewer.reactome.utils.DataSourceUpdater;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Proxy class used for creating objects that will be automatically updated from
 * reactome when needed.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDatabaseProxy {

	/**
	 * Default class logger.
	 */
	private static Logger			logger	= Logger.getLogger(ReactomeDatabaseProxy.class);

	/**
	 * Interface used for accessing data in the reactome database.
	 */
	@Autowired
	private DataSourceUpdater	dsu;

	/**
	 * This is proxy method that is called whenever getter is called. The main
	 * purpose is to catch all request for fields in reactome objects that should
	 * be filled from reactome database but weren't fetch yet and get these data
	 * from reactome. This allows to transparently access data from reactome
	 * without need for updating manualy the fields.
	 */
	private MethodHandler			handler	= new MethodHandler() {
																			@SuppressWarnings({ "rawtypes", "unchecked" })
																			@Override
																			public Object invoke(Object object, Method thisMethod, Method method, Object[] args) throws Throwable {
																				if (Modifier.isPublic(thisMethod.getModifiers()) && thisMethod.getParameterTypes().length == 0
																						&& (thisMethod.getName().startsWith("get") || thisMethod.getName().startsWith("is"))) {
																					if (ReactomeDatabaseObject.class.isAssignableFrom(method.getReturnType())) {
																						try {
																							ReactomeDatabaseObject value = (ReactomeDatabaseObject) method.invoke(object);
																							if (value != null) {
																								if (value.getStatus().equals(ReactomeStatus.ONLY_ID)) {

																									ReactomeDatabaseObject newValue = dsu.getFullObjectForDbId(value.getDbId());
																									String setterName = thisMethod.getName();
																									if (setterName.startsWith("get")) {
																										setterName = setterName.replaceFirst("get", "set");
																									} else {
																										setterName = setterName.replaceFirst("is", "set");
																									}
																									try {
																										Method setterMethod = object.getClass().getMethod(setterName, method.getReturnType());
																										setterMethod.invoke(object, newValue);
																									} catch (NoSuchMethodException e) {
																										e.printStackTrace();
																									} catch (IllegalArgumentException e) {
																										logger.error("Cannot assign " + newValue.getClass() + " (id: " + value.getDbId() + ") to field: "
																												+ setterName.replace("set", "") + " (" + method.getReturnType() + "). Class: " + object.getClass());
																									} catch (SecurityException e) {
																										e.printStackTrace();
																										// ignore it - however this
																										// would be
																										// strange
																									}
																								}
																							}
																						} catch (IllegalAccessException e) {
																							e.printStackTrace();
																						} catch (InvocationTargetException e) {
																							e.printStackTrace();
																							// ignore it
																						}

																					} else if (List.class.isAssignableFrom(method.getReturnType())) {
																						try {
																							List list = (List) method.invoke(object);
																							List<Integer> ids = new ArrayList<Integer>();
																							for (int i = 0; i < list.size(); i++) {
																								Object obj = list.get(i);
																								if (obj instanceof ReactomeDatabaseObject) {
																									if (((ReactomeDatabaseObject) obj).getStatus().equals(ReactomeStatus.ONLY_ID)) {
																										ids.add(((ReactomeDatabaseObject) obj).getDbId());

																									}
																								}
																							}
																							if (ids.size() > 0) {
																								Map<Integer, ReactomeDatabaseObject> objectsMap = dsu.getFullObjectsForDbIds(ids);

																								for (int i = 0; i < list.size(); i++) {
																									Object obj = list.get(i);
																									if (obj instanceof ReactomeDatabaseObject) {
																										if (((ReactomeDatabaseObject) obj).getStatus().equals(ReactomeStatus.ONLY_ID)) {
																											ReactomeDatabaseObject newValue = objectsMap.get(((ReactomeDatabaseObject) obj).getDbId());
																											if (newValue != null) {
																												list.set(i, newValue);
																											}
																										}
																									}
																								}
																							}
																						} catch (IllegalAccessException e) {
																							e.printStackTrace();
																						} catch (InvocationTargetException e) {
																							e.printStackTrace();
																							// ignore it
																						}
																					}
																				}
																				return method.invoke(object, args);
																			}
																		};

	/**
	 * Creates enoty proxy object for class given in the parameter.
	 * 
	 * @param clazz
	 *          type of the class for which we create proxy object.
	 * @return proxy object created for class clazz
	 */
	public ReactomeDatabaseObject createProxyInstanceForClassType(Class<?> clazz) {
		try {
			ProxyFactory factory = new ProxyFactory();
			factory.setSuperclass(clazz);
			ReactomeDatabaseObject result = (ReactomeDatabaseObject) factory.create(new Class<?>[0], new Object[0], handler);
			return result;
		} catch (Exception e) {
			throw new InvalidArgumentException(e.getMessage());
		}

	}

	/**
	 * @return the dsu
	 */
	public DataSourceUpdater getDsu() {
		return dsu;
	}

	/**
	 * @param dsu
	 *          the dsu to set
	 */
	public void setDsu(DataSourceUpdater dsu) {
		this.dsu = dsu;
	}
}
