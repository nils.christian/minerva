package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;
import lcsb.mapviewer.reactome.xml.columnParser.GenomeEncodedEntityColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GenomeEncodedEntity"
 * >GenomeEncodedEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGenomeEncodedEntityParser extends ReactomeNodeParser<ReactomeGenomeEncodedEntity> {

	/**
	 * Default constructor.
	 */
	public ReactomeGenomeEncodedEntityParser()   {
		super(ReactomeGenomeEncodedEntity.class, GenomeEncodedEntityColumnParser.class);
	}
}
