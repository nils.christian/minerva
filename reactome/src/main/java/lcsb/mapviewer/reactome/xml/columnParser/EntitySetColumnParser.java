package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeEntitySet;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntitySet"
 * >EntitySet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EntitySetColumnParser extends ColumnParser<ReactomeEntitySet> {
	/**
	 * Default constructor. Defines parser for super class of EntitySet.
	 */
	protected EntitySetColumnParser() {
		super(PhysicalEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEntitySet object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("hasMember")) {
			object.addHasMembers(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("species")) {
			object.addSpecies(getsParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
