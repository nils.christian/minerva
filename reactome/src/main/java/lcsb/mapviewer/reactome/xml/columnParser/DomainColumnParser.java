package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeDomain;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Domain"
 * >Domain</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DomainColumnParser extends ColumnParser<ReactomeDomain> {

	/**
	 * Default constructor. Defines parser for super class of Domain.
	 */
	protected DomainColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeDomain object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
