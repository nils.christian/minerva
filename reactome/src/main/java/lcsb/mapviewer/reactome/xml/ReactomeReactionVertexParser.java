package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReactionVertex;
import lcsb.mapviewer.reactome.xml.columnParser.ReactionVertexColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionVertex"
 * >ReactionVertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionVertexParser extends ReactomeNodeParser<ReactomeReactionVertex> {

	/**
	 * Default constructor.
	 */
	public ReactomeReactionVertexParser()   {
		super(ReactomeReactionVertex.class, ReactionVertexColumnParser.class);
	}
}
