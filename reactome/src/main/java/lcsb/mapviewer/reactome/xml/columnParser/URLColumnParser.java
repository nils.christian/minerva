package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeURL;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=URL"
 * >URL</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class URLColumnParser extends ColumnParser<ReactomeURL> {
	/**
	 * Default constructor. Defines parser for super class of URL.
	 */
	protected URLColumnParser() {
		super(PublicationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeURL object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("uniformResourceLocator")) {
			object.setUniformResourceLocator(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
