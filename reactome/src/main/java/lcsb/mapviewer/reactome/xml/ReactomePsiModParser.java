package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePsiMod;
import lcsb.mapviewer.reactome.xml.columnParser.PsiModColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PsiMod"
 * >PsiMod</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePsiModParser extends ReactomeNodeParser<ReactomePsiMod> {

	/**
	 * Default constructor.
	 */
	public ReactomePsiModParser() {
		super(ReactomePsiMod.class, PsiModColumnParser.class);
	}
}
