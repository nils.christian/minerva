package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceDNASequence;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceDNASequence"
 * >ReferenceDNASequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceDNASequenceColumnParser extends ColumnParser<ReactomeReferenceDNASequence> {
	/**
	 * Default constructor. Defines parser for super class of ReferenceDNASequence.
	 */
	protected ReferenceDNASequenceColumnParser() {
		super(ReferenceSequenceColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceDNASequence object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
