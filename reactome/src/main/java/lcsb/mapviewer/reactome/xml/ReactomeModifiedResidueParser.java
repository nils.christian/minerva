package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeModifiedResidue;
import lcsb.mapviewer.reactome.xml.columnParser.ModifiedResidueColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=NegativeRegulation"
 * >NegativeRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeModifiedResidueParser extends ReactomeNodeParser<ReactomeModifiedResidue> {

	/**
	 * Default constructor.
	 */
	public ReactomeModifiedResidueParser() {
		super(ReactomeModifiedResidue.class, ModifiedResidueColumnParser.class);
	}
}
