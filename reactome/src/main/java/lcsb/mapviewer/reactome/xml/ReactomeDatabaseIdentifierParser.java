package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeDatabaseIdentifier;
import lcsb.mapviewer.reactome.xml.columnParser.DatabaseIdentifierColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DatabaseIdentifier"
 * >DatabaseIdentifier</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDatabaseIdentifierParser extends ReactomeNodeParser<ReactomeDatabaseIdentifier> {

	/**
	 * Default constructor.
	 */
	public ReactomeDatabaseIdentifierParser() {
		super(ReactomeDatabaseIdentifier.class, DatabaseIdentifierColumnParser.class);
	}
}
