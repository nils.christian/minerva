package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeRequirement;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Requirement"
 * >Requirement</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class RequirementColumnParser extends ColumnParser<ReactomeRequirement> {
	/**
	 * Default constructor. Defines parser for super class of Requirement.
	 */
	protected RequirementColumnParser() {
		super(PositiveRegulationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeRequirement object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
