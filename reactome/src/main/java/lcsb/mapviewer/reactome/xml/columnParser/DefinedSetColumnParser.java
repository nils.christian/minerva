package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DefinedSet"
 * >DefinedSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DefinedSetColumnParser extends ColumnParser<ReactomeDefinedSet> {
	/**
	 * Default constructor. Defines parser for super class of DefinedSet.
	 */
	protected DefinedSetColumnParser() {
		super(EntitySetColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeDefinedSet object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
