package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceDatabase;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceDatabase"
 * >ReferenceDatabase</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceDatabaseColumnParser extends ColumnParser<ReactomeReferenceDatabase> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceDatabase.
	 */
	protected ReferenceDatabaseColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceDatabase object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("accessUrl")) {
			object.setAccessUrl(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("url")) {
			object.setUrl(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
