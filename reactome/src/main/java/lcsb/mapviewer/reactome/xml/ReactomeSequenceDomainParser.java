package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeSequenceDomain;
import lcsb.mapviewer.reactome.xml.columnParser.SequenceDomainColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SequenceDomain">
 * SequenceDomain</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSequenceDomainParser extends ReactomeNodeParser<ReactomeSequenceDomain> {

	/**
	 * Default constructor.
	 */
	public ReactomeSequenceDomainParser()   {
		super(ReactomeSequenceDomain.class, SequenceDomainColumnParser.class);
	}
}
