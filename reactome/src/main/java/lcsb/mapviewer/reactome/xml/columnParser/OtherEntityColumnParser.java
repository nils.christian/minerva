package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=OtherEntity"
 * >OtherEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class OtherEntityColumnParser extends ColumnParser<ReactomeOtherEntity> {
	/**
	 * Default constructor. Defines parser for super class of OtherEntity.
	 */
	protected OtherEntityColumnParser() {
		super(PhysicalEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeOtherEntity object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
