package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeEntityCompartment;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityCompartment"
 * >EntityCompartment</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EntityCompartmentColumnParser extends ColumnParser<ReactomeEntityCompartment> {
	/**
	 * Default constructor. Defines parser for super class of EntityCompartment.
	 */
	protected EntityCompartmentColumnParser() {
		super(CompartmentColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEntityCompartment object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
