package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeEvidenceType;
import lcsb.mapviewer.reactome.xml.columnParser.EvidenceTypeColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EvidenceType"
 * >EvidenceType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEvidenceTypeParser extends ReactomeNodeParser<ReactomeEvidenceType> {

	/**
	 * Default constructor.
	 */
	public ReactomeEvidenceTypeParser() {
		super(ReactomeEvidenceType.class, EvidenceTypeColumnParser.class);
	}
}
