package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeOpenSet;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=OpenSet"
 * >OpenSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class OpenSetColumnParser extends ColumnParser<ReactomeOpenSet> {
	/**
	 * Default constructor. Defines parser for super class of OpenSet.
	 */
	protected OpenSetColumnParser() {
		super(EntitySetColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeOpenSet object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("referenceEntity")) {
			object.setReferenceEntity(getReParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
