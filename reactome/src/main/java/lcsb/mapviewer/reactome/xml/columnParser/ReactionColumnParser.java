package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReaction;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Reaction"
 * >Reaction</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionColumnParser extends ColumnParser<ReactomeReaction> {
	/**
	 * Default constructor. Defines parser for super class of Reaction.
	 */
	protected ReactionColumnParser() {
		super(ReactionlikeEventColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReaction object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("reverseReaction")) {
			object.setReverseReaction(getrParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
