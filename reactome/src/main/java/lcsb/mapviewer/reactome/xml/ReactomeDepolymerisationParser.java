package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeDepolymerisation;
import lcsb.mapviewer.reactome.xml.columnParser.DepolymerisationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Depolymerisation"
 * >Depolymerisation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDepolymerisationParser extends ReactomeNodeParser<ReactomeDepolymerisation> {

	/**
	 * Default constructor.
	 */
	public ReactomeDepolymerisationParser() {
		super(ReactomeDepolymerisation.class, DepolymerisationColumnParser.class);
	}
}
