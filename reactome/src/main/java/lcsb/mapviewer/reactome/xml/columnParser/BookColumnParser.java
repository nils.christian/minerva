package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeBook;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Book"
 * >Book</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class BookColumnParser extends ColumnParser<ReactomeBook> {
	/**
	 * Default constructor. Defines parser for super class of Book.
	 */
	protected BookColumnParser() {
		super(PublicationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeBook object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
