package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CatalystActivity"
 * >CatalystActivity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class CatalystActivityColumnParser extends ColumnParser<ReactomeCatalystActivity> {
	/**
	 * Default constructor. Defines parser for super class of CatalystActivity.
	 */
	protected CatalystActivityColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeCatalystActivity object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("activity")) {
			object.setActivity(getGmfParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("physicalEntity")) {
			object.setPhysicalEntity(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("activeUnit")) {
			object.addActiveUnit(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("literatureReference")) {
			object.addLiteratureReference(getPubParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
