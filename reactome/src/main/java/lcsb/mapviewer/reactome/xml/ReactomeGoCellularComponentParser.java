package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeGoCellularComponent;
import lcsb.mapviewer.reactome.xml.columnParser.GoCellularComponentColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_CellularComponent"
 * >GoCellularComponent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGoCellularComponentParser extends ReactomeNodeParser<ReactomeGoCellularComponent> {

	/**
	 * Default constructor.
	 */
	public ReactomeGoCellularComponentParser()   {
		super(ReactomeGoCellularComponent.class, GoCellularComponentColumnParser.class);
	}
}
