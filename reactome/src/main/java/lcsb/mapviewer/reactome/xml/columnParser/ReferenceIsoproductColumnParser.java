package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceIsoform;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceIsoform"
 * >ReferenceIsoform</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceIsoproductColumnParser extends ColumnParser<ReactomeReferenceIsoform> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceIsoform.
	 */
	protected ReferenceIsoproductColumnParser() {
		super(ReferenceGeneProductColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceIsoform object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("isoformParent")) {
			object.addIsoformParent(getRgpParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("variantIdentifier")) {
			object.setVariantIdentifier(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
