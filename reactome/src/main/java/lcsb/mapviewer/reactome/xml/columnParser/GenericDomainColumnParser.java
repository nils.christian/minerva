package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGenericDomain;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GenericDomain"
 * >GenericDomain</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GenericDomainColumnParser extends ColumnParser<ReactomeGenericDomain> {

	/**
	 * Default constructor. Defines parser for super class of GenericDomain.
	 */
	protected GenericDomainColumnParser() {
		super(DomainColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGenericDomain object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
