package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeBlackBoxEvent;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=BlackBoxEvent"
 * >BlackBoxEvent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class BlackBoxEventColumnParser extends ColumnParser<ReactomeBlackBoxEvent> {
	/**
	 * Default constructor. Defines parser for super class of BlackBoxEvent.
	 */
	protected BlackBoxEventColumnParser() {
		super(ReactionlikeEventColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeBlackBoxEvent object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
