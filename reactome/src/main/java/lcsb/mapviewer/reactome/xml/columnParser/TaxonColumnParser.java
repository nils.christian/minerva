package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeTaxon;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Taxon"
 * >Taxon</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class TaxonColumnParser extends ColumnParser<ReactomeTaxon> {
	/**
	 * Default constructor. Defines parser for super class of Taxon.
	 */
	protected TaxonColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeTaxon object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("crossReference")) {
			object.addCrossReference(getDiParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("superTaxon")) {
			object.setSuperTaxon(gettParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
