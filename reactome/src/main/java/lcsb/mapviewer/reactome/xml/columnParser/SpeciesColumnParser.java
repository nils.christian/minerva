package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeSpecies;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Species"
 * >Species</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SpeciesColumnParser extends ColumnParser<ReactomeSpecies> {
	/**
	 * Default constructor. Defines parser for super class of Species.
	 */
	protected SpeciesColumnParser() {
		super(TaxonColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeSpecies object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
