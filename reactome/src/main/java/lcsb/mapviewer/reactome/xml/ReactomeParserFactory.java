package lcsb.mapviewer.reactome.xml;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.persist.SpringApplicationContext;

import org.apache.log4j.Logger;

/**
 * Factory used for creation of {@link ReactomeNodeParser parsers} used for
 * creating reactome objects.
 * 
 * @author Piotr Gawron
 * 
 */
public final class ReactomeParserFactory {
	/**
	 * Default constructor. Prevents instatiation.
	 */
	private ReactomeParserFactory() {

	}

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger																logger	= Logger.getLogger(ReactomeParserFactory.class);

	/**
	 * Static field with all already created parsers.
	 */
	private static Map<Class<?>, ReactomeNodeParser<?>>	parsers	= new HashMap<Class<?>, ReactomeNodeParser<?>>();

	/**
	 * Gets parser for the apropriate class type.
	 * 
	 * @param classType
	 *          class type for which we want to create parser
	 * @return parser for given class type
	 */
	public static ReactomeNodeParser<?> getParserForClass(Class<?> classType) {
		String parserClassName = ReactomeDatabaseObjectParser.class.getPackage().getName() + "." + classType.getSimpleName() + "Parser";
		String parserSimpleClassName = classType.getSimpleName() + "Parser";

		try {
			Class<?> resultClass = Class.forName(parserClassName);
			ReactomeNodeParser<?> result = parsers.get(resultClass);
			if (result == null) {
				result = (ReactomeNodeParser<?>) SpringApplicationContext.getBean(parserSimpleClassName);
				parsers.put(resultClass, result);
			}
			return result;
		} catch (ClassNotFoundException e) {
			throw new InvalidArgumentException("Parser for class " + classType + " cannot be found");
		}
	}
}
