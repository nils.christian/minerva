package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PhysicalEntity"
 * >PhysicalEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PhysicalEntityColumnParser extends ColumnParser<ReactomePhysicalEntity> {

	/**
	 * Default constructor. Defines parser for super class of PhysicalEntity.
	 */
	protected PhysicalEntityColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePhysicalEntity object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("compartment")) {
			object.addCompartment(getEcParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("inferredTo")) {
			object.addInferredTo(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("inferredFrom")) {
			object.addInferredFrom(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("summation")) {
			object.addSummation(getSuParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("crossReference")) {
			object.addCrossReference(getDiParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("literatureReference")) {
			object.addLiteratureReference(getPubParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("edited")) {
			object.addEdited(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("revised")) {
			object.addRevised(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("reviewed")) {
			object.addReviewed(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("authored")) {
			object.setAuthored(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("figure")) {
			object.addFigure(getfParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("disease")) {
			object.addDisease(getdParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("goCellularComponent")) {
			object.setGoCellularComponent(getGccParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("definition")) {
			object.setDefinition(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("systematicName")) {
			object.setSystematicName(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
