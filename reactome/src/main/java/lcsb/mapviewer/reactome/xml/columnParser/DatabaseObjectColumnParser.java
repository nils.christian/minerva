package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DatabaseObject"
 * >DatabaseObject</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DatabaseObjectColumnParser extends ColumnParser<ReactomeDatabaseObject> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(DatabaseObjectColumnParser.class);

	/**
	 * Default constructor.
	 */
	protected DatabaseObjectColumnParser() {
		// there is no parent olumn parser (it's the top element in the hierarchy)
		super(null);
	}

	@Override
	public void updateColumnFromNode(ReactomeDatabaseObject object, Node node) {
		if (node.getNodeType() != Node.ELEMENT_NODE) {
			return;
		}
		if (node.getNodeName().equalsIgnoreCase("dbId")) {
			object.setDbId(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("created")) {
			object.setCreated(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("modified")) {
			object.addModified(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("displayName")) {
			object.setDisplayName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("schemaClass")) {
			object.setSchemaClass(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("stableIdentifier")) {
			object.setStableIdentifier(getSiParser().parseSimplifiedObject(node));
		} else {
			throw new InvalidArgumentException(
					"Unknown node type for ReactomeDatabaseObject: " + node.getNodeName() + ". Object: " + object.getClass() + " [" + object.getDbId() + "]");
		}
	}
}
