package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePathway;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Pathway"
 * >Pathway</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PathwayColumnParser extends ColumnParser<ReactomePathway> {
	/**
	 * Default constructor. Defines parser for super class of Pathway.
	 */
	protected PathwayColumnParser() {
		super(EventColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePathway object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("hasDiagram")) {
			object.setHasDiagram(node.getTextContent().equalsIgnoreCase("TRUE"));
		} else if (node.getNodeName().equalsIgnoreCase("hasEvent")) {
			object.addHasEvent(geteParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("doi")) {
			object.setDoi(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
