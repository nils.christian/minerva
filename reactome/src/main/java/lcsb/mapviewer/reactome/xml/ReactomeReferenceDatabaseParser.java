package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceDatabase;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceDatabaseColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceDatabase"
 * >ReferenceDatabase</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceDatabaseParser extends ReactomeNodeParser<ReactomeReferenceDatabase> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceDatabaseParser()   {
		super(ReactomeReferenceDatabase.class, ReferenceDatabaseColumnParser.class);
	}
}
