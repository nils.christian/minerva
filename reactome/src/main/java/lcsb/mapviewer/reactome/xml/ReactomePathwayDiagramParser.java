package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePathwayDiagram;
import lcsb.mapviewer.reactome.xml.columnParser.PathwayDiagramColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayDiagram"
 * >PathwayDiagram</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathwayDiagramParser extends ReactomeNodeParser<ReactomePathwayDiagram> {

	/**
	 * Default constructor.
	 */
	public ReactomePathwayDiagramParser()   {
		super(ReactomePathwayDiagram.class, PathwayDiagramColumnParser.class);
	}
}
