package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFragmentDeletionModification;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentDeletionModification"
 * >FragmentDeletionModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FragmentDeletionModificationColumnParser extends ColumnParser<ReactomeFragmentDeletionModification> {
	/**
	 * Default constructor. Defines parser for super class of
	 * FragmentDeletionModification.
	 */
	protected FragmentDeletionModificationColumnParser() {
		super(FragmentModificationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFragmentDeletionModification object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
