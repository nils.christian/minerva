package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeModifiedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ModifiedResidue"
 * >ModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModifiedResidueColumnParser extends ColumnParser<ReactomeModifiedResidue> {
	/**
	 * Default constructor. Defines parser for super class of ModifiedResidue.
	 */
	protected ModifiedResidueColumnParser() {
		super(TranslationalModificationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeModifiedResidue object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
