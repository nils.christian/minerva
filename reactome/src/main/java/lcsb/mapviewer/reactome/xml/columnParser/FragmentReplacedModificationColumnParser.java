package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFragmentReplacedModification;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentReplacedModification"
 * >FragmentReplacedModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FragmentReplacedModificationColumnParser extends ColumnParser<ReactomeFragmentReplacedModification> {
	/**
	 * Default constructor. Defines parser for super class of FragmentReplacedModification.
	 */
	protected FragmentReplacedModificationColumnParser() {
		super(FragmentModificationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFragmentReplacedModification object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
