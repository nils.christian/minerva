package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeConcurrentEventSet;
import lcsb.mapviewer.reactome.xml.columnParser.ConcurrentEventSetColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ConcurrentEventSet"
 * >ConcurrentEventSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeConcurrentEventSetParser extends ReactomeNodeParser<ReactomeConcurrentEventSet> {

	/**
	 * Default constructor.
	 */
	public ReactomeConcurrentEventSetParser() {
		super(ReactomeConcurrentEventSet.class, ConcurrentEventSetColumnParser.class);
	}
}
