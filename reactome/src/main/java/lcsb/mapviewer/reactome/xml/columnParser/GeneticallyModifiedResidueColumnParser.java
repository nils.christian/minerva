package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGeneticallyModifiedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GeneticallyModifiedResidue"
 * >GeneticallyModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneticallyModifiedResidueColumnParser extends ColumnParser<ReactomeGeneticallyModifiedResidue> {
	/**
	 * Default constructor. Defines parser for super class of GeneticallyModifiedResidue.
	 */
	protected GeneticallyModifiedResidueColumnParser() {
		super(AbstractModifiedResidueColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGeneticallyModifiedResidue object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
