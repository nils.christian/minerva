package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeRequirement;
import lcsb.mapviewer.reactome.xml.columnParser.RequirementColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Requirement"
 * >Requirement</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeRequirementParser extends ReactomeNodeParser<ReactomeRequirement> {

	/**
	 * Default constructor.
	 */
	public ReactomeRequirementParser()   {
		super(ReactomeRequirement.class, RequirementColumnParser.class);
	}
}
