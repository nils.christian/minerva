package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeGroupModifiedResidue;
import lcsb.mapviewer.reactome.xml.columnParser.GroupModifiedResidueColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GroupModifiedResidue"
 * >GroupModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGroupModifiedResidueParser extends ReactomeNodeParser<ReactomeGroupModifiedResidue> {

	/**
	 * Default constructor.
	 */
	public ReactomeGroupModifiedResidueParser()   {
		super(ReactomeGroupModifiedResidue.class, GroupModifiedResidueColumnParser.class);
	}
}
