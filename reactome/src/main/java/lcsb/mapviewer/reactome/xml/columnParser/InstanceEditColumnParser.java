package lcsb.mapviewer.reactome.xml.columnParser;

import java.text.ParseException;
import java.util.Calendar;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.reactome.model.ReactomeInstanceEdit;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=InstanceEdit"
 * >InstanceEdit</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class InstanceEditColumnParser extends ColumnParser<ReactomeInstanceEdit> {

	/**
	 * Default constructor. Defines parser for super class of InstanceEdit.
	 */
	protected InstanceEditColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeInstanceEdit object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("author")) {
			object.setAuthor(getPersonParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("note")) {
			object.setNote(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("dateTime")) {
			Calendar cal = Calendar.getInstance();
			try {
				cal.setTime(TIME_STAMP_PARSER.parse(node.getTextContent()));
			} catch (DOMException e) {
				throw new InvalidArgumentException("Invalid date format: " + node.getTextContent());
			} catch (ParseException e) {
				throw new InvalidArgumentException("Invalid date format: " + node.getTextContent());
			}
			object.setDateTime(cal);
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
