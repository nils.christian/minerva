package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGoMolecularFunction;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_MolecularFunction"
 * >GO_MolecularFunction</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GoMolecularFunctionColumnParser extends ColumnParser<ReactomeGoMolecularFunction> {
	/**
	 * Default constructor. Defines parser for super class of GoMolecularFunction.
	 */
	protected GoMolecularFunctionColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGoMolecularFunction object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("accession")) {
			object.setAccession(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("definition")) {
			object.setDefinition(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("ecNumber")) {
			object.addEcNumber(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("referenceDatabase")) {
			object.setReferenceDatabase(getRdParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("regulate")) {
			object.addRegulate(getGbpParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
