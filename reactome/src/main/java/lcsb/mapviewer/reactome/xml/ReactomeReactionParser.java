package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReaction;
import lcsb.mapviewer.reactome.xml.columnParser.ReactionColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Reaction"
 * >Reaction</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionParser extends ReactomeNodeParser<ReactomeReaction> {

	/**
	 * Default constructor.
	 */
	public ReactomeReactionParser()   {
		super(ReactomeReaction.class, ReactionColumnParser.class);
	}
}
