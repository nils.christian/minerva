package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGoBiologicalProcess;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_BiologicalProcess"
 * >GO_BiologicalProcess</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GoBiologicalProcessColumnParser extends ColumnParser<ReactomeGoBiologicalProcess> {
	/**
	 * Default constructor. Defines parser for super class of GoBiologicalProcess.
	 */
	protected GoBiologicalProcessColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGoBiologicalProcess object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("accession")) {
			object.setAccession(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("definition")) {
			object.setDefinition(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("referenceDatabase")) {
			object.setReferenceDatabase(getRdParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
