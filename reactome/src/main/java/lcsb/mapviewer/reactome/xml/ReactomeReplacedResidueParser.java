package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReplacedResidue;
import lcsb.mapviewer.reactome.xml.columnParser.ReplacedResidueColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReplacedResidue"
 * >ReplacedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReplacedResidueParser extends ReactomeNodeParser<ReactomeReplacedResidue> {

	/**
	 * Default constructor.
	 */
	public ReactomeReplacedResidueParser() {
		super(ReactomeReplacedResidue.class, ReplacedResidueColumnParser.class);
	}
}
