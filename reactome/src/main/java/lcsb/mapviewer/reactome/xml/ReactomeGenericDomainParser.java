package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeGenericDomain;
import lcsb.mapviewer.reactome.xml.columnParser.GenericDomainColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GenericDomain"
 * >GenericDomain</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGenericDomainParser extends ReactomeNodeParser<ReactomeGenericDomain> {

	/**
	 * Default constructor.
	 */
	public ReactomeGenericDomainParser()   {
		super(ReactomeGenericDomain.class, GenericDomainColumnParser.class);
	}
}
