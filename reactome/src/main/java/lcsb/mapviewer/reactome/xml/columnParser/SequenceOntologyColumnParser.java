package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeSequenceOntology;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SequenceOntology"
 * >SequenceOntology</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SequenceOntologyColumnParser extends ColumnParser<ReactomeSequenceOntology> {

	/**
	 * Default constructor. Defines parser for super class of SequenceOntology.
	 */
	protected SequenceOntologyColumnParser() {
		super(ExternalOntologyColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeSequenceOntology object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
