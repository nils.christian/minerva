package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceRNASequence;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceRNASequenceColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceRNASequence"
 * >ReferenceRNASequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceRNASequenceParser extends ReactomeNodeParser<ReactomeReferenceRNASequence> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceRNASequenceParser()   {
		super(ReactomeReferenceRNASequence.class, ReferenceRNASequenceColumnParser.class);
	}
}
