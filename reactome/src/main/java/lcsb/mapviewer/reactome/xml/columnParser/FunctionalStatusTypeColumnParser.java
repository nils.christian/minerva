package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFunctionalStatusType;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FunctionalStatusType"
 * >FunctionalStatusType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FunctionalStatusTypeColumnParser extends ColumnParser<ReactomeFunctionalStatusType> {
	/**
	 * Default constructor. Defines parser for super class of FunctionalStatusType.
	 */
	protected FunctionalStatusTypeColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFunctionalStatusType object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
