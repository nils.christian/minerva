package lcsb.mapviewer.reactome.xml.columnParser;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;

/**
 * Factory that creates single instance of ColumnParser class for specific
 * Reactome class and returns it whenever asked for the instance of this class.
 * 
 * @author Piotr Gawron
 * 
 */
public final class ColumnParserFactory {
	/**
	 * Map with all possible column parsers.
	 */
	private static Map<Class<?>, Object>	instances	= new HashMap<Class<?>, Object>();

	/**
	 * Privet constructor to prevent instatization of the class.
	 */
	private ColumnParserFactory() {
	}

	/**
	 * Returns column parser for a given class.
	 * 
	 * @param columnParserClazz
	 *          parser class that we want to get
	 * @return ColumnParser object for a class given in the param
	 */
	@SuppressWarnings("unchecked")
	public static ColumnParser<ReactomeDatabaseObject> getColumnParser(Class<?> columnParserClazz) {
		if (instances.get(columnParserClazz) == null) {
			try {
				instances.put(columnParserClazz, columnParserClazz.newInstance());
			} catch (InstantiationException e) {
				throw new InvalidArgumentException("Invalid class type");
			} catch (IllegalAccessException e) {
				throw new InvalidArgumentException("Invalid class type");
			}
		}
		return (ColumnParser<ReactomeDatabaseObject>) instances.get(columnParserClazz);
	}
}
