package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceEntityColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceEntity"
 * >ReferenceEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceEntityParser extends ReactomeNodeParser<ReactomeReferenceEntity> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceEntityParser()   {
		super(ReactomeReferenceEntity.class, ReferenceEntityColumnParser.class);
	}
}
