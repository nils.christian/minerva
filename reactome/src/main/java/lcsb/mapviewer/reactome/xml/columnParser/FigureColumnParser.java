package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFigure;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Figure"
 * >Figure</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FigureColumnParser extends ColumnParser<ReactomeFigure> {
	/**
	 * Default constructor. Defines parser for super class of Figure.
	 */
	protected FigureColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFigure object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("url")) {
			object.setUrl(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
