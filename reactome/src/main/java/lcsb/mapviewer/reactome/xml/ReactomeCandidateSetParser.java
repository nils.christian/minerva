package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;
import lcsb.mapviewer.reactome.xml.columnParser.CandidateSetColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CandidateSet"
 * >CandidateSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCandidateSetParser extends ReactomeNodeParser<ReactomeCandidateSet> {

	/**
	 * Default constructor.
	 */
	public ReactomeCandidateSetParser() {
		super(ReactomeCandidateSet.class, CandidateSetColumnParser.class);
	}
}
