package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePolymer;
import lcsb.mapviewer.reactome.xml.columnParser.PolymerColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Polymer"
 * >Polymer</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePolymerParser extends ReactomeNodeParser<ReactomePolymer> {

	/**
	 * Default constructor.
	 */
	public ReactomePolymerParser()   {
		super(ReactomePolymer.class, PolymerColumnParser.class);
	}
}
