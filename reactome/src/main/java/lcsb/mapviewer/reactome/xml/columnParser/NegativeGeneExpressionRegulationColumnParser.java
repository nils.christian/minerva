package lcsb.mapviewer.reactome.xml.columnParser;

import org.w3c.dom.Node;

import lcsb.mapviewer.reactome.model.ReactomeNegativeGeneExpressionRegulation;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=NegativeGeneExpressionRegulation"
 * >NegativeGeneExpressionRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class NegativeGeneExpressionRegulationColumnParser extends ColumnParser<ReactomeNegativeGeneExpressionRegulation> {
	/**
	 * Default constructor. Defines parser for super class of NegativeRegulation.
	 */
	protected NegativeGeneExpressionRegulationColumnParser() {
		super(NegativeRegulationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeNegativeGeneExpressionRegulation object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
