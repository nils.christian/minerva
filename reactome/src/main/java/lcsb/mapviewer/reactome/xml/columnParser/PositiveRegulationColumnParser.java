package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePositiveRegulation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PositiveRegulation"
 * >PositiveRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PositiveRegulationColumnParser extends ColumnParser<ReactomePositiveRegulation> {
	/**
	 * Default constructor. Defines parser for super class of PositiveRegulation.
	 */
	protected PositiveRegulationColumnParser() {
		super(RegulationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePositiveRegulation object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
