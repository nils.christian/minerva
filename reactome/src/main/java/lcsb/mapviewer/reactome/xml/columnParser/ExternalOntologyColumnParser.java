package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeExternalOntology;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ExternalOntology"
 * >ExternalOntology</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ExternalOntologyColumnParser extends ColumnParser<ReactomeExternalOntology> {
	/**
	 * Default constructor. Defines parser for super class of ExternalOntology.
	 */
	protected ExternalOntologyColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeExternalOntology object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("definition")) {
			object.setDefinition(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("synonym")) {
			object.addSynonym(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("identifier")) {
			object.setIdentifier(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("referenceDatabase")) {
			object.setReferenceDatabase(getRdParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
