package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFunctionalStatus;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FunctionalStatus"
 * >FunctionalStatus</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FunctionalStatusColumnParser extends ColumnParser<ReactomeFunctionalStatus> {
	/**
	 * Default constructor. Defines parser for super class of FunctionalStatus.
	 */
	protected FunctionalStatusColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFunctionalStatus object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
