package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.xml.columnParser.PhysicalEntityColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PhysicalEntity"
 * >PhysicalEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePhysicalEntityParser extends ReactomeNodeParser<ReactomePhysicalEntity> {

	/**
	 * Default constructor.
	 */
	public ReactomePhysicalEntityParser()   {
		super(ReactomePhysicalEntity.class, PhysicalEntityColumnParser.class);
	}
}
