package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePathwayCoordinates;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayCoordinates"
 * >PathwayCoordinates</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PathwayCoordinatesColumnParser extends ColumnParser<ReactomePathwayCoordinates> {
	/**
	 * Default constructor. Defines parser for super class of PathwayCoordinates.
	 */
	protected PathwayCoordinatesColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePathwayCoordinates object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("maxX")) {
			object.setMaxX(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("maxY")) {
			object.setMaxY(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("minX")) {
			object.setMinX(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("minY")) {
			object.setMinY(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
