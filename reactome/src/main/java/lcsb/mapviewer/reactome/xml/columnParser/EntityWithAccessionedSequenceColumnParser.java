package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityWithAccessionedSequence"
 * >EntityWithAccessionedSequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EntityWithAccessionedSequenceColumnParser extends ColumnParser<ReactomeEntityWithAccessionedSequence> {
	/**
	 * Default constructor. Defines parser for super class of EntityWithAccessionedSequence.
	 */
	protected EntityWithAccessionedSequenceColumnParser() {
		super(GenomeEncodedEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEntityWithAccessionedSequence object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("endCoordinate")) {
			object.setEndCoordinate(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("referenceEntity")) {
			object.setReferenceEntity(getReParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("startCoordinate")) {
			object.setStartCoordinate(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("hasModifiedResidue")) {
			object.addHasModifiedResidue(getAmrParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
