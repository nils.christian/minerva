package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeDatabaseIdentifier;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DatabaseIdentifier"
 * >DatabaseIdentifier</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DatabaseIdentifierColumnParser extends ColumnParser<ReactomeDatabaseIdentifier> {
	/**
	 * Default constructor. Defines parser for super class of DatabaseIdentifier.
	 */
	protected DatabaseIdentifierColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeDatabaseIdentifier object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("identifier")) {
			object.setIdentifier(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("url")) {
			object.setUrl(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("referenceDatabase")) {
			object.setReferenceDatabase(getRdParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
