package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFigure;
import lcsb.mapviewer.reactome.xml.columnParser.FigureColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Figure"
 * >Figure</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFigureParser extends ReactomeNodeParser<ReactomeFigure> {

	/**
	 * Default constructor.
	 */
	public ReactomeFigureParser() {
		super(ReactomeFigure.class, FigureColumnParser.class);
	}
}
