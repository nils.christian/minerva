package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePathway;
import lcsb.mapviewer.reactome.xml.columnParser.PathwayColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Pathway"
 * >Pathway</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathwayParser extends ReactomeNodeParser<ReactomePathway> {

	/**
	 * Default constructor.
	 */
	public ReactomePathwayParser()   {
		super(ReactomePathway.class, PathwayColumnParser.class);
	}
}
