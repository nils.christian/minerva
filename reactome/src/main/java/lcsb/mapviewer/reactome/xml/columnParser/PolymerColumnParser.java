package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePolymer;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Polymer"
 * >Polymer</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PolymerColumnParser extends ColumnParser<ReactomePolymer> {
	/**
	 * Default constructor. Defines parser for super class of Polymer.
	 */
	protected PolymerColumnParser() {
		super(PhysicalEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePolymer object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("minUnitCount")) {
			object.setMinUnitCount(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("maxUnitCount")) {
			object.setMaxUnitCount(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("repeatedUnit")) {
			object.addRepeatedUnit(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
