package lcsb.mapviewer.reactome.xml.columnParser;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.reactome.model.ReactomeEvent;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Event"
 * >Event</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EventColumnParser extends ColumnParser<ReactomeEvent> {
	/**
	 * Default constructor. Defines parser for super class of Event.
	 */
	protected EventColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEvent object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("compartment")) {
			object.addCompartment(getEcParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("evidenceType")) {
			object.setEvidenceType(getEtParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("inferredFrom")) {
			object.addInferredFrom(geteParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("precedingEvent")) {
			object.addPrecedingEvent(geteParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("orthologousEvent")) {
			object.addOrthologousEvent(geteParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("authored")) {
			object.addAuthored(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("edited")) {
			object.addEdited(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("reviewed")) {
			object.addReviewed(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("definition")) {
			object.setDefinition(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("isInDisease")) {
			object.setIsInDisease(node.getTextContent().equalsIgnoreCase("TRUE"));
		} else if (node.getNodeName().equalsIgnoreCase("_doRelease")) {
			object.setDoRelease(node.getTextContent().equalsIgnoreCase("TRUE"));
		} else if (node.getNodeName().equalsIgnoreCase("isInferred")) {
			object.setIsInferred(node.getTextContent().equalsIgnoreCase("TRUE"));
		} else if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("summation")) {
			object.addSummation(getSuParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("literatureReference")) {
			object.addLiteratureReferences(getPubParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("revised")) {
			object.addRevised(getIeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("crossReference")) {
			object.addCrossReferences(getDiParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("figure")) {
			object.addFigure(getfParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("goBiologicalProcess")) {
			object.setGoBiologicalProcess(getGbpParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("releaseStatus")) {
			object.setReleaseStatus(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("relatedSpecies")) {
			object.addRelatedSpecies(getsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("disease")) {
			object.addDisease(getdParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("speciesName")) {
			object.setSpeciesName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("releaseDate")) {
			Calendar calendar = Calendar.getInstance();
			Date date;
			try {
				date = DATE_PARSER.parse(node.getTextContent());
				calendar.setTime(date);
				object.setReleaseDate(calendar);
			} catch (DOMException e) {
				throw new InvalidArgumentException("Problem with date parsing: " + e.getMessage());
			} catch (ParseException e) {
				throw new InvalidArgumentException("Problem with date parsing: " + e.getMessage());
			}
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}
}
