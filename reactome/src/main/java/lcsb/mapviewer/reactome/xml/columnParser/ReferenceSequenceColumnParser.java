package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceSequence;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceSequence"
 * >ReferenceSequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceSequenceColumnParser extends ColumnParser<ReactomeReferenceSequence> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceSequence.
	 */
	protected ReferenceSequenceColumnParser() {
		super(ReferenceEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceSequence object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("checksum")) {
			object.setChecksum(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("keyword")) {
			object.addKeyword(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("description")) {
			object.addDescription(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("comment")) {
			object.addComment(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("geneName")) {
			object.addGeneName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("secondaryIdentifier")) {
			object.addSecondaryIdentifier(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("sequenceLength")) {
			object.setSequenceLength(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("isSequenceChanged")) {
			object.setIsSequenceChanged(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
