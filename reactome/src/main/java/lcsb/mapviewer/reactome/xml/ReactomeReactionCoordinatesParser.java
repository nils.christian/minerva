package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReactionCoordinates;
import lcsb.mapviewer.reactome.xml.columnParser.ReactionCoordinatesColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionCoordinates"
 * >ReactionCoordinates</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionCoordinatesParser extends ReactomeNodeParser<ReactomeReactionCoordinates> {

	/**
	 * Default constructor.
	 */
	public ReactomeReactionCoordinatesParser()   {
		super(ReactomeReactionCoordinates.class, ReactionCoordinatesColumnParser.class);
	}
}
