package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeEntityCompartment;
import lcsb.mapviewer.reactome.xml.columnParser.EntityCompartmentColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityCompartment"
 * >EntityCompartment</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntityCompartmentParser extends ReactomeNodeParser<ReactomeEntityCompartment> {

	/**
	 * Default constructor.
	 */
	public ReactomeEntityCompartmentParser() {
		super(ReactomeEntityCompartment.class, EntityCompartmentColumnParser.class);
	}
}
