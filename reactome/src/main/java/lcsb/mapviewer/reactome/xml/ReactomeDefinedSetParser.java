package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeDefinedSet;
import lcsb.mapviewer.reactome.xml.columnParser.DefinedSetColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DefinedSet"
 * >DefinedSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDefinedSetParser extends ReactomeNodeParser<ReactomeDefinedSet> {

	/**
	 * Default constructor.
	 */
	public ReactomeDefinedSetParser() {
		super(ReactomeDefinedSet.class, DefinedSetColumnParser.class);
	}
}
