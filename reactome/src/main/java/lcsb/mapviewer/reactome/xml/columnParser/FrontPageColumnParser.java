package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFrontPage;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FrontPage"
 * >FrontPage</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FrontPageColumnParser extends ColumnParser<ReactomeFrontPage> {
	/**
	 * Default constructor. Defines parser for super class of FrontPage.
	 */
	protected FrontPageColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFrontPage object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("frontPageItem")) {
			object.addFrontPageItem(geteParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
