package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeStableIdentifier;
import lcsb.mapviewer.reactome.xml.columnParser.StableIdentifierColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=StableIdentifier"
 * >StableIdentifier</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeStableIdentifierParser extends ReactomeNodeParser<ReactomeStableIdentifier> {

	/**
	 * Default constructor.
	 */
	public ReactomeStableIdentifierParser()   {
		super(ReactomeStableIdentifier.class, StableIdentifierColumnParser.class);
	}
}
