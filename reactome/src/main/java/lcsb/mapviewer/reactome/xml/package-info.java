/**
 * Provides parsers for xml data retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a>.
 */
package lcsb.mapviewer.reactome.xml;

