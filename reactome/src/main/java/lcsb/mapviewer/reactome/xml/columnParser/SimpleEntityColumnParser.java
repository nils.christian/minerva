package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SimpleEntity"
 * >SimpleEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SimpleEntityColumnParser extends ColumnParser<ReactomeSimpleEntity> {
	/**
	 * Default constructor. Defines parser for super class of SimpleEntity.
	 */
	protected SimpleEntityColumnParser() {
		super(PhysicalEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeSimpleEntity object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("referenceEntity")) {
			object.addReferenceEntity(getRmParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
