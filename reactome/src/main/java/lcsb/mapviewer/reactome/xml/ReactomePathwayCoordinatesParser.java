package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePathwayCoordinates;
import lcsb.mapviewer.reactome.xml.columnParser.PathwayCoordinatesColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayCoordinates"
 * >PathwayCoordinates</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathwayCoordinatesParser extends ReactomeNodeParser<ReactomePathwayCoordinates> {

	/**
	 * Default constructor.
	 */
	public ReactomePathwayCoordinatesParser()   {
		super(ReactomePathwayCoordinates.class, PathwayCoordinatesColumnParser.class);
	}
}
