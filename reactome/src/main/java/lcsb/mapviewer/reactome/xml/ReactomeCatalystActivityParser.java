package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.xml.columnParser.CatalystActivityColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CatalystActivity"
 * >CatalystActivity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCatalystActivityParser extends ReactomeNodeParser<ReactomeCatalystActivity> {

	/**
	 * Default constructor.
	 */
	public ReactomeCatalystActivityParser() {
		super(ReactomeCatalystActivity.class, CatalystActivityColumnParser.class);
	}
}
