package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePerson;
import lcsb.mapviewer.reactome.xml.columnParser.PersonColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Person"
 * >Person</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePersonParser extends ReactomeNodeParser<ReactomePerson> {

	/**
	 * Default constructor.
	 */
	public ReactomePersonParser()   {
		super(ReactomePerson.class, PersonColumnParser.class);
	}
}
