package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceMoleculeColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceMolecule"
 * >ReferenceMolecule</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceMoleculeParser extends ReactomeNodeParser<ReactomeReferenceMolecule> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceMoleculeParser()   {
		super(ReactomeReferenceMolecule.class, ReferenceMoleculeColumnParser.class);
	}
}
