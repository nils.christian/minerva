package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFunctionalStatus;
import lcsb.mapviewer.reactome.xml.columnParser.FunctionalStatusColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FunctionalStatus"
 * >FunctionalStatus</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFunctionalStatusParser extends ReactomeNodeParser<ReactomeFunctionalStatus> {

	/**
	 * Default constructor.
	 */
	public ReactomeFunctionalStatusParser()   {
		super(ReactomeFunctionalStatus.class, FunctionalStatusColumnParser.class);
	}
}
