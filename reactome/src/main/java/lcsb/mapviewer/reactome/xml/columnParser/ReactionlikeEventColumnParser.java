package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionlikeEvent"
 * >ReactionlikeEvent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionlikeEventColumnParser extends ColumnParser<ReactomeReactionlikeEvent> {
	/**
	 * Default constructor. Defines parser for super class of ReactionlikeEvent.
	 */
	protected ReactionlikeEventColumnParser() {
		super(EventColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReactionlikeEvent object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("catalystActivity")) {
			object.addCatalystActivity(getCaParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("input")) {
			object.addInput(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("output")) {
			object.addOutput(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("entityOnOtherCell")) {
			object.addEntityOnOtherCell(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("isChimeric")) {
			object.setIsChimeric(node.getTextContent().equalsIgnoreCase("TRUE"));
		} else if (node.getNodeName().equalsIgnoreCase("entityFunctionalStatus")) {
			object.addEntityFunctionalStatus(getEfsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("normalReaction")) {
			object.addNormalReactions(getRleParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("requiredInputComponent")) {
			object.addRequiredInputComponents(getDoParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
