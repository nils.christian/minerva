package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFragmentInsertionModification;
import lcsb.mapviewer.reactome.xml.columnParser.FragmentInsertionModificationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentInsertionModification"
 * >FragmentInsertionModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFragmentInsertionModificationParser extends ReactomeNodeParser<ReactomeFragmentInsertionModification> {

	/**
	 * Default constructor.
	 */
	public ReactomeFragmentInsertionModificationParser() {
		super(ReactomeFragmentInsertionModification.class, FragmentInsertionModificationColumnParser.class);
	}
}
