package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeAffiliation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Affiliation"
 * >Affiliation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class AffiliationColumnParser extends ColumnParser<ReactomeAffiliation> {
	/**
	 * Default constructor. Defines parser for super class of Affiliation.
	 */
	protected AffiliationColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeAffiliation object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
