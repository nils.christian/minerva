package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeEntityFunctionalStatus;
import lcsb.mapviewer.reactome.xml.columnParser.EntityFunctionalStatusColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityFunctionalStatus"
 * >EntityFunctionalStatus</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntityFunctionalStatusParser extends ReactomeNodeParser<ReactomeEntityFunctionalStatus> {

	/**
	 * Default constructor.
	 */
	public ReactomeEntityFunctionalStatusParser() {
		super(ReactomeEntityFunctionalStatus.class, EntityFunctionalStatusColumnParser.class);
	}
}
