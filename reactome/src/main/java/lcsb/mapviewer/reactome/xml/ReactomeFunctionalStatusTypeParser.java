package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFunctionalStatusType;
import lcsb.mapviewer.reactome.xml.columnParser.FunctionalStatusTypeColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FunctionalStatusType"
 * >FunctionalStatusType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFunctionalStatusTypeParser extends ReactomeNodeParser<ReactomeFunctionalStatusType> {

	/**
	 * Default constructor.
	 */
	public ReactomeFunctionalStatusTypeParser()   {
		super(ReactomeFunctionalStatusType.class, FunctionalStatusTypeColumnParser.class);
	}
}
