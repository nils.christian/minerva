package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceGroup;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceGroupColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceGroup"
 * >ReferenceGroup</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceGroupParser extends ReactomeNodeParser<ReactomeReferenceGroup> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceGroupParser()   {
		super(ReactomeReferenceGroup.class, ReferenceGroupColumnParser.class);
	}
}
