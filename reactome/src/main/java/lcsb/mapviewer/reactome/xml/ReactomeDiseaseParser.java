package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeDisease;
import lcsb.mapviewer.reactome.xml.columnParser.DiseaseColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Disease"
 * >Disease</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDiseaseParser extends ReactomeNodeParser<ReactomeDisease> {

	/**
	 * Default constructor.
	 */
	public ReactomeDiseaseParser() {
		super(ReactomeDisease.class, DiseaseColumnParser.class);
	}
}
