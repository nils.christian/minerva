package lcsb.mapviewer.reactome.xml.columnParser;

import java.text.SimpleDateFormat;

import lcsb.mapviewer.reactome.model.ReactomeAbstractModifiedResidue;
import lcsb.mapviewer.reactome.model.ReactomeAffiliation;
import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;
import lcsb.mapviewer.reactome.model.ReactomeCatalystActivity;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseIdentifier;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeDisease;
import lcsb.mapviewer.reactome.model.ReactomeEntityCompartment;
import lcsb.mapviewer.reactome.model.ReactomeEntityFunctionalStatus;
import lcsb.mapviewer.reactome.model.ReactomeEvent;
import lcsb.mapviewer.reactome.model.ReactomeEvidenceType;
import lcsb.mapviewer.reactome.model.ReactomeFigure;
import lcsb.mapviewer.reactome.model.ReactomeFunctionalStatus;
import lcsb.mapviewer.reactome.model.ReactomeGoBiologicalProcess;
import lcsb.mapviewer.reactome.model.ReactomeGoCellularComponent;
import lcsb.mapviewer.reactome.model.ReactomeGoMolecularFunction;
import lcsb.mapviewer.reactome.model.ReactomeInstanceEdit;
import lcsb.mapviewer.reactome.model.ReactomeInterChainCrosslinkedResidue;
import lcsb.mapviewer.reactome.model.ReactomeIntraChainCrosslinkedResidue;
import lcsb.mapviewer.reactome.model.ReactomePathway;
import lcsb.mapviewer.reactome.model.ReactomePerson;
import lcsb.mapviewer.reactome.model.ReactomePhysicalEntity;
import lcsb.mapviewer.reactome.model.ReactomePsiMod;
import lcsb.mapviewer.reactome.model.ReactomePublication;
import lcsb.mapviewer.reactome.model.ReactomeReaction;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.model.ReactomeReferenceDNASequence;
import lcsb.mapviewer.reactome.model.ReactomeReferenceDatabase;
import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;
import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;
import lcsb.mapviewer.reactome.model.ReactomeReferenceRNASequence;
import lcsb.mapviewer.reactome.model.ReactomeReferenceSequence;
import lcsb.mapviewer.reactome.model.ReactomeSpecies;
import lcsb.mapviewer.reactome.model.ReactomeStableIdentifier;
import lcsb.mapviewer.reactome.model.ReactomeSummation;
import lcsb.mapviewer.reactome.model.ReactomeTaxon;
import lcsb.mapviewer.reactome.xml.ReactomeAbstractModifiedResidueParser;
import lcsb.mapviewer.reactome.xml.ReactomeAffiliationParser;
import lcsb.mapviewer.reactome.xml.ReactomeCandidateSetParser;
import lcsb.mapviewer.reactome.xml.ReactomeCatalystActivityParser;
import lcsb.mapviewer.reactome.xml.ReactomeDatabaseIdentifierParser;
import lcsb.mapviewer.reactome.xml.ReactomeDatabaseObjectParser;
import lcsb.mapviewer.reactome.xml.ReactomeDiseaseParser;
import lcsb.mapviewer.reactome.xml.ReactomeEntityCompartmentParser;
import lcsb.mapviewer.reactome.xml.ReactomeEntityFunctionalStatusParser;
import lcsb.mapviewer.reactome.xml.ReactomeEventParser;
import lcsb.mapviewer.reactome.xml.ReactomeEvidenceTypeParser;
import lcsb.mapviewer.reactome.xml.ReactomeFigureParser;
import lcsb.mapviewer.reactome.xml.ReactomeFunctionalStatusParser;
import lcsb.mapviewer.reactome.xml.ReactomeGoBiologicalProcessParser;
import lcsb.mapviewer.reactome.xml.ReactomeGoCellularComponentParser;
import lcsb.mapviewer.reactome.xml.ReactomeGoMolecularFunctionParser;
import lcsb.mapviewer.reactome.xml.ReactomeInstanceEditParser;
import lcsb.mapviewer.reactome.xml.ReactomeInterChainCrosslinkedResidueParser;
import lcsb.mapviewer.reactome.xml.ReactomeIntraChainCrosslinkedResidueParser;
import lcsb.mapviewer.reactome.xml.ReactomeParserFactory;
import lcsb.mapviewer.reactome.xml.ReactomePathwayParser;
import lcsb.mapviewer.reactome.xml.ReactomePersonParser;
import lcsb.mapviewer.reactome.xml.ReactomePhysicalEntityParser;
import lcsb.mapviewer.reactome.xml.ReactomePsiModParser;
import lcsb.mapviewer.reactome.xml.ReactomePublicationParser;
import lcsb.mapviewer.reactome.xml.ReactomeReactionParser;
import lcsb.mapviewer.reactome.xml.ReactomeReactionlikeEventParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceDNASequenceParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceDatabaseParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceEntityParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceGeneProductParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceMoleculeParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceRNASequenceParser;
import lcsb.mapviewer.reactome.xml.ReactomeReferenceSequenceParser;
import lcsb.mapviewer.reactome.xml.ReactomeSpeciesParser;
import lcsb.mapviewer.reactome.xml.ReactomeStableIdentifierParser;
import lcsb.mapviewer.reactome.xml.ReactomeSummationParser;
import lcsb.mapviewer.reactome.xml.ReactomeTaxonParser;

import org.w3c.dom.Node;

/**
 * Abstract class interface for parsing columns in the reactome objects from the
 * xml obtained by restfull API.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          reactome class to be parsed
 */
public abstract class ColumnParser<T extends ReactomeDatabaseObject> {
	/**
	 * Standard timestamp parser for reactome data.
	 */
	static final SimpleDateFormat									TIME_STAMP_PARSER	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * Standard date parser for reactome data.
	 */
	static final SimpleDateFormat									DATE_PARSER				= new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Parser of the columns of parent object (of T class) in the inheritance
	 * tree.
	 */
	private ColumnParser<ReactomeDatabaseObject>	parentParser			= null;

	/**
	 * Default constructor that creates parent column parser.
	 * 
	 * @param parentClass
	 *          parent class (for T class) in inheritance tree
	 */
	protected ColumnParser(Class<?> parentClass) {
		if (parentClass != null) {
			parentParser = ColumnParserFactory.getColumnParser(parentClass);
		}
	}

	/**
	 * Updates data in object from xml node.
	 * 
	 * @param object
	 *          object to be updated
	 * @param node
	 *          xml node from hich data are taken
	 */
	public abstract void updateColumnFromNode(T object, Node node);

	/**
	 * @return the parentParser
	 */
	ColumnParser<ReactomeDatabaseObject> getParentParser() {
		return parentParser;
	}

	/**
	 * @param parentParser
	 *          the parentParser to set
	 */
	void setParentParser(ColumnParser<ReactomeDatabaseObject> parentParser) {
		this.parentParser = parentParser;
	}

	/**
	 * Standard reactome DatabaseIdentifier parser.
	 */
	private ReactomeDatabaseIdentifierParser						diParser				= null;
	/**
	 * Standard reactome DatabaseIdentifier parser.
	 */
	private ReactomeAffiliationParser										aParser					= null;
	/**
	 * Standard reactome EntityCompartment parser.
	 */
	private ReactomeEntityCompartmentParser							ecParser				= null;
	/**
	 * Standard reactome EvidenceType parser.
	 */
	private ReactomeEvidenceTypeParser									etParser				= null;
	/**
	 * Standard reactome Event parser.
	 */
	private ReactomeEventParser													eParser					= null;
	/**
	 * Standard reactome Summation parser.
	 */
	private ReactomeSummationParser											suParser				= null;
	/**
	 * Standard reactome Species parser.
	 */
	private ReactomeSpeciesParser												sParser					= null;
	/**
	 * Standard reactome ReferenceEntity parser.
	 */
	private ReactomeReferenceEntityParser								reParser				= null;
	/**
	 * Standard reactome PhysicalEntity parser.
	 */
	private ReactomePhysicalEntityParser								peParser				= null;
	/**
	 * Standard reactome CatalystActivity parser.
	 */
	private ReactomeCatalystActivityParser							caParser				= null;
	/**
	 * Standard reactome InstanceEdit parser.
	 */
	private ReactomeInstanceEditParser									ieParser				= null;
	/**
	 * Standard reactome StableIdentifier parser.
	 */
	private ReactomeStableIdentifierParser							siParser				= null;
	/**
	 * Standard reactome GoMolecularFunction parser.
	 */
	private ReactomeGoMolecularFunctionParser						gmfParser				= null;
	/**
	 * Standard reactome GoBiologicalProcess parser.
	 */
	private ReactomeGoBiologicalProcessParser						gbpParser				= null;
	/**
	 * Standard reactome Taxon parser.
	 */
	private ReactomeTaxonParser													tParser					= null;
	/**
	 * Standard reactome Person parser.
	 */
	private ReactomePersonParser												personParser		= null;
	/**
	 * Standard reactome ReferenceMolecule parser.
	 */
	private ReactomeReferenceMoleculeParser							rmParser				= null;
	/**
	 * Standard reactome ReferenceDatabase parser.
	 */
	private ReactomeReferenceDatabaseParser							rdParser				= null;
	/**
	 * Standard reactome Publication parser.
	 */
	private ReactomePublicationParser										pubParser				= null;
	/**
	 * Standard reactome AbstractModifiedResidue parser.
	 */
	private ReactomeAbstractModifiedResidueParser				amrParser				= null;
	/**
	 * Standard reactome ReferenceDNASequence parser.
	 */
	private ReactomeReferenceDNASequenceParser					rdsParser				= null;
	/**
	 * Standard reactome ReferenceRNASequence parser.
	 */
	private ReactomeReferenceRNASequenceParser					rrsParser				= null;
	/**
	 * Standard reactome Reaction parser.
	 */
	private ReactomeReactionParser											rParser					= null;
	/**
	 * Standard reactome Figure parser.
	 */
	private ReactomeFigureParser												fParser					= null;
	/**
	 * Standard reactome CandidateSet parser.
	 */
	private ReactomeCandidateSetParser									csParser				= null;
	/**
	 * Standard reactome GoCellularComponent parser.
	 */
	private ReactomeGoCellularComponentParser						gccParser				= null;
	/**
	 * Standard reactome ReferenceGeneProduct parser.
	 */
	private ReactomeReferenceGeneProductParser					rgpParser				= null;
	/**
	 * Standard reactome Disease parser.
	 */
	private ReactomeDiseaseParser												dParser					= null;
	/**
	 * Standard reactome EntityFunctionalStatus parser.
	 */
	private ReactomeEntityFunctionalStatusParser				efsParser				= null;
	/**
	 * Standard reactome ReactionlikeEvent parser.
	 */
	private ReactomeReactionlikeEventParser							rleParser				= null;
	/**
	 * Standard reactome DatabaseObject parser.
	 */
	private ReactomeDatabaseObjectParser								doParser				= null;
	/**
	 * Standard reactome ReferenceSequence parser.
	 */
	private ReactomeReferenceSequenceParser							rsParser				= null;
	/**
	 * Standard reactome PsiMod parser.
	 */
	private ReactomePsiModParser												pmParser				= null;
	/**
	 * Standard reactome InterChainCrosslinkedResidue parser.
	 */
	private ReactomeInterChainCrosslinkedResidueParser	interCcrParser	= null;
	/**
	 * Standard reactome IntraChainCrosslinkedResidue parser.
	 */
	private ReactomeIntraChainCrosslinkedResidueParser	intraCcrParser	= null;
	/**
	 * Standard reactome FunctionalStatus parser.
	 */
	private ReactomeFunctionalStatusParser							fsParser				= null;
	/**
	 * Standard reactome Pathway parser.
	 */
	private ReactomePathwayParser												pParser					= null;

	/**
	 * 
	 * @return instance of ReactomeEntityCompartmentParser
	 */
	protected ReactomeEntityCompartmentParser getEcParser() {
		if (ecParser == null) {
			ecParser = (ReactomeEntityCompartmentParser) ReactomeParserFactory.getParserForClass(ReactomeEntityCompartment.class);
		}
		return ecParser;
	}

	/**
	 * 
	 * @return instance of ReactomeEvidenceTypeParser
	 */
	protected ReactomeEvidenceTypeParser getEtParser() {
		if (etParser == null) {
			etParser = (ReactomeEvidenceTypeParser) ReactomeParserFactory.getParserForClass(ReactomeEvidenceType.class);
		}
		return etParser;
	}

	/**
	 * 
	 * @return instance of ReactomeEventParser
	 */
	protected ReactomeEventParser geteParser() {
		if (eParser == null) {
			eParser = (ReactomeEventParser) ReactomeParserFactory.getParserForClass(ReactomeEvent.class);
		}
		return eParser;
	}

	/**
	 * 
	 * @return instance of ReactomeSpeciesParser
	 */
	protected ReactomeSpeciesParser getsParser() {
		if (sParser == null) {
			sParser = (ReactomeSpeciesParser) ReactomeParserFactory.getParserForClass(ReactomeSpecies.class);
		}
		return sParser;
	}

	/**
	 * 
	 * @return instance of ReactomeSummationParser
	 */
	protected ReactomeSummationParser getSuParser() {
		if (suParser == null) {
			suParser = (ReactomeSummationParser) ReactomeParserFactory.getParserForClass(ReactomeSummation.class);
		}
		return suParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceEntityParser
	 */
	protected ReactomeReferenceEntityParser getReParser() {
		if (reParser == null) {
			reParser = (ReactomeReferenceEntityParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceEntity.class);
		}
		return reParser;
	}

	/**
	 * 
	 * @return instance of ReactomePhysicalEntityParser
	 */
	protected ReactomePhysicalEntityParser getPeParser() {
		if (peParser == null) {
			peParser = (ReactomePhysicalEntityParser) ReactomeParserFactory.getParserForClass(ReactomePhysicalEntity.class);
		}
		return peParser;
	}

	/**
	 * 
	 * @return instance of ReactomeCatalystActivityParser
	 */
	protected ReactomeCatalystActivityParser getCaParser() {
		if (caParser == null) {
			caParser = (ReactomeCatalystActivityParser) ReactomeParserFactory.getParserForClass(ReactomeCatalystActivity.class);
		}
		return caParser;
	}

	/**
	 * 
	 * @return instance of ReactomeInstanceEditParser
	 */
	protected ReactomeInstanceEditParser getIeParser() {
		if (ieParser == null) {
			ieParser = (ReactomeInstanceEditParser) ReactomeParserFactory.getParserForClass(ReactomeInstanceEdit.class);
		}
		return ieParser;
	}

	/**
	 * 
	 * @return instance of ReactomeStableIdentifierParser
	 */
	protected ReactomeStableIdentifierParser getSiParser() {
		if (siParser == null) {
			siParser = (ReactomeStableIdentifierParser) ReactomeParserFactory.getParserForClass(ReactomeStableIdentifier.class);
		}
		return siParser;
	}

	/**
	 * 
	 * @return instance of ReactomeGoMolecularFunctionParser
	 */
	protected ReactomeGoMolecularFunctionParser getGmfParser() {
		if (gmfParser == null) {
			gmfParser = (ReactomeGoMolecularFunctionParser) ReactomeParserFactory.getParserForClass(ReactomeGoMolecularFunction.class);
		}
		return gmfParser;
	}

	/**
	 * 
	 * @return instance of ReactomeDatabaseIdentifierParser
	 */
	protected ReactomeDatabaseIdentifierParser getDiParser() {
		if (diParser == null) {
			diParser = (ReactomeDatabaseIdentifierParser) ReactomeParserFactory.getParserForClass(ReactomeDatabaseIdentifier.class);
		}
		return diParser;
	}

	/**
	 * 
	 * @return instance of {@link ReactomeAffiliationParser}
	 */
	protected ReactomeAffiliationParser getAParser() {
		if (aParser == null) {
			aParser = (ReactomeAffiliationParser) ReactomeParserFactory.getParserForClass(ReactomeAffiliation.class);
		}
		return aParser;
	}

	/**
	 * 
	 * @return instance of ReactomeTaxonParser
	 */
	protected ReactomeTaxonParser gettParser() {
		if (tParser == null) {
			tParser = (ReactomeTaxonParser) ReactomeParserFactory.getParserForClass(ReactomeTaxon.class);
		}
		return tParser;
	}

	/**
	 * 
	 * @return instance of ReactomePersonParser
	 */
	protected ReactomePersonParser getPersonParser() {
		if (personParser == null) {
			personParser = (ReactomePersonParser) ReactomeParserFactory.getParserForClass(ReactomePerson.class);
		}
		return personParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceMoleculeParser
	 */
	protected ReactomeReferenceMoleculeParser getRmParser() {
		if (rmParser == null) {
			rmParser = (ReactomeReferenceMoleculeParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceMolecule.class);
		}
		return rmParser;
	}

	/**
	 * 
	 * @return instance of ReactomePublicationParser
	 */
	protected ReactomePublicationParser getPubParser() {
		if (pubParser == null) {
			pubParser = (ReactomePublicationParser) ReactomeParserFactory.getParserForClass(ReactomePublication.class);
		}
		return pubParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceDatabaseParser
	 */
	protected ReactomeReferenceDatabaseParser getRdParser() {
		if (rdParser == null) {
			rdParser = (ReactomeReferenceDatabaseParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceDatabase.class);
		}
		return rdParser;
	}

	/**
	 * 
	 * @return instance of ReactomeAbstractModifiedResidueParser
	 */
	protected ReactomeAbstractModifiedResidueParser getAmrParser() {
		if (amrParser == null) {
			amrParser = (ReactomeAbstractModifiedResidueParser) ReactomeParserFactory.getParserForClass(ReactomeAbstractModifiedResidue.class);
		}
		return amrParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceDNASequenceParser
	 */
	protected ReactomeReferenceDNASequenceParser getRdsParser() {
		if (rdsParser == null) {
			rdsParser = (ReactomeReferenceDNASequenceParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceDNASequence.class);
		}
		return rdsParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceRNASequenceParser
	 */
	protected ReactomeReferenceRNASequenceParser getRrsParser() {
		if (rrsParser == null) {
			rrsParser = (ReactomeReferenceRNASequenceParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceRNASequence.class);
		}
		return rrsParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReactionParser
	 */
	protected ReactomeReactionParser getrParser() {
		if (rParser == null) {
			rParser = (ReactomeReactionParser) ReactomeParserFactory.getParserForClass(ReactomeReaction.class);
		}
		return rParser;
	}

	/**
	 * 
	 * @return instance of ReactomeFigureParser
	 */
	protected ReactomeFigureParser getfParser() {
		if (fParser == null) {
			fParser = (ReactomeFigureParser) ReactomeParserFactory.getParserForClass(ReactomeFigure.class);
		}
		return fParser;
	}

	/**
	 * 
	 * @return instance of ReactomeGoBiologicalProcessParser
	 */
	protected ReactomeGoBiologicalProcessParser getGbpParser() {
		if (gbpParser == null) {
			gbpParser = (ReactomeGoBiologicalProcessParser) ReactomeParserFactory.getParserForClass(ReactomeGoBiologicalProcess.class);
		}
		return gbpParser;
	}

	/**
	 * 
	 * @return instance of ReactomeCandidateSetParser
	 */
	protected ReactomeCandidateSetParser getCsParser() {
		if (csParser == null) {
			csParser = (ReactomeCandidateSetParser) ReactomeParserFactory.getParserForClass(ReactomeCandidateSet.class);
		}
		return csParser;
	}

	/**
	 * 
	 * @return instance of ReactomeGoCellularComponentParser
	 */
	protected ReactomeGoCellularComponentParser getGccParser() {
		if (gccParser == null) {
			gccParser = (ReactomeGoCellularComponentParser) ReactomeParserFactory.getParserForClass(ReactomeGoCellularComponent.class);
		}
		return gccParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceGeneProductParser
	 */
	protected ReactomeReferenceGeneProductParser getRgpParser() {
		if (rgpParser == null) {
			rgpParser = (ReactomeReferenceGeneProductParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceGeneProduct.class);
		}
		return rgpParser;
	}

	/**
	 * 
	 * @return instance of ReactomeDiseaseParser
	 */
	protected ReactomeDiseaseParser getdParser() {
		if (dParser == null) {
			dParser = (ReactomeDiseaseParser) ReactomeParserFactory.getParserForClass(ReactomeDisease.class);
		}
		return dParser;
	}

	/**
	 * 
	 * @return instance of ReactomeEntityFunctionalStatusParser
	 */
	protected ReactomeEntityFunctionalStatusParser getEfsParser() {
		if (efsParser == null) {
			efsParser = (ReactomeEntityFunctionalStatusParser) ReactomeParserFactory.getParserForClass(ReactomeEntityFunctionalStatus.class);
		}
		return efsParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReactionlikeEventParser
	 */
	protected ReactomeReactionlikeEventParser getRleParser() {
		if (rleParser == null) {
			rleParser = (ReactomeReactionlikeEventParser) ReactomeParserFactory.getParserForClass(ReactomeReactionlikeEvent.class);
		}
		return rleParser;
	}

	/**
	 * 
	 * @return instance of ReactomeDatabaseObjectParser
	 */
	protected ReactomeDatabaseObjectParser getDoParser() {
		if (doParser == null) {
			doParser = (ReactomeDatabaseObjectParser) ReactomeParserFactory.getParserForClass(ReactomeDatabaseObject.class);
		}
		return doParser;
	}

	/**
	 * 
	 * @return instance of ReactomeReferenceSequenceParser
	 */
	protected ReactomeReferenceSequenceParser getRsParser() {
		if (rsParser == null) {
			rsParser = (ReactomeReferenceSequenceParser) ReactomeParserFactory.getParserForClass(ReactomeReferenceSequence.class);
		}
		return rsParser;
	}

	/**
	 * 
	 * @return instance of ReactomePsiModParser
	 */
	protected ReactomePsiModParser getPmParser() {
		if (pmParser == null) {
			pmParser = (ReactomePsiModParser) ReactomeParserFactory.getParserForClass(ReactomePsiMod.class);
		}
		return pmParser;
	}

	/**
	 * 
	 * @return instance of ReactomeInterChainCrosslinkedResidueParser
	 */
	protected ReactomeInterChainCrosslinkedResidueParser getInterCcrParser() {
		if (interCcrParser == null) {
			interCcrParser = (ReactomeInterChainCrosslinkedResidueParser) ReactomeParserFactory.getParserForClass(ReactomeInterChainCrosslinkedResidue.class);
		}
		return interCcrParser;
	}

	/**
	 * 
	 * @return instance of ReactomeIntraChainCrosslinkedResidueParser
	 */
	protected ReactomeIntraChainCrosslinkedResidueParser getIntraCcrParser() {
		if (intraCcrParser == null) {
			intraCcrParser = (ReactomeIntraChainCrosslinkedResidueParser) ReactomeParserFactory.getParserForClass(ReactomeIntraChainCrosslinkedResidue.class);
		}
		return intraCcrParser;
	}

	/**
	 * 
	 * @return instance of ReactomeFunctionalStatusParser
	 */
	protected ReactomeFunctionalStatusParser getFsParser() {
		if (fsParser == null) {
			fsParser = (ReactomeFunctionalStatusParser) ReactomeParserFactory.getParserForClass(ReactomeFunctionalStatus.class);
		}
		return fsParser;
	}

	/**
	 * 
	 * @return instance of ReactomePathwayParser
	 */
	protected ReactomePathwayParser getpParser() {
		if (pParser == null) {
			pParser = (ReactomePathwayParser) ReactomeParserFactory.getParserForClass(ReactomePathway.class);
		}
		return pParser;
	}

}
