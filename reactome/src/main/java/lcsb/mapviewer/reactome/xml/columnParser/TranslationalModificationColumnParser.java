package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeTranslationalModification;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=TranslationalModification"
 * >TranslationalModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class TranslationalModificationColumnParser extends ColumnParser<ReactomeTranslationalModification> {
	/**
	 * Default constructor. Defines parser for super class of TranslationalModification.
	 */
	protected TranslationalModificationColumnParser() {
		super(AbstractModifiedResidueColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeTranslationalModification object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("coordinate")) {
			object.setCoordinate(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("psiMod")) {
			object.setPsiMod(getPmParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
