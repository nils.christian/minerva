package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeSummation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Summation"
 * >Summation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SummationColumnParser extends ColumnParser<ReactomeSummation> {
	/**
	 * Default constructor. Defines parser for super class of Summation.
	 */
	protected SummationColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeSummation object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("text")) {
			object.setText(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("literatureReference")) {
			object.addLiteratureReferences(getPubParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
