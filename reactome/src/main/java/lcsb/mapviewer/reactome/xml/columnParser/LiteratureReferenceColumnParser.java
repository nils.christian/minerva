package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeLiteratureReference;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=LiteratureReference"
 * >LiteratureReference</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class LiteratureReferenceColumnParser extends ColumnParser<ReactomeLiteratureReference> {
	/**
	 * Default constructor. Defines parser for super class of LiteratureReference.
	 */
	protected LiteratureReferenceColumnParser() {
		super(PublicationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeLiteratureReference object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("journal")) {
			object.setJournal(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("pages")) {
			object.setPages(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("volume")) {
			object.setVolume(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("pubMedIdentifier")) {
			object.setPubMedIdentifier(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("year")) {
			object.setYear(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
