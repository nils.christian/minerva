package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeIntraChainCrosslinkedResidue;
import lcsb.mapviewer.reactome.xml.columnParser.IntraChainCrosslinkedResidueColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=IntraChainCrosslinkedResidue"
 * >IntraChainCrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeIntraChainCrosslinkedResidueParser extends ReactomeNodeParser<ReactomeIntraChainCrosslinkedResidue> {

	/**
	 * Default constructor.
	 */
	public ReactomeIntraChainCrosslinkedResidueParser() {
		super(ReactomeIntraChainCrosslinkedResidue.class, IntraChainCrosslinkedResidueColumnParser.class);
	}
}
