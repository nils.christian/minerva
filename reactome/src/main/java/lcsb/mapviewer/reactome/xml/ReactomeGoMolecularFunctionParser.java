package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeGoMolecularFunction;
import lcsb.mapviewer.reactome.xml.columnParser.GoMolecularFunctionColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_MolecularFunction"
 * >GoMolecularFunction</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGoMolecularFunctionParser extends ReactomeNodeParser<ReactomeGoMolecularFunction> {

	/**
	 * Default constructor.
	 */
	public ReactomeGoMolecularFunctionParser()   {
		super(ReactomeGoMolecularFunction.class, GoMolecularFunctionColumnParser.class);
	}
}
