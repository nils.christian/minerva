package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeCellType;
import lcsb.mapviewer.reactome.xml.columnParser.CellTypeColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CellType"
 * >CellType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCellTypeParser extends ReactomeNodeParser<ReactomeCellType> {

	/**
	 * Default constructor.
	 */
	public ReactomeCellTypeParser() {
		super(ReactomeCellType.class, CellTypeColumnParser.class);
	}
}
