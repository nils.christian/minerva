package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGroupModifiedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GroupModifiedResidue"
 * >GroupModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GroupModifiedResidueColumnParser extends ColumnParser<ReactomeGroupModifiedResidue> {
	/**
	 * Default constructor. Defines parser for super class of GroupModifiedResidue.
	 */
	protected GroupModifiedResidueColumnParser() {
		super(TranslationalModificationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGroupModifiedResidue object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("modification")) {
			object.setModification(getDoParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
