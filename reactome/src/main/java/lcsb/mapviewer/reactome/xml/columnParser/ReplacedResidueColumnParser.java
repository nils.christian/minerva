package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReplacedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReplacedResidue"
 * >ReplacedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReplacedResidueColumnParser extends ColumnParser<ReactomeReplacedResidue> {
	/**
	 * Default constructor. Defines parser for super class of ReplacedResidue.
	 */
	protected ReplacedResidueColumnParser() {
		super(GeneticallyModifiedResidueColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReplacedResidue object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("coordinate")) {
			object.setCoordinate(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("psiMod")) {
			object.setPsiMod(getPmParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
