package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeTaxon;
import lcsb.mapviewer.reactome.xml.columnParser.TaxonColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Taxon"
 * >Taxon</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeTaxonParser extends ReactomeNodeParser<ReactomeTaxon> {

	/**
	 * Default constructor.
	 */
	public ReactomeTaxonParser() {
		super(ReactomeTaxon.class, TaxonColumnParser.class);
	}
}
