package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePositiveRegulation;
import lcsb.mapviewer.reactome.xml.columnParser.PositiveRegulationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PositiveRegulation"
 * >PositiveRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePositiveRegulationParser extends ReactomeNodeParser<ReactomePositiveRegulation> {

	/**
	 * Default constructor.
	 */
	public ReactomePositiveRegulationParser()   {
		super(ReactomePositiveRegulation.class, PositiveRegulationColumnParser.class);
	}
}
