package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeGoBiologicalProcess;
import lcsb.mapviewer.reactome.xml.columnParser.GoBiologicalProcessColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_BiologicalProcess"
 * >GoBiologicalProcess</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeGoBiologicalProcessParser extends ReactomeNodeParser<ReactomeGoBiologicalProcess> {

	/**
	 * Default constructor.
	 */
	public ReactomeGoBiologicalProcessParser()   {
		super(ReactomeGoBiologicalProcess.class, GoBiologicalProcessColumnParser.class);
	}
}
