package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFragmentDeletionModification;
import lcsb.mapviewer.reactome.xml.columnParser.FragmentDeletionModificationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentDeletionModification"
 * >FragmentDeletionModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFragmentDeletionModificationParser extends ReactomeNodeParser<ReactomeFragmentDeletionModification> {

	/**
	 * Default constructor.
	 */
	public ReactomeFragmentDeletionModificationParser() {
		super(ReactomeFragmentDeletionModification.class, FragmentDeletionModificationColumnParser.class);
	}
}
