package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeComplex;
import lcsb.mapviewer.reactome.xml.columnParser.ComplexColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Complex"
 * >Complex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeComplexParser extends ReactomeNodeParser<ReactomeComplex> {

	/**
	 * Default constructor.
	 */
	public ReactomeComplexParser() {
		super(ReactomeComplex.class, ComplexColumnParser.class);
	}
}
