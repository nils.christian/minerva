package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeCrosslinkedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CrosslinkedResidue"
 * >CrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class CrosslinkedResidueColumnParser extends ColumnParser<ReactomeCrosslinkedResidue> {
	/**
	 * Default constructor. Defines parser for super class of CrosslinkedResidue.
	 */
	protected CrosslinkedResidueColumnParser() {
		super(TranslationalModificationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeCrosslinkedResidue object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("modification")) {
			object.addModification(getDoParser().parseObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("secondCoordinate")) {
			object.addSecondCoordinate(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
