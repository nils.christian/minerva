package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceIsoform;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceIsoproductColumnParser;


/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceIsoform"
 * >ReferenceIsoform</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceIsoformParser extends ReactomeNodeParser<ReactomeReferenceIsoform> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceIsoformParser()   {
		super(ReactomeReferenceIsoform.class, ReferenceIsoproductColumnParser.class);
	}
}
