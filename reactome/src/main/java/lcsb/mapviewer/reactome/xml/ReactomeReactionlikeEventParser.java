package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.xml.columnParser.ReactionlikeEventColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionlikeEvent"
 * >ReactionlikeEvent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReactionlikeEventParser extends ReactomeNodeParser<ReactomeReactionlikeEvent> {

	/**
	 * Default constructor.
	 */
	public ReactomeReactionlikeEventParser()   {
		super(ReactomeReactionlikeEvent.class, ReactionlikeEventColumnParser.class);
	}
}
