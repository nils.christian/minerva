package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeCellType;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CellType"
 * >CellType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellTypeColumnParser extends ColumnParser<ReactomeCellType> {
	/**
	 * Default constructor. Defines parser for super class of CellType.
	 */
	protected CellTypeColumnParser() {
		super(ExternalOntologyColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeCellType object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
