package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence;
import lcsb.mapviewer.reactome.xml.columnParser.EntityWithAccessionedSequenceColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityWithAccessionedSequence"
 * >EntityWithAccessionedSequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntityWithAccessionedSequenceParser extends ReactomeNodeParser<ReactomeEntityWithAccessionedSequence> {

	/**
	 * Default constructor.
	 */
	public ReactomeEntityWithAccessionedSequenceParser() {
		super(ReactomeEntityWithAccessionedSequence.class, EntityWithAccessionedSequenceColumnParser.class);
	}
}
