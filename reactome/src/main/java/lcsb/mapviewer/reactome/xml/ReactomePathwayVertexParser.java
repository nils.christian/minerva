package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePathwayVertex;
import lcsb.mapviewer.reactome.xml.columnParser.PathwayVertexColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayVertex"
 * >PathwayVertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePathwayVertexParser extends ReactomeNodeParser<ReactomePathwayVertex> {

	/**
	 * Default constructor.
	 */
	public ReactomePathwayVertexParser()   {
		super(ReactomePathwayVertex.class, PathwayVertexColumnParser.class);
	}
}
