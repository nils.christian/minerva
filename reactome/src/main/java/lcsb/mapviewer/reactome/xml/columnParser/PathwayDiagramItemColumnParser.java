package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePathwayDiagramItem;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayDiagramItem"
 * >PathwayDiagramItem</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PathwayDiagramItemColumnParser extends ColumnParser<ReactomePathwayDiagramItem> {
	/**
	 * Default constructor. Defines parser for super class of PathwayDiagramItem.
	 */
	protected PathwayDiagramItemColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePathwayDiagramItem object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
