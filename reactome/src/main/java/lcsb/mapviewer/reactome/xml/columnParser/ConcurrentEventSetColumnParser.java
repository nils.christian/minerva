package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeConcurrentEventSet;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ConcurrentEventSet"
 * >ConcurrentEventSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ConcurrentEventSetColumnParser extends ColumnParser<ReactomeConcurrentEventSet> {
	/**
	 * Default constructor. Defines parser for super class of ConcurrentEventSet.
	 */
	protected ConcurrentEventSetColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeConcurrentEventSet object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
