package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeIntraChainCrosslinkedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=IntraChainCrosslinkedResidue"
 * >IntraChainCrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class IntraChainCrosslinkedResidueColumnParser extends ColumnParser<ReactomeIntraChainCrosslinkedResidue> {
	/**
	 * Default constructor. Defines parser for super class of IntraChainCrosslinkedResidue.
	 */
	protected IntraChainCrosslinkedResidueColumnParser() {
		super(CrosslinkedResidueColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeIntraChainCrosslinkedResidue object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
