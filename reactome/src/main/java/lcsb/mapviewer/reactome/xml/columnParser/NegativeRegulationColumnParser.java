package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeNegativeRegulation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=NegativeRegulation"
 * >NegativeRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class NegativeRegulationColumnParser extends ColumnParser<ReactomeNegativeRegulation> {
	/**
	 * Default constructor. Defines parser for super class of NegativeRegulation.
	 */
	protected NegativeRegulationColumnParser() {
		super(RegulationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeNegativeRegulation object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
