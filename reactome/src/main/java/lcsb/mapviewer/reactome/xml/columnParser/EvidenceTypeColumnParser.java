package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeEvidenceType;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EvidenceType"
 * >EvidenceType</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EvidenceTypeColumnParser extends ColumnParser<ReactomeEvidenceType> {
	/**
	 * Default constructor. Defines parser for super class of EvidenceType.
	 */
	protected EvidenceTypeColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEvidenceType object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
