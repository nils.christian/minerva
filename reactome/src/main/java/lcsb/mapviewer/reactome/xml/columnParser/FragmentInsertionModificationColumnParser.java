package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFragmentInsertionModification;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentInsertionModification"
 * >FragmentInsertionModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FragmentInsertionModificationColumnParser extends ColumnParser<ReactomeFragmentInsertionModification> {
	/**
	 * Default constructor. Defines parser for super class of FragmentInsertionModification.
	 */
	protected FragmentInsertionModificationColumnParser() {
		super(FragmentModificationColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFragmentInsertionModification object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("coordinate")) {
			object.setCoordinate(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
