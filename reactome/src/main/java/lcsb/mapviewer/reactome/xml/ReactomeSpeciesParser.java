package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeSpecies;
import lcsb.mapviewer.reactome.xml.columnParser.SpeciesColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Species"
 * >Species</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSpeciesParser extends ReactomeNodeParser<ReactomeSpecies> {

	/**
	 * Default constructor.
	 */
	public ReactomeSpeciesParser()   {
		super(ReactomeSpecies.class, SpeciesColumnParser.class);
	}
}
