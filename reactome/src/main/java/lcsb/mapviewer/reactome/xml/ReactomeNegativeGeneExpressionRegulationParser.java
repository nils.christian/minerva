package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeNegativeGeneExpressionRegulation;
import lcsb.mapviewer.reactome.xml.columnParser.NegativeGeneExpressionRegulationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=NegativeGeneExpressionRegulation"
 * >NegativeGeneExpressionRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeNegativeGeneExpressionRegulationParser extends ReactomeNodeParser<ReactomeNegativeGeneExpressionRegulation> {

	/**
	 * Default constructor.
	 */
	public ReactomeNegativeGeneExpressionRegulationParser()   {
		super(ReactomeNegativeGeneExpressionRegulation.class, NegativeGeneExpressionRegulationColumnParser.class);
	}
}
