package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceGeneProductColumnParser;


/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceGeneProduct"
 * >ReferenceGeneProduct</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceGeneProductParser extends ReactomeNodeParser<ReactomeReferenceGeneProduct> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceGeneProductParser()   {
		super(ReactomeReferenceGeneProduct.class, ReferenceGeneProductColumnParser.class);
	}
}
