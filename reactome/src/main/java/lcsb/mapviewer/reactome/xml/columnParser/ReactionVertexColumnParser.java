package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReactionVertex;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionVertex"
 * >ReactionVertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionVertexColumnParser extends ColumnParser<ReactomeReactionVertex> {
	/**
	 * Default constructor. Defines parser for super class of ReactionVertex.
	 */
	protected ReactionVertexColumnParser() {
		super(VertexColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReactionVertex object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("pointCoordinates")) {
			object.setPointCoordinates(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
