package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeVertex;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Vertex"
 * >Vertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class VertexColumnParser extends ColumnParser<ReactomeVertex> {
	/**
	 * Default constructor. Defines parser for super class of Vertex.
	 */
	protected VertexColumnParser() {
		super(PathwayDiagramItemColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeVertex object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("x")) {
			object.setX(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("y")) {
			object.setY(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("width")) {
			object.setWidth(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("height")) {
			object.setHeight(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
