package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFragmentReplacedModification;
import lcsb.mapviewer.reactome.xml.columnParser.FragmentReplacedModificationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentReplacedModification"
 * >FragmentReplacedModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFragmentReplacedModificationParser extends ReactomeNodeParser<ReactomeFragmentReplacedModification> {

	/**
	 * Default constructor.
	 */
	public ReactomeFragmentReplacedModificationParser()   {
		super(ReactomeFragmentReplacedModification.class, FragmentReplacedModificationColumnParser.class);
	}
}
