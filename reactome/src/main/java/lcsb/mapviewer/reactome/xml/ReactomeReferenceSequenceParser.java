package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceSequence;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceSequenceColumnParser;


/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceSequence"
 * >ReferenceSequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceSequenceParser extends ReactomeNodeParser<ReactomeReferenceSequence> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceSequenceParser()   {
		super(ReactomeReferenceSequence.class, ReferenceSequenceColumnParser.class);
	}
}
