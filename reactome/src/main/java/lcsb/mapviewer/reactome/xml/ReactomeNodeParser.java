package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.reactome.ReactomeDatabaseProxy;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeStatus;
import lcsb.mapviewer.reactome.xml.columnParser.ColumnParser;
import lcsb.mapviewer.reactome.xml.columnParser.ColumnParserFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Generic parser for reactome objects.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 */
@Transactional(value = "txManager")
public abstract class ReactomeNodeParser<T extends ReactomeDatabaseObject> {
	/**
	 * Default class logger.
	 */
	private Logger								logger	= Logger.getLogger(ReactomeNodeParser.class);
	/**
	 * Class for which this parser was created.
	 */
	private Class<T>							clazz;
	/**
	 * PArser used for parsing columns in the xml representation of the object.
	 */
	private ColumnParser<T>				columnParser;
	/**
	 * Proxy used for generating objects that will be fetched on demand from
	 * reactome database.
	 */
	@Autowired
	private ReactomeDatabaseProxy	databaseProxy;

	/**
	 * Default constructor that initializes {@link #columnParser}.
	 * 
	 * @param clazz
	 *          class type for which parser is created
	 * @param columnParserClazz
	 *          {@link ColumnParser} class used for parsing xml data
	 */
	@SuppressWarnings("unchecked")
	protected ReactomeNodeParser(Class<T> clazz, Class<?> columnParserClazz) {
		this.clazz = clazz;
		this.columnParser = (ColumnParser<T>) ColumnParserFactory.getColumnParser(columnParserClazz);
		try {
			clazz.newInstance();
		} catch (InstantiationException e) {
			throw new InvalidArgumentException("Invalid class type. Cannot instatiate class to create new object.");
		} catch (IllegalAccessException e) {
			throw new InvalidArgumentException("Invalid class type. Cannot instatiate class to create new object.");
		}
	}

	/**
	 * Creates object from the xml node.
	 * 
	 * @param node
	 *          node to parse
	 * @return object representing xml node
	 */
	public T parseObject(Node node) {
		T result = getNewInstance();
		updateObjectFromNode(result, node);
		return result;
	}

	/**
	 * Private xml parser class helping in transformation of the xml to string.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	private class ToXmlParser extends XmlParser {
		@Override
		public String nodeToString(Node node) {
			return super.nodeToString(node);
		}

		@Override
		public String nodeToString(Node node, boolean includeHeader) {
			return super.nodeToString(node, includeHeader);
		}
	}

	/**
	 * Updates data in the object from the xml node.
	 * 
	 * @param object
	 *          object that should be updated
	 * @param node
	 *          xml node with the data
	 */
	public void updateObjectFromNode(T object, Node node) {
		object.setStatus(ReactomeStatus.FULL);
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node childNode = nodes.item(i);
			try {
				columnParser.updateColumnFromNode(object, childNode);
			} catch (InvalidArgumentException e) {
				ToXmlParser parser = new ToXmlParser();
				logger.error("Problematic node: " + parser.nodeToString(node, true));
				throw e;
			}
		}
	}

	/**
	 * Creates {@link ReactomeDatabaseObject} from xml node with basic
	 * informations.
	 * 
	 * @param node
	 *          xml node
	 * @return {@link ReactomeDatabaseObject} obtaine from xml
	 */
	public T parseSimplifiedObject(Node node) {
		T result = getNewInstance();
		result.setStatus(ReactomeStatus.ONLY_ID);
		Integer id = null;
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node childNode = nodes.item(i);
			if (childNode.getNodeName().equalsIgnoreCase("dbId")) {
				id = Integer.parseInt(childNode.getTextContent());
			}
		}
		if (id == null) {
			throw new InvalidArgumentException("Required dbId node not found");
		} else {
			result.setDbId(id);
		}
		return result;
	}

	/**
	 * Creates new instance of the object for which this parser was created.
	 * 
	 * @return new instance of the object for which this parser was created
	 */
	@SuppressWarnings("unchecked")
	private T getNewInstance() {
		T result = null;
		try {
			result = clazz.newInstance();
			result = (T) getDatabaseProxy().createProxyInstanceForClassType(clazz);
		} catch (InstantiationException e) {
			logger.fatal(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.fatal(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * @return the databaseProxy
	 * @see #databaseProxy
	 */
	public ReactomeDatabaseProxy getDatabaseProxy() {
		return databaseProxy;
	}

	/**
	 * @param databaseProxy
	 *          the databaseProxy to set
	 * @see #databaseProxy
	 */
	public void setDatabaseProxy(ReactomeDatabaseProxy databaseProxy) {
		this.databaseProxy = databaseProxy;
	}

}
