package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeCandidateSet;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=CandidateSet"
 * >CandidateSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class CandidateSetColumnParser extends ColumnParser<ReactomeCandidateSet> {
	/**
	 * Default constructor. Defines parser for super class of CandidateSet.
	 */
	protected CandidateSetColumnParser() {
		super(EntitySetColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeCandidateSet object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("hasCandidate")) {
			object.addHasCandidate(getPeParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
