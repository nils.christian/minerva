package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeCompartment;
import lcsb.mapviewer.reactome.xml.columnParser.CompartmentColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Compartment"
 * >Compartment</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeCompartmentParser extends ReactomeNodeParser<ReactomeCompartment> {

	/**
	 * Default constructor.
	 */
	public ReactomeCompartmentParser() {
		super(ReactomeCompartment.class, CompartmentColumnParser.class);
	}
}
