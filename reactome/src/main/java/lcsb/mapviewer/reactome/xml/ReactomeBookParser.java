package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeBook;
import lcsb.mapviewer.reactome.xml.columnParser.BookColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Book"
 * >Book</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeBookParser extends ReactomeNodeParser<ReactomeBook> {

	/**
	 * Default constructor.
	 */
	public ReactomeBookParser() {
		super(ReactomeBook.class, BookColumnParser.class);
	}
}
