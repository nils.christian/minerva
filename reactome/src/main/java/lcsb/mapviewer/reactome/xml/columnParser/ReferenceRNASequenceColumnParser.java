package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceRNASequence;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceRNASequence"
 * >ReferenceRNASequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceRNASequenceColumnParser extends ColumnParser<ReactomeReferenceRNASequence> {
	/**
	 * Default constructor. Defines parser for super class of ReferenceRNASequence.
	 */
	protected ReferenceRNASequenceColumnParser() {
		super(ReferenceSequenceColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceRNASequence object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
