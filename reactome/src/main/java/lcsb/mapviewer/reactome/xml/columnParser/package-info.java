/**
 * Provides column parser functionality for different reactome structures. Main
 * class from which all other classes inherit is
 * {@link lcsb.mapviewer.reactome.xml.columnParser.ColumnParser ColumnParser}.
 */
package lcsb.mapviewer.reactome.xml.columnParser;

