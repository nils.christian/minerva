package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeSequenceOntology;
import lcsb.mapviewer.reactome.xml.columnParser.SequenceOntologyColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SequenceOntology"
 * >SequenceOntology</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSequenceOntologyParser extends ReactomeNodeParser<ReactomeSequenceOntology> {

	/**
	 * Default constructor.
	 */
	public ReactomeSequenceOntologyParser()   {
		super(ReactomeSequenceOntology.class, SequenceOntologyColumnParser.class);
	}
}
