package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeBlackBoxEvent;
import lcsb.mapviewer.reactome.xml.columnParser.BlackBoxEventColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=BlackBoxEvent"
 * >BlackBoxEvent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeBlackBoxEventParser extends ReactomeNodeParser<ReactomeBlackBoxEvent> {

	/**
	 * Default constructor.
	 */
	public ReactomeBlackBoxEventParser() {
		super(ReactomeBlackBoxEvent.class, BlackBoxEventColumnParser.class);
	}
}
