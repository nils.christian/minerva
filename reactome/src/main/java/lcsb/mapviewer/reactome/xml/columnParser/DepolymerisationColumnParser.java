package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeDepolymerisation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Depolymerisation"
 * >Depolymerisation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DepolymerisationColumnParser extends ColumnParser<ReactomeDepolymerisation> {
	/**
	 * Default constructor. Defines parser for super class of Depolymerisation.
	 */
	protected DepolymerisationColumnParser() {
		super(ReactionlikeEventColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeDepolymerisation object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
