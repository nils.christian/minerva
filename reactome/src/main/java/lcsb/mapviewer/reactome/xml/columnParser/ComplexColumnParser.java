package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeComplex;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Complex"
 * >Complex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexColumnParser extends ColumnParser<ReactomeComplex> {
	/**
	 * Default constructor. Defines parser for super class of Complex.
	 */
	protected ComplexColumnParser() {
		super(PhysicalEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeComplex object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("hasComponent")) {
			object.addHasComponent(getPeParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("includedLocation")) {
			object.addIncludedLocation(getEcParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("isChimeric")) {
			object.setIsChimeric(node.getTextContent().equalsIgnoreCase("TRUE"));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
