package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeEntityFunctionalStatus;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityFunctionalStatus"
 * >EntityFunctionalStatus</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EntityFunctionalStatusColumnParser extends ColumnParser<ReactomeEntityFunctionalStatus> {
	/**
	 * Default constructor. Defines parser for super class of EntityFunctionalStatus.
	 */
	protected EntityFunctionalStatusColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEntityFunctionalStatus object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("functionalStatus")) {
			object.addFunctionalStatus(getFsParser().parseObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("physicalEntity")) {
			object.setPhysicalEntity(getPeParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
