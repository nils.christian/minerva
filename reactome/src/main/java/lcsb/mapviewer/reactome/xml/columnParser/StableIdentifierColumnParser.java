package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeStableIdentifier;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=StableIdentifier"
 * >StableIdentifier</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class StableIdentifierColumnParser extends ColumnParser<ReactomeStableIdentifier> {

	/**
	 * Default constructor. Defines parser for super class of StableIdentifier.
	 */
	protected StableIdentifierColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeStableIdentifier object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("identifier")) {
			object.setIdentifier(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("identifierVersion")) {
			object.setIdentifierVersion(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("oldIdentifier")) {
			object.setOldIdentifier(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("oldIdentifierVersion")) {
			object.setOldIdentifierVersion(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("released")) {
			object.setReleased("true".equalsIgnoreCase(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
