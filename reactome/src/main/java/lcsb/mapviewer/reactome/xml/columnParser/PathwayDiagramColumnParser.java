package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePathwayDiagram;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayDiagram"
 * >PathwayDiagram</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PathwayDiagramColumnParser extends ColumnParser<ReactomePathwayDiagram> {
	/**
	 * Default constructor. Defines parser for super class of PathwayDiagram.
	 */
	protected PathwayDiagramColumnParser() {
		super(PathwayDiagramItemColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePathwayDiagram object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("height")) {
			object.setHeight(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("width")) {
			object.setWidth(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("representedPathway")) {
			object.addRepresentedPathway(getpParser().parseObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("storedATXML")) {
			object.setStoredATXML(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
