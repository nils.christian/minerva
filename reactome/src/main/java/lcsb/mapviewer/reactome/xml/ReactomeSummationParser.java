package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeSummation;
import lcsb.mapviewer.reactome.xml.columnParser.SummationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Summation"
 * >Summation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSummationParser extends ReactomeNodeParser<ReactomeSummation> {

	/**
	 * Default constructor.
	 */
	public ReactomeSummationParser()   {
		super(ReactomeSummation.class, SummationColumnParser.class);
	}
}
