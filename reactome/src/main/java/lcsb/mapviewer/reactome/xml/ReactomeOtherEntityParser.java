package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeOtherEntity;
import lcsb.mapviewer.reactome.xml.columnParser.OtherEntityColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=OtherEntity"
 * >OtherEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeOtherEntityParser extends ReactomeNodeParser<ReactomeOtherEntity> {

	/**
	 * Default constructor.
	 */
	public ReactomeOtherEntityParser()   {
		super(ReactomeOtherEntity.class, OtherEntityColumnParser.class);
	}
}
