package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeCompartment;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Compartment"
 * >Compartment</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentColumnParser extends ColumnParser<ReactomeCompartment> {
	/**
	 * Default constructor. Defines parser for super class of XXXXXXXXXX.
	 */
	protected CompartmentColumnParser() {
		super(GoCellularComponentColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeCompartment object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
