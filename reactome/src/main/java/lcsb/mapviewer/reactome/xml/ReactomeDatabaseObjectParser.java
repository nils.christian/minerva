package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.xml.columnParser.DatabaseObjectColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=DatabaseObject"
 * >DatabaseObject</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeDatabaseObjectParser extends ReactomeNodeParser<ReactomeDatabaseObject> {

	/**
	 * Default constructor.
	 */
	public ReactomeDatabaseObjectParser() {
		super(ReactomeDatabaseObject.class, DatabaseObjectColumnParser.class);
	}
}
