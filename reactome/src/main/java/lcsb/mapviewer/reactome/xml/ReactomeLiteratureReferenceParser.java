package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeLiteratureReference;
import lcsb.mapviewer.reactome.xml.columnParser.LiteratureReferenceColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=LiteratureReference"
 * >LiteratureReference</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeLiteratureReferenceParser extends ReactomeNodeParser<ReactomeLiteratureReference> {

	/**
	 * Default constructor.
	 */
	public ReactomeLiteratureReferenceParser() {
		super(ReactomeLiteratureReference.class, LiteratureReferenceColumnParser.class);
	}
}
