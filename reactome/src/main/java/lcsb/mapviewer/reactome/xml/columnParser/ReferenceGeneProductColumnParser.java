package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceGeneProduct;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceGeneProduct"
 * >ReferenceGeneProduct</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceGeneProductColumnParser extends ColumnParser<ReactomeReferenceGeneProduct> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceGeneProduct.
	 */
	protected ReferenceGeneProductColumnParser() {
		super(ReferenceSequenceColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceGeneProduct object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("referenceGene")) {
			object.addReferenceGene(getRdsParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("referenceTranscript")) {
			object.addReferenceTranscript(getRrsParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
