package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePolymerisation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Polymerisation"
 * >Polymerisation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PolymerisationColumnParser extends ColumnParser<ReactomePolymerisation> {
	/**
	 * Default constructor. Defines parser for super class of Polymerisation.
	 */
	protected PolymerisationColumnParser() {
		super(ReactionlikeEventColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePolymerisation object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
