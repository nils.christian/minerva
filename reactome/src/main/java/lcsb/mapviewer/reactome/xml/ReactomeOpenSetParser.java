package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeOpenSet;
import lcsb.mapviewer.reactome.xml.columnParser.OpenSetColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=OpenSet"
 * >OpenSet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeOpenSetParser extends ReactomeNodeParser<ReactomeOpenSet> {

	/**
	 * Default constructor.
	 */
	public ReactomeOpenSetParser()   {
		super(ReactomeOpenSet.class, OpenSetColumnParser.class);
	}
}
