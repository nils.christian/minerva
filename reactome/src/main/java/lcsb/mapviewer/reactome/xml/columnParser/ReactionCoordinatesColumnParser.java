package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReactionCoordinates;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReactionCoordinates"
 * >ReactionCoordinates</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionCoordinatesColumnParser extends ColumnParser<ReactomeReactionCoordinates> {
	/**
	 * Default constructor. Defines parser for super class of ReactionCoordinates.
	 */
	protected ReactionCoordinatesColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReactionCoordinates object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("sourceX")) {
			object.setSourceX(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("sourceY")) {
			object.setSourceY(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("targetX")) {
			object.setTargetX(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("targetY")) {
			object.setTargetY(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
