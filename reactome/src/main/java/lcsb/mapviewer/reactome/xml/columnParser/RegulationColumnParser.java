package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeRegulation;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Regulation"
 * >Regulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class RegulationColumnParser extends ColumnParser<ReactomeRegulation> {
	/**
	 * Default constructor. Defines parser for super class of Regulation.
	 */
	protected RegulationColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeRegulation object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("regulatedEntity")) {
			object.setRegulatedEntity(geteParser().parseObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("regulator")) {
			object.setRegulator(getDoParser().parseObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("containedInPathway")) {
			object.setRegulator(getpParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
