package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePathwayVertex;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PathwayVertex"
 * >PathwayVertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PathwayVertexColumnParser extends ColumnParser<ReactomePathwayVertex> {
	/**
	 * Default constructor. Defines parser for super class of PathwayVertex.
	 */
	protected PathwayVertexColumnParser() {
		super(VertexColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePathwayVertex object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
