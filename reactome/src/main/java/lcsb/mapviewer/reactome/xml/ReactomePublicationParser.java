package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePublication;
import lcsb.mapviewer.reactome.xml.columnParser.PublicationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Publication"
 * >Publication</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePublicationParser extends ReactomeNodeParser<ReactomePublication> {

	/**
	 * Default constructor.
	 */
	public ReactomePublicationParser()   {
		super(ReactomePublication.class, PublicationColumnParser.class);
	}
}
