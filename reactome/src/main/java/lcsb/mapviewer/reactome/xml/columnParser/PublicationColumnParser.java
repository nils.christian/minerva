package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePublication;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Publication"
 * >Publication</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PublicationColumnParser extends ColumnParser<ReactomePublication> {
	/**
	 * Default constructor. Defines parser for super class of Publication.
	 */
	protected PublicationColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePublication object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("author")) {
			object.addAuthor(getPersonParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("title")) {
			object.setTitle(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
