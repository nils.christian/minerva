package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeEntityVertex;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntityVertex"
 * >EntityVertex</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class EntityVertexColumnParser extends ColumnParser<ReactomeEntityVertex> {
	/**
	 * Default constructor. Defines parser for super class of EntityVertex.
	 */
	protected EntityVertexColumnParser() {
		super(VertexColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeEntityVertex object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
