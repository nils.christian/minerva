package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePerson;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Person"
 * >Person</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PersonColumnParser extends ColumnParser<ReactomePerson> {
	/**
	 * Default constructor. Defines parser for super class of Person.
	 */
	protected PersonColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePerson object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("firstname")) {
			object.setFirstname(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("surname")) {
			object.setSurname(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("initial")) {
			object.setInitial(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("affiliation")) {
			object.setAffilation(getAParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("eMailAddress")) {
			object.seteMailAddress(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("figure")) {
			object.setFigure(getfParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("project")) {
			object.setProject(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("url")) {
			object.setUrl(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("crossReference")) {
			object.setCrosssReference(getDiParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}

	}

}
