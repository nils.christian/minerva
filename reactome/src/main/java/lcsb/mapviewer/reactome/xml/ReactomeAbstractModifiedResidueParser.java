package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeAbstractModifiedResidue;
import lcsb.mapviewer.reactome.xml.columnParser.AbstractModifiedResidueColumnParser;

/**
 * Parser for reactome xml retrieved from
 * <a href= "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html" >
 * Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=AbstractModifiedResidue"
 * >AbstractModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeAbstractModifiedResidueParser extends ReactomeNodeParser<ReactomeAbstractModifiedResidue> {

	/**
	 * Default constructor.
	 */
	public ReactomeAbstractModifiedResidueParser() {
		super(ReactomeAbstractModifiedResidue.class, AbstractModifiedResidueColumnParser.class);
	}
}
