package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceGroup;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceGroup"
 * >ReferenceGroup</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceGroupColumnParser extends ColumnParser<ReactomeReferenceGroup> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceGroup.
	 */
	protected ReferenceGroupColumnParser() {
		super(ReferenceEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceGroup object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
