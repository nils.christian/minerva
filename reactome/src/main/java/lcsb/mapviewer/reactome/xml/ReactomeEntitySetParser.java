package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeEntitySet;
import lcsb.mapviewer.reactome.xml.columnParser.EntitySetColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=EntitySet"
 * >EntitySet</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEntitySetParser extends ReactomeNodeParser<ReactomeEntitySet> {

	/**
	 * Default constructor.
	 */
	public ReactomeEntitySetParser() {
		super(ReactomeEntitySet.class, EntitySetColumnParser.class);
	}
}
