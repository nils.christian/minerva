package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeInstanceEdit;
import lcsb.mapviewer.reactome.xml.columnParser.InstanceEditColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=InstanceEdit"
 * >InstanceEdit</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeInstanceEditParser extends ReactomeNodeParser<ReactomeInstanceEdit> {

	/**
	 * Default constructor.
	 */
	public ReactomeInstanceEditParser()   {
		super(ReactomeInstanceEdit.class, InstanceEditColumnParser.class);
	}
}
