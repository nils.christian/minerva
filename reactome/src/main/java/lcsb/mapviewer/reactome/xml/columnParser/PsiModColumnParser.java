package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomePsiMod;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=PsiMod"
 * >PsiMod</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PsiModColumnParser extends ColumnParser<ReactomePsiMod> {
	/**
	 * Default constructor. Defines parser for super class of PsiMod.
	 */
	protected PsiModColumnParser() {
		super(ExternalOntologyColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomePsiMod object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
