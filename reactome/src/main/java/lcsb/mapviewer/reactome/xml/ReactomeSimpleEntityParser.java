package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeSimpleEntity;
import lcsb.mapviewer.reactome.xml.columnParser.SimpleEntityColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SimpleEntity"
 * >SimpleEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeSimpleEntityParser extends ReactomeNodeParser<ReactomeSimpleEntity> {

	/**
	 * Default constructor.
	 */
	public ReactomeSimpleEntityParser()   {
		super(ReactomeSimpleEntity.class, SimpleEntityColumnParser.class);
	}
}
