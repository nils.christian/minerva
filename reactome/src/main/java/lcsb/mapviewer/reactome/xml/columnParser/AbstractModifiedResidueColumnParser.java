package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeAbstractModifiedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=AbstractModifiedResidue"
 * >AbstractModifiedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class AbstractModifiedResidueColumnParser extends ColumnParser<ReactomeAbstractModifiedResidue> {
	/**
	 * Default constructor. It prevents to instatiate this parser from the outside
	 * of the package.
	 */
	protected AbstractModifiedResidueColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeAbstractModifiedResidue object, Node node) {
		// parse not inherited columns in this class
		if (node.getNodeName().equalsIgnoreCase("referenceSequence")) {
			object.setReferenceSequence(getRsParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
