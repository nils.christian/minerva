package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeURL;
import lcsb.mapviewer.reactome.xml.columnParser.URLColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=URL"
 * >URL</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeURLParser extends ReactomeNodeParser<ReactomeURL> {

	/**
	 * Default constructor.
	 */
	public ReactomeURLParser()   {
		super(ReactomeURL.class, URLColumnParser.class);
	}
}
