package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeInterChainCrosslinkedResidue;
import lcsb.mapviewer.reactome.xml.columnParser.InterChainCrosslinkedResidueColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=InterChainCrosslinkedResidue"
 * >InterChainCrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeInterChainCrosslinkedResidueParser extends ReactomeNodeParser<ReactomeInterChainCrosslinkedResidue> {

	/**
	 * Default constructor.
	 */
	public ReactomeInterChainCrosslinkedResidueParser()   {
		super(ReactomeInterChainCrosslinkedResidue.class, InterChainCrosslinkedResidueColumnParser.class);
	}
}
