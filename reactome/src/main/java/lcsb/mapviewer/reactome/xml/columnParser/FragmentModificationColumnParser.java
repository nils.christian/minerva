package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeFragmentModification;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FragmentModification"
 * >FragmentModification</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class FragmentModificationColumnParser extends ColumnParser<ReactomeFragmentModification> {
	/**
	 * Default constructor. Defines parser for super class of FragmentModification.
	 */
	protected FragmentModificationColumnParser() {
		super(GeneticallyModifiedResidueColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeFragmentModification object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("startPositionInReferenceSequence")) {
			object.setStartPositionInReferenceSequence(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("endPositionInReferenceSequence")) {
			object.setEndPositionInReferenceSequence(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
