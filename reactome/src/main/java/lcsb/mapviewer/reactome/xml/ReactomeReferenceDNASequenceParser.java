package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeReferenceDNASequence;
import lcsb.mapviewer.reactome.xml.columnParser.ReferenceDNASequenceColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceDNASequence"
 * >ReferenceDNASequence</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeReferenceDNASequenceParser extends ReactomeNodeParser<ReactomeReferenceDNASequence> {

	/**
	 * Default constructor.
	 */
	public ReactomeReferenceDNASequenceParser()   {
		super(ReactomeReferenceDNASequence.class, ReferenceDNASequenceColumnParser.class);
	}
}
