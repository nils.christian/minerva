package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeSequenceDomain;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=SequenceDomain"
 * >SequenceDomain</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SequenceDomainColumnParser extends ColumnParser<ReactomeSequenceDomain> {

	/**
	 * Default constructor. Defines parser for super class of SequenceDomain.
	 */
	protected SequenceDomainColumnParser() {
		super(DomainColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeSequenceDomain object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("startCoordinate")) {
			object.setStartCoordinate(Integer.parseInt(node.getTextContent()));
		} else if (node.getNodeName().equalsIgnoreCase("endCoordinate")) {
			object.setEndCoordinate(Integer.parseInt(node.getTextContent()));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
