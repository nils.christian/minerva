package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceEntity;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceEntity"
 * >ReferenceEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceEntityColumnParser extends ColumnParser<ReactomeReferenceEntity> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceEntity.
	 */
	protected ReferenceEntityColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceEntity object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("identifier")) {
			object.setIdentifier(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("referenceDatabase")) {
			object.setReferenceDatabase(getRdParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("crossReference")) {
			object.addCrossReferences(getDiParser().parseSimplifiedObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("name")) {
			object.addName(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("otherIdentifier")) {
			object.addOtherIdentifier(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
