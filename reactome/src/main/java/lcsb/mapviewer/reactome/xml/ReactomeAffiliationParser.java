package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeAffiliation;
import lcsb.mapviewer.reactome.xml.columnParser.AffiliationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Affiliation"
 * >Affiliation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeAffiliationParser extends ReactomeNodeParser<ReactomeAffiliation> {

	/**
	 * Default constructor.
	 */
	public ReactomeAffiliationParser() {
		super(ReactomeAffiliation.class, AffiliationColumnParser.class);
	}
}
