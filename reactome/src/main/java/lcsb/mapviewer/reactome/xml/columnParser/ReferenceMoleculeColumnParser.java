package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=ReferenceMolecule"
 * >ReferenceMolecule</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceMoleculeColumnParser extends ColumnParser<ReactomeReferenceMolecule> {

	/**
	 * Default constructor. Defines parser for super class of ReferenceMolecule.
	 */
	protected ReferenceMoleculeColumnParser() {
		super(ReferenceEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeReferenceMolecule object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("atomicConnectivity")) {
			object.setAtomicConnectivity(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("formula")) {
			object.setFormula(node.getTextContent());
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
