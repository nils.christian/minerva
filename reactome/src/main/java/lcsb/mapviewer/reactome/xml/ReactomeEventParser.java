package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeEvent;
import lcsb.mapviewer.reactome.xml.columnParser.EventColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Event"
 * >Event</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeEventParser extends ReactomeNodeParser<ReactomeEvent> {

	/**
	 * Default constructor.
	 */
	public ReactomeEventParser() {
		super(ReactomeEvent.class, EventColumnParser.class);
	}
}
