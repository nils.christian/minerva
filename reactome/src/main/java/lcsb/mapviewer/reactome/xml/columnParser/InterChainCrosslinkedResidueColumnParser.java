package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeInterChainCrosslinkedResidue;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=InterChainCrosslinkedResidue"
 * >InterChainCrosslinkedResidue</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class InterChainCrosslinkedResidueColumnParser extends ColumnParser<ReactomeInterChainCrosslinkedResidue> {
	/**
	 * Default constructor. Defines parser for super class of InterChainCrosslinkedResidue.
	 */
	protected InterChainCrosslinkedResidueColumnParser() {
		super(CrosslinkedResidueColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeInterChainCrosslinkedResidue object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("equivalentTo")) {
			object.addEquivalentTo(getInterCcrParser().parseObject(node));
		} else if (node.getNodeName().equalsIgnoreCase("secondReferenceSequence")) {
			object.addSecondReferenceSequence(getRsParser().parseObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
