package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeDisease;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Disease"
 * >Disease</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DiseaseColumnParser extends ColumnParser<ReactomeDisease> {
	/**
	 * Default constructor. Defines parser for super class of Disease.
	 */
	protected DiseaseColumnParser() {
		super(ExternalOntologyColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeDisease object, Node node) {
		getParentParser().updateColumnFromNode(object, node);
	}

}
