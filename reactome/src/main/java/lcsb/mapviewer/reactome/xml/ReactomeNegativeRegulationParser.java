package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeNegativeRegulation;
import lcsb.mapviewer.reactome.xml.columnParser.NegativeRegulationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=NegativeRegulation"
 * >NegativeRegulation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeNegativeRegulationParser extends ReactomeNodeParser<ReactomeNegativeRegulation> {

	/**
	 * Default constructor.
	 */
	public ReactomeNegativeRegulationParser()   {
		super(ReactomeNegativeRegulation.class, NegativeRegulationColumnParser.class);
	}
}
