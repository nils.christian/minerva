package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGoCellularComponent;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GO_CellularComponent"
 * >GO_CellularComponent</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GoCellularComponentColumnParser extends ColumnParser<ReactomeGoCellularComponent> {
	/**
	 * Default constructor. Defines parser for super class of GoCellularComponent.
	 */
	protected GoCellularComponentColumnParser() {
		super(DatabaseObjectColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGoCellularComponent object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("accession")) {
			object.setAccession(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("definition")) {
			object.setDefinition(node.getTextContent());
		} else if (node.getNodeName().equalsIgnoreCase("referenceDatabase")) {
			object.setReferenceDatabase(getRdParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
