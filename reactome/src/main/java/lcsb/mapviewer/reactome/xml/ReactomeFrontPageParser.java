package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomeFrontPage;
import lcsb.mapviewer.reactome.xml.columnParser.FrontPageColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=FrontPage"
 * >FrontPage</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomeFrontPageParser extends ReactomeNodeParser<ReactomeFrontPage> {

	/**
	 * Default constructor.
	 */
	public ReactomeFrontPageParser()   {
		super(ReactomeFrontPage.class, FrontPageColumnParser.class);
	}
}
