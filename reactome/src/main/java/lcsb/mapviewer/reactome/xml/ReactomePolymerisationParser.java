package lcsb.mapviewer.reactome.xml;

import lcsb.mapviewer.reactome.model.ReactomePolymerisation;
import lcsb.mapviewer.reactome.xml.columnParser.PolymerisationColumnParser;

/**
 * Parser for reactome xml retrieved from <a href=
 * "http://reactome.org/ReactomeRESTfulAPI/ReactomeRESTFulAPI.html"
 * >Reactome API</a> for <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=Polymerisation"
 * >Polymerisation</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactomePolymerisationParser extends ReactomeNodeParser<ReactomePolymerisation> {

	/**
	 * Default constructor.
	 */
	public ReactomePolymerisationParser()   {
		super(ReactomePolymerisation.class, PolymerisationColumnParser.class);
	}
}
