package lcsb.mapviewer.reactome.xml.columnParser;

import lcsb.mapviewer.reactome.model.ReactomeGenomeEncodedEntity;

import org.w3c.dom.Node;

/**
 * Parser for properties of Reactome <a href=
 * "http://www.reactome.org/cgi-bin/classbrowser?DB=gk_current&CLASS=GenomeEncodedEntity"
 * >GenomeEncodedEntity</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GenomeEncodedEntityColumnParser extends ColumnParser<ReactomeGenomeEncodedEntity> {
	/**
	 * Default constructor. Defines parser for super class of GenomeEncodedEntity.
	 */
	protected GenomeEncodedEntityColumnParser() {
		super(PhysicalEntityColumnParser.class);
	}

	@Override
	public void updateColumnFromNode(ReactomeGenomeEncodedEntity object, Node node) {
		if (node.getNodeName().equalsIgnoreCase("species")) {
			object.setSpecies(getsParser().parseSimplifiedObject(node));
		} else {
			getParentParser().updateColumnFromNode(object, node);
		}
	}

}
