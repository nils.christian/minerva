package lcsb.mapviewer.services.overlay;

import lcsb.mapviewer.modelutils.map.ClassTreeNode;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnnotatedObjectTreeRowTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new AnnotatedObjectTreeRow(new ClassTreeNode(Object.class)));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
