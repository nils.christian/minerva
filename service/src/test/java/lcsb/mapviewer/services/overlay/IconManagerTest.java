package lcsb.mapviewer.services.overlay;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IconManagerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		IconManager im = IconManager.getInstance();
		assertNotNull(im);
		String icon1 = im.getIconForIndex(0, IconType.SEARCH, 0);
		String icon2 = im.getIconForIndex(1, IconType.SEARCH, 0);
		assertFalse(icon1.equals(icon2));
	}

	@Test
	public void test1() {
		IconManager im = IconManager.getInstance();
		assertNotNull(im);
		String icon1 = im.getIconForIndex(0, IconType.SEARCH, 0);
		String icon2 = im.getIconForIndex(0, IconType.DRUG, 0);
		assertFalse(icon1.equals(icon2));
	}

	@Test
	public void test2() {
		IconManager im = IconManager.getInstance();
		assertNotNull(im);
		String icon1 = im.getIconForIndex(0, IconType.SEARCH, 0);
		String icon2 = im.getIconForIndex(0, IconType.SEARCH, 1);
		assertFalse(icon1.equals(icon2));
	}

}
