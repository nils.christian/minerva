package lcsb.mapviewer.services.overlay;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AnnotatedObjectTreeRowTest.class, //
		ChebiTreeRowTest.class,//
		IconManagerTest.class, //
})
public class AllOverlayTests {

}
