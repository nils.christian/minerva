package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class ExternalServicesServiceTest extends ServiceTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(timeout = 60000)
	@Ignore("Bug 451")
	public void testDefaultServices() throws Exception {
		try {
			externalServicesService.clearServices();
			externalServicesService.registerDefaultServices();
			List<ExternalServiceStatus> statuses = externalServicesService.getExternalServiceStatuses();
			assertNotNull(statuses);
			assertTrue(statuses.size() > 0);

			for (ExternalServiceStatus externalServiceStatus : statuses) {
				assertEquals(externalServiceStatus.getName() + " is down", ExternalServiceStatusType.OK, externalServiceStatus.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRegister() throws Exception {
		try {
			externalServicesService.clearServices();
			externalServicesService.registerService(new OkServiceMock());
			externalServicesService.registerService(new FailServiceMock());

			List<ExternalServiceStatus> statuses = externalServicesService.getExternalServiceStatuses();
			assertNotNull(statuses);
			assertEquals(2, statuses.size());

			assertEquals(ExternalServiceStatusType.OK, statuses.get(0).getStatus());
			assertEquals(ExternalServiceStatusType.DOWN, statuses.get(1).getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
