package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.annotation.Rollback;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.UserDTO;

@Rollback(true)
public class LdapServiceTest extends ServiceTestFunctions {
  static Logger logger = Logger.getLogger(LdapServiceTest.class);

  LdapService ldapService;

  @Before
  public void setUp() throws Exception {
    try {
      configurationService.setConfigurationValue(ConfigurationElementType.LDAP_BASE_DN, "dc=uni,dc=lu");
      configurationService.setConfigurationValue(ConfigurationElementType.LDAP_OBJECT_CLASS, "person");
      configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER, "memberof=cn=gitlab,cn=groups,cn=accounts,dc=uni,dc=lu");

      ldapService = Mockito.spy(LdapService.class);
      ldapService.setConfigurationService(configurationService);
      Mockito.when(ldapService.getConnection()).thenAnswer(new Answer<LDAPConnection>() {

        @Override
        public LDAPConnection answer(InvocationOnMock invocation) throws Throwable {
          // Create the configuration to use for the server.
          InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
          config.addAdditionalBindCredentials("uid=piotr.gawron,cn=users,cn=accounts,dc=uni,dc=lu", "test_passwd");
          config.setSchema(null);

          // Create the directory server instance, populate it with data from the
          // "test-data.ldif" file, and start listening for client connections.
          InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
          ds.importFromLDIF(true, "testFiles/ldap/testdata.ldif");
          ds.startListening();
          return ds.getConnection();
        }
      });

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testIsValidConfiguration() throws Exception {
    try {
      assertTrue(ldapService.isValidConfiguration());
      configurationService.setConfigurationValue(ConfigurationElementType.LDAP_BASE_DN, "");
      assertFalse(ldapService.isValidConfiguration());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testLogin() throws Exception {
    try {
      assertTrue(ldapService.login("piotr.gawron", "test_passwd"));
      assertFalse(ldapService.login("piotr.gawron", "invalid_password"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUsernames() throws Exception {
    try {
      List<String> list = ldapService.getUsernames();
      assertEquals(2, list.size());
      assertTrue(list.contains("piotr.gawron"));
      assertFalse(list.contains("john.doe"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUsernamesWithFiltering() throws Exception {
    try {
      configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER,
          "(memberof=cn=owncloud,cn=groups,cn=accounts,dc=uni,dc=lu)");

      List<String> list = ldapService.getUsernames();
      assertEquals(1, list.size());
      assertTrue(list.contains("piotr.gawron"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUserByLogin() throws Exception {
    try {
      UserDTO user = ldapService.getUserByLogin("piotr.gawron");
      assertEquals("Piotr", user.getFirstName());
      assertEquals("Gawron", user.getLastName());
      assertEquals("piotr.gawron", user.getLogin());
      assertEquals("piotr.gawron@uni.lu", user.getEmail());
      assertNotNull(user.getBindDn());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
