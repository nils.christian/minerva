package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GeneVariation;
import lcsb.mapviewer.model.map.layout.GeneVariationColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.services.interfaces.ILayoutService.CreateLayoutParams;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;
import lcsb.mapviewer.services.utils.data.ColorSchemaType;

public class LayoutServiceTest2 {
  Logger logger = Logger.getLogger(LayoutServiceTest2.class);
  ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidPreprareTableResultForGeneric() throws Exception {
    try {
      class CSR extends ColorSchemaReader {
        public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
          List<ColorSchemaColumn> result = new ArrayList<ColorSchemaColumn>();
          for (ColorSchemaColumn csc : ColorSchemaColumn.values()) {
            if (csc.getTypes().contains(ColorSchemaType.GENERIC)) {
              result.add(csc);
            }
          }
          return result;
        }
      }
      ;
      List<ColorSchema> schemas = new ArrayList<ColorSchema>();
      ColorSchema cs = new GenericColorSchema();
      cs.setColor(Color.CYAN);
      cs.addCompartment("BLA");
      cs.addMiriamData(new MiriamData(MiriamType.CAS, "CAS_ID"));
      cs.setLineWidth(3.3);
      cs.setMatches(2);
      cs.setName("UUU");
      cs.setElementId("RE ID");
      cs.setReverseReaction(true);
      cs.addType(GenericProtein.class);
      cs.setValue(1111.1111);
      schemas.add(cs);

      LayoutService ls = new LayoutService();
      String result = ls.prepareTableResult(schemas, new CSR());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testValidPreprareTableResultForGeneVariation() throws Exception {
    try {
      class CSR extends ColorSchemaReader {
        public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
          List<ColorSchemaColumn> result = new ArrayList<ColorSchemaColumn>();
          for (ColorSchemaColumn csc : ColorSchemaColumn.values()) {
            if (csc.getTypes().contains(ColorSchemaType.GENETIC_VARIANT)) {
              result.add(csc);
            }
          }
          return result;
        }
      }
      ;
      List<ColorSchema> schemas = new ArrayList<ColorSchema>();
      GeneVariationColorSchema cs = new GeneVariationColorSchema();
      cs.setColor(Color.CYAN);
      cs.addCompartment("BLA");
      cs.addMiriamData(new MiriamData(MiriamType.CAS, "CAS_ID"));
      cs.setLineWidth(3.3);
      cs.setMatches(2);
      cs.setName("UUU");
      cs.setElementId("RE ID");
      cs.setReverseReaction(true);
      cs.addType(GenericProtein.class);
      cs.setValue(1111.1111);
      GeneVariation gv = new GeneVariation();
      gv.setModifiedDna("C");
      gv.setContig("chr1");
      gv.setOriginalDna("T");
      gv.setPosition(12);
      gv.setReferenceGenomeType(ReferenceGenomeType.UCSC);
      gv.setReferenceGenomeVersion("hg19");
      cs.addGeneVariation(gv);
      GeneVariation gv2 = new GeneVariation();
      gv2.setModifiedDna("CC");
      gv2.setContig("chr2");
      gv2.setOriginalDna("TT");
      gv2.setPosition(124);
      gv2.setReferenceGenomeType(ReferenceGenomeType.UCSC);
      gv2.setReferenceGenomeVersion("hg18");
      gv2.addReference(new MiriamData(MiriamType.CHEBI, "XXX"));
      cs.addGeneVariation(gv2);
      schemas.add(cs);

      LayoutService ls = new LayoutService();
      String result = ls.prepareTableResult(schemas, new CSR());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testEmptyPreprareTableResultForGeneric() throws Exception {
    try {
      class CSR extends ColorSchemaReader {
        public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
          List<ColorSchemaColumn> result = new ArrayList<ColorSchemaColumn>();
          for (ColorSchemaColumn csc : ColorSchemaColumn.values()) {
            if (csc.getTypes().contains(ColorSchemaType.GENERIC)) {
              result.add(csc);
            }
          }
          return result;
        }
      }
      ;
      List<ColorSchema> schemas = new ArrayList<ColorSchema>();
      ColorSchema cs = new GenericColorSchema();
      schemas.add(cs);

      LayoutService ls = new LayoutService();
      String result = ls.prepareTableResult(schemas, new CSR());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testEmptyPreprareTableResultForGeneVariant() throws Exception {
    try {
      class CSR extends ColorSchemaReader {
        public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
          List<ColorSchemaColumn> result = new ArrayList<ColorSchemaColumn>();
          for (ColorSchemaColumn csc : ColorSchemaColumn.values()) {
            if (csc.getTypes().contains(ColorSchemaType.GENETIC_VARIANT)) {
              result.add(csc);
            }
          }
          return result;
        }
      }
      ;
      List<ColorSchema> schemas = new ArrayList<ColorSchema>();
      ColorSchema cs = new GeneVariationColorSchema();
      schemas.add(cs);

      LayoutService ls = new LayoutService();
      String result = ls.prepareTableResult(schemas, new CSR());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetLayoutAliasesForInvalidAlias() throws Exception {
    try {

      Model model = new CellDesignerXmlParser()
          .createModel(new ConverterParams().filename("testFiles/coloring/problematicModel2.xml"));

      FileInputStream fis = new FileInputStream("testFiles/coloring/problematicSchema2.txt");
      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(fis).//
          async(false);
      ColorSchemaReader reader = new ColorSchemaReader();
      final Collection<ColorSchema> schemas = reader.readColorSchema(params.getColorInputStream(),
          TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream()));

      ColorModelCommand command = new ColorModelCommand(model, schemas, colorExtractor);
      command.execute();

      command.getModifiedElements();

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSchemaWithCompartments() throws Exception {
    try {
      FileInputStream fis = new FileInputStream("testFiles/coloring/schemaWithCompartment.txt");
      ColorSchemaReader reader = new ColorSchemaReader();
      final Collection<ColorSchema> schemas = reader.readColorSchema(fis, new HashMap<>());
      for (ColorSchema colorSchema : schemas) {
        for (String string : colorSchema.getCompartments()) {
          assertNotNull(string);
          assertFalse(string.isEmpty());
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
