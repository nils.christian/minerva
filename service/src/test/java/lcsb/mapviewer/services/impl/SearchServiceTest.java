package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ISearchService.CoordinatesSearchParams;
import lcsb.mapviewer.services.utils.SearchIndexer;

public class SearchServiceTest extends ServiceTestFunctions {
  static Logger logger = Logger.getLogger(SearchServiceTest.class);

  SearchIndexer indexer = new SearchIndexer();

  Map<Class<?>, String> speciesSearchPrefix = new HashMap<Class<?>, String>();
  Map<String, Class<?>> speciesSearchReversePrefix = new HashMap<String, Class<?>>();

  @Before
  public void setUp() throws Exception {
    addSearchPrefix("complex", Complex.class);
    addSearchPrefix("degrded", Degraded.class);
    addSearchPrefix("drug", Drug.class);
    addSearchPrefix("gene", Gene.class);
    addSearchPrefix("ion", Ion.class);
    addSearchPrefix("phenotype", Phenotype.class);
    addSearchPrefix("protein", Protein.class);
    addSearchPrefix("rna", Rna.class);
    addSearchPrefix("molecule", SimpleMolecule.class);
    addSearchPrefix("unknown", Unknown.class);
  }

  private void addSearchPrefix(String prefix, Class<?> clazz) {
    speciesSearchPrefix.put(clazz, prefix);
    speciesSearchReversePrefix.put(prefix, clazz);
  }

  protected Model createFullModel() throws Exception {
    Model model = getModelForFile("testFiles/searchModel.xml", false);
    model.setProject(new Project("unknown project"));
    return model;
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSearchByName() throws Exception {
    try {
      Model model = createFullModel();
      long count = searchHistoryDao.getCount();
      List<BioEntity> result = searchService.searchByQuery(model, "reaction:re21", 50, null, "127.0.0.1");

      assertNotNull(result);
      assertEquals(4, result.size());
      Reaction reaction = (Reaction) result.get(0);
      assertTrue(reaction.getIdReaction().contains("re21"));

      long count2 = searchHistoryDao.getCount();

      assertEquals(count + 1, count2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSearchByName2() throws Exception {
    try {
      Model model = createFullModel();
      List<BioEntity> result = searchService.searchByQuery(model, "BAD:BCL-2", 50, null, "127.0.0.1");

      assertNotNull(result);
      assertTrue(result.size() > 0);
      Species alias = (Species) result.get(0);
      assertEquals("BAD:BCL-2", alias.getName());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSearchById() throws Exception {
    try {
      Model model = createFullModel();
      MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_HAS_VERSION, MiriamType.CAS, "123");
      model.getElements().iterator().next().addMiriamData(md);
      List<BioEntity> global = searchService.searchByQuery(model, "CAS:123", 50, null, "127.0.0.1");
      assertNotNull(global);
      assertEquals(1, global.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSearchByElementId() throws Exception {
    try {
      Model model = createFullModel();
      Element element = model.getElements().iterator().next();
      element.setId(907);
      List<BioEntity> global = searchService.searchByQuery(model, "element:907", 50, null, "127.0.0.1");
      assertNotNull(global);
      assertEquals(1, global.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

    @Test
    public void testSearchByUnknwonElementId() throws Exception {
        try {
            Model model = createFullModel();
            Element element =model.getElements().iterator().next();
            element.setId(907);
            List<BioEntity> global = searchService.searchByQuery(model, "species:90111117", 50, null, "127.0.0.1");
            assertEquals(0, global.size());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

  @Test
  public void testSearchByName4() throws Exception {
    try {
      Model model = createFullModel();
      List<BioEntity> result = searchService.searchByQuery(model, "BC", 50, true, "127.0.0.1");

      assertNotNull(result);
      assertEquals(0, result.size());

      result = searchService.searchByQuery(model, "BC", 50, false, "127.0.0.1");
      assertNotNull(result);
      assertTrue(result.size() > 0);

      result = searchService.searchByQuery(model, "BAD:BCL-2", 50, true, "127.0.0.1");
      assertNotNull(result);
      assertEquals(1, result.size());
      Species alias = (Species) result.get(0);
      assertEquals("BAD:BCL-2", alias.getName());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testQueryName() throws Exception {
    try {
      Model model = createFullModel();
      List<BioEntity> result = searchService.searchByQuery(model,
          indexer.getQueryStringForIndex("reaction:re21", new HashSet<>()), 50, null, "127.0.0.1");

      assertNotNull(result);
      assertEquals(4, result.size());
      Reaction reaction = (Reaction) result.get(0);
      assertEquals("re21", reaction.getIdReaction());
      assertTrue(result.get(1) instanceof Species);
      assertTrue(result.get(2) instanceof Species);
      assertTrue(result.get(3) instanceof Species);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testQueryReactionId() throws Exception {
    try {
      Model model = createFullModel();
      model.getReactions().iterator().next().setIdReaction("R_x1");
      List<BioEntity> result = searchService.searchByQuery(model,
          indexer.getQueryStringForIndex("reaction:r_x1", new HashSet<>()), 50, null, "127.0.0.1");

      assertNotNull(result);
      assertTrue(result.size() > 0);
      Reaction reaction = (Reaction) result.get(0);
      assertTrue(reaction.getIdReaction().contains("R_x1"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testQueryReactionId2() throws Exception {
    try {
      Model model = createFullModel();
      Reaction reaction = model.getReactions().iterator().next();
      reaction.setId(154);
      reaction.setIdReaction("test-name");
      List<BioEntity> result = searchService.searchByQuery(model,
          indexer.getQueryStringForIndex("reaction:154", new HashSet<>()), 50, null, "127.0.0.1");

      assertNotNull(result);
      assertTrue(result.size() > 0);
      Reaction resultReaction = (Reaction) result.get(0);
      assertTrue(resultReaction.getIdReaction().contains("test-name"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testQueryName2() throws Exception {
    try {
      Model model = createFullModel();
      model.getModelData().setId(-13);
      List<BioEntity> result = searchService.searchByQuery(model,
          indexer.getQueryStringForIndex("BCL-2", speciesSearchReversePrefix.keySet()), 50, null, "127.0.0.1");
      assertNotNull(result);
      assertTrue(result.size() > 0);
      Species alias = (Species) result.get(0);
      assertTrue("Invalid alias: " + alias.getName(), alias.getName().toUpperCase().contains("BCL"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testQueryName3() throws Exception {
    try {
      Model model = createFullModel();
      List<BioEntity> result = searchService.searchByQuery(model, "fdhgjkhdsfsdhfgfhsd", 50, null, "127.0.0.1");

      assertNotNull(result);
      assertEquals(0, result.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testQueryName4() throws Exception {
    try {
      Model model = createFullModel();
      List<BioEntity> result = searchService.searchByQuery(model,
          indexer.getQueryStringForIndex("protein:BCL-2", speciesSearchReversePrefix.keySet()), 50, null, "127.0.0.1");
      assertNotNull(result);
      assertTrue(result.size() > 0);
      Species alias = (Species) result.get(0);
      assertTrue("Invalid alias: " + alias.getName(), alias.getName().toUpperCase().contains("BCL"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSearchClosest() throws Exception {
    try {
      Model model = getModelForFile("testFiles/graph_path_example3.xml", true);
      List<BioEntity> elements = searchService.getClosestElements(model, new Point2D.Double(0, 0), 5, false,
          new ArrayList<>());
      assertNotNull(elements);
      assertEquals(5, elements.size());
      assertTrue(elements.get(0) instanceof Species);
      Species sAlias = (Species) elements.get(0);
      assertEquals("s1", sAlias.getName());
      assertTrue(elements.get(1) instanceof Reaction);
      Reaction reaction = (Reaction) elements.get(1);
      assertEquals("re1", reaction.getIdReaction());
      assertTrue(elements.get(2) instanceof Species);
      sAlias = (Species) elements.get(2);
      assertEquals("s3", sAlias.getName());
      assertTrue(elements.get(3) instanceof Reaction);
      reaction = (Reaction) elements.get(3);
      assertEquals("re2", reaction.getIdReaction());
      assertTrue(elements.get(4) instanceof Species);
      sAlias = (Species) elements.get(4);
      assertEquals("s2", sAlias.getName());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSearchClosest2() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      GenericProtein protein1 = new GenericProtein("s1");
      protein1.setWidth(20);
      protein1.setHeight(20);
      protein1.setX(5);
      protein1.setY(5);
      model.addElement(protein1);
      GenericProtein protein2 = new GenericProtein("s2");
      protein2.setWidth(10);
      protein2.setHeight(10);
      protein2.setX(5);
      protein2.setY(5);
      model.addElement(protein2);
      GenericProtein protein3 = new GenericProtein("s3");
      protein3.setWidth(30);
      protein3.setHeight(30);
      protein3.setX(0);
      protein3.setY(0);
      model.addElement(protein3);

      List<BioEntity> elements = searchService.getClosestElements(model, new Point2D.Double(10, 10), 5, false,
          new ArrayList<>());
      assertNotNull(elements);
      assertEquals(3, elements.size());
      BioEntity sAlias = elements.get(0);
      assertEquals("s2", sAlias.getName());
      BioEntity reaction = elements.get(1);
      assertEquals("s1", reaction.getName());
      sAlias = elements.get(2);
      assertEquals("s3", sAlias.getName());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSearchClosestWithEmptyModel() throws Exception {
    try {
      Model model = new ModelFullIndexed(null);
      List<BioEntity> elements = searchService.getClosestElements(model, new Point2D.Double(0, 0), 5, false,
          new ArrayList<>());
      assertNotNull(elements);
      assertEquals(0, elements.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAutocompleList() {
    try {
      Model model = new ModelFullIndexed(null);
      Species proteinAlias = new GenericProtein("a");
      proteinAlias.setName("PROT identifier");
      proteinAlias.getSynonyms().add("PROT synonym");
      proteinAlias.getSynonyms().add("another synonym");
      proteinAlias.setFullName("Protein common name");
      model.addElement(proteinAlias);

      SimpleMolecule alias = new SimpleMolecule("a2");
      alias.setName("Molecule2");
      model.addElement(alias);

      int oldVal = Configuration.getAutocompleteSize();
      Configuration.setAutocompleteSize(5);

      List<String> list = searchService.getAutocompleteList(model, "P");
      assertNotNull(list);
      assertEquals(4, list.size());
      assertTrue(list.contains("PROT identifier".toLowerCase()));
      assertTrue(list.contains("PROT synonym".toLowerCase()));
      assertTrue(list.contains("Protein common name".toLowerCase()));

      list = searchService.getAutocompleteList(model, "PROTEIN");
      assertNotNull(list);
      assertEquals(2, list.size());
      assertTrue(list.contains("Protein common name".toLowerCase()));

      list = searchService.getAutocompleteList(model, "m");
      assertNotNull(list);
      assertEquals(2, list.size());
      assertTrue(list.contains("Molecule2".toLowerCase()));

      list = searchService.getAutocompleteList(model, "blablabla");
      assertNotNull(list);
      assertEquals(0, list.size());

      Configuration.setAutocompleteSize(oldVal);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetAutocompleList2() {
    try {
      Model model = new ModelFullIndexed(null);
      GenericProtein proteinAlias = new GenericProtein("a1");
      proteinAlias.setName("PROT identifier");
      proteinAlias.setNotes("Synonyms: PROT synonym, another synonym\nName: Protein common name");
      model.addElement(proteinAlias);

      SimpleMolecule alias = new SimpleMolecule("a2");
      alias.setName("Molecule2");
      model.addElement(alias);

      List<String> list = searchService.getAutocompleteList(model, "PROT identifier");
      assertNotNull(list);
      assertEquals(1, list.size());
      assertTrue(list.get(0).equalsIgnoreCase("PROT identifier"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testSearchGenericProtein() throws Exception {
    try {
      Model model = getModelForFile("testFiles/generic.xml", true);

      List<String> list = searchService.getAutocompleteList(model, "generic");
      assertNotNull(list);
      assertEquals(2, list.size());
      assertTrue(list.contains("generic"));
      assertTrue(list.contains("generic protein"));

      List<BioEntity> result = searchService.searchByQuery(model, "generic protein", 10, false, null);
      assertNotNull(result);

      assertTrue(result.size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testSearchByCoordCompartment() throws Exception {
    try {
      Model model = getModelForFile("testFiles/graph_path_example3.xml", true);

      // we search in non-nested model
      List<BioEntity> res = searchService.searchByCoordinates(new CoordinatesSearchParams().model(model).x(50).y(50));
      assertNotNull(res);
      assertEquals(0, res.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testSearchByCoordCompartment2() throws Exception {
    try {
      Model model = getModelForFile("testFiles/graph_path_example3.xml", true);

      Layout layout = new Layout();
      layout.setHierarchicalView(true);
      model.addLayout(layout);
      // we search in nested model
      List<BioEntity> res = searchService
          .searchByCoordinates(new CoordinatesSearchParams().model(model).x(50).y(50).layoutIdentifier(0));
      assertNotNull(res);

      assertTrue(res.get(0) instanceof Compartment);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testSearchByCoordReaction() throws Exception {
    try {
      Model model = getModelForFile("testFiles/graph_path_example3.xml", true);

      // find compartment
      List<BioEntity> res = searchService.searchByCoordinates(new CoordinatesSearchParams().model(model));
      // reaction is too far
      assertEquals(0, res.size());

      res = searchService.searchByCoordinates(new CoordinatesSearchParams().model(model).distance(1000));
      assertEquals(3, res.size());

      assertTrue(res.get(0) instanceof Reaction);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testSearchByCoordAlias() throws Exception {
    try {
      Model model = getModelForFile("testFiles/graph_path_example3.xml", true);

      List<BioEntity> res = searchService.searchByCoordinates(new CoordinatesSearchParams().model(model).x(60).y(60));
      assertNotNull(res);
      assertTrue(res.size() > 0);

      assertTrue(res.get(0) instanceof Species);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetMiriamTypeForQuery() throws Exception {
    try {
      MiriamData md = new SearchService().getMiriamTypeForQuery("hgnc:SNCA");
      assertNotNull(md);
      assertEquals(MiriamType.HGNC, md.getDataType());
      assertEquals("SNCA", md.getResource());

      md = new SearchService().getMiriamTypeForQuery("reactome:REACT_15128");
      assertNotNull(md);
      assertEquals(MiriamType.REACTOME, md.getDataType());
      assertEquals("REACT_15128", md.getResource());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testSearchByMiriamInSubmap() throws Exception {
    try {
      String query = "HGNC_SYMBOL:SNCA";
      Model model = new ModelFullIndexed(null);
      Model submodel = new ModelFullIndexed(null);
      GenericProtein protein = new GenericProtein("s1");
      protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
      submodel.addElement(protein);
      model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.UNKNOWN));

      List<BioEntity> result = searchService.searchByQuery(model, query, 50, null, "127.0.0.1");

      assertEquals(1, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetMiriamTypeForQuery2() throws Exception {
    try {
      MiriamData md = new SearchService().getMiriamTypeForQuery("UNKNOWN_HGNC:SNCA");
      assertNull(md);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

}
