package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.LayoutStatus;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.user.BasicPrivilege;
import lcsb.mapviewer.model.user.ObjectPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.ILayoutService.CreateLayoutParams;
import lcsb.mapviewer.services.utils.EmailSender;

public class LayoutServiceTest extends ServiceTestFunctions {
  Logger logger = Logger.getLogger(LayoutServiceTest.class);

  Project project = null;
  Model model = null;

  String projectId = "Some_id";
  String clearProjectId = "Some_id2";

  @Autowired
  LayoutDao layoutDao;

  EmailSender originalSender;

  @Autowired
  ILayoutService layoutService;

  // assume that we have admin account with all the privileges
  String adminToken;
  String token;

  @Before
  public void setUp() throws Exception {
    adminToken = userService.login("admin", "admin");

    logService.setLoggedUser(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN));

    // we use custom threads because in layout service there is commit method
    // called, and because of that hibernate session injected by spring cannot
    // commit at the end of the test case

    dbUtils.createSessionForCurrentThread();
    dbUtils.setAutoFlush(false);

    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }

    project = projectDao.getProjectByProjectId(clearProjectId);
    if (project != null) {
      projectDao.delete(project);
    }

    project = new Project();

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

    project.addModel(model);
    project.setProjectId(projectId);

    projectService.addProject(project);

    createUser();
    createUser2();

    originalSender = layoutService.getEmailSender();
    layoutService.setEmailSender(Mockito.mock(EmailSender.class));
    token = userService.login(user.getLogin(), "passwd");
  }

  @After
  public void tearDown() throws Exception {
    userService.deleteUser(user2);
    userService.deleteUser(user);
    projectService.removeProject(project, null, false, adminToken);

    // close session
    dbUtils.closeSessionForCurrentThread();

    layoutService.setEmailSender(originalSender);
  }

  @Test
  public void testUserCanAddLayout() throws Exception {
    try {
      int initialSize = model.getLayouts().size();
      assertFalse(layoutService.userCanAddLayout(model, user));
      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));

      assertFalse(layoutService.userCanAddLayout(model, user));

      userService.setUserPrivilege(user, new ObjectPrivilege(project, 1, PrivilegeType.VIEW_PROJECT, user));

      assertTrue(layoutService.userCanAddLayout(model, user));

      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user);
      Layout row = layoutService.createLayout(params);

      assertFalse(layoutService.userCanAddLayout(model, user));

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));

      assertFalse(layoutService.userCanAddLayout(model, user));

      userService.setUserPrivilege(user, new BasicPrivilege(2, PrivilegeType.CUSTOM_LAYOUTS, user));

      assertTrue(layoutService.userCanAddLayout(model, user));

      assertTrue(layoutService.userCanRemoveLayout(row, user));

      layoutService.removeLayout(row, null);

      assertEquals(initialSize, model.getLayouts().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAvailable() throws Exception {
    try {
      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));
      assertEquals(1, layoutService.getAvailableCustomLayoutsNumber(user));

      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user);
      Layout row = layoutService.createLayout(params);

      assertEquals(0, layoutService.getAvailableCustomLayoutsNumber(user));

      ModelSubmodelConnection connection = new ModelSubmodelConnection(model, SubmodelType.UNKNOWN);
      model.addSubmodelConnection(connection);

      modelDao.update(model);

      assertEquals(1, layoutService.getAvailableCustomLayoutsNumber(user));

      layoutService.removeLayout(row, null);

      assertEquals(1, layoutService.getAvailableCustomLayoutsNumber(user));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetCustomLayouts() throws Exception {
    try {
      List<Layout> layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertNotNull(layouts);
      assertEquals(0, layouts.size());

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));

      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user);
      Layout row = layoutService.createLayout(params);

      assertNotNull(row);
      assertTrue(row.getId() > 0);

      layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertEquals(1, layouts.size());

      String token2 = userService.login(user2.getLogin(), "passwd");
      layouts = layoutService.getCustomLayouts(model, token2, false, user2);
      assertEquals(0, layouts.size());

      layoutService.removeLayout(row, null);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateLayout() throws Exception {
    try {
      List<Layout> layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertNotNull(layouts);
      assertEquals(0, layouts.size());

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));

      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user);
      Layout row = layoutService.createLayout(params);

      layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertEquals(1, layouts.size());
      assertEquals("Test", layouts.get(0).getTitle());

      row.setTitle("New name");

      layoutService.updateLayout(row);

      layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertEquals(1, layouts.size());
      assertEquals("New name", layouts.get(0).getTitle());

      layoutService.removeLayout(row, null);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testCreateAsyncLayout() throws Exception {
    try {
      List<Layout> layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertNotNull(layouts);
      assertEquals(0, layouts.size());

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));
      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user).//
          async(true);

      long logCounter = logDao.getCount();
      Layout row = layoutService.createLayout(params);

      assertNotNull(row);
      assertTrue(row.getId() > 0);

      Layout layoutDb = layoutDao.getById(row.getId());
      do {
        Thread.sleep(200);
        layoutDao.refresh(layoutDb);
      } while (layoutDb.getStatus() != LayoutStatus.OK);

      layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertEquals(1, layouts.size());

      long logCounter2 = logDao.getCount();
      assertTrue("Log didn't appeard after creating layout", logCounter < logCounter2);

      Layout layout = layouts.get(0);
      logger.debug(layout.getStatus() + ", " + layout.getProgress());

      assertFalse("Layout images weren't generated...", layout.getStatus().equals(LayoutStatus.NA));

      logCounter = logDao.getCount();
      layoutService.removeLayout(layout, null);
      logCounter2 = logDao.getCount();

      assertTrue("Log didn't appeard after removing layout", logCounter < logCounter2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddLayoutToComplexModel() throws Exception {
    Project localProject = new Project();
    try {

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      Model localModel = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
      Model localSubmodelModel = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

      ModelSubmodelConnection connection = new ModelSubmodelConnection(localSubmodelModel, SubmodelType.UNKNOWN);
      connection.setName("AS");
      localModel.addSubmodelConnection(connection);

      localProject.addModel(localModel);
      localProject.setProjectId(clearProjectId);

      projectService.addProject(localProject);

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));
      userService.setUserPrivilege(user, new ObjectPrivilege(localProject, 1, PrivilegeType.VIEW_PROJECT, user));

      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(localModel).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user);

      layoutService.createLayout(params);

      Project pr2 = projectService.getProjectByProjectId(localProject.getProjectId(), token);

      Model fromDbModel = pr2.getModels().iterator().next().getModel();

      assertTrue(fromDbModel.getLayouts().size() > 0);

      Model submodel = fromDbModel.getSubmodelConnections().iterator().next().getSubmodel().getModel();
      assertEquals(fromDbModel.getLayouts().size(), submodel.getLayouts().size());

      File f1 = new File(fromDbModel.getLayouts().get(0).getDirectory() + "/2/0/0.PNG");
      File f2 = new File(submodel.getLayouts().get(0).getDirectory() + "/2/0/0.PNG");
      assertFalse(f1.exists());
      assertFalse(f2.exists());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      projectService.removeProject(localProject, null, false, adminToken);
    }
  }

  @Test
  public void testInputDataInLayout() throws Exception {
    try {
      List<Layout> layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertNotNull(layouts);
      assertEquals(0, layouts.size());

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));
      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(new FileInputStream("testFiles/enricoData/ge001.txt")).//
          user(user).//
          async(false);

      Layout row = layoutService.createLayout(params);

      assertNotNull(row);
      assertTrue(row.getId() > 0);
      assertNotNull(row.getInputData());
      byte[] inputData = row.getInputData().getFileContent();
      assertNotNull(inputData);
      byte[] originalData = IOUtils.toByteArray(new FileInputStream("testFiles/enricoData/ge001.txt"));
      assertEquals(new String(originalData, StandardCharsets.UTF_8), new String(inputData, StandardCharsets.UTF_8));

      layoutService.removeLayout(row, null);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetLayoutAliases() throws Exception {
    try {
      List<Layout> layouts = layoutService.getCustomLayouts(model, token, false, user);
      assertNotNull(layouts);
      assertEquals(0, layouts.size());

      userService.setUserPrivilege(user, new BasicPrivilege(1, PrivilegeType.CUSTOM_LAYOUTS, user));
      ByteArrayInputStream bais = new ByteArrayInputStream(
          "name	reactionIdentifier	Color\ns1		#CC0000\ns2		#CCCC00\n	re1	#CCCC00\n"
              .getBytes(StandardCharsets.UTF_8));
      CreateLayoutParams params = new CreateLayoutParams().name("Test").//
          directory("testDir").//
          model(model).//
          colorInputStream(bais).//
          user(user).//
          async(false);

      Layout row = layoutService.createLayout(params);

      String token = userService.login(user.getLogin(), "passwd");

      List<?> result = layoutService.getAliasesForLayout(model, row.getId(), token);

      assertTrue(result.size() > 0);

      List<?> result2 = layoutService.getReactionsForLayout(model, row.getId(), token);

      assertEquals(1, result2.size());

      layoutService.removeLayout(row, null);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
