package lcsb.mapviewer.services.impl;

import static org.junit.Assert.*;
import lcsb.mapviewer.services.ServiceTestFunctions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Md5PasswordEncoderTest extends ServiceTestFunctions {
	@Autowired
	PasswordEncoder passwordEncoder;
	
	String passwd = "test";
	String hash = "098f6bcd4621d373cade4e832627b4f6";
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEncode() {
		try {
			assertEquals(hash, passwordEncoder.encode(passwd));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testMatch() {
		try {
			assertTrue(passwordEncoder.matches(passwd, hash));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
