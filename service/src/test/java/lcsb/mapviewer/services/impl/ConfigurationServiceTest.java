package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.FrameworkVersion;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.services.ServiceTestFunctions;

@Rollback(true)
public class ConfigurationServiceTest extends ServiceTestFunctions {
  Logger logger = Logger.getLogger(ConfigurationServiceTest.class);

  @Before
  public void setUp() throws Exception {
    // clear information about git version
    Configuration.getSystemBuildVersion(null, true);
  }

  @After
  public void tearDown() throws Exception {
    // clear information about git version
    Configuration.getSystemBuildVersion(null, true);
  }

  @Test
  public void testGetUpdate() throws Exception {
    try {
      String address = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
      configurationService.deleteConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
      String newAddress = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
      assertEquals(ConfigurationElementType.EMAIL_ADDRESS.getDefaultValue(), newAddress);
      configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS, "hello@world");
      newAddress = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
      assertEquals("hello@world", newAddress);

      configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS, address);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testList() throws Exception {
    try {
      List<ConfigurationOption> list = configurationService.getAllValues(true);
      assertEquals(ConfigurationElementType.values().length, list.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testLimitedList() throws Exception {
    try {
      List<ConfigurationOption> list = configurationService.getAllValues(false);
      assertTrue(ConfigurationElementType.values().length > list.size());
      assertTrue(list.size() > 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSystemVersion() throws Exception {
    try {
      FrameworkVersion view = configurationService.getSystemVersion("testFiles/gitVersionTest/testNormal/");
      assertNotNull(view);
      assertEquals("100", view.getGitVersion());
      assertEquals("202", view.getVersion());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSystemVersion2() throws Exception {
    try {
      FrameworkVersion view = configurationService.getSystemVersion("testFiles/gitVersionTest/testModified/");
      assertNotNull(view);
      assertEquals("100:105", view.getGitVersion());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSystemVersion3() throws Exception {
    try {
      FrameworkVersion view = configurationService.getSystemVersion("testFiles/gitVersionTest/testCorrectSvn/");
      assertNotNull(view);
      assertEquals("117", view.getGitVersion());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetConfiguratioElemntForPrivilege() throws Exception {
    try {
      for (PrivilegeType type : PrivilegeType.values()) {
        ConfigurationOption value = configurationService.getValue(type);
        if (Project.class.equals(type.getPrivilegeObjectType())) {
          assertNotNull("No default value for privilege " + type.getCommonName(), value);
          assertNotNull(value.getValue());
        } else {
          assertNull(value);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
