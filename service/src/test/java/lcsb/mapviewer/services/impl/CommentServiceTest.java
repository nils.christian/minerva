package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.ServiceTestFunctions;

@Rollback(true)
public class CommentServiceTest extends ServiceTestFunctions {
  static Logger logger = Logger.getLogger(CommentServiceTest.class);
  Model model;
  Project project;
  Element alias;
  Element alias2;
  private String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
    try {
      dbUtils.setAutoFlush(true);
      project = projectDao.getProjectByProjectId(projectId);
      if (project != null) {
        projectDao.delete(project);
      }
      project = new Project();
      project.setProjectId(projectId);
      model = getModelForFile("testFiles/centeredAnchorInModifier.xml", false);
      model.setTileSize(128);
      Set<Element> aliases = model.getElements();
      alias = null;
      alias2 = null;
      for (Element nAlias : aliases) {
        alias2 = alias;
        alias = nAlias;
      }
      project.addModel(model);
      projectDao.add(project);

      projectDao.evict(project);
      modelDao.evict(model);

      project = projectDao.getById(project.getId());
      model = new ModelFullIndexed(modelDao.getLastModelForProjectIdentifier(project.getProjectId(), false));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @After
  public void tearDown() throws Exception {
    try {
      projectDao.delete(project);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testGetAgregatedComments() throws Exception {
    try {
      commentService.addComment("John Doe", "a@a.pl", "Conteneta 1", model, new Point2D.Double(0, 1), alias, false,
          model);
      commentService.addComment("John Doe", "a@a.pl", "Contenetb 2", model, new Point2D.Double(0, 2), alias, false,
          model);
      commentService.addComment("John Doe", "a@a.pl", "Contenetc 3", model, new Point2D.Double(0, 3), alias2, false,
          model);
      commentService.addComment("John Doe", "a@a.pl", "Contenetc 4", model, new Point2D.Double(0, 4), null, false,
          model);
      commentService.addComment("John Doe", "a@a.pl", "Contenetc 5", model, new Point2D.Double(0, 5), null, false,
          model);
      CommentService service = new CommentService();
      service.setCommentDao(commentDao);
      List<List<Comment>> comments = service.getAgregatedComments(model, null);
      assertEquals(4, comments.size());
      int size = 0;
      for (List<Comment> list : comments) {
        size += list.size();
      }
      assertEquals(5, size);

      List<Comment> allComments = commentDao.getCommentByModel(model, null, null);
      assertEquals(5, allComments.size());
      for (Comment comment : allComments) {
        commentDao.delete(comment);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddComment() throws Exception {
    try {

      long counter = commentService.getCommentCount();
      Comment feedback = commentService.addComment("a", "b", "c", model, new Point2D.Double(0, 0), null, false, model);
      long counter2 = commentService.getCommentCount();
      assertEquals(counter + 1, counter2);
      commentDao.delete(feedback);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddCommentForReaction() throws Exception {
    try {

      long counter = commentService.getCommentCount();
      Comment feedback = commentService.addComment("a", "b", "c", model, new Point2D.Double(0, 0),
          model.getReactions().iterator().next(), false, model);
      long counter2 = commentService.getCommentCount();
      assertEquals(counter + 1, counter2);
      commentDao.delete(feedback);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddCommentForAlias() throws Exception {
    try {
      Element alias = model.getElementByElementId("sa1");
      long counter = commentService.getCommentCount();
      Comment feedback = commentService.addComment("a", "b", "c", model, new Point2D.Double(0, 0), alias, false, model);
      long counter2 = commentService.getCommentCount();
      assertEquals(counter + 1, counter2);
      commentDao.delete(feedback);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComparePointCommentId() throws Exception {
    try {
      CommentService cs = new CommentService();
      assertTrue(cs.equalPoints("Point2D.Double[117.685546875, 204.6923828125001]", "(117.69, 204.69)"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComparePointCommentId2() throws Exception {
    try {
      CommentService cs = new CommentService();
      assertFalse(cs.equalPoints("Point2D.Double[118.685546875, 204.6923828125001]", "(117.69, 204.69)"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComparePointCommentId3() throws Exception {
    try {
      CommentService cs = new CommentService();
      assertFalse(cs.equalPoints("Point2D.Double[117.685546875, 205.6923828125001]", "(117.69, 204.69)"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
