package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.annotation.Rollback;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.BasicPrivilege;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ObjectPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ILdapService;

@Rollback(true)
public class UserServiceTest extends ServiceTestFunctions {
  static Logger logger = Logger.getLogger(UserServiceTest.class);

  @Before
  public void setUp() throws Exception {
    createUser();
    logService.setLoggedUser(user);
  }

  @After
  public void tearDown() throws Exception {
    userDao.delete(user);
  }

  @Test
  public void testLogin() {
    try {
      assertNull(userService.login("john.doe", "incorrect password"));
      assertNotNull(userService.login("john.doe", "passwd"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testLoginFromLdap() throws Exception {
    ILdapService originalLdapService = userService.getLdapService();
    try {
      String login = "john.doe.test";
      String passwd = "test_passwd";

      assertNull(userService.getUserByLogin(login));

      LdapService ldapService = createMockLdapService("testFiles/ldap/john-doe-test-example.ldif", login, passwd);

      userService.setLdapService(ldapService);
      assertNull(userService.login(login, "incorrect password"));
      assertNotNull("User from LDAP wasn't authenticated", userService.login(login, passwd));

      User user = userService.getUserByLogin(login);
      assertNotNull("After authentication from LDAP user is not present in the system", user);
      assertTrue(user.isConnectedToLdap());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      userService.setLdapService(originalLdapService);
    }
  }

  private LdapService createMockLdapService(String filename, String login, String passwd) throws LDAPException {
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_BASE_DN, "dc=uni,dc=lu");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_OBJECT_CLASS, "person");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER, "memberof=cn=gitlab,cn=groups,cn=accounts,dc=uni,dc=lu");

    LdapService ldapService = Mockito.spy(LdapService.class);
    ldapService.setConfigurationService(configurationService);
    Mockito.when(ldapService.getConnection()).thenAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=" + login + ",cn=users,cn=accounts,dc=uni,dc=lu", passwd);
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, filename);
        ds.startListening();
        return ds.getConnection();
      }
    });
    return ldapService;
  }

  @Test
  public void testLoginWithNull() {
    try {
      assertNull(userService.login("john.doe", null));
      assertNull(userService.login(null, "passwd"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUserHasPrivilegeUserPrivilegeType1() {
    try {
      assertFalse(userService.userHasPrivilege(user, PrivilegeType.ADD_MAP));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUserHasPrivilegeUserPrivilegeType3() {
    try {
      userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT);
      fail("An exception should occure. VIEW_MAP must be connected with object");
    } catch (Exception e) {

    }
  }

  @Test
  public void testUserHasPrivilegeUserPrivilegeType2() {
    try {
      BasicPrivilege privilege = new BasicPrivilege();
      privilege.setLevel(1);
      privilege.setType(PrivilegeType.ADD_MAP);
      userService.setUserPrivilege(user, privilege);
      assertTrue(userService.userHasPrivilege(user, PrivilegeType.ADD_MAP));

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknown exception");
    }
  }

  @Test
  public void testUserHasPrivilegeUserPrivilegeTypeObject() {
    try {
      Project project = new Project();
      projectDao.add(project);

      assertFalse(userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT, project));
      try {
        userService.userHasPrivilege(user, PrivilegeType.ADD_MAP, project);
        fail("An exception should occure. ADD_MAP must not be connected with object");
      } catch (Exception e) {

      }

      try {
        userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT, user);
        fail("An exception should occure. VIEW_MAP must be connected with Model type");
      } catch (Exception e) {

      }

      ObjectPrivilege privilege = new ObjectPrivilege();
      privilege.setLevel(1);
      privilege.setType(PrivilegeType.VIEW_PROJECT);
      privilege.setIdObject(project.getId());
      userService.setUserPrivilege(user, privilege);

      assertTrue(userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT, project));

      privilege = new ObjectPrivilege(project, 0, PrivilegeType.VIEW_PROJECT, user);
      userService.setUserPrivilege(user, privilege);

      assertFalse(userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT, project));

      projectDao.delete(project);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknown exception");
    }
  }

  @Test
  public void testUserHasPrivilegeForDefaultProjectWithoutSetting() {
    assertFalse(userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT, null));
  }

  @Test
  public void testAddUser() throws Exception {
    try {
      long logCount = logDao.getCount();
      User user2 = new User();
      user2.setLogin("login");
      userService.addUser(user2);
      assertNotNull(user2.getId());
      long logCount2 = logDao.getCount();
      assertEquals("Log entry is missing for add user event", logCount + 1, logCount2);
      userDao.evict(user2);
      User user3 = userService.getUserByLogin(user2.getLogin());
      assertNotNull(user3);
      userService.deleteUser(user3);
      user3 = userService.getUserByLogin(user2.getLogin());
      assertNull(user3);

      long logCount3 = logDao.getCount();
      assertEquals("Log entry is missing for remove user event", logCount2 + 1, logCount3);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
