package lcsb.mapviewer.services.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import lcsb.mapviewer.services.ServiceTestFunctions;

public class EmailSenderTest extends ServiceTestFunctions {
  Logger logger = Logger.getLogger(EmailSenderTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  @Ignore("This is just a showcase")
  public void test() throws Exception {
    try {
      EmailSender emailSender = new EmailSender(configurationService);
      List<String> recipients = new ArrayList<>();
      recipients.add("piotr.gawron@uni.lu");
      emailSender.sendEmail(recipients, new ArrayList<>(), "Test subject", "Test content");
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
