package lcsb.mapviewer.services.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.utils.ColorSchemaXlsxReader;

public class ColorSchemaReaderXlsTest extends ServiceTestFunctions {
	Logger	logger	= Logger.getLogger(ColorSchemaReaderXlsTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReadSchema() throws Exception {
		try {
			ColorSchemaXlsxReader reader = new ColorSchemaXlsxReader();

			Collection<ColorSchema> schemas = null;
			
			schemas = reader.readColorSchema("testFiles/enricoData/enricoData.xls", "ageing" );

			assertNotNull(schemas);
			assertEquals(412, schemas.size());

			schemas = reader.readColorSchema("testFiles/enricoData/enricoData.xls", "ge001");

			assertNotNull(schemas);
			assertEquals(3057, schemas.size());

			schemas = reader.readColorSchema("testFiles/enricoData/enricoData.xls", "ge005");

			assertNotNull(schemas);
			assertEquals(4338, schemas.size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReadSchema2() throws Exception {
		try {
			ColorSchemaXlsxReader reader = new ColorSchemaXlsxReader();

			Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/coloring.xls", "goodSchema");

			assertNotNull(schemas);
			assertEquals(3, schemas.size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReadSchema3() throws Exception {
		try {
			try {
				ColorSchemaXlsxReader reader = new ColorSchemaXlsxReader();
				reader.readColorSchema("testFiles/coloring/coloring.xls", "wrongSchema");
				fail("Excepion expected");
			} catch (InvalidColorSchemaException e) {
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testProblematicStephanSchema3() throws Exception {
		try {
			ColorSchemaXlsxReader reader = new ColorSchemaXlsxReader();
			Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/coloring.xls", "problematicSchema");
			assertNotNull(schemas);
			assertEquals(329, schemas.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReadReactionSchema() throws Exception {
		try {
			ColorSchemaXlsxReader reader = new ColorSchemaXlsxReader();
			Collection<ColorSchema> collection = reader.readColorSchema("testFiles/coloring/coloring.xls", "reactionSchema");
			assertEquals(1, collection.size());
			ColorSchema schema = collection.iterator().next();
			assertEquals("re1", schema.getElementId());
			assertEquals(3.0, schema.getLineWidth(), EPSILON);
			assertEquals(Color.RED, schema.getColor());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(timeout=15000)
	public void testNextVersionReadSchema() throws Exception {
		try {
			ColorSchemaXlsxReader reader = new ColorSchemaXlsxReader();

			Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/coloring.xls", "goodLayout.v=1.0");

			assertNotNull(schemas);
			assertEquals(3, schemas.size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
