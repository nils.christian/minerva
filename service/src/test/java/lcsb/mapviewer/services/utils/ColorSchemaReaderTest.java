package lcsb.mapviewer.services.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class ColorSchemaReaderTest extends ServiceTestFunctions {
  Logger logger = Logger.getLogger(ColorSchemaReaderTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testReadSchema() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/enricoData/ageing.txt");

      assertNotNull(schemas);
      assertEquals(412, schemas.size());

      schemas = reader.readColorSchema("testFiles/enricoData/ge001.txt");

      assertNotNull(schemas);
      assertEquals(3057, schemas.size());

      schemas = reader.readColorSchema("testFiles/enricoData/ge005.txt");

      assertNotNull(schemas);
      assertEquals(4338, schemas.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReadGeneVariantsSchema() throws Exception {
    try {
      File f = new File("testFiles/coloring/gene_variants.txt");
      InputStream in = new FileInputStream(f);

      byte[] buff = new byte[8000];

      int bytesRead = 0;

      ByteArrayOutputStream bao = new ByteArrayOutputStream();

      while ((bytesRead = in.read(buff)) != -1) {
        bao.write(buff, 0, bytesRead);
      }
      in.close();
      bao.close();

      byte[] data = bao.toByteArray();

      ByteArrayInputStream bin = new ByteArrayInputStream(data);

      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema(bin,
          TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
      assertNotNull(schemas);
      assertEquals(3, schemas.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReadGeneVariantsSchemaWithAF() throws Exception {
    try {
      File f = new File("testFiles/coloring/gene_variants_all_freq.txt");
      InputStream in = new FileInputStream(f);

      byte[] buff = new byte[8000];

      int bytesRead = 0;

      ByteArrayOutputStream bao = new ByteArrayOutputStream();

      while ((bytesRead = in.read(buff)) != -1) {
        bao.write(buff, 0, bytesRead);
      }
      in.close();
      bao.close();

      byte[] data = bao.toByteArray();

      ByteArrayInputStream bin = new ByteArrayInputStream(data);

      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema(bin,
          TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
      assertNotNull(schemas);
      assertEquals(2, schemas.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReadInvalidGeneVariantsSchema() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();

      reader.readColorSchema("testFiles/coloring/gene_variants_invalid_genome.txt");
      fail("Exception expected");
    } catch (InvalidColorSchemaException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReadSchema2() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/goodSchema.txt");

      assertNotNull(schemas);
      assertEquals(3, schemas.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReadSchema3() throws Exception {
    try {
      try {
        ColorSchemaReader reader = new ColorSchemaReader();
        reader.readColorSchema("testFiles/coloring/wrongSchema.txt");
        fail("Excepion expected");
      } catch (InvalidColorSchemaException e) {
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProblematicStephanSchema3() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      reader.readColorSchema("testFiles/coloring/problematicSchema.txt");
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReadReactionSchema() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> collection = reader.readColorSchema("testFiles/coloring/reactionSchema.txt");
      assertEquals(1, collection.size());
      ColorSchema schema = collection.iterator().next();
      assertEquals("re1", schema.getElementId());
      assertEquals(3.0, schema.getLineWidth(), EPSILON);
      assertEquals(Color.RED, schema.getColor());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(timeout = 15000)
  public void testNextVersionReadSchema() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/goodLayout.v=1.0.txt");

      assertNotNull(schemas);
      assertEquals(3, schemas.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testColoring3() throws Exception {
    try {
      ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE);

      Model model = getModelForFile("testFiles/coloring/protein_to_color.xml", false);

      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/problematicSchema.txt");
      ColorModelCommand factory = new ColorModelCommand(model, schemas, colorExtractor);
      factory.execute();

      assertFalse(model.getElementByElementId("sa1").getColor().equals(Color.WHITE));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testColoringWithValueOrColor() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Map<String, String> params = new HashMap<>();
      params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

      String input = "name\tcolor\tvalue\n" + //
          "s1\t#ff0000\t\n" + //
          "s2\t\t1.0\n";
      Collection<ColorSchema> schemas = reader
          .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
      assertEquals(2, schemas.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testElementsByType() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Map<String, String> params = new HashMap<>();
      params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

      String input = "type\tname\tvalue\n" + //
          "protein\t\t1.0\n";
      Collection<ColorSchema> schemas = reader
          .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
      assertEquals(1, schemas.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testColoringWithInvalidValueAndColor() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Map<String, String> params = new HashMap<>();
      params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

      String input = "name\tcolor\tvalue\ns1\t#ff0000\t1.0";
      reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
      fail("Exception expected");
    } catch (InvalidColorSchemaException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testColoringWithInvalidValueAndColor2() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Map<String, String> params = new HashMap<>();
      params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

      String input = "name\tcolor\tvalue\ns1\t\t";
      reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSchemasWithId() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/schemaWithIdentifiers.txt");
      for (ColorSchema colorSchema : schemas) {
        for (MiriamData md : colorSchema.getMiriamData()) {
          assertNotNull(md.getResource());
          assertFalse(md.getResource().isEmpty());
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSchemasWithEmptyNameAndId() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();

      Collection<ColorSchema> schemas = reader.readColorSchema("testFiles/coloring/schemaIdWithoutName.txt");
      for (ColorSchema colorSchema : schemas) {
        for (MiriamData md : colorSchema.getMiriamData()) {
          assertFalse(md.getResource().isEmpty());
          assertNull(colorSchema.getName());
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSimpleNameSchemas() throws Exception {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      String input = "#header\ns1\ns2\n";
      Collection<ColorSchema> schemas = reader
          .readSimpleNameColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));
      assertEquals(2, schemas.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
