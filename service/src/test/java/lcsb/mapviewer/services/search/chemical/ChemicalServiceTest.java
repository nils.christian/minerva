package lcsb.mapviewer.services.search.chemical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;

public class ChemicalServiceTest extends ServiceTestFunctions {
	Logger logger = Logger.getLogger(ChemicalServiceTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testgetPDChemicalByName2() throws Exception {
		try {
			MiriamData pdDiseaseID = new MiriamData(MiriamType.MESH_2012, "D010300");
			String name = "Amphetamine";
			Chemical result = chemicalService.getByName(name, new DbSearchCriteria().disease(pdDiseaseID));
			assertNotNull(result);
			assertEquals("D000661", result.getChemicalId().getResource());

			assertEquals("No warnings expected.", 0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSearchByElements4() throws Exception {
		try {
			List<Element> elements = new ArrayList<>();
			Rna protein = new Rna("id");
			protein.setName("GDNF");
			elements.add(protein);

			List<Chemical> chemicals = chemicalService.getForTargets(elements, new DbSearchCriteria().disease(new MiriamData(MiriamType.MESH_2012, "D010300")));
			assertNotNull(chemicals);
			assertTrue(chemicals.size() > 0);

			assertEquals("No warnings expected.", 0, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

}
