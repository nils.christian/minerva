package lcsb.mapviewer.services.search;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class ElementMatcherTest extends ServiceTestFunctions {

  ElementMatcher elementMatcher = new ElementMatcher();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testMatchOther() {
    Target target = new Target();

    target.setType(TargetType.OTHER);
    assertFalse(elementMatcher.elementMatch(target, new GenericProtein("s1")));
  }

  @Test
  public void testElementMatch() {
    String geneName = "GDNF";
    Chemical view = chemicalService.getByName("Amphetamine",
        new DbSearchCriteria().disease(new MiriamData(MiriamType.MESH_2012, "D010300")));
    Target target = null;
    for (Target t : view.getInferenceNetwork()) {
      for (MiriamData row : t.getGenes()) {
        if (row.getResource().equals(geneName)) {
          target = t;
        }
      }
    }

    Element element = new Rna("id");
    element.setName(geneName);

    assertTrue(elementMatcher.elementMatch(target, element));

  }

}
