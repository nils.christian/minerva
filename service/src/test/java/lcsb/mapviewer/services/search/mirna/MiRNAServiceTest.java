package lcsb.mapviewer.services.search.mirna;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.mirna.IMiRNAService;

public class MiRNAServiceTest extends ServiceTestFunctions {
  Logger logger = Logger.getLogger(MiRNAServiceTest.class);

  @Autowired
  protected IMiRNAService miRNAService;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCacheStub() throws Exception {
    try {
      // CellDesignerXmlParser parser = new CellDesignerXmlParser();
      // Model model = parser.createModel(new
      // ConverterParams().filename("testFiles/pd_full/PD_130909.xml"));
      Model model = new ModelFullIndexed(null);
      miRNAService.cacheDataForModel(model, new IProgressUpdater() {
        @Override
        public void setProgress(double progress) {
          logger.debug("Progress: " + progress);
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
