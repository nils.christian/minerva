package lcsb.mapviewer.services.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.SearchIndex;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class is responsible for seqrch queries. It allows to create an index
 * for an alias and a query this index in a way.
 * 
 * @author Piotr Gawron
 * 
 */
public class SearchIndexer {

	/**
	 * Base score of the index generated for alias id. It will be used as a shift
	 * for {@link SearchIndex#weight} value.
	 */
	private static final int SEARCH_SCORE_LEVEL_FOR_ALIAS_ID							= 60;

	/**
	 * Base score of the index generated for {@link Element#formerSymbols}. It will
	 * be used as a shift for {@link SearchIndex#weight} value.
	 */
	private static final int SEARCH_SCORE_LEVEL_FOR_SPECIES_FORMER_SYMBOL	= 75;

	/**
	 * Base score of the index generated for full name. It will be used as a shift
	 * for {@link SearchIndex#weight} value.
	 */
	private static final int SEARCH_SCORE_LEVEL_FOR_SPECIES_FULL_NAME			= 80;

	/**
	 * Base score of the index generated for synonims. It will be used as a shift
	 * for {@link SearchIndex#weight} value.
	 */
	private static final int SEARCH_SCORE_LEVEL_FOR_SPECIES_SYNONIM				= 90;

	/**
	 * Base score of the index generated for name. It will be used as a shift for
	 * {@link SearchIndex#weight} value.
	 */
	private static final int SEARCH_SCORE_LEVEL_FOR_SPECIES_NAME					= 100;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger					 logger																				= Logger.getLogger(SearchIndexer.class);

	/**
	 * This method create list of indexes for an alias. Right now every index is
	 * one of the following indexed strings (from most important to less
	 * important): <br/>
	 * <ul>
	 * <li>species name,</li>
	 * <li>synonyms (only for proteins annotated by vencata server),</li>
	 * <li>common names (only for proteins annotated by vencata server),</li>
	 * <li>former symbols,</li>
	 * <li>speciesID,</li>
	 * <li>aliasID,</li>
	 * </ul>
	 * 
	 * @param species
	 *          object to be indexed
	 * @return list of indexes for an alias
	 */
	public List<SearchIndex> createIndexForAlias(Element species) {
		List<SearchIndex> result = new ArrayList<>();
		String id1 = species.getElementId();
		result.add(new SearchIndex(getIndexStringForString(id1), SEARCH_SCORE_LEVEL_FOR_ALIAS_ID));
		String id3 = species.getName();

		result.add(new SearchIndex(getIndexStringForString(id3), SEARCH_SCORE_LEVEL_FOR_SPECIES_NAME));

		for (String string : species.getSynonyms()) {
			result.add(new SearchIndex(getIndexStringForString(string), SEARCH_SCORE_LEVEL_FOR_SPECIES_SYNONIM));
		}
		for (String string : species.getFormerSymbols()) {
			result.add(new SearchIndex(getIndexStringForString(string), SEARCH_SCORE_LEVEL_FOR_SPECIES_FORMER_SYMBOL));
		}
		String name = species.getFullName();
		if (name != null && !name.equals("")) {
			result.add(new SearchIndex(getIndexStringForString(name), SEARCH_SCORE_LEVEL_FOR_SPECIES_FULL_NAME));
		}
		return result;
	}

	/**
	 * This method transform a string into indexed string version. Right now the
	 * indexed version is a lowercase string which contains only alphanumerical
	 * characters.
	 * 
	 * @param str
	 *          string for conversion
	 * @return indexed version of the input string
	 */
	private String getIndexStringForString(String str) {
		return str.toLowerCase().replaceAll("[^a-z0-9]", "");
	}

	/**
	 * This method transform a string into indexed query. AvailablePrefixes
	 * contains a collection of all possible prefixes for query string that should
	 * be ommitted.
	 * 
	 * @param originalQuery
	 *          query to be transformed
	 * @param availablePrefixes
	 *          collection of valid prefixes in the query
	 * 
	 * @return indexed string of the qoriginal query
	 */
	public String getQueryStringForIndex(String originalQuery, Collection<String> availablePrefixes) {
		String result = originalQuery;
		for (String string : availablePrefixes) {
			if (result.startsWith(string + ":")) {
				result = result.replaceFirst(string + ":", "");
				break;
			}
		}

		return getIndexStringForString(result);
	}

	/**
	 * This method check if query match search index and return probability of the
	 * hit between <0,1> (0 - means query is not similar to index, 1 means perfect
	 * match) increased by the index weight if probability is higher than 0.
	 * 
	 * @param query
	 *          query to be checked (it should be already prepared in the same way
	 *          as index is)
	 * @param index
	 *          search index value to be checked
	 * @return probabily value (range <0,1>) of match between query and index
	 *         increased by the index weight
	 */
	public double match(String query, SearchIndex index) {
		double partialValue = 0;
		if (index.getValue().contains(query)) {
			partialValue = ((double) query.length()) / ((double) index.getValue().length());
		}
		if (partialValue > 0) {
			return index.getWeight() + partialValue;
		} else {
			return 0;
		}
	}

	/**
	 * Return type of elements that can be returned for the query.
	 * 
	 * @param string
	 *          query string.
	 * @param speciesSearchReversePrefix
	 *          map containing information about possible prefixes used in the
	 *          query
	 * @return class of the element that might exist in the result
	 */
	public Class<? extends Element> getTypeForQuery(String string, Map<String, Class<? extends Element>> speciesSearchReversePrefix) {
		String result = string;
		for (Entry<String, Class<? extends Element>> entry : speciesSearchReversePrefix.entrySet()) {
			if (result.startsWith(entry.getKey() + ":")) {
				return entry.getValue();
			}
		}
		return Element.class;
	}
}
