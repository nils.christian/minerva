package lcsb.mapviewer.services.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.model.celldesigner.species.SpeciesMapping;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

/**
 * Class that reads information about set of {@link ColorSchema color schemas}
 * from the input file.
 * 
 * @author Piotr Gawron
 * 
 */
public class ColorSchemaXlsxReader {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ColorSchemaXlsxReader.class);

  /**
   * @param fileName
   *          file name
   * @param sheetName
   *          sheet name.
   * @return list of ColorSchema.
   * @throws IOException
   *           exception related to opening the input stream.
   * @throws InvalidColorSchemaException
   *           invalid color schema exception.
   */
  public Collection<ColorSchema> readColorSchema(String fileName, String sheetName)
      throws IOException, InvalidColorSchemaException {
    ColorParser colorParser = new ColorParser();
    List<ColorSchema> result = new ArrayList<>();
    FileInputStream file = null;
    Workbook workbook = null;

    try {
      file = new FileInputStream(fileName);
      // Using XSSF for xlsx format, for xls use HSSF
      // <-Interface, accepts both HSSF and XSSF.
      if (FilenameUtils.getExtension(fileName).equalsIgnoreCase("xls")) {
        workbook = new HSSFWorkbook(file);
      } else if (FilenameUtils.getExtension(fileName).equalsIgnoreCase("xlsx")) {
        workbook = new XSSFWorkbook(file);
      } else {
        throw new IllegalArgumentException("Received file does not have a standard excel extension.");
      }
      Sheet sheet = null;
      if (sheetName == null) {
        sheet = workbook.getSheetAt(0);
      } else {
        sheet = workbook.getSheet(sheetName);
      }
      DataFormat fmt = workbook.createDataFormat();
      CellStyle textStyle = workbook.createCellStyle();
      textStyle.setDataFormat(fmt.getFormat("@"));

      Iterator<Row> rowIterator = sheet.iterator();
      Integer valueColumn = null;
      Integer colorColumn = null;
      Integer nameColumn = null;
      Integer modelNameColumn = null;
      Integer identifierColumn = null;
      Integer elementIdentifierColumn = null;
      Integer compartmentColumn = null;
      Integer typeColumn = null;
      Integer lineWidthColumn = null;
      Integer reverseReactionColumn = null;
      List<Pair<MiriamType, Integer>> foundCustomIdentifiers = new ArrayList<Pair<MiriamType, Integer>>();
      int lineIndex = 0;
      Map<ColorSchemaColumn, Integer> foundSchemaColumns = new HashMap<ColorSchemaColumn, Integer>();
      while (rowIterator.hasNext()) {
        Row row = rowIterator.next();
        Cell cell = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
        if (cell == null) {
          continue;
        } else {
          cell.setCellType(Cell.CELL_TYPE_STRING);
          if (cell.getStringCellValue().startsWith("#")) {
            continue;
          }
        }
        lineIndex++;
        if (lineIndex == 1) {

          Map<String, MiriamType> acceptableIdentifiers = new HashMap<String, MiriamType>();
          for (MiriamType type : MiriamType.values()) {
            acceptableIdentifiers.put(type.getCommonName().toLowerCase(), type);
          }

          Iterator<Cell> cellIterator = row.cellIterator();
          int columnIndex = 0;
          while (cellIterator.hasNext()) {
            columnIndex++;
            sheet.setDefaultColumnStyle(columnIndex, textStyle);
            cell = cellIterator.next();
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String value = cell.getStringCellValue();
            boolean found = false;
            for (ColorSchemaColumn schemaColumn : ColorSchemaColumn.values()) {
              if (value.trim().equalsIgnoreCase(schemaColumn.getTitle())) {
                foundSchemaColumns.put(schemaColumn, columnIndex - 1);
                found = true;
                break;
              }
            }
            if (!found) {
              if (acceptableIdentifiers.keySet().contains(value.toLowerCase())) {
                foundCustomIdentifiers.add(
                    new Pair<MiriamType, Integer>(acceptableIdentifiers.get(value.toLowerCase()), columnIndex - 1));
              } else {
                String columnNames = "";
                for (ColorSchemaColumn schemaColumn : ColorSchemaColumn.values()) {
                  columnNames += schemaColumn.getTitle() + ", ";
                }
                for (String string : acceptableIdentifiers.keySet()) {
                  columnNames += ", " + string;
                }
                throw new InvalidColorSchemaException(
                    "Unknown column type: " + value + ". Acceptable column name: " + columnNames);
              }
            }
          }
          valueColumn = foundSchemaColumns.get(ColorSchemaColumn.VALUE);
          colorColumn = foundSchemaColumns.get(ColorSchemaColumn.COLOR);
          nameColumn = foundSchemaColumns.get(ColorSchemaColumn.NAME);
          modelNameColumn = foundSchemaColumns.get(ColorSchemaColumn.MODEL_NAME);
          identifierColumn = foundSchemaColumns.get(ColorSchemaColumn.IDENTIFIER);
          elementIdentifierColumn = foundSchemaColumns.get(ColorSchemaColumn.ELEMENT_IDENTIFIER);
          if (elementIdentifierColumn == null) {
            elementIdentifierColumn = foundSchemaColumns.get(ColorSchemaColumn.REACTION_IDENTIFIER);
          }
          compartmentColumn = foundSchemaColumns.get(ColorSchemaColumn.COMPARTMENT);
          typeColumn = foundSchemaColumns.get(ColorSchemaColumn.TYPE);
          lineWidthColumn = foundSchemaColumns.get(ColorSchemaColumn.LINE_WIDTH);
          reverseReactionColumn = foundSchemaColumns.get(ColorSchemaColumn.REVERSE_REACTION);

          if (valueColumn != null && colorColumn != null) {
            throw new InvalidColorSchemaException("Schema can contain only one of these two columns: ");
          }

          if (nameColumn == null && identifierColumn == null && foundCustomIdentifiers.size() == 0
              && elementIdentifierColumn == null) {
            throw new InvalidColorSchemaException(
                "One of these columns is obligatory: " + ColorSchemaColumn.NAME.getTitle() + ","
                    + ColorSchemaColumn.IDENTIFIER.getTitle() + "," + ColorSchemaColumn.ELEMENT_IDENTIFIER.getTitle());
          }

          if (valueColumn == null && colorColumn == null) {
            throw new InvalidColorSchemaException("Schema must contain one of these two columns: value, name");
          }

        } else {
          ColorSchema schema = new GenericColorSchema();
          if (nameColumn != null) {
            schema.setName(row.getCell(nameColumn).getStringCellValue());
          }
          if (modelNameColumn != null) {
            schema.setModelName(row.getCell(modelNameColumn).getStringCellValue());
          }
          if (valueColumn != null) {
            try {
              cell = row.getCell(valueColumn);
              cell.setCellType(Cell.CELL_TYPE_STRING);
              schema.setValue(Double.parseDouble(cell.getStringCellValue().replace(",", ".")));
            } catch (Exception e) {
              throw new InvalidColorSchemaException(
                  "[Line " + lineIndex + "] Problem with parsing value for value column. Cell value" + cell);
            }
            if (schema.getValue() > 1 + Configuration.EPSILON || schema.getValue() < -1 - Configuration.EPSILON) {
              throw new InvalidColorSchemaException("[Line " + lineIndex + "] Value " + schema.getValue()
                  + " out of range. Only values between -1 and 1 are allowed.");
            }
          }
          if (compartmentColumn != null) {
            String value = row.getCell(compartmentColumn).getStringCellValue();
            if (value != null) {
              String[] compartments = value.split(",");
              schema.addCompartments(compartments);
            }
          }
          if (typeColumn != null) {
            String value = row.getCell(typeColumn).getStringCellValue();
            if (value != null) {
              String[] types = value.split(",");
              for (String string : types) {
                SpeciesMapping mapping = SpeciesMapping.getMappingByString(string);
                if (mapping != null) {
                  schema.addType(mapping.getModelClazz());
                } else {
                  throw new InvalidColorSchemaException("Unknown class type: " + string + ".");
                }
              }
            }
          }
          if (colorColumn != null) {
            schema.setColor(colorParser.parse(row.getCell(colorColumn).getStringCellValue()));
          }
          if (elementIdentifierColumn != null) {
            schema.setElementId(row.getCell(elementIdentifierColumn).getStringCellValue());
          }
          if (lineWidthColumn != null) {
            cell = row.getCell(lineWidthColumn);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String value = cell.getStringCellValue();
            if (value != null && !value.trim().isEmpty()) {
              try {
                schema.setLineWidth(Double.parseDouble(value.replace(",", ".")));
              } catch (NumberFormatException e) {
                throw new InvalidColorSchemaException(
                    "[Line " + lineIndex + "] Problem with parsing value: \"" + value + "\"");
              }
            }
          }
          if (reverseReactionColumn != null) {
            cell = row.getCell(reverseReactionColumn);
            if (cell != null) {
              cell.setCellType(Cell.CELL_TYPE_STRING);
              schema.setReverseReaction("true".equalsIgnoreCase(cell.getStringCellValue()));
            }
          }
          if (identifierColumn != null) {
            cell = row.getCell(identifierColumn);
            if (cell != null && !cell.getStringCellValue().trim().isEmpty()) {
              MiriamConnector miriamConnector = new MiriamConnector();
              String value = cell.getStringCellValue().trim();
              if (miriamConnector.isValidIdentifier(value)) {
                schema.addMiriamData(MiriamType.getMiriamDataFromIdentifier(value));
              } else {
                throw new InvalidColorSchemaException("[Line " + lineIndex + "]" + " Invalid identifier: " + value);
              }
            }
          }
          for (Pair<MiriamType, Integer> pair : foundCustomIdentifiers) {
            cell = row.getCell(pair.getRight());
            if (cell != null) {
              schema.addMiriamData(new MiriamData(pair.getLeft(), cell.getStringCellValue()));
            }
          }

          if ((schema.getValue() != null && schema.getColor() != null)
              || (schema.getValue() == null && schema.getColor() == null)) {
            throw new InvalidColorSchemaException("Value or Color is needed not both");
          }

          if (schema.getName() == null && schema.getMiriamData().size() == 0 && foundCustomIdentifiers.size() == 0
              && schema.getElementId() == null) {
            throw new InvalidColorSchemaException(
                "One of these columns values is obligatory: name, identifier, reactionIdentifier");
          }
          result.add(schema);
        }
      }
    } finally {
      try {
        if (file != null) {
          file.close();
        }
        if (workbook != null) {
          workbook.close();
        }
      } catch (Exception e) {

      }
    }
    return result;

  }

  /**
   * Returns list of columns that should be printed for given coloring schemas.
   * 
   * @param schemas
   *          list of schemas
   * @return list of columns that should be printed (were set in the coloring
   *         schemas)
   */
  public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
    Set<ColorSchemaColumn> result = new HashSet<ColorSchemaColumn>();
    for (ColorSchema schema : schemas) {
      if (schema.getColor() != null) {
        result.add(ColorSchemaColumn.COLOR);
      }
      if (schema.getCompartments().size() > 0) {
        result.add(ColorSchemaColumn.COMPARTMENT);
      }
      if (schema.getMiriamData().size() > 0) {
        result.add(ColorSchemaColumn.IDENTIFIER);
      }
      if (schema.getLineWidth() != null) {
        result.add(ColorSchemaColumn.LINE_WIDTH);
      }
      if (schema.getName() != null) {
        result.add(ColorSchemaColumn.NAME);
      }
      if (schema.getModelName() != null) {
        result.add(ColorSchemaColumn.MODEL_NAME);
      }
      if (schema.getElementId() != null) {
        result.add(ColorSchemaColumn.ELEMENT_IDENTIFIER);
      }
      if (schema.getReverseReaction() != null) {
        result.add(ColorSchemaColumn.REVERSE_REACTION);
      }
      if (schema.getTypes().size() > 0) {
        result.add(ColorSchemaColumn.TYPE);
      }
      if (schema.getValue() != null) {
        result.add(ColorSchemaColumn.VALUE);
      }
    }
    return result;
  }

}
