package lcsb.mapviewer.services.utils.data;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;

/**
 * This enum defines which columns are available for defining
 * {@link lcsb.mapviewer.model.map.layout.ColorSchema}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ColorSchemaColumn {

  /**
   * Name of the element.
   */
  NAME("name", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Name of the element.
   */
  MODEL_NAME("model_name", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Value that will be transformed into new color.
   * 
   * @see ColorSchemaColumn#COLOR
   */
  VALUE("value", new ColorSchemaType[] { ColorSchemaType.GENERIC }), //

  /**
   * In which compartment the element should be located.
   */
  COMPARTMENT("compartment", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Class type of the element.
   */
  TYPE("type", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * New element/reaction color.
   */
  COLOR("color", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Identifier of the element.
   */
  IDENTIFIER("identifier", new ColorSchemaType[] { ColorSchemaType.GENERIC, ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Element identifier.
   */
  ELEMENT_IDENTIFIER("elementIdentifier", new ColorSchemaType[] { ColorSchemaType.GENERIC }), //
  
  /**
   * Element identifier.
   */
  @Deprecated
  REACTION_IDENTIFIER("reactionIdentifier", new ColorSchemaType[] { ColorSchemaType.GENERIC }), //

  /**
   * New line width of the reaction.
   */
  LINE_WIDTH("lineWidth", new ColorSchemaType[] { ColorSchemaType.GENERIC }), //

  /**
   * Position where gene variants starts.
   */
  POSITION("position", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Original DNA of the variant.
   */
  ORIGINAL_DNA("original_dna", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Alternative DNA of the variant.
   */
  ALTERNATIVE_DNA("alternative_dna", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Short description of the entry.
   */
  DESCRIPTION("description", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT, ColorSchemaType.GENERIC }), //

  /**
   * Variant references.
   */
  REFERENCES("references", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * What's the {@link ReferenceGenomeType}.
   */
  REFERENCE_GENOME_TYPE("reference_genome_type", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * {@link ReferenceGenome#version Version} of the reference genome.
   */
  REFERENCE_GENOME_VERSION("reference_genome_version", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Contig where variant was observed.
   */
  CONTIG("contig", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  ALLEL_FREQUENCY("allel_frequency", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  VARIANT_IDENTIFIER("variant_identifier", new ColorSchemaType[] { ColorSchemaType.GENETIC_VARIANT }), //

  /**
   * Should the direction of reaction be reversed.
   */
  REVERSE_REACTION("reverseReaction", new ColorSchemaType[] { ColorSchemaType.GENERIC }); //

  /**
   * Default constructor that creates enum entry.
   * 
   * @param title
   *          {@link #title}
   * @param types
   *          list of {@link ColumnType types} where this column is allowed
   */
  ColorSchemaColumn(String title, ColorSchemaType[] types) {
    this.title = title;
    for (ColorSchemaType colorSchemaType : types) {
      this.types.add(colorSchemaType);
    }
  }

  /**
   * Human readable title used in input file.
   */
  private String title;

  /**
   * Set of types where column is allowed.
   */
  private Set<ColorSchemaType> types = new HashSet<>();

  /**
   * 
   * @return {@link #title}
   */
  public String getTitle() {
    return title;
  }

  /**
   * @return the types
   * @see #types
   */
  public Set<ColorSchemaType> getTypes() {
    return types;
  }

}
