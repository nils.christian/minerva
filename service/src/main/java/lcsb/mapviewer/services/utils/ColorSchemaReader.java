package lcsb.mapviewer.services.utils;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.model.celldesigner.species.SpeciesMapping;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GeneVariation;
import lcsb.mapviewer.model.map.layout.GeneVariationColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;
import lcsb.mapviewer.services.utils.data.ColorSchemaType;

/**
 * Class that reads information about set of {@link ColorSchema color schemas}
 * from the input file.
 * 
 * @author Piotr Gawron
 * 
 */
public class ColorSchemaReader {

  /**
   * Defines number of gene variants per element that is a threshold for changing
   * color.
   */
  private static final int SATURATION_SIZE_OF_GENE_VARIANTS = 10;

  /**
   * Max value of red in a color.
   */
  private static final int MAX_RED_VALUE = 255;

  /**
   * Min value of red in a color when computing color for gene variants.
   */
  private static final int MIN_GV_RED_VALUE = 128;

  /**
   * Object that parses colors from string.
   */
  private ColorParser colorParser = new ColorParser();

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ColorSchemaReader.class);

  /**
   * Objects that manipulates {@link MiriamData}.
   */
  private MiriamConnector mc = new MiriamConnector();

  /**
   * Reads information about set of {@link ColorSchema color schemas} from the
   * input stream.
   * 
   * @param colorInputStream
   *          input stream with {@link ColorSchema}
   * @param params
   *          list of parameters that were parsed from file header (lines starting
   *          with '#')
   * @return list of coloring schemas
   * @throws IOException
   *           thrown when there is a problem with input stream
   * @throws InvalidColorSchemaException
   *           thrown when color schema is invalid
   */
  public Collection<ColorSchema> readColorSchema(InputStream colorInputStream, Map<String, String> params)
      throws IOException, InvalidColorSchemaException {
    if (params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE) == null) {
      if (Integer.valueOf(params.get(TextFileUtils.COLUMN_COUNT_PARAM)) == 1) {
        return readSimpleNameColorSchema(colorInputStream);
      } else {
        return readGenericColorSchema(colorInputStream);
      }
    } else {
      ColorSchemaType type = ColorSchemaType.valueOf(params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE));
      if (type == null) {
        logger.warn("Unknown type of layout file: " + params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE));
        return readGenericColorSchema(colorInputStream);
      }
      switch (type) {
      case GENERIC:
        return readGenericColorSchema(colorInputStream);
      case GENETIC_VARIANT:
        return readGeneticVariantColorSchema(colorInputStream, params);
      default:
        logger.warn("Layout type not implemented: " + type);
        return readGenericColorSchema(colorInputStream);
      }
    }
  }

  /**
   * Reads information about set of {@link GeneVariationColorSchema gene variant
   * color schemas} from the input stream.
   * 
   * @param colorInputStream
   *          input stream with {@link GeneVariationColorSchema}
   * @param params
   *          list of parameters that were parsed from file header (lines starting
   *          with '#')
   * @return list of coloring schemas
   * @throws IOException
   *           thrown when there is a problem with input stream
   * @throws InvalidColorSchemaException
   *           thrown when color schema is invalid
   */
  protected Collection<ColorSchema> readGeneticVariantColorSchema(InputStream colorInputStream,
      Map<String, String> params) throws IOException, InvalidColorSchemaException {
    List<ColorSchema> result = new ArrayList<>();
    BufferedReader br = new BufferedReader(new InputStreamReader(colorInputStream));
    String referenceGenomeVersionStr = params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_VERSION);
    ReferenceGenomeType genomeType = extractReferenceGenomeType(
        params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_TYPE));
    try {
      String line = br.readLine();
      int lineIndex = 1;
      while (line != null && line.startsWith("#")) {
        lineIndex++;
        line = br.readLine();
      }
      String[] columns = line.split("\t");

      Map<ColorSchemaColumn, Integer> schemaColumns = new HashMap<ColorSchemaColumn, Integer>();
      List<Pair<MiriamType, Integer>> customIdentifiers = new ArrayList<Pair<MiriamType, Integer>>();
      parseColumns(columns, schemaColumns, customIdentifiers, ColorSchemaType.GENETIC_VARIANT);
      Integer colorColumn = schemaColumns.get(ColorSchemaColumn.COLOR);
      Integer contigColumn = schemaColumns.get(ColorSchemaColumn.CONTIG);
      Integer nameColumn = schemaColumns.get(ColorSchemaColumn.NAME);
      Integer modelNameColumn = schemaColumns.get(ColorSchemaColumn.MODEL_NAME);
      Integer identifierColumn = schemaColumns.get(ColorSchemaColumn.IDENTIFIER);
      Integer variantIdentifierColumn = schemaColumns.get(ColorSchemaColumn.VARIANT_IDENTIFIER);
      Integer allelFrequencyColumn = schemaColumns.get(ColorSchemaColumn.ALLEL_FREQUENCY);
      Integer compartmentColumn = schemaColumns.get(ColorSchemaColumn.COMPARTMENT);
      Integer typeColumn = schemaColumns.get(ColorSchemaColumn.TYPE);
      Integer positionColumn = schemaColumns.get(ColorSchemaColumn.POSITION);
      Integer originalDnaColumn = schemaColumns.get(ColorSchemaColumn.ORIGINAL_DNA);
      Integer alternativeDnaColumn = schemaColumns.get(ColorSchemaColumn.ALTERNATIVE_DNA);
      Integer descriptionColumn = schemaColumns.get(ColorSchemaColumn.DESCRIPTION);
      Integer referencesColumn = schemaColumns.get(ColorSchemaColumn.REFERENCES);
      Integer referenceGenomeTypeColumn = schemaColumns.get(ColorSchemaColumn.REFERENCE_GENOME_TYPE);
      Integer referenceGenomeVersionColumn = schemaColumns.get(ColorSchemaColumn.REFERENCE_GENOME_VERSION);
      if (nameColumn == null && identifierColumn == null && customIdentifiers.size() == 0) {
        throw new InvalidColorSchemaException("One of these columns is obligatory: name, identifier");
      }
      if (contigColumn == null) {
        throw new InvalidColorSchemaException(ColorSchemaColumn.CONTIG.getTitle() + " column is obligatory");
      }
      if (positionColumn == null) {
        throw new InvalidColorSchemaException(ColorSchemaColumn.POSITION.getTitle() + " column is obligatory");
      }
      if (originalDnaColumn == null) {
        throw new InvalidColorSchemaException(ColorSchemaColumn.ORIGINAL_DNA.getTitle() + " column is obligatory");
      }
      if (referenceGenomeTypeColumn == null && genomeType == null) {
        throw new InvalidColorSchemaException(
            ColorSchemaColumn.REFERENCE_GENOME_TYPE.getTitle() + " column is obligatory");
      }
      if (referenceGenomeVersionColumn == null && referenceGenomeVersionStr == null) {
        throw new InvalidColorSchemaException(
            ColorSchemaColumn.REFERENCE_GENOME_VERSION.getTitle() + " column is obligatory");
      }
      lineIndex++;
      line = br.readLine();
      while (line != null) {
        lineIndex++;
        String errorPrefix = "[Line " + lineIndex + "]\t";
        if (!line.trim().equals("")) {
          String[] values = line.split("\t", -1);
          if (values.length != columns.length) {
            throw new InvalidColorSchemaException("[Line " + lineIndex + "] Wrong number of cells");
          }
          GeneVariationColorSchema schema = new GeneVariationColorSchema();
          if (nameColumn != null) {
            processNameColumn(schema, values[nameColumn]);
          }
          if (modelNameColumn != null) {
            processModelNameColumn(schema, values[modelNameColumn]);
          }
          if (compartmentColumn != null) {
            processCompartmentColumn(schema, values[compartmentColumn]);
          }
          if (typeColumn != null) {
            String[] types = values[typeColumn].split(",");
            for (String string : types) {
              SpeciesMapping mapping = SpeciesMapping.getMappingByString(string);
              if (mapping != null) {
                schema.addType(mapping.getModelClazz());
              } else {
                throw new InvalidColorSchemaException("[Line " + lineIndex + "] Unknown class type: " + string + ".");
              }
            }
          }
          if (colorColumn != null) {
            schema.setColor(colorParser.parse(values[colorColumn]));
          }
          if (identifierColumn != null && !values[identifierColumn].equals("")) {
            processGeneralIdentifier(values[identifierColumn], schema, errorPrefix);
          }
          if (descriptionColumn != null) {
            schema.setDescription(values[descriptionColumn]);
          }
          for (Pair<MiriamType, Integer> pair : customIdentifiers) {
            processIdentifier(values[pair.getRight()], pair.getLeft(), schema);
          }

          GeneVariation gv = new GeneVariation();
          if (positionColumn != null) {
            try {
              gv.setPosition(Long.parseLong(values[positionColumn]));
            } catch (NumberFormatException e) {
              throw new InvalidColorSchemaException(
                  "[Line " + lineIndex + "] Invalid position: " + values[positionColumn], e);
            }
          }
          if (originalDnaColumn != null) {
            gv.setOriginalDna(values[originalDnaColumn]);
          }
          if (allelFrequencyColumn != null) {
            gv.setAllelFrequency(values[allelFrequencyColumn]);
          }
          if (variantIdentifierColumn != null) {
            gv.setVariantIdentifier(values[variantIdentifierColumn]);
          }
          if (alternativeDnaColumn != null) {
            gv.setModifiedDna(values[alternativeDnaColumn]);
          }
          if (referencesColumn != null) {
            String[] references = values[referencesColumn].split(";");
            for (String string : references) {
              try {
                gv.addReference(MiriamType.getMiriamByUri(string));
              } catch (InvalidArgumentException e) {
                throw new InvalidColorSchemaException("[Line " + lineIndex + "] Invalid reference: " + string, e);
              }
            }
          }
          if (referenceGenomeTypeColumn != null) {
            gv.setReferenceGenomeType(extractReferenceGenomeType(values[referenceGenomeTypeColumn]));
          } else {
            gv.setReferenceGenomeType(genomeType);
          }
          if (referenceGenomeVersionColumn != null) {
            gv.setReferenceGenomeVersion(values[referenceGenomeVersionColumn]);
          } else {
            gv.setReferenceGenomeVersion(referenceGenomeVersionStr);
          }
          gv.setContig(values[contigColumn]);

          schema.addGeneVariation(gv);
          if (schema.getName().contains(";")) {
            String[] names = schema.getName().split(";");
            for (String string : names) {
              if (!string.trim().isEmpty()) {
                GeneVariationColorSchema schemaCopy = schema.copy();
                schemaCopy.setName(string);
                result.add(schemaCopy);
              }
            }
          } else {
            result.add(schema);
          }
        }
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return mergeSchemas(result);
  }

  private void processGeneralIdentifier(String string, ColorSchema schema, String errorPrefix)
      throws InvalidColorSchemaException {
    if (mc.isValidIdentifier(string)) {
      schema.addMiriamData(MiriamType.getMiriamDataFromIdentifier(string));
    } else {
      throw new InvalidColorSchemaException(errorPrefix + " Invalid identifier: " + string);
    }
  }

  /**
   * Sets proper value of identifier to {@link ColorSchema} from cell content.
   * 
   * @param schema
   *          {@link ColorSchema} where name should be set
   * @param type
   *          {@link MiriamType} type of the identifier
   * @param content
   *          content of the cell where identifier of given type is stored
   */
  private void processIdentifier(String content, MiriamType type, ColorSchema schema) {
    if (!content.isEmpty()) {
      schema.addMiriamData(new MiriamData(type, content));
    }
  }

  /**
   * Sets proper name to {@link ColorSchema} from cell content.
   * 
   * @param schema
   *          {@link ColorSchema} where name should be set
   * @param content
   *          content of the cell where name is stored
   */
  private void processNameColumn(ColorSchema schema, String content) {
    if (!content.isEmpty()) {
      schema.setName(content);
    }
  }

  private void processModelNameColumn(ColorSchema schema, String content) {
    if (!content.isEmpty()) {
      schema.setModelName(content);
    }
  }

  /**
   * Sets proper compartment names to {@link ColorSchema} from cell content.
   * 
   * @param schema
   *          {@link ColorSchema} where name should be set
   * @param content
   *          content of the cell where compartments are stored
   */
  private void processCompartmentColumn(ColorSchema schema, String content) {
    for (String string : content.split(",")) {
      if (!string.isEmpty()) {
        schema.addCompartment(string);
      }
    }
  }

  /**
   * Transforms string into {@link ReferenceGenomeType}.
   * 
   * @param referenceGenomeStr
   *          type as a string
   * @return {@link ReferenceGenomeType} obtained from input string
   * @throws InvalidColorSchemaException
   *           thrown when input string cannot be resolved into
   *           {@link ReferenceGenomeType}
   */
  protected ReferenceGenomeType extractReferenceGenomeType(String referenceGenomeStr)
      throws InvalidColorSchemaException {
    ReferenceGenomeType genomeType = null;
    if (referenceGenomeStr != null) {
      try {
        genomeType = ReferenceGenomeType.valueOf(referenceGenomeStr);
      } catch (IllegalArgumentException e) {
        throw new InvalidColorSchemaException("Unknown genome type: " + referenceGenomeStr + ". Acceptable values: "
            + StringUtils.join(ReferenceGenomeType.values(), ","), e);
      }
    }
    return genomeType;
  }

  /**
   * Merges collection of {@link ColorSchema} that might contain duplicate names
   * into collection that doesn't have duplicate names.
   * 
   * @param schemas
   *          {@link Collection} of {@link ColorSchema} that might contain
   *          duplicate name
   * @return {@link Collection} of {@link ColorSchema} that doesn't contain
   *         duplicate names
   * 
   */
  private Collection<ColorSchema> mergeSchemas(Collection<ColorSchema> schemas) {
    Map<String, ColorSchema> schemasByName = new HashMap<>();
    for (ColorSchema colorSchema : schemas) {
      ColorSchema mergedSchema = schemasByName.get(colorSchema.getName());
      if (mergedSchema == null) {
        mergedSchema = colorSchema.copy();
        schemasByName.put(colorSchema.getName(), mergedSchema);
      } else {
        if (mergedSchema instanceof GeneVariationColorSchema) {
          if (colorSchema instanceof GeneVariationColorSchema) {
            ((GeneVariationColorSchema) mergedSchema)
                .addGeneVariations(((GeneVariationColorSchema) colorSchema).getGeneVariations());
          } else {
            throw new NotImplementedException(
                "Merge between classes not imeplemented:" + mergedSchema.getClass() + "," + colorSchema.getClass());
          }
        } else {
          throw new NotImplementedException(
              "Merge between classes not imeplemented:" + mergedSchema.getClass() + "," + colorSchema.getClass());
        }
      }
    }
    for (ColorSchema colorSchema : schemasByName.values()) {
      if (colorSchema instanceof GeneVariationColorSchema) {
        colorSchema.setColor(getGeneVariantsColor(((GeneVariationColorSchema) colorSchema).getGeneVariations()));
      }
    }
    return schemasByName.values();
  }

  /**
   * Gets color that should be assigned to {@link GeneVariationColorSchema}.
   * 
   * @param geneVariations
   *          list of viariants
   * @return {@link Color} that should be assigned to
   *         {@link GeneVariationColorSchema}
   */
  private Color getGeneVariantsColor(List<GeneVariation> geneVariations) {
    int size = geneVariations.size();
    if (size >= SATURATION_SIZE_OF_GENE_VARIANTS) {
      return Color.RED;
    } else {
      double ratio = (double) size / (double) SATURATION_SIZE_OF_GENE_VARIANTS;
      return new Color((int) (MIN_GV_RED_VALUE + ratio * (MAX_RED_VALUE - MIN_GV_RED_VALUE)), 0, 0);
    }
  }

  /**
   * Reads information about set of {@link GenericColorSchema generic color
   * schemas} from the input stream.
   * 
   * @param colorInputStream
   *          input stream with {@link GenericColorSchema}
   * @return list of coloring schemas
   * @throws IOException
   *           thrown when there is a problem with input stream
   * @throws InvalidColorSchemaException
   *           thrown when color schema is invalid
   */
  protected Collection<ColorSchema> readGenericColorSchema(InputStream colorInputStream)
      throws IOException, InvalidColorSchemaException {
    List<ColorSchema> result = new ArrayList<>();

    BufferedReader br = new BufferedReader(new InputStreamReader(colorInputStream));

    try {
      String line = br.readLine();
      int lineIndex = 1;
      while (line != null && line.startsWith("#")) {
        lineIndex++;
        line = br.readLine();
      }
      String[] columns = line.split("\t");

      Map<ColorSchemaColumn, Integer> schemaColumns = new HashMap<>();
      List<Pair<MiriamType, Integer>> customIdentifiers = new ArrayList<>();
      parseColumns(columns, schemaColumns, customIdentifiers, ColorSchemaType.GENERIC);

      Integer valueColumn = schemaColumns.get(ColorSchemaColumn.VALUE);
      Integer colorColumn = schemaColumns.get(ColorSchemaColumn.COLOR);
      Integer nameColumn = schemaColumns.get(ColorSchemaColumn.NAME);
      Integer modelNameColumn = schemaColumns.get(ColorSchemaColumn.MODEL_NAME);
      Integer identifierColumn = schemaColumns.get(ColorSchemaColumn.IDENTIFIER);
      Integer elementIdentifierColumn = schemaColumns.get(ColorSchemaColumn.ELEMENT_IDENTIFIER);
      if (elementIdentifierColumn == null) {
        elementIdentifierColumn = schemaColumns.get(ColorSchemaColumn.REACTION_IDENTIFIER);
      }
      Integer compartmentColumn = schemaColumns.get(ColorSchemaColumn.COMPARTMENT);
      Integer typeColumn = schemaColumns.get(ColorSchemaColumn.TYPE);
      Integer lineWidthColumn = schemaColumns.get(ColorSchemaColumn.LINE_WIDTH);
      Integer reverseReactionColumn = schemaColumns.get(ColorSchemaColumn.REVERSE_REACTION);
      Integer descriptionColumn = schemaColumns.get(ColorSchemaColumn.DESCRIPTION);

      if (nameColumn == null && identifierColumn == null && customIdentifiers.size() == 0
          && elementIdentifierColumn == null) {
        throw new InvalidColorSchemaException("One of these columns is obligatory: " + ColorSchemaColumn.NAME.getTitle()
            + "," + ColorSchemaColumn.IDENTIFIER.getTitle() + "," + ColorSchemaColumn.ELEMENT_IDENTIFIER.getTitle());
      }

      if (valueColumn == null && colorColumn == null) {
        throw new InvalidColorSchemaException("Schema must contain one of these two columns: value, color");
      }

      lineIndex++;
      line = br.readLine();
      while (line != null) {
        String errorPrefix = "[Line " + lineIndex + "]\t";
        lineIndex++;

        if (line.trim().equals("")) {
          line = br.readLine();
          continue;
        }
        String[] values = line.split("\t", -1);
        if (values.length != columns.length) {
          throw new InvalidColorSchemaException(errorPrefix + "Wrong number of cells");
        }
        ColorSchema schema = new GenericColorSchema();
        if (nameColumn != null) {
          processNameColumn(schema, values[nameColumn]);
        }
        if (modelNameColumn != null) {
          processModelNameColumn(schema, values[modelNameColumn]);
        }
        if (valueColumn != null) {
          schema.setValue(parseValueColumn(values[valueColumn], errorPrefix));
        }
        if (colorColumn != null && !values[colorColumn].isEmpty()) {
          schema.setColor(colorParser.parse(values[colorColumn]));
        }
        if (schema.getValue() != null && schema.getColor() != null) {
          throw new InvalidColorSchemaException(errorPrefix + "Either color or value can be defined but found both");
        }
        if (compartmentColumn != null) {
          processCompartmentColumn(schema, values[compartmentColumn]);
        }
        if (typeColumn != null) {
          schema.setTypes(parseSpeciesTypes(values[typeColumn], errorPrefix));
        }
        if (descriptionColumn != null) {
          schema.setDescription(values[descriptionColumn]);
        }
        if (elementIdentifierColumn != null) {
          processReactionIdentifier(schema, values[elementIdentifierColumn]);
        }
        if (lineWidthColumn != null) {
          if (!values[lineWidthColumn].trim().equals("")) {
            try {
              schema.setLineWidth(Double.parseDouble(values[lineWidthColumn].replace(",", ".")));
            } catch (NumberFormatException e) {
              throw new InvalidColorSchemaException(
                  errorPrefix + "Problem with parsing value: \"" + values[lineWidthColumn] + "\"");
            }
          }
        }
        if (reverseReactionColumn != null) {
          schema.setReverseReaction("true".equalsIgnoreCase(values[reverseReactionColumn]));
        }
        if (identifierColumn != null && !values[identifierColumn].equals("")) {
          processGeneralIdentifier(values[identifierColumn], schema, errorPrefix);
        }
        for (Pair<MiriamType, Integer> pair : customIdentifiers) {
          processIdentifier(values[pair.getRight()], pair.getLeft(), schema);
        }
        result.add(schema);
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return result;
  }

  private List<Class<? extends Element>> parseSpeciesTypes(String typesString, String errorPrefix)
      throws InvalidColorSchemaException {
    List<Class<? extends Element>> result = new ArrayList<>();
    String[] types = typesString.split(",");
    for (String string : types) {
      SpeciesMapping mapping = SpeciesMapping.getMappingByString(string);
      if (mapping != null) {
        result.add(mapping.getModelClazz());
      } else {
        String validStrings = "";
        for (SpeciesMapping speciesMapping : SpeciesMapping.values()) {
          validStrings += speciesMapping.getCellDesignerString() + ", ";
        }
        throw new InvalidColorSchemaException(
            errorPrefix + "Unknown class type: " + string + ". Valid values are: " + validStrings);
      }
    }
    return result;
  }

  private Double parseValueColumn(String string, String errorPrefix) throws InvalidColorSchemaException {
    Double result = null;
    if (!string.isEmpty()) {
      try {
        result = Double.parseDouble(string.replace(",", "."));
      } catch (NumberFormatException e) {
        throw new InvalidColorSchemaException(errorPrefix + "Problem with parsing value: \"" + string + "\"");
      }
      if (result > 1 + Configuration.EPSILON || result < -1 - Configuration.EPSILON) {
        throw new InvalidColorSchemaException(
            errorPrefix + "Value " + result + " out of range. Only values between -1 and 1 are allowed.");
      }
    }
    return result;
  }

  protected Collection<ColorSchema> readSimpleNameColorSchema(InputStream colorInputStream)
      throws IOException, InvalidColorSchemaException {
    List<ColorSchema> result = new ArrayList<>();

    BufferedReader br = new BufferedReader(new InputStreamReader(colorInputStream));

    try {
      String line = br.readLine();
      while (line != null && line.startsWith("#")) {
        line = br.readLine();
      }

      while (line != null) {
        if (line.trim().equals("")) {
          line = br.readLine();
          continue;
        }
        String[] values = line.split("\t", -1);
        ColorSchema schema = new GenericColorSchema();
        processNameColumn(schema, values[0]);
        schema.setDescription("");
        result.add(schema);
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return result;
  }

  /**
   * Sets proper reaction identifier to {@link ColorSchema} from cell content.
   * 
   * @param schema
   *          {@link ColorSchema} where name should be set
   * @param content
   *          content of the cell where reaction identifiers stored
   */

  private void processReactionIdentifier(ColorSchema schema, String content) {
    if (!content.isEmpty()) {
      schema.setElementId(content);
    }
  }

  /**
   * Transform headers of columns into map with {@link ColorSchemaColumn column
   * types} to column number.
   * 
   * @param columns
   *          headers of columns
   * @param schemaColumns
   *          map with {@link ColorSchemaColumn column types} to column number
   *          where result will be returned
   * @param customIdentifiers
   *          identifiers columns (used when elements are identified by
   *          {@link MiriamType})
   * @param type
   *          type of the color schema (for instance
   *          {@link ColorSchemaType#GENETIC_VARIANT gene variants})
   * @throws InvalidColorSchemaException
   *           thrown when the list of column headers contain invalid value
   */
  public void parseColumns(String[] columns, Map<ColorSchemaColumn, Integer> schemaColumns,
      List<Pair<MiriamType, Integer>> customIdentifiers, ColorSchemaType type) throws InvalidColorSchemaException {
    Map<String, MiriamType> acceptableIdentifiers = new HashMap<String, MiriamType>();
    for (MiriamType miriamType : MiriamType.values()) {
      acceptableIdentifiers.put(miriamType.getCommonName().toLowerCase(), miriamType);
    }

    for (int i = 0; i < columns.length; i++) {
      boolean found = false;
      for (ColorSchemaColumn schemaColumn : ColorSchemaColumn.values()) {
        if (columns[i].trim().equalsIgnoreCase(schemaColumn.getTitle()) && schemaColumn.getTypes().contains(type)) {
          schemaColumns.put(schemaColumn, i);
          found = true;
        }
      }
      if (!found) {
        if (acceptableIdentifiers.keySet().contains(columns[i].toLowerCase())) {
          customIdentifiers.add(new Pair<MiriamType, Integer>(acceptableIdentifiers.get(columns[i].toLowerCase()), i));
        } else {
          String columnNames = "";
          for (ColorSchemaColumn schemaColumn : ColorSchemaColumn.values()) {
            if (schemaColumn.getTypes().contains(type)) {
              columnNames += schemaColumn.getTitle() + ", ";
            }
          }
          for (String string : acceptableIdentifiers.keySet()) {
            columnNames += string + ", ";
          }
          throw new InvalidColorSchemaException(
              "Unknown column type: " + columns[i] + ". Acceptable column name: " + columnNames);
        }
      }
    }
  }

  /**
   * Reads information about set of {@link ColorSchema color schemas} from the
   * filename.
   * 
   * @param filename
   *          file with {@link ColorSchema}
   * @return list of coloring schemas
   * @throws IOException
   *           thrown when there is a problem with input stream
   * @throws InvalidColorSchemaException
   *           thrown when color schema is invalid
   */
  public Collection<ColorSchema> readColorSchema(String filename) throws IOException, InvalidColorSchemaException {
    return readColorSchema(new FileInputStream(filename),
        TextFileUtils.getHeaderParametersFromFile(new FileInputStream(filename)));
  }

  /**
   * Returns list of columns that should be printed for given coloring schemas.
   * 
   * @param schemas
   *          list of schemas
   * @return list of columns that should be printed (were set in the coloring
   *         schemas)
   */
  public Collection<ColorSchemaColumn> getSetColorSchemaColumns(Collection<ColorSchema> schemas) {
    Set<ColorSchemaColumn> result = new HashSet<ColorSchemaColumn>();
    for (ColorSchema schema : schemas) {
      if (schema.getColor() != null) {
        result.add(ColorSchemaColumn.COLOR);
      }
      if (schema.getCompartments().size() > 0) {
        result.add(ColorSchemaColumn.COMPARTMENT);
      }
      if (schema.getMiriamData().size() > 0) {
        result.add(ColorSchemaColumn.IDENTIFIER);
      }
      if (schema.getLineWidth() != null) {
        result.add(ColorSchemaColumn.LINE_WIDTH);
      }
      if (schema.getName() != null) {
        result.add(ColorSchemaColumn.NAME);
      }
      if (schema.getModelName() != null) {
        result.add(ColorSchemaColumn.MODEL_NAME);
      }
      if (schema.getElementId() != null) {
        result.add(ColorSchemaColumn.ELEMENT_IDENTIFIER);
      }
      if (schema.getReverseReaction() != null) {
        result.add(ColorSchemaColumn.REVERSE_REACTION);
      }
      if (schema.getTypes().size() > 0) {
        result.add(ColorSchemaColumn.TYPE);
      }
      if (schema.getValue() != null) {
        result.add(ColorSchemaColumn.VALUE);
      }
    }
    return result;
  }

  /**
   * Reads information about set of {@link ColorSchema color schemas} from the
   * input stream represented as byte array.
   * 
   * @param inputData
   *          source in form of byte array
   * @return list of coloring schemas
   * @throws IOException
   *           thrown when there is a problem with input stream
   * @throws InvalidColorSchemaException
   *           thrown when color schema is invalid
   */
  public Collection<ColorSchema> readColorSchema(byte[] inputData) throws IOException, InvalidColorSchemaException {
    return readColorSchema(new ByteArrayInputStream(inputData),
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(inputData)));
  }
}
