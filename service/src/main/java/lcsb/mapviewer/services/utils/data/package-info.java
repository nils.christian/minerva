/**
 * This package contains data structures used in
 * {@link lcsb.mapviewer.services.utils} package.
 */
package lcsb.mapviewer.services.utils.data;

