package lcsb.mapviewer.services.utils.data;

/**
 * This enum defines set of build-in layouts. Some of them are PD related.
 * Layout is a way of visualizing map.
 * 
 * @author Piotr Gawron
 * 
 */
public enum BuildInLayout { //
	/**
	 * Normal strainghtforward visualization.
	 */
	NORMAL("Network", "_normal", null, false), //
	// /**
	// * Set of data containing Substantia Nigra data for Parkinson Disease (false
	// * discovery ratio 0.01).
	// */
	// FDR_001("PD Substantia Nigra FDR=0.01", "_001", "/enricoData/ge001.txt",
	// false), //
	// /**
	// * Set of data containing Substantia Nigra data for Parkinson Disease (false
	// * discovery ratio 0.05).
	// */
	// FDR_005("PD Substantia Nigra FDR=0.05", "_005", "/enricoData/ge005.txt",
	// false), //
	// /**
	// * Set of data containing aging data for Parkinson Disease .
	// */
	// AGING("Aging", "_ageing", "/enricoData/ageing.txt", false), //
	/**
	 * Standard visualization with hierarchical view.
	 */
	NESTED("Pathways and compartments", "_nested", null, true), //
	SEMANTIC("Semantic zoom", "_semantic", null, true),
	/**
	 * Clean visualization (with colors reset to black and white).
	 */
	CLEAN("Empty", "_empty", null, false); //

	/**
	 * Title of the layout.
	 */
	private String	title;

	/**
	 * Suffix used for the directory name during image generation.
	 */
	private String	directorySuffix;

	/**
	 * File with the color values for the layout.
	 */
	private String	coloringFile;

	/**
	 * Should visualization be hierarchical or simple.
	 */
	private boolean	nested;

	/**
	 * Default constructor with all information needed by layout.
	 * 
	 * @param title
	 *          Title of the layout
	 * @param directorySuffix
	 *          Suffix used for the directory name during image generation
	 * @param coloringFileName
	 *          File with the color values for the layout
	 * @param nested
	 *          Should visualization be hierarchical or simple
	 */
	BuildInLayout(String title, String directorySuffix, String coloringFileName, boolean nested) {
		this.title = title;
		this.directorySuffix = directorySuffix;
		this.coloringFile = coloringFileName;
		this.nested = nested;
	}

	/**
	 * @return the title
	 * @see #title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the directorySuffix
	 * @see #directorySuffix
	 */
	public String getDirectorySuffix() {
		return directorySuffix;
	}

	/**
	 * @return the coloringFile
	 * @see #coloringFile
	 */
	public String getColoringFile() {
		return coloringFile;
	}

	/**
	 * @return the nested
	 * @see #nested
	 */
	public boolean isNested() {
		return nested;
	}

}
