package lcsb.mapviewer.services.utils.data;

import lcsb.mapviewer.model.map.layout.ColorSchema;

/**
 * Type of the {@link ColorSchema}.
 * 
 * @author Piotr Gawron
 *
 */
public enum ColorSchemaType {
	/**
	 * Generic color schema (used for expression levels, etc).
	 */
	GENERIC,

	/**
	 * Customized color schema used for highlightig genetic variants.
	 */
	GENETIC_VARIANT
}
