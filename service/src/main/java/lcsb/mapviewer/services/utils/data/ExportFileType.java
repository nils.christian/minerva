package lcsb.mapviewer.services.utils.data;

/**
 * Defines types of files that can be used in export function.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ExportFileType {
	
	/**
	 * Tab separated file.
	 */
	TAB_SEPARATED("Tab separated"),
	
	/**
	 * Simnple interaction format used by <a
	 * href="http://wiki.cytoscape.org/Cytoscape_User_Manual/Network_Formats"
	 * >Cytoscape</a>.
	 */

	SIF("SIF");

	/**
	 * Default constructor.
	 * 
	 * @param title
	 *          {@link #title}
	 */
	ExportFileType(final String title) {
		this.title = title;
	}

	/**
	 * Human readable name of the format.
	 */
	private String	title;

	/**
	 * 
	 * @return {@link #title}
	 */
	public String getTitle() {
		return title;
	}
}
