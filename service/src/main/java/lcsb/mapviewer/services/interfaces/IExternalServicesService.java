package lcsb.mapviewer.services.interfaces;

import java.util.List;

import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.IExternalService;

/**
 * Service that retrieves information about services that access external
 * resources (like chebi, chembl, etc.).
 * 
 * @author Piotr Gawron
 * 
 */
public interface IExternalServicesService {

	/**
	 * Returns status information about all services.
	 * 
	 * @return status information about all services
	 */
	List<ExternalServiceStatus> getExternalServiceStatuses();

	/**
	 * Registers service that access external resource.
	 * 
	 * @param service
	 *          service that access external resource
	 */
	void registerService(IExternalService service);

	/**
	 * Registers default list of services.
	 */
	void registerDefaultServices();

	/**
	 * Clears list of known services.
	 */
	void clearServices();

}
