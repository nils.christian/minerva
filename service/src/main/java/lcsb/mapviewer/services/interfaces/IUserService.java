package lcsb.mapviewer.services.interfaces;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.BasicPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.SecurityException;

/**
 * Service that manages users.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IUserService {
  /**
   * Check if its possible to login using given login and password.
   * 
   * @param login
   *          user login
   * @param password
   *          plan password
   * @return {@link String} containing session id if credentials are valid,
   *         <code>null</code> otherwise
   */
  String login(String login, String password);

  /**
   * Returns user by login.
   * 
   * @param login
   *          user login
   * @return user if the login is valid, <code>null</code> otherwise
   */
  User getUserByLogin(String login);

  /**
   * Checks if user has a given privilege.
   * 
   * @param user
   *          user for which the check is performed
   * @param type
   *          type of the privilege
   * @return <code>true</code> if user has privilege, <code>false</code> otherwise
   */
  boolean userHasPrivilege(User user, PrivilegeType type);

  /**
   * Returns level of the user privilege.
   * 
   * @param user
   *          user for which the check is performed
   * @param type
   *          type of the privilege
   * @return level of the privilege
   */
  int getUserPrivilegeLevel(User user, PrivilegeType type);

  /**
   * Checks if user has a given privilege on the object.
   * 
   * @param user
   *          user for which the check is performed
   * @param type
   *          type of the privilege
   * @param object
   *          object of the privilege
   * @return <code>true</code> if user has privilege, <code>false</code> otherwise
   */
  boolean userHasPrivilege(User user, PrivilegeType type, Object object);

  /**
   * Returns level of the user privilege.
   * 
   * @param object
   *          object of the privilege
   * @param user
   *          user for which the check is performed
   * @param type
   *          type of the privilege
   * @return level of the privilege
   */
  int getUserPrivilegeLevel(User user, PrivilegeType type, Object object);

  /**
   * Sets the user privilege.
   * 
   * @param user
   *          user for whom the privilege should be granted/dropped
   * @param privilege
   *          user privilege
   */
  void setUserPrivilege(User user, BasicPrivilege privilege);

  /**
   * Adds user to the system.
   * 
   * @param user
   *          user to be added
   */
  void addUser(User user);

  /**
   * Updates user in the system.
   * 
   * @param user
   *          user to be updated
   */
  void updateUser(User user);

  /**
   * Removes user from the system.
   * 
   * @param user
   *          user to be removed
   */
  void deleteUser(User user);

  /**
   * Drops privileges for every user on the given object and type.
   * 
   * @param type
   *          type of privilege that should be dropped
   * @param objectId
   *          identifier of the object to which privileges should be dropped
   */
  void dropPrivilegesForObjectType(PrivilegeType type, int objectId);

  /**
   * Adds privilege to the user.
   * 
   * @param user
   *          user to which privilege should be added
   * @param type
   *          type of the privilege
   */
  void setUserPrivilege(User user, PrivilegeType type, Integer value);

  /**
   * @param password
   *          input password
   * @return encoded password
   */
  String encodePassword(String password);

  /**
   * Returns {@link ColorExtractor} that transform overlay values into colors for
   * given user.
   * 
   * @param user
   *          {@link User} for which {@link ColorExtractor} will be obtained
   * @return {@link ColorExtractor} that transform overlay values into colors for
   *         given user
   */
  ColorExtractor getColorExtractorForUser(User user);

  User getUserByToken(String token) throws SecurityException;

  boolean userHasPrivilege(String token, PrivilegeType type, Object object) throws SecurityException;

  void logout(String tokenString) throws SecurityException;

  boolean userHasPrivilege(String token, PrivilegeType addMap) throws SecurityException;

  List<User> getUsers(String token) throws SecurityException;

  void setUserPrivilege(User modifiedUser, PrivilegeType type, Object privilegeToSet, String authenticationToken)
      throws SecurityException;

  void setUserPrivilege(User modifiedUser, PrivilegeType type, Object privilegeToSet, Integer objectId,
      String authenticationToken) throws SecurityException;

  void updateUser(User modifiedUser, String authenticationToken) throws SecurityException;

  String login(String login, String password, String id);

  ILdapService getLdapService();

  void setLdapService(ILdapService ldapService);

  void createDefaultProjectPrivilegesForUser(Project project, User user);

  Map<String, Boolean> ldapAccountExistsForLogin(Collection<User> logins);
}
