package lcsb.mapviewer.services.interfaces;

import java.util.List;

import org.primefaces.model.TreeNode;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserAnnotatorsParam;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.UserAccessException;
import lcsb.mapviewer.services.utils.CreateProjectParams;

/**
 * Service that manages projects.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IProjectService {

  /**
   * Returns a project with a give {@link Project#projectId project identifier}.
   * 
   * @param projectId
   *          {@link Project#projectId project identifier}
   * @return project with an identifier given as parameter. Null if such project
   *         doesn't exist.
   * @throws SecurityException
   */
  Project getProjectByProjectId(String projectId, String token) throws UserAccessException, SecurityException;

  /**
   * Checks if project with a given {@link Project#projectId identifier} exists.
   * 
   * @param projectId
   *          {@link Project#projectId project identifier}
   * @return <code>true</code> if the project with the given name exists,
   *         <code>false</code> otherwise
   */
  boolean projectExists(String projectId);

  /**
   * Returns list of all projects.
   * 
   * @return list of all projects.
   * @throws SecurityException
   */
  List<Project> getAllProjects(String token) throws SecurityException;

  /**
   * Removes project from the system.
   * 
   * @param homeDir
   *          directory where the system is deployed
   * @param project
   *          object to remove
   * @param async
   *          should the operation be done asynchronously
   * @throws SecurityException
   */
  void removeProject(Project project, String homeDir, boolean async, String token) throws SecurityException;

  /**
   * Adds project to the system.
   * 
   * @param project
   *          object to add
   */
  void addProject(Project project);

  /**
   * Creates project using give parameters. See {@link CreateProjectParams}.
   * 
   * @param params
   *          information about project to create
   * @throws SecurityException
   */
  void createProject(CreateProjectParams params) throws SecurityException;

  /**
   * Creates {@link TreeNode} that contains information about all classes that can
   * be annotated and associate set of
   * {@link lcsb.mapviewer.annotation.services.annotators.ElementAnnotator
   * ElementAnnotator} valid for these classes.
   * 
   * @param user
   *          for which information should be retrieved
   * @return {@link TreeNode} that contains information about all classes that can
   *         be annotated
   */
  TreeNode createClassAnnotatorTree(User user);

  /**
   * Update default set of annotators, lists of required/valid annotations for a
   * given user and default params used when creating project.
   * 
   * @param user
   *          user for which we update information
   * @param sbgnFormat
   *          new
   *          {@link lcsb.mapviewer.model.user.UserAnnotationSchema#sbgnFormat}
   *          value
   * @param networkLayoutAsDefault
   *          new
   *          {@link lcsb.mapviewer.model.user.UserAnnotationSchema#networkLayoutAsDefault}
   *          value
   * @param annotatorsTree
   *          {@link TreeNode} that contains information about all classes that
   *          can be annotated
   */
  void updateClassAnnotatorTreeForUser(User user, TreeNode annotatorsTree, boolean sbgnFormat,
      boolean networkLayoutAsDefault);

  /**
   * Method that updates information about raw {@link Project} object.
   * 
   * @param project
   *          project to update
   * @throws SecurityException 
   */
  void updateProject(Project project, String token) throws SecurityException;

  UserAnnotationSchema prepareUserAnnotationSchema(User user);

  /**
   * Retrieves list of annotators {@link UserAnnotatorsParam parameters}
   * 
   * @return list of annotators parameters
   */
  List<UserAnnotatorsParam> getAnnotatorsParams(User user);

  List<Project> getAllProjects();

}
