package lcsb.mapviewer.services.interfaces;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.ColorSchemaType;

/**
 * Service that manages layouts of the map.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ILayoutService {

  /**
   * Parameters used for creation of the layout.
   *
   * @author Piotr Gawron
   *
   */
  class CreateLayoutParams {

    /**
     * Size of the buffer used to copy stream data.
     */
    private static final int BUFFER_SIZE = 1024;

    /**
     * Name of the layout.
     *
     */
    private String name;

    /**
     * Description of the layout.
     *
     */
    private String description;

    /**
     * Where the graphic files should be saved.
     *
     */
    private String directory;

    /**
     *
     * {@link Model} for which the layout is created.
     */
    private Model model;

    /**
     * Who creates the layout.
     *
     */
    private User user;

    /**
     *
     * Input stream with coloring information data in the stream should correspond
     * to a file that can be parsed by
     * {@link lcsb.mapviewer.services.utils.ColorSchemaXlsxReader ColorSchemaReader}
     * . This is a copy of original input stream, so we can read this input stream
     * multiple times.
     */
    private ByteArrayOutputStream colorInputStreamCopy;

    /**
     * Name of the file used as input (if available).
     */
    private String layoutFileName;

    /**
     * Determines if creation of the output files should be done asynchronously who
     * creates the layout.
     *
     */
    private boolean async = false;

        private boolean                             googleLicenseConsent               = false;

    /**
     * Type of the uploaded file.
     */
    private ColorSchemaType colorSchemaType;

    /**
     * @return the name
     * @see #name
     */
    public String getName() {
      return name;
    }

    /**
     * @param name
     *          the name to set
     * @see #name
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams name(String name) {
      this.name = name;
      return this;
    }

    /**
     * @return the directory
     * @see #directory
     */
    public String getDirectory() {
      return directory;
    }

    /**
     * @param directory
     *          the directory to set
     * @see #directory
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams directory(String directory) {
      this.directory = directory;
      return this;
    }

    /**
     * @return the model
     * @see #model
     */
    public Model getModel() {
      return model;
    }

    /**
     * @param model
     *          the model to set
     * @see #model
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams model(Model model) {
      this.model = model;
      return this;
    }

    /**
     * @return the user
     * @see #user
     */
    public User getUser() {
      return user;
    }

    /**
     * @param user
     *          the user to set
     * @see #user
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams user(User user) {
      this.user = user;
      return this;
    }

    /**
     * @return the colorInputStream
     * @see #colorInputStream
     */
    public InputStream getColorInputStream() {
      return new ByteArrayInputStream(colorInputStreamCopy.toByteArray());
    }

    /**
     * @param colorInputStream
     *          the colorInputStream to set
     * @see #colorInputStream
     * @return {@link CreateLayoutParams} object
     * @throws IOException
     *           thrown when there is a problem with accessing input stream
     */
    public CreateLayoutParams colorInputStream(InputStream colorInputStream) throws IOException {
      if (colorInputStream == null) {
        this.colorInputStreamCopy = null;
      } else {
        this.colorInputStreamCopy = new ByteArrayOutputStream();

        byte[] buffer = new byte[BUFFER_SIZE];
        int len;
        while ((len = colorInputStream.read(buffer)) > -1) {
          this.colorInputStreamCopy.write(buffer, 0, len);
        }
        this.colorInputStreamCopy.flush();
      }
      return this;
    }

    /**
     * @return the async
     * @see #async
     */
    public boolean isAsync() {
      return async;
    }

    /**
     * @param async
     *          the async to set
     * @see #async
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams async(boolean async) {
      this.async = async;
      return this;
    }

    /**
     * @return the googleLicenseConsent
     * @see #googleLicenseConsent
     */
    public boolean isGoogleLicenseConsent() {
        return googleLicenseConsent;
    }

    /**
     * @param googleLicenseConsent
     *          the googleLicenseConsent to set
     * @see #googleLicenseConsent
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams googleLicenseConsent(boolean googleLicenseConsent) {
       this.googleLicenseConsent = googleLicenseConsent;
       return this;
    }

    /**
     * @return the description
     * @see #description
     */
    public String getDescription() {
      return description;
    }

    /**
     * @param description
     *          the description to set
     * @see #description
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams description(String description) {
      this.description = description;
      return this;
    }

    /**
     *
     * @return {@link #colorSchemaType}
     */
    public ColorSchemaType getColorSchemaType() {
      return colorSchemaType;
    }

    /**
     * @param colorSchemaType
     *          the colorSchemaType to set
     * @see #colorSchemaType
     * @return {@link CreateLayoutParams} object
     */
    public CreateLayoutParams colorSchemaType(ColorSchemaType colorSchemaType) {
      this.colorSchemaType = colorSchemaType;
      return this;
    }

    /**
     * @return the layoutFileName
     * @see #layoutFileName
     */
    public String getLayoutFileName() {
      return layoutFileName;
    }

    /**
     * @param layoutFileName
     *          the layoutFileName to set
     * @return {@link CreateLayoutParams} object
     * @see #layoutFileName
     */
    public CreateLayoutParams layoutFileName(String layoutFileName) {
      this.layoutFileName = layoutFileName;
      return this;
    }
  }

  /**
   * Returns true if user can add layout to the model.
   *
   * @param model
   *          to which model user wants to add layout
   * @param user
   *          who wants to add layout
   * @return <code>true</code> if user can add layout, <code>false</code>
   *         otherwise
   */
  boolean userCanAddLayout(Model model, User user);

  /**
   * Returns true if user can remove layout from the model.
   *
   * @param layout
   *          which layout user want to remove
   * @param user
   *          who wants to remove layout
   * @return <code>true</code> if user can remove layout, <code>false</code>
   *         otherwise
   */
  boolean userCanRemoveLayout(Layout layout, User user);

  /**
   * Removes layout from the system.
   *
   * @param layout
   *          layout to remove
   * @param homeDir
   *          directory where the system is deployed
   * @throws IOException
   *           thrown when there are problems with removing layout files
   */
  void removeLayout(Layout layout, String homeDir) throws IOException;

  /**
   * Updates data about the layout.
   *
   * @param layout
   *          layout to update
   */
  void updateLayout(Layout layout);

  /**
   * Adds view privilege to the layout for the user.
   *
   * @param layout
   *          layout for privilege
   * @param user
   *          who should own the privilege
   */
  void addViewPrivilegeToLayout(Layout layout, User user);

  /**
   * Removes view privilege to the layout from the user.
   *
   * @param layout
   *          layout for privilege remove
   * @param user
   *          who shouldn't have the privilege
   */
  void dropViewPrivilegeFromLayout(Layout layout, User user);

  /**
   * Create layout based on the data in the parameter.
   *
   * @param params
   *          list of {@link CreateLayoutParams params} necessary to create layout
   * @return object that refers to created layout
   * @throws IOException
   *           thrown when there are problems with files
   * @throws InvalidColorSchemaException
   *           if the coloring source is invalid
   * @throws CommandExecutionException
   */
  Layout createLayout(CreateLayoutParams params) throws IOException, InvalidColorSchemaException;

  /**
   * Create layout based on the data in the parameter. Layout will contain set of
   * images that can be further visualized in goolge maps api.
   *
   * @param params
   *          list of {@link CreateLayoutParams params} necessary to create layout
   * @return object that refers to created layout
   * @throws IOException
   *           thrown when there are problems with files
   * @throws CommandExecutionException
   *           if the coloring source is invalid
   * @throws InvalidColorSchemaException
   *           if the coloring source is invalid
   */
  Layout createLayoutWithImages(CreateLayoutParams params)
      throws IOException, CommandExecutionException, InvalidColorSchemaException;

  /**
   * Returns number of still available custom layouts for the user.
   *
   * @param user
   *          user for which the number of available custom layouts will be
   *          returned
   * @return number of available custom layouts
   */
  long getAvailableCustomLayoutsNumber(User user);

  /**
   * Returns a list of pairs {@link Element} - {@link ColorSchema} that are visualized in
   * a {@link lcsb.mapviewer.model.map.layout.Layout}.
   *
   * @param model
   *          model where data is located
   * @param layoutId
   *          identifier of the layout
   * @return a list of pairs {@link Element} - {@link ColorSchema} that are visualized in
   *         a {@link lcsb.mapviewer.model.map.layout.Layout}
   * @throws SecurityException
   */
  List<Pair<Element, ColorSchema>> getAliasesForLayout(Model model, int layoutId, String token)
      throws SecurityException;

  /**
   * Returns a list of pairs {@link Reaction} - {@link ColorSchema}} that are
   * visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}.
   *
   * @param model
   *          model where data is located
   * @param layoutId
   *          identifier of the layout
   * @return a list of pairs {@link Reaction} - {@link ColorSchema} that are
   *         visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}
   * @throws SecurityException
   */
  List<Pair<Reaction, ColorSchema>> getReactionsForLayout(Model model, int layoutId, String token)
      throws SecurityException;

  /**
   * Returns mapping between {@link lcsb.mapviewer.model.map.species.Element
   * Alias}/ {@link lcsb.mapviewer.model.map.reaction.Reaction Reaction} and
   * {@link ColorSchema} used for coloring object in the layout given in the
   * parameter.
   *
   * @param model
   *          model where data is located
   * @param layoutId
   *          identifier of the layout
   * @return a list of pairs {@link Element} - {@link ColorSchema} that are
   *         visualized in a {@link lcsb.mapviewer.model.map.layout.Layout}
   * @throws SecurityException
   */
  Map<Object, ColorSchema> getElementsForLayout(Model model, Integer layoutId, String token) throws SecurityException;

  Pair<Element, ColorSchema> getFullAliasForLayout(Model model, Integer id, int layoutId, String token)
      throws SecurityException;

  Pair<Reaction, ColorSchema> getFullReactionForLayout(Model model, Integer id, int layoutId, String token)
      throws SecurityException;

  /**
   * Returns {@link EmailSender} used by the service.
   *
   * @return {@link EmailSender} used by the service
   */
  EmailSender getEmailSender();

  /**
   * Sets {@link EmailSender} used by the service.
   *
   * @param emailSender
   *          {@link EmailSender} used by the service
   */
  void setEmailSender(EmailSender emailSender);

  /**
   * Returns list of custom layouts.
   *
   * @param model
   *          model where the layouts lay on
   * @param user
   *          user who asks for the layouts
   * @return list of custom layouts
   */
  List<Layout> getCustomLayouts(Model model, String token, Boolean publicOverlay, User creator)
      throws SecurityException;

  Layout getLayoutById(int overlayId, String token) throws SecurityException;

  boolean userCanRemoveLayout(Layout layout, String authenticationToken) throws SecurityException;

}
