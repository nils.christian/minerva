package lcsb.mapviewer.services.interfaces;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelConnection;

/**
 * Service that gives access to search functionalities.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ISearchService {
	/**
	 * Parameters of search objects on the map using coordinates. It's possible to
	 * add also information about nested model and level at which searching is
	 * performed in the hierarchical view. More information about hierarchical
	 * view can be found in the definition of
	 * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator
	 * AbstractImageGenerator}.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	class CoordinatesSearchParams {

		/**
		 * Default class logger.
		 */
		private final Logger logger						= Logger.getLogger(CoordinatesSearchParams.class);

		/**
		 * Model where we perform search.
		 */
		private Model				 model						= null;

		/**
		 * X coordinate of the search event.
		 */
		private double			 x								= 0;

		/**
		 * Y coordinate of the search event.
		 */
		private double			 y								= 0;

		/**
		 * Maximum distance between object that could be found and point that is
		 * valid.
		 */
		private double			 distance					= 0;

		/**
		 * Index of the layout in model for which we perform search. It allows to
		 * determine if the layout is hierarchical or not.
		 */
		private Integer			 layoutIdentifier	= null;

		/**
		 * Level at which the search is performed. It's used during determining if
		 * the object is visible in hierarchical view.
		 */
		private Integer			 level						= null;

		/**
		 * @return the model
		 * @see #model
		 */
		public Model getModel() {
			return model;
		}

		/**
		 * @param model
		 *          the model to set
		 * @return object with all parameters
		 * @see #model
		 */
		public CoordinatesSearchParams model(Model model) {
			this.model = model;
			return this;
		}

		/**
		 * @param point
		 *          the coordinates to set
		 * @return object with all parameters
		 * @see #model
		 */
		public CoordinatesSearchParams point(Point2D point) {
			this.x = point.getX();
			this.y = point.getY();
			return this;
		}

		/**
		 * @return the x
		 * @see #x
		 */
		public double getX() {
			return x;
		}

		/**
		 * @param x
		 *          the x to set
		 * @return object with all parameters
		 * @see #x
		 */
		public CoordinatesSearchParams x(double x) {
			this.x = x;
			return this;
		}

		/**
		 * @return the y
		 * @see #y
		 */
		public double getY() {
			return y;
		}

		/**
		 * @param y
		 *          the y to set
		 * @return object with all parameters
		 * @see #y
		 */
		public CoordinatesSearchParams y(double y) {
			this.y = y;
			return this;
		}

		/**
		 * @return the distance
		 * @see #distance
		 */
		public double getDistance() {
			return distance;
		}

		/**
		 * @param distance
		 *          the distance to set
		 * @return object with all parameters
		 * @see #distance
		 */
		public CoordinatesSearchParams distance(double distance) {
			this.distance = distance;
			return this;
		}

		/**
		 * @return layout database identifier
		 * @see #layoutIdentifier
		 */
		public Integer getLayoutIdentfier() {
			return layoutIdentifier;
		}

		/**
		 * @param layoutIdentifier
		 *          layout database identifier to set
		 * @return object with all parameters
		 * @see #layoutIdentifier
		 */
		public CoordinatesSearchParams layoutIdentifier(Integer layoutIdentifier) {
			nested = null;
			this.layoutIdentifier = layoutIdentifier;
			return this;
		}

		/**
		 * @param level
		 *          the level to set
		 * @return object with all parameters
		 * @see #level
		 */
		public CoordinatesSearchParams level(Integer level) {
			this.level = level;
			nested = null;
			return this;
		}

		/**
		 * @return the level
		 * @see #level
		 */
		public Integer getLevel() {
			return level;
		}

		/**
		 * This value determines if the search is performed on the nested
		 * (hierarchical) view or not. If nested is <code>null</code>, then it means
		 * that the nested state is unknown and should be recomputed.
		 */
		private Boolean nested = null;

		/**
		 * Checks if the search param point to search in nested (hierarchical) view.
		 * 
		 * @return <code>true</code> if search param point to search in nested
		 *         (hierarchical) view, <code>false</code> otherwise
		 */
		public boolean isNested() {
			if (nested == null) {
				computeNested();
			}
			return nested;
		}

		/**
		 * This method finds out if the search is performed on the nested
		 * (hierarchical) view or not and saves it in the local state.
		 */
		private void computeNested() {
			nested = false;
			if (getLayoutIdentfier() != null) {
				Layout layout = getModel().getLayoutByIdentifier(getLayoutIdentfier());
				if (layout != null) {
					nested = layout.isHierarchicalView();
				} else {
					for (SubmodelConnection sc : getModel().getParentModels()) {
						if (sc instanceof ModelSubmodelConnection) {

							layout = ((ModelSubmodelConnection) sc).getParentModel().getModel().getLayoutByIdentifier(getLayoutIdentfier());
							if (layout != null) {
								break;
							}
						}
					}
					if (layout != null) {
						nested = layout.isHierarchicalView();
					} else {
						logger.warn("Unkown layout with id: " + getLayoutIdentfier());
					}
				}
			} else {
				logger.warn("Layout identifier is null");
			}

		}

	}

	/**
	 * Search the element on the map using coordinates. It is also possible to
	 * provide information about layout (if it's nested or not) and hierarchical
	 * level.
	 * 
	 * @param params
	 *          set of search params. More information can be found here:
	 *          {@link CoordinatesSearchParams}
	 * @return object that match best the search criteria. If such object doesn't
	 *         exist then <code>null</code> is returned.
	 */
	List<BioEntity> searchByCoordinates(CoordinatesSearchParams params);

	/**
	 * Search for elements on the map by query. Returns many possibilities from
	 * best matching to less matching possibility.
	 * 
	 * @param model
	 *          model on which we perform search
	 * @param query
	 *          the query
	 * @param limit
	 *          maximum number of results
	 * @param perfectMatch
	 *          should the match be perfect
	 * @param ipAddress
	 *          ip address of a client who is searching (used for logging purpose)
	 * @return list of objects that matches the query sorted by the match quality
	 */
	List<BioEntity> searchByQuery(Model model, String query, int limit, Boolean perfectMatch, String ipAddress);

	List<BioEntity> searchByQuery(Model model, String query, int limit, Boolean perfectMatch);

	/**
	 * Returns the closest elements to the coordinates on the model.
	 * 
	 * @param model
	 *          model on which the search is performed
	 * @param point
	 *          coordinates where search is performed
	 * @param numberOfElements
	 *          how many closest elements should be returned
	 * @return list of the closest elements
	 */
	List<BioEntity> getClosestElements(Model model, Point2D point, int numberOfElements, boolean perfectHit, Collection<String> types);

	/**
	 * Returns list of autocomplete strings for the partial query.
	 * 
	 * @param model
	 *          on which model the search is performed
	 * @param query
	 *          partial query used for autocomplete
	 * @return list of strings that could be used for autocomplete the query
	 */
	List<String> getAutocompleteList(Model model, String query);

	String[] getSuggestedQueryList(Model model);

}
