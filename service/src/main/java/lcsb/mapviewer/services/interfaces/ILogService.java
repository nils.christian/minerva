package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.log.GenericLog;
import lcsb.mapviewer.model.log.LogType;
import lcsb.mapviewer.model.user.User;

/**
 * Service that allows to log some system messages.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ILogService {
	/**
	 * Structure describing log event parameters.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	class LogParams {
		/**
		 * Type of the log event. Field is obligatory.
		 */
		private LogType	type;
		/**
		 * Description of the log event.
		 */
		private String	description;
		/**
		 * Object to which this entry refers to.
		 */
		private Object	object;
		/**
		 * Logged user that caused this entry. If set to null then value from
		 * {@link ILogService#setLoggedUser(User)} will be used.
		 */
		private User		user;

		/**
		 * @return the type
		 * @see #type
		 */
		public LogType getType() {
			return type;
		}

		/**
		 * @param type
		 *          the type to set
		 * @see #type
		 * @return {@link LogParams object} with all params
		 */
		public LogParams type(LogType type) {
			this.type = type;
			return this;
		}

		/**
		 * @return the description
		 * @see #description
		 */
		public String description() {
			return description;
		}

		/**
		 * @param description
		 *          the description to set
		 * @see #description
		 * @return {@link LogParams object} with all params
		 */
		public LogParams description(String description) {
			this.description = description;
			return this;
		}

		/**
		 * @return the object
		 * @see #object
		 */
		public Object getObject() {
			return object;
		}

		/**
		 * @param object
		 *          the object to set
		 * @see #object
		 * @return {@link LogParams object} with all params
		 */
		public LogParams object(Object object) {
			this.object = object;
			return this;
		}

		/**
		 * @return the user
		 * @see #user
		 */
		public User getUser() {
			return user;
		}

		/**
		 * @param user
		 *          the user to set
		 * @see #user
		 * @return {@link LogParams object} with all params
		 */
		public LogParams user(User user) {
			this.user = user;
			return this;
		}
	}

	/**
	 * Method adds a log entry. The parameters define additional information about
	 * log.
	 * 
	 * @param params
	 *          {@link LogParams params} of the log
	 * @return returns created log entry
	 */
	GenericLog log(LogParams params);

	/**
	 * Sets user that should be used as default user to be put as the author of
	 * new entries.
	 * 
	 * @param loggedUser
	 *          user that should be used as a deafult one for loggin events. This
	 *          value is always overriden by {@link LogParams#user} (if set).
	 */
	void setLoggedUser(User loggedUser);
}
