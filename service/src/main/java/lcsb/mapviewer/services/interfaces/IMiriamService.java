package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * Service responsible for accessing miriam registry.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IMiriamService {

	/**
	 * Returns url for data represented by {@link MiriamData}.
	 * 
	 * @param md
	 *          object to which link will be returned
	 * @return url to miriam data
	 */
	String getUrlForMiriamData(MiriamData md);

	/**
	 * Returns {@link MiriamType} associated with parameter uri address.
	 * 
	 * @param uri
	 *          uri to check
	 * @return {@link MiriamType} for given uri
	 */
	MiriamType getTypeForUri(String uri);

}
