package lcsb.mapviewer.services.search.drug;

import java.util.List;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.services.search.IDbSearchService;

/**
 * Service for accessing information about drugs.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IDrugService extends IDbSearchService<Drug> {

  List<String> getSuggestedQueryList(Project project, MiriamData disease) throws DrugSearchException;

}
