package lcsb.mapviewer.services.search;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;

public class ElementMatcher {

  /**
   * Default class logger.
   */
  private final Logger logger = Logger.getLogger(ElementMatcher.class);

  public boolean elementMatch(Target target, BioEntity element) {
    MiriamData targetAnnotation = target.getSource();
    switch (target.getType()) {
    case COMPLEX_PROTEIN:
      if (MiriamType.CHEMBL_TARGET.equals(targetAnnotation.getDataType())) {
        if (element.getMiriamData().contains(targetAnnotation)) {
          return true;
        }
      }
      Collection<String> ids = new HashSet<String>();
      for (MiriamData row : target.getGenes()) {
        ids.add(row.getResource());
      }

      if (element instanceof Complex) {
        Complex complex = (Complex) element;
        Set<Species> speciesSet = complex.getAllSimpleChildren();
        if (speciesSet.size() != ids.size()) {
          return false;
        }

        for (Element id : speciesSet) {
          if (!ids.contains(id.getName())) {
            return false;
          }
        }
        return true;
      }
      return false;
    case SINGLE_PROTEIN:
      if (target.getSource() != null) {
        targetAnnotation = target.getSource();
      } else if (target.getGenes().size() > 0) {
        targetAnnotation = target.getGenes().get(0);
      } else {
        logger.warn("Invalid target found: " + target);
      }
      if (targetAnnotation != null && element.getMiriamData().contains(targetAnnotation)) {
        return true;
      }
      if (element instanceof Protein || element instanceof Rna || element instanceof Gene) {
        if (target.getGenes().size() > 0) {
          String hgncId = target.getGenes().get(0).getResource();
          if (element.getName().equalsIgnoreCase(hgncId)) {
            return true;
          }
        }
      }
      return false;
    case PROTEIN_FAMILY:
      if (element instanceof Protein || element instanceof Rna || element instanceof Gene) {
        String hgncId = target.getGenes().get(0).getResource();
        if (element.getName().equalsIgnoreCase(hgncId)) {
          return true;
        } else if (element.getMiriamData().contains(new MiriamData(MiriamType.HGNC_SYMBOL, hgncId))) {
          return true;
        }
        for (MiriamData protein : target.getGenes()) {
          hgncId = protein.getResource();
          if (element.getName().equalsIgnoreCase(hgncId)) {
            return true;
          } else if (element.getMiriamData().contains(new MiriamData(MiriamType.HGNC_SYMBOL, hgncId))) {
            return true;
          }
        }
      }
      return false;
    case OTHER:
      // in other case just compare names
      String targetName = null;
      if (target.getGenes().size() > 0) {
        targetName = target.getGenes().get(0).getResource();
      }
      if (targetName == null) {
        targetName = target.getName();
      }
      if (targetName == null) {
        return false;
      }
      return element.getName().equalsIgnoreCase(targetName);
    default:
      throw new InvalidArgumentException("Unknown drug target type: " + target.getType());
    }
  }

}
