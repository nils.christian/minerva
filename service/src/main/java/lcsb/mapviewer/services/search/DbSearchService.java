package lcsb.mapviewer.services.search;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargettingStructure;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@Transactional(value = "txManager")
public abstract class DbSearchService {

	/**
	 * Service accessing
	 * <a href="http://europepmc.org/RestfulWebService">pubmed</a>.
	 */
	@Autowired
	private PubmedParser														 pubmedParser;

	protected void cacheMiriamData(TargettingStructure targettingStructure) throws AnnotatorException {
		Set<MiriamData> result = new HashSet<>();
		result.addAll(targettingStructure.getSources());
		for (Target target : targettingStructure.getTargets()) {
			result.addAll(target.getGenes());
			result.addAll(target.getReferences());

		}
		
		for (MiriamData miriamData : result) {
			if (MiriamType.PUBMED.equals(miriamData.getDataType())) {
				try {
					pubmedParser.getPubmedArticleById(Integer.valueOf(miriamData.getResource()));
				} catch (NumberFormatException | PubmedSearchException e) {
					throw new AnnotatorException(e);
				}
			}
		}
		
	}
}
