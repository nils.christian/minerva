package lcsb.mapviewer.services.search;

import java.util.Collection;
import java.util.List;

import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Service that allows to retrieve information of type T from external
 * resources/databases.
 * 
 * @author Piotr Gawron
 *
 * @param <T>
 *          type of returned results
 */
public interface IDbSearchService<T> {

	/**
	 * Returns the drugs found by drug name.
	 * 
	 * @param name
	 *          name of the drug
	 * @param searchCriteria
	 *          set of {@link DbSearchCriteria} used for searching (like:
	 *          {@link lcsb.mapviewer.model.map.model.Model Model})
	 * @return list of drugs for drug names
	 */
	T getByName(String name, DbSearchCriteria searchCriteria);

	/**
	 * Returns the list of drugs that target at least one of the element in the
	 * parameter.
	 * 
	 * @param targetElements
	 *          list of elements that should be targeted by drug
	 * @param searchCriteria
	 *          set of {@link DbSearchCriteria} used for searching (like:
	 *          {@link lcsb.mapviewer.model.map.model.Model Model})
	 * @return list of drugs that targets something from the elements collection
	 */
	List<T> getForTargets(Collection<Element> targetElements, DbSearchCriteria searchCriteria);

	/**
	 * This method will cache all queries that make sense for the model.
	 * 
	 * @param originalModel
	 *          model to be considered for database queries
	 * @param progressUpdater
	 *          callback function updating information about cache progress
	 */
	void cacheDataForModel(Model originalModel, IProgressUpdater progressUpdater);

}
