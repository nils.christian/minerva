package lcsb.mapviewer.services.search.chemical;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.services.search.IDbSearchService;

/**
 * Service for accessing information about chemical.
 * 
 * @author Ayan Rota
 * 
 */
public interface IChemicalService extends IDbSearchService<Chemical> {

}
