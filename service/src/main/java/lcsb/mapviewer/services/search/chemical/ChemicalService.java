package lcsb.mapviewer.services.search.chemical;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.services.ChemicalParser;
import lcsb.mapviewer.annotation.services.ChemicalSearchException;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.DbSearchService;

/**
 * Implementation of the service that allows access information about chemicals.
 * 
 * @author Ayan Rota
 * 
 */
@Transactional(value = "txManager")
public class ChemicalService extends DbSearchService implements IChemicalService {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ChemicalService.class);

  /**
   * List of chemicals pairs of id and name.
   */
  private Map<MiriamData, Map<MiriamData, String>> diseases = new HashMap<>();

  /**
   * Access point and parser for the online ctd database.
   */
  @Autowired
  private ChemicalParser chemicalParser;

  /**
   * Object used to perform operations on {@link MiriamType#HGNC} annotations.
   */
  @Autowired
  private HgncAnnotator hgncAnnotator;

  /**
   * Default constructor.
   */
  public ChemicalService() {
    super();
  }

  /**
   * Returns list of {@link Chemical chemicals} for a given disease and list of
   * names.
   * 
   * @param diseaseID
   *          {@link MiriamData} identifying disease; it should be a
   *          {@link MiriamType#MESH_2012} identifier
   * @param names
   *          list of drug names that we are looking for
   * @return list of {@link Chemical chemicals} that were found
   */
  protected List<Chemical> getChemicalByName(MiriamData diseaseID, List<String> names) {
    if (diseaseID == null || names == null) {
      throw new InvalidArgumentException("diseaseID cannot be null");
    }
    names.removeAll(Collections.singleton(null));

    // Create a map with the each search string and chemical id.
    // Remove empty string and special characters.
    Map<String, MiriamData> searchNamesMap = new LinkedHashMap<String, MiriamData>();
    for (int i = 0; i < names.size(); i++) {
      String name = names.get(i).trim().toLowerCase().replaceAll("[^\\w]", "");
      if (!name.isEmpty()) {
        searchNamesMap.put(name, null);
      }
    }
    try {

      // Search id, name Map for matching records
      List<MiriamData> ids = new ArrayList<MiriamData>();
      Iterator<Entry<MiriamData, String>> idNameEnteries = getChemicalsByDisease(diseaseID).entrySet().iterator();
      int namesFound = 0;
      while (idNameEnteries.hasNext() && namesFound < searchNamesMap.size()) {
        Map.Entry<MiriamData, String> pair = idNameEnteries.next();
        String chemicalName = pair.getValue();
        if (chemicalName != null) {
          chemicalName = chemicalName.toLowerCase().replaceAll("[^\\w]", "");
          for (Map.Entry<String, MiriamData> nameEntry : searchNamesMap.entrySet()) {
            if (nameEntry.getKey().equals(chemicalName)) {
              nameEntry.setValue(pair.getKey());
              namesFound++;

            }
          }

        }
      }

      // Get what all values include null where no matching record is found,
      // remove null and look in the database.
      ids.addAll(searchNamesMap.values());
      ids.removeAll(Collections.singleton(null));
      List<Chemical> sortedResult = new ArrayList<Chemical>();
      if (!ids.isEmpty()) {
        List<Chemical> result = chemicalParser.getChemicals(diseaseID, ids);

        // If the result is not empty and we found chemicals, try to sort as
        // by the initial list provided.
        Map<MiriamData, Chemical> resultAsMap = new HashMap<MiriamData, Chemical>();
        if (result != null && !result.isEmpty()) {
          for (Chemical chemo : result) {
            resultAsMap.put(chemo.getChemicalId(), chemo);
          }

          for (Map.Entry<String, MiriamData> nameEntry : searchNamesMap.entrySet()) {
            if (nameEntry.getValue() != null) {
              sortedResult.add(resultAsMap.get(nameEntry.getValue()));
            }
          }
        }
      }

      return sortedResult;
    } catch (ChemicalSearchException e) {
      logger.error("Problem with accessing database", e);
      return new ArrayList<Chemical>();
    }
  }

  @Override
  public Chemical getByName(String name, DbSearchCriteria searchCriteria) {
    if (searchCriteria.getDisease() == null) {
      throw new InvalidArgumentException("diseaseID cannot be null");
    }
    if (name == null || name.isEmpty()) {
      return null;
    }
    MiriamData id = null;
    List<Chemical> result = new ArrayList<Chemical>();
    name = name.toLowerCase().replaceAll("[^\\w]", "");
    try {
      Iterator<Entry<MiriamData, String>> mapValues = getChemicalsByDisease(searchCriteria.getDisease()).entrySet()
          .iterator();
      int shortestName = Integer.MAX_VALUE;
      while (mapValues.hasNext()) {
        Map.Entry<MiriamData, String> pair = mapValues.next();
        String chemicalName = pair.getValue();
        if (chemicalName != null) {
          chemicalName = chemicalName.toLowerCase().replaceAll("[^\\w]", "");
          if (chemicalName.contains(name) && name.length() < shortestName) {
            id = pair.getKey();
            shortestName = chemicalName.length();
          }
        }
      }
      if (id != null) {
        List<MiriamData> chemicalId = new ArrayList<>();
        chemicalId.add(id);
        result = chemicalParser.getChemicals(searchCriteria.getDisease(), chemicalId);
      } else {
        result = chemicalParser.getChemicalsBySynonym(searchCriteria.getDisease(), name);
      }
    } catch (ChemicalSearchException e) {
      logger.error("Problem with accesing database", e);
    }
    if (result.size() == 0) {
      return null;
    } else {
      return result.get(0);
    }

  }

  @Override
  public List<Chemical> getForTargets(Collection<Element> targets, DbSearchCriteria searchCriteria) {
    List<Chemical> chemicalList = new ArrayList<>();
    Set<MiriamData> targetsMiriam = new HashSet<MiriamData>();
    for (Element alias : targets) {
      if (alias instanceof Protein || alias instanceof Gene || alias instanceof Rna) {
        boolean hgncFound = false;
        for (MiriamData md : alias.getMiriamData()) {
          if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
            targetsMiriam.add(md);
            hgncFound = true;
          } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
            // ad also entrez in case of mouse, rat, etc
            targetsMiriam.add(md);
          }

        }
        if (!hgncFound) {
          MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, alias.getName());
          try {
            if (hgncAnnotator.isValidHgncMiriam(md)) {
              targetsMiriam.add(md);
            }
          } catch (AnnotatorException e) {
            logger.error("Problem with accessing HGNC database", e);
          }
        }
      }
    }
    try {
      chemicalList = chemicalParser.getChemicalListByTarget(targetsMiriam, searchCriteria.getDisease());
    } catch (ChemicalSearchException e) {
      logger.error("Problem with accessing chemical database", e);
    }

    Collections.sort(chemicalList, new Chemical.NameComparator());
    return chemicalList;
  }

  /**
   * @param disease
   *          {@link MiriamType#MESH_2012 mesh term} describing disease
   * 
   * @return chemicals related to the disease
   * @see #diseases
   * @throws ChemicalSearchException
   *           thrown when there is problem with accessing ctd database
   */
  private Map<MiriamData, String> getChemicalsByDisease(MiriamData disease) throws ChemicalSearchException {
    if (diseases.get(disease) == null) {
      diseases.put(disease, chemicalParser.getChemicalsForDisease(disease));
    }
    return diseases.get(disease);
  }

  @Override
  public void cacheDataForModel(Model originalModel, IProgressUpdater iProgressUpdater) {
    double progress = 0.0;
    iProgressUpdater.setProgress(progress * IProgressUpdater.MAX_PROGRESS);
    MiriamData disease = originalModel.getProject().getDisease();
    if (disease != null) {
      logger.debug("Caching chemical queries...");
      Set<MiriamData> targetMiriams = new HashSet<>();
      List<Model> models = new ArrayList<>();
      models.add(originalModel);
      models.addAll(originalModel.getSubmodels());
      for (Model model : models) {
        for (BioEntity element : model.getBioEntities()) {
          for (MiriamData md : element.getMiriamData()) {
            if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
              targetMiriams.add(md);
            } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
              targetMiriams.add(md);
            }
          }
        }
      }
      double counter = 0.0;
      for (MiriamData md : targetMiriams) {
        try {
          List<Chemical> chemicalList = chemicalParser.getChemicalListByTarget(md, disease);
          for (Chemical chemical : chemicalList) {
            cacheMiriamData(chemical);
          }
        } catch (ChemicalSearchException | AnnotatorException e) {
          logger.error("Problem with accessing info about chemical for target: " + md, e);
        }
        counter += 1;
        progress = counter / (double) targetMiriams.size();
        iProgressUpdater.setProgress(progress * IProgressUpdater.MAX_PROGRESS);
      }
    } else {
      logger.debug("Caching chemical queries not possible...");
    }
  }

}
