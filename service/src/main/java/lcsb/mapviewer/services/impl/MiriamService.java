package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.interfaces.IMiriamService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of service responsible for accessing miriam registry.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class MiriamService implements IMiriamService {

	/**
	 * Object accessing <a href= "http://www.ebi.ac.uk/miriam/main/" >miriam
	 * registry</a>.
	 */
	@Autowired
	private MiriamConnector	miriamConnector;

	@Override
	public String getUrlForMiriamData(MiriamData md) {
		return miriamConnector.getUrlString(md);
	}

	/**
	 * @return the miriamConnector
	 * @see #miriamConnector
	 */
	public MiriamConnector getMiriamConnector() {
		return miriamConnector;
	}

	/**
	 * @param miriamConnector
	 *          the miriamConnector to set
	 * @see #miriamConnector
	 */
	public void setMiriamConnector(MiriamConnector miriamConnector) {
		this.miriamConnector = miriamConnector;
	}

	@Override
	public MiriamType getTypeForUri(String uri) {
		return MiriamType.getTypeByUri(uri);
	}

}
