package lcsb.mapviewer.services.impl;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Custom password encoder using MD5 hash. Used for compatibility reasons.
 * 
 * @author Piotr Gawron
 *
 */

public class Md5PasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence password) {

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(encodeUtf8(password));
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < thedigest.length; ++i) {
				// CHECKSTYLE:OFF
				// this line transforms single byte into hex representation
				sb.append(Integer.toHexString((thedigest[i] & 0xFF) | 0x100).substring(1, 3));
				// CHECKSTYLE:ON
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean matches(CharSequence arg0, String arg1) {
		return encode(arg0).equals(arg1);
	}

	/**
	 * Transforms {@link CharSequence} into byte array.
	 * 
	 * @param string
	 *          input {@link CharSequence}
	 * @return byte array representation of the input {@link CharSequence}
	 */
	private static byte[] encodeUtf8(CharSequence string) {
		ByteBuffer bb = Charset.forName("UTF-8").encode(CharBuffer.wrap(string));
		byte[] result = new byte[bb.remaining()];
		bb.get(result);
		return result;
	}
}
