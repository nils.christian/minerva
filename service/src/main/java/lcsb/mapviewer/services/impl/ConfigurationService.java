package lcsb.mapviewer.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.EnumUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.FrameworkVersion;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

/**
 * Service implementation used for accessing and modifying configuration
 * parameters.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class ConfigurationService implements IConfigurationService {

  /**
   * Number of bytes in a megabyte.
   */
  private static final int MEGABYTE_SIZE = 1024 * 1024;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = Logger.getLogger(ConfigurationService.class);

  /**
   * Data access object for configuration parameters.
   */
  @Autowired
  private ConfigurationDao configurationDao;

  @Override
  public String getConfigurationValue(ConfigurationElementType type) {
    ConfigurationOption result = configurationDao.getByType(type);
    if (result == null) {
      result = new ConfigurationOption();
      result.setType(type);
      result.setValue(type.getDefaultValue());
      configurationDao.add(result);
    }
    return result.getValue();
  }

  @Override
  public void setConfigurationValue(ConfigurationElementType type, String value) {
    ConfigurationOption configuration = configurationDao.getByType(type);
    if (configuration == null) {
      configuration = new ConfigurationOption();
      configuration.setType(type);
    }
    configuration.setValue(value);
    configurationDao.add(configuration);

    if (type.equals(ConfigurationElementType.X_FRAME_DOMAIN)) {
      Configuration.getxFrameDomain().clear();
      for (String domain : getConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN).split(";")) {
        Configuration.getxFrameDomain().add(domain);
      }
    }
  }

  @Override
  public void deleteConfigurationValue(ConfigurationElementType type) {
    configurationDao.delete(configurationDao.getByType(type));
  }

  @Override
  public List<ConfigurationOption> getAllValues(boolean includeServerSide) {
    List<ConfigurationOption> result = new ArrayList<>();

    for (ConfigurationElementType type : ConfigurationElementType.values()) {
      if (!type.isServerSide() || (type.isServerSide() && includeServerSide)) {
        ConfigurationOption configuration = configurationDao.getByType(type);
        if (configuration == null) {
          getConfigurationValue(type);
          configuration = configurationDao.getByType(type);
        }
        result.add(configuration);
      }
    }
    return result;
  }

  @Override
  public ConfigurationOption getValue(ConfigurationElementType type) {
    ConfigurationOption configuration = configurationDao.getByType(type);
    if (configuration == null) {
      getConfigurationValue(type);
      configuration = configurationDao.getByType(type);
    }
    return configuration;
  }

  @Override
  public void updateConfiguration(List<ConfigurationOption> values) {
    for (ConfigurationOption configurationElement : values) {
      setConfigurationValue(configurationElement.getType(), configurationElement.getValue());
    }
  }

  /**
   * @return the configurationDao
   */
  public ConfigurationDao getConfigurationDao() {
    return configurationDao;
  }

  /**
   * @param configurationDao
   *          the configurationDao to set
   */
  public void setConfigurationDao(ConfigurationDao configurationDao) {
    this.configurationDao = configurationDao;
  }

  @Override
  public String getSystemSvnVersion(String baseDir) {
    return lcsb.mapviewer.common.Configuration.getSystemVersion(baseDir);
  }

  @Override
  public String getSystemGitVersion(String baseDir) {
    return lcsb.mapviewer.common.Configuration.getSystemBuildVersion(baseDir);
  }

  @Override
  public String getSystemBuild(String baseDir) {
    return lcsb.mapviewer.common.Configuration.getSystemBuild(baseDir);
  }

  @Override
  public FrameworkVersion getSystemVersion(String baseDir) {
    return lcsb.mapviewer.common.Configuration.getFrameworkVersion(baseDir);
  }

  @Override
  public Long getMemoryUsage() {
    Runtime runtime = Runtime.getRuntime();

    return (runtime.totalMemory() - runtime.freeMemory()) / MEGABYTE_SIZE;
  }

  @Override
  public Long getMaxMemory() {
    Runtime runtime = Runtime.getRuntime();

    return runtime.maxMemory() / MEGABYTE_SIZE;
  }

  @Override
  public ConfigurationOption getValue(PrivilegeType type) {
    String name = "DEFAULT_" + type.name();
    if (EnumUtils.isValidEnum(ConfigurationElementType.class, name)) {
      return getValue(ConfigurationElementType.valueOf(name));
    } else {
      return null;
    }
  }

}
