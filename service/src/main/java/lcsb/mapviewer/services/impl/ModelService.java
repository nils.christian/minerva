package lcsb.mapviewer.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;

/**
 * Implementation of the service that manages models.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class ModelService implements IModelService {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ModelService.class);

  /**
   * List of cached models.
   */
  private static Map<String, Model> models = new HashMap<String, Model>();

  /**
   * List of models that are currently being loaded from database.
   */
  private static Set<String> modelsInLoadStage = new HashSet<>();

  /**
   * Service that manages and gives access to user information.
   */
  @Autowired
  private IUserService userService;

  /**
   * Data access object for models.
   */
  @Autowired
  private ModelDao modelDao;

  @Autowired
  private ProjectDao projectDao;

  /**
   * Local backend to the pubmed data.
   */
  @Autowired
  private PubmedParser backend;

  /**
   * Service used for managing layouts.
   */
  @Autowired
  private ILayoutService layoutService;

  /**
   * Connector used for accessing data from miriam registry.
   */
  @Autowired
  private MiriamConnector miriamConnector;

  @Override
  public Model getLastModelByProjectId(String projectName, String token)
      throws lcsb.mapviewer.services.SecurityException {
    if (projectName == null) {
      return null;
    }

    // check if model is being loaded by another thread
    boolean waitForModel = false;
    do {
      synchronized (modelsInLoadStage) {
        waitForModel = modelsInLoadStage.contains(projectName);
      }
      // if model is being loaded then wait until it's loaded
      if (waitForModel) {
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          logger.fatal(e, e);
        }
      }
    } while (waitForModel);

    Model model = models.get(projectName);
    if (model == null) {
      try {
        // mark model as being load stage, so other threads that want to access
        // it will wait
        synchronized (modelsInLoadStage) {
          modelsInLoadStage.add(projectName);
        }
        logger.debug("Unknown model, trying to load the model into memory.");

        ModelData modelData = modelDao.getLastModelForProjectIdentifier(projectName, false);

        if (modelData == null) {
          logger.debug("Model doesn't exist");
          return null;
        }
        logger.debug("Model loaded from db.");
        model = new ModelFullIndexed(modelData);

        // this is a trick to load all required subelements of the model... ;/
        // lets copy model - it will access all elements...
        new CopyCommand(model).execute();

        for (ModelData m : model.getProject().getModels()) {
          new CopyCommand(m.getModel()).execute();
        }

        logger.debug("Model loaded successfullly");
        models.put(projectName, model);
      } finally {
        // model is not being loaded anymore
        synchronized (modelsInLoadStage) {
          modelsInLoadStage.remove(projectName);
        }
      }
    }
    if (userService.userHasPrivilege(token, PrivilegeType.VIEW_PROJECT, model.getProject())) {
      return model;
    } else if (userService.userHasPrivilege(token, PrivilegeType.ADD_MAP)) {
      return model;
    } else {
      logger.debug(userService.getUserByToken(token).getLogin());
      throw new SecurityException("User doesn't have access to project");
    }
  }

  @Override
  public void cacheAllPubmedIds(Model model, IProgressUpdater updater) {
    logger.debug("Caching pubmed ids...");
    if (model != null) {
      Set<Integer> pubmedIds = new HashSet<>();

      for (Element element : model.getElements()) {
        for (MiriamData md : element.getMiriamData()) {
          if (MiriamType.PUBMED.equals(md.getDataType())) {
            try {
              pubmedIds.add(Integer.parseInt(md.getResource()));
            } catch (NumberFormatException e) {
              logger.error("Problem with parsing: " + e.getMessage(), e);
            }
          }
        }
      }
      for (Reaction reaction : model.getReactions()) {
        for (MiriamData md : reaction.getMiriamData()) {
          if (MiriamType.PUBMED.equals(md.getDataType())) {
            try {
              pubmedIds.add(Integer.parseInt(md.getResource()));
            } catch (NumberFormatException e) {
              logger.error("Problem with parsing: " + e.getMessage(), e);
            }
          }
        }
      }
      double amount = pubmedIds.size();
      double counter = 0;
      for (Integer id : pubmedIds) {
        try {
          Article art = backend.getPubmedArticleById(id);
          if (art == null) {
            logger.warn("Cannot find pubmed article. Pubmed_id = " + id);
          }
        } catch (PubmedSearchException e) {
          logger.warn("Problem with accessing info about pubmed: " + id, e);
        }
        counter++;
        updater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
      }
    }
    logger.debug("Caching finished");
  }

  @Override
  public void removeModelFromCache(Model model) {
    models.remove(model.getProject().getProjectId());

  }

  /**
   * @return the modelDao
   * @see #modelDao
   */
  public ModelDao getModelDao() {
    return modelDao;
  }

  /**
   * @param modelDao
   *          the modelDao to set
   * @see #modelDao
   */
  public void setModelDao(ModelDao modelDao) {
    this.modelDao = modelDao;
  }

  /**
   * @return the backend
   * @see #backend
   */
  public PubmedParser getBackend() {
    return backend;
  }

  /**
   * @param backend
   *          the backend to set
   * @see #backend
   */
  public void setBackend(PubmedParser backend) {
    this.backend = backend;
  }

  @Override
  public void cacheAllMiriamLinks(Model model, IProgressUpdater updater) {
    logger.debug("Caching miriam ids...");
    if (model != null) {
      Set<MiriamData> pubmedIds = new HashSet<MiriamData>();

      for (Element element : model.getElements()) {
        pubmedIds.addAll(element.getMiriamData());
      }
      for (Reaction reaction : model.getReactions()) {
        pubmedIds.addAll(reaction.getMiriamData());
      }
      double amount = pubmedIds.size();
      double counter = 0;
      for (MiriamData md : pubmedIds) {
        miriamConnector.getUrlString(md);
        counter++;
        updater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
      }
    }
    logger.debug("Caching finished");

  }

  @Override
  public void removeModelFromCache(ModelData model) {
    models.remove(model.getProject().getProjectId());
  }

  /**
   * @return the layoutService
   * @see #layoutService
   */
  public ILayoutService getLayoutService() {
    return layoutService;
  }

  /**
   * @param layoutService
   *          the layoutService to set
   * @see #layoutService
   */
  public void setLayoutService(ILayoutService layoutService) {
    this.layoutService = layoutService;
  }

  @Override
  public void removeModelFromCacheByProjectId(String projectId) {
    models.remove(projectId);
  }

  /**
   * Returns list of {@link Element aliases} for given identifiers.
   * 
   * @param model
   *          model where aliases are located
   * @param identifiers
   *          list of alias identifiers in a given submodel. Every {@link Pair}
   *          contains information about {@link Model#getId() model identifier}
   *          (in {@link Pair#left}) and
   *          {@link lcsb.mapviewer.model.map.species.Element#getId() alias
   *          identifier} (in {@link Pair#right}).
   * @return list of {@link Element aliases} for given identifiers
   */
  private List<Element> getAliasesByIds(Model model, List<Pair<Integer, Integer>> identifiers) {
    Map<Integer, Set<Integer>> identifiersForMap = new HashMap<>();
    for (Pair<Integer, Integer> pair : identifiers) {
      Set<Integer> set = identifiersForMap.get(pair.getLeft());
      if (set == null) {
        set = new HashSet<>();
        identifiersForMap.put(pair.getLeft(), set);
      }
      set.add(pair.getRight());
    }

    List<Element> result = new ArrayList<>();
    List<Model> models = new ArrayList<>();
    models.add(model);
    models.addAll(model.getSubmodels());
    for (Model model2 : models) {
      Set<Integer> set = identifiersForMap.get(model2.getModelData().getId());
      if (set != null) {
        for (Element alias : model2.getElements()) {
          if (set.contains(alias.getId())) {
            result.add(alias);
            set.remove(alias.getId());
          }
          if (set.size() == 0) {
            break;
          }
        }
        if (set.size() > 0) {
          Integer aliasId = set.iterator().next();
          throw new InvalidArgumentException(
              "Cannot find alias (id: " + aliasId + ") in a model (id: " + model2.getModelData().getId() + ")");
        }
        identifiersForMap.remove(model2.getModelData().getId());
      }
    }
    if (identifiersForMap.keySet().size() > 0) {
      Integer modelId = identifiersForMap.keySet().iterator().next();
      Integer aliasId = identifiersForMap.get(modelId).iterator().next();
      throw new InvalidArgumentException(
          "Cannot find alias (id: " + aliasId + ") in a model (id: " + modelId + "). Model doesn't exist.");
    }
    return result;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * @return the projectDao
   * @see #projectDao
   */
  public ProjectDao getProjectDao() {
    return projectDao;
  }

  /**
   * @param projectDao
   *          the projectDao to set
   * @see #projectDao
   */
  public void setProjectDao(ProjectDao projectDao) {
    this.projectDao = projectDao;
  }

  @Override
  public void updateModel(ModelData model, String token) throws lcsb.mapviewer.services.SecurityException {
    Model topCachedData = getLastModelByProjectId(model.getProject().getProjectId(), token);
    Model cachedData = topCachedData.getSubmodelById(model.getId());
    cachedData.setDefaultCenterX(model.getDefaultCenterX());
    cachedData.setDefaultCenterY(model.getDefaultCenterY());
    cachedData.setDefaultZoomLevel(model.getDefaultZoomLevel());
    modelDao.update(model);

  }

}
