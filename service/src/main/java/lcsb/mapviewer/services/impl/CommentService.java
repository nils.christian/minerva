package lcsb.mapviewer.services.impl;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.ObjectUtils;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.UserAccessException;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailSender;

/**
 * This class is responsible for all services connected with comments
 * functionality.
 * 
 * @author Piotr Gawron
 * 
 */

@Transactional(value = "txManager")
public class CommentService implements ICommentService {

  /**
   * Max distance between two points on map for which two comments should be
   * considered as referring to the same point.
   */
  private static final double COMMENT_POINT_DISTANCE_EPSILON = 0.02;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(CommentService.class);

  /**
   * Data access object fir comments.
   */
  @Autowired
  private CommentDao commentDao;

  /**
   * Service for operations on Model object.
   */
  @Autowired
  private IModelService modelService;

  /**
   * Service for operations on User object.
   */
  @Autowired
  private IUserService userService;

  /**
   * Service for operations on Configuration object.
   */
  @Autowired
  private IConfigurationService configurationService;

  @Override
  public Comment addComment(String name, String email, String content, Model model, Point2D coordinates, Object object,
      boolean pinned, Model submodel) {
    Comment comment = new Comment();
    comment.setName(name);
    comment.setEmail(email);
    comment.setContent(content);
    comment.setModel(model);
    comment.setCoordinates(coordinates);
    if (object != null) {
      comment.setTableName(object.getClass());
      comment.setTableId(ObjectUtils.getIdOfObject(object));
      if (object instanceof Element) {
        comment.setSubmodel(((Element) object).getModel());
      } else if (object instanceof Reaction) {
        comment.setSubmodel(((Reaction) object).getModel());
      } else {
        throw new InvalidClassException("Cannot comment on object of class: " + object.getClass());
      }
    } else {
      comment.setSubmodel(submodel);
    }
    comment.setUser(null);
    comment.setPinned(pinned);
    commentDao.add(comment);

    try {
      EmailSender emailSender = new EmailSender(configurationService);
      emailSender.sendEmail("New comment appeard in the " + model.getProject().getProjectId(), content,
          model.getProject().getNotifyEmail());
    } catch (MessagingException e) {
      logger.error("Problem with sending email.", e);
    }

    return comment;
  }

  /**
   * This method returns all comments for model agregated by commented elements.
   * 
   * @param model
   *          object to which we are looking for comments
   * @param pinned
   *          this parameter defines what kind of comments we want to see:
   *          <ul>
   *          <li>null - all comments</li>
   *          <li>false - comments that are not visible on the map</li>
   *          <li>true - comments that are visible on the map</li>
   *          </ul>
   * @return list of comments grouped by commented elements
   */
  protected List<List<Comment>> getAgregatedComments(Model model, Boolean pinned) {
    List<Comment> comments = commentDao.getCommentByModel((Model) model, pinned, false);
    List<List<Comment>> result = new ArrayList<List<Comment>>();

    Map<String, List<Comment>> objects = new HashMap<String, List<Comment>>();
    for (Comment comment : comments) {
      if (comment.getTableName() == null) {
        List<Comment> list = new ArrayList<Comment>();
        list.add(comment);
        result.add(list);
      } else {
        String id = comment.getTableId() + "," + comment.getTableName();

        List<Comment> list = objects.get(id);
        if (list == null) {
          list = new ArrayList<Comment>();
          objects.put(id, list);
          result.add(list);
        }
        list.add(comment);
      }
    }
    return result;
  }

  @Override
  public void deleteComment(User loggedUser, String commentId, String reason) {
    int id = -1;
    try {
      id = Integer.parseInt(commentId);

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    Comment comment = commentDao.getById(id);
    if (comment == null) {
      logger.error("Invalid comment id: " + commentId);
    } else {
      comment.setDeleted(true);
      comment.setRemoveReason(reason);
      commentDao.update(comment);
    }
  }

  @Override
  public long getCommentCount() {
    return commentDao.getCount();
  }

  @Override
  public void removeCommentsForModel(Model model) {
    List<Comment> comments = commentDao.getCommentByModel(model, null, null);
    for (Comment comment : comments) {
      commentDao.delete(comment);
    }
  }

  /**
   * @return the commentDao
   * @see #commentDao
   */
  public CommentDao getCommentDao() {
    return commentDao;
  }

  /**
   * @param commentDao
   *          the commentDao to set
   * @see #commentDao
   */
  public void setCommentDao(CommentDao commentDao) {
    this.commentDao = commentDao;
  }

  /**
   * @return the modelService
   * @see #modelService
   */
  public IModelService getModelService() {
    return modelService;
  }

  /**
   * @param modelService
   *          the modelService to set
   * @see #modelService
   */
  public void setModelService(IModelService modelService) {
    this.modelService = modelService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @Override
  public void removeCommentsForModel(ModelData model) {
    List<Comment> comments = commentDao.getCommentByModel(model, null, null);
    for (Comment comment : comments) {
      commentDao.delete(comment);
    }
  }

  /**
   * Checks if identifier of the points refer to the same point.
   * 
   * @param uniqueId
   *          identifier assigned by the java code
   * @param objectId
   *          identifier assigned by the javascript code (browser)
   * @return <code>true</code> if both identifiers refer to the same point,
   *         <code>false</code> otherwise
   */
  protected boolean equalPoints(String uniqueId, String objectId) {
    String tmp = uniqueId.substring(uniqueId.indexOf("[") + 1, uniqueId.indexOf("]"));
    String[] ids = tmp.split(",");
    Double x1 = Double.valueOf(ids[0].trim());
    Double y1 = Double.valueOf(ids[1].trim());

    tmp = objectId.substring(objectId.indexOf("(") + 1, objectId.indexOf(")"));
    ids = tmp.split(",");
    Double x2 = Double.valueOf(ids[0].trim());
    Double y2 = Double.valueOf(ids[1].trim());

    if (Math.abs(x1 - x2) > COMMENT_POINT_DISTANCE_EPSILON || Math.abs(y1 - y2) > COMMENT_POINT_DISTANCE_EPSILON) {
      return false;
    }
    return true;
  }

  @Override
  public List<Comment> getCommentsByProject(Project project, String token) throws SecurityException {
    boolean editComments = userService.userHasPrivilege(token, PrivilegeType.EDIT_COMMENTS_PROJECT, project);
    boolean viewProject = userService.userHasPrivilege(token, PrivilegeType.VIEW_PROJECT, project);
    boolean manageProjects = userService.userHasPrivilege(token, PrivilegeType.PROJECT_MANAGEMENT);
    if (!editComments && !viewProject && !manageProjects) {
      throw new UserAccessException("You have no privileges to see comments for given project");
    }
    List<Comment> comments = new ArrayList<>();
    for (ModelData model : project.getModels()) {
      List<Comment> modelComments = commentDao.getCommentByModel(model, null, null);
      comments.addAll(modelComments);
    }

    return comments;
  }

  @Override
  public void deleteComment(Comment comment, String token, String reason) throws SecurityException {
    Project project = comment.getModelData().getProject();
    boolean editComments = userService.userHasPrivilege(token, PrivilegeType.EDIT_COMMENTS_PROJECT, project);
    User user = userService.getUserByToken(token);
    if (editComments || user.equals(comment.getUser())) {
      comment.setDeleted(true);
      comment.setRemoveReason(reason);
      commentDao.update(comment);
    } else {
      throw new UserAccessException("You have no privileges to remove the comment");
    }
  }

  @Override
  public Comment getCommentById(String commentId) {
    int id = -1;
    try {
      id = Integer.parseInt(commentId);
    } catch (NumberFormatException e) {
    }
    return commentDao.getById(id);
  }

}
