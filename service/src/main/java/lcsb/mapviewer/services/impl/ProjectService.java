package lcsb.mapviewer.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.ProblematicAnnotation;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.commands.ClearColorModelCommand;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.commands.SetFixedHierarchyLevelCommand;
import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.ComplexZipConverter;
import lcsb.mapviewer.converter.ComplexZipConverterParams;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.ProjectFactory;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.log.LogType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.LayoutStatus;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.user.ObjectPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserAnnotatorsParam;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.UserClassRequiredAnnotations;
import lcsb.mapviewer.model.user.UserClassValidAnnotations;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.UserAccessException;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILogService;
import lcsb.mapviewer.services.interfaces.ILogService.LogParams;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.overlay.AnnotatedObjectTreeRow;
import lcsb.mapviewer.services.search.chemical.IChemicalService;
import lcsb.mapviewer.services.search.drug.IDrugService;
import lcsb.mapviewer.services.search.mirna.IMiRNAService;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.BuildInLayout;

/**
 * Implementation of the project service. It allows to manage and access project
 * data.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class ProjectService implements IProjectService {

  /**
   * Size of the artificial buffer that will be released when
   * {@link OutOfMemoryError} is thrown to gain some free memory and report
   * problem.
   */
  private static final int OUT_OF_MEMORY_BACKUP_BUFFER_SIZE = 10000;

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ProjectService.class);

  /**
   * Data access object for projects.
   */
  @Autowired
  private ProjectDao projectDao;

  /**
   * Data access object for models.
   */
  @Autowired
  private ModelDao modelDao;

  /**
   * Data access object for users.
   */
  @Autowired
  private UserDao userDao;

  /**
   * Service that allows to access and manage models.
   */
  @Autowired
  private IModelService modelService;

  /**
   * Service that allows to access and manage comments.
   */
  @Autowired
  private ICommentService commentService;

  /**
   * Service used to access logs.
   * 
   * @see ILogService
   */
  @Autowired
  private ILogService logService;

  /**
   * Service that manages and gives access to configuration parameters.
   */
  @Autowired
  private IConfigurationService configurationService;

  /**
   * Services that manages and gives access to user information.
   */
  @Autowired
  private IUserService userService;

  /**
   * Services that access data about chemicals.
   */
  @Autowired
  private IChemicalService chemicalService;

  /**
   * Services that access data about drugs.
   */
  @Autowired
  private IDrugService drugService;

  /**
   * Services that access data about mirna.
   */
  @Autowired
  private IMiRNAService mirnaService;

  /**
   * Data access object for comments.
   */
  @Autowired
  private CommentDao commentDao;

  /**
   * Service that provides password encoding.
   */
  @Autowired
  private PasswordEncoder passwordEncoder;

  /**
   * Module that allows to annotate maps.
   */
  @Autowired
  private ModelAnnotator modelAnnotator;

  /**
   * Utils that help to manage the sessions in custom multithreaded
   * implementation.
   */
  @Autowired
  private DbUtils dbUtils;

  /**
   * Access point and parser for the online ctd database.
   */
  @Autowired
  private MeSHParser meshParser;

  /**
   * Access point and parser for the online ctd database.
   */
  @Autowired
  private TaxonomyBackend taxonomyBackend;

  /**
   * Class that helps to generate images for google maps API.
   */
  private MapGenerator generator = new MapGenerator();

  @Override
  public Project getProjectByProjectId(String name, String token) throws SecurityException {
    Project result = projectDao.getProjectByProjectId(name);
    if (result == null) {
      return result;
    }
    if (userService.userHasPrivilege(token, PrivilegeType.VIEW_PROJECT, result)) {
      return result;
    } else if (userService.userHasPrivilege(token, PrivilegeType.ADD_MAP)) {
      return result;
    }
    throw new UserAccessException("User cannot access project");
  }

  @Override
  public boolean projectExists(String projectName) {
    if (projectName == null || projectName.equals("")) {
      return false;
    }
    return projectDao.isProjectExistsByName(projectName);
  }

  @Override
  public List<Project> getAllProjects(String token) throws SecurityException {
    List<Project> projects = projectDao.getAll();
    if (userService.userHasPrivilege(token, PrivilegeType.ADD_MAP)) {
      return projects;
    }
    List<Project> result = new ArrayList<>();
    for (Project project : projects) {
      if (userService.userHasPrivilege(token, PrivilegeType.VIEW_PROJECT, project)) {
        result.add(project);
      }
    }
    return result;
  }

  /**
   * Removes all types of privileges from every user to the project given in the
   * parameter.
   * 
   * @param project
   *          from which project we remove privileges
   */
  private void removePrivilegesForProject(Project project) {
    for (PrivilegeType type : PrivilegeType.values()) {
      if (type.getPrivilegeObjectType() == Project.class) {
        userService.dropPrivilegesForObjectType(type, project.getId());
      }
    }
  }

  @Override
  public void removeProject(final Project p, final String dir, final boolean async, String token)
      throws SecurityException {
    if (!userService.userHasPrivilege(userService.getUserByToken(token), PrivilegeType.PROJECT_MANAGEMENT)) {
      throw new UserAccessException("User cannot remove project");
    }

    final String homeDir;
    if (dir != null) {
      if (p.getDirectory() != null) {
        homeDir = dir + "/../map_images/" + p.getDirectory() + "/";
      } else {
        homeDir = dir + "/../map_images/";
      }
    } else {
      if (p.getDirectory() != null) {
        homeDir = p.getDirectory() + "/";
      } else {
        homeDir = null;
      }
    }
    removePrivilegesForProject(p);
    updateProjectStatus(p, ProjectStatus.REMOVING, 0, new CreateProjectParams());
    Thread computations = new Thread(new Runnable() {

      @Override
      public void run() {
        if (async) {
          // because we are running this in separate thread we need to open a
          // new session for db connection
          dbUtils.createSessionForCurrentThread();
        }

        Project project = projectDao.getById(p.getId());

        try {
          String email = null;
          MapGenerator mapGenerator = new MapGenerator();
          for (ModelData originalModel : project.getModels()) {
            List<ModelData> models = new ArrayList<ModelData>();
            models.add(originalModel);
            for (ModelSubmodelConnection connection : originalModel.getSubmodels()) {
              models.add(connection.getSubmodel());
            }
            modelService.removeModelFromCache(originalModel);
            for (ModelData model : models) {
              logger.debug("Remove model: " + model.getId());
              commentService.removeCommentsForModel(model);

              for (Layout layout : model.getLayouts()) {
                try {
                  mapGenerator.removeLayout(layout, homeDir);
                } catch (IOException e) {
                  logger.error("Problem with removing directory: " + layout.getDirectory(), e);
                }
              }
            }
            email = project.getNotifyEmail();
          }
          if (homeDir != null) {
            File homeDirFile = new File(homeDir);
            if (homeDirFile.exists()) {
              logger.debug("Removing project directory: " + homeDirFile.getAbsolutePath());
              try {
                FileUtils.deleteDirectory(homeDirFile);
              } catch (IOException e) {
                logger.error("Problem with removing diriectory", e);
              }
            }
          }
          projectDao.delete(project);
          if (async) {
            projectDao.commit();
          }

          LogParams params = new LogParams().type(LogType.MAP_REMOVED).object(project);
          logService.log(params);

          if (email != null) {
            try {
              sendSuccesfullRemoveEmail(project.getProjectId(), email);
            } catch (MessagingException e) {
              logger.error("Problem with sending remove email.", e);
            }
          }
          modelService.removeModelFromCacheByProjectId(p.getProjectId());

        } catch (HibernateException e) {
          logger.error("Problem with database", e);
          handleHibernateExceptionRemovingReporting(project, e, token);
        } finally {
          if (async) {
            // close the transaction for this thread
            dbUtils.closeSessionForCurrentThread();
          }
        }

      }
    });

    if (async) {
      computations.start();
    } else {
      computations.run();
    }

  }

  /**
   * When we encountered hibernate exception we need to handle error reporting
   * differently (hibernate session is broken). This method handles such case when
   * hibernate exception occurred when removing project.
   * 
   * @param originalProject
   *          project that was being removed
   * @param exception
   *          hibernate exception that caused problems
   */
  protected void handleHibernateExceptionRemovingReporting(Project originalProject, HibernateException exception,
      String token) {
    // we need to open separate thread because current one thrown db exception
    // and transaction is corrupted and will be rolledback
    Thread reportInSeparateThread = new Thread(new Runnable() {

      @Override
      public void run() {
        dbUtils.createSessionForCurrentThread();
        try {
          // we need to get the project from db, because session where
          // originalProject was retrieved is broken
          Project project = getProjectByProjectId(originalProject.getProjectId(), token);
          String errorMessage = "Severe problem with removing object. Underlaying eror:\n" + exception.getMessage()
              + "\nMore information can be found in log file.";
          project.setErrors(errorMessage + "\n" + project.getErrors());
          project.setStatus(ProjectStatus.FAIL);
          projectDao.update(project);
        } catch (SecurityException e) {
          logger.error(e, e);
        } finally {
          dbUtils.closeSessionForCurrentThread();
        }
      }

    });
    reportInSeparateThread.start();
  }

  @Override
  public void addProject(Project project) {
    projectDao.add(project);

  }

  /**
   * This methods add privileges for the users listed in params to the project.
   * 
   * @param project
   *          to which project we add privileges
   * @param params
   *          which users should be included to have privileges to the project
   */
  private void addUsers(Project project, CreateProjectParams params) {
    if (project == null) {
      logger.warn("Users won't be added. Project not defined");
      return;
    }
    List<String[]> users = params.getUsers();
    String[][] newUsers = new String[users.size() + 1][2];
    for (int i = 0; i < users.size(); i++) {
      for (int j = 0; j < 2; j++) {
        newUsers[i][j] = users.get(i)[j];
      }
    }
    Set<User> processedUser = new HashSet<>();
    for (int i = 0; i < newUsers.length; i++) {
      String login = newUsers[i][0];
      User user = userService.getUserByLogin(login);
      if (user != null) {
        processedUser.add(user);
        logger.debug("Root privileges for " + login + " for project " + project.getProjectId());
        ObjectPrivilege privilege = new ObjectPrivilege(project, 1, PrivilegeType.VIEW_PROJECT, user);
        userService.setUserPrivilege(user, privilege);
        privilege = new ObjectPrivilege(project, 1, PrivilegeType.LAYOUT_MANAGEMENT, user);
        userService.setUserPrivilege(user, privilege);
        privilege = new ObjectPrivilege(project, 1, PrivilegeType.EDIT_COMMENTS_PROJECT, user);
        userService.setUserPrivilege(user, privilege);
      }
    }
    for (User user : userDao.getAll()) {
      if (!processedUser.contains(user)) {
        processedUser.add(user);
        userService.createDefaultProjectPrivilegesForUser(project, user);
      }
    }

  }

  /**
   * This method creates set of images for the model layouts.
   * 
   * @param originalModel
   *          model for which we create layout images
   * @param params
   *          configuration parameters including set of layouts to generate
   * @throws IOException
   *           thrown when there are problems with generating files
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   * @throws CommandExecutionException
   *           thrown when one of the files describing layouts is invalid
   */
  protected void createImages(final Model originalModel, final CreateProjectParams params)
      throws IOException, DrawingException, CommandExecutionException {
    if (!params.isImages()) {
      return;
    }
    updateProjectStatus(originalModel.getProject(), ProjectStatus.GENERATING_IMAGES, 0, params);

    List<Model> models = new ArrayList<>();
    models.add(originalModel);
    int size = originalModel.getLayouts().size();
    for (ModelSubmodelConnection connection : originalModel.getSubmodelConnections()) {
      models.add(connection.getSubmodel().getModel());
      size += connection.getSubmodel().getModel().getLayouts().size();
    }
    int counter = 0;
    for (final Model model : models) {
      for (int i = 0; i < model.getLayouts().size(); i++) {
        Layout layout = model.getLayouts().get(i);
        if (layout.getInputData() == null) {
          final double imgCounter = counter;
          final double finalSize = size;
          IProgressUpdater updater = new IProgressUpdater() {
            @Override
            public void setProgress(double progress) {
              updateProjectStatus(originalModel.getProject(), ProjectStatus.GENERATING_IMAGES,
                  IProgressUpdater.MAX_PROGRESS * imgCounter / finalSize + progress / finalSize, params);
            }
          };
          generateImagesForBuiltInOverlay(params, model, layout, updater);
        }
        counter++;
      }
    }
  }

  private void generateImagesForBuiltInOverlay(final CreateProjectParams params, final Model model, Layout layout,
      IProgressUpdater updater) throws CommandExecutionException, IOException, DrawingException {
    String directory = layout.getDirectory();
    Model output = model;
    if (layout.isHierarchicalView()) {
      output = new CopyCommand(model).execute();
      new SetFixedHierarchyLevelCommand(output, layout.getHierarchyViewLevel()).execute();
    }
    if (layout.getTitle().equals(BuildInLayout.CLEAN.getTitle())) {
      output = new CopyCommand(model).execute();
      new ClearColorModelCommand(output).execute();
    }
    MapGeneratorParams imgParams = generator.new MapGeneratorParams().directory(directory).sbgn(params.isSbgnFormat())
        .nested(layout.isHierarchicalView()).updater(updater);
    imgParams.model(output);
    generator.generateMapImages(imgParams);
  }

  /**
   * Creates project. Loads model from the input and run PostLoadModification.
   * 
   * @param params
   *          params used to create model
   * @param dbProject
   *          project where the model should be placed
   * @throws InvalidInputDataExecption
   *           thrown when there is a problem with input file
   */
  protected void createModel(final CreateProjectParams params, Project dbProject) throws InvalidInputDataExecption {
    ModelData modelData = modelDao.getLastModelForProjectIdentifier(params.getProjectId(), false);
    if (modelData != null) {
      throw new InvalidArgumentException("Model with the given name already exists");
    }

    final Project project = dbProject;
    updateProjectStatus(project, ProjectStatus.PARSING_DATA, 0.0, params);

    if (params.isComplex()) {
      try {
        Class<? extends IConverter> clazz = CellDesignerXmlParser.class;
        if (params.getParser() != null) {
          clazz = params.getParser().getClass();
        }
        ComplexZipConverter parser = new ComplexZipConverter(clazz);
        ComplexZipConverterParams complexParams;
        complexParams = new ComplexZipConverterParams().zipFile(params.getProjectFile());
        complexParams.visualizationDir(params.getProjectDir());
        for (ZipEntryFile entry : params.getZipEntries()) {
          complexParams.entry(entry);
        }
        ProjectFactory projectFactory = new ProjectFactory(parser);
        projectFactory.create(complexParams, dbProject);
      } catch (IOException e) {
        throw new InvalidInputDataExecption(e);
      }
    } else {
      IConverter parser;
      if (params.getParser() != null) {
        parser = params.getParser();
      } else if (params.getProjectFile().endsWith("sbgn")) {
        parser = new SbgnmlXmlConverter();
      } else {
        parser = new CellDesignerXmlParser();
      }
      try {
        Model model = parser.createModel(new ConverterParams().filename(params.getProjectFile())
            .sizeAutoAdjust(params.isAutoResize()).sbgnFormat(params.isSbgnFormat()));
        model.setName(params.getProjectName());
        project.addModel(model);
      } catch (FileNotFoundException ex) {
        throw new InvalidInputDataExecption(ex);
      }
    }
    Model topModel = project.getModels().iterator().next().getModel();
    topModel.setZoomLevels(generator.computeZoomLevels(topModel));
    topModel.setTileSize(MapGenerator.TILE_SIZE);
    dbProject.setNotifyEmail(params.getNotifyEmail());

    updateProjectStatus(project, ProjectStatus.UPLOADING_TO_DB, 0.0, params);
    dbUtils.setAutoFlush(false);
    projectDao.update(project);
    dbUtils.setAutoFlush(true);
    projectDao.flush();

    List<BuildInLayout> buildInLayouts = new ArrayList<>();
    BuildInLayout nested = null;
    if (params.isSemanticZoom()) {
      nested = BuildInLayout.SEMANTIC;
    } else {
      nested = BuildInLayout.NESTED;
    }
    if (params.isNetworkLayoutAsDefault()) {
      buildInLayouts.add(BuildInLayout.NORMAL);
      buildInLayouts.add(nested);
    } else {
      buildInLayouts.add(nested);
      buildInLayouts.add(BuildInLayout.NORMAL);
    }
    buildInLayouts.add(BuildInLayout.CLEAN);

    // reverse the order of build in layouts, so we can insert them at the
    // beginning of list of layouts (the order will be the same)
    Collections.reverse(buildInLayouts);

    for (BuildInLayout buildInLayout : buildInLayouts) {
      Layout topLayout = new Layout(buildInLayout.getTitle(),
          params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + topModel.getId() + "/", true);
      topLayout.setStatus(LayoutStatus.NA);
      topLayout.setProgress(0.0);
      topLayout.setHierarchicalView(buildInLayout.isNested());
      topModel.addLayout(0, topLayout);
      int submodelId = 1;
      List<Layout> semanticLevelOverlays = new ArrayList<>();
      if (buildInLayout.equals(BuildInLayout.SEMANTIC)) {
        for (int i = 0; i <= topModel.getZoomLevels(); i++) {
          String directory = params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + "-" + i + "-"
              + topModel.getId() + "/";
          Layout semanticOverlay = new Layout(buildInLayout.getTitle() + "-" + i, directory, true);
          semanticOverlay.setStatus(LayoutStatus.NA);
          semanticOverlay.setProgress(0.0);
          semanticOverlay.setHierarchicalView(buildInLayout.isNested());
          semanticOverlay.setHierarchyViewLevel(i);
          semanticLevelOverlays.add(semanticOverlay);
          topModel.addLayout(1, semanticOverlay);
        }
      }
      for (ModelSubmodelConnection connection : topModel.getSubmodelConnections()) {
        Layout layout = new Layout(buildInLayout.getTitle(),
            params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + submodelId + "/", true);
        layout.setStatus(LayoutStatus.NA);
        layout.setProgress(0.0);
        layout.setHierarchicalView(buildInLayout.isNested());
        layout.setParentLayout(topLayout);
        connection.getSubmodel().addLayout(0, layout);

        connection.getSubmodel().setZoomLevels(generator.computeZoomLevels(connection.getSubmodel().getModel()));
        connection.getSubmodel().setTileSize(MapGenerator.TILE_SIZE);
        if (buildInLayout.equals(BuildInLayout.SEMANTIC)) {
          for (int i = 0; i <= topModel.getZoomLevels(); i++) {
            String directory = params.getProjectDir() + "/" + buildInLayout.getDirectorySuffix() + "-" + i + "-"
                + submodelId + "/";
            Layout semanticOverlay = new Layout(buildInLayout.getTitle() + "-" + i, directory, true);
            semanticOverlay.setStatus(LayoutStatus.NA);
            semanticOverlay.setProgress(0.0);
            semanticOverlay.setHierarchicalView(buildInLayout.isNested());
            semanticOverlay.setParentLayout(semanticLevelOverlays.get(i));
            semanticOverlay.setHierarchyViewLevel(i);
            connection.getSubmodel().addLayout(1, semanticOverlay);
          }
        }
        submodelId++;
      }
    }

    if (params.isUpdateAnnotations()) {
      Map<Class<?>, List<ElementAnnotator>> annotators = null;
      if (params.getAnnotatorsMap() != null) {
        annotators = new HashMap<Class<?>, List<ElementAnnotator>>();
        for (Class<?> clazz : params.getAnnotatorsMap().keySet()) {
          annotators.put(clazz, modelAnnotator.getAnnotatorsFromCommonNames(params.getAnnotatorsMap().get(clazz)));
        }
      }
      logger.debug("Updating annotations");
      modelAnnotator.performAnnotations(topModel, new IProgressUpdater() {

        @Override
        public void setProgress(final double progress) {
          updateProjectStatus(project, ProjectStatus.ANNOTATING, progress, params);
        }
      }, annotators, params.getAnnotatorsParamsAsMap());
      logger.debug("Annotations updated");
    }
    logger.debug("Model created");

  }

  /**
   * Updates status of the generating project.
   * 
   * @param project
   *          project that is generated
   * @param status
   *          what is the current status
   * @param progress
   *          what is the progress
   * @param params
   *          parameters used for project creation
   */
  private void updateProjectStatus(Project project, ProjectStatus status, double progress, CreateProjectParams params) {
    if (project != null) {
      if (!status.equals(project.getStatus())
          || (Math.abs(progress - project.getProgress()) > IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION)) {
        project.setStatus(status);
        project.setProgress(progress);
        projectDao.update(project);
        if (params.isAsync()) {
          projectDao.commit();
        }
      }
    } else {
      logger.debug("status: " + status + ", " + progress);
    }
  }

  @Override
  public void createProject(final CreateProjectParams params) throws SecurityException {
    if (!userService.userHasPrivilege(params.getAuthenticationToken(), PrivilegeType.ADD_MAP)) {
      throw new SecurityException("Adding projects not allowed.");
    }

    // this count down is used to wait for asynchronous thread to initialize
    // data in the db (probably it would be better to move the initialization to
    // main thread)
    final CountDownLatch waitForInitialData = new CountDownLatch(1);

    Thread computations = new Thread(new Runnable() {

      @Override
      public void run() {
        if (params.isAsync()) {
          // because we are running this in separate thread we need to open a
          // new session for db connection
          dbUtils.createSessionForCurrentThread();
        }

        Project project = new Project();
        project.setProjectId(params.getProjectId());
        project.setName(params.getProjectName());
        if (params.getProjectDir() == null) {
          logger.warn("Project directory not set");
          project.setDirectory(null);
        } else {
          project.setDirectory(new File(params.getProjectDir()).getName());
        }
        project.setSbgnFormat(params.isSbgnFormat());

        MiriamData disease = null;
        if (params.getDisease() != null && !params.getDisease().isEmpty()) {
          disease = new MiriamData(MiriamType.MESH_2012, params.getDisease());
        }
        MiriamData organism = null;
        if (params.getOrganism() != null && !params.getOrganism().isEmpty()) {
          organism = new MiriamData(MiriamType.TAXONOMY, params.getOrganism());
        }
        project.setMapCanvasType(params.getMapCanvasType());
        project.setVersion(params.getVersion());
        projectDao.add(project);
        if (params.isAsync()) {
          projectDao.commit();
        }
        waitForInitialData.countDown();
        double[] outOfMemoryBuffer;
        EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
        try {
          Logger.getRootLogger().addAppender(appender);
          logger.debug("Running: " + params.getProjectId() + "; " + params.getProjectFile());
          outOfMemoryBuffer = new double[OUT_OF_MEMORY_BACKUP_BUFFER_SIZE];
          for (int i = 0; i < OUT_OF_MEMORY_BACKUP_BUFFER_SIZE; i++) {
            outOfMemoryBuffer[i] = Math.random() * OUT_OF_MEMORY_BACKUP_BUFFER_SIZE;
          }

          File inputFile = new File(params.getProjectFile());
          if (inputFile.exists()) {
            UploadedFileEntry file = new UploadedFileEntry();
            file.setFileContent(IOUtils.toByteArray(new FileInputStream(inputFile)));
            file.setOriginalFileName(FilenameUtils.getName(params.getProjectFile()));
            file.setLength(file.getFileContent().length);
            User owner = null;
            for (String[] userRow : params.getUsers()) {
              User user = userService.getUserByLogin(userRow[0]);
              if (user != null) {
                owner = user;
              }
            }
            file.setOwner(owner);
            project.setInputData(file);
          }

          createModel(params, project);
          Model originalModel = project.getModels().iterator().next().getModel();
          if (!params.isSemanticZoom()) {
            new CreateHierarchyCommand(originalModel, generator.computeZoomLevels(originalModel),
                generator.computeZoomFactor(originalModel)).execute();
            for (Model model : originalModel.getSubmodels()) {
              new CreateHierarchyCommand(model, generator.computeZoomLevels(model), generator.computeZoomFactor(model))
                  .execute();
            }
          }

          addUsers(project, params);
          createImages(originalModel, params);

          for (Layout layout : originalModel.getLayouts()) {
            String[] tmp = layout.getDirectory().split("[\\\\/]");
            layout.setDirectory(tmp[tmp.length - 1]);
          }
          for (ModelSubmodelConnection connection : originalModel.getSubmodelConnections()) {
            for (Layout layout : connection.getSubmodel().getLayouts()) {
              String[] tmp = layout.getDirectory().split("[\\\\/]");
              layout.setDirectory(tmp[tmp.length - 1]);
            }
          }

          try {
            if (meshParser.isValidMeshId(disease)) {
              project.setDisease(disease);
            } else {
              logger.warn("No valid disease is provided for project:" + project.getName());
            }
          } catch (AnnotatorException e1) {
            logger.warn("Problem with accessing mesh db. More info in logs.");
          }

          if (taxonomyBackend.getNameForTaxonomy(organism) != null) {
            project.setOrganism(organism);
          } else {
            logger.warn(project.getProjectId() + "\tNo valid organism is provided for project. " + organism);
          }

          modelDao.update(originalModel);

          if (params.isAnalyzeAnnotations()) {
            analyzeAnnotations(originalModel, params);
          }
          Logger.getRootLogger().removeAppender(appender);
          project.addLoggingInfo(appender);

          if (params.isCacheModel()) {
            cacheData(originalModel, params);
          }

          updateProjectStatus(project, ProjectStatus.DONE, IProgressUpdater.MAX_PROGRESS, params);
          if (project.getNotifyEmail() != null && !project.getNotifyEmail().equals("")) {
            try {
              sendSuccesfullEmail(originalModel);
            } catch (MessagingException e) {
              logger.error(e, e);
            }
          }

          LogParams params = new LogParams().description("Created successfully").type(LogType.MAP_CREATED)
              .object(originalModel);
          logService.log(params);

        } catch (HibernateException e) {
          outOfMemoryBuffer = null;
          logger.error("Problem with database", e);
          handleHibernateExceptionReporting(params, e);
        } catch (Exception e) {
          outOfMemoryBuffer = null;
          handleCreateProjectException(params, e);
        } catch (OutOfMemoryError oome) {
          // release some memory
          outOfMemoryBuffer = null;
          logger.error("Out of memory", oome);
          if (project != null) {
            project.setErrors("Out of memory: " + oome.getMessage());
          }
          updateProjectStatus(project, ProjectStatus.FAIL, IProgressUpdater.MAX_PROGRESS, params);
        } finally {
          if (params.isAsync()) {
            // close the transaction for this thread
            dbUtils.closeSessionForCurrentThread();
          }
          Logger.getRootLogger().removeAppender(appender);
        }
      }
    });
    if (params.isAsync()) {
      computations.start();
    } else {
      computations.run();
    }

    try {
      waitForInitialData.await();
    } catch (InterruptedException e1) {
      logger.error(e1, e1);
    }

  }

  /**
   * Cache pubmed data for the model.
   * 
   * @param originalModel
   *          model for which we want to cache data.
   * @param params
   *          parameters used for model generation
   */
  private void cacheData(final Model originalModel, final CreateProjectParams params) {
    modelService.cacheAllPubmedIds(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING, progress, params);
      }
    });
    modelService.cacheAllMiriamLinks(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_MIRIAM, progress, params);
      }
    });

    chemicalService.cacheDataForModel(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_CHEMICAL, progress, params);
      }
    });

    drugService.cacheDataForModel(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_DRUG, progress, params);
      }
    });

    mirnaService.cacheDataForModel(originalModel, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
        updateProjectStatus(originalModel.getProject(), ProjectStatus.CACHING_MI_RNA, progress, params);
      }
    });

  }

  /**
   * @return the modelDao
   * @see #modelDao
   */
  public ModelDao getModelDao() {
    return modelDao;
  }

  /**
   * @param modelDao
   *          the modelDao to set
   * @see #modelDao
   */
  public void setModelDao(ModelDao modelDao) {
    this.modelDao = modelDao;
  }

  /**
   * @return the commentService
   * @see #commentService
   */
  public ICommentService getCommentService() {
    return commentService;
  }

  /**
   * @param commentService
   *          the commentService to set
   * @see #commentService
   */
  public void setCommentService(ICommentService commentService) {
    this.commentService = commentService;
  }

  /**
   * @return the passwordEncoder
   * @see #passwordEncoder
   */
  public PasswordEncoder getPasswordEncoder() {
    return passwordEncoder;
  }

  /**
   * @param passwordEncoder
   *          the passwordEncoder to set
   * @see #passwordEncoder
   */
  public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * @return the commentDao
   * @see #commentDao
   */
  public CommentDao getCommentDao() {
    return commentDao;
  }

  /**
   * @param commentDao
   *          the commentDao to set
   * @see #commentDao
   */
  public void setCommentDao(CommentDao commentDao) {
    this.commentDao = commentDao;
  }

  /**
   * @return the modelAnnotator
   * @see #modelAnnotator
   */
  public ModelAnnotator getModelAnnotator() {
    return modelAnnotator;
  }

  /**
   * @param modelAnnotator
   *          the modelAnnotator to set
   * @see #modelAnnotator
   */
  public void setModelAnnotator(ModelAnnotator modelAnnotator) {
    this.modelAnnotator = modelAnnotator;
  }

  /**
   * @return the dbUtils
   * @see #dbUtils
   */
  public DbUtils getDbUtils() {
    return dbUtils;
  }

  /**
   * @param dbUtils
   *          the dbUtils to set
   * @see #dbUtils
   */
  public void setDbUtils(DbUtils dbUtils) {
    this.dbUtils = dbUtils;
  }

  /**
   * @return the projectDao
   * @see #projectDao
   */
  public ProjectDao getProjectDao() {
    return projectDao;
  }

  /**
   * @param projectDao
   *          the projectDao to set
   * @see #projectDao
   */
  public void setProjectDao(ProjectDao projectDao) {
    this.projectDao = projectDao;
  }

  /**
   * Analyzes annotation of the model and put information about invalid
   * annotations into the {@link Model#creationWarnings} field.
   * 
   * @param originalModel
   *          model to analyze
   * @param params
   *          parameters used for model generation
   */
  protected void analyzeAnnotations(final Model originalModel, final CreateProjectParams params) {
    logger.debug("Analyze annotations");
    Collection<? extends ProblematicAnnotation> improperAnnotations = modelAnnotator
        .findImproperAnnotations(originalModel, new IProgressUpdater() {

          @Override
          public void setProgress(double progress) {
            updateProjectStatus(originalModel.getProject(), ProjectStatus.VALIDATING_MIRIAM, progress, params);
          }
        }, params.getValidAnnotations());
    List<String> res = new ArrayList<>();
    for (ProblematicAnnotation improperAnnotation : improperAnnotations) {
      res.add(improperAnnotation.toString());
    }
    Collections.sort(res);

    Collection<? extends ProblematicAnnotation> missingAnnotations = modelAnnotator
        .findMissingAnnotations(originalModel, params.getRequiredAnnotations());
    for (ProblematicAnnotation improperAnnotation : missingAnnotations) {
      res.add(improperAnnotation.toString());
    }
    for (String message : res) {
      logger.warn(message);
    }
    logger.debug("Analyze finished");
  }

  /**
   * Sends notification email that map was removed.
   * 
   * @param projectId
   *          identifier of the project
   * @param email
   *          otification email
   * @throws MessagingException
   *           thrown when there is a problem with sending email
   */
  protected void sendSuccesfullRemoveEmail(String projectId, String email) throws MessagingException {
    EmailSender emailSender = new EmailSender(configurationService);
    StringBuilder content = new StringBuilder("Map " + projectId + " was successfully removed.<br/>");
    emailSender.sendEmail("MapViewer notification", content.toString(), email);
  }

  @Override
  public TreeNode createClassAnnotatorTree(User user) {

    UserAnnotationSchema annotationSchema = prepareUserAnnotationSchema(user);

    ElementUtils elementUtils = new ElementUtils();

    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();

    Class<?> clazz = top.getClazz();
    TreeNode root = new DefaultTreeNode(new AnnotatedObjectTreeRow(top, modelAnnotator.getAvailableAnnotators(clazz),
        modelAnnotator.getAnnotatorsFromCommonNames(annotationSchema.getAnnotatorsForClass(clazz)),
        annotationSchema.getValidAnnotations(clazz), annotationSchema.getRequiredAnnotations(clazz)), null);

    root.setExpanded(true);

    Queue<Pair<ClassTreeNode, TreeNode>> nodes = new LinkedList<Pair<ClassTreeNode, TreeNode>>();
    nodes.add(new Pair<ClassTreeNode, TreeNode>(top, root));
    // create children

    Queue<TreeNode> expandParents = new LinkedList<TreeNode>();

    while (!nodes.isEmpty()) {
      Pair<ClassTreeNode, TreeNode> element = nodes.poll();

      for (ClassTreeNode node : element.getLeft().getChildren()) {

        clazz = node.getClazz();
        AnnotatedObjectTreeRow data = new AnnotatedObjectTreeRow(node, modelAnnotator.getAvailableAnnotators(clazz),
            modelAnnotator.getAnnotatorsFromCommonNames(annotationSchema.getAnnotatorsForClass(clazz)),
            annotationSchema.getValidAnnotations(clazz), annotationSchema.getRequiredAnnotations(clazz));
        TreeNode treeNode = new DefaultTreeNode(data, element.getRight());
        nodes.add(new Pair<ClassTreeNode, TreeNode>(node, treeNode));
        if (data.getUsedAnnotators().size() > 0 || data.getValidAnnotators().size() > 0) {
          expandParents.add(treeNode);
        }
      }
    }
    while (!expandParents.isEmpty()) {
      TreeNode node = expandParents.poll();
      if (node.getParent() != null && !node.getParent().isExpanded()) {
        node.getParent().setExpanded(true);
        expandParents.add(node.getParent());
      }
    }

    return root;

  }

  /**
   * Retrieves (or creates) annotation schema for a given user.
   * 
   * @param user
   *          for this users {@link UserAnnotationSchema} will be prepared
   * @return {@link UserAnnotationSchema} for {@link User}
   */
  public UserAnnotationSchema prepareUserAnnotationSchema(User user) {
    UserAnnotationSchema annotationSchema = null;
    if (user != null) {
      annotationSchema = userDao.getById(user.getId()).getAnnotationSchema();
    }
    if (annotationSchema == null) {
      annotationSchema = new UserAnnotationSchema();

      ElementUtils elementUtils = new ElementUtils();

      ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();

      Map<Class<? extends BioEntity>, Set<MiriamType>> validMiriam = modelAnnotator.getDefaultValidClasses();
      Map<Class<? extends BioEntity>, Set<MiriamType>> requiredMiriam = modelAnnotator.getDefaultRequiredClasses();

      Queue<ClassTreeNode> nodes = new LinkedList<ClassTreeNode>();
      nodes.add(top);

      while (!nodes.isEmpty()) {
        ClassTreeNode element = nodes.poll();
        annotationSchema.addClassAnnotator(new UserClassAnnotators(element.getClazz(),
            modelAnnotator.getAvailableDefaultAnnotatorNames(element.getClazz())));
        annotationSchema.addClassValidAnnotations(
            new UserClassValidAnnotations(element.getClazz(), validMiriam.get(element.getClazz())));
        annotationSchema.addClassRequiredAnnotations(
            new UserClassRequiredAnnotations(element.getClazz(), requiredMiriam.get(element.getClazz())));
        for (ClassTreeNode node : element.getChildren()) {
          nodes.add(node);
        }
      }
      if (user != null) {
        User dbUser = userDao.getById(user.getId());
        dbUser.setAnnotationSchema(annotationSchema);
        userDao.update(dbUser);
      }
    }
    return annotationSchema;
  }

  @Override
  public void updateClassAnnotatorTreeForUser(User user, TreeNode annotatorsTree, boolean sbgnFormat,
      boolean networkLayoutAsDefault) {
    User dbUser = userDao.getById(user.getId());
    if (dbUser.getAnnotationSchema() == null) {
      dbUser.setAnnotationSchema(new UserAnnotationSchema());
    }
    UserAnnotationSchema annotationSchema = dbUser.getAnnotationSchema();

    Queue<TreeNode> queue = new LinkedList<TreeNode>();
    queue.add(annotatorsTree);
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();
      for (TreeNode child : node.getChildren()) {
        queue.add(child);
      }
      AnnotatedObjectTreeRow data = (AnnotatedObjectTreeRow) node.getData();
      annotationSchema.addClassAnnotator(new UserClassAnnotators(data.getClazz(), data.getUsedAnnotators()));
      annotationSchema.addClassRequiredAnnotations(
          new UserClassRequiredAnnotations(data.getClazz(), data.getRequiredAnnotations()));
      annotationSchema
          .addClassValidAnnotations(new UserClassValidAnnotations(data.getClazz(), data.getValidAnnotations()));
    }
    annotationSchema.setSbgnFormat(sbgnFormat);
    annotationSchema.setNetworkLayoutAsDefault(networkLayoutAsDefault);
    userService.updateUser(dbUser);
    user.setAnnotationSchema(annotationSchema);
  }

  @Override
  public List<UserAnnotatorsParam> getAnnotatorsParams(User user) {
    User dbUser = userDao.getById(user.getId());
    UserAnnotationSchema annotationSchema = dbUser.getAnnotationSchema();
    if (annotationSchema == null) {
      annotationSchema = new UserAnnotationSchema();
      dbUser.setAnnotationSchema(annotationSchema);
    }

    /*
     * Hibernate lazy loads collections so each element needs to be accessed to be
     * loaded now or otherwise the data might not be available when accessed because
     * the session might not be available at that time.
     */
    List<UserAnnotatorsParam> aps = new ArrayList<>();
    for (UserAnnotatorsParam ap : annotationSchema.getAnnotatorsParams()) {
      aps.add(ap);
    }
    return aps;
  }

  /**
   * Sends email about unsuccessful project creation.
   * 
   * @param projectName
   *          name of the project
   * @param email
   *          email where we want to send information
   * @param e
   *          exception that caused problem
   */
  private void sendUnsuccesfullEmail(String projectName, String email, Exception e) {
    EmailSender emailSender = new EmailSender(configurationService);
    StringBuilder content = new StringBuilder("");
    content.append("There was a problem with generating " + projectName + " map.<br/>");
    content.append(e.getClass().getName() + ": " + e.getMessage());
    try {
      emailSender.sendEmail("MapViewer notification", content.toString(), email);
    } catch (MessagingException e1) {
      logger.error(e1);
    }
  }

  /**
   * Sends email about successful project creation.
   * 
   * @param originalModel
   *          model that was created
   * @throws MessagingException
   *           exception thrown when there is a problem with sending email
   */
  protected void sendSuccesfullEmail(Model originalModel) throws MessagingException {
    EmailSender emailSender = new EmailSender(configurationService);
    StringBuilder content = new StringBuilder("Your map was generated successfully.<br/>");
    emailSender.sendEmail("MapViewer notification", content.toString(), originalModel.getProject().getNotifyEmail());
  }

  @Override
  public void updateProject(Project project, String token) throws SecurityException {
    projectDao.update(project);
    if (token != null) {
      Model model = modelService.getLastModelByProjectId(project.getProjectId(), token);
      if (model != null) {
        model.setProject(project);
      }
    }
  }

  /**
   * This method handles situation when sever db error appeared during uploading
   * of the project into database.
   * 
   * @param params
   *          parameters used to create project
   * @param e
   *          exception that occurred during uploading of the project
   */
  private void handleHibernateExceptionReporting(CreateProjectParams params, HibernateException e) {
    // we need to open separate thread because current one thrown db exception
    // and transaction is corrupted and will be rolledback
    Thread reportInSeparateThread = new Thread(new Runnable() {

      @Override
      public void run() {
        dbUtils.createSessionForCurrentThread();

        try {
          Project project = getProjectByProjectId(params.getProjectId(), params.getAuthenticationToken());
          String errorMessage = "Problem with uploading to database. "
              + "You might violated some unhandled constraints or you run out of memory. Underlaying eror:\n"
              + e.getMessage() + "\nMore information can be found in log file.";
          project.setErrors(errorMessage);
          project.setStatus(ProjectStatus.FAIL);
          projectDao.update(project);
        } catch (Exception e) {
          logger.error(e, e);
        } finally {
          dbUtils.closeSessionForCurrentThread();
        }
      }

    });
    reportInSeparateThread.start();

  }

  /**
   * Method that handles exception reporting during creation of a project.
   * 
   * @param params
   *          set of parameters used to create project
   * @param e
   *          exception that caused problems
   */
  private void handleCreateProjectException(final CreateProjectParams params, Exception e) {
    Project p = projectDao.getProjectByProjectId(params.getProjectId());
    logger.error(e.getMessage(), e);
    if (p != null) {
      p.setErrors("Problem with uploading map: " + e.getMessage() + ". More details can be found in log file.");
    }
    updateProjectStatus(p, ProjectStatus.FAIL, IProgressUpdater.MAX_PROGRESS, params);

    String email = params.getNotifyEmail();
    String projectName = params.getProjectId();

    LogParams logParams = new LogParams().description("Failed: " + e.getMessage()).type(LogType.MAP_CREATED).object(p);
    logService.log(logParams);
    if (email != null) {
      sendUnsuccesfullEmail(projectName, email, e);
    }
  }

  /**
   * @return the taxonomyBackend
   * @see #taxonomyBackend
   */
  public TaxonomyBackend getTaxonomyBackend() {
    return taxonomyBackend;
  }

  /**
   * @param taxonomyBackend
   *          the taxonomyBackend to set
   * @see #taxonomyBackend
   */
  public void setTaxonomyBackend(TaxonomyBackend taxonomyBackend) {
    this.taxonomyBackend = taxonomyBackend;
  }

  @Override
  public List<Project> getAllProjects() {
    return projectDao.getAll();
  }

}
