package lcsb.mapviewer.services.impl;

import java.util.Calendar;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.ObjectUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.log.GenericLog;
import lcsb.mapviewer.model.log.ObjectLog;
import lcsb.mapviewer.model.log.SystemLog;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.log.LogDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.ILogService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class responsible for logging.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class LogService implements ILogService {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger	= Logger.getLogger(LogService.class);

	/**
	 * Data access object for log entries.
	 */
	@Autowired
	private LogDao				logDao;

	/**
	 * Default used used for loggin events.
	 */
	private User					loggedUser;

	/**
	 * Service used to access logs.
	 * 
	 * @see ILogService
	 */
	@Autowired
	private UserDao				userDao;

	/**
	 * @return the logDao
	 * @see #logDao
	 */
	public LogDao getLogDao() {
		return logDao;
	}

	/**
	 * @param logDao
	 *          the logDao to set
	 * @see #logDao
	 */
	public void setLogDao(LogDao logDao) {
		this.logDao = logDao;
	}

	@Override
	public void setLoggedUser(User user) {
		this.loggedUser = user;
	}

	@Override
	public GenericLog log(LogParams params) {
		if (params.getType() == null) {
			throw new InvalidArgumentException("Type cannot be null");
		}

		GenericLog log;
		if (params.getObject() != null) {
			Object object = params.getObject();
			// get identifier of an object
			Integer id = null;
			try {
				id = ObjectUtils.getIdOfObject(object);
			} catch (Exception e) {
				throw new InvalidArgumentException("Invalid object: " + object.getClass());
			}
			if (id == null) {
				throw new InvalidArgumentException("Object must contain not null id");
			}

			log = new ObjectLog();
			((ObjectLog) log).setTable(object.getClass());
			((ObjectLog) log).setObjectId(id);

		} else {
			log = new SystemLog();
		}
		log.setTime(Calendar.getInstance());
		log.setType(params.getType());
		log.setDescription(params.description());

		if (params.getUser() != null) {
			log.setUser(params.getUser());
		} else if (loggedUser != null) {
			log.setUser(loggedUser);
		} else {
			log.setUser(userDao.getUserByLogin(Configuration.ANONYMOUS_LOGIN));
		}
		logDao.add(log);
		return log;
	}

}
