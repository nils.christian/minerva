package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.statistics.SearchHistory;
import lcsb.mapviewer.model.map.statistics.SearchType;
import lcsb.mapviewer.persist.dao.map.statistics.SearchHistoryDao;
import lcsb.mapviewer.services.interfaces.ISearchHistoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the service that manages search history.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class SearchHistoryService implements ISearchHistoryService {

	/**
	 * Data access object for search history entries.
	 */
	@Autowired
	private SearchHistoryDao	searchHistoryDao;

	@Override
	public void addQuery(String query, SearchType type, String ipAddress, String map) {
		SearchHistory searchHistory = new SearchHistory();
		searchHistory.setIpAddress(ipAddress);
		searchHistory.setQuery(query);
		searchHistory.setType(type);
		searchHistory.setMap(map);
		searchHistoryDao.add(searchHistory);
	}

	/**
	 * @return the searchHistoryDao
	 */
	public SearchHistoryDao getSearchHistoryDao() {
		return searchHistoryDao;
	}

	/**
	 * @param searchHistoryDao
	 *          the searchHistoryDao to set
	 */
	public void setSearchHistoryDao(SearchHistoryDao searchHistoryDao) {
		this.searchHistoryDao = searchHistoryDao;
	}

}
