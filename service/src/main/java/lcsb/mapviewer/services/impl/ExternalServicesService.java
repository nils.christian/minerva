package lcsb.mapviewer.services.impl;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.annotation.services.ChEMBLParser;
import lcsb.mapviewer.annotation.services.DrugbankHTMLParser;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.ChemicalParser;
import lcsb.mapviewer.annotation.services.annotators.BiocompendiumAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator;
import lcsb.mapviewer.annotation.services.annotators.EnsemblAnnotator;
import lcsb.mapviewer.annotation.services.annotators.EntrezAnnotator;
import lcsb.mapviewer.annotation.services.annotators.GoAnnotator;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.services.interfaces.IExternalServicesService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Implementation of the service that retrieves information about services that
 * access external resources (like chebi, chembl, etc.).
 * 
 * @author Piotr Gawron
 * 
 */
public class ExternalServicesService implements IExternalServicesService {

	/**
	 * List of services that should be checked for status.
	 */
	private List<IExternalService>	services				= new ArrayList<IExternalService>();

	/**
	 * Service accessing <a href="http://www.drugbank.ca/">drugbank</a>.
	 */
	@Autowired
	private DrugbankHTMLParser			drugbankHTMLParser;
	
	
	/**
	 * Service accessing <a href="http://ctdbase.org/">ctd</a>.
	 */
	@Autowired
	private ChemicalParser			ctdParser;

	/**
	 * Service accessing <a
	 * href="https://www.ebi.ac.uk/chembl/compound/inspect/">chembl</a>.
	 */
	@Autowired
	private ChEMBLParser						chEMBLParser;

	/**
	 * Service accessing <a
	 * href="http://www.ebi.ac.uk/chebi/webServices.do">chebi</a>.
	 */
	@Autowired
	private ChebiAnnotator					chebiBackend;

	/**
	 * Service accessing <a
	 * href="http://europepmc.org/RestfulWebService">pubmed</a>.
	 */
	@Autowired
	private PubmedParser						pubmedParser;

	/**
	 * Service accessing <a href= "http://www.ebi.ac.uk/miriam/main/" >miriam
	 * registry</a>.
	 */
	@Autowired
	private MiriamConnector					miriamConnector;

	/**
	 * Service accessing <a href= "http://biocompendium.embl.de/" >internal
	 * annotating service</a>.
	 */
	@Autowired
	private BiocompendiumAnnotator	annotationRestService;

	/**
	 * Service accessing <a href= "http://www.uniprot.org/" >uniprot</a>.
	 */
	@Autowired
	private UniprotAnnotator				uniprotAnnotator;

	/**
	 * Service accessing Ensembl database.
	 */
	@Autowired
	private EnsemblAnnotator				ensemblAnnotator;

	/**
	 * Service accessing Entrez database.
	 */
	@Autowired
	private EntrezAnnotator					entrezAnnotator;

	/**
	 * Service accessing <a href= "http://www.ebi.ac.uk/QuickGO/" >Gene
	 * Ontology</a>.
	 */
	@Autowired
	private GoAnnotator							goBackend;
	
	/**
	 * Service accessing HGNC restfull API .
	 */
	@Autowired
	private HgncAnnotator						hgncBackend;

	/**
	 * Utils that help to manage the sessions in custom multithreaded
	 * implementation.
	 */
	@Autowired
	private DbUtils									dbUtils;

	@Override
	public List<ExternalServiceStatus> getExternalServiceStatuses() {
		List<ExternalServiceStatus> result = new ArrayList<ExternalServiceStatus>();
		for (IExternalService service : services) {
			boolean sessionOpened = false;
			if (!dbUtils.isCustomSessionForCurrentThread()) {
				sessionOpened = true;
				dbUtils.createSessionForCurrentThread();
			}
			result.add(service.getServiceStatus());
			if (sessionOpened) {
				dbUtils.closeSessionForCurrentThread();
			}
		}
		return result;
	}

	@Override
	public void registerService(IExternalService service) {
		services.add(service);

	}

	@Override
	public void registerDefaultServices() {
		registerService(drugbankHTMLParser);
		registerService(chEMBLParser);
		registerService(chebiBackend);
		registerService(pubmedParser);
		registerService(miriamConnector);
		registerService(annotationRestService);
		registerService(ensemblAnnotator);
		registerService(entrezAnnotator);
		registerService(goBackend);
		registerService(ctdParser);
		registerService(hgncBackend);
		registerService(uniprotAnnotator);
	}

	@Override
	public void clearServices() {
		services.clear();

	}
}
