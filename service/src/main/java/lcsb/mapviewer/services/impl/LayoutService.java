package lcsb.mapviewer.services.impl;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.log.LogType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GeneVariation;
import lcsb.mapviewer.model.map.layout.GeneVariationColorSchema;
import lcsb.mapviewer.model.map.layout.GenericColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.LayoutStatus;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.ObjectPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.ILogService;
import lcsb.mapviewer.services.interfaces.ILogService.LogParams;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.ColorSchemaColumn;

/**
 * Implementation of the layout service.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class LayoutService implements ILayoutService {

  private static final Comparator<? super Pair<? extends BioEntity, ColorSchema>> ELEMENT_PAIR_COMPARATOR = new Comparator<Pair<? extends BioEntity, ColorSchema>>() {

    @Override
    public int compare(Pair<? extends BioEntity, ColorSchema> o1, Pair<? extends BioEntity, ColorSchema> o2) {
      return ((Integer) o1.getLeft().getId()).compareTo(o2.getLeft().getId());
    }
  };

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(LayoutService.class);

  /**
   * Layout data access object.
   */
  @Autowired
  private LayoutDao layoutDao;

  /**
   * Service that manages and gives access to user information.
   */
  @Autowired
  private IUserService userService;

  /**
   * Service used to access logs.
   *
   * @see ILogService
   */
  @Autowired
  private ILogService logService;

  /**
   * Service that manages and gives access to configuration parameters.
   */
  @Autowired
  private IConfigurationService configurationService;

  /**
   * Utility class that helps to manage the sessions in custom multi-threaded
   * implementation.
   */
  @Autowired
  private DbUtils dbUtils;

  /**
   * Object that sends emails.
   */
  private EmailSender emailSender;

  /**
   * Method called after spring initialized all interfaces.
   */
  @PostConstruct
  public void springInit() {
    emailSender = new EmailSender(configurationService);
  }

  @Override
  public boolean userCanAddLayout(Model model, User user) {
    // if we don't have privileges to view the object then we cannot add layouts
    if (!userService.userHasPrivilege(user, PrivilegeType.VIEW_PROJECT, model.getProject())) {
      return false;
    }
    long count = getAvailableCustomLayoutsNumber(user);
    return count > 0;
  }

  @Override
  public long getAvailableCustomLayoutsNumber(User user) {
    long level = userService.getUserPrivilegeLevel(user, PrivilegeType.CUSTOM_LAYOUTS);
    long layouts = layoutDao.getCountByUser(user);
    return level - layouts;

  }

  @Override
  public boolean userCanRemoveLayout(Layout layout, User user) {
    User creator = layout.getCreator();
    Project project = layout.getModel().getProject();
    if (creator == null) {
      return userService.userHasPrivilege(user, PrivilegeType.LAYOUT_MANAGEMENT, project);
    } else {
      return creator.getId().equals(user.getId())
          || userService.userHasPrivilege(user, PrivilegeType.LAYOUT_MANAGEMENT, project);
    }
  }

  private boolean userCanViewOverlay(Layout overlay, User user) {
    if (overlay.isPublicLayout()) {
      return true;
    }
    if (overlay.getCreator() == null) {
      return true;
    }
    if (overlay.getCreator().getLogin().equals(user.getLogin())) {
      return true;
    }

    if (userService.userHasPrivilege(user, PrivilegeType.LAYOUT_VIEW, overlay)) {
      return true;
    }
    if (userService.userHasPrivilege(user, PrivilegeType.LAYOUT_MANAGEMENT, overlay.getModel().getProject())) {
      return true;
    }
    return false;
  }

  @Override
  public void removeLayout(Layout layout, final String homeDir) throws IOException {
    final String dir;
    if (homeDir != null) {
      if (layout.getModel().getProject().getDirectory() != null) {
        dir = homeDir + "/../map_images/" + layout.getModel().getProject().getDirectory() + "/";
      } else {
        dir = homeDir + "/../map_images/";
      }
    } else {
      dir = null;
    }

    String projectId = layout.getModel().getProject().getProjectId();
    final String email;
    User user = layout.getCreator();
    if (user != null) {
      email = user.getEmail();
    } else {
      email = null;
    }

    layout.getModel().removeLayout(layout);
    layoutDao.delete(layout);

    LogParams params = new LogParams().object(layout).type(LogType.LAYOUT_REMOVED);
    logService.log(params);

    if (email != null) {
      Thread sendEmailThread = new Thread(new Runnable() {
        @Override
        public void run() {
          try {
            sendSuccesfullRemoveEmail(projectId, layout.getTitle(), email);
          } catch (MessagingException e) {
            logger.error(e);
          }
        }
      });
      sendEmailThread.start();
    }
    Thread removeFilesThread = new Thread(new Runnable() {

      @Override
      public void run() {
        MapGenerator generator = new MapGenerator();
        try {
          generator.removeLayout(layout, dir);
        } catch (IOException e) {
          logger.error(e);
        }

      }

    });
    removeFilesThread.start();
  }

  @Override
  public void updateLayout(Layout layout) {
    layoutDao.update(layout);
  }

  @Override
  public void addViewPrivilegeToLayout(Layout layout, User user) {
    ObjectPrivilege privilege = new ObjectPrivilege();
    privilege.setIdObject(layout.getId());
    privilege.setLevel(1);
    privilege.setType(PrivilegeType.LAYOUT_VIEW);
    privilege.setUser(user);
    userService.setUserPrivilege(user, privilege);
  }

  @Override
  public void dropViewPrivilegeFromLayout(Layout layout, User user) {
    ObjectPrivilege privilege = new ObjectPrivilege();
    privilege.setIdObject(layout.getId());
    privilege.setLevel(0);
    privilege.setType(PrivilegeType.LAYOUT_VIEW);
    privilege.setUser(user);
    userService.setUserPrivilege(user, privilege);
  }

  @Override
  public Layout createLayoutWithImages(final CreateLayoutParams params)
      throws IOException, InvalidColorSchemaException, CommandExecutionException {
    ColorSchemaReader reader = new ColorSchemaReader();
    final Collection<ColorSchema> schemas = reader.readColorSchema(params.getColorInputStream(),
        TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream()));
    final Model colorModel = new CopyCommand(params.getModel()).execute();
    new ColorModelCommand(params.getModel(), schemas, userService.getColorExtractorForUser(params.getUser())).execute();
    String[] tmp = params.getDirectory().split("[\\\\/]");
    String simpleDir = tmp[tmp.length - 1];

    layoutDao.flush();

    boolean customSession = false;
    if (params.isAsync()) {
      customSession = !dbUtils.isCustomSessionForCurrentThread();
      if (customSession) {
        dbUtils.createSessionForCurrentThread();
      }
    }
    final Map<Model, Integer> layoutIdByModel = new HashMap<>();

    Layout topLayout = new Layout(params.getName(), simpleDir, false);
    if (params.getUser() == null) {
      topLayout.setPublicLayout(true);
    } else {
      topLayout.setPublicLayout(false);
    }
    topLayout.setStatus(LayoutStatus.NA);
    topLayout.setProgress(0.0);
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(params.getColorInputStream()));
    fileEntry.setOriginalFileName(params.getLayoutFileName());
    fileEntry.setLength(fileEntry.getFileContent().length);
    fileEntry.setOwner(params.getUser());
    topLayout.setInputData(fileEntry);
    topLayout.setDescription(params.getDescription());
    params.getModel().addLayout(topLayout);
    topLayout.setCreator(params.getUser());
    layoutDao.add(topLayout);
    topLayout.setDirectory(simpleDir + topLayout.getId());

    layoutDao.update(topLayout);

    layoutIdByModel.put(colorModel, topLayout.getId());

    for (ModelSubmodelConnection connection : params.getModel().getSubmodelConnections()) {
      Layout layout = new Layout(params.getName(), simpleDir, false);
      layout.setStatus(LayoutStatus.NA);
      layout.setProgress(0.0);
      connection.getSubmodel().getModel().addLayout(layout);
      layout.setCreator(params.getUser());
      layout.setPublicLayout(false);
      topLayout.addLayout(layout);
      layoutDao.add(layout);
      layout.setDirectory(simpleDir + layout.getId());

      layoutDao.update(layout);

      layoutIdByModel.put(colorModel.getSubmodelByConnectionName(connection.getName()), layout.getId());
    }

    if (params.isAsync()) {
      if (customSession) {
        dbUtils.closeSessionForCurrentThread();
      } else {
        layoutDao.commit();
      }
    }
    final int layoutId = layoutIdByModel.get(colorModel);

    Thread computations = new Thread(new Runnable() {
      @Override
      public void run() {
        if (params.isAsync()) {
          // open transaction for this thread
          dbUtils.createSessionForCurrentThread();
        }

        try {
          MapGenerator generator = new MapGenerator();
          final int models = layoutIdByModel.entrySet().size();

          int count = 0;
          for (Model m : layoutIdByModel.keySet()) {
            final int counted = count;
            int id = layoutIdByModel.get(m);
            MapGeneratorParams imgParams = generator.new MapGeneratorParams().model(m)
                .directory(params.getDirectory() + id).updater(new IProgressUpdater() {
                  private int lastProgress = -1;

                  @Override
                  public void setProgress(double progress) {
                    progress = progress / ((double) models) + ((double) counted * MAX_PROGRESS) / ((double) models);
                    if (((int) progress) != lastProgress) {
                      Layout layout = layoutDao.getById(layoutId);
                      lastProgress = (int) progress;
                      layout.setProgress(progress);
                      layout.setStatus(LayoutStatus.GENERATING);
                      layoutDao.update(layout);
                      if (params.isAsync()) {
                        layoutDao.commit();
                      }
                    }
                  }
                });
            imgParams.sbgn(params.getModel().getProject().isSbgnFormat());
            generator.generateMapImages(imgParams);
            count++;
          }
          Layout layout = layoutDao.getById(layoutId);
          layout.setProgress(IProgressUpdater.MAX_PROGRESS);
          layout.setStatus(LayoutStatus.OK);
          layoutDao.update(layout);
          if (params.isAsync()) {
            layoutDao.commit();
          }

          try {
            sendSuccessfullGenerationEmail(params, schemas);
          } catch (MessagingException e) {
            logger.error("Problem with sending email", e);
          }
          logService
              .log(new LogParams().object(layout).description("Created successfully.").type(LogType.LAYOUT_CREATED));
        } catch (Exception e) {
          logger.error("Problem with creating layout", e);

          Layout layout = layoutDao.getById(layoutId);
          layout.setProgress(IProgressUpdater.MAX_PROGRESS);
          layout.setStatus(LayoutStatus.FAILURE);
          layoutDao.update(layout);

          try {
            emailSender.sendEmail("MapViewer status", "There was a problem with generating layout " + params.getName()
                + ". Contact administrator to solve this issue.", params.getUser());
          } catch (MessagingException e1) {
            logger.error("Problem with sending email", e1);
          }
          logService
              .log(new LogParams().object(layout).description("Problem during creation.").type(LogType.LAYOUT_CREATED));
        }
        if (params.isAsync()) {
          // close the transaction for this thread
          dbUtils.closeSessionForCurrentThread();
        }
      }
    });

    if (params.isAsync()) {
      computations.start();
    } else {
      computations.run();
    }
    return topLayout;
  }

  @Override
  public Layout createLayout(final CreateLayoutParams params) throws IOException, InvalidColorSchemaException {
    ColorSchemaReader reader = new ColorSchemaReader();
    final Collection<ColorSchema> schemas = reader.readColorSchema(params.getColorInputStream(),
        TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream()));

    // check if we can color our model using this schema,
    // if not then exception will be thrown and passed up
    try {
      Model copy = new CopyCommand(params.getModel()).execute();
      new ColorModelCommand(copy, schemas, userService.getColorExtractorForUser(params.getUser())).execute();
    } catch (CommandExecutionException e) {
      throw new InvalidColorSchemaException(e);
    }

    String[] tmp = params.getDirectory().split("[\\\\/]");
    String simpleDir = tmp[tmp.length - 1];

    layoutDao.flush();

    Layout topLayout = new Layout(params.getName(), simpleDir, false);
    if (params.getUser() == null) {
      topLayout.setPublicLayout(true);
    } else {
      topLayout.setPublicLayout(false);
    }
    topLayout.setGoogleLicenseConsent(params.isGoogleLicenseConsent());
    topLayout.setStatus(LayoutStatus.OK);
    topLayout.setProgress(0.0);
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(params.getColorInputStream()));
    fileEntry.setOriginalFileName(params.getLayoutFileName());
    fileEntry.setLength(fileEntry.getFileContent().length);
    fileEntry.setOwner(params.getUser());
    topLayout.setInputData(fileEntry);
    topLayout.setDescription(params.getDescription());
    params.getModel().addLayout(topLayout);
    topLayout.setCreator(params.getUser());
    layoutDao.add(topLayout);
    topLayout.setDirectory(simpleDir + topLayout.getId());

    layoutDao.update(topLayout);

    for (ModelSubmodelConnection connection : params.getModel().getSubmodelConnections()) {
      Layout layout = new Layout(params.getName(), simpleDir, false);
      layout.setStatus(LayoutStatus.OK);
      layout.setProgress(0.0);
      connection.getSubmodel().getModel().addLayout(layout);
      layout.setCreator(params.getUser());
      layout.setPublicLayout(false);
      topLayout.addLayout(layout);
      layoutDao.add(layout);
      layout.setDirectory(simpleDir + layout.getId());

      layoutDao.update(layout);
    }

    Thread computations = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          // open transaction for this thread
          dbUtils.createSessionForCurrentThread();
          sendSuccessfullGenerationEmail(params, schemas);
        } catch (MessagingException e) {
          logger.error("Problem with sending email", e);
        } finally {
          dbUtils.closeSessionForCurrentThread();
        }
      }
    });

    LogParams logParams = new LogParams().object(topLayout).description("Created successfully.")
        .type(LogType.LAYOUT_CREATED);
    logService.log(logParams);

    computations.start();
    return topLayout;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * @return the dbUtils
   * @see #dbUtils
   */
  public DbUtils getDbUtils() {
    return dbUtils;
  }

  /**
   * @param dbUtils
   *          the dbUtils to set
   * @see #dbUtils
   */
  public void setDbUtils(DbUtils dbUtils) {
    this.dbUtils = dbUtils;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * Sends notification email that layout was removed.
   *
   * @param projectId
   *          identifier of the project
   * @param layoutName
   *          name of the layout
   * @param email
   *          notification email
   * @throws MessagingException
   *           thrown when there is a problem with sending email
   */
  protected void sendSuccesfullRemoveEmail(String projectId, String layoutName, String email)
      throws MessagingException {
    StringBuilder content = new StringBuilder(
        "Layout " + layoutName + " in map " + projectId + " was successfully removed.<br/>");
    emailSender.sendEmail("MapViewer notification", content.toString(), email);
  }

  /**
   * Sends notification email that layout was generated.
   *
   * @param params
   *          list of {@link CreateLayoutParams params} used for layout creation
   * @param schemas
   *          set of schemas used in coloring
   * @throws MessagingException
   *           thrown when there is a problem with sending email
   */
  protected void sendSuccessfullGenerationEmail(final CreateLayoutParams params, final Collection<ColorSchema> schemas)
      throws MessagingException {
    StringBuilder content = new StringBuilder("Layout " + params.getName() + " generated successfully.\n");
    content.append("Result of coloring:<br/>");

    String table = prepareTableResult(schemas, new ColorSchemaReader());
    content.append(table);
    String email = params.getModel().getProject().getNotifyEmail();
    if (params.getUser() != null) { // send email to a user who owns the layout
      emailSender.sendEmail("MapViewer notification", content.toString(), params.getUser());

      // send email to the model owner
      if (email != null && !email.equals(params.getUser().getEmail())) {
        content = new StringBuilder("");
        String username = "UNKNOWN";
        if (params.getUser() != null) {
          username = params.getUser().getLogin();
        }
        content.append(
            "User " + username + " created layout in " + params.getModel().getProject().getProjectId() + " map.");

        emailSender.sendEmail("MapViewer notification", content.toString(), email);
      }
    } else if (email != null) {
      // if nobody owns the layout then send it to the model owner
      emailSender.sendEmail("MapViewer notification", content.toString(), email);
    }
  }

  /**
   * Prepares table with statistics about coloring.
   *
   * @param schemas
   *          schemas that were used for coloring
   * @param scr
   *          interface that returns list of columns that should be printed
   * @return table with statistics about coloring
   */
  protected String prepareTableResult(Collection<ColorSchema> schemas, ColorSchemaReader scr) {
    StringBuilder sb = new StringBuilder("");

    Collection<ColorSchemaColumn> columns = scr.getSetColorSchemaColumns(schemas);

    for (ColorSchemaColumn column : ColorSchemaColumn.values()) {
      if (columns.contains(column)) {
        sb.append(column.getTitle() + "\t");
      }
    }
    sb.append("matches<br/>\n");

    for (ColorSchema originalSchema : schemas) {
      for (ColorSchema schema : splitColorSchema(originalSchema)) {
        sb.append(prepareTableRow(columns, schema));
      }
    }

    return sb.toString();
  }

  /**
   * {@link ColorSchema} sometimes contains merged value from few rows. This
   * method split single {@link ColorSchema} object to simple flat objects that
   * can be serialiazed in a simple tab separated file.
   *
   * @param originalSchema
   *          original {@link ColorSchema} objcet that we want to spli into flat
   *          objects
   * @return {@link List} of flat schema objects obtained from input
   */
  private List<ColorSchema> splitColorSchema(ColorSchema originalSchema) {
    List<ColorSchema> result = new ArrayList<>();
    if (originalSchema instanceof GenericColorSchema) {
      result.add(originalSchema);
    } else if (originalSchema instanceof GeneVariationColorSchema) {
      for (GeneVariation gv : ((GeneVariationColorSchema) originalSchema).getGeneVariations()) {
        GeneVariationColorSchema copy = (GeneVariationColorSchema) originalSchema.copy();
        copy.getGeneVariations().clear();
        copy.addGeneVariation(gv.copy());
        result.add(copy);
      }
    } else {
      throw new InvalidArgumentException("Unknown class type: " + originalSchema.getClass());
    }
    return result;
  }

  /**
   * Prepares tab separated {@link String} represenation of {@link ColorSchema}.
   *
   * @param columns
   *          columns that should be outputed in the result String
   * @param schema
   *          input ColorSchema that is going to be transformed into String
   *          representation
   * @return tab separated {@link String} represenation of {@link ColorSchema}
   */
  protected String prepareTableRow(Collection<ColorSchemaColumn> columns, ColorSchema schema) {
    StringBuilder sb = new StringBuilder();
    for (ColorSchemaColumn column : ColorSchemaColumn.values()) {
      if (columns.contains(column)) {
        if (schema instanceof GenericColorSchema) {
          sb.append(prepareTableCellForGenericSchema((GenericColorSchema) schema, column));
        } else if (schema instanceof GeneVariationColorSchema) {
          sb.append(prepareTableCellForGeneVariationSchema((GeneVariationColorSchema) schema, column));
        } else {
          throw new InvalidArgumentException("Unknown schema type: " + schema.getClass());
        }
      }
    }
    sb.append(schema.getMatches() + "<br/>\n");
    return sb.toString();
  }

  /**
   * Returns String representing data of {@link GenericColorSchema} that should
   * appear in a given {@link ColorSchemaColumn}.
   *
   * @param schema
   *          object for which data will be returned
   * @param column
   *          column for which data should be returned
   * @return {@link String} representing data of {@link GenericColorSchema} that
   *         should appear in a given {@link ColorSchemaColumn}
   */
  protected String prepareTableCellForGenericSchema(GenericColorSchema schema, ColorSchemaColumn column) {
    StringBuilder sb = new StringBuilder();
    if (column.equals(ColorSchemaColumn.COLOR)) {
      if (schema.getColor() != null) {
        sb.append("#" + Integer.toHexString(schema.getColor().getRGB()).substring(2).toUpperCase());
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.NAME)) {
      sb.append(schema.getName() + "\t");
    } else if (column.equals(ColorSchemaColumn.MODEL_NAME)) {
      sb.append(schema.getModelName() + "\t");
    } else if (column.equals(ColorSchemaColumn.VALUE)) {
      sb.append(schema.getValue() + "\t");
    } else if (column.equals(ColorSchemaColumn.COMPARTMENT)) {
      for (String str : schema.getCompartments()) {
        sb.append(str + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.TYPE)) {
      for (Class<? extends Element> str : schema.getTypes()) {
        sb.append(str.getSimpleName() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.IDENTIFIER)) {
      for (MiriamData md : schema.getMiriamData()) {
        sb.append(md.getDataType().getCommonName() + ": " + md.getResource() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.ELEMENT_IDENTIFIER)) {
      sb.append(schema.getElementId() + "\t");
    } else if (column.equals(ColorSchemaColumn.LINE_WIDTH)) {
      sb.append(schema.getLineWidth() + "\t");
    } else if (column.equals(ColorSchemaColumn.REVERSE_REACTION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.POSITION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.DESCRIPTION)) {
      sb.append(schema.getDescription() + "\t");
    } else {
      throw new InvalidArgumentException("Unknown column type: " + column + " for schema type: " + schema.getClass());
    }
    return sb.toString();
  }

  /**
   * Returns String representing data of {@link GeneVariationColorSchema} that
   * should appear in a given {@link ColorSchemaColumn}.
   *
   * @param schema
   *          object for which data will be returned
   * @param column
   *          column for which data should be returned
   * @return {@link String} representing data of {@link GeneVariationColorSchema}
   *         that should appear in a given {@link ColorSchemaColumn}
   */
  protected String prepareTableCellForGeneVariationSchema(GeneVariationColorSchema schema, ColorSchemaColumn column) {
    StringBuilder sb = new StringBuilder();
    if (column.equals(ColorSchemaColumn.COLOR)) {
      if (schema.getColor() != null) {
        sb.append("#" + Integer.toHexString(schema.getColor().getRGB()).substring(2).toUpperCase());
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.NAME)) {
      sb.append(schema.getName() + "\t");
    } else if (column.equals(ColorSchemaColumn.MODEL_NAME)) {
      sb.append(schema.getModelName() + "\t");
    } else if (column.equals(ColorSchemaColumn.VALUE)) {
      sb.append(schema.getValue() + "\t");
    } else if (column.equals(ColorSchemaColumn.COMPARTMENT)) {
      for (String str : schema.getCompartments()) {
        sb.append(str + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.TYPE)) {
      for (Class<? extends Element> str : schema.getTypes()) {
        sb.append(str.getSimpleName() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.IDENTIFIER)) {
      for (MiriamData md : schema.getMiriamData()) {
        sb.append(md.getDataType().getCommonName() + ": " + md.getResource() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.ELEMENT_IDENTIFIER)) {
      sb.append(schema.getElementId() + "\t");
    } else if (column.equals(ColorSchemaColumn.LINE_WIDTH)) {
      sb.append(schema.getLineWidth() + "\t");
    } else if (column.equals(ColorSchemaColumn.REVERSE_REACTION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.POSITION)) {
      sb.append(schema.getGeneVariations().get(0).getPosition() + "\t");
    } else if (column.equals(ColorSchemaColumn.DESCRIPTION)) {
      sb.append(schema.getDescription() + "\t");
    } else if (column.equals(ColorSchemaColumn.ORIGINAL_DNA)) {
      sb.append(schema.getGeneVariations().get(0).getOriginalDna() + "\t");
    } else if (column.equals(ColorSchemaColumn.ALTERNATIVE_DNA)) {
      sb.append(schema.getGeneVariations().get(0).getModifiedDna() + "\t");
    } else if (column.equals(ColorSchemaColumn.REFERENCE_GENOME_TYPE)) {
      sb.append(schema.getGeneVariations().get(0).getReferenceGenomeType() + "\t");
    } else if (column.equals(ColorSchemaColumn.REFERENCE_GENOME_VERSION)) {
      sb.append(schema.getGeneVariations().get(0).getReferenceGenomeVersion() + "\t");
    } else if (column.equals(ColorSchemaColumn.CONTIG)) {
      sb.append(schema.getGeneVariations().get(0).getContig() + "\t");
    } else if (column.equals(ColorSchemaColumn.REFERENCES)) {
      MiriamConnector mc = new MiriamConnector();
      for (MiriamData md : schema.getGeneVariations().get(0).getReferences()) {
        sb.append(mc.miriamDataToUri(md) + "\t");
      }
    } else {
      throw new InvalidArgumentException("Unknown column type: " + column + " for schema type: " + schema.getClass());
    }
    return sb.toString();
  }

  /**
   * Returns byte array containing data from original input file that was used to
   * generate the layout.
   *
   * @param layoutId
   *          identifier of layout for which we want to retrieve original file
   *          data
   * @return original data file for given layout, if such file is not stored in
   *         database (compatibility reasons) then null is returned
   * @throws SecurityException
   */
  private byte[] getInputDataForLayout(int layoutId, String token) throws SecurityException {
    Layout layout = getLayoutById(layoutId, token);
    if (layout == null) {
      return null;
    } else {
      if (layout.getInputData() != null) {
        return layout.getInputData().getFileContent();
      } else {
        return null;
      }
    }
  }

  @Override
  public List<Pair<Element, ColorSchema>> getAliasesForLayout(Model model, int layoutId, String token)
      throws SecurityException {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas = reader.readColorSchema(getInputDataForLayout(layoutId, token));
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));
      List<Pair<Element, ColorSchema>> result = new ArrayList<>();
      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Element) {
          result.add(new Pair<Element, ColorSchema>((Element) entry.getKey(), entry.getValue()));
        }
      }
      result.sort(LayoutService.ELEMENT_PAIR_COMPARATOR);
      return result;
    } catch (InvalidColorSchemaException e) {
      throw new InvalidStateException(e);
    } catch (IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public List<Pair<Reaction, ColorSchema>> getReactionsForLayout(Model model, int layoutId, String token)
      throws SecurityException {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas = reader.readColorSchema(getInputDataForLayout(layoutId, token));
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));
      List<Pair<Reaction, ColorSchema>> result = new ArrayList<>();
      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Reaction) {
          result.add(new Pair<Reaction, ColorSchema>((Reaction) entry.getKey(), entry.getValue()));
        }
      }
      result.sort(LayoutService.ELEMENT_PAIR_COMPARATOR);
      return result;
    } catch (InvalidColorSchemaException e) {
      throw new InvalidStateException(e);
    } catch (IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public Map<Object, ColorSchema> getElementsForLayout(Model model, Integer layoutId, String token)
      throws SecurityException {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas;
      schemas = reader.readColorSchema(getInputDataForLayout(layoutId, token));
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));
      return command.getModifiedElements();
    } catch (InvalidColorSchemaException e) {
      throw new InvalidStateException(e);
    } catch (IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public EmailSender getEmailSender() {
    return emailSender;
  }

  @Override
  public void setEmailSender(EmailSender emailSender) {
    this.emailSender = emailSender;
  }

  @Override
  public List<Layout> getCustomLayouts(Model model, String token, Boolean publicOverlay, User creator)
      throws SecurityException {
    User user = userService.getUserByToken(token);
    List<Layout> result = new ArrayList<>();
    if (model == null || user == null) {
      return result;
    }
    List<Layout> overlays = layoutDao.getLayoutsByModel(model, creator, publicOverlay);
    for (Layout overlay : overlays) {
      if (userCanViewOverlay(overlay, user)) {
        result.add(overlay);
      }
    }
    Collections.sort(result, Layout.ID_COMPARATOR);
    return result;
  }

  @Override
  public Layout getLayoutById(int overlayId, String token) throws SecurityException {
    Layout layout = layoutDao.getById(overlayId);
    if (layout == null) {
      return null;
    }
    User user = userService.getUserByToken(token);
    if (!userCanViewOverlay(layout, user)) {
      throw new SecurityException("User doesn't have access to overlay");
    }
    return layout;
  }

  @Override
  public Pair<Element, ColorSchema> getFullAliasForLayout(Model model, Integer id, int layoutId, String token)
      throws SecurityException {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas;
      schemas = reader.readColorSchema(getInputDataForLayout(layoutId, token));
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));

      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Element) {
          Element alias = (Element) entry.getKey();
          if (id.equals(alias.getId())) {
            return new Pair<Element, ColorSchema>(alias, entry.getValue());
          }
        }
      }
      return null;
    } catch (InvalidColorSchemaException e) {
      throw new InvalidStateException(e);
    } catch (IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public Pair<Reaction, ColorSchema> getFullReactionForLayout(Model model, Integer id, int layoutId, String token)
      throws SecurityException {
    try {
      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas;
      schemas = reader.readColorSchema(getInputDataForLayout(layoutId, token));
      // colors here are not important
      ColorModelCommand command = new ColorModelCommand(model, schemas,
          new ColorExtractor(Color.BLACK, Color.BLACK, Color.BLACK));

      for (Map.Entry<Object, ColorSchema> entry : command.getModifiedElements().entrySet()) {
        if (entry.getKey() instanceof Reaction) {
          Reaction alias = (Reaction) entry.getKey();
          if (id.equals(alias.getId())) {
            return new Pair<Reaction, ColorSchema>(alias, entry.getValue());
          }
        }
      }
      return null;
    } catch (InvalidColorSchemaException e) {
      throw new InvalidStateException(e);
    } catch (IOException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public boolean userCanRemoveLayout(Layout layout, String authenticationToken) throws SecurityException {
    User user = userService.getUserByToken(authenticationToken);
    return userCanRemoveLayout(layout, user);
  }
}
