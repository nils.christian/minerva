package lcsb.mapviewer.services.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.genome.FileNotAvailableException;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnector;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.annotation.services.genome.UcscReferenceGenomeConnector;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import lcsb.mapviewer.services.utils.ReferenceGenomeExistsException;

/**
 * Service managing reference genomes.
 * 
 * @author Piotr Gawron
 *
 */
@Transactional(value = "txManager")
public class ReferenceGenomeService implements IReferenceGenomeService {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ReferenceGenomeService.class);

  /**
   * Class responsible for connection to {@link ReferenceGenomeType#UCSC}
   * database.
   */
  @Autowired
  private UcscReferenceGenomeConnector ucscReferenceGenomeConnector;

  /**
   * Data access object for {@link ReferenceGenome} objects.
   */
  @Autowired
  private ReferenceGenomeDao referenceGenomeDao;

  @Override
  public void addReferenceGenome(ReferenceGenomeType type, MiriamData organism, String version, String customUrl)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    for (ReferenceGenome genome : getDownloadedGenomes()) {
      if (genome.getType().equals(type) && genome.getOrganism().equals(organism)
          && genome.getVersion().equals(version)) {
        throw new ReferenceGenomeExistsException("Selected reference genome already downloaded");
      }
    }
    getReferenceGenomeConnector(type).downloadGenomeVersion(organism, version, new IProgressUpdater() {
      @Override
      public void setProgress(double progress) {
      }
    }, true, customUrl);
  }

  /**
   * Return {@link ReferenceGenomeConnector} implementation for given reference
   * genome type.
   * 
   * @param type
   *          type of reference genome
   * @return {@link ReferenceGenomeConnector} implementation for given reference
   *         genome type
   */
  private ReferenceGenomeConnector getReferenceGenomeConnector(ReferenceGenomeType type) {
    if (type == ReferenceGenomeType.UCSC) {
      return ucscReferenceGenomeConnector;
    } else {
      throw new InvalidArgumentException("Unknown reference genome type: " + type);
    }
  }

  @Override
  public List<MiriamData> getOrganismsByReferenceGenomeType(ReferenceGenomeType type)
      throws ReferenceGenomeConnectorException {
    return getReferenceGenomeConnector(type).getAvailableOrganisms();
  }

  @Override
  public List<String> getAvailableGenomeVersions(ReferenceGenomeType type, MiriamData organism)
      throws ReferenceGenomeConnectorException {
    return getReferenceGenomeConnector(type).getAvailableGenomeVersion(organism);
  }

  @Override
  public String getUrlForGenomeVersion(ReferenceGenomeType type, MiriamData organism, String version) {
    try {
      return getReferenceGenomeConnector(type).getGenomeVersionFile(organism, version);
    } catch (FileNotAvailableException e) {
      return null;
    }
  }

  @Override
  public List<ReferenceGenome> getDownloadedGenomes() {
    return referenceGenomeDao.getAll();
  }

  @Override
  public void removeGenome(ReferenceGenome genome) throws IOException {
    getReferenceGenomeConnector(genome.getType()).removeGenomeVersion(genome.getOrganism(), genome.getVersion());
  }

  @Override
  public void addReferenceGenomeGeneMapping(ReferenceGenome referenceGenome, String name, String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    getReferenceGenomeConnector(referenceGenome.getType()).downloadGeneMappingGenomeVersion(referenceGenome, name,
        new IProgressUpdater() {
          @Override
          public void setProgress(double progress) {
          }
        }, true, url);

  }

  @Override
  public void removeReferenceGenomeGeneMapping(ReferenceGenomeGeneMapping genome) throws IOException {
    getReferenceGenomeConnector(genome.getReferenceGenome().getType()).removeGeneMapping(genome);
  }

  @Override
  public ReferenceGenome getReferenceGenomeViewByParams(MiriamData miriamData, ReferenceGenomeType genomeType,
      String version, String authenticationToken) {
    List<ReferenceGenome> list = referenceGenomeDao.getByType(genomeType);
    for (ReferenceGenome referenceGenome : list) {
      if (referenceGenome.getOrganism().equals(miriamData) && referenceGenome.getVersion().equals(version)) {
        return referenceGenome;
      }
    }
    return null;
  }

  @Override
  public ReferenceGenome getReferenceGenomeById(int id, String authenticationToken) {
    return referenceGenomeDao.getById(id);
  }

}
