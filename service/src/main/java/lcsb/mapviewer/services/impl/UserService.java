package lcsb.mapviewer.services.impl;

import java.awt.Color;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.unboundid.ldap.sdk.LDAPException;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.ObjectUtils;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.log.LogType;
import lcsb.mapviewer.model.user.BasicPrivilege;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ObjectPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.PrivilegeDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.UserDTO;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILdapService;
import lcsb.mapviewer.services.interfaces.ILogService;
import lcsb.mapviewer.services.interfaces.ILogService.LogParams;
import lcsb.mapviewer.services.interfaces.IUserService;

/**
 * Implementation of the service that manages users.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional(value = "txManager")
public class UserService implements IUserService {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(UserService.class);

  /**
   * Data access object for users.
   */
  @Autowired
  private UserDao userDao;

  /**
   * Data access object for privileges.
   */
  @Autowired
  private PrivilegeDao privilegeDao;

  @Autowired
  private SessionRegistry sessionRegistry;

  /**
   * Service that provides password encoding.
   */
  @Autowired
  private PasswordEncoder passwordEncoder;

  /**
   * Service used for logging.
   */
  @Autowired
  private ILogService logService;

  @Autowired
  private ILdapService ldapService;

  @Autowired
  private ProjectDao projectDao;

  /**
   * Service used for accessing configuration parameters.
   */
  @Autowired
  private IConfigurationService configurationService;

  @Override
  public String login(String login, String password) {
    Random random = new SecureRandom();
    String id = new BigInteger(130, random).toString(32);
    return this.login(login, password, id);
  }

  @Override
  public boolean userHasPrivilege(User user, PrivilegeType type) {
    return getUserPrivilegeLevel(user, type) > 0;
  }

  @Override
  public boolean userHasPrivilege(User user, PrivilegeType type, Object object) {
    return getUserPrivilegeLevel(user, type, object) > 0;
  }

  @Override
  public void setUserPrivilege(User user, BasicPrivilege privilege) {
    updateUserPrivilegesWithoutDbModification(user, privilege);
    updateUser(user);
    userDao.flush();
  }

  private void updateUserPrivilegesWithoutDbModification(User user, BasicPrivilege privilege) {
    BasicPrivilege oldPrivilege = null;
    for (BasicPrivilege privilegeIter : user.getPrivileges()) {
      if (privilegeIter.equalsPrivilege(privilege)) {
        oldPrivilege = privilegeIter;
      }
    }
    if (oldPrivilege != null) {
      privilege.setUser(null);
      oldPrivilege.setLevel(privilege.getLevel());
    } else {
      privilege.setUser(user);
      user.getPrivileges().add(privilege);
    }
  }

  @Override
  public void addUser(User user) {
    userDao.add(user);
    LogParams params = new LogParams().description("User " + user.getLogin() + " created.").type(LogType.USER_CREATED)
        .object(user);
    logService.log(params);
  }

  @Override
  public void updateUser(User user) {
    userDao.update(user);
  }

  @Override
  public void deleteUser(User user) {
    userDao.delete(user);
    LogParams params = new LogParams().description("User " + user.getLogin() + " removed.").type(LogType.USER_CREATED)
        .object(user);
    logService.log(params);
  }

  @Override
  public User getUserByLogin(String login) {
    User result = userDao.getUserByLogin(login);
    if (result != null) {
      userDao.refresh(result);
    }
    return result;
  }

  @Override
  public void dropPrivilegesForObjectType(PrivilegeType type, int id) {
    IntegerComparator integerComparator = new IntegerComparator();
    // this will be slow when number of user will increase (we fetch all
    // users and drop privileges one by one)
    List<User> users = userDao.getAll();
    for (User user : users) {
      List<BasicPrivilege> toRemove = new ArrayList<>();
      for (BasicPrivilege privilege : user.getPrivileges()) {
        if (privilege.getType().equals(type) && privilege instanceof ObjectPrivilege
            && integerComparator.compare(((ObjectPrivilege) privilege).getIdObject(), id) == 0) {
          toRemove.add(privilege);
        }
      }
      if (toRemove.size() > 0) {
        user.getPrivileges().removeAll(toRemove);
        userDao.update(user);
      }
    }
  }

  @Override
  public int getUserPrivilegeLevel(User user, PrivilegeType type) {
    if (type.getPrivilegeClassType() != BasicPrivilege.class) {
      throw new InvalidArgumentException("This privilege requires additional information");
    }
    for (BasicPrivilege privilege : user.getPrivileges()) {
      if (privilege.getType().equals(type)) {
        return privilege.getLevel();
      }
    }
    return 0;
  }

  @Override
  public int getUserPrivilegeLevel(User user, PrivilegeType type, Object object) {
    Integer id = null;
    if (object != null) {
      try {
        id = ObjectUtils.getIdOfObject(object);
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
        throw new InvalidArgumentException("Internal server error. Problem with accessing id of the parameter object");
      }
      if (!type.getPrivilegeObjectType().isAssignableFrom(object.getClass())) {
        throw new InvalidArgumentException("This privilege accept only " + type.getPrivilegeObjectType()
            + " objects parameter, but " + object.getClass() + " class found.");
      }
    }
    return getUserPrivilegeLevel(user, type, id);
  }

  private int getUserPrivilegeLevel(User user, PrivilegeType type, Integer id) {
    if (type.getPrivilegeClassType() != ObjectPrivilege.class) {
      throw new InvalidArgumentException("This privilege doesn't accept object parameter");
    }
    if (user == null) {
      throw new InvalidArgumentException("User cannot be null");
    }

    // refresh user from db
    if (user.getId() != null) {
      user = userDao.getById(user.getId());
    }
    IntegerComparator integerComparator = new IntegerComparator();
    for (BasicPrivilege privilege : user.getPrivileges()) {
      if (privilege.getClass() == ObjectPrivilege.class) {
        ObjectPrivilege oPrivilege = (ObjectPrivilege) privilege;
        if (oPrivilege.getType().equals(type) && integerComparator.compare(oPrivilege.getIdObject(), id) == 0) {
          return privilege.getLevel();
        }
      }
    }
    return -1;
  }

  @Override
  public void setUserPrivilege(User user, PrivilegeType type, Integer value) {
    BasicPrivilege privilege = new BasicPrivilege(value, type, user);

    BasicPrivilege oldPrivilege = null;
    for (BasicPrivilege privilegeIter : user.getPrivileges()) {
      if (privilegeIter.getType().equals(type)) {
        oldPrivilege = privilegeIter;
      }
    }
    if (oldPrivilege != null) {
      user.getPrivileges().remove(oldPrivilege);
      oldPrivilege.setUser(null);
    }
    user.getPrivileges().add(privilege);
    updateUser(user);
    userDao.flush();

  }

  /**
   * @return the userDao
   * @see #userDao
   */
  public UserDao getUserDao() {
    return userDao;
  }

  /**
   * @param userDao
   *          the userDao to set
   * @see #userDao
   */
  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  /**
   * @return the privilegeDao
   * @see #privilegeDao
   */
  public PrivilegeDao getPrivilegeDao() {
    return privilegeDao;
  }

  /**
   * @param privilegeDao
   *          the privilegeDao to set
   * @see #privilegeDao
   */
  public void setPrivilegeDao(PrivilegeDao privilegeDao) {
    this.privilegeDao = privilegeDao;
  }

  /**
   * @return the passwordEncoder
   * @see #passwordEncoder
   */
  public PasswordEncoder getPasswordEncoder() {
    return passwordEncoder;
  }

  /**
   * @param passwordEncoder
   *          the passwordEncoder to set
   * @see #passwordEncoder
   */
  public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * @param password
   *          input password
   * @return encoded password
   */
  @Override
  public String encodePassword(String password) {
    return passwordEncoder.encode(password);
  }

  @Override
  public ColorExtractor getColorExtractorForUser(User loggedUser) {
    Color colorMin = null;
    Color colorMax = null;
    Color colorSimple = null;
    if (loggedUser != null) {
      User dbUser = getUserByLogin(loggedUser.getLogin());
      if (dbUser != null) {
        colorMin = dbUser.getMinColor();
        colorMax = dbUser.getMaxColor();
        colorSimple = dbUser.getSimpleColor();
      }
    }
    ColorParser parser = new ColorParser();

    if (colorMin == null) {
      colorMin = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.MIN_COLOR_VAL));
    }
    if (colorMax == null) {
      colorMax = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.MAX_COLOR_VAL));
    }
    if (colorSimple == null) {
      colorSimple = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.SIMPLE_COLOR_VAL));
    }
    return new ColorExtractor(colorMin, colorMax, colorSimple);
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @Override
  public User getUserByToken(String token) throws SecurityException {
    if (!isSessionExpired(token)) {
      String login = ((org.springframework.security.core.userdetails.User) (sessionRegistry.getSessionInformation(token)
          .getPrincipal())).getUsername();
      return userDao.getUserByLogin(login);
    } else {
      throw new SecurityException("Invalid token");
    }
  }

  private boolean isSessionExpired(String token) {
    SessionInformation sessionData = sessionRegistry.getSessionInformation(token);
    if (sessionData == null) {
      logger.debug("No session data for token id: " + token);
      return true;
    }
    return sessionData.isExpired();
  }

  @Override
  public boolean userHasPrivilege(String token, PrivilegeType type, Object object) throws SecurityException {
    return userHasPrivilege(getUserByToken(token), type, object);
  }

  @Override
  public void logout(String tokenString) {
    if (!isSessionExpired(tokenString)) {
      sessionRegistry.removeSessionInformation(tokenString);
    }
  }

  @Override
  public List<User> getUsers(String token) throws SecurityException {
    if (userHasPrivilege(token, PrivilegeType.USER_MANAGEMENT)) {
      return userDao.getAll();
    } else {
      throw new SecurityException("You have no access to users data");
    }
  }

  @Override
  public void setUserPrivilege(User user, PrivilegeType type, Object value, String token) throws SecurityException {
    if (!userHasPrivilege(token, PrivilegeType.USER_MANAGEMENT)) {
      throw new SecurityException("You cannot modify user privileges");
    }
    if (value instanceof Integer) {
      setUserPrivilege(user, type, (Integer) value);
    } else if (value instanceof Boolean) {
      if ((Boolean) value) {
        setUserPrivilege(user, type, 1);
      } else {
        setUserPrivilege(user, type, 0);
      }
    } else {
      throw new InvalidArgumentException("Invalid privilege value: " + value);
    }
  }

  @Override
  public void setUserPrivilege(User user, PrivilegeType type, Object value, Integer objectId, String token)
      throws SecurityException {
    boolean canModify = userHasPrivilege(token, PrivilegeType.USER_MANAGEMENT);
    if (!canModify) {
      if (type.getPrivilegeObjectType().isAssignableFrom(Project.class)) {
        canModify = getUserPrivilegeLevel(getUserByToken(token), type, objectId) > 0;
      }
    }
    if (!canModify) {
      throw new SecurityException("You cannot modify user privileges");
    }
    Project projectIdWrapper = new Project();
    if (objectId == null) {
      projectIdWrapper = null;
    } else {
      projectIdWrapper.setId(objectId);
    }
    if (value instanceof Integer) {
      setUserPrivilege(user, new ObjectPrivilege(projectIdWrapper, (Integer) value, type, user));
    } else if (value instanceof Boolean) {
      if ((Boolean) value) {
        setUserPrivilege(user, new ObjectPrivilege(projectIdWrapper, 1, type, user));
      } else {
        setUserPrivilege(user, new ObjectPrivilege(projectIdWrapper, 0, type, user));
      }
    } else {
      throw new InvalidArgumentException("Invalid privilege value: " + value);
    }

  }

  @Override
  public void updateUser(User modifiedUser, String token) throws SecurityException {
    User user = getUserByToken(token);
    if (user.getLogin().equals(modifiedUser.getLogin()) || userHasPrivilege(token, PrivilegeType.USER_MANAGEMENT)) {
      updateUser(modifiedUser);
    } else {
      throw new SecurityException("You cannot modify user");
    }

  }

  @Override
  public boolean userHasPrivilege(String token, PrivilegeType type) throws SecurityException {
    return userHasPrivilege(getUserByToken(token), type);
  }

  @Override
  public String login(String login, String password, String token) {
    User user = userDao.getUserByLogin(login);
    if (user == null) {
      user = createUserFromLdap(login, password);
    } else if (!user.isConnectedToLdap()) {
      String cryptedPassword;
      if (password != null) {
        cryptedPassword = passwordEncoder.encode(password);
      } else {
        cryptedPassword = "";
      }
      user = userDao.getUserByLoginAndCryptedPassword(login, cryptedPassword);
    } else {
      if (!authenticateOverLdap(login, password)) {
        user = null;
      }
    }

    if (user == null && Configuration.ANONYMOUS_LOGIN.equals(login) && "".equals(password)) {
      user = getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    }
    if (user != null) {
      sessionRegistry.registerNewSession(token, new org.springframework.security.core.userdetails.User(login,
          passwordEncoder.encode(password), AuthorityUtils.commaSeparatedStringToAuthorityList("")));
      return token;
    } else {
      return null;
    }
  }

  private boolean authenticateOverLdap(String login, String password) {
    if (!ldapService.isValidConfiguration()) {
      return false;
    }
    try {
      return ldapService.login(login, password);
    } catch (LDAPException e) {
      logger.warn("Problem with accessing LDAP directory", e);
      return false;
    }
  }

  private User createUserFromLdap(String login, String password) {
    if (login == null || password == null) {
      return null;
    }
    if (!ldapService.isValidConfiguration()) {
      return null;
    }
    try {
      User user = null;
      boolean authenticatedOverLdap = ldapService.login(login, password);
      if (authenticatedOverLdap) {
        UserDTO ldapUserData = ldapService.getUserByLogin(login);
        user = new User();
        user.setLogin(login);
        user.setCryptedPassword(passwordEncoder.encode(password));
        user.setName(ldapUserData.getFirstName());
        user.setSurname(ldapUserData.getLastName());
        user.setEmail(ldapUserData.getEmail());
        user.setConnectedToLdap(true);
        addUser(user);
        for (Project project : projectDao.getAll()) {
          createDefaultProjectPrivilegesForUser(project, user);
        }
      }
      return user;
    } catch (LDAPException e) {
      logger.warn("Problem with accessing LDAP directory", e);
      return null;
    }
  }

  @Override
  public ILdapService getLdapService() {
    return ldapService;
  }

  @Override
  public void setLdapService(ILdapService ldapService) {
    this.ldapService = ldapService;
  }

  @Override
  public void createDefaultProjectPrivilegesForUser(Project project, User user) {
    for (PrivilegeType type : PrivilegeType.values()) {
      if (Project.class.equals(type.getPrivilegeObjectType())) {
        int level = getUserPrivilegeLevel(user, type, (Integer) null);
        if (level < 0) {
          if (configurationService.getValue(type).getValue().equalsIgnoreCase("true")) {
            level = 1;
          } else {
            level = 0;
          }
        }
        ObjectPrivilege privilege = new ObjectPrivilege(project, level, type, user);
        setUserPrivilege(user, privilege);
      }
    }
  }

  @Override
  public Map<String, Boolean> ldapAccountExistsForLogin(Collection<User> logins) {
    Map<String, Boolean> result = new HashMap<>();
    for (User user : logins) {
      result.put(user.getLogin(), false);
    }

    if (ldapService.isValidConfiguration()) {
      try {
        List<String> ldapUserNames = getLdapService().getUsernames();
        for (String string : ldapUserNames) {
          if (result.keySet().contains(string)) {
            result.put(string, true);
          }
        }
      } catch (LDAPException e) {
        logger.error("Problem with accessing LDAP directory", e);
      }
    }
    return result;
  }

}
