package lcsb.mapviewer.services.overlay;

import java.io.Serializable;

/**
 * This object is used for representing row of object in chebi ontology in the
 * client side Primefaces p:treeTable object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ChebiTreeRow implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	
	/**
	 * Name of the chebi entity.
	 */
	private String	name;
	
	/**
	 * Identifier of chebi entity.
	 */
	private String	id;
	
	/**
	 * Link to the chebi entity in chebi webpage.
	 */
	private String	link;
	
	/**
	 * Relation associated with this entity.
	 */
	private String	relationType;

	/**
	 * Default constructor.
	 * 
	 * @param name
	 *          Name of the chebi entity
	 * @param id
	 *          Identifier of chebi entity
	 * @param link
	 *          Link to the chebi entity in chebi webpage
	 * @param relationType
	 *          Relation associated with this entity
	 */
	public ChebiTreeRow(String name, String id, String link, String relationType) {
		this.name = name;
		this.id = id;
		this.link = link;
		this.relationType = relationType;
	}

	/**
	 * @return the name
	 * @see #name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 * @see #name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the id
	 * @see #id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the link
	 * @see #link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *          the link to set
	 * @see #link
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the relationType
	 * @see #relationType
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * @param relationType
	 *          the relationType to set
	 * @see #relationType
	 */
	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

}
