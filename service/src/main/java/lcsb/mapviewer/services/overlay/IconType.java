package lcsb.mapviewer.services.overlay;

/**
 * Type of the icon that is returned to the client.
 * 
 * @author Piotr Gawron
 *
 */
public enum IconType {

	/**
	 * Search result.
	 */
	SEARCH("marker", 0),

	/**
	 * Result of the drug search.
	 */
	DRUG("drug", 5),
	/**
	 * Result of the drug search.
	 */
	CHEMICAL("ball", 1),
	/**
	 * Result of the drug search.
	 */
	MI_RNA("target", 2);

	/**
	 * Name of the icons in filesystem.
	 */
	private String	iconFileSystemName;

	/**
	 * Color set starting index from which generasted icons should start for given
	 * type.
	 */
	private int			startingColorSet;

	/**
	 * Default constructor.
	 * 
	 * @param name
	 *          {@link #iconFileSystemName}
	 * @param startColorSet
	 *          {@link #startingColorSet}
	 */
	IconType(String name, int startColorSet) {
		this.iconFileSystemName = name;
		this.startingColorSet = startColorSet;
	}

	/**
	 * @return the iconFileSystemName
	 * @see #iconFileSystemName
	 */
	public String getIconFileSystemName() {
		return iconFileSystemName;
	}

	/**
	 * @return the startingColorSet
	 * @see #startingColorSet
	 */
	public int getStartingColorSet() {
		return startingColorSet;
	}
}
