package lcsb.mapviewer.services.overlay;

import org.apache.commons.lang3.mutable.MutableInt;

/**
 * This class return links to icons used by the client. It might be good idea to
 * move part of the functionality to client side as server shouldn't decide
 * about representation layer.
 * 
 * @author Piotr Gawron
 * 
 */
public final class IconManager {
	/**
	 * Max index of the image.
	 */
	private static final int		MAX_INDEX	= 99;

	/**
	 * Colors available for bubble icons.
	 */
	private String[]						colors		= { "red", "blue", "green", "purple", "yellow", "pink", "paleblue", "brown", "orange" };

	/**
	 * Singelton.
	 */
	private static IconManager	manager;

	/**
	 * Default singleton constructor.
	 */
	private IconManager() {

	}

	/**
	 * Returns only available instance of this class.
	 * 
	 * @return singletone instance
	 */
	public static IconManager getInstance() {
		if (manager == null) {
			manager = new IconManager();
		}
		return manager;
	}

	/**
	 * Returns path to the search icon with the given index.
	 * 
	 * @param index
	 *          identifier of the icon
	 * @param type
	 *          type of the icon to be generated
	 * @param colorSet
	 *          number of the color set that should be used in the generasted icon
	 *          (number is influenced by {@link IconType#startingColorSet}
	 * 
	 * @return path to the icon
	 */
	public String getIconForIndex(MutableInt index, IconType type, int colorSet) {
		return getIconForIndex(index.toInteger(), type, colorSet);
	}

	/**
	 * Returns path to the search icon with the given index.
	 * 
	 * @param index
	 *          identifier of the icon
	 * @param type
	 *          type of the icon to be generated
	 * @param colorSet
	 *          number of the color set that should be used in the generasted icon
	 *          (number is influenced by {@link IconType#startingColorSet}
	 * 
	 * @return path to the icon
	 */
	public String getIconForIndex(int index, IconType type, int colorSet) {
		String color = normalizeColor(colorSet + type.getStartingColorSet());
		index = normalizeIndex(index);
		String result = "marker/" + type.getIconFileSystemName() + "/" + type.getIconFileSystemName() + "_" + color + "_" + index + ".png";
		return result;
	}

	/**
	 * Returns index of the icon that is not out of the range.
	 * 
	 * @param index
	 *          original index
	 * @return index of the icon that is not out of the range
	 */
	public int normalizeIndex(int index) {
		return (index % MAX_INDEX) + 1;
	}

	/**
	 * Returns color set of the icon that is not out of the range.
	 * 
	 * @param set
	 *          original index of the color set
	 * @return index of the color set that is not out of the range
	 */
	private String normalizeColor(int set) {
		return colors[Math.abs(set % colors.length)];
	}

	/**
	 * Returns path to empty icon.
	 * 
	 * @return path to empty icon
	 */
	public String getEmpyIcon() {
		return "marker/empty.png";
	}

	/**
	 * Returns path to delete icon.
	 * 
	 * @return path to delete icon
	 */
	public String getDeleteIcon() {
		return "icons/delete.png";
	}

	/**
	 * Returns path to icon that indicate similarity search.
	 * 
	 * @return path to icon that indicate similarity search
	 */
	public String getSimilarIcon() {
		return "icons/gunsight.png";
	}

	/**
	 * Returns path to comment icon.
	 * 
	 * @return path to comment icon
	 */
	public String getCommentIcon() {
		return "icons/comment.png";
	}
}
