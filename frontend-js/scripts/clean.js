var del = require('del');
var mkdirp = require('mkdirp');


del(["dist/"]).then(function () {
    return mkdirp("dist");
});
