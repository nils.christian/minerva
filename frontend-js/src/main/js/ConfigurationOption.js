"use strict";

var ObjectWithListeners = require('./ObjectWithListeners');

var logger = require('./logger');

function ConfigurationOption(data) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  if (data instanceof ConfigurationOption) {
    self.setType(data.getType());
    self.setCommonName(data.getCommonName());
    self.setValue(data.getValue());
    self.setGroup(data.getGroup());
    self.setValueType(data.getValueType());
  } else {
    self.setType(data.type);
    self.setCommonName(data.commonName);
    self.setGroup(data.group);
    self.setValue(data.value);
    self.setValueType(data.valueType);
  }
}

ConfigurationOption.prototype = Object.create(ObjectWithListeners.prototype);
ConfigurationOption.prototype.constructor = ConfigurationOption;

ConfigurationOption.prototype.setType = function (type) {
  this._type = type;
};

ConfigurationOption.prototype.getType = function () {
  return this._type;
};

ConfigurationOption.prototype.setValue = function (value) {
  this._value = value;
};

ConfigurationOption.prototype.getValue = function () {
  return this._value;
};

ConfigurationOption.prototype.setValueType = function (valueType) {
  this._valueType = valueType;
};

ConfigurationOption.prototype.getValueType = function () {
  return this._valueType;
};

ConfigurationOption.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

ConfigurationOption.prototype.getCommonName = function () {
  return this._commonName;
};

ConfigurationOption.prototype.setGroup = function (group) {
  this._group = group;
};

ConfigurationOption.prototype.getGroup = function () {
  return this._group;
};


module.exports = ConfigurationOption;
