"use strict";

/* exported logger */

var Promise = require("bluebird");

var CustomMapOptions = require('./map/CustomMapOptions');
var GuiUtils = require('./gui/leftPanel/GuiUtils');
var ObjectWithListeners = require('./ObjectWithListeners');

var ConfigurationAdminPanel = require('./gui/admin/ConfigurationAdminPanel');
var GenomeAdminPanel = require('./gui/admin/GenomeAdminPanel');
var MapsAdminPanel = require('./gui/admin/MapsAdminPanel');
var UsersAdminPanel = require('./gui/admin/UsersAdminPanel');

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');
var Functions = require('./Functions');

/**
 * Default constructor.
 *
 * @param {CustomMapOptions} options
 *           object representing all parameters needed for map creation
 */
function Admin(options) {
  var self = this;
  self._panels = [];
  self._tabIdCount = 0;
  if (!(options instanceof CustomMapOptions)) {
    options = new CustomMapOptions(options);
  }
  self.setProject(options.getProject());
  self.setElement(options.getElement());

  self.setConfiguration(options.getConfiguration());
  self.setServerConnector(options.getServerConnector());
  self.setGuiUtils(new GuiUtils());
  self._createGui();
}

Admin.prototype = Object.create(ObjectWithListeners.prototype);
Admin.prototype.constructor = ObjectWithListeners;

Admin.prototype._createGui = function () {
  var self = this;
  self.getElement().innerHTML = "";

  var panels = [{
    name: "PROJECTS",
    panelClass: MapsAdminPanel
  }, {
    name: "USERS",
    panelClass: UsersAdminPanel
  }, {
    name: "CONFIGURATION",
    panelClass: ConfigurationAdminPanel
  }, {
    name: "GENOMES",
    panelClass: GenomeAdminPanel
  }];

  var tabDiv = Functions.createElement({
    type: "div",
    name: "tabView",
    className: "tabbable boxed parentTabs"
  });
  self.getElement().appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type: "div",
    className: "tab-content"
  });
  tabDiv.appendChild(tabContentDiv);

  for (var i = 0; i < panels.length; i++) {
    self.addTab(panels[i], tabMenuDiv, tabContentDiv);
  }
  self.addLogoutButton(tabMenuDiv);
};

Admin.prototype.addLogoutButton = function (navElement) {
  var self = this;
  var logoutLink = self.getGuiUtils().createLogoutLink();

  var navLi = document.createElement("li");
  navLi.appendChild(logoutLink);
  navLi.style.cssFloat = "right";

  navElement.appendChild(navLi);
};

Admin.prototype.addTab = function (params, navElement, contentElement) {
  var self = this;

  var tabId = "admin_panel_tab_" + this._tabIdCount;
  self._tabIdCount++;

  var navLi = self.getGuiUtils().createTabMenuObject({
    id: tabId,
    name: params.name,
    navigationBar: navElement
  });
  navElement.appendChild(navLi);

  var contentDiv = self.getGuiUtils().createTabContentObject({
    id: tabId,
    navigationObject: navLi
  });

  contentElement.appendChild(contentDiv);

  this._panels.push(new params.panelClass({
    element: contentDiv,
    name: params.name,
    project: self.getProject(),
    configuration: self.getConfiguration(),
    serverConnector: self.getServerConnector()
  }));
};

Admin.prototype.setProject = function (project) {
  this._project = project;
};
Admin.prototype.getProject = function () {
  return this._project;
};

Admin.prototype.setElement = function (element) {
  this._element = element;
};
Admin.prototype.getElement = function () {
  return this._element;
};

Admin.prototype.init = function () {
  var promises = [];
  for (var i = 0; i < this._panels.length; i++) {
    promises.push(this._panels[i].init());
  }
  return Promise.all(promises).then(function () {
    $(window).trigger('resize');
  });
};

Admin.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

Admin.prototype.getConfiguration = function () {
  return this._configuration;
};

Admin.prototype.setGuiUtils = function (guiUtils) {
  this._guiUtils = guiUtils;
};

Admin.prototype.getGuiUtils = function () {
  return this._guiUtils;
};

Admin.prototype.destroy = function () {
  var self = this;
  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].destroy());
  }
  return Promise.all(promises);
};

/**
 *
 * @param {ServerConnector} serverConnector
 */
Admin.prototype.setServerConnector = function (serverConnector) {
  this._serverConnector = serverConnector;
};

/**
 *
 * @returns {ServerConnector}
 */
Admin.prototype.getServerConnector = function () {
  return this._serverConnector;
};


module.exports = Admin;
