"use strict";

var log4js = require('log4js');

/**
 * @typedef {Object} Logger
 * @property {function(string):void} setLevel
 * @property {function(Object):void} warn
 * @property {function(Object):void} info
 * @property {function(Object):void} debug
 * @property {function(Object):void} error
 * @property {function(Object):void} fatal
 */

/**
 * @type Logger
 */
var logger = log4js.getLogger();

module.exports = logger;
