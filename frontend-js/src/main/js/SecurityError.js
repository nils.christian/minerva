"use strict";

/* exported logger */

var logger = require('./logger');

function SecurityError(message) {
  this.message = message;
  this.stack = (new Error()).stack;
}

SecurityError.prototype = Object.create(Error.prototype);
SecurityError.prototype.constructor = SecurityError;

module.exports = SecurityError;
