"use strict";

/* exported logger */

var logger = require('./logger');

function InvalidArgumentError(message) {
  this.message = message;
  this.stack = (new Error()).stack;
}

InvalidArgumentError.prototype = Object.create(Error.prototype);
InvalidArgumentError.prototype.constructor = InvalidArgumentError;

module.exports = InvalidArgumentError;
