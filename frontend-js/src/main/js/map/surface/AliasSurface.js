"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var functions = require('../../Functions');

var AbstractSurfaceElement = require('./AbstractSurfaceElement');
var ConfigurationType = require('../../ConfigurationType');
var IdentifiedElement = require('../data/IdentifiedElement');
var Bounds = require('../canvas/Bounds');
var Point = require('../canvas/Point');

/**
 * Class representing overlay of the alias on the map relevant for a specific
 * layout.
 *
 * @param {LayoutAlias} [params.overlayAlias] - {@link LayoutAlias} for which overlay is created
 * @param {number} [params.startX] - this is the ratio on OX axis that should be use as a
 *          starting point of the overlay. For instance when there are three
 *          overlays to visualize then
 *          <ul>
 *          <li>the first layout have startX=0.0; endX=0.33333</li>
 *          <li>second layout have startX=0.33333; endX=0.66666</li>
 *          <li>the last layout have startX=0.66666; endX=1.0</li>
 *          </ul>
 * @param {number} [params.endX] this is the ratio on OX axis that should be use as a
 *          starting point of the overlay

 * @param {IdentifiedElement} [params.element]
 * @param {Alias} params.alias
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} [params.onClick]
 * @constructor
 * @extends AbstractSurfaceElement
 */
function AliasSurface(params) {
  // call super constructor
  AbstractSurfaceElement.call(this, params);

  this.setOverlayData(params.overlayAlias);
  this.setStartX(params.startX);
  this.setEndX(params.endX);

  this.setColor(params.color);
  this.setFillOpacity(params.opacity);
  this.setStrokeWeight(params.strokeWeight);
  this.setStrokeColor(params.strokeColor);
  this.setStrokeOpacity(params.strokeOpacity);

  // original data
  this.setBioEntity(params.alias);
  this.setIdentifiedElement(new IdentifiedElement(params.alias));
}

AliasSurface.prototype = Object.create(AbstractSurfaceElement.prototype);
AliasSurface.prototype.constructor = AliasSurface;

/**
 *
 * @param {string} color
 */
AliasSurface.prototype.setColor = function (color) {
  if (color === undefined) {
    color = "#FF0000";
  }

  this._color = color;
  var mapCanvasObjects = this.getMapCanvasObjects();
  for (var i = 0; i < mapCanvasObjects.length; i++) {
    mapCanvasObjects[i].setOptions({
      strokeColor: color
    });
  }
};

/**
 *
 * @param {number} opacity
 */
AliasSurface.prototype.setFillOpacity = function (opacity) {
  this._fillOpacity = opacity;
};

/**
 *
 * @returns {number|undefined}
 */
AliasSurface.prototype.getFillOpacity = function () {
  return this._fillOpacity;
};

/**
 *
 * @param {number} weight
 */
AliasSurface.prototype.setStrokeWeight = function (weight) {
  if (weight === undefined) {
    weight = 1;
  }
  this._strokeWeight = weight;
};

/**
 *
 * @returns {number}
 */
AliasSurface.prototype.getStrokeWeight = function () {
  return this._strokeWeight;
};

/**
 *
 * @param {string} color
 */
AliasSurface.prototype.setStrokeColor = function (color) {
  if (color === undefined) {
    color = "#000000";
  }
  this._strokeColor = color;
};

/**
 *
 * @returns {string}
 */
AliasSurface.prototype.getStrokeColor = function () {
  return this._strokeColor;
};

/**
 *
 * @param {number} opacity
 */
AliasSurface.prototype.setStrokeOpacity = function (opacity) {
  if (opacity === undefined) {
    opacity = 1;
  }
  this._strokeOpacity = opacity;
};

AliasSurface.prototype.getStrokeOpacity = function () {
  return this._strokeOpacity;
};

/**
 * Function used to recalculate boundaries of the {@link AliasSurface}.
 * Boundaries define how big part of original alias is taken by this layout
 * visualization.
 *
 * @param {number} startX
 *          value between 0..1 defining where should be the start on OX axis
 * @param {number} endX
 *          value between 0..1 defining where should be the end on OX axis
 */
AliasSurface.prototype.setBoundsForAlias = function (startX, endX) {
  var alias = this.getBioEntity();
  var pointA = new Point(alias.getX() + startX * alias.getWidth(), alias.getY());
  var pointB = new Point(alias.getX() + endX * alias.getWidth(), alias.getY() + alias.getHeight());

  var bounds = new Bounds(pointA, pointB);
  this.getMapCanvasObjects()[0].setBounds(bounds);
};

/**
 *
 * @returns {PromiseLike<any>}
 */
AliasSurface.prototype.init = function () {
  var self = this;
  var overlayData = self.getOverlayData();
  var alias = self.getBioEntity();
  var map = self.getCustomMap();
  var startX = self.getStartX();
  var endX = self.getEndX();

  var pointA = new Point(alias.getX() + startX * alias.getWidth(), alias.getY());
  var pointB = new Point(alias.getX() + endX * alias.getWidth(), alias.getY() + alias.getHeight());

  var bounds = new Bounds(pointA, pointB);
  var fillOpacity;
  return ServerConnector.getConfigurationParam(ConfigurationType.OVERLAY_OPACITY).then(function (result) {
    fillOpacity = self.getFillOpacity();
    if (fillOpacity === undefined) {
      fillOpacity = result;
    }
    if (overlayData !== undefined) {
      return functions.overlayToColor(overlayData);
    } else {
      return "#FF0000";
    }
  }).then(function (color) {
    self.addMapCanvasObject(map.getMapCanvas().createRectangle({
      fillOpacity: fillOpacity,
      strokeColor: self.getStrokeColor(),
      strokeOpacity: self.getStrokeOpacity(),
      strokeWeight: self.getStrokeWeight(),
      fillColor: color,
      bounds: bounds
    }));
  });
};

/**
 *
 * @returns {LayoutAlias}
 */
AliasSurface.prototype.getOverlayData = function () {
  return this._overlayData;
};

/**
 *
 * @param {LayoutAlias} overlayData
 */
AliasSurface.prototype.setOverlayData = function (overlayData) {
  this._overlayData = overlayData;
};

/**
 *
 * @returns {number}
 */
AliasSurface.prototype.getStartX = function () {
  return this._startX;
};

/**
 *
 * @param {number} startX
 */
AliasSurface.prototype.setStartX = function (startX) {
  if (startX === undefined) {
    startX = 0;
  }
  this._startX = startX;
};

/**
 *
 * @returns {number}
 */
AliasSurface.prototype.getEndX = function () {
  return this._endX;
};

/**
 *
 * @param {number} endX
 */
AliasSurface.prototype.setEndX = function (endX) {
  if (endX === undefined) {
    endX = 1;
  }
  this._endX = endX;
};


module.exports = AliasSurface;
