"use strict";

var Promise = require("bluebird");

var AbstractInfoWindow = require('./AbstractInfoWindow');
var IdentifiedElement = require('../data/IdentifiedElement');

/**
 *
 * @param {IdentifiedElement} [params.identifiedElement]
 * @param {AbstractCustomMap} params.map
 * @param {Marker} params.marker
 * @param {PointData} params.point
 *
 * @constructor
 * @extends AbstractInfoWindow
 */
function PointInfoWindow(params) {
  if (params.identifiedElement === undefined) {
    params.identifiedElement = new IdentifiedElement(params.point);
  }
  // call super constructor
  AbstractInfoWindow.call(this, params);

  this.pointData = params.point;

}

PointInfoWindow.prototype = Object.create(AbstractInfoWindow.prototype);
PointInfoWindow.prototype.constructor = PointInfoWindow;

/**
 *
 * @returns {PromiseLike<HTMLElement>}
 */
PointInfoWindow.prototype.createContentDiv = function () {
  var result = document.createElement("div");
  var title = document.createElement("h3");
  title.innerHTML = "Point: " + this.pointData.getPoint();
  result.appendChild(title);

  return Promise.resolve(result);
};

/**
 *
 * @param {Object.<string,boolean>} general
 * @returns {Promise}
 */
PointInfoWindow.prototype.getOverlaysData = function (general) {
  return this.getCustomMap().getTopMap().getOverlayDataForPoint(this.pointData, general);
};

/**
 *
 * @returns {Point}
 */
PointInfoWindow.prototype.getPosition = function () {
  return this.pointData.getPoint();
};


module.exports = PointInfoWindow;
