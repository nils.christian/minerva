"use strict";

var logger = require('../../logger');
var Functions = require('../../Functions');

var AbstractInfoWindow = require('./AbstractInfoWindow');
var Alias = require('../data/Alias');
var IdentifiedElement = require('../data/IdentifiedElement');
var LayoutAlias = require('../data/LayoutAlias');
var Point = require('../canvas/Point');

var pileup = require('pileup');

var Promise = require("bluebird");

/**
 * Class representing info window that should be opened when clicking on alias.
 *
 * @param {IdentifiedElement} [params.identifiedElement]
 * @param {AbstractCustomMap} params.map
 * @param {Marker} params.marker
 * @param {Alias} params.alias
 *
 * @constructor
 * @extends AbstractInfoWindow
 */
function AliasInfoWindow(params) {
  var self = this;

  if (params.identifiedElement === undefined) {
    params.identifiedElement = new IdentifiedElement(params.alias);
  }
  // call super constructor
  AbstractInfoWindow.call(this, params);

  this.setAlias(params.alias);

  var overlayListChanged = function () {
    return self.getCustomMap().getAliasVisibleLayoutsData(self.getAlias().getId()).then(function (layoutAliases) {
      self.layoutAliases = layoutAliases;
      return self.update();
    });
  };

  var dbOverlaySearchChanged = function () {
    return self.update();
  };

  params.map.getTopMap().addListener("onShowOverlay", overlayListChanged);
  params.map.getTopMap().addListener("onHideOverlay", overlayListChanged);

  var drugDbOverlay = params.map.getTopMap().getOverlayByName("drug");
  if (drugDbOverlay !== undefined) {
    drugDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
  var chemicalDbOverlay = params.map.getTopMap().getOverlayByName("chemical");
  if (chemicalDbOverlay !== undefined) {
    chemicalDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
  var mirnaDbOverlay = params.map.getTopMap().getOverlayByName("mirna");
  if (mirnaDbOverlay !== undefined) {
    mirnaDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
}

AliasInfoWindow.prototype = Object.create(AbstractInfoWindow.prototype);
AliasInfoWindow.prototype.constructor = AliasInfoWindow;


/**
 * Updates content of info window. The content will be automatically obtained
 * from {@link CustomMap} instance. The only optional parameter is {@link Alias}
 * data.
 *
 * @returns {Promise}
 */
AliasInfoWindow.prototype.update = function () {
  return AbstractInfoWindow.prototype.update.call(this);
};

/**
 *
 * @returns {PromiseLike<any>}
 */
AliasInfoWindow.prototype.init = function () {
  var self = this;
  var alias = self.getAlias();
  return AbstractInfoWindow.prototype.init.call(self).then(function () {
    return self.getCustomMap().getModel().getAliasById(alias.getId(), true);
  }).then(function (alias) {
    self.setAlias(alias);
    return self.update();
  });
};

/**
 * Creates and returns chart representing data related to alias on different
 * overlays.
 *
 * @returns {PromiseLike<HTMLElement>} html element representing chart with data related to alias
 *          on different overlays
 */
AliasInfoWindow.prototype.createChartDiv = function () {
  var result = document.createElement("div");
  var rows = [];
  var self = this;
  return Promise.each(self.layoutAliases, function (data, i) {
    var rowDiv = document.createElement("div");
    if (i % 2 === 0) {
      rowDiv.className = "mapChartRowEvenDiv";
    } else {
      rowDiv.className = "mapChartRowOddDiv";
    }
    rowDiv.style.position = "relative";
    var nameDiv = document.createElement("div");
    nameDiv.className = "mapChartNameDiv";
    nameDiv.innerHTML = self.layoutNames[i] + "&nbsp;";
    rowDiv.appendChild(nameDiv);

    rows[i] = rowDiv;
    if (data !== undefined && data !== null) {
      return Functions.overlayToColor(data).then(function (color) {
        var value = parseFloat(data.value);
        var description = data.description;
        if (description === null || description === undefined || description === "") {
          description = "";
          if (!isNaN(value)) {
            description = value.toFixed(2);
          }
        }
        var leftMarginDiv = document.createElement("div");
        leftMarginDiv.innerHTML = "&nbsp;";
        leftMarginDiv.style.float = "left";
        var centerBarDiv = document.createElement("div");
        centerBarDiv.style.width = "1px";
        centerBarDiv.style.float = "left";
        centerBarDiv.style.background = "#000000";
        centerBarDiv.innerHTML = "&nbsp;";

        var rightBarDiv = document.createElement("div");
        rightBarDiv.innerHTML = "&nbsp;";
        rightBarDiv.style.float = "left";
        rightBarDiv.style.background = color;
        rightBarDiv.style.width = Math.abs(value * 100) + "px";
        var offset = 100;
        var descDiv = document.createElement("div");
        descDiv.style.float = "right";
        descDiv.style.textAlign = "right";
        descDiv.style.position = "absolute";
        descDiv.style.right = "0";
        descDiv.innerHTML = "<span>" + description + "</span>";
        if (!isNaN(value)) {
          if (value > 0) {
            offset = 100;
            leftMarginDiv.style.width = offset + "px";

            rightBarDiv.style.textAlign = "right";

            rowDiv.appendChild(leftMarginDiv);
            rowDiv.appendChild(centerBarDiv);
            rowDiv.appendChild(rightBarDiv);
          } else {
            offset = 100 + (value * 100);
            leftMarginDiv.style.width = offset + "px";

            rowDiv.appendChild(leftMarginDiv);
            rowDiv.appendChild(rightBarDiv);
            rowDiv.appendChild(centerBarDiv);
          }

        } else {
          offset = 100;
          leftMarginDiv.style.width = offset + "px";
          leftMarginDiv.style.background = color;
          rightBarDiv.style.width = offset + "px";
          rightBarDiv.style.background = color;
          rightBarDiv.style.textAlign = "right";
          rowDiv.appendChild(leftMarginDiv);
          rowDiv.appendChild(centerBarDiv);
          rowDiv.appendChild(rightBarDiv);
        }
        rowDiv.appendChild(descDiv);
      });
    } else {
      var emptyDiv = document.createElement("div");
      emptyDiv.innerHTML = "&nbsp;";
      emptyDiv.style.float = "left";
      emptyDiv.style.width = "201px";
      rowDiv.appendChild(emptyDiv);
      return Promise.resolve();
    }
  }).then(function () {
    for (var i = 0; i < rows.length; i++) {
      result.appendChild(rows[i]);
    }
    return result;
  });
};

/**
 * Methods that creates and return DOM object with the content of the window.
 *
 * @returns {Promise<HTMLElement>| PromiseLike<HTMLElement> } DOM object representing html code for content of the info window
 */
AliasInfoWindow.prototype.createContentDiv = function () {
  var self = this;
  var alias = self.getAlias();
  if (alias.isComplete()) {
    var result = document.createElement("div");
    var title = document.createElement("h3");
    title.innerHTML = alias.getType() + ": " + alias.getName();
    result.appendChild(title);

    var overlayDiv = document.createElement("div");

    result.appendChild(overlayDiv);

    return self.getCustomMap().getAliasVisibleLayoutsData(alias.getId()).then(function (layoutAliases) {
      self.layoutAliases = layoutAliases;
      return self.getCustomMap().getTopMap().getVisibleDataOverlays();
    }).then(function (dataOverlays) {
      self.layoutNames = [];
      for (var i = 0; i < dataOverlays.length; i++) {
        self.layoutNames.push(dataOverlays[i].getName());
      }
      return self.createChartDiv();
    }).then(function (chartDiv) {
      overlayDiv.appendChild(chartDiv);
      return self.createGenomicDiv();
    }).then(function (genomicDiv) {
      overlayDiv.appendChild(genomicDiv);
      return result;
    });
  } else {
    return Promise.resolve(self.createWaitingContentDiv());
  }
};

/**
 * Returns array with data taken from all known {@link AbstractDbOverlay}.
 *
 * @param {Object.<string,boolean>} general
 *          if true then all elements will be returned, if false then only ones
 *          available right now in the overlay
 *
 * @returns {Promise} array with data from {@link AbstractDbOverlay}
 */
AliasInfoWindow.prototype.getOverlaysData = function (general) {
  return this.getCustomMap().getTopMap().getOverlayDataForAlias(this.getAlias(), general);
};

/**
 *
 * @returns {PromiseLike<HTMLElement>}
 */
AliasInfoWindow.prototype.createGenomicDiv = function () {
  var self = this;

  var result = document.createElement("div");
  result.style.width = '640px';

  var titleElement = document.createElement("h3");
  titleElement.innerHTML = "Gene variants";
  result.appendChild(titleElement);

  var contentElement = document.createElement("div");
  result.appendChild(contentElement);

  var geneticInformation = false;
  var genomes = [];
  var genomeUrls = [];

  var pileupSource = [{
    viz: pileup.viz.scale(),
    name: 'Scale'
  }, {
    viz: pileup.viz.location(),
    name: 'Location'
  }];
  var pileupRange = {
    contig: 'chr1',
    start: 3000000000,
    stop: 0
  };

  var globalGeneVariants = [];

  return Promise.each(
    self.layoutAliases,
    function (data) {
      if (data !== null && data !== undefined && data.getType() === LayoutAlias.GENETIC_VARIANT) {
        geneticInformation = true;
        return Promise.each(data.getGeneVariants(), function (variant) {
          return self.getCustomMap().getTopMap().getReferenceGenome(variant.getReferenceGenomeType(),
            variant.getReferenceGenomeVersion()).then(
            function (genome) {
              if (genome.getUrl() !== null && genome.getUrl() !== undefined) {
                if (genomes[genome.getUrl()] === undefined) {
                  genomes[genome.getUrl()] = genome;
                  genomeUrls.push(genome.getUrl());
                }
              } else {
                logger.warn("Genome for " + variant.getReferenceGenomeType() + ","
                  + variant.getReferenceGenomeVersion() + " not loaded");
              }
            },
            function () {
              logger.warn("Genome for " + variant.getReferenceGenomeType() + ","
                + variant.getReferenceGenomeVersion() + " not loaded");

            });
        });

      }
    }).then(function () {
    for (var i = 0; i < genomeUrls.length; i++) {
      var genome = genomes[genomeUrls[i]];
      pileupSource.splice(0, 0, {
        viz: pileup.viz.genome(),
        isReference: pileupSource.length === 2,
        data: pileup.formats.twoBit({
          url: genome.getUrl()
        }),
        name: 'Reference ' + genome.getVersion()
      });
      for (var k = 0; k < genome.getGeneMappings().length; k++) {
        var mapping = genome.getGeneMappings()[k];
        pileupSource.push({
          viz: pileup.viz.genes(),
          data: pileup.formats.bigBed({
            url: mapping.getUrl()
          }),
          name: 'Genes ' + mapping.getName()
        });
      }
    }
    return Promise.each(self.layoutAliases, function (data, i) {
      globalGeneVariants[i] = [];
      if (data !== null && data !== undefined && data.getType() === LayoutAlias.GENETIC_VARIANT) {
        var geneVariants = data.getGeneVariants();
        for (var j = 0; j < geneVariants.length; j++) {
          var variant = geneVariants[j];
          globalGeneVariants[i].push(variant);
          if (variant.getContig() !== undefined) {
            pileupRange.contig = variant.getContig();
          }
          pileupRange.start = Math.min(pileupRange.start, variant.getPosition());
          var length = Math.max(variant.getModifiedDna().length, variant.getOriginalDna().length);
          pileupRange.stop = Math.max(pileupRange.stop, variant.getPosition() + length);
        }
      }
    });
  }).then(function () {
    if (geneticInformation) {
      if (genomeUrls.length === 0) {
        contentElement.innerHTML = "No reference genome data available on minerva platform";
      } else {
        for (var i = 0; i < self.layoutAliases.length; i++) {
          if (globalGeneVariants[i].length > 0) {
            var vcfContent = self.createVcfString(globalGeneVariants[i]);
            pileupSource.push({
              viz: pileup.viz.variants(),
              data: pileup.formats.vcf({
                content: vcfContent
              }),
              name: self.layoutNames[i] + ' - Variants',
              options: {
                variantHeightByFrequency: true
              }
            });
          }
        }

        pileupRange.stop = Math.max(pileupRange.stop, pileupRange.start + 50);

        pileupRange.start--;
        pileupRange.stop++;

        if (self.pileup !== undefined) {
          logger.debug("Destroy pileup");
          self.pileup.destroy();
          logger.debug("Pileup destroyed");
        }
        self.pileup = pileup.create(contentElement, {
          range: pileupRange,
          tracks: pileupSource
        });


        //Code below is a workaround - for some reason on openlayers events bound by pileup are not working properly...
        var zoomControls = $(".zoom-controls", contentElement);

        zoomControls.children().hide();

        var zoomIn = $('<input type="button" value="+" class="btn-zoom-in"/>');
        zoomIn.on("click", function () {
          var range = self.pileup.getRange();
          var distDiff = parseInt((range.stop - range.start + 1) / 4);
          range.start += distDiff;
          range.stop -= distDiff;

          self.pileup.setRange(range);
        });
        zoomIn.appendTo(zoomControls);
        var zoomOut = $('<input type="button" value="-" class="btn-zoom-out"/>');
        zoomOut.on("click", function () {
          var range = self.pileup.getRange();
          var distDiff = parseInt((range.stop - range.start) / 2);
          if (distDiff === 0) {
            distDiff = 1;
          }
          range.start -= distDiff;
          range.stop += distDiff;
          if (range.start < 0) {
            range.stop -= range.start;
            range.start = 0;
          }

          self.pileup.setRange(range);
        });
        zoomOut.appendTo(zoomControls);
      }

      return result;
    } else {
      return document.createElement("div");
    }
  });
};

/**
 *
 * @param {GeneVariant[]} geneVariants
 * @returns {string}
 */
AliasInfoWindow.prototype.createVcfString = function (geneVariants) {
  var result = "";
  result += "##fileformat=VCFv4.0\n";
  result += "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n";
  for (var i = 0; i < geneVariants.length; i++) {
    var variant = geneVariants[i];
    var additionalInfo = "";
    if (variant.getAllelFrequency() !== undefined) {
      additionalInfo = "AF=" + variant.getAllelFrequency();
    }
    var variantId = ".";
    if (variant.getVariantIdentifier() !== undefined) {
      variantId = variant.getVariantIdentifier();
    }
    result += variant.getContig() + "\t" + //
      variant.getPosition() + "\t" + //
      variantId + "\t" + //
      variant.getOriginalDna() + "\t" + //
      variant.getModifiedDna() + "\t" + //
      "100.0\t" + //
      "PASS\t" + //
      additionalInfo + "\n";
  }
  return result;
};

/**
 *
 * @param {Alias} alias
 */
AliasInfoWindow.prototype.setAlias = function (alias) {
  if (alias === undefined || alias === null || (!(alias instanceof Alias))) {
    throw new Error("invalid alias");
  }
  this.alias = alias;
};

/**
 *
 * @returns {Alias}
 */
AliasInfoWindow.prototype.getAlias = function () {
  return this.alias;
};

AliasInfoWindow.prototype.destroy = function () {
  if (this.pileup !== undefined) {
    this.pileup.destroy();
  }
};

/**
 *
 * @returns {Point}
 */
AliasInfoWindow.prototype.getPosition = function () {
  var alias = this.getAlias();
  return new Point(alias.x + alias.width / 2, alias.y + alias.height / 2);
};


module.exports = AliasInfoWindow;
