"use strict";

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var Bounds = require('../Bounds');
var Functions = require('../../../Functions');
var MapCanvas = require('../MapCanvas');
var Point = require('../Point');

var GoogleMapsApiInfoWindow = require('./GoogleMapsApiInfoWindow');
var GoogleMapsApiMarker = require('./GoogleMapsApiMarker');
var GoogleMapsApiPolyline = require('./GoogleMapsApiPolyline');
var GoogleMapsApiRectangle = require('./GoogleMapsApiRectangle');

var Promise = require('bluebird');

/**
 *
 * @param {HTMLElement} element
 * @param {Object} options
 * @param {number} options.tileSize
 * @param {number} options.minZoom
 * @param {number} options.height
 * @param {number} options.width
 * @param {number} options.zoom
 * @param {Point} options.center
 * @param {Object[]} options.backgroundOverlays
 * @constructor
 * @extends MapCanvas
 */

function GoogleMapsApiCanvas(element, options) {
  MapCanvas.call(this, element, options);

  var self = this;
  self.setGoogleMap(new google.maps.Map(element, self.prepareGoogleMapOptions(options)));
  self.setupBackgroundOverlays(options.backgroundOverlays);
  self.addCenterButton();

  google.maps.event.addListener(this.getGoogleMap(), "click", function (e) {
    return self.callListeners("click", {point: self.fromLatLngToPoint(e.latLng)});
  });
  google.maps.event.addListener(this.getGoogleMap(), "rightclick", function (e) {
    return self.callListeners("rightclick", {point: self.fromLatLngToPoint(e.latLng)});
  });
  google.maps.event.addListener(this.getGoogleMap(), "mouseup", function () {
    return self.callListeners("mouseup");
  });
  google.maps.event.addListener(this.getGoogleMap(), "dragstart", function () {
    return self.callListeners("dragstart");
  });
  google.maps.event.addListener(this.getGoogleMap(), "zoom_changed", function () {
    return self.callListeners("zoom_changed");
  });
  google.maps.event.addListener(this.getGoogleMap(), "center_changed", function () {
    return self.callListeners("center_changed");
  });
  google.maps.event.addListener(this.getGoogleMap(), "maptypeid_changed", function () {
    return self.callListeners("maptypeid_changed");
  });

}

GoogleMapsApiCanvas.prototype = Object.create(MapCanvas.prototype);
GoogleMapsApiCanvas.prototype.constructor = GoogleMapsApiCanvas;

/**
 *
 * @param {google.maps.Map} map
 */
GoogleMapsApiCanvas.prototype.setGoogleMap = function (map) {
  this._map = map;
};

/**
 * Returns google map object associated with this canvas.
 * @returns {google.maps.Map}
 */
GoogleMapsApiCanvas.prototype.getGoogleMap = function () {
  return this._map;
};

/**
 * Creates {google.maps.ImageMapTypeOptions} for specific data overlay.
 *
 * @param {string} overlay.directory directory where images for this overlay are hosted
 * @param {string} overlay.name name of the overlay
 * @returns {{getTileUrl: function, tileSize: google.maps.Size, maxZoom: number, minZoom: number, radius: number, name: string}}
 */
GoogleMapsApiCanvas.prototype.createMapTypeOption = function (overlay) {
  var self = this;
  return {
    // this is a function that will retrieve valid url to png images for
    // tiles on different zoom levels
    getTileUrl: function (coord, zoom) {
      // we have 1 tile on self.getConfiguration().MIN_ZOOM and
      // therefore must limit tails according to this
      /* jshint bitwise: false */
      var maxTileRange = 1 << (zoom - self.getMinZoom());
      var maxTileXRange = maxTileRange;
      var maxTileYRange = maxTileRange;

      var width = self.getWidth();
      var height = self.getHeight();
      if (width > height) {
        maxTileYRange = height / width * maxTileRange;
      } else if (width < height) {
        maxTileXRange = width / height * maxTileRange;
      }
      if (coord.y < 0 || coord.y >= maxTileYRange || coord.x < 0 || coord.x >= maxTileXRange) {
        return null;
      }
      return overlay.directory + "/" + zoom + "/" + coord.x + "/" + coord.y + ".PNG";
    },
    tileSize: new google.maps.Size(self.getTileSize(), self.getTileSize()),
    maxZoom: self.getMaxZoom(),
    minZoom: self.getMinZoom(),
    radius: 360,
    name: overlay.name
  };
};

/**
 * Assigns overlays with images to the google map (which set of images should be
 * used by google maps api for which overlay).
 *
 * @param {{id: number, name:string, directory:string}[]} backgroundOverlays
 */
GoogleMapsApiCanvas.prototype.setupBackgroundOverlays = function (backgroundOverlays) {
  for (var i = 0; i < backgroundOverlays.length; i++) {
    var overlay = backgroundOverlays[i];
    var typeOptions = this.createMapTypeOption(overlay);
    var mapType = new google.maps.ImageMapType(typeOptions);
    this.getGoogleMap().mapTypes.set(overlay.id.toString(), mapType);
  }
  this.getGoogleMap().setMapTypeId(backgroundOverlays[0].id.toString());
};

/**
 *
 * @param {number} options.zoom
 * @param {Point} options.center
 * @returns {{center: google.maps.LatLng, rotateControl: boolean, panControl: boolean, mapTypeControl: boolean, zoom: number, streetViewControl: boolean, fullscreenControl: boolean, panControlOptions: {position: *}, zoomControlOptions: {style: string, position: *}}}
 */
GoogleMapsApiCanvas.prototype.prepareGoogleMapOptions = function (options) {
  var self = this;
  // noinspection SpellCheckingInspection
  return {
    center: self.fromPointToLatLng(options.center),
    rotateControl: true,
    panControl: true,
    mapTypeControl: false,
    zoom: options.zoom,
    streetViewControl: false,
    fullscreenControl: false,

    panControlOptions: {
      position: google.maps.ControlPosition.LEFT_TOP
    },
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.LARGE,
      position: google.maps.ControlPosition.LEFT_TOP
    }

  };
};

/**
 * Adds center button to the map.
 */
GoogleMapsApiCanvas.prototype.addCenterButton = function () {
  var self = this;
  var centerDiv = Functions.createElement({
    type: "div",
    style: "padding:5px"
  });
  var centerButton = Functions.createElement({
    type: "a",
    content: "<i class='fa fa-crosshairs' style='font-size:24px;color:grey'></i>&nbsp;",
    title: "center map",
    href: "#",
    onclick: function () {
      var bounds = new Bounds(new Point(0, 0), new Point(self.getWidth(), self.getHeight()));
      return self.fitBounds(bounds);
    },
    xss: false
  });
  centerDiv.appendChild(centerButton);
  self.getGoogleMap().controls[google.maps.ControlPosition.RIGHT_TOP].push(centerDiv);
};


/**
 * Transforms google.maps.LatLng to tile coordinate (for instance on which tile
 * mouse clicked).
 *
 *
 * @param {google.maps.LatLng} latLng
 *          coordinates in LatLng format
 * @param {number} zoom
 *          zoom level at which we want to find coordinates of tile
 * @return {Point} coordinates of a tile
 */
GoogleMapsApiCanvas.prototype.latLngToTile = function (latLng, zoom) {
  var self = this;
  var worldCoordinate = self.fromLatLngToPoint(latLng);
  var pixelCoordinate = new Point(worldCoordinate.x * Math.pow(2, zoom), worldCoordinate.y * Math.pow(2, zoom));
  return new Point(Math.floor(pixelCoordinate.x / self.getOptions().tileSize), Math
    .floor(pixelCoordinate.y / self.getOptions().tileSize));
};

/**
 *
 * @param {Point} options.position
 * @param {string} options.icon
 * @param {string} [options.id]
 * @returns {GoogleMapsApiMarker}
 */
GoogleMapsApiCanvas.prototype.createMarker = function (options) {
  if (!(options.position instanceof Point)) {
    throw new Error("position must be of type Point");
  }
  return new GoogleMapsApiMarker({
    position: options.position,
    icon: options.icon,
    id: options.id,
    map: this
  });
};

/**
 *
 * @param {Bounds} options.bounds
 * @param {string} options.id
 * @param {number} options.fillOpacity
 * @param {string} options.fillColor
 * @param {string} options.strokeColor
 * @param {number} options.strokeOpacity
 * @param {number} options.strokeWeight
 * @returns {GoogleMapsApiRectangle}
 */

GoogleMapsApiCanvas.prototype.createRectangle = function (options) {
  if (!(options.bounds instanceof Bounds)) {
    throw new Error("position must be of type Bounds");
  }
  options.map = this;
  return new GoogleMapsApiRectangle(options);
};

/**
 *
 * @param {Point[]} options.path
 * @param {string} options.strokeColor
 * @param {number} options.strokeOpacity
 * @param {number} options.strokeWeight
 * @returns {GoogleMapsApiPolyline}
 */

GoogleMapsApiCanvas.prototype.createPolyline = function (options) {
  options.map = this;
  return new GoogleMapsApiPolyline(options);
};

/**
 *
 * @param {Point} options.position
 * @param {GoogleMapsApiMarker} [options.marker]
 * @param {string} options.id
 * @returns {GoogleMapsApiInfoWindow}
 */
GoogleMapsApiCanvas.prototype.createInfoWindow = function (options) {
  if (!(options.position instanceof Point)) {
    throw new Error("position must be of type Point");
  }
  return new GoogleMapsApiInfoWindow({
    position: options.position,
    marker: options.marker,
    id: options.id,
    map: this
  });
};

/**
 *
 * @param {HTMLElement} element
 */
GoogleMapsApiCanvas.prototype.addLeftBottomControl = function (element) {
  this.getGoogleMap().controls[google.maps.ControlPosition.LEFT_BOTTOM].push(element);
};

/**
 *
 * @param {HTMLElement} element
 */
GoogleMapsApiCanvas.prototype.addRightBottomControl = function (element) {
  this.getGoogleMap().controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(element);
};

/**
 * @param {string} type
 * @param {Object} [data]
 */
GoogleMapsApiCanvas.prototype.triggerListeners = function (type, data) {
  var self = this;
  google.maps.event.trigger(this.getGoogleMap(), type, data);
  if (type === "resize") {
    return Promise.delay(100).then(function () {
      return self.callListeners("center_changed");
    });
  }
};

/**
 * @param {string} type
 * @param {Object} [data]
 */
GoogleMapsApiCanvas.prototype.triggerEvent = function (type, data) {
  var mev = data;
  if (type === "click" || type === "rightclick") {
    var latLng = undefined;
    if (data.point !== undefined) {
      latLng = this.fromPointToLatLng(data.point);
    }
    mev = {
      stop: data.stop,
      latLng: latLng
    };
  }
  return google.maps.event.trigger(this.getGoogleMap(), type, mev);
};

/**
 *
 * @param {Bounds} bounds
 */
GoogleMapsApiCanvas.prototype.fitBounds = function (bounds) {
  if (!(bounds instanceof Bounds)) {
    throw new Error("Expected bounds but found: " + bounds);
  }
  var latLngBounds = new google.maps.LatLngBounds();
  latLngBounds.extend(this.fromPointToLatLng(bounds.getTopLeft()));
  latLngBounds.extend(this.fromPointToLatLng(bounds.getRightBottom()));

  this.getGoogleMap().fitBounds(latLngBounds);
};

/**
 *
 * @param {Point} point
 * @returns {Promise|void}
 */
GoogleMapsApiCanvas.prototype.setCenter = function (point) {
  if (!(point instanceof Point)) {
    throw new Error("Expected bounds but found: " + bounds);
  }
  return this.getGoogleMap().setCenter(this.fromPointToLatLng(point));
};

/**
 *
 * @returns {Point}
 */
GoogleMapsApiCanvas.prototype.getCenter = function () {
  return this.fromLatLngToPoint(this.getGoogleMap().getCenter());
};

/**
 *
 * @returns {number}
 */
GoogleMapsApiCanvas.prototype.getZoom = function () {
  return this.getGoogleMap().getZoom();
};

/**
 *
 * @param zoom
 * @returns {Promise|void}
 */
GoogleMapsApiCanvas.prototype.setZoom = function (zoom) {
  return this.getGoogleMap().setZoom(zoom);
};

/**
 *
 * @returns {string}
 */
GoogleMapsApiCanvas.prototype.getBackgroundId = function () {
  return this.getGoogleMap().getMapTypeId();
};

/**
 *
 * @param {string|number} backgroundId
 */
GoogleMapsApiCanvas.prototype.setBackgroundId = function (backgroundId) {
  this.getGoogleMap().setMapTypeId(backgroundId.toString());
};

/**
 *
 * @returns {Bounds}
 */
GoogleMapsApiCanvas.prototype.getBounds = function () {
  var self = this;
  var bounds = self.getGoogleMap().getBounds();
  var ne = bounds.getNorthEast();
  var sw = bounds.getSouthWest();

  var p1 = this.fromLatLngToPoint(ne);
  var p2 = this.fromLatLngToPoint(sw);
  if (p2.x > p1.x) {
    p2.x -= 360 * self.pixelsPerLonDegree_ * self.zoomFactor;
  }
  return new Bounds(p1, p2);
};

/*
* Turns on drawing manager on the map.
*/
GoogleMapsApiCanvas.prototype.turnOnDrawing = function () {

  if (this.isDrawingOn()) {
    logger.warn("Trying to turn on drawing manager, but it is already available.");
    return;
  }
  var self = this;
  this._drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [google.maps.drawing.OverlayType.POLYGON]
    },
    markerOptions: {
      icon: 'images/beachflag.png'
    },
    circleOptions: {
      fillColor: '#ffff00',
      fillOpacity: 1,
      strokeWeight: 5,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
  self._drawingManager.setMap(self.getGoogleMap());
  self._drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);

  google.maps.event.addListener(this._drawingManager, 'overlaycomplete', function (e) {
    if (e.type !== google.maps.drawing.OverlayType.MARKER) {
      // Switch back to non-drawing mode after drawing a shape.
      self._drawingManager.setDrawingMode(null);

      // Add an event listener that selects the newly-drawn shape when the
      // user mouses down on it.
      var newShape = e.overlay;
      newShape.type = e.type;
      google.maps.event.addListener(newShape, 'rightclick', function (e) {
        self.setSelectedArea(newShape);
        newShape.position = e.latLng;

        var polygon = self.areaToString(newShape);

        return self.callListeners("shape-rightclick", {polygon: polygon});
      });
    }
  });
};

/**
 * Turns off drawing manager on the map.
 */
GoogleMapsApiCanvas.prototype.turnOffDrawing = function () {
  if (this.isDrawingOn()) {
    this._drawingManager.setMap(null);
    this._drawingManager = null;
  } else {
    logger.warn("Trying to turn off drawing manager, but it is not available.");
  }
};

/**
 * Checks if the drawing manager for the map is on.
 *
 * @returns {boolean}
 */
GoogleMapsApiCanvas.prototype.isDrawingOn = function () {
  return this._drawingManager !== null && this._drawingManager !== undefined;
};

/**
 * Sets selectedArea on this map.
 *
 *
 * @param {Object} area
 */
GoogleMapsApiCanvas.prototype.setSelectedArea = function (area) {
  this._selectedArea = area;
};

/**
 * Returns selectedArea on this map.
 *
 * @returns {Object}
 */
GoogleMapsApiCanvas.prototype.getSelectedArea = function () {
  return this._selectedArea;
};

/**
 * Transforms google.maps.Polygon into string with coordinates.
 *
 * @param {google.maps.Polygon} area
 * @returns {string}
 */
GoogleMapsApiCanvas.prototype.areaToString = function (area) {
  var len = area.getPath().length;
  var path = area.getPath();
  var res = "";
  for (var i = 0; i < len; i++) {
    var point = this.fromLatLngToPoint(path.getAt(i));
    res += point.x.toFixed(2) + "," + point.y.toFixed(2) + ";";
  }
  return res;
};

/**
 * Removes selected area (polygon) from the map.
 */
GoogleMapsApiCanvas.prototype.removeSelection = function () {
  if (this._selectedArea) {
    this._selectedArea.setMap(null);
    this._selectedArea = null;
  } else {
    logger.warn("Cannot remove selected area. No area was selected");
  }
};

/**
 * Transforms coordinates on the map from {Point} to
 * {google.maps.LatLng}
 *
 * @param {Point} point
 *          coordinates in standard x,y format
 * @return {google.maps.LatLng} coordinates in lat,lng format
 */
GoogleMapsApiCanvas.prototype.fromPointToLatLng = function (point) {
  var tmp = MapCanvas.prototype.pointToLatLng.call(this, point);
  return new google.maps.LatLng(tmp[0], tmp[1]);
};

/**
 * Transforms coordinates on the map from {google.maps.LatLng} to
 * {Point}
 *
 * @param {google.maps.LatLng} latLng
 *          in lat,lng format
 * @returns {Point} coordinates in x,y format
 *
 */
GoogleMapsApiCanvas.prototype.fromLatLngToPoint = function (latLng) {
  return MapCanvas.prototype.latLngToPoint.call(this, [latLng.lat(), latLng.lng()]);
};


module.exports = GoogleMapsApiCanvas;
