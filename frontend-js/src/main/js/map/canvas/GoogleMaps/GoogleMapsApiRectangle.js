"use strict";

var Rectangle = require('../Rectangle');
var Bounds = require('../Bounds');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

/**
 *
 * @param {MapCanvas} options.map
 * @param {Bounds} options.bounds
 * @param {MapCanvas} options.fillOpacity
 * @param {number} options.id
 * @param {number} options.strokeWeight
 * @param {string} options.fillColor
 * @param {string} options.strokeColor
 * @param {number} options.strokeOpacity
 *
 * @constructor
 * @extends Rectangle
 */
function GoogleMapsApiRectangle(options) {
  Rectangle.call(this, options);

  var self = this;

  var bounds = new google.maps.LatLngBounds();
  bounds.extend(this.getMap().fromPointToLatLng(options.bounds.getTopLeft()));
  bounds.extend(this.getMap().fromPointToLatLng(options.bounds.getRightBottom()));

  this.setGoogleRectangle(new google.maps.Rectangle({
    bounds: bounds,
    fillOpacity: options.fillOpacity,
    id: options.id,
    strokeWeight: options.strokeWeight,
    fillColor: options.fillColor,
    strokeColor: options.strokeColor,
    strokeOpacity: options.strokeOpacity
  }));

  google.maps.event.addListener(this.getGoogleRectangle(), "click", function(){
    return self.callListeners("click");
  });
}

GoogleMapsApiRectangle.prototype = Object.create(Rectangle.prototype);
GoogleMapsApiRectangle.prototype.constructor = GoogleMapsApiRectangle;

/**
 *
 * @param {google.maps.Rectangle} rectangle
 */
GoogleMapsApiRectangle.prototype.setGoogleRectangle = function (rectangle) {
  this._rectangle = rectangle;
};

/**
 *
 * @returns {google.maps.Rectangle}
 */
GoogleMapsApiRectangle.prototype.getGoogleRectangle = function () {
  return this._rectangle;
};

GoogleMapsApiRectangle.prototype.show = function () {
  var googleRectangle = this.getGoogleRectangle();
  if (googleRectangle.getMap() !== undefined && googleRectangle.getMap() !== null) {
    logger.warn("Rectangle is already shown");
  }
  else {
    googleRectangle.setMap(this.getMap().getGoogleMap());
  }
};

GoogleMapsApiRectangle.prototype.hide = function () {
  if (!this.isShown()) {
    logger.warn("Rectangle is already invisible");
  } else {
    this.getGoogleRectangle().setMap(null);
  }
};

/**
 *
 * @returns {boolean}
 */
GoogleMapsApiRectangle.prototype.isShown = function () {
  var googleRectangle = this.getGoogleRectangle();
  return googleRectangle.getMap() !== null && googleRectangle.getMap() !== undefined;
};

/**
 *
 * @param bounds {Bounds}
 */
GoogleMapsApiRectangle.prototype.setBounds = function (bounds) {
  var latLngBounds = new google.maps.LatLngBounds();
  latLngBounds.extend(this.getMap().fromPointToLatLng(bounds.getTopLeft()));
  latLngBounds.extend(this.getMap().fromPointToLatLng(bounds.getRightBottom()));
  this.getGoogleRectangle().setBounds(latLngBounds);
};

/**
 *
 * @returns {Bounds}
 */
GoogleMapsApiRectangle.prototype.getBounds = function () {
  var latLngBounds = this.getGoogleRectangle().getBounds();
  var result = new Bounds();
  result.extend(this.getMap().fromLatLngToPoint(latLngBounds.getSouthWest()));
  result.extend(this.getMap().fromLatLngToPoint(latLngBounds.getNorthEast()));
  return result;
};

/**
 *
 * @param {Object} options
 */
GoogleMapsApiRectangle.prototype.setOptions = function (options) {
  this.getGoogleRectangle().setOptions(options);
};


module.exports = GoogleMapsApiRectangle;
