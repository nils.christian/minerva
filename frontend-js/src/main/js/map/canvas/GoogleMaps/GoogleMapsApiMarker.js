"use strict";

var Bounds = require('../Bounds');
var Marker = require('../Marker');
var Promise = require('bluebird');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

/**
 *
 * @param {Point} options.position
 * @param {string} options.icon
 * @param {string} options.id
 * @param {GoogleMapsApiCanvas} options.map

 * @constructor
 * @extends {Marker}
 */

function GoogleMapsApiMarker(options) {
  Marker.call(this, options);

  var self = this;

  self.setGoogleMarker(new google.maps.Marker({
    position: this.getMap().fromPointToLatLng(options.position),
    icon: options.icon,
    id: options.id
  }));

  google.maps.event.addListener(self.getGoogleMarker(), "click", function () {
    return self.callListeners("click");
  });

}

GoogleMapsApiMarker.prototype = Object.create(Marker.prototype);
GoogleMapsApiMarker.prototype.constructor = GoogleMapsApiMarker;

/**
 *
 * @param {google.maps.Marker} marker
 */
GoogleMapsApiMarker.prototype.setGoogleMarker = function (marker) {
  this._marker = marker;
};

/**
 *
 * @returns {google.maps.Marker}
 */
GoogleMapsApiMarker.prototype.getGoogleMarker = function () {
  return this._marker;
};

/**
 *
 * @returns {Promise}
 */
GoogleMapsApiMarker.prototype.show = function () {
  var googleMarker = this.getGoogleMarker();
  if (googleMarker.getMap() !== undefined && googleMarker.getMap() !== null) {
    logger.warn("Marker is already shown");
  } else {
    googleMarker.setMap(this.getMap().getGoogleMap());
  }
  return Promise.resolve();
};

GoogleMapsApiMarker.prototype.hide = function () {
  if (!this.isShown()) {
    logger.warn("Marker is already invisible");
  } else {
    this.getGoogleMarker().setMap(null);
  }
};

/**
 *
 * @returns {boolean}
 */
GoogleMapsApiMarker.prototype.isShown = function () {
  var googleMarker = this.getGoogleMarker();
  return googleMarker.getMap() !== null && googleMarker.getMap() !== undefined;
};

/**
 *
 * @returns {Bounds}
 */
GoogleMapsApiMarker.prototype.getBounds = function () {
  return new Bounds(this.getMap().fromLatLngToPoint(this.getGoogleMarker().position));
};

/**
 *
 * @param {string} icon
 */
GoogleMapsApiMarker.prototype.setIcon = function (icon) {
  this.getGoogleMarker().setIcon(icon);
};


module.exports = GoogleMapsApiMarker;
