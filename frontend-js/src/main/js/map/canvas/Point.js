"use strict";

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {number|string||Point} x
 * @param {number|string} [y]
 * @constructor
 */
function Point(x, y) {
  if (x instanceof Point) {
    this.x = x.x;
    this.y = x.y;
  } else if (typeof x === 'object') {
    this.x = parseFloat(x.x);
    this.y = parseFloat(x.y);
  } else {
    this.x = parseFloat(x);
    this.y = parseFloat(y);
  }
}

module.exports = Point;
