"use strict";

var Rectangle = require('../Rectangle');
var Bounds = require('../Bounds');
var Point = require('../Point');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var Functions = require('../../../Functions');

/**
 *
 * @param options
 * @constructor
 * @extends Rectangle
 */
function OpenLayersRectangle(options) {
  Rectangle.call(this, options);

  var self = this;

  if (options.strokeColor === undefined) {
    options.strokeColor = "#000000";
  }
  if (options.strokeWeight === undefined) {
    options.strokeWeight = 1;
  }
  this._options = options;

  var style = self.createStyle(options);

  var polygon = this.createPolygon(options.bounds);

  var feature = new ol.Feature({
    geometry: polygon,
    id: options.id
  });

  this.setOpenLayersRectangle(feature);
  feature.setStyle(new ol.style.Style({}));
  options.source.addFeature(feature);

  feature.__openLayerRectangle = this;

  this._style = style;

}

OpenLayersRectangle.prototype = Object.create(Rectangle.prototype);
OpenLayersRectangle.prototype.constructor = OpenLayersRectangle;

OpenLayersRectangle.prototype.createPolygon = function (bounds) {
  var self = this;

  var p1 = bounds.getTopLeft();
  var p3 = bounds.getRightBottom();
  var p2 = new Point(p3.x, p1.y);
  var p4 = new Point(p1.x, p3.y);

  var points = [
    self.getMap().fromPointToProjection(p1),
    self.getMap().fromPointToProjection(p2),
    self.getMap().fromPointToProjection(p3),
    self.getMap().fromPointToProjection(p4)
  ];

  return new ol.geom.Polygon([points]);
};

OpenLayersRectangle.prototype.createStyle = function (options) {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: Functions.colorToRgbaString(options.strokeColor, options.strokeOpacity),
      width: options.strokeWeight
    }),
    fill: new ol.style.Fill({
      color: Functions.colorToRgbaString(options.fillColor, options.fillOpacity)
    })
  });
};


OpenLayersRectangle.prototype.setOpenLayersRectangle = function (rectangle) {
  this._rectangle = rectangle;
};

OpenLayersRectangle.prototype.getOpenLayersRectangle = function () {
  return this._rectangle;
};

OpenLayersRectangle.prototype.show = function () {
  return this.getOpenLayersRectangle().setStyle(this._style);
};

OpenLayersRectangle.prototype.hide = function () {
  return this.getOpenLayersRectangle().setStyle(new ol.style.Style({}));
};
OpenLayersRectangle.prototype.isShown = function () {
  return this.getOpenLayersRectangle().getStyle().getFill() !== null;
};

/**
 *
 * @param bounds {Bounds}
 */
OpenLayersRectangle.prototype.setBounds = function (bounds) {
  this._options.bounds = bounds;
  this.getOpenLayersRectangle().setGeometry(this.createPolygon(bounds));
};

OpenLayersRectangle.prototype.getBounds = function () {
  var self = this;
  var extent = self.getOpenLayersRectangle().getGeometry().getExtent();

  var projection1 = [extent[0], extent[1]];
  var p1 = self.getMap().fromProjectionToPoint(projection1);
  var projection2 = [extent[2], extent[3]];
  var p2 = self.getMap().fromProjectionToPoint(projection2);
  return new Bounds(p1, p2);
};

OpenLayersRectangle.prototype.setOptions = function (options) {
  var self = this;
  self._options = Object.assign(self._options, options);
  var style = self.createStyle(self._options);
  if (self.isShown()) {
    self.getOpenLayersRectangle().setStyle(style);
  }
  self._style = style;
};

module.exports = OpenLayersRectangle;
