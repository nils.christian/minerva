"use strict";

var InfoWindow = require('../InfoWindow');
var OpenLayerMarker = require('./OpenLayerMarker');

var logger = require('../../../logger');
// noinspection JSUnusedLocalSymbols
var Functions = require('../../../Functions');

var Promise = require('bluebird');

function modifyPopover() {
  if ($.fn.popover.Constructor.prototype.reposition === undefined) {
    $.fn.popover.Constructor.prototype.reposition = function () {
      var $tip = this.tip();

      var placement = typeof this.options.placement === 'function' ? this.options.placement.call(this, $tip[0], this.$element[0]) : this.options.placement;

      var autoToken = /\s?auto?\s?/i;
      var autoPlace = autoToken.test(placement);
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top';

      var pos = this.getPosition();
      var actualWidth = $tip[0].offsetWidth;
      var actualHeight = $tip[0].offsetHeight;

      if (autoPlace) {
        var orgPlacement = placement;
        var viewportDim = this.getPosition(this.$viewport);

        placement = placement === 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top' :
          placement === 'top' && pos.top - actualHeight < viewportDim.top ? 'bottom' :
            placement === 'right' && pos.right + actualWidth > viewportDim.width ? 'left' :
              placement === 'left' && pos.left - actualWidth < viewportDim.left ? 'right' :
                placement;

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight);

      this.applyPlacement(calculatedOffset, placement)
    }
  }
}

function OpenLayerInfoWindow(options) {
  InfoWindow.call(this, options);

  modifyPopover();


  var self = this;

  if (options.marker !== undefined) {
    if (options.marker instanceof OpenLayerMarker) {
      self.setMarker(options.marker);
    } else {
      throw new Error("Invalid argument: " + options.marker);
    }
  }
  var element = Functions.createElement({type: "div", name: "popup-window"});

  options.map.getOpenLayersMap().getTargetElement().appendChild(element);

  var popup = new ol.Overlay({
    element: element,
    positioning: 'bottom-center'
  });
  options.map.getOpenLayersMap().addOverlay(popup);

  self._position = options.position;
  self._element = element;

  self.setOpenLayerInfoWindow(popup);
  self._visible = false;
  self.setContent(options.content);

  //the only way that I found how to keep track if the window is opened or noth
  $(element).on('hidden.bs.popover', function () {
    self._visible = false;
  });
  $(element).on('shown.bs.popover', function () {
    self._visible = true;
  });
}

OpenLayerInfoWindow.prototype = Object.create(InfoWindow.prototype);
OpenLayerInfoWindow.prototype.constructor = OpenLayerInfoWindow;

OpenLayerInfoWindow.prototype.updatePosition = function () {
  var self = this;
  var popup = self.getOpenLayerInfoWindow();
  var feature = self.getMarker();
  var position;
  if (feature !== undefined && feature !== null) {
    position = feature.getGeometry().getCoordinates();
    popup.setOffset([0, -36]);
  } else {
    position = self.getMap().fromPointToProjection(self._position);
    popup.setOffset([0, 0]);
  }
  popup.setPosition(position);

  return Promise.delay(100).then(function () {
    $(self._element).popover('reposition');
  });
};

OpenLayerInfoWindow.prototype.open = function () {
  var self = this;

  var title = Functions.createElement({type: "div", content: "&nbsp;"});
  title.appendChild(Functions.createElement({
    type: "button", className: "close", onclick: function () {
      return self.hide();
    },
    content: "&times;"
  }));

  $(self._element).popover({
    'placement': 'top',
    'title': title,
    'content': this._content,
    'html': true
  });

  self.updatePosition();
  $(self._element).popover('show');
  self._visible = true;
};

OpenLayerInfoWindow.prototype.hide = function () {
  var self = this;
  $(self._element).popover('hide');
  self._visible = false;
};

OpenLayerInfoWindow.prototype.setOpenLayerInfoWindow = function (infoWindow) {
  this._infoWindow = infoWindow;
};

OpenLayerInfoWindow.prototype.getOpenLayerInfoWindow = function () {
  return this._infoWindow;
};

OpenLayerInfoWindow.prototype.setContent = function (content) {
  var self = this;
  if (self._content === undefined) {
    self._content = Functions.createElement({type: "div"});
  }
  if (Functions.isDomElement(content)) {
    Functions.removeChildren(self._content);
    self._content.appendChild(content);
  } else {
    self._content.innerHTML = content;
  }
  self.updatePosition();

};

/**
 *
 * @param {OpenLayerMarker} marker
 */
OpenLayerInfoWindow.prototype.setMarker = function (marker) {
  var self = this;
  marker.addListener("onHide", function () {
    return self.hide();
  });
  self._marker = marker.getOpenLayerMarker();
};

OpenLayerInfoWindow.prototype.getMarker = function () {
  return this._marker;
};

OpenLayerInfoWindow.prototype.isOpened = function () {
  return this._visible;
};


module.exports = OpenLayerInfoWindow;
