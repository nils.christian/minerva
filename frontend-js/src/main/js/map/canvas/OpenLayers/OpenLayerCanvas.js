"use strict";

var ol;

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var Bounds = require('../Bounds');
var Functions = require('../../../Functions');
var MapCanvas = require('../MapCanvas');
var OpenLayerMarker = require('./OpenLayerMarker');
var OpenLayerPolyline = require('./OpenLayerPolyline');
var OpenLayerRectangle = require('./OpenLayerRectangle');
var OpenLayerInfoWindow = require('./OpenLayerInfoWindow');

var Point = require('../Point');

var Promise = require('bluebird');

function OpenLayerCanvas(element, options) {
  ol = require('openlayers/dist/ol-debug');

  MapCanvas.call(this, element, options);
  $(element).css("background-color", "#e4e2de");
  var self = this;

  self._markerLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
      features: []
    })
  });

  self._rectangleLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
      features: []
    })
  });

  self._polylineLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
      features: []
    })
  });

  self._drawingLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
      features: []
    }),
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: Functions.colorToRgbaString("#000000", 1.0),
        width: 3.0
      }),
      fill: new ol.style.Fill({
        color: Functions.colorToRgbaString("#000000", 0.5)
      })
    })
  });

  var layers = self.createLayers(options);
  layers.push(self._rectangleLayer);
  layers.push(self._markerLayer);
  layers.push(self._polylineLayer);
  layers.push(self._drawingLayer);

  var map = new ol.Map({
    controls: ol.control.defaults({
      attributionOptions: {
        collapsible: false
      }
    }),
    target: element,
    layers: layers,
    view: new ol.View({
      center: self.fromPointToProjection(options.center),
      zoom: options.zoom,
      minZoom: options.minZoom,
      maxZoom: options.maxZoom,
      enableRotation: false
    })
  });
  self.setOpenLayersMap(map);
  self.addCenterButton();

  map.on("click", function (e) {
    var features = [];
    map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
      features.push(feature);
      if (layer === self._markerLayer) {
        return feature.__openLayerMarker.callListeners("click");
      } else if (layer === self._rectangleLayer) {
        return feature.__openLayerRectangle.callListeners("click");
      } else if (layer === self._polylineLayer) {
        return feature.__openLayerPolyline.callListeners("click");
      } else if (layer === self._drawingLayer) {
        self.setSelectedArea(feature);
        var polygon = self.featureToString(feature);
        return self.callListeners("shape-rightclick", {polygon: polygon});
      } else if (layer !== null) {
        throw new Error("Unknown layer");
      }
    });
    if (features.length === 0) { //we clicked on something - not directly on the map
      var point = self.fromProjectionToPoint(e.coordinate);
      return self.callListeners("click", {point: point});
    }
  });

  //rightclick
  map.getViewport().addEventListener('contextmenu', function (evt) {
    evt.preventDefault();

    var featureClicked;
    map.forEachFeatureAtPixel(map.getEventPixel(evt), function (feature, layer) {
      if (layer === self._drawingLayer) {
        featureClicked = feature;
      }
    });
    if (featureClicked) {
      self.setSelectedArea(featureClicked);
      var polygon = self.featureToString(featureClicked);
      return self.callListeners("shape-rightclick", {polygon: polygon});
    } else {
      var projectionPoint = map.getEventCoordinate(evt);
      var point = self.fromProjectionToPoint(projectionPoint);
      return self.callListeners("rightclick", {point: point});
    }
  });

  var dragStarted = false;
  $(element).on("mouseup", function () {
    dragStarted = false;
    //there is nothing in the api that allows for it
    return self.callListeners("mouseup");
  });
  map.on("pointerdrag", function (evt) {
    if (!dragStarted) {
      dragStarted = true;
      return self.callListeners("dragstart");
    }
  });

  map.getView().on('change:resolution', function (e) {
    var zoom = map.getView().getZoom();
    if (zoom === Math.floor(zoom)) {
      return self.callListeners("zoom_changed");
    }
  });
  map.getView().on('change:center', function () {
    return self.callListeners("center_changed");
  });


  // change mouse cursor when over marker
  $(map.getViewport()).on('mousemove', function (e) {
    var pixel = map.getEventPixel(e.originalEvent);
    var hit = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
      return true;
    });
    if (hit) {
      map.getTarget().style.cursor = 'pointer';
    } else {
      map.getTarget().style.cursor = '';
    }
  });

}

OpenLayerCanvas.prototype = Object.create(MapCanvas.prototype);
OpenLayerCanvas.prototype.constructor = MapCanvas;

OpenLayerCanvas.prototype.setOpenLayersMap = function (map) {
  this._map = map;
};

OpenLayerCanvas.prototype.getOpenLayersMap = function () {
  return this._map;
};

OpenLayerCanvas.prototype.createLayers = function (options) {
  var self = this;
  var result = [];
  self._layers = [];
  options.backgroundOverlays.forEach(function (overlay, index) {
    var layer = new ol.layer.Tile({
      visible: (index === 0),
      source: new ol.source.XYZ({
        minZoom: options.minZoom,
        maxZoom: options.maxZoom,
        wrapX: false,
        tileLoadFunction: function (imageTile, src) {
          if (src !== null) {
            imageTile.getImage().src = src;
          }
        },
        tileUrlFunction: function (coordinate) {

          var zoom = coordinate[0];
          // we have 1 tile on self.getConfiguration().MIN_ZOOM and
          // therefore must limit tails according to this
          /* jshint bitwise: false */
          var maxTileRange = 1 << (zoom - self.getMinZoom());
          var maxTileXRange = maxTileRange;
          var maxTileYRange = maxTileRange;

          //transform the coordinates to something that was used in google maps
          var x = coordinate[1];
          var y = -coordinate[2] - 1;

          var width = self.getWidth();
          var height = self.getHeight();
          if (width > height) {
            maxTileYRange = height / width * maxTileRange;
          } else if (width < height) {
            maxTileXRange = width / height * maxTileRange;
          }
          if (y < 0 || y >= maxTileYRange || x < 0 || x >= maxTileXRange) {
            return null;
          }

          return overlay.directory + "/" + zoom + "/" + x + "/" + y + ".PNG";
        }
      })
    });
    layer.on('change:visible', function () {
      if (layer.getVisible()) {
        return self.callListeners("maptypeid_changed");
      }
    });
    self._layers[overlay.id] = layer;
    result.push(layer);
  });
  return result;
};

/**
 *
 * @returns {OpenLayerMarker}
 */
OpenLayerCanvas.prototype.createMarker = function (options) {
  if (!(options.position instanceof Point)) {
    throw new Error("position must be of type Point");
  }
  options.map = this;
  options.source = this._markerLayer.getSource();
  return new OpenLayerMarker(options);
};

/**
 *
 * @param {Point} options.position
 * @param {OpenLayerMarker} [options.marker]
 * @param {string} options.id
 *
 * @returns {OpenLayerInfoWindow}
 */
OpenLayerCanvas.prototype.createInfoWindow = function (options) {
  options.map = this;
  return new OpenLayerInfoWindow(options);
};

/**
 *
 * @param options
 * @returns {OpenLayersRectangle}
 */
OpenLayerCanvas.prototype.createRectangle = function (options) {
  if (!(options.bounds instanceof Bounds)) {
    throw new Error("position must be of type Bounds");
  }
  options.map = this;
  options.source = this._rectangleLayer.getSource();
  return new OpenLayerRectangle(options);
};

/**
 *
 * @param options
 * @returns {OpenLayerPolyline}
 */
OpenLayerCanvas.prototype.createPolyline = function (options) {
  options.map = this;
  options.source = this._polylineLayer.getSource();
  return new OpenLayerPolyline(options);
};

/**
 *
 * @param {HTMLElement} element
 */
OpenLayerCanvas.prototype.addLeftBottomControl = function (element) {
  var wrapper = Functions.createElement({type: "div", className: "ol-control minerva-ol-bottom-left"});
  wrapper.appendChild(element);
  this.getOpenLayersMap().addControl(new ol.control.Control({element: wrapper}));
};

/**
 *
 * @param {HTMLElement} element
 */
OpenLayerCanvas.prototype.addRightBottomControl = function (element) {
  var wrapper = Functions.createElement({type: "div", className: "ol-control minerva-ol-bottom-right"});
  wrapper.appendChild(element);
  this.getOpenLayersMap().addControl(new ol.control.Control({element: wrapper}));
};

/**
 * Adds center button to the map.
 */
OpenLayerCanvas.prototype.addCenterButton = function () {
  var self = this;
  var wrapper = Functions.createElement({
    type: "div",
    className: "ol-control minerva-ol-top-right"
  });
  wrapper.appendChild(Functions.createElement({
    type: "a",
    content: "<i class='fa fa-crosshairs' style='font-size:24px;color:grey'></i>&nbsp;",
    title: "center map",
    href: "#",
    onclick: function () {
      var bounds = new Bounds(new Point(0, 0), new Point(self.getWidth(), self.getHeight()));
      return self.fitBounds(bounds);
    },
    xss: false
  }));
  this.getOpenLayersMap().addControl(new ol.control.Control({element: wrapper}));
};


OpenLayerCanvas.prototype.fitBounds = function (bounds) {
  var self = this;
  var projectionBounds = [self.fromPointToProjection(bounds.getRightBottom()), self.fromPointToProjection(bounds.getTopLeft())];
  var extent = ol.extent.boundingExtent(projectionBounds);
  return self.getOpenLayersMap().getView().fit(extent);
};

OpenLayerCanvas.prototype.setCenter = function (center) {
  this.getOpenLayersMap().getView().setCenter(this.fromPointToProjection(center));
};

/**
 * @returns {Point}
 */
OpenLayerCanvas.prototype.getCenter = function () {
  return this.fromProjectionToPoint(this.getOpenLayersMap().getView().getCenter());
};

/**
 * @returns {number}
 */
OpenLayerCanvas.prototype.getZoom = function () {
  return Math.floor(this.getOpenLayersMap().getView().getZoom());
};
/**
 *
 * @param {number} zoom
 */
OpenLayerCanvas.prototype.setZoom = function (zoom) {
  var self = this;
  if (self.getZoom() !== zoom) {
    self.getOpenLayersMap().getView().setZoom(zoom);
    return self.callListeners("zoom_changed")
  }
};

/**
 * @returns {string}
 */
OpenLayerCanvas.prototype.getBackgroundId = function () {
  var layers = this._layers;
  for (var id in layers) {
    if (layers.hasOwnProperty(id)) {
      var layer = layers[id];
      if (layer.getVisible()) {
        return id;
      }
    }
  }
  return null;
};

/**
 *
 * @param {number|string} backgroundId
 */
OpenLayerCanvas.prototype.setBackgroundId = function (backgroundId) {
  backgroundId = parseInt(backgroundId);
  var layers = this._layers;
  var id, layer;
  //first hide
  for (id in layers) {
    if (layers.hasOwnProperty(id)) {
      layer = layers[id];
      if (parseInt(id) !== backgroundId) {
        layer.setVisible(false);
      }
    }
  }
  //then show, so there will always be at most one map visible
  for (id in layers) {
    if (layers.hasOwnProperty(id)) {
      layer = layers[id];
      if (parseInt(id) === backgroundId) {
        layer.setVisible(true);
      }
    }
  }
};

/**
 * @returns {Bounds}
 */
OpenLayerCanvas.prototype.getBounds = function () {
  var self = this;

  var map = self.getOpenLayersMap();
  var extent = map.getView().calculateExtent(map.getSize());

  var projection1 = [extent[0], extent[1]];
  var p1 = self.fromProjectionToPoint(projection1);
  var projection2 = [extent[2], extent[3]];
  var p2 = self.fromProjectionToPoint(projection2);
  var maxWidth = 360 * self.pixelsPerLonDegree_ * self.zoomFactor;
  if (p1.x > maxWidth / 2) {
    p1.x -= maxWidth;
  }
  var bounds = new Bounds(p1, p2);
  //if the world is wrapped (it's a workaround and might not work for browser with huge resolution)
  if (!bounds.contains(self.getCenter())) {
    p1.x = -maxWidth / 2;
    p2.x = maxWidth / 2;
    bounds = new Bounds(p1, p2)
  }
  return bounds;
};

OpenLayerCanvas.prototype.turnOnDrawing = function () {
  var self = this;
  if (self.isDrawingOn()) {
    logger.warn("Trying to turn on drawing manager, but it is already available.");
    return;
  }
  self._draw = new ol.interaction.Draw({
    source: this._drawingLayer.getSource(),
    type: "Polygon",
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: Functions.colorToRgbaString("#000000", 1.0),
        width: 3.0
      }),
      fill: new ol.style.Fill({
        color: Functions.colorToRgbaString("#000000", 0.5)
      })
    })
  });

  self.getOpenLayersMap().addInteraction(self._draw);

  self._draw.on("drawend", function () {
    self.turnOffDrawing();
  });
};
OpenLayerCanvas.prototype.turnOffDrawing = function () {
  if (!this.isDrawingOn()) {
    logger.warn("Trying to turn off drawing manager, but it is not there.");
    return;
  }
  this.getOpenLayersMap().removeInteraction(this._draw);
  this._draw = undefined;
};
/**
 * @returns {boolean}
 */
OpenLayerCanvas.prototype.isDrawingOn = function () {
  return this._draw !== undefined;
};

OpenLayerCanvas.prototype.setSelectedArea = function (area) {
  this._selectedArea = area;
};

OpenLayerCanvas.prototype.getSelectedArea = function () {
  return this._selectedArea;
};

OpenLayerCanvas.prototype.removeSelection = function () {
  this._drawingLayer.getSource().removeFeature(this._selectedArea);
  this._selectedArea = undefined;
};

OpenLayerCanvas.prototype.triggerListeners = function (type, data) {
  var self = this;
  if (type === "resize") {
    return Promise.delay(200).then(function () {
      self.getOpenLayersMap().updateSize();
      return self.callListeners("center_changed");
    });
  } else if (type === "maptypeid_changed") {
    return self.callListeners("maptypeid_changed");
  } else if (type === "projection_changed") {
    // We ignore projection change event trigger.
  } else {
    throw new Error("Not implemented trigger listener: " + type);
  }
};

OpenLayerCanvas.prototype.fromPointToProjection = function (point) {
  var latLng = this.pointToLatLng(point);
  return ol.proj.fromLonLat([latLng[1], latLng[0]]);
};

/**
 *
 * @param {number[]|null} projection
 * @returns {Point|null}
 */
OpenLayerCanvas.prototype.fromProjectionToPoint = function (projection) {
  if (projection === null) {
    return null;
  }
  var lngLat = ol.proj.toLonLat(projection);
  return this.latLngToPoint([lngLat[1], lngLat[0]]);
};

/**
 * @param {string} type
 * @param {Object} [data]
 */
OpenLayerCanvas.prototype.triggerEvent = function (type, data) {
  var self = this;
  var mev = data;
  var map = self.getOpenLayersMap();
  if (type === "click") {
    var projection = undefined;
    if (data.point !== undefined) {
      projection = self.fromPointToProjection(data.point);
    }

    mev = {
      dragging: false,
      coordinate: projection,
      map: map,
      pixel: map.getPixelFromCoordinate(projection),
      target: map,
      type: type
    };
    return map.dispatchEvent(mev);
  } else if (type === "rightclick") {
    var clickEvent = new window.Event("contextmenu");
    return map.getViewport().dispatchEvent(clickEvent);
  } else if (type === "center_changed") {
    return self.callListeners("center_changed");
  } else {
    throw new Error("Don't know how to trigger: " + type);
  }
};

/**
 *
 * @param {ol.Feature} feature
 * @returns {string}
 */
OpenLayerCanvas.prototype.featureToString = function (feature) {
  var result = "";
  var coordinates = feature.getGeometry().getCoordinates()[0];
  for (var i = 0; i < coordinates.length; i++) {
    var point = this.fromProjectionToPoint(coordinates[i]);
    result += point.x.toFixed(2) + "," + point.y.toFixed(2) + ";";
  }
  return result;
};

module.exports = OpenLayerCanvas;
