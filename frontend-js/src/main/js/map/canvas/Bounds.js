"use strict";

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Point = require('./Point');

/**
 *
 * @param {Point} [p1]
 * @param {Point} [p2]
 * @constructor
 */
function Bounds(p1, p2) {
  this._topLeft = undefined;
  this._rightBottom = undefined;
  this.extend(p1);
  this.extend(p2);
}

/**
 *
 * @param {Point} [point]
 */
Bounds.prototype.extend = function (point) {
  if (point !== undefined) {
    if (point instanceof Bounds) {
      this.extend(point.getRightBottom());
      this.extend(point.getTopLeft());
    } else {
      if (!(point instanceof Point)) {
        point = new Point(point);
      }
      if (this._topLeft === undefined) {
        this._topLeft = new Point(point);
        this._rightBottom = new Point(point);
      } else {
        this._topLeft.x = Math.min(this._topLeft.x, point.x);
        this._topLeft.y = Math.min(this._topLeft.y, point.y);

        this._rightBottom.x = Math.max(this._rightBottom.x, point.x);
        this._rightBottom.y = Math.max(this._rightBottom.y, point.y);
      }
    }
  }
};

/**
 *
 * @returns {Point}
 */
Bounds.prototype.getTopLeft = function () {
  return this._topLeft;
};

/**
 *
 * @returns {Point}
 */
Bounds.prototype.getRightBottom = function () {
  return this._rightBottom;
};

/**
 *
 * @param {Point} point
 * @returns {boolean}
 */
Bounds.prototype.contains = function (point) {
  return this._topLeft.x <= point.x && //
    this._topLeft.y <= point.y && //
    this._rightBottom.x >= point.x && //
    this._rightBottom.y >= point.y;

};

module.exports = Bounds;
