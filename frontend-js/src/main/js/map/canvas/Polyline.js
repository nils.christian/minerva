"use strict";

var MapCanvas = require('./MapCanvas');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 * @param {MapCanvas} options.map
 *
 * @constructor
 * @augments {ObjectWithListeners}
 */
function Polyline(options) {
  ObjectWithListeners.call(this);
  this.setMap(options.map);
  this.registerListenerType("click");
}

Polyline.prototype = Object.create(ObjectWithListeners.prototype);
Polyline.prototype.constructor = ObjectWithListeners;

Polyline.prototype.show = function () {
  throw new Error("Not implemented");
};

Polyline.prototype.hide = function () {
  throw new Error("Not implemented");
};

/**
 * @returns {Bounds}
 */
Polyline.prototype.getBounds = function () {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Object} options
 */
Polyline.prototype.setOptions = function (options) {
  throw new Error("Not implemented");
};

/**
 *
 * @param {MapCanvas} map
 */
Polyline.prototype.setMap = function (map) {
  if (!(map instanceof MapCanvas) && map !== null) {
    throw new Error("Map must be of MapCanvas class");
  }
  this._map = map;
};

/**
 *
 * @returns {MapCanvas}
 */
Polyline.prototype.getMap = function () {
  return this._map;
};

module.exports = Polyline;
