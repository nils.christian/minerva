var Functions = require('../../Functions');
var SubMenu = require('../../gui/SubMenu');


function MolArt(containerParentElement, customMap) {

  var molartDiv = Functions.createElement({
    type: "div",
    id: "minervaMolArtContainer",
    className: "minerva-molart-container"
  });
  containerParentElement.appendChild(molartDiv);

  this.setContainerElement(molartDiv);
  this._customMap = customMap;
}

MolArt.prototype.setContainerElement = function (containerElement) {
  this._containerElement = containerElement;
};

MolArt.prototype.getContainerElement = function () {
  return this._containerElement;
};

function removeFromContextMenu(menu) {
  $(menu.getElement()).find('li:contains("MolArt")').remove();
}

MolArt.prototype.activateInContextMenu = function (uniprotIds) {

  var self = this;
  var menu = this._customMap.getContextMenu();
  removeFromContextMenu(menu);

  var submenu = new SubMenu({
    element: Functions.createElement({type: "li"}),
    name: "Open MolArt",
    customMap: self._customMap
  });
  uniprotIds.forEach(function (uniprotId) {
    submenu.addOption(uniprotId, function () {
      self._activate(uniprotId);
    }, false);
  });
  menu.addOption(submenu);

};

MolArt.prototype.deactivateInContextMenu = function () {
  var menu = this._customMap.getContextMenu();
  removeFromContextMenu(menu);
  menu.addOption("Open MolArt (no UniProt ID available)", function () {
  }, true);
};

MolArt.prototype._deactivate = function () {
  var container = this.getContainerElement();
  container.style.display = 'none';
  $(container).empty();
  this.molArt.destroy();
  this.molArt = undefined;
};

MolArt.prototype._activate = function (uniprotId) {

  var self = this;

  var container = this.getContainerElement();
  if (!container) return;

  var maxZIndex = Math.max.apply(null,
    $.map($('body *'), function (e) {
      if ($(e).css('position') !== 'static') {
        return parseInt($(e).css('z-index')) || 1;
      }
    }));

  $(container).css('z-index', maxZIndex + 1);

  var molArtCloseButton = Functions.createElement({
    type: "div",
    className: "minerva-molart-close-button",
    content: 'x'
  });
  molArtCloseButton.addEventListener('click', function () {
    self._deactivate();
    return false;
  });
  container.appendChild(molArtCloseButton);

  var MolArtPlugin = require('molart');

  this.molArt = new MolArtPlugin({
    uniprotId: uniprotId,
    containerId: container.id
  });
  container.style.display = 'block';
};


module.exports = MolArt;
