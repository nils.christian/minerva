"use strict";

/* exported logger */

var logger = require('../../logger');

var AbstractTargettingDbOverlay = require('./AbstractTargettingDbOverlay');

var ServerConnector = require('../../ServerConnector');

function ChemicalDbOverlay(params) {
  params.iconType= "ball";
  params.iconColorStart = 1;
  // call super constructor
  AbstractTargettingDbOverlay.call(this, params);
  
}

ChemicalDbOverlay.prototype = Object.create(AbstractTargettingDbOverlay.prototype);
ChemicalDbOverlay.prototype.constructor = ChemicalDbOverlay;

ChemicalDbOverlay.prototype.getNamesByTargetFromServer = function(param) {
  return ServerConnector.getChemicalNamesByTarget(param);
};

ChemicalDbOverlay.prototype.getElementsByQueryFromServer = function(param) {
  return ServerConnector.getChemicalsByQuery(param);
};

module.exports = ChemicalDbOverlay;
