"use strict";

/* exported logger */

var logger = require('../../logger');

var AbstractTargettingDbOverlay = require('./AbstractTargettingDbOverlay');

var ServerConnector = require('../../ServerConnector');

function MiRnaDbOverlay(params) {
  params.iconType= "target";
  params.iconColorStart = 2;
  // call super constructor
  AbstractTargettingDbOverlay.call(this, params);
  
}

MiRnaDbOverlay.prototype = Object.create(AbstractTargettingDbOverlay.prototype);
MiRnaDbOverlay.prototype.constructor = MiRnaDbOverlay;

MiRnaDbOverlay.prototype.getNamesByTargetFromServer = function(param) {
  return ServerConnector.getMiRnaNamesByTarget(param);
};

MiRnaDbOverlay.prototype.getElementsByQueryFromServer = function(param) {
  return ServerConnector.getMiRnasByQuery(param);
};

module.exports = MiRnaDbOverlay;
