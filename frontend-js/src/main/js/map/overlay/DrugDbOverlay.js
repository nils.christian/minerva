"use strict";

/* exported logger */

var logger = require('../../logger');

var AbstractTargettingDbOverlay = require('./AbstractTargettingDbOverlay');

var ServerConnector = require('../../ServerConnector');

function DrugDbOverlay(params) {
  params.iconType= "drug";
  params.iconColorStart = 5;
  // call super constructor
  AbstractTargettingDbOverlay.call(this, params);
}

DrugDbOverlay.prototype = Object.create(AbstractTargettingDbOverlay.prototype);
DrugDbOverlay.prototype.constructor = DrugDbOverlay;

DrugDbOverlay.prototype.getNamesByTargetFromServer = function(param) {
  return ServerConnector.getDrugNamesByTarget(param);
};

DrugDbOverlay.prototype.getElementsByQueryFromServer = function(param) {
  return ServerConnector.getDrugsByQuery(param);
};

module.exports = DrugDbOverlay;
