"use strict";

var ObjectWithListeners = require('../../ObjectWithListeners');

function PrivilegeType(data, name) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  self.setName(name);
  self.setCommonName(data.commonName);
  self.setObjectType(data.objectType);
  self.setValueType(data.valueType);
}

PrivilegeType.prototype = Object.create(ObjectWithListeners.prototype);
PrivilegeType.prototype.constructor = PrivilegeType;

PrivilegeType.CONFIGURATION_MANAGE = 'CONFIGURATION_MANAGE';
PrivilegeType.MANAGE_GENOMES = 'MANAGE_GENOMES';
PrivilegeType.PROJECT_MANAGEMENT = 'PROJECT_MANAGEMENT';
PrivilegeType.USER_MANAGEMENT = 'USER_MANAGEMENT';

PrivilegeType.prototype.setObjectType = function (objectType) {
  this._objectType = objectType;
};

PrivilegeType.prototype.getObjectType = function () {
  return this._objectType;
};
PrivilegeType.prototype.setValueType = function (valueType) {
  this._valueType = valueType;
};

PrivilegeType.prototype.getValueType = function () {
  return this._valueType;
};
PrivilegeType.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

PrivilegeType.prototype.getCommonName = function () {
  return this._commonName;
};

PrivilegeType.prototype.setName = function (name) {
  this._name = name;
};

PrivilegeType.prototype.getName = function () {
  return this._name;
};

module.exports = PrivilegeType;
