"use strict";

var Promise = require("bluebird");

var logger = require('../../logger');

var Alias = require('./Alias');
var IdentifiedElement = require('./IdentifiedElement');
var PointData = require('./PointData');
var Point = require('../canvas/Point');
var Reaction = require('./Reaction');

// This file describes javascript representation of Java Model class that
// represents content of the map.

/**
 * Default constructor.
 *
 */

function MapModel(configuration) {

  // list of aliases is empty (it will be filled dynamically - when necessary)
  this._aliases = [];

  // list of reactions is empty (it will be filled dynamically - when necessary)
  this._reactions = [];
  this._reactionsByParticipantElementId = [];

  // list of aliases that should be updated from server side during the next
  // connection
  this._missingAliases = [];

  // list of reactions that should be updated from server side during the next
  // connection
  this._missingReactions = [];

  // information about points and associated data (for now we have only comments
  // associated to the point,
  // but it can be extended)
  this._pointsData = [];

  this._sbmlFunctions = [];
  this._sbmlParameters = [];


  if (configuration !== undefined) {
    if (configuration instanceof MapModel) {
      this.setId(configuration.getId());
      this.setName(configuration.getName());
      this.setTileSize(configuration.getTileSize());
      this.setWidth(configuration.getWidth());
      this.setHeight(configuration.getHeight());
      this.setMinZoom(configuration.getMinZoom());
      this.setMaxZoom(configuration.getMaxZoom());
      this.setSubmodelType(configuration.getSubmodelType());
      this.setDefaultCenterX(configuration.getDefaultCenterX());
      this.setDefaultCenterY(configuration.getDefaultCenterY());
      this.setDefaultZoomLevel(configuration.getDefaultZoomLevel());
    } else {
      this.setId(configuration.idObject);
      this.setName(configuration.name);
      this.setTileSize(configuration.tileSize);
      this.setWidth(configuration.width);
      this.setHeight(configuration.height);
      this.setMinZoom(configuration.minZoom);
      this.setMaxZoom(configuration.maxZoom);
      this.setSubmodelType(configuration.submodelType);
      this.setDefaultCenterX(configuration.defaultCenterX);
      this.setDefaultCenterY(configuration.defaultCenterY);
      this.setDefaultZoomLevel(configuration.defaultZoomLevel);
    }
  }
}

/**
 * Return list of all aliases that were added to the model.
 */
MapModel.prototype.getAliases = function (params) {
  var self = this;
  return ServerConnector.getAliases({
    columns: "id,modelId",
    type: params.type,
    modelId: self.getId(),
    includedCompartmentIds: params.includedCompartmentIds,
    excludedCompartmentIds: params.excludedCompartmentIds

  }).then(function (lightAliases) {
    var identifiedElements = [];
    for (var i = 0; i < lightAliases.length; i++) {
      self.addAlias(lightAliases[i]);
      identifiedElements.push(new IdentifiedElement(lightAliases[i]));
    }
    return self.getByIdentifiedElements(identifiedElements, params.complete);
  });
};

/**
 * Returns {@link Alias} by identifier.
 *
 * @param {boolean} [complete]
 * @param {number} id
 *          identifier of the {@link Alias}
 * @returns {PromiseLike<Alias>} by identifier
 */
MapModel.prototype.getAliasById = function (id, complete) {
  var self = this;
  if (complete) {
    return this.getCompleteAliasById(id);
  }
  if (self._aliases[id] !== undefined) {
    return Promise.resolve(self._aliases[id]);
  } else {
    return self.getMissingElements({
      aliasIds: [id]
    }).then(function () {
      return self._aliases[id];
    });
  }
};

MapModel.prototype.getCompleteAliasById = function (id) {
  var self = this;
  if (self._aliases[id] !== undefined && self._aliases[id].isComplete()) {
    return Promise.resolve(self._aliases[id]);
  } else {
    return ServerConnector.getAliases({
      ids: id
    }).then(function (aliases) {
      if (self._aliases[id] === undefined) {
        self._aliases[id] = aliases[0];
      } else {
        self._aliases[id].update(aliases[0]);
      }
      return self._aliases[id];
    });
  }
};

/**
 * Returns {@link Reaction} by identifier.
 *
 * @param id
 *          identifier of the {@link Reaction}
 * @param {boolean} [complete]
 * @returns {Promise<Reaction>| PromiseLike<Reaction>} by identifier
 */
MapModel.prototype.getReactionById = function (id, complete) {
  var self = this;
  if (complete) {
    return this.getCompleteReactionById(id);
  }
  if (self._reactions[id] !== undefined) {
    return Promise.resolve(self._reactions[id]);
  } else {
    return self.getMissingElements({
      reactionIds: [id]
    }).then(function () {
      return self._reactions[id];
    });
  }
};

MapModel.prototype._getMissingReactionsElementIds = function (reactions) {
  var self = this;
  var result = [];
  var ids = [];
  for (var i = 0; i < reactions.length; i++) {
    var reaction = reactions[i];
    var elements = reaction.getElements();
    for (var j = 0; j < elements.length; j++) {
      var element = elements[j];
      if (!(element instanceof Alias)) {
        if (self._aliases[element] === undefined || !self._aliases[element].isComplete()) {
          if (!ids[element]) {
            ids[element] = true;
            result.push(element);
          }
        }
      }
    }
  }
  return result;
};

MapModel.prototype.getCompleteReactionById = function (id) {
  var self = this;
  if (self._reactions[id] instanceof Reaction && self._reactions[id].isComplete()) {
    return Promise.resolve(self._reactions[id]);
  } else {
    var result;
    return self.getReactionById(id).then(function (result) {
      var ids = self._getMissingReactionsElementIds([result]);
      return self.getMissingElements({
        aliasIds: ids,
        complete: true
      });
    }).then(function () {
      var i;
      result = self._reactions[id];
      for (i = 0; i < result.getReactants().length; i++) {
        if (!(result.getReactants()[i] instanceof Alias)) {
          result.getReactants()[i].setAlias(self._aliases[result.getReactants()[i].getAlias()]);
        }
      }
      for (i = 0; i < result.getProducts().length; i++) {
        if (!(result.getProducts()[i] instanceof Alias)) {
          result.getProducts()[i].setAlias(self._aliases[result.getProducts()[i].getAlias()]);
        }
      }
      for (i = 0; i < result.getModifiers().length; i++) {
        if (!(result.getModifiers()[i] instanceof Alias)) {
          result.getModifiers()[i].setAlias(self._aliases[result.getModifiers()[i].getAlias()]);
        }
      }
      return result;
    });
  }
};

MapModel.prototype.getMissingElements = function (elements) {
  var self = this;

  var aliasIds = [];
  var reactionIds = [];

  var i = 0;
  if (elements.reactionIds !== undefined) {
    reactionIds.push.apply(reactionIds, elements.reactionIds);
    for (i = 0; i < reactionIds.length; i++) {
      this._missingReactions[reactionIds[i]] = reactionIds[i];
    }

  }
  if (elements.aliasIds !== undefined) {
    aliasIds.push.apply(aliasIds, elements.aliasIds);
    for (i = 0; i < aliasIds.length; i++) {
      this._missingAliases[aliasIds[i]] = aliasIds[i];
    }
  }

  var reactionPromise = null;
  if (reactionIds.length > 0) {
    reactionPromise = ServerConnector.getReactions({
      ids: reactionIds,
      complete: elements.complete
    });
  }

  var aliasPromise = null;
  if (aliasIds.length > 0) {
    if (elements.complete) {
      aliasPromise = ServerConnector.getAliases({
        ids: aliasIds
      });
    } else {
      aliasPromise = ServerConnector.getAliases({
        ids: aliasIds,
        columns: "id,bounds,modelId"
      });

    }
  }

  var result = [];
  return Promise.all([reactionPromise, aliasPromise]).then(function (values) {
    var i;
    var reactions = values[0];
    var aliases = values[1];
    var ids = [];

    if (reactions !== null) {
      for (i = 0; i < reactions.length; i++) {
        var reaction = reactions[i];
        self.addReaction(reaction);
        result.push(reaction);
      }
      ids = self._getMissingReactionsElementIds(reactions);
    }
    if (aliases !== null) {
      for (i = 0; i < aliases.length; i++) {
        var alias = aliases[i];
        self.addAlias(alias);
        result.push(alias);
      }
    }
    if (ids.length > 0) {
      return self.getMissingElements({
        aliasIds: ids,
        complete: true
      });
    } else {
      return Promise.resolve([]);
    }
  }).then(function () {
    return result;
  });
};

/**
 * Adds information about alias.
 *
 * @param aliasData
 *          raw data about alias
 */
MapModel.prototype.addAlias = function (aliasData) {
  var alias = aliasData;
  if (!(aliasData instanceof Alias)) {
    alias = new Alias(aliasData);
  }
  if (this._aliases[alias.getId()] !== undefined) {
    this._aliases[alias.getId()].update(alias);
  } else {
    this._aliases[alias.getId()] = alias;
    if (this._missingAliases[alias.getId()] !== undefined) {
      this._missingAliases[alias.getId()] = null;
      delete this._missingAliases[alias.getId()];
    }
  }
};

/**
 * Adds information about reaction.
 *
 * @param reactionData
 *          raw data about reaction
 */
MapModel.prototype.addReaction = function (reactionData) {
  var reaction = null;
  if (reactionData instanceof Reaction) {
    reaction = reactionData;
  } else {
    reaction = new Reaction(reactionData);
  }
  if (this._reactions[reaction.getId()] !== undefined) {
    logger.warn("Reaction with id: " + reaction.getId() + " already exists");
  } else {
    this._reactions[reaction.getId()] = reaction;
    if (this._missingReactions[reaction.getId()] !== undefined) {
      this._missingReactions[reaction.getId()] = null;
      delete this._missingReactions[reaction.getId()];
    }
  }
};

/**
 * Returns {@link PointData} for a given point on the map.
 *
 * @param {Point} inputPoint
 *           where we are requesting data
 * @returns {PointData} for a given point on the map
 */
MapModel.prototype.getPointDataByPoint = function (inputPoint) {
  if (inputPoint instanceof Point) {
    var point = this._roundPoint(inputPoint);
    var id = this._pointToId(point);
    var result = this._pointsData[id];
    if (result === undefined) {
      result = new PointData(point, this.getId());
      this._pointsData[id] = result;
    }
    return result;
  } else {
    logger.warn("point must be of class: Point");
    return null;
  }
};

/**
 * Returns point where x and y coordinate are rounded to 2 decimal places.
 *
 * @param point
 *          input point
 * @returns {Point} point where x and y coordinate are rounded to 2
 *          decimal places
 */
MapModel.prototype._roundPoint = function (point) {
  var x = parseFloat(point.x).toFixed(2);
  var y = parseFloat(point.y).toFixed(2);
  return new Point(x, y);
};

/**
 * Transform point into string identifier.
 *
 * @param point
 *          {Point} to transform
 * @returns {String} string identifier for a given point
 */
MapModel.prototype._pointToId = function (point) {
  if (point instanceof Point) {
    return "(" + point.x + ", " + point.y + ")";
  } else {
    return point.replace(/ /g, '');
  }
};

MapModel.prototype.getId = function () {
  return this.id;
};

MapModel.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {number}
 */
MapModel.prototype.getWidth = function () {
  return this._width;
};

MapModel.prototype.setWidth = function (width) {
  this._width = width;
};

/**
 *
 * @returns {number}
 */
MapModel.prototype.getHeight = function () {
  return this._height;
};

MapModel.prototype.setHeight = function (height) {
  this._height = height;
};

MapModel.prototype.getName = function () {
  return this._name;
};

MapModel.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {number}
 */
MapModel.prototype.getMinZoom = function () {
  return this._minZoom;
};

MapModel.prototype.setMinZoom = function (minZoom) {
  this._minZoom = minZoom;
};

/**
 *
 * @returns {number|null}
 */
MapModel.prototype.getDefaultZoomLevel = function () {
  return this._defaultZoomLevel;
};

MapModel.prototype.setDefaultZoomLevel = function (defaultZoomLevel) {
  this._defaultZoomLevel = defaultZoomLevel;
};

/**
 *
 * @returns {null|number}
 */
MapModel.prototype.getDefaultCenterX = function () {
  return this._defaultCenterX;
};

MapModel.prototype.setDefaultCenterX = function (defaultCenterX) {
  this._defaultCenterX = defaultCenterX;
};

/**
 *
 * @returns {number|null}
 */
MapModel.prototype.getDefaultCenterY = function () {
  return this._defaultCenterY;
};

MapModel.prototype.setDefaultCenterY = function (defaultCenterY) {
  this._defaultCenterY = defaultCenterY;
};

MapModel.prototype.getSubmodelType = function () {
  return this._submodelType;
};

MapModel.prototype.setSubmodelType = function (submodelType) {
  this._submodelType = submodelType;
};

MapModel.prototype.getMaxZoom = function () {
  return this._maxZoom;
};

MapModel.prototype.setMaxZoom = function (maxZoom) {
  this._maxZoom = maxZoom;
};

/**
 *
 * @returns {number}
 */
MapModel.prototype.getTileSize = function () {
  return this._tileSize;
};

MapModel.prototype.getPictureSize = function () {
  return Math.max(this.getWidth(), this.getHeight());
};

MapModel.prototype.setTileSize = function (tileSize) {
  this._tileSize = tileSize;
};

MapModel.prototype.getByIdentifiedElement = function (ie, complete) {
  var self = this;
  if (ie.getType() === "ALIAS") {
    return self.getAliasById(ie.getId(), complete);
  } else if (ie.getType() === "REACTION") {
    return self.getReactionById(ie.getId(), complete);
  } else if (ie.getType() === "POINT") {
    var id = self._pointToId(ie.getId());
    var result = this._pointsData[id];
    if (result === undefined) {
      result = new PointData(ie);
      this._pointsData[id] = result;
    }
    return Promise.resolve(result);
  } else {
    throw new Error("Unknown type: " + ie.getType());
  }
};

MapModel.prototype.getByIdentifiedElements = function (identifiedElements, complete) {
  var self = this;
  var missingAliases = [];
  var missingReactions = [];

  for (var i = 0; i < identifiedElements.length; i++) {
    var ie = identifiedElements[i];
    if (!this.isAvailable(ie, complete)) {
      if (ie.getType() === "ALIAS") {
        missingAliases.push(ie.getId());
      } else if (ie.getType() === "REACTION") {
        missingReactions.push(ie.getId());
      } else {
        throw new Error("Unknown type " + ie);
      }
    }
  }

  return self.getMissingElements({
    aliasIds: missingAliases,
    reactionIds: missingReactions,
    complete: complete
  }).then(function () {
    var promises = [];
    for (var i = 0; i < identifiedElements.length; i++) {
      promises.push(self.getByIdentifiedElement(identifiedElements[i], complete));
    }
    return Promise.all(promises);
  });

};

MapModel.prototype.isAvailable = function (ie, complete) {
  var element;
  if (ie.getType() === "ALIAS") {
    element = this._aliases[ie.getId()];
  } else if (ie.getType() === "REACTION") {
    element = this._reactions[ie.getId()];
  } else if (ie.getType() === "POINT") {
    var id = this._pointToId(ie.getId());
    var result = this._pointsData[id];
    if (result === undefined) {
      result = new PointData(ie);
      this._pointsData[id] = result;
    }
    element = this._pointsData[id];
  } else {
    throw new Error("Unknown type: " + ie.getType() + "," + complete);
  }
  if (element === undefined) {
    return false;
  } else if (complete) {
    return element.isComplete();
  } else {
    return true;
  }
};

MapModel.prototype.getReactionsForElement = function (element, complete) {
  return this.getReactionsForElements([element], complete);
};

MapModel.prototype.getReactionsForElements = function (elements, complete) {
  var self = this;
  var ids = [];
  var i;
  for (i = 0; i < elements.length; i++) {
    ids.push(elements[i].getId());
  }
  var idString = ids.join();
  if (this._reactionsByParticipantElementId[idString]) {
    var reactions = self._reactionsByParticipantElementId[idString];
    if (!complete) {
      return Promise.resolve(reactions);
    } else {
      var promises = [];
      for (i = 0; i < reactions.length; i++) {
        promises.push(self.getCompleteReactionById(reactions[i].getId()));
      }
      return Promise.all(promises);
    }
  }

  var result = [];
  return ServerConnector.getReactions({
    modelId: self.getId(),
    participantId: ids
  }).then(function (reactions) {
    result = reactions;

    for (var i = 0; i < reactions.length; i++) {
      var reaction = reactions[i];
      var id = reaction.getId();
      if (self._reactions[id] === undefined) {
        self._reactions[id] = reaction;
      } else {
        self._reactions[id].update(reaction);
      }
    }
    var ids = self._getMissingReactionsElementIds(reactions);
    return self.getMissingElements({
      aliasIds: ids,
      complete: true
    });
  }).then(function () {
    var promises = [];
    for (var i = 0; i < result.length; i++) {
      promises.push(self.getCompleteReactionById(result[i].getId()));
    }
    return Promise.all(promises);
  });
};

MapModel.prototype.getCompartments = function () {
  var self = this;

  var promise = Promise.resolve();
  if (self._compartments === undefined) {
    promise = ServerConnector.getAliases({
      columns: "id,bounds,modelId",
      type: "Compartment",
      modelId: self.getId()
    }).then(function (compartments) {
      self._compartments = [];
      for (var i = 0; i < compartments.length; i++) {
        self._compartments.push(new IdentifiedElement(compartments[i]));
      }
    });
  }
  return promise.then(function () {
    return self.getByIdentifiedElements(self._compartments, true);
  });
};

MapModel.prototype.addSbmlFunction = function (sbmlFunction) {
  this._sbmlFunctions[sbmlFunction.getId()] = sbmlFunction;
};


MapModel.prototype.addSbmlParameter = function (sbmlParameter) {
  this._sbmlParameters[sbmlParameter.getId()] = sbmlParameter;
};


MapModel.prototype.getSbmlFunctionById = function (id) {
  var self = this;
  if (self._sbmlFunctions[id] !== undefined) {
    return Promise.resolve(self._sbmlFunctions[id]);
  } else {
    return ServerConnector.getSbmlFunction({modelId: self.getId(), functionId: id}).then(function (sbmlFunction) {
      self.addSbmlFunction(sbmlFunction);
      return sbmlFunction;
    })
  }
};

MapModel.prototype.getSbmlParameterById = function (id) {
  var self = this;
  if (self._sbmlParameters[id] !== undefined) {
    return Promise.resolve(self._sbmlParameters[id]);
  } else {
    return ServerConnector.getSbmlParameter({modelId: self.getId(), parameterId: id}).then(function (sbmlParameter) {
      self.addSbmlParameter(sbmlParameter);
      return sbmlParameter;
    })
  }
};


module.exports = MapModel;
