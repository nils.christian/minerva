"use strict";

var ObjectWithListeners = require('../../ObjectWithListeners');

function ModificationStateType(data, name) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  self.setAbbreviation(data.abbreviation);
  self.setName(name);
  self.setCommonName(data.commonName);
}

ModificationStateType.prototype = Object.create(ObjectWithListeners.prototype);
ModificationStateType.prototype.constructor = ModificationStateType;

ModificationStateType.prototype.setName = function(name) {
  this._name = name;
};

ModificationStateType.prototype.getName = function() {
  return this._name;
};

ModificationStateType.prototype.setAbbreviation= function(abbreviation) {
  this._abbreviation = abbreviation;
};

ModificationStateType.prototype.getAbbreviation = function() {
  return this._abbreviation;
};

ModificationStateType.prototype.setCommonName = function(commonName) {
  this._commonName = commonName;
};

ModificationStateType.prototype.getCommonName = function() {
  return this._commonName;
};

module.exports = ModificationStateType;
