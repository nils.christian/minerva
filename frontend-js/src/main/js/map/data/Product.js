"use strict";


function Product(javaObject) {
  this.setAlias(javaObject.aliasId);
  this.setStoichiometry(javaObject.stoichiometry);
}

Product.prototype.getAlias = function () {
  return this._alias;
};
Product.prototype.setAlias = function (alias) {
  this._alias = alias;
};
Product.prototype.getStoichiometry = function () {
  return this._stoichiometry;
};
Product.prototype.setStoichiometry = function (stoichiometry) {
  this._stoichiometry = stoichiometry;
};


module.exports = Product;
