"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

function UserPreferences(javaObject) {
  if (javaObject !== undefined) {
    this.setProjectUpload(javaObject["project-upload"]);
    this.setElementAnnotators(javaObject["element-annotators"]);
    this.setElementRequiredAnnotations(javaObject["element-required-annotations"]);
    this.setElementValidAnnotations(javaObject["element-valid-annotations"]);
    this.setAnnotatorsParameters(javaObject["annotators-parameters"]);
    this.setGuiPreferences(javaObject["gui-preferences"]);
  } else {
    this._projectUpload = {};
    this._elementAnnotators = {};
    this._elementRequiredAnnotations = {};
    this._elementValidAnnotations = {};
    this._annotatorsParameters = {};
    this._guiPreferences = {};
  }
}

UserPreferences.prototype.update = function (userPreferences) {
  var updateDict = function (originalDict, newDict) {
    for (var key in newDict) {
      if (newDict.hasOwnProperty(key)) {
        originalDict[key] = newDict[key];
      }
    }
  };

  updateDict(this._projectUpload, userPreferences.getProjectUpload());
  updateDict(this._elementAnnotators, userPreferences.getElementAnnotators);
  updateDict(this._elementValidAnnotations, userPreferences.getElementValidAnnotations());
  updateDict(this._elementRequiredAnnotations, userPreferences.getElementRequiredAnnotations());
  updateDict(this._annotatorsParameters, userPreferences.getAnnotatorsParameters());
  updateDict(this._guiPreferences, userPreferences.getGuiPreferences());
};

UserPreferences.prototype.setProjectUpload = function (projectUpload) {
  this._projectUpload = {
    autoResize: projectUpload["auto-resize"],
    validateMiriam: projectUpload["validate-miriam"],
    annotateModel: projectUpload["annotate-model"],
    cacheData: projectUpload["cache-data"],
    semanticZooming: projectUpload["semantic-zooming"],
    sbgn: projectUpload["sbgn"]
  };
};
UserPreferences.prototype.getAnnotatorsParameters = function () {
  return this._annotatorsParameters;
};
UserPreferences.prototype.setAnnotatorsParameters = function (annotatorsParameters) {
  this._annotatorsParameters = annotatorsParameters;
};
UserPreferences.prototype.getGuiPreferences = function () {
  return this._guiPreferences;
};
UserPreferences.prototype.setGuiPreference = function (key, value) {
  this._guiPreferences[key] = value;
};
UserPreferences.prototype.getGuiPreference = function (key, defaultValue) {
  var result = this._guiPreferences[key];
  if (result === undefined) {
    result = defaultValue;
  }
  return result;
};
UserPreferences.prototype.setGuiPreferences = function (guiPreferences) {
  this._guiPreferences = guiPreferences;
};
UserPreferences.prototype.getProjectUpload = function () {
  return this._projectUpload;
};
UserPreferences.prototype.setElementAnnotators = function (elementAnnotators) {
  this._elementAnnotators = elementAnnotators;
};
UserPreferences.prototype.getElementAnnotators = function (className) {
  var result = this._elementAnnotators[className];
  if (result === undefined) {
    this._elementAnnotators[className] = [];
    result = this._elementAnnotators[className];
  }
  return result;
};
UserPreferences.prototype.setElementRequiredAnnotations = function (elementRequiredAnnotations) {
  this._elementRequiredAnnotations = {};
  for (var key in elementRequiredAnnotations) {
    if (elementRequiredAnnotations.hasOwnProperty(key)) {
      var data = elementRequiredAnnotations[key];
      this._elementRequiredAnnotations[key] = {
        requiredAtLeastOnce: data["require-at-least-one"],
        list: data["annotation-list"]
      };
    }
  }
};
UserPreferences.prototype.getElementRequiredAnnotations = function (className) {
  var result = this._elementRequiredAnnotations[className];
  if (result === undefined) {
    result = {list: []};
  }
  return result;
};
UserPreferences.prototype.setElementValidAnnotations = function (elementValidAnnotations) {
  this._elementValidAnnotations = elementValidAnnotations;
};
UserPreferences.prototype.getElementValidAnnotations = function (className) {
  var result = this._elementValidAnnotations[className];
  if (result === undefined) {
    result = [];
  }
  return result;
};

UserPreferences.prototype.toExport = function () {
  var requiredAnnotations = {};
  for (var key in this._elementRequiredAnnotations) {
    if (this._elementRequiredAnnotations.hasOwnProperty(key)) {
      var data = this._elementRequiredAnnotations[key];
      requiredAnnotations[key] = {
        "require-at-least-one": data.requiredAtLeastOnce,
        "annotation-list": data.list
      };
    }
  }

  return {
    "project-upload": {
      "auto-resize": this._projectUpload.autoResize,
      "validate-miriam": this._projectUpload.validateMiriam,
      "annotate-model": this._projectUpload.annotateModel,
      "cache-data": this._projectUpload.cacheData,
      "sbgn": this._projectUpload.sbgn,
      "semantic-zooming": this._projectUpload.semanticZooming
    },
    "element-annotators": this._elementAnnotators,
    "element-valid-annotations": this._elementValidAnnotations,
    "element-required-annotations": requiredAnnotations,
    "annotators-parameters": this.getAnnotatorsParameters(),
    "gui-preferences": this.getGuiPreferences()
  };
};

module.exports = UserPreferences;
