"use strict";

var GeneVariant = require('./GeneVariant');

/**
 * Class representing alias visualized in a overlay.
 *
 * @param javaObject
 *          object de-serialized ajax query to the server side
 */
function LayoutAlias(javaObject) {
  this.setId(javaObject.idObject);
  this.setValue(javaObject.value);
  this.setColor(javaObject.color);
  this.setModelId(javaObject.modelId);
  this.setDescription(javaObject.description);
  if (javaObject.type === undefined) {
    this.setType(LayoutAlias.LIGHT);
  } else if (javaObject.type === LayoutAlias.GENETIC_VARIANT) {
    this.setType(LayoutAlias.GENETIC_VARIANT);
  } else if (javaObject.type === LayoutAlias.GENERIC) {
    this.setType(LayoutAlias.GENERIC);
  } else {
    throw new Error("Unknown type: " + javaObject.type);
  }

  this.setGeneVariants([]);
  if (javaObject.geneVariations !== undefined) {
    for (var i = 0; i < javaObject.geneVariations.length; i++) {
      this.addGeneVariant(new GeneVariant(javaObject.geneVariations[i]));
    }
  }
}

LayoutAlias.LIGHT = "LIGHT";
LayoutAlias.GENETIC_VARIANT = "GENETIC_VARIANT";
LayoutAlias.GENERIC = "GENERIC";

LayoutAlias.prototype.getId = function () {
  return this.id;
};

LayoutAlias.prototype.setId = function (id) {
  this.id = parseInt(id);
};

LayoutAlias.prototype.getModelId = function () {
  return this._modelId;
};

LayoutAlias.prototype.setModelId = function (modelId) {
  this._modelId = parseInt(modelId);
};

LayoutAlias.prototype.getValue = function () {
  return this.value;
};

LayoutAlias.prototype.getColor = function () {
  return this.color;
};

LayoutAlias.prototype.getType = function () {
  return this._type;
};

LayoutAlias.prototype.getGeneVariants = function () {
  return this._geneVariants;
};

LayoutAlias.prototype.setValue = function (newValue) {
  this.value = newValue;
};

LayoutAlias.prototype.setColor = function (newColor) {
  this.color = newColor;
};

LayoutAlias.prototype.setType = function (newType) {
  this._type = newType;
};

LayoutAlias.prototype.setGeneVariants = function (newGeneVariants) {
  this._geneVariants = newGeneVariants;
};

LayoutAlias.prototype.update = function (alias) {
  if (!(alias instanceof LayoutAlias)) {
    throw new Error("Unknown parameter type: " + alias);
  }

  this.setValue(alias.getValue());
  this.setColor(alias.getColor());
  this.setGeneVariants(alias.getGeneVariants());
  this.setType(alias.getType());
  this.setDescription(alias.getDescription());
};

LayoutAlias.prototype.addGeneVariant = function (geneVariant) {
  this._geneVariants.push(geneVariant);
};

LayoutAlias.prototype.getDescription = function () {
  return this._description;
};

LayoutAlias.prototype.setDescription = function (description) {
  this._description = description;
};

module.exports = LayoutAlias;
