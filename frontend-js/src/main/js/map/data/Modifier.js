"use strict";


function Modifier(javaObject) {
  this.setAlias(javaObject.aliasId);
  this.setStoichiometry(javaObject.stoichiometry);
}

Modifier.prototype.getAlias = function () {
  return this._alias;
};
Modifier.prototype.setAlias = function (alias) {
  this._alias = alias;
};
Modifier.prototype.getStoichiometry = function () {
  return this._stoichiometry;
};
Modifier.prototype.setStoichiometry = function (stoichiometry) {
  this._stoichiometry = stoichiometry;
};

module.exports = Modifier;
