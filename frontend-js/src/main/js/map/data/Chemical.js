"use strict";

var Annotation = require("./Annotation");
var TargettingStructure = require("./TargettingStructure");

/**
 *
 * @param javaObject
 * @constructor
 */
function Chemical(javaObject) {
  TargettingStructure.call(this, javaObject);
  if (javaObject !== undefined) {
    this.setReferences(javaObject.references);
    this.setDescription(javaObject.description);
    this.setSynonyms(javaObject.synonyms);

    this.setDirectEvidence(javaObject.directEvidence);
    this.setDirectEvidenceReferences(javaObject.directEvidenceReferences);
  }
}

Chemical.prototype = Object.create(TargettingStructure.prototype);
Chemical.prototype.constructor = Chemical;

/**
 *
 * @param {string} directEvidence
 */
Chemical.prototype.setDirectEvidence = function (directEvidence) {
  this._directEvidence = directEvidence;
};

/**
 *
 * @returns {string}
 */
Chemical.prototype.getDirectEvidence = function () {
  return this._directEvidence;
};

/**
 *
 * @param {Annotation[]} directEvidenceReferences
 */
Chemical.prototype.setDirectEvidenceReferences = function (directEvidenceReferences) {
  this._directEvidenceReferences = [];
  for (var i = 0; i < directEvidenceReferences.length; i++) {
    this._directEvidenceReferences.push(new Annotation(directEvidenceReferences[i]));
  }
};


/**
 *
 * @returns {Annotation[]}
 */
Chemical.prototype.getDirectEvidenceReferences = function () {
  return this._directEvidenceReferences;
};

/**
 *
 * @param {Annotation[]} references
 */
Chemical.prototype.setReferences = function (references) {
  this._references = [];
  for (var i = 0; i < references.length; i++) {
    this._references.push(new Annotation(references[i]));
  }
};

/**
 *
 * @returns {Annotation[]}
 */
Chemical.prototype.getReferences = function () {
  return this._references;
};

/**
 *
 * @param {string[]} synonyms
 */
Chemical.prototype.setSynonyms = function (synonyms) {
  this._synonyms = synonyms;
};

/**
 *
 * @returns {string[]}
 */
Chemical.prototype.getSynonyms = function () {
  return this._synonyms;
};

/**
 *
 * @param {string} description
 */
Chemical.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
Chemical.prototype.getDescription = function () {
  return this._description;
};

module.exports = Chemical;
