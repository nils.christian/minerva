"use strict";

function KineticLaw(jsonObject) {
  var self = this;
  self.setParameterIds(jsonObject.parameterIds);
  self.setFunctionIds(jsonObject.functionIds);
  self.setDefinition(jsonObject.definition);
  self.setMathMlPresentation(jsonObject.mathMlPresentation);
}

KineticLaw.prototype.setParameterIds = function (parameterIds) {
  this._parameterIds = parameterIds;
};
KineticLaw.prototype.getParameterIds = function () {
  return this._parameterIds;
};

KineticLaw.prototype.setFunctionIds = function (functionIds) {
  this._functionIds = functionIds;
};
KineticLaw.prototype.getFunctionIds = function () {
  return this._functionIds;
};

KineticLaw.prototype.setDefinition = function (definition) {
  this._definition = definition;
};
KineticLaw.prototype.getDefinition = function () {
  return this._definition;
};

KineticLaw.prototype.setMathMlPresentation = function (mathMlPresentation) {
  this._mathMlPresentation = mathMlPresentation;
};
KineticLaw.prototype.getMathMlPresentation = function () {
  return this._mathMlPresentation;
};

module.exports = KineticLaw;
