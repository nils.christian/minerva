"use strict";

var Alias = require('./Alias');
var LayoutAlias = require('./LayoutAlias');
var Reaction = require('./Reaction');
var LayoutReaction = require('./LayoutReaction');
var PointData = require('./PointData');
var Point = require('../canvas/Point');

var logger = require('../../logger');

/**
 * @typedef {Object} IdentifiedElementInput
 * @property {string|number} [id]
 * @property {string|number} [objectId]
 * @property {number} modelId
 * @property {string} type
 * @property {string} [icon]
 */

/**
 * This is object representing element tha should be visualized on the map. It's
 * very light and contains only the most important data. There are three types
 * of objects to visualize:
 * <ul>
 * <li>"ALIAS" - for {@link Alias} objects</li>
 * <li>"REACTION" - for {@link Reaction} objects</li>
 * <li>"POINT" - for any point on the map, the data connected to this kind of
 * objects are stored in {@link PointData}</li>
 * </ul>
 *
 * @param {BioEntity|LayoutAlias|LayoutReaction|PointData|IdentifiedElementInput} object
 * @constructor
 */
function IdentifiedElement(object) {
  this._visualizationdata = {};
  if (object instanceof Alias) {
    this.setId(object.getId());
    this.setModelId(object.getModelId());
    this.setType("ALIAS");
  } else if (object instanceof LayoutAlias) {
    this.setId(object.getId());
    this.setModelId(object.getModelId());
    this.setType("ALIAS");
  } else if (object instanceof Reaction) {
    this.setId(object.getId());
    this.setModelId(object.getModelId());
    this.setType("REACTION");
  } else if (object instanceof LayoutReaction) {
    this.setId(object.getId());
    this.setModelId(object.getModelId());
    this.setType("REACTION");
  } else if (object instanceof PointData) {
    this.setId(object.getId());
    this.setModelId(object.getModelId());
    this.setType("POINT");
  } else {
    // identifier of the object to visualize
    if (object.objectId === undefined) {
      this.setId(object.id);
    } else {
      this.setId(object.objectId);
    }
    // which marker should be used to show this object
    this.setIcon(object.icon);
    // on which model the element is located
    this.setModelId(object.modelId);
    // what kind of object we are talking about
    this.setType(object.type);
  }

  if (this.getType() === "POINT") {
    var tmp = this.getId();
    if (tmp.indexOf("Point2D.Double") >= 0) {
      tmp = tmp.replace("Point2D.Double", "");
      tmp = JSON.parse(tmp);
    } else {
      tmp = tmp.replace("(", "");
      tmp = tmp.replace(")", "");
      tmp = tmp.split(",");
    }
    var x = parseFloat(tmp[0]).toFixed(2);
    var y = parseFloat(tmp[1]).toFixed(2);
    this._point = new Point(x, y);
    this.setId("(" + x + "," + y + ")");
  } else if (this.getType() !== "ALIAS" && this.getType() !== "REACTION") {
    throw new Error("Unknown type of identified element: " + this.getType());
  }

  if (this.getId() === undefined) {
    throw new Error("Id not defined for element: " + object);
  }
}

/**
 * Returns point where it should be visualized when the type of object is
 * "POINT".
 *
 * @returns {Point|null} where it should be visualized when the
 *          type of object is "POINT".
 */
IdentifiedElement.prototype.getPoint = function () {
  if (this._point === undefined || this._point === null) {
    logger.warn("No point associated with IdentifiedElement");
    return null;
  } else {
    return this._point;
  }
};

/**
 * Returns element identifier.
 *
 * @returns {string|number} element identifier
 */
IdentifiedElement.prototype.getId = function () {
  return this.id;
};

IdentifiedElement.prototype.setId = function (id) {
  // some elements are identified by id that is not a number (like point on the
  // map)
  if (!isNaN(id)) {
    id = parseInt(id);
  }
  this.id = id;
};

/**
 *
 * @param {number|string} modelId
 */
IdentifiedElement.prototype.setModelId = function (modelId) {
  if (modelId === undefined || modelId === null) {
    throw new Error("ModelId is invalid");
  }
  this.modelId = parseInt(modelId);
};

/**
 * Returns model identifier where element is placed.
 *
 * @returns {number} model identifier
 */
IdentifiedElement.prototype.getModelId = function () {
  return this.modelId;
};

/**
 * Returns type of the element. For now only three types are allowed:
 * <ul>
 * <li>"ALIAS" - for {@link Alias} objects</li>
 * <li>"REACTION" - for {@link Reaction} objects</li>
 * <li>"POINT" - for any point on the map, the data connected to this kind of
 * objects are stored in {@link PointData}</li>
 * </ul>
 *
 * @returns {string} type of the element
 */
IdentifiedElement.prototype.getType = function () {
  return this.type;
};

IdentifiedElement.prototype.setType = function (type) {
  if (type === undefined || type === null) {
    throw new Error("Type not defined");
  }

  this.type = type.toUpperCase();
};

/**
 * Returns icon that should be used for visualization.
 *
 * @returns {string|undefined} icon that should be used for visualization
 */
IdentifiedElement.prototype.getIcon = function () {
  return this._visualizationdata._icon;
};

/**
 *
 * @param {string} icon
 */
IdentifiedElement.prototype.setIcon = function (icon) {
  this._visualizationdata._icon = icon;
};

/**
 * @returns {string|undefined}
 */
IdentifiedElement.prototype.getColor = function () {
  return this._visualizationdata._color;
};

IdentifiedElement.prototype.setColor = function (color) {
  this._visualizationdata._color = color;
};

IdentifiedElement.prototype.getOpacity = function () {
  return this._visualizationdata._opacity;
};

IdentifiedElement.prototype.setOpacity = function (opacity) {
  this._visualizationdata._opacity = opacity;
};

IdentifiedElement.prototype.getLineWeight = function () {
  return this._visualizationdata._lineWeight;
};

IdentifiedElement.prototype.setLineWeight = function (lineWeight) {
  this._visualizationdata._lineWeight = lineWeight;
};
IdentifiedElement.prototype.getLineOpacity = function () {
  return this._visualizationdata._lineOpacity;
};

IdentifiedElement.prototype.setLineOpacity = function (lineOpacity) {
  this._visualizationdata._lineOpacity = lineOpacity;
};
IdentifiedElement.prototype.getLineColor = function () {
  return this._visualizationdata._lineColor;
};

IdentifiedElement.prototype.setLineColor = function (lineColor) {
  this._visualizationdata._lineColor = lineColor;
};

IdentifiedElement.prototype.equals = function (argument) {
  if (argument instanceof IdentifiedElement) {
    return (this.getType() === argument.getType() && //
      this.getId() === argument.getId() && //
      this.getModelId() === argument.getModelId());
  } else {
    return false;
  }
};

/**
 *
 * @returns {string}
 */
IdentifiedElement.prototype.toString = function () {
  var self = this;
  return "[" + IdentifiedElement.prototype.constructor.name + "] " + self.getType() + " " + self.getId() + " (model: "
    + self.getModelId() + ")";
};

module.exports = IdentifiedElement;
