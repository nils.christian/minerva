"use strict";

/**
 * Class representing reaction visualized in a overlay.
 * 
 * @param javaObject
 *          object de-serialized from ajax query to the server side
 */
function LayoutReaction(javaObject) {
  this.setId(javaObject.idObject);
  this.setModelId(javaObject.modelId);
  this.setWidth(javaObject.width);
  this.setColor(javaObject.color);
  this.setReverse(javaObject.reverse);
}

LayoutReaction.prototype.getId = function() {
  return this.id;
};

LayoutReaction.prototype.setId = function(id) {
  this.id = parseInt(id);
};

LayoutReaction.prototype.getModelId = function() {
  return this._modelId;
};

LayoutReaction.prototype.setModelId = function(modelId) {
  this._modelId = parseInt(modelId);
};

LayoutReaction.prototype.setWidth = function(width) {
  this.width = width;
};

LayoutReaction.prototype.setColor = function(color) {
  this.color = color;
};

LayoutReaction.prototype.setReverse= function(reverse) {
  this.reverse = reverse;
};

LayoutReaction.prototype.getWidth = function() {
  return this.width;
};
LayoutReaction.prototype.getColor = function() {
  return this.color;
};
LayoutReaction.prototype.getReverse = function() {
  return this.reverse;
};

module.exports = LayoutReaction;
