"use strict";

/* exported logger */

var ObjectWithListeners = require('../../ObjectWithListeners');

var logger = require('../../logger');

function ProjectStatistics(data, configuration) {
  // call super constructor
  ObjectWithListeners.call(this);

  this.setReactionAnnotations(data.reactionAnnotations, configuration);
  this.setElementAnnotations(data.elementAnnotations, configuration);
  this.setPublicationCount(data.publications, configuration);
}

ProjectStatistics.prototype = Object.create(ObjectWithListeners.prototype);
ProjectStatistics.prototype.constructor = ProjectStatistics;

ProjectStatistics.prototype.setReactionAnnotations = function(reactionAnnotations, configuration) {
  var self = this;
  self._reactionAnnotations = [];
  for ( var key in reactionAnnotations) {
    if (reactionAnnotations.hasOwnProperty(key)) {
      var miriamType = configuration.getMiriamTypeByName(key);
      self._reactionAnnotations.push({
        miriamType : miriamType,
        count : reactionAnnotations[key]
      });
    }
  }
};

ProjectStatistics.prototype.setElementAnnotations = function(elementAnnotations, configuration) {
  var self = this;
  self._elementAnnotations = [];
  for ( var key in elementAnnotations) {
    if (elementAnnotations.hasOwnProperty(key)) {
      var miriamType = configuration.getMiriamTypeByName(key);
      self._elementAnnotations.push({
        miriamType : miriamType,
        count : elementAnnotations[key]
      });
    }
  }
};

ProjectStatistics.prototype.getElementAnnotations = function() {
  return this._elementAnnotations;
};
ProjectStatistics.prototype.getReactionAnnotations = function() {
  return this._reactionAnnotations;
};

ProjectStatistics.prototype.setPublicationCount = function(count) {
  this._publicationsCount = count;
};
ProjectStatistics.prototype.getPublicationCount = function() {
  return this._publicationsCount;
};

module.exports = ProjectStatistics;
