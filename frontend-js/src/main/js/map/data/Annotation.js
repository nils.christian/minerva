"use strict";

/* exported logger */

var Article = require("./Article");

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 * @typedef {Object} AnnotationOptions
 * @property {string} link
 * @property {number} id
 * @property {Article|ArticleOptions} [article]
 * @property {string} type
 * @property {string} resource
 * @property {string} [annotatorClassName]
 */

/**
 *
 * @param {AnnotationOptions|Annotation} javaObject
 * @constructor
 */
function Annotation(javaObject) {
  if (javaObject instanceof Annotation) {
    this.setLink(javaObject.getLink());
    this.setId(javaObject.getId());
    if (javaObject.getArticle() !== undefined) {
      this.setArticle(new Article(javaObject.getArticle()));
    }
    this.setType(javaObject.getType());
    this.setResource(javaObject.getResource());
    this.setAnnotatorClassName(javaObject.getAnnotatorClassName());
  } else {
    this.setLink(javaObject.link);
    this.setId(javaObject.id);
    if (javaObject.article !== undefined && javaObject.article !== null) {
      this.setArticle(new Article(javaObject.article));
    }
    this.setType(javaObject.type);
    this.setResource(javaObject.resource);
    this.setAnnotatorClassName(javaObject.annotatorClassName);
  }
}

/**
 *
 * @param {string} link
 */
Annotation.prototype.setLink = function (link) {
  this._link = link;
};

/**
 *
 * @returns {string}
 */
Annotation.prototype.getLink = function () {
  return this._link;
};

/**
 *
 * @param {number} id
 */
Annotation.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
Annotation.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {string} resource
 */
Annotation.prototype.setResource = function (resource) {
  if (resource === undefined) {
    throw new Error("resource cannot be undefined");
  }
  this._resource = resource;
};

/**
 *
 * @returns {string}
 */
Annotation.prototype.getResource = function () {
  return this._resource;
};

/**
 *
 * @param {string} type
 */
Annotation.prototype.setType = function (type) {
  if (type === undefined) {
    throw new Error("type cannot be undefined");
  }
  this._type = type;
};

/**
 *
 * @returns {string}
 */
Annotation.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {Article} article
 */
Annotation.prototype.setArticle = function (article) {
  this._article = article;
};

/**
 *
 * @returns {Article}
 */
Annotation.prototype.getArticle = function () {
  return this._article;
};

/**
 *
 * @param {string} annotatorClassName
 */
Annotation.prototype.setAnnotatorClassName = function (annotatorClassName) {
  this._annotatorClassName = annotatorClassName;
};

/**
 *
 * @returns {string|undefined}
 */
Annotation.prototype.getAnnotatorClassName = function () {
  return this._annotatorClassName;
};

module.exports = Annotation;
