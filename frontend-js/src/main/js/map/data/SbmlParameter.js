"use strict";

function SbmlParameter(jsonObject) {
  var self = this;
  self.setParameterId(jsonObject.parameterId);
  self.setValue(jsonObject.value);
  self.setGlobal(jsonObject.global);
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setUnitsId(jsonObject.unitsId);
}

SbmlParameter.prototype.setParameterId = function (parameterId) {
  this._parameterId = parameterId;
};
SbmlParameter.prototype.getParameterId = function () {
  return this._parameterId;
};

SbmlParameter.prototype.setUnitsId = function (unitsId) {
  this._unitsId = unitsId;
};
SbmlParameter.prototype.getUnitsId = function () {
  return this._unitsId;
};

SbmlParameter.prototype.setValue = function (value) {
  this._value = value;
};
SbmlParameter.prototype.getValue = function () {
  return this._value;
};

SbmlParameter.prototype.setGlobal = function (global) {
  this._global = global;
};
SbmlParameter.prototype.getGlobal = function () {
  return this._global;
};

SbmlParameter.prototype.setName = function (name) {
  this._name = name;
};
SbmlParameter.prototype.getName = function () {
  return this._name;
};

SbmlParameter.prototype.setId = function (id) {
  this._id = id;
};
SbmlParameter.prototype.getId = function () {
  return this._id;
};


module.exports = SbmlParameter;
