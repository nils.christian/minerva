"use strict";

/**
 * @typedef {Object} ArticleOptions
 * @property {string} title
 * @property {string[]} authors
 * @property {string} journal
 * @property {number} year
 * @property {string} link
 * @property {number} citationCount
 * @property {string} id
 */

/**
 *
 * @param {Article|ArticleOptions} javaObject
 * @constructor
 */
function Article(javaObject) {
  if (javaObject instanceof Article) {
    this.setTitle(javaObject.getTitle());
    this.setAuthors(javaObject.getAuthors());
    this.setJournal(javaObject.getJournal());
    this.setYear(javaObject.getYear());
    this.setLink(javaObject.getLink());
    this.setCitationCount(javaObject.getCitationCount());
    this.setId(javaObject.getId());
  } else {
    this.setTitle(javaObject.title);
    this.setAuthors(javaObject.authors);
    this.setJournal(javaObject.journal);
    this.setYear(javaObject.year);
    this.setLink(javaObject.link);
    this.setCitationCount(javaObject.citationCount);
    this.setId(javaObject.id);
  }
}

/**
 *
 * @param {string} title
 */
Article.prototype.setTitle = function (title) {
  this._title = title;
};

/**
 *
 * @returns {string}
 */
Article.prototype.getTitle = function () {
  return this._title;
};

/**
 *
 * @param {string} id
 */
Article.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {string[]}
 */
Article.prototype.getAuthors = function () {
  return this._authors;
};

/**
 *
 * @param {string[]} authors
 */
Article.prototype.setAuthors = function (authors) {
  this._authors = authors;
};

/**
 *
 * @param {string} id
 */
Article.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {string}
 */
Article.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {string} journal
 */
Article.prototype.setJournal = function (journal) {
  this._journal = journal;
};

/**
 *
 * @returns {string}
 */
Article.prototype.getJournal = function () {
  return this._journal;
};

/**
 *
 * @param {number} year
 */
Article.prototype.setYear = function (year) {
  this._year = year;
};

/**
 *
 * @returns {number}
 */
Article.prototype.getYear = function () {
  return this._year;
};

/**
 *
 * @param {string} link
 */
Article.prototype.setLink = function (link) {
  this._link = link;
};

/**
 *
 * @returns {string}
 */
Article.prototype.getLink = function () {
  return this._link;
};

/**
 *
 * @param {number} citationCount
 */
Article.prototype.setCitationCount = function (citationCount) {
  this._citationCount = citationCount;
};

/**
 *
 * @returns {number}
 */
Article.prototype.getCitationCount = function () {
  return this._citationCount;
};

module.exports = Article;
