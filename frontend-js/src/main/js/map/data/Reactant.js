"use strict";

var Alias = require("./Alias");


function Reactant(javaObject) {
  this.setAlias(javaObject.aliasId);
  this.setStoichiometry(javaObject.stoichiometry);
}

Reactant.prototype.getAlias = function () {
  return this._alias;
};

Reactant.prototype.setAlias = function (alias) {
  this._alias = alias;
};

Reactant.prototype.isComplete = function (alias) {
  return this.getAlias() instanceof Alias;
};
Reactant.prototype.getStoichiometry = function () {
  return this._stoichiometry;
};
Reactant.prototype.setStoichiometry = function (stoichiometry) {
  this._stoichiometry = stoichiometry;
};


module.exports = Reactant;
