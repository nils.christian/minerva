"use strict";

/* exported logger */

var Target = require("./Target");

var logger = require('../../logger');

function TargettingStructure(javaObject) {
  if (javaObject !== undefined) {
    this.setName(javaObject.name);
    this.setId(javaObject.id);
    this.setTargets(javaObject.targets);
  }
}

TargettingStructure.prototype.setName = function(name) {
  this._name = name;
};

TargettingStructure.prototype.getName = function() {
  return this._name;
};

TargettingStructure.prototype.setId = function(id) {
  this._id = id;
};

TargettingStructure.prototype.getId = function() {
  return this._id;
};

TargettingStructure.prototype.setTargets = function(targets) {
  this._targets = [];
  for (var i = 0; i < targets.length; i++) {
    this._targets.push(new Target(targets[i]));
  }
};

TargettingStructure.prototype.getTargets = function() {
  return this._targets;
};

TargettingStructure.prototype.getTargetsForIdentifiedElement = function(targetIdentifiedElement) {
  var result = [];
  this.getTargets().forEach(function(target) {
    var targetOk = false;
    target.getTargetElements().forEach(function(identifiedElement){
      if (targetIdentifiedElement.equals(identifiedElement)) {
        targetOk = true;
      }
    });
    if (targetOk) {
      result.push(target);
    }
  });
  return result;
};

module.exports = TargettingStructure;
