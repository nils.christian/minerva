"use strict";

/* exported logger */

var ObjectWithListeners = require('../../ObjectWithListeners');

var Annotation = require("./Annotation");
var DataOverlay = require("./DataOverlay");
var Model = require('./MapModel');

var Promise = require('bluebird');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Project|string|Object} [data]
 * @constructor
 */
function Project(data) {
  // call super constructor
  ObjectWithListeners.call(this);
  this.registerListenerType("onreload");

  this._dataOverlays = [];
  this._models = [];
  this._elementsPointingToSubmap = [];

  if (data !== undefined) {
    this.loadFromData(data);
  }
}

// this class inherits from ObjectWithListeners class where generic methods for
// listeners are set
Project.prototype = Object.create(ObjectWithListeners.prototype);
Project.prototype.constructor = Project;

/**
 *
 * @param {Project|string} data
 * @private
 */
Project.prototype.loadFromData = function (data) {
  var self = this;
  if (typeof data === "string") {
    // replace is due to some strange problem with serialization
    data = JSON.parse(data.replace(/\n/g, " "));
  }
  if (data instanceof Project) {
    self._update(data);
  } else {
    self.setId(parseInt(data.idObject));
    self.setProjectId(data.projectId);
    self.setDirectory(data.directory);
    self.setVersion(data.version);
    self.setName(data.name);
    self.setOverviewImages(data.overviewImageViews);
    self.setTopOverviewImage(data.topOverviewImage);
    self.setDisease(data.disease);
    self.setOrganism(data.organism);
    self.setStatus(data.status);
    self.setNotifyEmail(data.notifyEmail);
    self.setProgress(data.progress);
    self.setHasWarnings(data.warnings);
    self.setHasErrors(data.errors);
    self.setMapCanvasType(data.mapCanvasType);
  }
};

/**
 *
 * @param {Project} data
 * @returns {PromiseLike}
 */
Project.prototype.update = function (data) {
  this._update(data);
  return this.callListeners("onreload");
};

/**
 *
 * @param {Project} data
 * @private
 */
Project.prototype._update = function (data) {
  var self = this;
  self.setId(data.getId());
  self.setProjectId(data.getProjectId());
  self.setDirectory(data.getDirectory());
  self.setVersion(data.getVersion());
  self.setName(data.getName());
  self.setOverviewImages(data.getOverviewImages());
  self.setTopOverviewImage(data.getTopOverviewImage());
  self.setDisease(data.getDisease());
  self.setOrganism(data.getOrganism());
  self.setStatus(data.getStatus());
  self.setProgress(data.getProgress());
  self.setNotifyEmail(data.getNotifyEmail());
  self.setHasWarnings(data.hasWarnings());
  self.setHasErrors(data.hasErrors());
  self.setMapCanvasType(data.getMapCanvasType());

  if (data.getModels() !== undefined) {
    var models = data.getModels();
    for (var i = 0; i < models.length; i++) {
      self.addModel(new Model(models[i]));
    }
  }
};

/**
 *
 * @returns {number}
 */
Project.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {number|string} id
 */
Project.prototype.setId = function (id) {
  this._id = parseInt(id);
};

/**
 *
 * @returns {boolean}
 */
Project.prototype.hasErrors = function () {
  return this._hasErrors === true;
};

/**
 *
 * @param {boolean} hasErrors
 */
Project.prototype.setHasErrors = function (hasErrors) {
  this._hasErrors = hasErrors;
};

/**
 *
 * @returns {boolean}
 */
Project.prototype.hasWarnings = function () {
  return this._hasWarnings === true;
};

/**
 *
 * @param {boolean} hasWarnings
 */
Project.prototype.setHasWarnings = function (hasWarnings) {
  this._hasWarnings = hasWarnings;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getProjectId = function () {
  return this._projectId;
};

/**
 *
 * @param {string} projectId
 */
Project.prototype.setProjectId = function (projectId) {
  this._projectId = projectId;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getDirectory = function () {
  return this._directory;
};

/**
 *
 * @param {string} directory
 */
Project.prototype.setDirectory = function (directory) {
  this._directory = directory;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getVersion = function () {
  return this._version;
};

/**
 *
 * @param {string} version
 */
Project.prototype.setVersion = function (version) {
  this._version = version;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} name
 */
Project.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {MapModel[]}
 */
Project.prototype.getModels = function () {
  return this._models;
};

/**
 *
 * @param modelId
 * @returns {MapModel|null}
 */
Project.prototype.getModelById = function (modelId) {
  for (var i = 0; i < this._models.length; i++) {
    if (this._models[i].getId() === modelId) {
      return this._models[i];
    }
  }
  return null;
};

/**
 *
 * @param {MapModel[]} models
 */
Project.prototype.setModels = function (models) {
  this._models = models;
};

/**
 *
 * @param {MapModel} model
 */
Project.prototype.addModel = function (model) {
  this._models.push(model);
};

Project.prototype.getOverviewImages = function () {
  return this._overviewImages;
};
Project.prototype.setOverviewImages = function (overviewImages) {
  this._overviewImages = overviewImages;
};

Project.prototype.getTopOverviewImage = function () {
  return this._topOverviewImage;
};
Project.prototype.setTopOverviewImage = function (topOverviewImage) {
  this._topOverviewImage = topOverviewImage;
};

/**
 *
 * @returns {Annotation}
 */
Project.prototype.getDisease = function () {
  return this._disease;
};

/**
 *
 * @param {Object} disease
 */
Project.prototype.setDisease = function (disease) {
  if (disease !== undefined && disease !== null) {
    this._disease = new Annotation(disease);
  } else {
    this._disease = undefined;
  }
};

/**
 *
 * @returns {Annotation}
 */
Project.prototype.getOrganism = function () {
  return this._organism;
};

/**
 *
 * @param {Object} organism
 */
Project.prototype.setOrganism = function (organism) {
  if (organism !== undefined && organism !== null) {
    this._organism = new Annotation(organism);
  } else {
    this._organism = undefined;
  }
};

/**
 *
 * @returns {string}
 */
Project.prototype.getMapCanvasType = function () {
  return this._mapCanvasType;
};

/**
 *
 * @param {string} mapCanvasType
 */
Project.prototype.setMapCanvasType = function (mapCanvasType) {
  this._mapCanvasType = mapCanvasType;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getStatus = function () {
  return this._status;
};

/**
 *
 * @param {string} status
 */
Project.prototype.setStatus = function (status) {
  this._status = status;
};

/**
 *
 * @returns {number}
 */
Project.prototype.getProgress = function () {
  return this._progress;
};

/**
 *
 * @param {number} progress
 */
Project.prototype.setProgress = function (progress) {
  this._progress = progress;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getNotifyEmail = function () {
  return this._notifyEmail;
};

/**
 *
 * @param {string} notifyEmail
 */
Project.prototype.setNotifyEmail = function (notifyEmail) {
  this._notifyEmail = notifyEmail;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {boolean} [updateWhenExists=false]
 */
Project.prototype.addDataOverlay = function (overlay, updateWhenExists) {
  var overlayToAdd = null;
  if (overlay instanceof DataOverlay) {
    overlayToAdd = overlay;
  } else {
    overlayToAdd = new DataOverlay(overlay);
  }
  var object = this._dataOverlays[overlayToAdd.getId()];
  if (object === undefined) {
    this._dataOverlays[overlayToAdd.getId()] = overlayToAdd;
  } else {
    if (updateWhenExists) {
      object.update(overlay);
    } else {
      throw new Error("Overlay " + overlayToAdd.getId() + " already exists in a project " + this.getProjectId());
    }
  }
};

/**
 *
 * @param {DataOverlay} overlay
 */
Project.prototype.addOrUpdateDataOverlay = function (overlay) {
  this.addDataOverlay(overlay, true);
};


/**
 *
 * @param {DataOverlay[]} overlay
 */
Project.prototype.addOrUpdateDataOverlays = function (overlay) {
  this.addDataOverlays(overlay, true);
};

/**
 *
 * @param {DataOverlay[]} overlays
 * @param {boolean} updateWhenExists
 */
Project.prototype.addDataOverlays = function (overlays, updateWhenExists) {
  if (overlays === undefined) {
    logger.warn("Overlays are undefined...");
  } else {
    for (var i = 0; i < overlays.length; i++) {
      this.addDataOverlay(overlays[i], updateWhenExists);
    }
  }
};

/**
 *
 * @returns {DataOverlay[]}
 */
Project.prototype.getDataOverlays = function () {
  var result = [];
  for (var id in this._dataOverlays) {
    if (this._dataOverlays.hasOwnProperty(id)) {
      result.push(this._dataOverlays[id]);
    }
  }
  result.sort(function (dataOverlay1, dataOverlay2) {
    if (dataOverlay1.getOrder() < dataOverlay2.getOrder())
      return -1;
    if (dataOverlay1.getOrder() > dataOverlay2.getOrder())
      return 1;
    return 0;
  });
  return result;
};


/**
 * Returns data overlay for a given overlay identifier.
 *
 * @param {number} overlayId
 *          overlay identifier
 * @returns {PromiseLike<DataOverlay>} for a given overlay identifier
 */
Project.prototype.getDataOverlayById = function (overlayId) {
  var self = this;
  if (self._dataOverlays[overlayId] !== undefined) {
    return Promise.resolve(self._dataOverlays[overlayId]);
  } else {
    return ServerConnector.getOverlayById(overlayId).then(function (overlay) {
      self.addDataOverlay(overlay, false);
      return self._dataOverlays[overlayId];
    });
  }
};

/**
 * Returns promise with list of elements pointing to the submap.
 *
 * @param {number} modelId id of the submap
 * @returns {Promise} for a given overlay identifier
 */
Project.prototype.getElementsPointingToSubmap = function (modelId) {
  var self = this;
  if (self._elementsPointingToSubmap[modelId] !== undefined) {
    return Promise.resolve(self._elementsPointingToSubmap[modelId]);
  }
  var promise = Promise.resolve();
  if (self._submapConnections === undefined) {
    promise = ServerConnector.getSubmapConnections().then(function (data) {
      self._submapConnections = data;
    });
  }
  return promise.then(function () {
    self._elementsPointingToSubmap[modelId] = [];

    var queue = [modelId];

    while (queue.length > 0) {
      var id = queue.shift();
      for (var i = 0; i < self._submapConnections.length; i++) {
        var connection = self._submapConnections[i];
        if (connection.to === id) {
          var elementToAdd = connection.from;
          var alreadyAdded = false;
          for (var j = 0; j < self._elementsPointingToSubmap[modelId].length; j++) {
            var elementAlreadyAdded = self._elementsPointingToSubmap[modelId][j];
            if (elementToAdd.getId() === elementAlreadyAdded.getId()) {
              alreadyAdded = true;
            }
          }
          if (!alreadyAdded) {
            self._elementsPointingToSubmap[modelId].push(elementToAdd);
            queue.push(elementToAdd.getModelId());
          }
        }
      }
    }

    return self._elementsPointingToSubmap[modelId];
  });
};


module.exports = Project;
