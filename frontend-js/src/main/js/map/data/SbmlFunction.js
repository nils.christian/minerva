"use strict";

function SbmlFunction(jsonObject) {
  var self = this;
  self.setFunctionId(jsonObject.functionId);
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setArguments(jsonObject["arguments"]);
  self.setDefinition(jsonObject.definition);
  self.setMathMlPresentation(jsonObject.mathMlPresentation);
}

SbmlFunction.prototype.setFunctionId = function (functionId) {
  this._functionId = functionId;
};
SbmlFunction.prototype.getFunctionId = function () {
  return this._functionId;
};

SbmlFunction.prototype.setDefinition = function (definition) {
  this._definition = definition;
};
SbmlFunction.prototype.getDefinition = function () {
  return this._definition;
};

SbmlFunction.prototype.setMathMlPresentation = function (mathMlPresentation) {
  this._mathMlPresentation = mathMlPresentation;
};
SbmlFunction.prototype.getMathMlPresentation = function () {
  return this._mathMlPresentation;
};

SbmlFunction.prototype.setName = function (name) {
  this._name = name;
};
SbmlFunction.prototype.getName = function () {
  return this._name;
};
SbmlFunction.prototype.setArguments = function (args) {
  this._arguments = args;
};
SbmlFunction.prototype.getArguments = function () {
  return this._arguments;
};

SbmlFunction.prototype.setId = function (id) {
  this._id = id;
};
SbmlFunction.prototype.getId = function () {
  return this._id;
};


module.exports = SbmlFunction;
