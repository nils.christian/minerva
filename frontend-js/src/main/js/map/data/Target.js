"use strict";

/* exported logger */

var Annotation = require("./Annotation");
var IdentifiedElement = require('./IdentifiedElement');

var logger = require('../../logger');

function Target(javaObject) {
  this.setName(javaObject.name);
  this.setTargetElements(javaObject.targetElements);
  this.setTargetParticipants(javaObject.targetParticipants);
  this.setReferences(javaObject.references);
}

Target.prototype.setTargetElements = function(targetElements) {
  this._targetElements = [];
  for (var i = 0; i < targetElements.length; i++) {
    this._targetElements.push(new IdentifiedElement(targetElements[i]));
  }
  this.setIsVisible(this._targetElements.length > 0);
};

Target.prototype.getTargetElements = function() {
  return this._targetElements;
};

Target.prototype.setTargetParticipants = function(targetParticipants) {
  this._targetParticipants = [];
  for (var i = 0; i < targetParticipants.length; i++) {
    this._targetParticipants.push(new Annotation(targetParticipants[i]));
  }
};

Target.prototype.getTargetParticipants = function() {
  return this._targetParticipants;
};

Target.prototype.setName = function(name) {
  this._name = name;
};

Target.prototype.getName = function() {
  return this._name;
};

Target.prototype.setIsVisible = function(visible) {
  this._isVisible = visible;
};

Target.prototype.isVisible = function() {
  return this._isVisible;
};

Target.prototype.setReferences = function(references) {
  this._references = [];
  for (var i = 0; i < references.length; i++) {
    this._references.push(new Annotation(references[i]));
  }
};

Target.prototype.getReferences = function() {
  return this._references;
};

module.exports = Target;
