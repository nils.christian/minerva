"use strict";

function Mesh(jsonObject) {
  var self = this;
  self.setSynonyms(jsonObject.synonyms);
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setDescription(jsonObject.description);
}

Mesh.prototype.setSynonyms = function (synonyms) {
  this._synonyms = synonyms;
};
Mesh.prototype.getSynonyms = function () {
  return this._synonyms;
};
Mesh.prototype.setName = function (name) {
  this._name = name;
};
Mesh.prototype.getName = function () {
  return this._name;
};
Mesh.prototype.setId = function (id) {
  this._id = id;
};
Mesh.prototype.getId = function () {
  return this._id;
};
Mesh.prototype.setDescription = function (description) {
  this._description = description;
};
Mesh.prototype.getDescription = function () {
  return this._description;
};

module.exports = Mesh;
