"use strict";

var ObjectWithListeners = require('../../ObjectWithListeners');

function MiriamType(data, name) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  self.setUris(data.uris);
  self.setHomepage(data.homepage);
  self.setName(name);
  self.setCommonName(data.commonName);
  self.setRegistryIdentifier(data.registryIdentifier);
}

MiriamType.prototype = Object.create(ObjectWithListeners.prototype);
MiriamType.prototype.constructor = MiriamType;

MiriamType.prototype.setUris = function(uris) {
  this._uris = uris;
};

MiriamType.prototype.getUris = function() {
  return this._uris;
};

MiriamType.prototype.setHomepage = function(homepage) {
  this._homepage = homepage;
};

MiriamType.prototype.getHomepage = function() {
  return this._homepage;
};

MiriamType.prototype.setName = function(name) {
  this._name = name;
};

MiriamType.prototype.getName = function() {
  return this._name;
};

MiriamType.prototype.setCommonName = function(commonName) {
  this._commonName = commonName;
};

MiriamType.prototype.getCommonName = function() {
  return this._commonName;
};

MiriamType.prototype.setRegistryIdentifier = function(registryIdentifier) {
  this._registryIdentifier = registryIdentifier;
};

MiriamType.prototype.getRegistryIdentifier = function() {
  return this._registryIdentifier;
};

module.exports = MiriamType;
