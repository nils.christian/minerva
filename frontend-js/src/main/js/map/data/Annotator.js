"use strict";

/* exported logger */

var Article = require("./Article");

var logger = require('../../logger');

function Annotator(javaObject, configuration) {
  var self = this;
  if (javaObject instanceof Annotator) {
    self.setClassName(javaObject.getClassName());
    this._elementTypes = javaObject.getElementTypes();
    self.setName(javaObject.getName());
    self.setDescription(javaObject.getDescription());
    self.setParametersDefinitions(javaObject.getParametersDefinitions());
    self.setUrl(javaObject.getUrl());
  } else {
    self.setClassName(javaObject.className);
    self.setElementTypes(javaObject.elementClassNames, configuration);
    self.setName(javaObject.name);
    self.setDescription(javaObject.description);
    self.setParametersDefinitions(javaObject.parametersDefinitions);
    self.setUrl(javaObject.url);
  }
}

Annotator.prototype.setClassName = function (className) {
  this._className = className;
};

Annotator.prototype.getClassName = function () {
  return this._className;
};

Annotator.prototype.setName = function (name) {
  this._name = name;
};

Annotator.prototype.getName = function () {
  return this._name;
};

Annotator.prototype.setDescription = function (description) {
  this._description = description;
};

Annotator.prototype.getDescription = function () {
  return this._description;
};

Annotator.prototype.setUrl = function (url) {
  this._url = url;
};

Annotator.prototype.getUrl = function () {
  return this._url;
};

Annotator.prototype.getParametersDefinitions = function () {
  return this._parametersDefinitions;
};

Annotator.prototype.setParametersDefinitions = function (parametersDefinitions) {
  this._parametersDefinitions = parametersDefinitions;
};

Annotator.prototype.setElementTypes = function (elementTypesClassNames, configuration) {
  this._elementTypes = [];
  var typeByClassName = {};
  var types = configuration.getElementTypes();
  var i, type;
  for (i = 0; i < types.length; i++) {
    type = types[i];
    typeByClassName[type.className] = type;
  }
  types = configuration.getReactionTypes();
  for (i = 0; i < types.length; i++) {
    type = types[i];
    typeByClassName[type.className] = type;
  }

  for (i = 0; i < elementTypesClassNames.length; i++) {
    type = typeByClassName[elementTypesClassNames[i]];
    if (type !== undefined) {
      this._elementTypes.push(type)
    } else {
      throw new Error("Unknown elementType: " + elementTypesClassNames[i]);
    }
  }
};

Annotator.prototype.getElementTypes = function () {
  return this._elementTypes;
};


module.exports = Annotator;
