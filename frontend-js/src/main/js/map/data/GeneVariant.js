"use strict";

/**
 *
 * @param {Object} javaObject
 * @param {number} javaObject.position
 * @param {string} javaObject.originalDna
 * @param {string} javaObject.modifiedDna
 * @param {string} javaObject.referenceGenomeType
 * @param {string} javaObject.referenceGenomeVersion
 * @param {string} javaObject.contig
 * @param {null|number} [javaObject.allelFrequency]
 * @param {null|string} [javaObject.variantIdentifier]
 *
 * @constructor
 */
function GeneVariant(javaObject) {
  this.setPosition(javaObject.position);
  this.setOriginalDna(javaObject.originalDna);
  this.setModifiedDna(javaObject.modifiedDna);
  this.setReferenceGenomeType(javaObject.referenceGenomeType);
  this.setReferenceGenomeVersion(javaObject.referenceGenomeVersion);
  this.setContig(javaObject.contig);
  this.setAllelFrequency(javaObject.allelFrequency);
  this.setVariantIdentifier(javaObject.variantIdentifier);
}

/**
 *
 * @param {number} position
 */
GeneVariant.prototype.setPosition = function (position) {
  this._position = position;
};

/**
 *
 * @returns {number}
 */
GeneVariant.prototype.getPosition = function () {
  return this._position;
};

/**
 *
 * @param {string} originalDna
 */
GeneVariant.prototype.setOriginalDna = function (originalDna) {
  this._original = originalDna;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getOriginalDna = function () {
  return this._original;
};

/**
 *
 * @param {string} modifiedDna
 */
GeneVariant.prototype.setModifiedDna = function (modifiedDna) {
  this._modifiedDna = modifiedDna;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getModifiedDna = function () {
  return this._modifiedDna;
};

/**
 *
 * @param {string} contig
 */
GeneVariant.prototype.setContig = function (contig) {
  this._contig = contig;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getContig = function () {
  return this._contig;
};

/**
 *
 * @param {number} allelFrequency
 */
GeneVariant.prototype.setAllelFrequency = function (allelFrequency) {
  if (allelFrequency === null) {
    this._allelFrequency = undefined;
  } else {
    this._allelFrequency = allelFrequency;
  }
};

/**
 *
 * @returns {number}
 */
GeneVariant.prototype.getAllelFrequency = function () {
  return this._allelFrequency;
};

/**
 *
 * @param {string} variantIdentifier
 */
GeneVariant.prototype.setVariantIdentifier = function (variantIdentifier) {
  if (variantIdentifier === null) {
    this._variantIdentifier = undefined;
  } else {
    this._variantIdentifier = variantIdentifier;
  }
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getVariantIdentifier = function () {
  return this._variantIdentifier;
};

/**
 *
 * @param {string} referenceGenomeType
 */
GeneVariant.prototype.setReferenceGenomeType = function (referenceGenomeType) {
  this._referenceGenomeType = referenceGenomeType;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getReferenceGenomeType = function () {
  return this._referenceGenomeType;
};

/**
 *
 * @param {string} referenceGenomeVersion
 */
GeneVariant.prototype.setReferenceGenomeVersion = function (referenceGenomeVersion) {
  this._referenceGenomeVersion = referenceGenomeVersion;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getReferenceGenomeVersion = function () {
  return this._referenceGenomeVersion;
};

module.exports = GeneVariant;
