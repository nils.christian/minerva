"use strict";

var logger = require('./../logger');

/**
 * Object representing information needed for constructing {@link CustomMap}
 * objects.
 *
 * @param {Object|CustomMapOptions} params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {string} params.projectId
 * @param {ServerConnector} params.serverConnector
 * @param {boolean} [params.bigLogo]
 * @param {boolean} [params.debug]
 * @param {Object} [params.plugins]
 *
 * @constructor
 */
function CustomMapOptions(params) {
  if (params.element === undefined) {
    throw new Error("element must be defined");
  }
  this.setElement(params.element);
  this.setConfiguration(params.configuration);
  this.setProject(params.project);
  this.setProjectId(params.projectId);
  this.setServerConnector(params.serverConnector);

  if (params.bigLogo !== undefined) {
    if (typeof params.bigLogo === "boolean") {
      this._bigLogo = params.bigLogo;
    } else {
      logger.warn("bigLogo must be of type boolean");
      this._bigLogo = false;
    }
  } else {
    this._bigLogo = false;
  }

  this.setDebug(params.debug);
  this.setPlugins(params.plugins);

}

/**
 *
 * @returns {boolean}
 */
CustomMapOptions.prototype.isBigLogo = function () {
  return this._bigLogo;
};

/**
 *
 * @returns {Project}
 */
CustomMapOptions.prototype.getProject = function () {
  return this._project;
};

/**
 *
 * @param {Project} project
 */
CustomMapOptions.prototype.setProject = function (project) {
  this._project = project;
};

/**
 *
 * @returns {ServerConnector}
 */
CustomMapOptions.prototype.getServerConnector = function () {
  return this._serverConnector;
};

/**
 *
 * @param {ServerConnector} serverConnector
 */
CustomMapOptions.prototype.setServerConnector = function (serverConnector) {
  this._serverConnector = serverConnector;
};

CustomMapOptions.prototype.getPlugins = function () {
  return this._plugins;
};
CustomMapOptions.prototype.setPlugins = function (plugins) {
  if (plugins === undefined) {
    plugins = [];
  }
  this._plugins = plugins;
};

/**
 *
 * @returns {Configuration}
 */
CustomMapOptions.prototype.getConfiguration = function () {
  return this._configuration;
};

/**
 *
 * @param {Configuration} configuration
 */
CustomMapOptions.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

/**
 *
 * @returns {string}
 */
CustomMapOptions.prototype.getProjectId = function () {
  if (this.getProject() !== undefined) {
    return this.getProject().getProjectId();
  } else {
    return this._projectId;
  }
};

/**
 *
 * @param {string} projectId
 */
CustomMapOptions.prototype.setProjectId = function (projectId) {
  this._projectId = projectId;
};

/**
 *
 * @param {boolean} debug
 */
CustomMapOptions.prototype.setDebug = function (debug) {
  if (debug !== undefined) {
    if (typeof debug !== "boolean") {
      logger.warn("Params must be boolean");
    }
    this._debug = debug;
  }
};

/**
 *
 * @returns {boolean}
 */
CustomMapOptions.prototype.isDebug = function () {
  return this._debug === true;
};

/**
 *
 * @param {HTMLElement} element
 */
CustomMapOptions.prototype.setElement = function (element) {
  this._element = element;
};

/**
 *
 * @returns {HTMLElement}
 */
CustomMapOptions.prototype.getElement = function () {
  return this._element;
};

module.exports = CustomMapOptions;
