"use strict";

var logger = require('./../logger');
var DataOverlay = require('./data/DataOverlay');
var TextDecoder = require('text-encoding').TextDecoder;

function OverlayParser() {
}

/**
 *
 * @param {string| Uint8Array|ArrayBuffer} content
 * @returns {DataOverlay}
 */
OverlayParser.prototype.parse = function (content) {
  if (content instanceof Uint8Array || content instanceof ArrayBuffer) {
    content = new TextDecoder("UTF8").decode(content);
  }
  var data = {content: content};
  var lines = content.split("\n");
  for (var i = 0; i < lines.length; i++) {
    var line = lines[i];
    if (line.indexOf("#") === 0) {
      if (line.indexOf("=") > 0) {
        var name = line.substring(1, line.indexOf("=")).trim();
        var value = line.substring(line.indexOf("=") + 1).trim();
        if (name === "NAME") {
          data.name = value;
        } else if (name === "DESCRIPTION") {
          data.description = value;
        } else if (name === "TYPE") {
          data.type = value;
        }
      } else {
        logger.warn("Invalid overlay header line: " + line);
      }
    } else {
      break;
    }
  }

  return new DataOverlay(data);
};


module.exports = OverlayParser;
