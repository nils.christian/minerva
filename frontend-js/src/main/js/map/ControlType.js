"use strict";

var ControlType = {
  SUBMAP_DIALOGS : "SUBMAP_DIALOGS",
  LOGO_IMG : "LOGO_IMG",
  LOGO_2_IMG : "LOGO_2_IMG"
};

module.exports = ControlType;
