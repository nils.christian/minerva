"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

var Annotator = require('./map/data/Annotator');
var ConfigurationType = require('./ConfigurationType');
var ConfigurationOption = require('./ConfigurationOption');
var MiriamType = require('./map/data/MiriamType');
var PrivilegeType = require('./map/data/PrivilegeType');
var ModificationStateType = require('./map/data/ModificationStateType');

function Configuration(json) {
  var self = this;

  if (json instanceof Configuration) {
    self.update(json);
  } else {
    var i;

    self._options = [];

    for (i = 0; i < json.options.length; i++) {
      var conf = json.options[i];
      var type = conf.type;
      self.setOption(type, new ConfigurationOption(conf));
    }

    var legendFiles = [];
    if (self.getOption("LEGEND_FILE_1") !== undefined) {
      legendFiles.push(self.getOption("LEGEND_FILE_1"));
    }
    if (self.getOption("LEGEND_FILE_2") !== undefined) {
      legendFiles.push(self.getOption("LEGEND_FILE_2"));
    }
    if (self.getOption("LEGEND_FILE_3") !== undefined) {
      legendFiles.push(self.getOption("LEGEND_FILE_3"));
    }
    if (self.getOption("LEGEND_FILE_4") !== undefined) {
      legendFiles.push(self.getOption("LEGEND_FILE_4"));
    }
    self.setOption(ConfigurationType.LEGEND_FILES, legendFiles);

    var overlayTypes = [];
    for (i = 0; i < json.overlayTypes.length; i++) {
      overlayTypes.push(json.overlayTypes[i].name);
    }
    self.setOverlayTypes(overlayTypes);

    self.setImageConverters(json.imageFormats);
    self.setModelConverters(json.modelFormats);
    self.setElementTypes(json.elementTypes);
    self.setReactionTypes(json.reactionTypes);
    self.setMiriamTypes(json.miriamTypes);
    self.setMapTypes(json.mapTypes);
    self.setMapCanvasTypes(json.mapCanvasTypes);
    self.setModificationStateTypes(json.modificationStateTypes);
    self.setPrivilegeTypes(json.privilegeTypes);
    self.setAnnotators(json.annotators);
    self.setVersion(json.version);
    self.setBuildDate(json.buildDate);
    self.setGitHash(json.gitHash);
    self.setPluginsData(json.plugins);
  }
}

Configuration.prototype.setVersion = function (version) {
  this._version = version;
};
Configuration.prototype.getVersion = function () {
  return this._version;
};

Configuration.prototype.setBuildDate = function (buildDate) {
  this._buildDate = buildDate;
};
Configuration.prototype.getBuildDate = function () {
  return this._buildDate;
};

Configuration.prototype.setGitHash = function (gitHash) {
  this._gitHash = gitHash;
};
Configuration.prototype.getGitHash = function () {
  return this._gitHash;
};

/**
 *
 * @param {string} type
 * @param {ConfigurationOption} value
 */
Configuration.prototype.setOption = function (type, value) {
  this._options[type] = value;
};

/**
 *
 * @param {string} type
 * @returns {ConfigurationOption}
 */
Configuration.prototype.getOption = function (type) {
  return this._options[type];
};

/**
 *
 * @returns {ConfigurationOption[]}
 */
Configuration.prototype.getOptions = function () {
  var self = this;
  var result = [];
  for (var key in self._options) {
    if (self._options.hasOwnProperty(key) && self._options[key] instanceof ConfigurationOption) {
      result.push(self._options[key]);
    }
  }
  return result;
};

/**
 *
 * @param {string[]} overlayTypes
 */
Configuration.prototype.setOverlayTypes = function (overlayTypes) {
  this._overlayTypes = overlayTypes;
};

/**
 *
 * @returns {string[]}
 */
Configuration.prototype.getOverlayTypes = function () {
  return this._overlayTypes;
};

Configuration.prototype.setImageConverters = function (imageConverters) {
  this._imageConverters = imageConverters;
};
Configuration.prototype.getImageConverters = function () {
  return this._imageConverters;
};

Configuration.prototype.setModelConverters = function (modelConverters) {
  this._modelConverters = modelConverters;
};
Configuration.prototype.getModelConverters = function () {
  return this._modelConverters;
};

Configuration.prototype.setElementTypes = function (elementTypes) {
  this._elementTypes = elementTypes;
};
Configuration.prototype.getElementTypes = function () {
  return this._elementTypes;
};
Configuration.prototype.getElementTypeNames = function () {
  var result = [];
  for (var i = 0; i < this._elementTypes.length; i++) {
    result.push(this._elementTypes[i].name);
  }
  return result;
};

Configuration.prototype.getParentType = function (elementType) {
  var i;
  for (i = 0; i < this._elementTypes.length; i++) {
    if (this._elementTypes[i].className === elementType.parentClass) {
      return this._elementTypes[i];
    }
  }
  for (i = 0; i < this._reactionTypes.length; i++) {
    if (this._reactionTypes[i].className === elementType.parentClass) {
      return this._reactionTypes[i];
    }
  }
  return null;
};

Configuration.prototype.getSimpleElementTypeNames = function () {
  var classesToBeExcluded = {};
  var i;
  for (i = 0; i < this._elementTypes.length; i++) {
    classesToBeExcluded[this._elementTypes[i].parentClass] = true;
  }
  var result = [];
  for (i = 0; i < this._elementTypes.length; i++) {
    if (classesToBeExcluded[this._elementTypes[i].className] === undefined) {
      result.push(this._elementTypes[i].name);
    }
  }
  return result;
};


Configuration.prototype.setReactionTypes = function (reactionTypes) {
  this._reactionTypes = reactionTypes;
};
Configuration.prototype.getReactionTypes = function () {
  return this._reactionTypes;
};

Configuration.prototype.setMiriamTypes = function (miriamTypes) {
  this._miriamTypes = [];
  for (var key in miriamTypes) {
    if (miriamTypes.hasOwnProperty(key)) {
      var typeData = miriamTypes[key];
      this._miriamTypes.push(new MiriamType(typeData, key));
    }
  }
  this._miriamTypes.sort(function compare(a, b) {
      if (a.getCommonName() < b.getCommonName())
        return -1;
      if (a.getCommonName() > b.getCommonName())
        return 1;
      return 0;
    }
  );
};

Configuration.prototype.setMapTypes = function (mapTypes) {
  this._mapTypes = mapTypes;
};
Configuration.prototype.getMapTypes = function () {
  return this._mapTypes;
};

/**
 *
 * @param {Array}mapCanvasTypes
 */
Configuration.prototype.setMapCanvasTypes = function (mapCanvasTypes) {
  this._mapCanvasTypes = mapCanvasTypes;
};

/**
 *
 * @returns {Array}
 */
Configuration.prototype.getMapCanvasTypes = function () {
  return this._mapCanvasTypes;
};

Configuration.prototype.setPrivilegeTypes = function (privilegeTypes) {
  this._privilegeTypes = [];
  for (var key in privilegeTypes) {
    if (privilegeTypes.hasOwnProperty(key)) {
      var typeData = privilegeTypes[key];
      this._privilegeTypes.push(new PrivilegeType(typeData, key));
    }
  }
};

Configuration.prototype.getPrivilegeTypes = function () {
  return this._privilegeTypes;
};

Configuration.prototype.getPrivilegeType = function (name) {
  var self = this;
  var privilegeTypes = self.getPrivilegeTypes();
  for (var i = 0; i < privilegeTypes.length; i++) {
    var type = privilegeTypes[i];
    if (type.getName() === name) {
      return type;
    }
  }
  return null;
};

Configuration.prototype.getMiriamTypeByName = function (name) {
  var self = this;
  for (var i = 0; i < self._miriamTypes.length; i++) {
    var miriamType = self._miriamTypes[i];
    if (miriamType.getName() === name) {
      return miriamType;
    }
  }
  return null;
};

Configuration.prototype.getMiriamTypes = function () {
  return this._miriamTypes;
};

Configuration.prototype.setModificationStateTypes = function (modificationStateTypes) {
  this._modificationStateTypes = [];
  for (var key in modificationStateTypes) {
    if (modificationStateTypes.hasOwnProperty(key)) {
      var typeData = modificationStateTypes[key];
      this._modificationStateTypes.push(new ModificationStateType(typeData, key));
    }
  }
};
Configuration.prototype.getModificationStateTypes = function () {
  return this._modificationStateTypes;
};
Configuration.prototype.getModificationStateTypeByName = function (name) {
  var self = this;
  for (var i = 0; i < self._modificationStateTypes.length; i++) {
    var modificationStateType = self._modificationStateTypes[i];
    if (modificationStateType.getName() === name) {
      return modificationStateType;
    }
  }
  return null;
};

Configuration.prototype.setAnnotators = function (annotators) {
  this._annotators = [];
  for (var key in annotators) {
    if (annotators.hasOwnProperty(key)) {
      var annotator = annotators[key];
      this._annotators.push(new Annotator(annotator, this));
    }
  }
};

Configuration.prototype.getPluginsData = function () {
  return this._pluginsData;
};
Configuration.prototype.setPluginsData = function (pluginsData) {
  this._pluginsData = pluginsData;
};

Configuration.prototype.getAnnotators = function () {
  return this._annotators;
};

Configuration.prototype.getElementAnnotators = function (type) {
  if (type === undefined) {
    return this._annotators;
  }
  var result = [];
  for (var i = 0; i < this._annotators.length; i++) {
    var annotator = this._annotators[i];
    var ok = false;
    var elementTypes = annotator.getElementTypes();
    for (var j = 0; j < elementTypes.length; j++) {
      var elementType = elementTypes[j];
      var checkedType = type;
      while (checkedType !== null) {
        if (elementType.className === checkedType.className) {
          ok = true;
          checkedType = null;
        } else {
          checkedType = this.getParentType(checkedType);
        }
      }
    }
    if (ok) {
      result.push(annotator);
    }
  }
  return result;
};

function createCopy(original) {
  if (Object.prototype.toString.call(original) === '[object Array]') {
    var result = [];
    for (var i = 0; i < original.length; i++) {
      result.push(createCopy(original[i]));
    }
    return result;
  } else {
    return jQuery.extend({}, original);
  }
}

Configuration.prototype.update = function (original) {
  var self = this;
  var i;
  self._options = [];
  for (i = 0; i < original.getOptions().length; i++) {
    var conf = original.getOptions()[i];
    var type = conf.getType();
    self.setOption(type, new ConfigurationOption(conf));
  }

  var legendFiles = [];
  if (self.getOption("LEGEND_FILE_1") !== undefined) {
    legendFiles.push(self.getOption("LEGEND_FILE_1"));
  }
  if (self.getOption("LEGEND_FILE_2") !== undefined) {
    legendFiles.push(self.getOption("LEGEND_FILE_2"));
  }
  if (self.getOption("LEGEND_FILE_3") !== undefined) {
    legendFiles.push(self.getOption("LEGEND_FILE_3"));
  }
  if (self.getOption("LEGEND_FILE_4") !== undefined) {
    legendFiles.push(self.getOption("LEGEND_FILE_4"));
  }
  self.setOption(ConfigurationType.LEGEND_FILES, legendFiles);

  self.setOverlayTypes(createCopy(original.getOverlayTypes()));

  self.setImageConverters(createCopy(original.getImageConverters()));
  self.setModelConverters(createCopy(original.getModelConverters()));
  self.setElementTypes(createCopy(original.getElementTypes()));
  self.setReactionTypes(createCopy(original.getReactionTypes()));
  self.setMiriamTypes(createCopy(original.getMiriamTypes()));
  self.setMapTypes(createCopy(original.getMapTypes()));
  self.setModificationStateTypes(createCopy(original.getModificationStateTypes()));
  self.setPrivilegeTypes(createCopy(original.getPrivilegeTypes()));
  self.setPluginsData(createCopy(original.getPluginsData()));
  self._annotators = [];
  for (i = 0; i < original.getAnnotators(); i++) {
    self._annotators.push(new Annotator(original.getAnnotators()[i]));
  }
};

Configuration.prototype.getElementTypeTree = function () {
  var elementTypes = this.getElementTypes();
  var reactionTypes = this.getReactionTypes();

  var treeNodes = {
    "lcsb.mapviewer.model.map.BioEntity": {
      text: "BioEntity",
      children: []
    }
  };

  var i, type;
  for (i = 0; i < elementTypes.length; i++) {
    type = elementTypes[i];
    var name = type.className;
    if (name.indexOf(".") > 0) {
      name = name.substr(name.lastIndexOf(".") + 1);
    }
    treeNodes[type.className] = {
      text: name,
      data: type,
      children: []
    };
  }

  for (i = 0; i < reactionTypes.length; i++) {
    type = reactionTypes[i];
    treeNodes[type.className] = {
      text: type.name,
      data: type,
      children: []
    };
  }

  for (var treeNodeName in treeNodes) {
    if (treeNodes.hasOwnProperty(treeNodeName)) {
      var treeNode = treeNodes[treeNodeName];
      if (treeNode.data !== undefined) {
        var parentNode = treeNodes[treeNode.data.parentClass];
        if (parentNode.data === undefined || parentNode.data.name !== "Compartment") {
          parentNode.children.push(treeNode);
        }
      }
    }
  }

  return treeNodes["lcsb.mapviewer.model.map.BioEntity"];
};


module.exports = Configuration;
