"use strict";

/* exported logger */

var logger = require('./logger');

function ValidationError(message) {
  this.message = message;
  this.stack = (new Error()).stack;
}

ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

module.exports = ValidationError;
