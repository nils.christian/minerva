"use strict";

/* exported logger */

var Promise = require("bluebird");

var CustomMapOptions = require('./map/CustomMapOptions');
var ElementExportPanel = require('./gui/export/ElementExportPanel');
var GraphicsExportPanel = require('./gui/export/GraphicsExportPanel');
var NetworkExportPanel = require('./gui/export/NetworkExportPanel');
var Header = require('./gui/Header');
var ObjectWithListeners = require('./ObjectWithListeners');

var logger = require('./logger');
var Functions = require('./Functions');

/**
 * Default constructor.
 * 
 * @param options
 *          CustomMapOptions object representing all parameters needed for map
 *          creation
 */
function Export(options) {
  var self = this;
  self._panels = [];
  self._tabIdCount = 0;
  if (!(options instanceof CustomMapOptions)) {
    options = new CustomMapOptions(options);
  }
  self.setProject(options.getProject());
  self.setElement(options.getElement());

  self.setConfiguration(options.getConfiguration());
  self._createGui();
}

Export.prototype = Object.create(ObjectWithListeners.prototype);
Export.prototype.constructor = ObjectWithListeners;

Export.prototype._createGui = function() {
  var self = this;
  self.getElement().innerHTML = "";
  var headerDiv = Functions.createElement({
    type : "div"
  });
  new Header({
    element : headerDiv,
    customMap : null,
    project : self.getProject()
  });
  self.getElement().appendChild(headerDiv);

  var panels = [ {
    name : "ELEMENTS",
    panelClass : ElementExportPanel
  }, {
    name : "NETWORK",
    panelClass : NetworkExportPanel
  }, {
    name : "GRAPHICS",
    panelClass : GraphicsExportPanel
  } ];

  var tabDiv = Functions.createElement({
    type : "div",
    name : "tabView",
    className : "tabbable boxed parentTabs"
  });
  self.getElement().appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type : "ul",
    className : "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type : "div",
    className : "tab-content"
  });
  tabDiv.appendChild(tabContentDiv);

  for (var i = 0; i < panels.length; i++) {
    self.addTab(panels[i], tabMenuDiv, tabContentDiv);
  }
};

Export.prototype.addTab = function(params, navElement, contentElement) {
  var self = this;

  var name = params.name;

  var tabId = "export_panel_tab_" + this._tabIdCount;
  self._tabIdCount++;

  var navClass = '';
  var contentClass = 'tab-pane';
  if (navElement.children.length === 0) {
    navClass = "active";
    contentClass = "tab-pane active";
  }

  var navLi = document.createElement("li");
  navLi.className = navClass;

  var navLink = document.createElement("a");
  navLink.href = "#" + tabId;
  if (name !== undefined) {
    if (name.length > 12) {
      name = name.substring(0, 10) + "...";
    }
    navLink.innerHTML = name;
  }
  navLink.onclick = function() {
    $(this).tab('show');
  };
  navLi.appendChild(navLink);
  if (name !== undefined) {
    navLink.innerHTML = name;
  }
  navElement.appendChild(navLi);

  var contentDiv = document.createElement("div");
  contentDiv.style.height = "100%";
  contentDiv.className = contentClass;
  contentDiv.id = tabId;

  contentElement.appendChild(contentDiv);

  this._panels.push(new params.panelClass({
    element : contentDiv,
    project : self.getProject(),
    configuration : self.getConfiguration()
  }));
};

Export.prototype.setProject = function(project) {
  this._project = project;
};
Export.prototype.getProject = function() {
  return this._project;
};

Export.prototype.setElement = function(element) {
  this._element = element;
};
Export.prototype.getElement = function() {
  return this._element;
};

Export.prototype.init = function() {
  var promises = [];
  for (var i = 0; i < this._panels.length; i++) {
    promises.push(this._panels[i].init());
  }
  return Promise.all(promises);
};

Export.prototype.setConfiguration = function(configuration) {
  this._configuration = configuration;
};

Export.prototype.getConfiguration = function() {
  return this._configuration;
};

module.exports = Export;
