"use strict";

/* exported logger */

var Point = require('./map/canvas/Point');
var SessionObjectType = require('./SessionObjectType');

var Cookies = require('js-cookie');

var logger = require('./logger');

function SessionData(project) {
  if (project === undefined) {
    throw new Error("Project must be passed as an argument");
  }
  this.setProject(project);
}

SessionData.prototype.setProject = function(project) {
  this._project = project;
};

SessionData.prototype.getProject = function() {
  return this._project;
};

SessionData.prototype.getProjectId = function() {
  return this._project.getProjectId();
};

SessionData.prototype.getShowComments = function() {
  var key = this.getKey(SessionObjectType.SHOW_COMMENT);
  return Cookies.get(key) === "true";
};

SessionData.prototype.setSearchQuery = function(value) {
  var key = this.getKey(SessionObjectType.SEARCH_QUERY);
  Cookies.set(key, JSON.stringify(value));
};

SessionData.prototype.setQuery = function(param) {
  if (param.type === "drug") {
    this.setDrugQuery(param.query);
  } else if (param.type === "search") {
    this.setSearchQuery(param.query);
  } else if (param.type === "chemical") {
    this.setChemicalQuery(param.query);
  } else if (param.type === "mirna") {
    this.setMiRnaQuery(param.query);
  } else {
    throw new Error("Invalid query type: " + param.type);
  }
};

SessionData.prototype.getSearchQuery = function() {
  var key = this.getKey(SessionObjectType.SEARCH_QUERY);
  var result = Cookies.get(key);
  if (result !== undefined) {
    result = JSON.parse(result);
  }
  return result;
};

SessionData.prototype.setDrugQuery = function(value) {
  var key = this.getKey(SessionObjectType.DRUG_QUERY);
  Cookies.set(key, JSON.stringify(value));
};

SessionData.prototype.getDrugQuery = function() {
  var key = this.getKey(SessionObjectType.DRUG_QUERY);
  var result = Cookies.get(key);
  if (result !== undefined) {
    result = JSON.parse(result);
  }
  return result;
};

SessionData.prototype.setMiRnaQuery = function(value) {
  var key = this.getKey(SessionObjectType.MI_RNA_QUERY);
  Cookies.set(key, JSON.stringify(value));
};

SessionData.prototype.getMiRnaQuery = function() {
  var key = this.getKey(SessionObjectType.MI_RNA_QUERY);
  var result = Cookies.get(key);
  if (result !== undefined) {
    result = JSON.parse(result);
  }
  return result;
};

SessionData.prototype.setChemicalQuery = function(value) {
  var key = this.getKey(SessionObjectType.CHEMICAL_QUERY);
  Cookies.set(key, JSON.stringify(value));
};

SessionData.prototype.getChemicalQuery = function() {
  var key = this.getKey(SessionObjectType.CHEMICAL_QUERY);
  var result = Cookies.get(key);
  if (result !== undefined) {
    result = JSON.parse(result);
  }
  return result;
};

SessionData.prototype.setShowComments = function(value) {
  var key = this.getKey(SessionObjectType.SHOW_COMMENT);
  Cookies.set(key, value + "");
};

SessionData.prototype.getSelectedBackgroundOverlay = function() {
  var key = this.getKey(SessionObjectType.SELECTED_BACKGROUND_OVERLAY);
  return Cookies.get(key);
};

SessionData.prototype.setSelectedBackgroundOverlay = function(value) {
  var key = this.getKey(SessionObjectType.SELECTED_BACKGROUND_OVERLAY);
  Cookies.set(key, value + "");
};

SessionData.prototype.getVisibleOverlays = function() {
  var key = this.getKey(SessionObjectType.VISIBLE_OVERLAYS);
  var value = Cookies.get(key);
  if (value === undefined || value === "") {
    value = [];
  } else {
    value = value.split(",");
  }
  return value;
};

SessionData.prototype.setVisibleOverlays = function(value) {
  var key = this.getKey(SessionObjectType.VISIBLE_OVERLAYS);
  Cookies.set(key, value + "");
};

/**
 *
 * @param {MapModel} model
 * @param {number} value
 */
SessionData.prototype.setZoomLevel = function(model, value) {
  var key = this.getKey(SessionObjectType.ZOOM_LEVEL, [ model.getId() ]);
  Cookies.set(key, value + "");
};

/**
 *
 * @param {MapModel} model
 * @returns {number}
 */
SessionData.prototype.getZoomLevel = function(model) {
  var key = this.getKey(SessionObjectType.ZOOM_LEVEL, [ model.getId() ]);
  var value = Cookies.get(key);
  if (value !== undefined) {
    value = parseInt(value);
  }
  return value;
};

SessionData.prototype.setToken = function(token) {
  var key = SessionObjectType.TOKEN;
  if (token === undefined) {
    Cookies.remove(key);
  } else {
    Cookies.set(key, token);
  }
};

SessionData.prototype.getToken = function() {
  var key = SessionObjectType.TOKEN;
  return Cookies.get(key);
};

SessionData.prototype.setLogin = function(login) {
  var key = SessionObjectType.LOGIN;
  if (login === undefined) {
    Cookies.remove(key);
  } else {
    Cookies.set(key, login);
  }
};

SessionData.prototype.getLogin = function() {
  var key = SessionObjectType.LOGIN;
  return Cookies.get(key);
};

SessionData.prototype.setCenter = function(model, value) {
  var key = this.getKey(SessionObjectType.CENTER, [ model.getId() ]);
  Cookies.set(key, value.x + "," + value.y);
};

/**
 *
 * @param {MapModel} model
 * @returns {Point}
 */
SessionData.prototype.getCenter = function(model) {
  var key = this.getKey(SessionObjectType.CENTER, [ model.getId() ]);
  var value = Cookies.get(key);
  if (value !== undefined) {
    var tmp = value.split(",");
    value = new Point(tmp[0], tmp[1]);
  }
  return value;
};

/**
 *
 * @param {string} type
 * @param {Array} [args]
 * @returns {string}
 */
SessionData.prototype.getKey = function(type, args) {
  if (type === undefined) {
    throw new Error("Undefined type");
  }
  if (args === undefined) {
    args = [];
  }
  return type + "_" + this.getProjectId() + "_" + args.join("_");
};

module.exports = SessionData;
