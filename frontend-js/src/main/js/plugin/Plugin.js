"use strict";

var MinervaPluginProxy = require('./MinervaPluginProxy');
var ObjectWithListeners = require('../ObjectWithListeners');

var Promise = require("bluebird");

var logger = require('../logger');
var Functions = require('../Functions');

var pluginId = 0;

function Plugin(options) {
  ObjectWithListeners.call(this);
  var self = this;
  self.setOptions(options);
  self.registerListenerType("onUnload");
  self.registerListenerType("onResize");
}

Plugin.prototype = Object.create(ObjectWithListeners.prototype);
Plugin.prototype.constructor = ObjectWithListeners;

Plugin.prototype.setOptions = function (options) {
  this._options = options;
};

Plugin.prototype.getOptions = function () {
  return this._options;
};

Plugin.prototype.setLoadedPluginData = function (loadedPluginData) {
  this._loadedPluginData = loadedPluginData;
};
Plugin.prototype.getLoadedPluginData = function () {
  return this._loadedPluginData;
};

Plugin.prototype.setMinervaPluginProxy = function (minervaPluginProxy) {
  this._minervaPluginProxy = minervaPluginProxy;
};
Plugin.prototype.getMinervaPluginProxy = function () {
  return this._minervaPluginProxy;
};
Plugin.prototype.getPluginId = function () {
  return this.getMinervaPluginProxy().pluginId;
};

Plugin.prototype.load = function () {
  var self = this;
  var options = self.getOptions();

  var hash;
  var error = false;
  var registerPromise = null;

  return ServerConnector.sendRequest({
    url: options.url,
    description: "Loading plugin: " + options.url,
    method: "GET"
  }).then(function (content) {
    hash = Functions.computeMD5(content);
    var pluginData = undefined;
    try {
      // noinspection JSUnusedLocalSymbols
      var minervaDefine = function (pluginFunction) {
        try {
          if (typeof pluginFunction === "function") {
            pluginData = pluginFunction();
          } else {
            pluginData = pluginFunction;
          }

          var minervaPluginProxy = new MinervaPluginProxy({
            hash: hash,
            map: options.map,
            configuration: options.configuration,
            element: options.element,
            plugin: self,
            pluginId: "plugin" + (pluginId++)
          });
          self.setLoadedPluginData(pluginData);
          self.setMinervaPluginProxy(minervaPluginProxy);
          registerPromise = new Promise(function (resolve) {
            resolve(pluginData.register(minervaPluginProxy));
          });
        } catch (e) {
          error = e;
        }
      };
      content += "//# sourceURL=" + options.url;
      eval(content);
    } catch (e) {
      error = e;
    }
    return ServerConnector.registerPlugin({
      hash: hash,
      url: options.url,
      name: self.getName(),
      version: self.getVersion()
    });
  }).then(function () {
    if (error) {
      return Promise.reject(error);
    }
    return registerPromise;
  });

};

Plugin.prototype.getMinWidth = function () {
  var value;
  var data = this.getLoadedPluginData();
  if (data.minWidth !== undefined) {
    if (typeof data.minWidth === "function") {
      value = parseInt(data.minWidth());
    } else {
      value = parseInt(data.minWidth);
    }
  }
  return value;
};

Plugin.prototype.unload = function () {
  var self = this;
  return Promise.resolve().then(function () {
    return self.getLoadedPluginData().unregister();
  }).then(function () {
    var removedListeners = self.getMinervaPluginProxy().project.map.removeAllListeners();
    if (removedListeners.length > 0) {
      logger.warn("'" + self.getLoadedPluginData().getName() + "' plugin didn't remove all registered listeners");
    }
    return self.callListeners("onUnload");
  }).then(function () {
    return self.destroy();
  });
};

Plugin.prototype.getName = function () {
  if (this.getLoadedPluginData() === undefined) {
    return "";
  }
  return this.getLoadedPluginData().getName();
};

Plugin.prototype.getVersion = function () {
  if (this.getLoadedPluginData() === undefined) {
    return "";
  }
  return this.getLoadedPluginData().getVersion();
};

Plugin.prototype.destroy = function () {
  return Promise.resolve();
};

Plugin.prototype.setHash = function (hash) {
  this.getMinervaPluginProxy().hash = hash;
};

module.exports = Plugin;
