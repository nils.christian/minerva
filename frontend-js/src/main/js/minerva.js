'use strict';

var functions = require('./Functions');

var AbstractDbOverlay = require('./map/overlay/AbstractDbOverlay');
var Admin = require('./Admin');
var DbOverlayCollection = require('./map/overlay/DbOverlayCollection');
var ConfigurationType = require('./ConfigurationType');
var CustomMap = require('./map/CustomMap');
var CustomMapOptions = require('./map/CustomMapOptions');
var Export = require('./Export');
var GuiUtils = require('./gui/leftPanel/GuiUtils');
var PluginManager = require('./plugin/PluginManager');
var Point = require('./map/canvas/Point');
var SearchDbOverlay = require('./map/overlay/SearchDbOverlay');

var LeftPanel = require('./gui/leftPanel/LeftPanel');
var TopMenu = require('./gui/topMenu/TopMenu');
var Legend = require('./gui/Legend');
var MapContextMenu = require('./gui/MapContextMenu');
var SelectionContextMenu = require('./gui/SelectionContextMenu');

var GuiConnector = require('./GuiConnector');
var ServerConnector = require('./ServerConnector');

var MolArt = require('./map/structure/MolArt');

var Promise = require("bluebird");

var logger = require('./logger');


var customMap, leftPanel, topMenu, legend, mapContextMenu, selectionContextMenu;

function processUrlGetParams(params) {
  var project = params.getProject();
  var sessionData = ServerConnector.getSessionData(project);

  var modelId;
  if (GuiConnector.getParams["submap"] !== undefined) {
    modelId = parseInt(GuiConnector.getParams["submap"]);
  } else {
    modelId = project.getModels()[0].getId()
  }

  var model = project.getModelById(modelId);

  if (GuiConnector.getParams["x"] !== undefined && GuiConnector.getParams["y"] !== undefined) {
    var point = new Point(GuiConnector.getParams["x"], GuiConnector.getParams["y"]);
    sessionData.setCenter(model, point);
  }
  if (GuiConnector.getParams["zoom"] !== undefined) {
    sessionData.setZoomLevel(model, GuiConnector.getParams["zoom"]);
  }

  if (GuiConnector.getParams["comments"] === "on") {
    sessionData.setShowComments(true);
  }
  if (GuiConnector.getParams["search"] !== undefined) {
    var query = SearchDbOverlay.prototype.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY,
      GuiConnector.getParams["search"]);
    sessionData.setSearchQuery(query);
  }

}

/**
 *
 * @param {User} user
 * @param {string} termsOfUseUrl
 */
function requestConsent(user, termsOfUseUrl) {
  if (termsOfUseUrl === "") {
    return;
  }
  var dialog = document.createElement("div");
  var dialogContent = document.createElement("div");
  dialogContent.appendChild(functions.createElement({
    type: "span",
    content: "I agree to the minerva <a href='" + termsOfUseUrl + "' target='_blank'>Terms of Use</a>."
  }));
  var checkbox = functions.createElement({type: "input", inputType: "checkbox", style: "float: left"});
  var okButton = functions.createElement({
    type: "button",
    content: "OK",
    className: "ui-button ui-corner-all ui-widget",
    onclick: function () {
      user.setTermsOfUseConsent($(checkbox).is(':checked'));
      return ServerConnector.updateUser(user).then(function () {
        $(dialog).dialog("close");
      }).catch(GuiConnector.alert);
    }
  });
  var cancelButton = functions.createElement({
    type: "button",
    content: "I disagree",
    className: "ui-button ui-corner-all ui-widget",
    onclick: function () {
      return ServerConnector.logout().catch(GuiConnector.alert);
    }
  });
  $(okButton).prop("disabled", true);
  $(checkbox).change(function () {
    $(okButton).prop("disabled", !$(checkbox).is(':checked'));
  });
  dialogContent.appendChild(checkbox);
  dialogContent.appendChild(okButton);
  dialogContent.appendChild(cancelButton);

  dialog.appendChild(dialogContent);
  document.body.appendChild(dialog);
  $(dialog).dialog({
    classes: {
      "ui-dialog": "ui-state-error"
    },
    modal: true,
    closeOnEscape: false,
    title: "Terms of Use"
  }).siblings('.ui-dialog-titlebar').css("background", "red");
  $(".ui-dialog-titlebar-close", $(dialog).dialog().siblings('.ui-dialog-titlebar')).hide();
  $(dialog).dialog("open");
}

function insertGoogleAnalyticsCode() {
  return ServerConnector.getConfigurationParam(ConfigurationType.GOOGLE_ANALYTICS_IDENTIFIER).then(function (identifier) {
    if (identifier === "" || identifier === undefined || identifier === null) {
      return Promise.resolve();
    } else {
      global._gaq = global._gaq || [];
      global._gaq.push(['_setAccount', identifier]);
      global._gaq.push(['_trackPageview']);

      (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www')
          + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
      })();
      return Promise.resolve();
    }
  });
}

function createDivStructure(element) {
  var tableDiv = functions.createElement({
    type: "div",
    className: "minerva-root"
  });
  element.appendChild(tableDiv);
  var leftPanelDiv = functions.createElement({
    type: "div",
    className: "minerva-left-panel"
  });
  tableDiv.appendChild(leftPanelDiv);
  var middlePanelDiv = functions.createElement({
    type: "div",
    className: "minerva-middle-panel"
  });
  tableDiv.appendChild(middlePanelDiv);

  //this container is required because google maps API modifies DOM and it will crash
  var middlePanelContainerDiv = functions.createElement({type: "div"});
  middlePanelDiv.appendChild(middlePanelContainerDiv);

  var splitBar = functions.createElement({
    type: "div",
    className: "minerva-plugin-split-bar",
    content: "&nbsp"
  });
  tableDiv.appendChild(splitBar);

  var rightPanelDiv = functions.createElement({
    type: "div",
    className: "minerva-plugin"
  });
  tableDiv.appendChild(rightPanelDiv);

  var menuDiv = functions.createElement({
    type: "div",
    className: "minerva-top-menu"
  });
  middlePanelContainerDiv.appendChild(menuDiv);

  var mapDiv = functions.createElement({
    type: "div",
    className: "minerva-map"
  });
  middlePanelContainerDiv.appendChild(mapDiv);

  var legendDiv = functions.createElement({
    type: "div",
    name: "legendDiv",
    className: "minerva-legend",
    style: "display:none"
  });
  middlePanelContainerDiv.appendChild(legendDiv);

  var contextMenu = functions.createElement({
    type: "ul",
    name: "contextMenu"
  });
  element.appendChild(contextMenu);
  var selectionContextMenu = functions.createElement({
    type: "ul",
    name: "selectionContextMenu"
  });
  element.appendChild(selectionContextMenu);
}


function initGlobals(params) {
  if (global.ServerConnector === undefined) {
    global.ServerConnector = ServerConnector;
    if (params.isDebug()) {
      logger.setLevel("debug");
    } else {
      logger.setLevel("info");
    }
    GuiConnector.init();
    if (GuiConnector.getParams['debug'] !== undefined) {
      logger.setLevel("debug");
    }
  } else {
    logger.warn("global ServerConnector found");
  }

}

function verifyBrowser() {

  var browser = functions.browser;
  if (browser.name === "IE") {
    if (browser.version <= 8 || browser.compatibilityMode) {
      var message = "This web page works only with Internet Explorer version 9 or greater.\n"
        + "If you have Internet Explorer version 9 or greater and still see this message, please, turn the 'Compatibility mode off:\n"
        + "Open Internet Explorer and press the Alt key on your keyboard.\n"
        + "Select 'Tools' menu item. \n"
        + "Select the 'Compatibility View' settings option. \n"
        + "Make sure the 'Display all websites in Compatibility View' check box is unchecked and that the 'Compatibility View; list of websites is cleared.\n"
        + "\n" + "Alternatively, please, use other browsers: Chrome, Firefox or Safari.";
      GuiConnector.alert(message);
    }
  }
}

function getProject(params) {
  if (params.getProject() !== undefined) {
    return Promise.resolve(params.getProject());
  } else {
    return ServerConnector.getProject(params.getProjectId());
  }
}

function modifyParamsForTouchTable(params) {
  if (params.bigLogo === undefined) {
    // noinspection UnnecessaryLocalVariableJS
    var isTouchTable = ((navigator.appVersion.indexOf("Win") > -1) && ('ontouchstart' in document.documentElement));
    params.bigLogo = isTouchTable;
  }
  return params;
}

function assignSplitBarHandler(customMap, pluginManager) {
  var splitBar = $('.minerva-plugin-split-bar');
  var rightPanelDiv = $('.minerva-plugin');
  var mouseDownHandler = function (e) {
    e.preventDefault();
    //TODO should use global size (but element size)
    var x = $("body").width() - e.pageX;
    $(rightPanelDiv).css("flex", "0 0 " + x + "px");
    customMap.getMapCanvas().triggerListeners('resize');
    return pluginManager.callListeners("onResize");
  };
  $(splitBar).mousedown(function (e) {
    e.preventDefault();
    $(document).mousemove(mouseDownHandler);
  });
  $(document).mouseup(function () {
    $(document).unbind('mousemove', mouseDownHandler);
  });
}

function create(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  params.setServerConnector(ServerConnector);
  initGlobals(params);
  params.getElement().innerHTML = "<div style='position:relative; width:100%;height:100%'>"
    + "<img src='resources/images/icons/ajax-loader.gif' style='position:absolute;top:0;bottom:0;left:0;right:0;margin:auto;'/>" + "</div>";

  // make sure that we are logged in
  return ServerConnector.createSession().then(function () {
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    params.setConfiguration(configuration);
    return getProject(params);
  }).then(function (project) {
    if (project === null) {
      var message = "Project with given id doesn't exist.";
      message += "<p>Please go to <a href='" + ServerConnector.getServerBaseUrl() + "'>default map</a>";
      return Promise.reject(new Error(message));
    }
    params.setProject(project);
    var promise = Promise.resolve();
    if (params.getProject().getMapCanvasType() === "GOOGLE_MAPS_API") {
      promise = functions.loadScript("https://maps.google.com/maps/api/js?libraries=drawing&key=" + params.getConfiguration().getOption(ConfigurationType.GOOGLE_MAPS_API_KEY).getValue());
    }
    return promise;
  }).then(function () {
    var element = params.getElement();

    verifyBrowser();

    processUrlGetParams(params);

    params.getElement().innerHTML = "";
    createDivStructure(element);
    params.setElement(functions.getElementByClassName(element, "minerva-map"));

    customMap = new CustomMap(params);


    new DbOverlayCollection({
      map: customMap
    });

    leftPanel = new LeftPanel({
      element: functions.getElementByClassName(element, "minerva-left-panel"),
      customMap: customMap,
      configuration: params.getConfiguration()
    });

    var pluginManager = new PluginManager({
      element: functions.getElementByClassName(element, "minerva-plugin"),
      map: customMap,
      project: params.getProject(),
      configuration: params.getConfiguration()
    });
    leftPanel.setPluginManager(pluginManager);
    assignSplitBarHandler(customMap, pluginManager);

    topMenu = new TopMenu({
      element: functions.getElementByClassName(element, "minerva-top-menu"),
      customMap: customMap
    });

    legend = new Legend({
      element: functions.getElementByName(element, "legendDiv"),
      customMap: customMap
    });

    mapContextMenu = new MapContextMenu({
      element: functions.getElementByName(element, "contextMenu"),
      customMap: customMap,
      molArt: new MolArt(element, customMap)
    });
    customMap.setContextMenu(mapContextMenu);

    selectionContextMenu = new SelectionContextMenu({
      element: functions.getElementByName(element, "selectionContextMenu"),
      customMap: customMap
    });
    customMap.setSelectionContextMenu(selectionContextMenu);

    topMenu.setLegend(legend);
    topMenu.setLeftPanel(leftPanel);

    return customMap.init();
  }).then(function () {
    return insertGoogleAnalyticsCode(customMap);
  }).then(function () {
    return leftPanel.init();
  }).then(function () {
    return legend.init();
  }).then(function () {
    return topMenu.init();
  }).then(function () {
    return selectionContextMenu.init();
  }).then(function () {
    return mapContextMenu.init();
  }).then(function () {
    if (GuiConnector.getParams["layout"] !== undefined) {
      var overlays = params.getProject().getDataOverlays();
      for (var j = 0; j < overlays.length; j++) {
        var overlay = overlays[j];
        if (overlay.getName() === GuiConnector.getParams["layout"]) {
          return customMap.openDataOverlay(overlay);
        }
      }
    }
  }).then(function () {

    var submapId = GuiConnector.getParams["submap"];
    if (submapId !== undefined) {
      return customMap.openSubmap(submapId);
    }
  }).then(function () {
    var promises = [];
    for (var i = 0; i < params.getPlugins().length; i++) {
      promises.push(leftPanel.getPluginManager().addPlugin(params.getPlugins()[i]))
    }
    return Promise.all(promises);
  }).then(function () {
    return ServerConnector.getLoggedUser();
  }).then(function (user) {
    if (leftPanel.isGoogleLicenseConsentRequired()) {
      GuiConnector.warn("Visualization of data overlays (Overlays tab) requires consent to terms of the <a href='https://cloud.google.com/maps-platform/terms/' target='_blank'>license of Google Maps Platform</a>. Click the \"Edit\" button for the overlay to do so.");
    }
    if (user.getLogin() !== "anonymous" && !user.isTermsOfUseConsent()) {
      requestConsent(user, params.getConfiguration().getOption(ConfigurationType.TERMS_OF_USE).getValue());
    }
    var result = {
      destroy: function () {
        return leftPanel.destroy().then(function () {
          customMap.destroy();
          return topMenu.destroy();
        });
      },
      getProject: function () {
        return customMap.getProject();
      }
    };

    if (params.isDebug()) {
      result.leftPanel = leftPanel;
      result.customMap = customMap;
    }
    return result;
  });

}

function createFooter() {
  var logoLink, logoText, logoImg;
  return ServerConnector.getConfigurationParam(ConfigurationType.LOGO_LINK).then(function (result) {
    logoLink = result;
    return ServerConnector.getConfigurationParam(ConfigurationType.LOGO_TEXT);
  }).then(function (result) {
    logoText = result;
    return ServerConnector.getConfigurationParam(ConfigurationType.LOGO_IMG);
  }).then(function (result) {
    logoImg = result;
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    return functions.createElement({
      type: "div",
      className: "minerva-footer-table",
      content: '<table width="100%" border="0" cellspacing="0" cellpadding="0">' +
      '<tr>' +
      '<td align="left"><a href="' + logoLink + '" title="' + logoText + '" target="_blank">' +
      '<img src="' + GuiConnector.getImgPrefix() + logoImg + '" width="80" height="80" border="0" alt="' + logoText + '"/>' +
      '</a></td>' +
      '<td align="center" class="minerva-footer-text">MiNERVA version ' + configuration.getVersion() + ';<br/>' +
      'build ' + configuration.getBuildDate() + ';<br/>' +
      'git: ' + configuration.getGitHash() + '</td>' +
      '<td align="right"><a href="http://wwwen.uni.lu/lcsb/" title="LCSB - Luxembourg Centre for Systems Biomedicine" target="_blank">' +
      '<img src="' + GuiConnector.getImgPrefix() + 'lcsb.png" width="80" height="80" border="0" alt="LCSB - Luxembourg Centre for Systems Biomedicine"/>' +
      '</a></td>' +
      '</tr>\n' +
      '</table>', xss: false
    });
  })
}

function createLoginDiv() {
  var loggedIn = false;

  var result = functions.createElement({type: "center"});
  var resultDiv = functions.createElement({type: "div", className: "minerva-login-form", style: "text-align:left"});
  result.appendChild(resultDiv);
  resultDiv.appendChild(functions.createElement({
    type: "div",
    className: "minerva-login-form-title",
    content: "AUTHORIZATION FORM"
  }));
  var guiUtils = new GuiUtils();
  var table = functions.createElement({
    type: "div",
    className: "loginDataPanelGrid",
    style: "display: table;border-spacing:5px"
  });
  table.appendChild(guiUtils.createTableRow([
    functions.createElement({type: "label", className: "labelText", content: "LOGIN:"}),
    functions.createElement({type: "input", className: "minerva-input-text", id: "username"})]
  ));
  table.appendChild(guiUtils.createTableRow([
    functions.createElement({type: "label", className: "labelText", content: "PASSWORD:"}),
    functions.createElement({
      type: "input",
      inputType: "password",
      className: "minerva-input-password",
      id: "password"
    })]
  ));

  table.appendChild(guiUtils.createTableRow([
    functions.createElement({type: "label", className: "labelText"}),
    functions.createElement({type: "button", className: "labelText", id: "login", content: "LOGIN"})]
  ));

  resultDiv.appendChild(table);
  resultDiv.appendChild(functions.createElement({type: "br"}));
  resultDiv.appendChild(functions.createElement({type: "br"}));
  resultDiv.appendChild(functions.createElement({
    type: "a",
    href: "javascript:;",
    id: "go_to_map_button",
    className: "adminLink",
    content: '<i class="fa fa-chevron-right"></i> BACK TO MAP',
    xss: false
  }));
  resultDiv.appendChild(functions.createElement({type: "br"}));
  resultDiv.appendChild(functions.createElement({
    type: "a",
    href: "javascript:;",
    id: "register_button",
    className: "adminLink",
    content: '<i class="fa fa-chevron-right"></i> REQUEST AN ACCOUNT</a>',
    xss: false
  }));

  var fromPage = GuiConnector.getParams["from"];

  $("#login", result).on("click", function login() {
    var loginString = document.getElementById('username').value;
    var passwordString = document.getElementById('password').value;
    return ServerConnector.login(loginString, passwordString).then(function () {
      loggedIn = true;
      if (fromPage !== undefined) {
        window.location.href = fromPage;
      } else {
        window.location.href = ServerConnector.getServerBaseUrl();
      }
    }, function (error) {
      if (error.constructor.name === "InvalidCredentialsError") {
        GuiConnector.alert("invalid credentials");
      } else {
        GuiConnector.alert(error);
      }
    });
  });
  $('#go_to_map_button', result).click(function () {
    return ServerConnector.getProjectId().then(function (projectId) {
      window.location.href = ServerConnector.getServerBaseUrl() + '?id=' + projectId;
    });
  });
  $('#register_button', result).click(function () {
    var email, content;
    return ServerConnector.getConfigurationParam("REQUEST_ACCOUNT_EMAIL").then(function (result) {
      email = result;
      return ServerConnector.getConfigurationParam("REQUEST_ACCOUNT_DEFAULT_CONTENT");
    }).then(function (result) {
      content = encodeURIComponent(result);
      document.location.href = 'mailto:' + email + '?subject=MINERVA account request&body=' + content;
    });
  });

  return result;
}


function createLogin(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  initGlobals(params);
  // make sure that we are logged in
  return ServerConnector.createSession().then(function () {
    return ServerConnector.getConfiguration();
  }).then(function () {
    var loginDiv = createLoginDiv(params);
    params.getElement().appendChild(loginDiv);
    return createFooter();
  }).then(function (footer) {
    params.getElement().appendChild(footer);
    return insertGoogleAnalyticsCode();
  })
}

function createExport(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  initGlobals(params);
  params.getElement().style.display = "table";
  params.getElement().innerHTML = "<div style='vertical-align:middle;display:table-cell;text-align: center'>"
    + "<img src='resources/images/icons/ajax-loader.gif'/>" + "</div>";

  var result;
  // make sure that we are logged in
  return ServerConnector.getConfiguration().then(function (configuration) {
    params.setConfiguration(configuration);
    return getProject(params);
  }).then(function (project) {
    params.setProject(project);
    result = new Export(params);
    return result.init();
  }).then(function () {
    return result;
  });
}

function createAdmin(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  params.setServerConnector(ServerConnector);
  initGlobals(params);
  params.getElement().style.display = "table";
  params.getElement().innerHTML = "<div style='vertical-align:middle;display:table-cell;text-align: center'>"
    + "<img src='resources/images/icons/ajax-loader.gif'/>" + "</div>";
  if (ServerConnector.getSessionData(null).getLogin() === "anonymous") {
    window.location.href = ServerConnector.getServerBaseUrl() + "login.xhtml?from=" + encodeURI(window.location.href);
    return Promise.resolve()
  }

  var result;
  // make sure that we are logged in
  return ServerConnector.createSession().then(function () {
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    params.setConfiguration(configuration);
    result = new Admin(params);
    return createFooter();
  }).then(function (footer) {
    params.getElement().appendChild(footer);
    return result.init();
  }).then(function () {
    return result;
  });
}

var minerva = {
  create: create,
  createLogin: createLogin,
  createExport: createExport,
  createAdmin: createAdmin,
  ServerConnector: ServerConnector,
  GuiConnector: GuiConnector
};

module.exports = minerva;
