"use strict";

var Promise = require("bluebird");

var logger = require('./logger');

var Functions = require('./Functions');
var SecurityError = require('./SecurityError');
var ValidationError = require('./ValidationError');
var GuiMessageError = require('./gui/GuiMessageError');
var NetworkError = require('./NetworkError');

/**
 * This static global object contains set of functions that returns/set data in
 * the Gui (html).
 */
function GuiConnector() {
  // X coordinate of the mouse in a browser.
  //@type {number}
  this.xPos = 0;
  // coordinate of the mouse in a browser.
  //@type {number}
  this.yPos = 0;

  this.getParams = [];
}

function returnThisOrSingleton(object) {
  if (object === undefined || object === null) {
    return GuiConnector.singleton;
  } else {
    return object;
  }
}

/**
 * List of GET params passed via url.
 */

GuiConnector.prototype.init = function () {
  var self = returnThisOrSingleton(this);

  if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (pattern) {
      var d = this.length - pattern.length;
      return d >= 0 && this.lastIndexOf(pattern) === d;
    };
  }
  var isIE = /* @cc_on!@ */false || !!document.documentMode;

  if (isIE) {
    alert("This web page works well with Chrome, Firefox and Safari.");
  }
  // bootstrap tab initialization
  $("ul.nav-tabs a").click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  self.getParams = [];

  // find GuiConnector.getParams
  window.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
      return decodeURIComponent(s.split("+").join(" "));
    }

    self.getParams[decode(arguments[1])] = decode(arguments[2]);
  });

  self._touchStartEvent = function (e) {
    if (e.originalEvent !== undefined) {
      self.updateMouseCoordinates(e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY);
    }
  };
  self._touchMoveEvent = function (e) {
    if (e.originalEvent !== undefined) {
      self.updateMouseCoordinates(e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY);
    }
  };

  // force browser to update mouse coordinates whenever mouse move
  jQuery(document).ready(function () {
    $(document).mousemove(function (e) {
      self.updateMouseCoordinates(e.pageX, e.pageY);
    });
    $(document).on('touchstart', self._touchStartEvent);
    $(document).on('touchmove', self._touchMoveEvent);
  });

  if (self._windowResizeEvents === undefined) {
    self._windowResizeEvents = [];

    if (window.onresize !== null && window.onresize !== undefined) {
      self.addWindowResizeEvent(window.onresize);
    }

    window.onresize = function () {
      for (var i = 0; i < self._windowResizeEvents.length; i++) {
        self._windowResizeEvents[i]();
      }
    };
  }
};

GuiConnector.prototype.addWindowResizeEvent = function (handler) {
  this._windowResizeEvents.push(handler);
};

GuiConnector.prototype.removeWindowResizeEvent = function (handler) {
  var events = this._windowResizeEvents;
  var index = events.indexOf(handler);
  if (index > -1) {
    events.splice(index, 1);
  } else {
    logger.warn("Cannot find listener", handler);
  }
};

/**
 * Returns name of the file with LCSB logo.
 *
 * @param bigLogo
 *          {@link Boolean} value determining if we want to have big logo or
 *          small one
 */
GuiConnector.prototype.getLcsbLogoImg = function (bigLogo) {
  if (bigLogo) {
    return 'lcsb_logo_mid.png';
  } else {
    return 'lcsb_logo.png';
  }
};

/**
 * Returns name of the file with image that should be presented when we are
 * waiting for data to be loaded.
 */
GuiConnector.prototype.getLoadingImg = function () {
  return "icons/ajax-loader.gif";
};


GuiConnector.prototype.getEmptyTileUrl = function () {
  return "resources/images/empty_tile.png";
};

/**
 * Returns home directory for images in the application.
 */
GuiConnector.prototype.getImgPrefix = function () {
  return "resources/images/";
};

/**
 * Updates coordinates of the mouse in the browser.
 */
GuiConnector.prototype.updateMouseCoordinates = function (x, y) {
  var self = returnThisOrSingleton(this);
  self.xPos = x;
  self.yPos = y;
};

GuiConnector.prototype.showProcessing = function (messageText) {
  var self = returnThisOrSingleton(this);
  if (self._processingDialog === undefined) {
    self._processingDialog = document.createElement("div");
    self._processingDialogContent = document.createElement("div");
    self._processingDialog.appendChild(self._processingDialogContent);
    document.body.appendChild(self._processingDialog);
    $(self._processingDialog).dialog({
      modal: true,
      title: "PROCESSING",
      width: "150px",
      closeOnEscape: false,
      dialogClass: 'minerva-no-close'
    });
  }
  if (messageText === undefined) {
    messageText = "PROCESSING";
  }
  var messageImg = Functions.createElement({
    type: "img",
    src: 'resources/images/icons/ajax-loader.gif'
  });
  self._processingDialogContent.innerHTML = "";
  self._processingDialogContent.style.textAlign = "center";
  self._processingDialogContent.appendChild(messageImg);

  $(self._processingDialog).dialog("option", "title", messageText);

  $(self._processingDialog).dialog("open");
};

GuiConnector.prototype.hideProcessing = function () {
  var self = returnThisOrSingleton(this);
  $(self._processingDialog).dialog("close");
};

GuiConnector.prototype.alert = function (error, redirectIfSecurityError) {
  if (redirectIfSecurityError === undefined) {
    redirectIfSecurityError = false;
  }
  if (redirectIfSecurityError && error instanceof SecurityError && ServerConnector.getSessionData().getLogin() === "anonymous") {
    window.location.href = ServerConnector.getServerBaseUrl() + "login.xhtml?from=" + encodeURI(window.location.href);
  } else {
    var self = returnThisOrSingleton(this);
    logger.error(error);
    if (self._errorDialog === undefined) {
      self._errorDialog = document.createElement("div");
      self._errorDialogContent = document.createElement("div");
      self._errorDialog.appendChild(self._errorDialogContent);
      document.body.appendChild(self._errorDialog);
      $(self._errorDialog).dialog({
        classes: {
          "ui-dialog": "ui-state-error"
        },
        modal: true,
        title: "ERROR"
      }).siblings('.ui-dialog-titlebar').css("background", "red");
    }
    self._errorDialogContent.innerHTML = self.getErrorMessageForError(error);
    $(self._errorDialog).dialog("open");
  }
};

GuiConnector.prototype.getErrorMessageForError = function (error) {
  var message = error;
  if (message instanceof SecurityError) {
    if (ServerConnector.getSessionData().getLogin() === "anonymous") {
      message = error.message + "<p>Please <a href=\"login.xhtml?from=" + encodeURI(window.location.href) + "\">login</a> to access this resource</p>";
    } else {
      message = error.message + "<p>Please <a href=\"login.xhtml?from=" + encodeURI(window.location.href) + "\">login</a> " +
        "as a different user or ask your administrator to change the permissions to access this resource.</p>";
    }
  } else if (message instanceof ValidationError) {
    message = error.message;
  } else if (message instanceof GuiMessageError) {
    message = error.message;
  } else if (message instanceof Error) {
    message = "Unexpected error occurred:<p>" + error.message + "</p>";
  }
  return message;
};

GuiConnector.prototype.info = function (message) {
  var self = returnThisOrSingleton(this);
  if (self._infoDialog === undefined) {
    self._infoDialog = document.createElement("div");
    self._infoDialogContent = document.createElement("div");
    self._infoDialog.appendChild(self._infoDialogContent);
    document.body.appendChild(self._infoDialog);
    $(self._infoDialog).dialog({
      classes: {
        "ui-dialog": "ui-state-info"
      },
      modal: true,
      title: "INFO"
    });
  }
  self._infoDialogContent.innerHTML = message;
  $(self._infoDialog).dialog("open");

};

GuiConnector.prototype.showConfirmationDialog = function (params) {
  var message = params.message;
  var title = params.title;
  if (title === undefined) {
    title = "Confirm";
  }
  return new Promise(function (resolve) {
    $('<div></div>').appendTo('body')
      .html('<div><h6>' + message + '</h6></div>')
      .dialog({
        modal: true, title: title, zIndex: 10000, autoOpen: true,
        width: 'auto', resizable: false,
        buttons: {
          Yes: function () {
            $(this).dialog("close");
            resolve(true);
          },
          No: function () {
            $(this).dialog("close");
            resolve(false);
          }
        },
        close: function (event, ui) {
          $(this).remove();
        }
      });
  });
};

GuiConnector.prototype.destroy = function () {
  var self = returnThisOrSingleton(this);

  if (self._infoDialog !== undefined) {
    $(self._infoDialog).dialog("destroy").remove();
  }
  if (self._warnDialog !== undefined) {
    $(self._warnDialog).dialog("destroy").remove();
  }
  if (self._processingDialog !== undefined) {
    $(self._processingDialog).dialog("destroy").remove();
  }

  if (self._errorDialog !== undefined) {
    $(self._errorDialog).dialog("destroy").remove();
  }

  self._windowResizeEvents = undefined;
  $(document).off('touchstart', self._touchStartEvent);
  $(document).off('touchmove', self._touchMoveEvent);

};

GuiConnector.prototype.warn = function (message) {
  var self = GuiConnector;
  logger.warn(message);
  if (self._warnDialog === undefined) {
    self._warnDialog = document.createElement("div");
    self._warnDialogContent = document.createElement("div");
    self._warnDialog.appendChild(self._warnDialogContent);
    document.body.appendChild(self._warnDialog);
    $(self._warnDialog).dialog({
      classes: {
        "ui-dialog": "ui-state-highlight"
      },
      modal: true,
      title: "WARNING"
    });
  }
  self._warnDialogContent.innerHTML = message;
  $(self._warnDialog).dialog("open");
};

GuiConnector.singleton = new GuiConnector();

module.exports = GuiConnector.singleton;
