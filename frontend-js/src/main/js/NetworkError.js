"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

function NetworkError(message, connectionParams) {
  this.message = message;
  this.stack = (new Error()).stack;
  if (connectionParams === undefined || connectionParams === null) {
    connectionParams = {};
  }
  this.statusCode = connectionParams.statusCode;
  this.url = connectionParams.url;
  this.content = connectionParams.content;
  if (this.content === undefined) {
    this.content = "";
  }
}

NetworkError.prototype = Object.create(Error.prototype);
NetworkError.prototype.constructor = NetworkError;

module.exports = NetworkError;
