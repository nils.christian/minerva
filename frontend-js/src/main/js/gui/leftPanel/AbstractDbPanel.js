"use strict";

/* exported logger */

var Promise = require("bluebird");

var GuiConnector = require('../../GuiConnector');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');

var logger = require('../../logger');
var Functions = require('../../Functions');

/**
 *
 * @param {string} params.placeholder
 * @param {string} params.panelName
 * @constructor
 * @extends Panel
 */
function AbstractPanel(params) {
  Panel.call(this, params);
  var self = this;

  this._initializeGui(params.placeholder);
  this.setOverlayDb(self.getMap().getOverlayByName(params.panelName));
  this._createEventHandlers();

  this._tabIdCount = 0;

}

AbstractPanel.prototype = Object.create(Panel.prototype);
AbstractPanel.prototype.constructor = AbstractPanel;

AbstractPanel.prototype._createEventHandlers = function () {
  var self = this;
  var searchButton = self.getControlElement(PanelControlElementType.SEARCH_BUTTON);
  var searchInput = self.getControlElement(PanelControlElementType.SEARCH_INPUT);

  var searchByQuery = function () {
    var busyImage = $("[name=busyImage]", self.getElement());
    busyImage.show();
    return self.searchByQuery().finally(function () {
      busyImage.hide();
    });
  };

  searchButton.onclick = searchByQuery;
  searchInput.onkeypress = function (event) {
    if (event.keyCode === 13) {
      return searchByQuery().then(null, GuiConnector.alert);
    }
  };

  $(searchInput).typeahead({
    minLength: 1,
    hint: true,
    highlight: true
  }, {
    source: function (query, callback) {
      callback(self.getAutocomplete(query.toLowerCase()));
    }
  });

  $(searchInput).on('typeahead:select', function () {
    searchByQuery();
  });

  self.getOverlayDb().addListener("onSearch", function () {
    return self.refreshSearchResults();
  });

  self.getOverlayDb().addListener("onClear", function () {
    searchInput.value = "";
  });

};

AbstractPanel.prototype._initializeGui = function (placeholder) {
  var searchQueryDiv = Functions.createElement({
    type: "div",
    name: "searchQuery",
    className: "searchPanel"
  });
  this.getElement().appendChild(searchQueryDiv);
  this.setControlElement(PanelControlElementType.SEARCH_DIV, searchQueryDiv);

  var busyImage = Functions.createElement({
    type: "img",
    name: "busyImage",
    style: "display:block;position: absolute;margin-left: 80px;z-index: 10;display:none",
    src: "resources/images/icons/ajax-loader.gif"
  });
  searchQueryDiv.appendChild(busyImage);

  var searchLabel = Functions.createElement({
    type: "div",
    name: "searchLabel",
    content: "SEARCH:"
  });
  searchQueryDiv.appendChild(searchLabel);
  this.setControlElement(PanelControlElementType.SEARCH_LABEL, searchLabel);

  var searchInputDiv = Functions.createElement({
    type: "table"
  });
  searchQueryDiv.appendChild(searchInputDiv);

  var searchInputRow = Functions.createElement({
    type: "tr"
  });
  searchInputDiv.appendChild(searchInputRow);

  var searchInputCell = Functions.createElement({
    type: "td"
  });
  searchInputRow.appendChild(searchInputCell);

  var searchInput = Functions.createElement({
    type: "input",
    name: "searchInput",
    className: "input-field typeahead"
  });
  if (placeholder !== undefined) {
    searchInput.placeholder = placeholder;
  }
  searchInputCell.appendChild(searchInput);
  this.setControlElement(PanelControlElementType.SEARCH_INPUT, searchInput);

  var searchButtonCell = Functions.createElement({
    type: "td"
  });
  searchInputRow.appendChild(searchButtonCell);

  var searchButton = Functions.createElement({
    type: "a",
    content: '<img src="resources/images/icons/search.png"/>',
    xss: false
  });
  searchButton.href = "#";
  searchButtonCell.appendChild(searchButton);
  this.setControlElement(PanelControlElementType.SEARCH_BUTTON, searchButton);

  var searchResultsDiv = Functions.createElement({
    type: "div",
    name: "searchResults",
    className: "tabbable boxed parentTabs"
  });
  this.getElement().appendChild(searchResultsDiv);
  this.setControlElement(PanelControlElementType.SEARCH_RESULTS_DIV, searchResultsDiv);

  var searchResultsNavTabDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs",
    content: '<li class="active"><a href="#set1"/></li>',
    xss: false
  });
  searchResultsDiv.appendChild(searchResultsNavTabDiv);
  this.setControlElement(PanelControlElementType.SEARCH_RESULTS_NAV_TAB, searchResultsNavTabDiv);

  var searchResultsContentTabDiv = Functions.createElement({
    type: "div",
    className: "tab-content",
    content: '<div class="tab-pane fade active in" name="set1" id="set1"/>',
    xss: false
  });
  searchResultsDiv.appendChild(searchResultsContentTabDiv);
  this.setControlElement(PanelControlElementType.SEARCH_RESULTS_CONTENT_TAB, searchResultsContentTabDiv);

};

AbstractPanel.prototype.setOverlayDb = function (overlayDb) {
  if (overlayDb === undefined) {
    throw new Error("Undefined overlayDb for panel: " + this.getPanelName());
  }
  this._overlayDb = overlayDb;
};

/**
 *
 * @returns {AbstractDbOverlay}
 */
AbstractPanel.prototype.getOverlayDb = function () {
  return this._overlayDb;
};

AbstractPanel.prototype.clearResults = function () {
  var navElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_NAV_TAB);
  while (navElement.firstChild) {
    navElement.removeChild(navElement.firstChild);
  }

  var contentElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_CONTENT_TAB);
  while (contentElement.firstChild) {
    contentElement.removeChild(contentElement.firstChild);
  }
};

AbstractPanel.prototype.refreshSearchResults = function () {
  var self = this;
  self.clearResults();
  var searchDb = self.getOverlayDb();
  var queries = searchDb.getQueries();

  var promises = [];
  for (var i = 0; i < queries.length; i++) {
    promises.push(searchDb.getElementsByQuery(queries[i]));
  }
  return Promise.all(promises).then(function (results) {
    return Promise.each(queries, function(query, index){
      return self.addResultTab(query, results[index]);
    });
  }).then(function(){
    self.onresize();
  });
};

AbstractPanel.prototype.getAutocomplete = function () {
  logger.warn("Get autocomplete not implemented");
};

AbstractPanel.prototype.searchByQuery = function () {
  throw new Error("searchByQuery is not implemented");
};

AbstractPanel.prototype.addResultTab = function (query, elements) {
  var self = this;
  var name = JSON.parse(query).query;

  var tabId = this.getPanelName() + "Tab_" + this._tabIdCount;
  this._tabIdCount++;

  var navElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_NAV_TAB);
  var contentElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_CONTENT_TAB);
  var navClass = '';
  var contentClass = 'tab-pane';
  if (navElement.children.length === 0) {
    navClass = "active";
    contentClass = "tab-pane active";
  }

  var navLi = document.createElement("li");
  navLi.className = navClass;

  var navLink = Functions.createElement({
    type: "a",
    href: "#" + tabId,
    onclick: function () {
      $(this).tab('show');
    }
  });
  if (name !== undefined) {
    if (name.length > 12) {
      name = name.substring(0, 10) + "...";
    }
    navLink.innerHTML = name;
  }
  navLi.appendChild(navLink);
  if (query.name !== undefined) {
    navLink.innerHTML = query.name;
  }
  navElement.appendChild(navLi);

  var contentDiv = document.createElement("div");
  contentDiv.className = "pre-scrollable " + contentClass;
  contentDiv.style.maxHeight = "10px";

  contentDiv.id = tabId;

  contentElement.appendChild(contentDiv);

  contentDiv.appendChild(this.createPreamble(elements.element));

  var tableDiv = document.createElement("table");
  tableDiv.className = "table table-bordered";
  contentDiv.appendChild(tableDiv);
  var tableBody = document.createElement("tbody");
  tableDiv.appendChild(tableBody);

  return Promise.each(elements, function (entry) {
    var element = entry.element;
    var icon = entry.icon;
    return self.createTableElement(element, icon).then(function (div) {
      return tableBody.appendChild(div);
    });
  });
};

/**
 *
 * @returns {Promise<HTMLTableRowElement>}
 */
AbstractPanel.prototype.createTableElement = function () {
  throw new Error("Not implemented");
};

/**
 *
 * @param {Target} target
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
AbstractPanel.prototype.createTargetRow = function (target, icon) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("tr");
  var iconColumn = document.createElement("td");
  iconColumn.style.width = "20px";
  iconColumn.style.verticalAlign = "middle";
  iconColumn.style.textAlign = "center";
  result.appendChild(iconColumn);
  var submaps = [];
  var i;
  if (target.getTargetElements().length > 0) {
    var submapAddedIds = [];
    iconColumn.appendChild(guiUtils.createIcon(icon));
    var checkbox = document.createElement('input');
    checkbox.type = "checkbox";
    checkbox.checked = target.isVisible();
    checkbox.onclick = function () {
      target.setIsVisible(!target.isVisible());
      self.getOverlayDb().callListeners("onTargetVisibilityChange");
    };

    iconColumn.appendChild(checkbox);
    var elements = target.getTargetElements();
    for (i = 0; i < elements.length; i++) {
      var elementId = elements[i].getModelId();
      if (elementId !== self.getMap().getId() && !submapAddedIds[elementId]) {
        submaps.push(elementId);
        submapAddedIds[elementId] = true;
      }
    }
  }

  var descColumn = document.createElement("td");
  result.appendChild(descColumn);

  descColumn.appendChild(guiUtils.createParamLine("Name: ", target.getName()));
  descColumn.appendChild(guiUtils.createAnnotations("Elements: ", target.getTargetParticipants(), {
    showType: false,
    inline: true,
    groupAnnotations: false
  }));
  descColumn.appendChild(guiUtils.createAnnotations("References: ", target.getReferences(), {
    showType: false,
    groupAnnotations: false
  }));
  if (submaps.length > 0) {
    descColumn.appendChild(guiUtils.createLabel("Available in submaps: "));
    for (i = 0; i < submaps.length; i++) {
      var model = self.getMap().getSubmapById(submaps[i]).getModel();
      var onclick = (function () {
        var id = model.getId();
        return function () {
          self.getMap().openSubmap(id);
        };
      }());
      var button = Functions.createElement({
        type: "button",
        className: "minerva-open-submap-button",
        content: model.getName(),
        onclick: onclick
      });
      descColumn.appendChild(button);
    }
  }
  return Promise.resolve(result);
};

AbstractPanel.prototype.computeAutocompleteDictionary = function (queries) {
  var result = {};

  var i, j, k, mainString, substring, list;
  //compute dict for prefixes
  for (i = 0; i < queries.length; i++) {
    mainString = queries[i].toLowerCase();
    for (j = 0; j < mainString.length; j++) {
      substring = mainString.substring(0, j + 1);
      if (result[substring] !== undefined) {
        continue;
      }

      list = [];
      for (k = 0; k < 5; k++) {
        if (k + i >= queries.length) {
          break;
        } else if (queries[k + i].toLowerCase().indexOf(substring.toLowerCase()) === 0) {
          list.push(queries[k + i]);
        }
      }

      result[substring] = list;
    }
  }

  //and now we add substring from inside
  for (i = 0; i < queries.length; i++) {
    mainString = queries[i].toLowerCase();
    for (var startSubstring = 1; startSubstring < mainString.length; startSubstring++) {
      for (var endSubstring = startSubstring + 1; endSubstring <= mainString.length; endSubstring++) {
        substring = mainString.substring(startSubstring, endSubstring);
        if (mainString.indexOf(substring) !== 0) {
          if (result[substring] === undefined) {
            result[substring] = [];
          }
          if (result[substring].length < 5) {
            var found = false;
            for (k = 0; k < result[substring].length; k++) {
              found |= result[substring][k] === mainString;
            }
            if (!found) {
              result[substring].push(mainString);
            }
          }
        }
      }
    }
  }

  return result;
};


module.exports = AbstractPanel;
