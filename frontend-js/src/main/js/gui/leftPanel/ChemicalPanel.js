"use strict";

var Promise = require("bluebird");

/* exported logger */
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var AbstractDbPanel = require('./AbstractDbPanel');
var PanelControlElementType = require('../PanelControlElementType');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbPanel
 */
function ChemicalPanel(params) {
  var self = this;
  params.panelName = "chemical";
  params.placeholder = "full chemical name (CTD)";


  AbstractDbPanel.call(self, params);

  if (self.getMap().getProject().getDisease() === undefined) {
    self.disablePanel("The Comparative Toxicogenomics Database (CTD) requires a disease context. " +
      "Choose an appropriate MeSH code and provide it in the via the Admin section of this MINERVA platform. " +
      "See User Manual for more details ");
  } else {
    self.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML = "SEARCH FOR TARGETS OF:";
  }
}

ChemicalPanel.prototype = Object.create(AbstractDbPanel.prototype);
ChemicalPanel.prototype.constructor = ChemicalPanel;

ChemicalPanel.prototype.createPreamble = function (chemical) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("div");
  if (chemical === undefined || chemical.getName() === undefined) {
    result.appendChild(guiUtils.createLabel("NOT FOUND"));
  } else {
    result.appendChild(guiUtils.createParamLine("Chemical: ", chemical.getName()));
    result.appendChild(guiUtils.createParamLine("Description: ", chemical.getDescription()));
    result.appendChild(guiUtils.createArrayParamLine("Synonyms: ", chemical.getSynonyms()));
    result.appendChild(guiUtils.createParamLine("Direct Evidence: ", chemical.getDirectEvidence()));
    result.appendChild(guiUtils
      .createAnnotations("Direct Evidence Publications: ", chemical.getDirectEvidenceReferences(), {groupAnnotations: false}));
    result.appendChild(guiUtils.createAnnotations("Sources: ", chemical.getReferences()));
    result.appendChild(guiUtils.createNewLine());
  }

  return result;
};

ChemicalPanel.prototype.createTableElement = function (target, icon) {
  return this.createTargetRow(target, icon);
};

ChemicalPanel.prototype.searchByQuery = function () {
  var self = this;
  var query = self.getControlElement(PanelControlElementType.SEARCH_INPUT).value;

  return self.getOverlayDb().searchByQuery(query);
};

ChemicalPanel.prototype.init = function () {
  var self = this;
  return self.getToolTipForAnnotation(self.getProject().getDisease()).then(function (toolTip) {
    self.setHelpTip(toolTip);
    var query = ServerConnector.getSessionData().getChemicalQuery();
    if (query !== undefined) {
      return self.getOverlayDb().searchByEncodedQuery(query);
    }
  });
};

ChemicalPanel.prototype.destroy = function () {
  return Promise.resolve();
};

ChemicalPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

ChemicalPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getChemicalSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

ChemicalPanel.prototype.getToolTipForAnnotation = function (annotation) {
  var self = this;
  var promise = Promise.resolve('disease');
  if (annotation !== null && annotation !== undefined) {
    promise = ServerConnector.getMesh({id: annotation.getResource()}).then(function (mesh) {
      return mesh.getName() + " (" + self.getGuiUtils().createAnnotationLink(annotation).outerHTML + ")";
    });
  }
  return promise.then(function (diseaseString) {
    var result = '<p>source: Comparative Toxicogenomics Database <a target="_ctd" href="http://ctdbase.org/">ctdbase.org</a></p>'
      + '<p>only associations between genes and chemicals with direct evidence to '
      + diseaseString + ' are displayed</p>'
      + '<p>use only the full name of chemicals according to <a target="_ctd_chemicals" href="http://ctdbase.org/voc.go?type=chem"> ctdbase/chem</a> for search</p>'
      + 'if the chemical name includes comma(s), place a semicolon behind the name to avoid a segmentation of the name</p>'
      + '<p>separate multiple search by semicolon';
    return Promise.resolve(result);
  });
};

module.exports = ChemicalPanel;
