"use strict";

var Promise = require("bluebird");

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var AbstractDbOverlay = require('../../map/overlay/AbstractDbOverlay');
var Alias = require('../../map/data/Alias');
var GuiUtils = require('./GuiUtils');
var Header = require('../Header');
var LoginDialog = require('../LoginDialog');
var OverlayPanel = require('./OverlayPanel');
var PointData = require('../../map/data/PointData');
var ProjectInfoPanel = require('./ProjectInfoPanel');
var Reaction = require('../../map/data/Reaction');
var SearchPanel = require('./SearchPanel');
var SubmapPanel = require('./SubmapPanel');

var Functions = require('../../Functions');
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function LeftPanel(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  this._tabIdCount = 0;
  this._panels = [];

  self._createPanelGui();

}

LeftPanel.prototype = Object.create(AbstractGuiElement.prototype);
LeftPanel.prototype.constructor = LeftPanel;

LeftPanel.prototype._createPanelGui = function () {
  var self = this;
  var panels = self.getPanelsDefinition();

  var headerDiv = Functions.createElement({
    type: "div",
    id: "headerPanel"
  });
  var header = new Header({
    element: headerDiv,
    customMap: self.getMap(),
    optionsMenu: true
  });
  self.getElement().appendChild(headerDiv);

  var loginDialogDiv = Functions.createElement({
    type: "div",
    name: "loginDialog",
    style: "display:none"
  });
  self.getElement().appendChild(loginDialogDiv);
  this.setLoginDialog(new LoginDialog({
    element: loginDialogDiv,
    customMap: self.getMap()
  }));

  var guiUtils = self.getGuiUtils();
  var tabData = guiUtils.createTabDiv({
    element: self.getElement(),
    id: "left_panel"
  });

  self.setHeader(header);

  self.elementInfoDiv = Functions.createElement({
    name: "elementInfoDiv",
    type: "div",
    style: "background-color:#f3f3f3",
    className: "minerva-element-info-div"
  });

  for (var i = 0; i < panels.length; i++) {
    self.addTab(panels[i], tabData);
  }
};

LeftPanel.prototype.getPanelsDefinition = function () {
  return [{
    name: "SEARCH",
    className: "fa-search",
    panelClass: SearchPanel
  }, {
    name: "OVERLAYS",
    className: "fa-th-list",
    panelClass: OverlayPanel
  }, {
    name: "SUBMAPS",
    className: "fa-sitemap",
    panelClass: SubmapPanel
  }, {
    name: "INFO",
    className: "fa-info",
    panelClass: ProjectInfoPanel
  }];
};

LeftPanel.prototype.hideTab = function (panel) {
  var self = this;
  var panelDefinitions = self.getPanelsDefinition();
  for (var i = 0; i < panelDefinitions.length; i++) {
    if (panel instanceof panelDefinitions[i].panelClass) {
      var liElement = $("li:has(a[data='left_tab_" + i + "'])", $(self.getElement()))[0];
      if (liElement !== undefined) {
        liElement.style.display = "none";
      } else {
        logger.warn("Cannot find tab link for panel: " + panel.getPanelName());
      }
    }
  }
};

LeftPanel.prototype.init = function () {
  var self = this;

  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].init());
  }
  promises.push(self.getHeader().init());

  var initEvents = new Promise(function (resolve) {
    self.getMap().addListener("onBioEntityClick", function (e) {
      return self.showElementDetails(e.arg);
    });
    self.getMap().getOverlayByName("search").addListener("onSearch", function (e) {
      if (e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES ||
        e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET) {
        return self.showElementDetails(e.arg.identifiedElements[0][0]);
      } else {
        return self.showElementDetails(undefined);
      }
    });
    resolve();
  });
  promises.push(initEvents);
  promises.push(self.getLoginDialog().init());
  return Promise.all(promises);
};

LeftPanel.prototype.showElementDetails = function (element) {
  var self = this;
  var div = self.elementInfoDiv;
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      resizable: false,
      width: $(self.getElement()).width(),
      height: 200,
      beforeclose: function () {
        $(this).dialog('option', 'position', [$(this).offset().left, $(this).offset().top]);
        $(this).dialog('option', 'width', $(this).width());
        $(this).dialog('option', 'height', $(this).height());
      },
      position: {
        my: "left bottom",
        at: "left bottom",
        of: $(self.getElement())
      }
    }).siblings('.ui-dialog-titlebar').css("background", "gray");
  }

  var openedTabs = $("[name='tabView'] > ul li.active a > .maintabdiv");
  var openTabName = openedTabs[0].innerHTML;
  var searchTabName = openedTabs[1].innerHTML;
  var isPanelHidden = (self.getElement().style.display === "none");
  if (isPanelHidden) {
    openTabName = undefined;
  }

  if (element !== undefined && openTabName !== undefined && (openTabName.indexOf("SEARCH") === -1 || searchTabName !== "GENERIC")) {
    var model = self.getMap().getSubmapById(element.getModelId()).getModel();
    var bioEntity;
    return model.getByIdentifiedElement(element, true).then(function (result) {
      bioEntity=result;
      return self.prepareElementDetailsContent(bioEntity);
    }).then(function(elementDetailsDiv){
      div.innerHTML = "";
      div.appendChild(elementDetailsDiv);
      $(div).dialog("open");
      $(div).dialog("option", "title", self.getElementTitle(bioEntity));
      $(div).scrollTop(0);
    });
  } else {
    $(div).dialog("close");
    return Promise.resolve();
  }
};

/**
 *
 * @param {BioEntity} bioEntity
 *
 * @returns {Promise<HTMLDivElement>}
 */
LeftPanel.prototype.prepareElementDetailsContent = function (bioEntity) {
  var guiUtils = this.getGuiUtils();
  if (bioEntity instanceof Reaction) {
    return guiUtils.createReactionElement({
      reaction: bioEntity,
      showTitle: false
    });
  } else if (bioEntity instanceof Alias) {
    return guiUtils.createAliasElement({
      alias: bioEntity,
      showTitle: false
    });
  } else if (bioEntity instanceof PointData) {
    return Promise.resolve(Functions.createElement({
      type: "div"
    }));
  } else {
    throw new Error("Unknown element type:" + bioEntity);
  }
};

/**
 *
 * @returns {GuiUtils}
 */
LeftPanel.prototype.getGuiUtils = function () {
  var self = this;
  if (self._guiUtils === undefined) {
    self._guiUtils = new GuiUtils(self.getMap().getConfiguration());
    self._guiUtils.setMap(self.getMap());
  }
  return self._guiUtils;
};

/**
 *
 * @param {BioEntity} bioEntity
 * @returns {string}
 */
LeftPanel.prototype.getElementTitle = function (bioEntity) {
  if (bioEntity instanceof Reaction) {
    return bioEntity.getType() + ": " + bioEntity.getReactionId();
  } else if (bioEntity instanceof Alias) {
    return bioEntity.getType() + ": " + bioEntity.getName();
  } else if (bioEntity instanceof PointData) {
    return "POINT: " + bioEntity.getId();
  } else {
    throw new Error("Unknown element type:" + bioEntity);
  }
};

LeftPanel.prototype.addTab = function (params, tabData) {
  var self = this;

  var name = params.name;
  if (name !== undefined) {
    if (name.length > 12) {
      name = name.substring(0, 10) + "...";
    }
  } else {
    name = "";
  }


  var guiUtils = self.getGuiUtils();
  var data = guiUtils.createTab({
    tabData: tabData,
    title: '<div class="maintabdiv"><i class="fa ' + params.className + ' maintab"></i><br>' + name
    + '</div>',
    content: ""
  });

  $(data.title).attr("data", "left_tab_" + this._panels.length);

  this._panels.push(new params.panelClass({
    element: data.content,
    customMap: self.getMap(),
    configuration: self.getConfiguration(),
    parent: self
  }));

};

LeftPanel.prototype.hide = function () {
  this.getElement().style.display = "none";
};
LeftPanel.prototype.show = function () {
  this.getElement().style.display = "block";
};

LeftPanel.prototype.setHeader = function (header) {
  this._header = header;
};

LeftPanel.prototype.getHeader = function () {
  return this._header;
};

LeftPanel.prototype.setLoginDialog = function (loginDialog) {
  this._loginDialog = loginDialog;
};

LeftPanel.prototype.getLoginDialog = function () {
  return this._loginDialog;
};

LeftPanel.prototype.setPluginManager = function (pluginManager) {
  this._pluginManager = pluginManager;
  this.getHeader().setPluginManager(pluginManager);
};

LeftPanel.prototype.getPluginManager = function () {
  return this._pluginManager;
};

LeftPanel.prototype.destroy = function () {
  var self = this;
  var promises = [];
  promises.push(self.getHeader().destroy());
  var div = self.elementInfoDiv;

  var destroyPanel = new Promise(function (resolve) {
    if ($(div).hasClass("ui-dialog-content")) {
      $(div).dialog("destroy");
    }
    resolve();
  });
  promises.push(destroyPanel);
  promises.push(self.getLoginDialog().destroy());
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].destroy());
  }

  if (self._pluginManager !== undefined) {
    promises.push(self._pluginManager.destroy());
  }

  return Promise.all(promises);
};

LeftPanel.prototype.setGoogleLicenseConsentRequired = function (value) {
  this._googleLicenseConsentRequired = value;
};

LeftPanel.prototype.isGoogleLicenseConsentRequired = function () {
  return this._googleLicenseConsentRequired;
};

module.exports = LeftPanel;
