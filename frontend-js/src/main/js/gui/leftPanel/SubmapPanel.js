"use strict";

var Promise = require("bluebird");

var GuiConnector= require('../../GuiConnector');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');

function SubmapPanel(params) {
  params.panelName = "submap";
  params.scrollable = true;
  params.helpTip = "The Submaps tab summarizes all the submap networks uploaded together and linked to the main network of <b>'"
    + params.parent.getMap().getProject().getName() + "'</b> project.";
  Panel.call(this, params);

  var self = this;
  self._createSubmapGui();
}

SubmapPanel.prototype = Object.create(Panel.prototype);
SubmapPanel.prototype.constructor = SubmapPanel;

SubmapPanel.prototype._createSubmapGui = function () {
  var submapDiv = Functions.createElement({
    type: "div",
    name: "submapDiv",
    className: "searchPanel"
  });
  this.getElement().appendChild(submapDiv);
  this.setControlElement(PanelControlElementType.SUBMAP_DIV, submapDiv);

};

SubmapPanel.prototype.createRow = function (model) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("tr");

  var nameTd = document.createElement("td");
  nameTd.innerHTML = model.getName();
  result.appendChild(nameTd);

  var openTd = document.createElement("td");

  if (model.getId() !== self.getMap().getId()) {
    var img = guiUtils.createIcon("icons/search.png");
    var link = document.createElement("a");
    link.href = "#";
    link.onclick = function () {
      return self.getMap().openSubmap(model.getId()).catch(GuiConnector.alert);
    };
    link.appendChild(img);
    openTd.appendChild(link);
  }

  result.appendChild(openTd);

  return result;
};

SubmapPanel.prototype.createTableHeader = function () {
  var result = document.createElement("thead");

  var row = document.createElement("tr");

  var nameTd = document.createElement("th");
  nameTd.innerHTML = "Name";
  row.appendChild(nameTd);

  row.appendChild(Functions.createElement({
    type: "th",
    style: "width: 60px;",
    content: "View"
  }));

  result.appendChild(row);
  return result;
};

SubmapPanel.prototype.init = function () {
  var self = this;
  return new Promise(function (resolve) {
    var div = self.getControlElement(PanelControlElementType.SUBMAP_DIV);
    div.innerHTML = "";
    var models = self.getMap().getProject().getModels();
    var modelsByType = [];
    var types = [];
    var i;
    for (i = 1; i < models.length; i++) {
      var model = models[i];
      if (modelsByType[model.getSubmodelType()] === undefined) {
        modelsByType[model.getSubmodelType()] = [];
        types.push(model.getSubmodelType());
      }
      modelsByType[model.getSubmodelType()].push(model);
    }
    for (i = 0; i < types.length; i++) {
      var type = types[i];
      var tableName = type + " submaps";
      if (type === "UNKNOWN") {
        tableName = ""
      }
      div.appendChild(self.createTable(modelsByType[type], tableName));
    }
    if (models.length <= 1) {
      self.getParent().hideTab(self);
    }
    return resolve();
  });
};

SubmapPanel.prototype.createTable = function (models, type) {
  var self = this;
  var result = Functions.createElement({
    type: "div"
  });

  var title = Functions.createElement({
    type: "h5",
    content: type
  });
  result.appendChild(title);

  var table = Functions.createElement({
    type: "table",
    className: "table table-bordered",
    style: "width:100%"
  });
  result.appendChild(table);

  table.appendChild(self.createTableHeader());
  var tableBody = Functions.createElement({
    type: "tbody"
  });
  table.appendChild(tableBody);

  for (var i = 0; i < models.length; i++) {
    tableBody.appendChild(self.createRow(models[i]));
  }
  return result;
};

SubmapPanel.prototype.destroy = function () {
  return Promise.resolve();
};

module.exports = SubmapPanel;
