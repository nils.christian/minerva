"use strict";

var Promise = require("bluebird");

/* exported logger */

var AbstractDbPanel = require('./AbstractDbPanel');
var Alias = require('../../map/data/Alias');
var InvalidArgumentError = require('../../InvalidArgumentError');
var PanelControlElementType = require('../PanelControlElementType');
var Reaction = require('../../map/data/Reaction');
var SearchBioEntityGroup = require('../../map/data/SearchBioEntityGroup');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');

function GenericSearchPanel(params) {
  params.panelName = "search";
  params.helpTip = "<p>search tab allows to search for particular elements or interactions in the map</p>"
    + "<p>perfect match tick box active: only terms with an exact match to the query will be returned</p>"
    + "<p>separate multiple search by semicolon</p>";
  params.placeholder = "keyword";

  AbstractDbPanel.call(this, params);

  this.createSearchGui();
}

GenericSearchPanel.prototype = Object.create(AbstractDbPanel.prototype);
GenericSearchPanel.prototype.constructor = GenericSearchPanel;

GenericSearchPanel.prototype.createSearchGui = function () {
  var searchDiv = this.getControlElement(PanelControlElementType.SEARCH_DIV);

  var perfectMatchCheckbox = Functions.createElement({
    type: "input",
    name: "searchPerfectMatch",
    inputType: "checkbox"
  });
  searchDiv.appendChild(perfectMatchCheckbox);
  this.setControlElement(PanelControlElementType.SEARCH_PERFECT_MATCH_CHECKBOX, perfectMatchCheckbox);

  var perfectMatchLabel = Functions.createElement({
    type: "span",
    content: "PERFECT MATCH"
  });
  searchDiv.appendChild(perfectMatchLabel);
};

/**
 *
 * @param {SearchBioEntityGroup|BioEntity} element
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createTableElement = function (element, icon) {
  if (element instanceof Alias) {
    return this.createAliasElement(element, icon);
  } else if (element instanceof Reaction) {
    return this.createReactionElement(element);
  } else if (element instanceof SearchBioEntityGroup) {
    return this.createSearchBioEntityGroupElement(element);
  } else {
    throw new Error("Unknown element type: " + element.constructor.name);
  }
};

GenericSearchPanel.prototype.createPreamble = function () {
  return document.createElement("div");
};

/**
 *
 * @param {Reaction} reaction
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createReactionElement = function (reaction) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("tr");
  var td = document.createElement("td");
  result.appendChild(td);

  return guiUtils.createReactionElement({reaction: reaction}).then((function (div) {
    div.appendChild(guiUtils.createSeparator());
    td.appendChild(div);
    return result;
  }));
};

/**
 *
 * @param {Alias} alias
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createAliasElement = function (alias, icon) {
  var self = this;
  var guiUtils = self.getGuiUtils();

  var result = document.createElement("tr");
  var td = document.createElement("td");
  result.appendChild(td);
  return guiUtils.createAliasElement({
    alias: alias,
    icon: icon
  }).then(function (div) {
    div.appendChild(guiUtils.createSeparator());
    td.appendChild(div);

    return result;
  });

};

/**
 *
 * @param {SearchBioEntityGroup} group
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createSearchBioEntityGroupElement = function (group) {
  var self = this;
  var guiUtils = self.getGuiUtils();

  var result = document.createElement("tr");
  var td = document.createElement("td");
  result.appendChild(td);
  return guiUtils.createSearchBioEntityGroupElement(group).then(function (div) {
    div.appendChild(guiUtils.createSeparator());
    td.appendChild(div);

    return result;
  });

};

GenericSearchPanel.prototype.searchByQuery = function () {
  var self = this;
  var query = this.getControlElement(PanelControlElementType.SEARCH_INPUT).value;
  var perfect = this.getControlElement(PanelControlElementType.SEARCH_PERFECT_MATCH_CHECKBOX).checked;
  return self.getOverlayDb().searchByQuery(query, perfect, true);
};

GenericSearchPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

GenericSearchPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

GenericSearchPanel.prototype.init = function () {
  var query = ServerConnector.getSessionData().getSearchQuery();
  if (query !== undefined) {
    return this.getOverlayDb().searchByEncodedQuery(query, false).catch(function (error) {
      if (error instanceof InvalidArgumentError) {
        logger.warn(error.message);
      } else {
        throw error;
      }
    });
  } else {
    return Promise.resolve();
  }
};


module.exports = GenericSearchPanel;
