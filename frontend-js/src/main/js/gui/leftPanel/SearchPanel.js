"use strict";

var Promise = require("bluebird");

/* exported logger */

var Panel = require('../Panel');
var ChemicalPanel = require('./ChemicalPanel');
var DrugPanel = require('./DrugPanel');
var MiRnaPanel = require('./MiRnaPanel');
var GenericSearchPanel = require('./GenericSearchPanel');

var Functions = require('../../Functions');
var logger = require('../../logger');

function SearchPanel(params) {
  params.panelName = "global-search";
  Panel.call(this, params);
  var self = this;

  this._tabIdCount = 0;
  this._panels = [];

  self._createPanelGui();

}

SearchPanel.prototype = Object.create(Panel.prototype);
SearchPanel.prototype.constructor = SearchPanel;

SearchPanel.prototype._createPanelGui = function() {
  var self = this;
  var panels = self.getPanelsDefinition();

  var tabDiv = Functions.createElement({
    type : "div",
    name : "tabView",
    className : "tabbable boxed parentTabs"
  });
  self.getElement().appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type : "ul",
    className : "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type : "div",
    className : "tab-content"
  });
  tabDiv.appendChild(tabContentDiv);

  for (var i = 0; i < panels.length; i++) {
    self.addTab(panels[i], tabMenuDiv, tabContentDiv);
  }
};

SearchPanel.prototype.getPanelsDefinition = function() {
  return [ {
    name : "GENERIC",
    className : "fa-search",
    panelClass : GenericSearchPanel
  }, {
    name : "DRUG",
    className : "fa-map-marker",
    panelClass : DrugPanel
  }, {
    name : "CHEMICAL",
    className : "fa-map-marker",
    panelClass : ChemicalPanel
  }, {
    name : "MiRNA",
    className : "fa-map-marker",
    panelClass : MiRnaPanel
  } ];
};

SearchPanel.prototype.hideTab = function(panel) {
  var self = this;
  var panelDefinitions = self.getPanelsDefinition();
  for (var i = 0; i < panelDefinitions.length; i++) {
    if (panel instanceof panelDefinitions[i].panelClass) {
      var liElement = $("li:has(a[href='#search_panel_tab_" + i + "'])", $(self.getElement()))[0];
      if (liElement !== undefined) {
        liElement.style.display = "none";
      } else {
        logger.warn("Cannot find tab link for panel: " + panel.getPanelName());
      }
    }
  }
};

SearchPanel.prototype.init = function() {
  var self = this;

  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].init());
  }
  return Promise.all(promises);
};

SearchPanel.prototype.addTab = function(params, navElement, contentElement) {
  var self = this;

  var name = params.name;

  var tabId = "search_panel_tab_" + this._tabIdCount;
  self._tabIdCount++;

  var navClass = '';
  var contentClass = 'tab-pane';
  if (navElement.children.length === 0) {
    navClass = "active";
    contentClass = "tab-pane active";
  }

  var navLi = document.createElement("li");
  navLi.className = navClass;

  var navLink = document.createElement("a");
  navLink.href = "#" + tabId;
  if (name !== undefined) {
    if (name.length > 12) {
      name = name.substring(0, 10) + "...";
    }
  } else {
    name = "";
  }
  navLink.innerHTML = "<div class='maintabdiv'>" + name + "</div>";

  navLink.onclick = function() {
    $(this).tab('show');
  };

  navLi.appendChild(navLink);

  navElement.appendChild(navLi);

  var contentDiv = document.createElement("div");
  contentDiv.style.height = "100%";
  contentDiv.className = contentClass;
  contentDiv.id = tabId;

  contentElement.appendChild(contentDiv);

  this._panels.push(new params.panelClass({
    element : contentDiv,
    customMap : self.getMap(),
    parent : self
  }));
};

SearchPanel.prototype.destroy = function () {
    return Promise.resolve();
};


module.exports = SearchPanel;
