"use strict";

var Promise = require("bluebird");

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var Alias = require('../../map/data/Alias');
var GuiConnector = require('../../GuiConnector');
var IdentifiedElement = require('../../map/data/IdentifiedElement');
var Reaction = require('../../map/data/Reaction');

var Functions = require('../../Functions');
var logger = require('../../logger');

function PublicationListDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createPublicationListDialogGui();
}

PublicationListDialog.prototype = Object.create(AbstractGuiElement.prototype);
PublicationListDialog.prototype.constructor = PublicationListDialog;

PublicationListDialog.prototype.createPublicationListDialogGui = function () {
  var self = this;
  var head = Functions.createElement({
    type: "thead",
    content: "<tr>" + "<th>Pubmed ID</th>" + //
    "<th>Title</th>" + //
    "<th>Authors</th>" + //
    "<th>Journal</th>" + //
    "<th>Year</th>" + //
    "<th>Elements on map</th>" + //
    "<th>Submaps</th>" + //
    "</tr>"//
  });
  var body = Functions.createElement({
    type: "tbody"
  });
  var tableElement = Functions.createElement({
    type: "table",
    className: "minerva-publication-table",
    style: "width: 100%"
  });

  tableElement.appendChild(head);
  tableElement.appendChild(body);

  self.tableElement = tableElement;
  self.getElement().appendChild(tableElement);
};

PublicationListDialog.prototype._dataTableAjaxCall = function (data, callback) {
  var self = this;
  return ServerConnector.getPublications({
    start: data.start,
    length: data.length,
    sortColumn: self.getColumnsDefinition()[data.order[0].column].name,
    sortOrder: data.order[0].dir,
    search: data.search.value
  }).then(function (publicationList) {
    var out = [];
    var allElements = [];
    for (var i = 0; i < publicationList.data.length; i++) {
      var publication = publicationList.data[i].publication.article;
      var elements = publicationList.data[i].elements;

      var row = [];
      var submaps = {};
      submaps[self.getMap().getId()] = true;
      row[0] = "<a href='" + publication.link + "'>" + publication.id + "</a>";
      row[1] = publication.title;
      row[2] = publication.authors.join();
      row[3] = publication.journal;
      row[4] = publication.year;
      row[5] = "<div>";
      row[6] = "<div>";
      for (var j = 0; j < elements.length; j++) {
        row[5] += "<a name='" + elements[j].id + "' href='#'>" + elements[j].type + ":" + elements[j].id + "</a>, ";
        allElements.push(new IdentifiedElement(elements[j]));
        var modelId = elements[j].modelId;
        if (submaps[modelId] === undefined) {
          row[6] += self.getMap().getSubmapById(modelId).getModel().getName() + ", ";
          submaps[elements[j].modelId] = true;
        }
      }
      row[5] += "</div>";
      row[6] += "</div>";
      out.push(row);
    }
    callback({
      draw: data.draw,
      recordsTotal: publicationList.totalSize,
      recordsFiltered: publicationList.filteredSize,
      data: out
    });
    var promises = [];
    allElements.forEach(function (element) {
      var model = self.getMap().getSubmapById(element.getModelId()).getModel();
      promises.push(model.getByIdentifiedElement(element, true).then(function (elementData) {
        var name = null;
        if (elementData instanceof Alias) {
          name = elementData.getName();
        } else if (elementData instanceof Reaction) {
          name = elementData.getReactionId();
        }
        $("a[name=" + elementData.getId() + "]", $(self.getElement())).html(name);
        var onclick = function () {
          var searchOverlay = self.getMap().getOverlayByName("search");
          var query;
          if (element.getType() === "ALIAS") {
            query = "element:" + element.getId();
          } else {
            query = "reaction:" + element.getId();
          }
          self.getMap().openSubmap(elementData.getModelId());
          return searchOverlay.searchByQuery(query, true, true).then(function () {
            $(self.getElement()).dialog("close");
          }).then(null, GuiConnector.alert);
        };
        $("a[name=" + elementData.getId() + "]", $(self.getElement())).click(onclick);
      }));
    });
    return Promise.all(promises);
  });
};

PublicationListDialog.prototype.show = function () {
  var self = this;
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      title: "Publication list",
      autoOpen: false,
      resizable: false,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  if (!$.fn.DataTable.isDataTable(self.tableElement)) {
    return new Promise(function (resolve) {
      $(self.tableElement).dataTable({
        serverSide: true,
        ordering: true,
        searching: true,
        ajax: function (data, callback, settings) {
          resolve(self._dataTableAjaxCall(data, callback, settings));
        },
        columns: self.getColumnsDefinition()
      });
    });
  } else {
    return Promise.resolve();
  }

};

PublicationListDialog.prototype.getColumnsDefinition = function () {
  return [{
    name: "pubmedId"
  }, {
    name: "title"
  }, {
    name: "authors"
  }, {
    name: "journal"
  }, {
    name: "year"
  }, {
    orderable: false,
    searchable: false,
    name: "elements"
  }, {
    orderable: false,
    searchable: false,
    name: "submaps"
  }];
};

PublicationListDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable(self.tableElement)) {
    $(self.tableElement).DataTable().destroy();
  }
};

module.exports = PublicationListDialog;
