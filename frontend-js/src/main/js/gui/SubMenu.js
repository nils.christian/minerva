"use strict";

/* exported logger */

var AbstractGuiElement = require('./AbstractGuiElement');
var Functions = require('../Functions');

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');

function SubMenu(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  self._createGui(params);

}

SubMenu.prototype = Object.create(AbstractGuiElement.prototype);
SubMenu.prototype.constructor = SubMenu;

SubMenu.prototype._createGui = function (params) {
  var self = this;

  var element = self.getElement();
  element.className = "dropdown-submenu";

  var link = Functions.createElement({
    type: "a",
    href: "#",
    className: "dropdown-link",
    content: params.name
  });
  link.tabIndex = -1;
  self.getElement().appendChild(link);

  var ul = Functions.createElement({
    type: "ul",
    className: "dropdown-menu"
  });
  self.getElement().appendChild(ul);
  self.setUl(ul);

};
SubMenu.prototype.addOption = function (name, handler) {
  var self = this;
  if (name instanceof SubMenu) {
    self.getElement().appendChild(name.getElement());
  } else {
    var option = Functions.createElement({
      type: "li"
    });
    var link = Functions.createElement({
      type: "a",
      href: "#",
      className: "dropdown-link",
      content: name
    });
    $(link).data("handler", handler);
    option.appendChild(link);
    self.getUl().appendChild(option);
  }
};

SubMenu.prototype.setUl = function (ul) {
  this._ul = ul;
};

SubMenu.prototype.getUl = function () {
  return this._ul;
};

module.exports = SubMenu;
