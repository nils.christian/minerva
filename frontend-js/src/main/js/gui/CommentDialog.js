"use strict";

/* exported logger */

var Promise = require("bluebird");

var AbstractGuiElement = require('./AbstractGuiElement');
var Alias = require('../map/data/Alias');
var Reaction = require('../map/data/Reaction');
// noinspection JSUnusedLocalSymbols
var logger = require('../logger');
var Functions = require('../Functions');

function CommentDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self._createGui();
  $(self.getElement()).dialog({
    autoOpen: false,
    resizable: false,
    width: window.innerWidth / 2,
    height: window.innerHeight / 2
  });
}

CommentDialog.prototype = Object.create(AbstractGuiElement.prototype);
CommentDialog.prototype.constructor = CommentDialog;

CommentDialog.GENERAL = "<General>";

function createRow(elements) {
  var row = document.createElement('tr');
  for (var i = 0; i < elements.length; i++) {
    var container = document.createElement('td');
    container.appendChild(elements[i]);
    row.appendChild(container);
  }
  return row;
}

CommentDialog.prototype.open = function (types) {
  var self = this;
  self.setTypes([CommentDialog.GENERAL]);

  var promises = [CommentDialog.GENERAL];
  for (var i = 0; i < types.length; i++) {
    var ie = types[i];
    if (ie.getType() === "ALIAS") {
      promises.push(self.getMap().getSubmapById(ie.getModelId()).getModel().getAliasById(ie.getId(), true));
    } else if (ie.getType() === "REACTION") {
      promises.push(self.getMap().getSubmapById(ie.getModelId()).getModel().getReactionById(ie.getId(), true));
    } else {
      throw new Error("Unknown element type: " + ie.getType());
    }
  }
  return Promise.all(promises).then(function (elements) {
    self.setTypes(elements);
    return self.getMap().getServerConnector().getLoggedUser();
  }).then(function (user) {
    if (user.getLogin() !== "anonymous") {
      self.setEmail(user.getEmail());
      self.setName(user.getName() + " " + user.getSurname());
    }

    $(self.getElement()).dialog("open");
  });
};

CommentDialog.prototype._createGui = function () {
  var self = this;

  var table = document.createElement('table');

  var typeLabel = document.createElement('label');
  typeLabel.innerHTML = "Type";
  var typeOptions = document.createElement("select");
  this.setTypeOptions(typeOptions);

  table.appendChild(createRow([typeLabel, typeOptions]));

  var detailDiv = document.createElement('div');

  table.appendChild(createRow([document.createElement('div'), detailDiv]));

  var pinnedLabel = document.createElement('label');
  pinnedLabel.innerHTML = "Pinned";
  var pinnedCheckbox = document.createElement('input');
  pinnedCheckbox.type = "checkbox";
  $(pinnedCheckbox).prop("checked", true);


  table.appendChild(createRow([pinnedLabel, pinnedCheckbox]));
  this.setPinnedCheckbox(pinnedCheckbox);

  var nameLabel = document.createElement('label');
  nameLabel.innerHTML = "Name:<br/>(Visible to moderators only)";
  var nameInput = document.createElement('input');
  nameInput.type = "text";

  table.appendChild(createRow([nameLabel, nameInput]));
  this.setNameInput(nameInput);

  var emailLabel = document.createElement('label');
  emailLabel.innerHTML = "Email:<br/>(Visible to moderators only)";
  var emailInput = document.createElement('input');
  emailInput.type = "text";

  table.appendChild(createRow([emailLabel, emailInput]));
  this.setEmailInput(emailInput);

  var contentLabel = document.createElement('label');
  contentLabel.innerHTML = "Content:";
  var contentInput = document.createElement('textarea');
  contentInput.cols = 80;
  contentInput.rows = 3;

  table.appendChild(createRow([contentLabel, contentInput]));
  this.setContentInput(contentInput);

  var sendButton = document.createElement('button');
  sendButton.innerHTML = "Send";
  sendButton.onclick = function () {
    return self.addComment().then(function () {
      $(self.getElement()).dialog("close");
    });
  };

  table.appendChild(createRow([sendButton]));

  self.getElement().appendChild(table);

  typeOptions.onchange = function () {
    var option = self.getSelectedType();
    var text = "";
    if (option instanceof Alias) {
      if (option.getFullName() !== undefined) {
        text = option.getFullName();
      }
    } else if (option instanceof Reaction) {
      text = "Reactants: ";
      var reactants = option.getReactants();
      var i;
      for (i = 0; i < reactants.length; i++) {
        text += reactants[i].getAlias().getName() + ",";
      }
      text += "<br/>";
      text += "Modifiers: ";
      var modifiers = option.getModifiers();
      for (i = 0; i < modifiers.length; i++) {
        text += modifiers[i].getAlias().getName() + ",";
      }
      text += "<br/>";
      text += "Products: ";
      var products = option.getProducts();
      for (i = 0; i < products.length; i++) {
        text += products[i].getAlias().getName() + ",";
      }
      text += "<br/>";
    }
    detailDiv.innerHTML = text;
  };
};
CommentDialog.prototype.setTypes = function (types) {
  var typeOptions = this.getTypeOptions();
  while (typeOptions.firstChild) {
    typeOptions.removeChild(typeOptions.firstChild);
  }

  for (var i = 0; i < types.length; i++) {
    var option = document.createElement("option");
    option.value = i.toString();
    var element = types[i];
    var text = element;
    if (element instanceof Alias) {
      text = element.getType() + ": " + element.getName();
    } else if (element instanceof Reaction) {
      text = "Reaction: " + element.getReactionId();
    }
    option.text = text;
    typeOptions.appendChild(option);
  }
  typeOptions.value = 0;

  this._types = types;
};

CommentDialog.prototype.getTypes = function () {
  return this._types;
};

CommentDialog.prototype.getSelectedType = function () {
  return this._types[this.getTypeOptions().value];
};

CommentDialog.prototype.setSelectedType = function (value) {
  if (Functions.isInt(value)) {
    this.getTypeOptions().value = value;
    this.getTypeOptions().onchange();
  } else {
    throw new Error("Unknown value type: " + value);
  }
};

CommentDialog.prototype.getTypeOptions = function () {
  return this._typeOptions;
};
CommentDialog.prototype.setTypeOptions = function (typeOptions) {
  this._typeOptions = typeOptions;
};
CommentDialog.prototype.setContentInput = function (contentInput) {
  this._contentInput = contentInput;
};
CommentDialog.prototype.getContentInput = function () {
  return this._contentInput;
};
CommentDialog.prototype.setNameInput = function (nameInput) {
  this._nameInput = nameInput;
};
CommentDialog.prototype.getNameInput = function () {
  return this._nameInput;
};
CommentDialog.prototype.setEmailInput = function (emailInput) {
  this._emailInput = emailInput;
};
CommentDialog.prototype.getEmailInput = function () {
  return this._emailInput;
};
CommentDialog.prototype.setPinnedCheckbox = function (pinnedCheckbox) {
  this._pinnedCheckbox = pinnedCheckbox;
};

CommentDialog.prototype.getPinnedCheckbox = function () {
  return this._pinnedCheckbox;
};

CommentDialog.prototype.getTypes = function () {
  return this._types;
};

CommentDialog.prototype.setName = function (name) {
  this.getNameInput().value = name;
};

CommentDialog.prototype.getName = function () {
  return this.getNameInput().value;
};

CommentDialog.prototype.getEmail = function () {
  return this.getEmailInput().value;
};

CommentDialog.prototype.setEmail = function (email) {
  this.getEmailInput().value = email;
};

CommentDialog.prototype.getContent = function () {
  return this.getContentInput().value;
};

CommentDialog.prototype.isPinned = function () {
  return this.getPinnedCheckbox().checked;
};
CommentDialog.prototype.getSelectedTypeId = function () {
  var selected = this.getSelectedType();
  if (selected instanceof Alias) {
    return selected.getId();
  } else if (selected instanceof Reaction) {
    return selected.getId();
  } else {
    return "";
  }
};

CommentDialog.prototype.getSelectedTypeClass = function () {
  var selected = this.getSelectedType();
  if (selected instanceof Alias) {
    return "ALIAS";
  } else if (selected instanceof Reaction) {
    return "REACTION";
  } else {
    return "POINT";
  }
};

/**
 *
 * @returns {Promise}
 */
CommentDialog.prototype.addComment = function () {
  var self = this;
  var name = self.getName();
  return self.getMap().getServerConnector().addComment({
    modelId: self.getMap().getActiveSubmapId(),
    coordinates: self.getMap().getActiveSubmapClickCoordinates(),
    name: name,
    email: self.getEmail(),
    content: self.getContent(),
    pinned: self.isPinned(),
    elementId: self.getSelectedTypeId(),
    elementType: self.getSelectedTypeClass()

  }).then(function (comment) {
    //if we visualize comments add it to data set
    if (self.getMap().getServerConnector().getSessionData(self.getProject()).getShowComments() && comment.isPinned()) {
      return self.getMap().getOverlayByName("comment").addComment(comment);
    } else {
      return [];
    }
  });
};

CommentDialog.prototype.destroy = function () {
  $(this.getElement()).dialog("destroy");
};

module.exports = CommentDialog;
