"use strict";

var Promise = require("bluebird");

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require('../../GuiConnector');
var OverviewDialog = require('../OverviewDialog');
var PanelControlElementType = require('../PanelControlElementType');

var Functions = require('../../Functions');
var logger = require('../../logger');

function TopMenu(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  self._createGui();
}

TopMenu.prototype = Object.create(AbstractGuiElement.prototype);
TopMenu.prototype.constructor = TopMenu;

TopMenu.prototype._createGui = function () {
  var self = this;

  var overviewDialogDiv = Functions.createElement({
    type: "div",
    name: "overviewDialog"
  });
  self.getElement().appendChild(overviewDialogDiv);
  self.setControlElement(PanelControlElementType.OVERVIEW_DIALOG_DIV, overviewDialogDiv);

  var hideButtonDiv = Functions.createElement({
    type: "div",
    className: "headerHideDivButton"
  });
  self.getElement().appendChild(hideButtonDiv);

  var hideButton = Functions.createElement({
    type: "button",
    className: "headerHideButton",
    name: "hideButton"
  });
  hideButtonDiv.appendChild(hideButton);
  self.setControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON, hideButton);

  var hideButtonIcon = Functions.createElement({
    type: "i",
    className: "fa fa-chevron-left",
    name: "hideButtonIcon"
  });
  hideButton.appendChild(hideButtonIcon);
  self.setControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON_ICON, hideButtonIcon);

  var versionDiv = Functions.createElement({
    type: "div",
    className: "headerTextBold",
    name: "versionDiv"
  });
  self.getElement().appendChild(versionDiv);
  self.setControlElement(PanelControlElementType.MENU_VERSION_DIV, versionDiv);

  var showOverviewDiv = Functions.createElement({
    type: "div",
    style: "float: left;"
  });
  self.getElement().appendChild(showOverviewDiv);

  var showOverviewButton = Functions.createElement({
    type: "button",
    className: "minerva-overview-button",
    name: "showOverviewButton",
    content: "<i class='fa fa-sitemap' style='font-size:18px; font-weight:400; padding-right:10px;'></i><span >SHOW OVERVIEW</span>",
    style: "display:none",
    xss: false
  });
  showOverviewDiv.appendChild(showOverviewButton);
  self.setControlElement(PanelControlElementType.MENU_SHOW_OVERVIEW_BUTTON, showOverviewButton);

  var rightHeaderMenuDiv = Functions.createElement({
    type: "div",
    className: "rightHeaderMenu"
  });
  self.getElement().appendChild(rightHeaderMenuDiv);

  var div4checkboxes = Functions.createElement({
    type: "div",
    className: "minerva-top-checkbox-div"
  });
  rightHeaderMenuDiv.appendChild(div4checkboxes);

  var legendCheckbox = Functions.createElement({
    type: "input",
    inputType: "checkbox",
    name: "legendCheckbox"
  });
  div4checkboxes.appendChild(legendCheckbox);
  self.setControlElement(PanelControlElementType.MENU_LEGEND_CHECKBOX, legendCheckbox);

  div4checkboxes.appendChild(Functions.createElement({
    type: "label",
    content: "LEGEND"
  }));

  var commentCheckbox = Functions.createElement({
    type: "input",
    inputType: "checkbox",
    name: "commentCheckbox"
  });
  div4checkboxes.appendChild(commentCheckbox);
  self.setControlElement(PanelControlElementType.MENU_COMMENTS_CHECKBOX, commentCheckbox);

  div4checkboxes.appendChild(Functions.createElement({
    type: "label",
    content: "COMMENTS"
  }));

  var refreshCommentButton = Functions.createElement({
    type: "button",
    className: "minerva-overview-button",
    name: "refreshCommentButton",
    content: "<i class='fa fa-refresh' style='font-size:21px; font-weight:400;'></i>",
    style: "display:none",
    xss: false
  });
  div4checkboxes.appendChild(refreshCommentButton);
  self.setControlElement(PanelControlElementType.MENU_REFRESH_COMMENTS_BUTTON, refreshCommentButton);

  var clearButton = Functions.createElement({
    type: "button",
    className: "minerva-overview-button",
    name: "clearButton",
    content: "<i class='fa fa-times' style='font-size:18px; font-weight:300; padding-right:10px;'></i>CLEAR",
    xss: false
  });
  rightHeaderMenuDiv.appendChild(clearButton);
  self.setControlElement(PanelControlElementType.MENU_CLEAR_BUTTON, clearButton);

};

TopMenu.prototype.init = function () {
  var self = this;
  self.getControlElement(PanelControlElementType.MENU_LEGEND_CHECKBOX).onclick = function () {
    return self.toggleLegend();
  };
  var hideButton = self.getControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON);
  var icon = self.getControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON_ICON);
  hideButton.onclick = function () {
    if (icon.className.indexOf("fa-chevron-left") >= 0) {
      icon.className = "fa fa-chevron-right";
      self.getLeftPanel().hide();
    } else {
      icon.className = "fa fa-chevron-left";
      self.getLeftPanel().show();
    }
    self.getMap().getMapCanvas().triggerListeners('resize');
  };

  var project = self.getMap().getProject();
  self.getControlElement(PanelControlElementType.MENU_VERSION_DIV).innerHTML = project.getVersion();

  var commentCheckbox = self.getControlElement(PanelControlElementType.MENU_COMMENTS_CHECKBOX);
  var refreshCommentButton = self.getControlElement(PanelControlElementType.MENU_REFRESH_COMMENTS_BUTTON);
  commentCheckbox.onclick = function () {
    ServerConnector.getSessionData(project).setShowComments(commentCheckbox.checked);
    if (commentCheckbox.checked) {
      $(refreshCommentButton).css("display","inline");
    } else {
      $(refreshCommentButton).css("display","none");
    }
    return self.getMap().refreshComments().then(null, GuiConnector.alert);
  };
  refreshCommentButton.onclick = (function () {
    return function () {
      self.getMap().refreshComments();
      return false;
    };
  })();

  var clearButton = self.getControlElement(PanelControlElementType.MENU_CLEAR_BUTTON);
  clearButton.onclick = (function () {
    return function () {
      return self.getMap().clearDbOverlays();
    };
  })();

  if (project.getTopOverviewImage() !== undefined && project.getTopOverviewImage() !== null) {
    self._overviewDialog = new OverviewDialog({
      customMap: self.getMap(),
      element: self.getControlElement(PanelControlElementType.OVERVIEW_DIALOG_DIV)
    });
    var showOverviewButton = self.getControlElement(PanelControlElementType.MENU_SHOW_OVERVIEW_BUTTON);
    showOverviewButton.onclick = function () {
      return self._overviewDialog.showOverview();
    };
    $(showOverviewButton).css("display","inline-block");
  }

  if (ServerConnector.getSessionData().getShowComments()) {
    self.getControlElement(PanelControlElementType.MENU_COMMENTS_CHECKBOX).checked = true;
    return self.getMap().refreshComments();
  } else {
    return Promise.resolve();
  }
};

TopMenu.prototype.setLegend = function (legend) {
  this._legend = legend;
};

TopMenu.prototype.getLegend = function () {
  return this._legend;
};

TopMenu.prototype.setLeftPanel = function (leftPanel) {
  this._leftPanel = leftPanel;
};

TopMenu.prototype.getLeftPanel = function () {
  return this._leftPanel;
};

TopMenu.prototype.destroy = function () {
  var self = this;
  if (self._overviewDialog !== undefined) {
    self._overviewDialog.destroy();
  }
  return Promise.resolve();
};

TopMenu.prototype.toggleLegend = function () {
  var self = this;
  var legend = self.getLegend();
  if (self.getControlElement(PanelControlElementType.MENU_LEGEND_CHECKBOX).checked) {
    legend.show();
  } else {
    legend.hide();
  }
};

module.exports = TopMenu;