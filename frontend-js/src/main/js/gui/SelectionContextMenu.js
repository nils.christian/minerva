"use strict";

/* exported logger */

var ContextMenu = require('./ContextMenu');

var logger = require('../logger');

function SelectionContextMenu(params) {
  ContextMenu.call(this, params);
  var self = this;

  self._createSelectionContextMenuGui();
}

SelectionContextMenu.prototype = Object.create(ContextMenu.prototype);
SelectionContextMenu.prototype.constructor = SelectionContextMenu;

SelectionContextMenu.prototype.init = function () {
  var self = this;
  return self.createExportAsImageSubmenu().then(function (submenu) {
    self.addOption(submenu);
    return self.createExportAsModelSubmenu();
  }).then(function (submenu) {
    self.addOption(submenu);
  });
};

SelectionContextMenu.prototype._createSelectionContextMenuGui = function () {
  var self = this;
  self.addOption("Remove Selection", function () {
    self.getMap().removeSelection();
    if (self.getMap().isDrawingOn()) {
      self.getMap().toggleDrawing();
    }
  });
};

module.exports = SelectionContextMenu;
