"use strict";

/* exported logger */

var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var SubMenu = require('./SubMenu');

var Functions = require('../Functions');

var logger = require('../logger');

var MARGIN = 5;

function ContextMenu(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  var element = self.getElement();
  element.className = "dropdown-menu";
  element.setAttribute("role", "menu");
  $(element).hide();

  self._handledTimeStamp = undefined;

  self.MIN_SHOW_TIME = 250;

  self._documentClickListener = function (e) {
    var className = e.target.className;
    if (typeof className === 'string' || className instanceof String) {
      if (className.indexOf("dropdown-link") === -1) {
        self.hide(new Date().getTime() - self.MIN_SHOW_TIME);
      }
    } else {
      self.hide(new Date().getTime() - self.MIN_SHOW_TIME);
    }
  };
  $(document).on('click', self._documentClickListener);
  $(document).on('touchstart', self._documentClickListener);
}

ContextMenu.prototype = Object.create(AbstractGuiElement.prototype);
ContextMenu.prototype.constructor = ContextMenu;

ContextMenu.prototype.destroy = function () {
  var self = this;
  $(document).off('click', self._documentClickListener);
  $(document).off('touchstart', self._documentClickListener);
};

ContextMenu.prototype.addOption = function (name, handler, disabled) {
  if (!disabled) disabled = false;
  var self = this;
  if (name instanceof SubMenu) {
    self.getElement().appendChild(name.getElement());
  } else {
    var option = Functions.createElement({
      type: "li"
    });
    var link = Functions.createElement({
      type: "a",
      className: "dropdown-link",
      content: name
    });
    if (!disabled) {
      link.href = "#";
    } else {
      link.className = 'disabled-link';
    }
    $(link).data("handler", handler);
    option.appendChild(link);
    self.getElement().appendChild(option);
  }
};

/**
 *
 * @param {number} x
 * @param {number} y
 * @param {number} timestamp
 */
ContextMenu.prototype.open = function (x, y, timestamp) {
  var self = this;
  self._handledTimeStamp = timestamp;

  var left = x;
  var right = "";

  if ($(self.getElement()).width() + x + MARGIN > $(document).width()) {
    left = Math.max(0, $(document).width() - $(self.getElement()).width() - MARGIN);
    right = 0;
  }

  $(self.getElement()).show().css({
    position: "absolute",
    left: left,
    right: right,
    top: y
  }).off('click').on('click', 'a', function (e) {
    //close if this is not a submenu
    if (this.parentNode === undefined || this.parentNode.className.indexOf("dropdown-submenu") === -1) {
      self.hide(new Date().getTime());
      if ($(this).data("handler") !== undefined) {
        return $(this).data("handler")();
      } else {
        logger.debug("Nothing to do");
      }
    }
  });
};

ContextMenu.prototype.hide = function (timestamp) {
  var self = this;
  if (self._handledTimeStamp < timestamp) {
    self._handledTimeStamp = timestamp;
    $(this.getElement()).hide();
  }
};

function extractDataOverlayIds(dataOverlays) {
  var ids = [];
  for (var i = 0; i < dataOverlays.length; i++) {
    ids.push(dataOverlays[i].getId());
  }
  return ids;
}

ContextMenu.prototype.createExportAsImageSubmenu = function () {
  var self = this;
  return self.getMap().getServerConnector().getImageConverters().then(function (converters) {
    var li = Functions.createElement({
      type: "li"
    });
    var submenu = new SubMenu({
      element: li,
      name: "Export as image",
      customMap: self.getMap()
    });

    var map = self.getMap();
    converters.forEach(function (converter) {
      submenu.addOption(converter.name, function () {
        return map.getVisibleDataOverlays().then(function (visibleDataOverlays) {
          var submapId = map.getActiveSubmapId();

          return map.getServerConnector().getImageDownloadUrl({
            polygonString: map.getSelectedPolygon(),
            modelId: submapId,
            handlerClass: converter.handler,
            backgroundOverlayId: map.getBackgroundDataOverlay().getId(),
            zoomLevel: map.getSubmapById(submapId).getZoom(),
            overlayIds: extractDataOverlayIds(visibleDataOverlays)
          });
        }).then(function (url) {
          return self.downloadFile(url);
        }).catch(GuiConnector.alert);
      });
    });
    return submenu;
  });
};

ContextMenu.prototype.createExportAsModelSubmenu = function () {
  var self = this;
  return self.getMap().getServerConnector().getModelConverters().then(function (converters) {
    var li = Functions.createElement({
      type: "li"
    });
    var submenu = new SubMenu({
      element: li,
      name: "Export as map",
      customMap: self.getMap()
    });

    var map = self.getMap();
    converters.forEach(function (converter) {
      submenu.addOption(converter.name, function () {
        return map.getVisibleDataOverlays().then(function (visibleDataOverlays) {
          return map.getServerConnector().getModelDownloadUrl({
            polygonString: map.getSelectedPolygon(),
            modelId: map.getActiveSubmapId(),
            handlerClass: converter.handler,
            backgroundOverlayId: map.getBackgroundDataOverlay().getId(),
            zoomLevel: map.getZoom(),
            overlayIds: extractDataOverlayIds(visibleDataOverlays)
          });
        }).then(function (url) {
          return self.downloadFile(url);
        }).catch(GuiConnector.alert);
      });
    });
    return submenu;
  });
};

module.exports = ContextMenu;
