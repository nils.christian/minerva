"use strict";

/* exported logger */

var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var PanelControlElementType = require('./PanelControlElementType');
var Functions = require('../Functions');
var OptionsMenu = require('./OptionsMenu');

var Promise = require("bluebird");

var logger = require('../logger');
var xss = require('xss');

function Header(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  var guiParams = {
    adminLink: true,
    optionsMenu: params.optionsMenu
  };
  if (params.adminLink !== undefined) {
    guiParams.adminLink = params.adminLink;
  }

  self._createHeaderGui(guiParams);
}

Header.prototype = Object.create(AbstractGuiElement.prototype);
Header.prototype.constructor = Header;

Header.prototype._createHeaderGui = function (guiParams) {
  var self = this;
  self.getElement().className = "minerva-header";

  var projectId = self.getProject().getProjectId();
  var projectName = xss(self.getProject().getName());

  var loadingDiv = Functions.createElement({
    type: "div",
    style: "display:none; "
  });
  loadingDiv.style.float = "right";

  var loadingImg = Functions.createElement({
    type: "img",
    src: 'resources/images/icons/ajax-loader.gif'
  });
  loadingImg.style.height = "35px";
  loadingDiv.appendChild(loadingImg);
  this.setControlElement(PanelControlElementType.FOOTER_LOADING_DIV, loadingDiv);

  if (guiParams.adminLink) {
    var link = Functions.createElement({
      type: "a",
      style: "padding-right:15px; float:right",
      content: '<i class="fa fa-lock" style="font-size:17px"></i>&nbsp;',
      xss: false
    });
    link.href = ServerConnector.getServerBaseUrl() + "admin.xhtml?id=" + projectId;
    self.getElement().appendChild(link);
  }

  if (guiParams.optionsMenu) {
    var optionsElement = Functions.createElement({type: "ul"});
    document.body.appendChild(optionsElement);
    self._optionsMenu = new OptionsMenu({
      customMap: self.getMap(),
      element: optionsElement
    });

    var menuLink = Functions.createElement({
      type: "a",
      style: "padding-right:5px; float:right",
      content: '<i class="fa fa-bars" style="font-size:17px"></i>&nbsp;',
      href: "#",
      onclick: function (e) {
        var link = $(menuLink);
        var offset = link.offset();

        var top = offset.top;
        var left = offset.left;

        var bottom = top + link.outerHeight();

        return self._optionsMenu.open(left, bottom, new Date().getTime());
      },
      xss: false
    });
    self.getElement().appendChild(menuLink);
  }

  self.getElement().appendChild(loadingDiv);

  var homeLink = Functions.createElement({
    type: "a",
    content: '<i class="fa fa-home" style="font-size:17px"></i> ' + projectName,
    xss: false
  });
  homeLink.href = ServerConnector.getServerBaseUrl() + "?id=" + projectId;
  self.getElement().appendChild(homeLink);
};

Header.prototype.addLoadMessage = function (message) {
  var self = this;
  self._loadMessages.push(message);
};
Header.prototype.removeLoadMessage = function (message) {
  var self = this;
  var index = self._loadMessages.indexOf(message);
  if (index > -1) {
    self._loadMessages.splice(index, 1);
  } else {
    logger.debug("Removing message that is not there: " + message);
  }
};

Header.prototype.init = function () {
  var self = this;
  return new Promise(function (resolve) {
    var div = self.getControlElement(PanelControlElementType.FOOTER_LOADING_DIV);

    self._loadMessages = [];
    self._onDataLoadStart = function (e) {
      self.addLoadMessage(e.arg);
      div.style.display = "block";
      div.title = e.arg;
    };

    self._onDataLoadStop = function (e) {
      self.removeLoadMessage(e.arg);
      if (self._loadMessages.length > 0) {
        div.title = self._loadMessages[0];
      } else {
        div.style.display = "none";
      }
    };
    ServerConnector.addListener("onDataLoadStart", self._onDataLoadStart);
    ServerConnector.addListener("onDataLoadStop", self._onDataLoadStop);
    resolve();
  });
};

Header.prototype.destroy = function () {
  var self = this;
  if (self._onDataLoadStart) {
    ServerConnector.removeListener("onDataLoadStart", self._onDataLoadStart);
    ServerConnector.removeListener("onDataLoadStop", self._onDataLoadStop);
  }
  if (self._optionsMenu !== undefined) {
    document.body.removeChild(self._optionsMenu.getElement());
  }
};

Header.prototype.setPluginManager = function (pluginManager) {
  var self = this;
  self._pluginManager = pluginManager;
  if (self._optionsMenu !== undefined) {
    self._optionsMenu.setPluginManager(pluginManager);
  }
};

Header.prototype.getPluginManager = function () {
  return this._pluginManager;
};

module.exports = Header;
