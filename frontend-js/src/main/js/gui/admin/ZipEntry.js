"use strict";

var types = ["IMAGE", "OVERLAY", "MAP"];

function ZipEntry(params) {
  var self = this;
  self.setType(params.type);
  self.setFilename(params.filename);
  self.setData(params.data);
}

ZipEntry.prototype.setType = function (type) {
  if (types.indexOf(type) === -1) {
    throw new Error("Unknown ZipEntryType: " + type + ".")
  }
  this._type = type;
};
ZipEntry.prototype.getType = function () {
  return this._type;
};

ZipEntry.prototype.setFilename = function (filename) {
  this._filename = filename;
};

ZipEntry.prototype.getFilename = function () {
  return this._filename;
};

ZipEntry.prototype.setData = function (data) {
  this._data = data;
};
ZipEntry.prototype.getData = function () {
  return this._data;
};

module.exports = ZipEntry;
