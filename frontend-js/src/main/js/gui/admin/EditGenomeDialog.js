"use strict";

var Promise = require("bluebird");

var AbstractGuiElement = require('../AbstractGuiElement');
var Annotation = require('../../map/data/Annotation');
var GuiConnector = require('../../GuiConnector');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {ReferenceGenome} params.referenceGenome
 * @param {ServerConnector} [params.serverConnector]

 * @constructor
 *
 * @extends AbstractGuiElement
 */
function EditGenomeDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.setReferenceGenome(params.referenceGenome);

  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
  self.registerListenerType("onSave");
}

EditGenomeDialog.prototype = Object.create(AbstractGuiElement.prototype);
EditGenomeDialog.prototype.constructor = EditGenomeDialog;

/**
 *
 * @param {ReferenceGenome} referenceGenome
 */
EditGenomeDialog.prototype.setReferenceGenome = function (referenceGenome) {
  this._referenceGenome = referenceGenome;
};

/**
 *
 * @returns {ReferenceGenome}
 */
EditGenomeDialog.prototype.getReferenceGenome = function () {
  return this._referenceGenome;
};

/**
 *
 * @returns {boolean}
 */
EditGenomeDialog.prototype.isNew = function () {
  return this.getReferenceGenome().getId() === undefined;
};

/**
 *
 */
EditGenomeDialog.prototype.createGui = function () {
  var self = this;
  var element = self.getElement();

  var tabDiv = Functions.createElement({
    type: "div",
    name: "tabView",
    className: "tabbable boxed parentTabs",
    style: "position:absolute;top:10px;bottom:40px;left:10px;right:10px"
  });
  element.appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type: "div",
    className: "tab-content",
    style: "height:100%"
  });
  tabDiv.appendChild(tabContentDiv);

  self.createGeneralTab(tabMenuDiv, tabContentDiv);
  self.createGeneMappingTab(tabMenuDiv, tabContentDiv);
  $("a", tabMenuDiv).bind("click", function () {
    //workaround for some css issues...
    tabDiv.style.top = "40px";
    tabDiv.style.bottom = "10px";
  });

};

/**
 *
 * @param {HTMLElement} tabMenuDiv
 * @param {HTMLElement} tabContentDiv
 */
EditGenomeDialog.prototype.createGeneralTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "DETAILS",
    content: self.createGeneralTabContent()
  });

};

/**
 *
 * @param {HTMLElement} tabMenuDiv
 * @param {HTMLElement} tabContentDiv
 */
EditGenomeDialog.prototype.createGeneMappingTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "GENE MAPPING",
    content: self.createGeneMappingTabContent()
  });

};

var id_counter = 0;

/**
 *
 * @param {string} tab_name
 * @returns {string}
 */
EditGenomeDialog.prototype.generateTabId = function (tab_name) {
  var self = this;
  var id = self.getReferenceGenome().getId();
  if (id === undefined) {
    id = "new_genome_" + (id_counter++);
  }
  return id + tab_name.replace(" ", "_") + "_TAB";
};

EditGenomeDialog.prototype.addTab = function (params) {
  var id = this.generateTabId(params.name);

  var navLi = guiUtils.createTabMenuObject({
    id: id,
    name: params.name,
    navigationBar: params.tabMenuDiv
  });
  params.tabMenuDiv.appendChild(navLi);

  var contentDiv = guiUtils.createTabContentObject({
    id: id,
    navigationObject: navLi,
    navigationBar: params.tabMenuDiv
  });

  $(contentDiv).css("overflow", "auto");
  if (params.content !== undefined) {
    contentDiv.appendChild(params.content);
  }

  params.tabContentDiv.appendChild(contentDiv);
};

/**
 *
 * @returns {HTMLElement}
 */
EditGenomeDialog.prototype.createGeneralTabContent = function () {

  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var table = Functions.createElement({
    type: "table",
    name: "detailsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(table);

  $(table).on("change", "[name='genomeOrganismSelect']", function () {
    return self._fillTypeSelect(self.getReferenceGenome(), self.getSelectedOrganism());
  });

  $(table).on("change", "[name='genomeTypeSelect']", function () {
    return self._fillVersionSelect(self.getReferenceGenome(), self.getSelectedOrganism(), self.getSelectedType());
  });

  $(table).on("change", "[name='genomeVersionSelect']", function () {
    return self._fillUrl(self.getReferenceGenome(), self.getSelectedOrganism(), self.getSelectedType(), self.getSelectedVersion());
  });


  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var saveUserButton = Functions.createElement({
    type: "button",
    name: "saveGenome",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelGenome",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveUserButton);
  menuRow.appendChild(cancelButton);

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditGenomeDialog.prototype.createGeneMappingTabContent = function () {

  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var geneMappingTable = Functions.createElement({
    type: "table",
    name: "geneMappingTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(geneMappingTable);

  // noinspection JSUnusedGlobalSymbols
  $(geneMappingTable).DataTable({
    columns: [{
      title: 'Name'
    }, {
      title: 'Source'
    }, {
      title: 'Progress'
    }, {
      title: 'Remove',
      orderable: false
    }],
    order: [[1, "asc"]]
  });

  $(geneMappingTable).on("click", "[name='removeMapping']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove this gene mapping?",
      input: false
    }).then(function (param) {
      if (param.status) {
        return self.getServerConnector().removeReferenceGenomeGeneMapping({
          mappingId: $(button).attr("data").toString(),
          genomeId: self.getReferenceGenome().getId().toString()
        }).then(function () {
          return self.getServerConnector().getReferenceGenome({genomeId: self.getReferenceGenome().getId()});
        }).then(function (referenceGenome) {
          self.setReferenceGenome(referenceGenome);
          return self.refresh();
        });
      }
    }).catch(GuiConnector.alert);
  });

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var addGeneMappingButton = Functions.createElement({
    type: "button",
    name: "saveGenome",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;ADD GENE MAPPING',
    onclick: function () {
      return self.openAddGeneMapping();
    },
    xss: false
  });

  menuRow.appendChild(addGeneMappingButton);

  return result;
};

/**
 *
 * @returns {Promise|PromiseLike}
 */
EditGenomeDialog.prototype.onSaveClicked = function () {
  var self = this;
  if (!self.isNew()) {
    return self.callListeners("onSave");
  } else {
    var genome = self.getReferenceGenome();
    genome.setSourceUrl(self.getSourceUrl());
    genome.setOrganism(self.getSelectedOrganism());
    genome.setType(self.getSelectedType());
    genome.setVersion(self.getSelectedVersion());
    return self.getServerConnector().addReferenceGenome(genome).then(function () {
      return self.callListeners("onSave");
    });
  }
};
/**
 *
 * @param {ReferenceGenome} genome
 * @returns {Promise}
 * @private
 */
EditGenomeDialog.prototype._fillOrganismSelect = function (genome) {
  var self = this;
  return self.getServerConnector().getReferenceGenomeOrganisms().then(function (organisms) {
    var genomeOrganismSelect = $("[name=genomeOrganismSelect]", self.getElement());
    genomeOrganismSelect.empty();
    var selectedOrganism;
    if (self.isNew()) {
      selectedOrganism = organisms[0];
      for (var i = 0; i < organisms.length; i++) {
        if (organisms[i].getResource() === "9606") {
          selectedOrganism = organisms[i];
        }
      }
    } else {
      selectedOrganism = genome.getOrganism();
    }
    $.each(organisms, function (i, organism) {
      var disable = false;
      if (organism.getResource() !== selectedOrganism.getResource() && !self.isNew()) {
        disable = true;
      }
      genomeOrganismSelect.append($('<option>', {
        value: organism.getResource(),
        text: organism.getResource(),
        disabled: disable
      }));
    });
    genomeOrganismSelect.val(selectedOrganism.getResource());
    return self._fillTypeSelect(genome, selectedOrganism);
  });
};

/**
 *
 * @param {ReferenceGenome} genome
 * @param {Annotation} selectedOrganism
 * @returns {Promise}
 * @private
 */
EditGenomeDialog.prototype._fillTypeSelect = function (genome, selectedOrganism) {
  var self = this;
  return self.getServerConnector().getReferenceGenomeTypes({organism: selectedOrganism}).then(function (types) {

    var genomeTypeSelect = $("[name=genomeTypeSelect]", self.getElement());
    genomeTypeSelect.empty();

    var selectedType;
    if (self.isNew()) {
      selectedType = types[0].type;
    } else {
      selectedType = genome.getType();
    }
    $.each(types, function (i, type) {
      var disable = false;
      if (type.type !== selectedType && !self.isNew()) {
        disable = true;
      }
      genomeTypeSelect.append($('<option>', {
        value: type.type,
        text: type.type,
        disabled: disable
      }));
    });
    genomeTypeSelect.val(selectedType);
    return self._fillVersionSelect(genome, selectedOrganism, selectedType);
  })
};

/**
 *
 * @param {ReferenceGenome} genome
 * @param {Annotation} selectedOrganism
 * @param {string} selectedType
 * @returns {Promise}
 * @private
 */
EditGenomeDialog.prototype._fillVersionSelect = function (genome, selectedOrganism, selectedType) {
  var self = this;
  return self.getServerConnector().getReferenceGenomeVersions({
    organism: selectedOrganism,
    type: selectedType
  }).then(function (versions) {
    var genomeVersionSelect = $("[name=genomeVersionSelect]", self.getElement());
    genomeVersionSelect.empty();

    var selectedVersion;
    if (self.isNew()) {
      selectedVersion = versions[0].version;
    } else {
      selectedVersion = genome.getVersion();
    }
    $.each(versions, function (i, version) {
      var disable = false;
      if (version.version !== selectedVersion && !self.isNew()) {
        disable = true;
      }
      genomeVersionSelect.append($('<option>', {
        value: version.version,
        text: version.version,
        disabled: disable
      }));
    });
    genomeVersionSelect.val(selectedVersion);
    return self._fillUrl(genome, selectedOrganism, selectedType, selectedVersion);
  })
};

/**
 *
 * @param {ReferenceGenome} genome
 * @param {Annotation} organism
 * @param {string} type
 * @param {string} version
 * @returns {Promise}
 * @private
 */
EditGenomeDialog.prototype._fillUrl = function (genome, organism, type, version) {
  var self = this;
  var genomeSourceUrlInput = $("[name=genomeSourceUrl]", self.getElement());
  var genomeLocalUrlInput = $("[name=genomeLocalUrl]", self.getElement());
  var genomeProgressInput = $("[name=genomeProgress]", self.getElement());
  genomeLocalUrlInput.prop("disabled", true);
  genomeProgressInput.val(genome.getDownloadProgressStatus());
  genomeProgressInput.prop("disabled", true);
  if (!self.isNew()) {
    genomeSourceUrlInput.val(genome.getSourceUrl());
    genomeSourceUrlInput.prop("disabled", true);

    genomeLocalUrlInput.val(genome.getLocalUrl());
  } else {
    genomeSourceUrlInput.prop("disabled", false);

    genomeLocalUrlInput.val("");
    return self.getServerConnector().getAvailableGenomeUrls({
      organism: organism,
      type: type,
      version: version
    }).then(function (urls) {
      if (urls.length > 0) {
        genomeSourceUrlInput.val(urls[0]);
      } else {
        genomeSourceUrlInput.val("");
      }
    })
  }
  return Promise.resolve();
}
;


/**
 *
 * @param {ReferenceGenomeGeneMapping} geneMapping
 * @returns {Array}
 */
EditGenomeDialog.prototype.geneMappingToTableRow = function (geneMapping) {
  var row = [];
  row[0] = geneMapping.getName();
  row[1] = geneMapping.getSourceUrl();
  row[2] = geneMapping.getProgress();
  row[3] = "<button name='removeMapping' data='" + geneMapping.getId() + "'><i class='fa fa-trash-o' style='font-size:17px'></button>";
  return row;
};
/**
 *
 * @returns {Promise}
 */
EditGenomeDialog.prototype.init = function () {
  var self = this;

  var detailsTable = $("[name=detailsTable]", self.getElement())[0];

  // noinspection JSCheckFunctionSignatures
  $(detailsTable).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });

  return self.refresh();
};

/**
 *
 * @returns {Promise}
 */
EditGenomeDialog.prototype.refresh = function () {
  var self = this;

  var genome = self.getReferenceGenome();

  var dataTable = $("[name=detailsTable]", self.getElement()).DataTable();
  var data = [];

  data.push(['Organism', Functions.createElement({type: "select", name: "genomeOrganismSelect"}).outerHTML]);
  data.push(['Type', Functions.createElement({type: "select", name: "genomeTypeSelect"}).outerHTML]);
  data.push(['Version', Functions.createElement({type: "select", name: "genomeVersionSelect"}).outerHTML]);
  data.push(['Source url', Functions.createElement({type: "input", name: "genomeSourceUrl"}).outerHTML]);
  data.push(['Local url', Functions.createElement({type: "input", name: "genomeLocalUrl"}).outerHTML]);
  data.push(['Progress', Functions.createElement({type: "input", name: "genomeProgress"}).outerHTML]);

  dataTable.clear().rows.add(data).draw();

  dataTable = $("[name=geneMappingTable]", self.getElement()).DataTable();
  data = [];
  var page = dataTable.page();

  for (var i = 0; i < genome.getGeneMappings().length; i++) {
    var geneMapping = genome.getGeneMappings()[i];
    var rowData = self.geneMappingToTableRow(geneMapping);
    data.push(rowData);
  }
  //it should be simplified, but I couldn't make it work
  dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);

  return self._fillOrganismSelect(genome);
};

/**
 *
 */
EditGenomeDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();

  var detailsTable = $("[name=detailsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(detailsTable)) {
    $(detailsTable).DataTable().destroy();
  }

  var geneMappingTable = $("[name=geneMappingTable]", div)[0];
  if ($.fn.DataTable.isDataTable(geneMappingTable)) {
    $(geneMappingTable).DataTable().destroy();
  }

  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
};

/**
 *
 */
EditGenomeDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  var title;
  if (self.isNew()) {
    title = "Download genome";
  } else {
    var genome = self.getReferenceGenome();
    title = genome.getType() + " " + genome.getOrganism().getResource() + " " + genome.getVersion();
  }
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      title: title,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2
    });
  }
  $(div).dialog("open");
};

/**
 *
 */
EditGenomeDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

/**
 *
 * @returns {Annotation}
 */
EditGenomeDialog.prototype.getSelectedOrganism = function () {
  var self = this;
  return new Annotation({
    resource: ($("[name='genomeOrganismSelect']", self.getElement()).val()).toString(),
    link: '',
    id: 0,
    type: ''
  })
};

/**
 *
 * @returns {string}
 */
EditGenomeDialog.prototype.getSelectedType = function () {
  var self = this;
  return ($("[name='genomeTypeSelect']", self.getElement()).val()).toString();
};

/**
 *
 * @returns {string}
 */
EditGenomeDialog.prototype.getSelectedVersion = function () {
  var self = this;
  return ($("[name='genomeVersionSelect']", self.getElement()).val()).toString();
};

/**
 *
 * @returns {string}
 */
EditGenomeDialog.prototype.getSourceUrl = function () {
  var self = this;
  return ($("[name='genomeSourceUrl']", self.getElement()).val()).toString();
};

/**
 *
 */
EditGenomeDialog.prototype.openAddGeneMapping = function () {
  var self = this;
  var html = '<form style="z-index:10000"><span>Name: </span><input type="text" name="gene-mapping-name"><br>' +
    '<span>Url: </span><input type="text" name="gene-mapping-url"><br></form>';
  $(html).dialog({
    modal: true,
    title: "Add gene mapping",
    close: function () {
      $(this).dialog('destroy').remove();
    },
    buttons: {
      'ADD': function () {
        var dialog = this;
        var name = $('input[name="gene-mapping-name"]').val().toString();
        var url = $('input[name="gene-mapping-url"]').val().toString();
        return self.getServerConnector().addGeneMapping({
          genomeId: self.getReferenceGenome().getId(),
          mappingName: name,
          mappingUrl: url
        }).then(function () {
          return self.getServerConnector().getReferenceGenome({genomeId: self.getReferenceGenome().getId()});
        }).then(function (referenceGenome) {
          self.setReferenceGenome(referenceGenome);
          return self.refresh();
        }).then(function () {
          $(dialog).dialog('destroy').remove();
        });
      },
      'Cancel': function () {
        $(this).dialog('destroy').remove();
      }
    }
  });
};

module.exports = EditGenomeDialog;
