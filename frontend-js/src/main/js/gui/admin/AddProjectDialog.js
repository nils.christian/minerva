"use strict";

var Promise = require("bluebird");
var JSZip = require("jszip");
var xss = require('xss');

var AbstractGuiElement = require('../AbstractGuiElement');
var ChooseAnnotatorsDialog = require('./ChooseAnnotatorsDialog');
var ChooseValidatorsDialog = require('./ChooseValidatorsDialog');
var GuiConnector = require('../../GuiConnector');
var OverlayParser = require('../../map/OverlayParser');
var ZipEntry = require('./ZipEntry');
var ConfigurationType = require('../../ConfigurationType');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();

var UserPreferences = require("../../map/data/UserPreferences");
var ValidationError = require("../../ValidationError");

function AddProjectDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.registerListenerType("onFileUpload");
  self.registerListenerType("onZipFileUpload");
  self.registerListenerType("onProjectAdd");
  self.setZipEntries([]);
  $(self.getElement()).addClass("minerva-edit-project-dialog");
  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
}

AddProjectDialog.prototype = Object.create(AbstractGuiElement.prototype);
AddProjectDialog.prototype.constructor = AddProjectDialog;

AddProjectDialog.prototype.createGui = function () {
  var self = this;
  var element = self.getElement();

  var tabDiv = Functions.createElement({
    type: "div",
    name: "tabView",
    className: "tabbable boxed parentTabs",
    style: "position:absolute;top:10px;bottom:40px;left:10px;right:10px"
  });
  element.appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type: "div",
    className: "tab-content",
    style: "height:100%"
  });
  tabDiv.appendChild(tabContentDiv);

  self.createGeneralTab(tabMenuDiv, tabContentDiv);
  self.createOverlaysTab(tabMenuDiv, tabContentDiv);
  self.createSubmapsTab(tabMenuDiv, tabContentDiv);
  self.createOverviewImagesTab(tabMenuDiv, tabContentDiv);


  $("a", tabMenuDiv).bind("click", function () {
    //workaround for some css issues...
    tabDiv.style.top = "40px";
    tabDiv.style.bottom = "10px";
  });
};

AddProjectDialog.prototype.createGeneralTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "GENERAL",
    id: "add_project_general_tab",
    content: self.createGeneralTabContent()
  });

};

AddProjectDialog.prototype.addTab = function (params) {
  var navLi = guiUtils.createTabMenuObject({
    id: params.id,
    name: params.name,
    navigationBar: params.tabMenuDiv
  });
  params.tabMenuDiv.appendChild(navLi);


  var contentDiv = guiUtils.createTabContentObject({
    id: params.id,
    navigationObject: navLi,
    navigationBar: params.tabMenuDiv
  });
  contentDiv.style.overflow = "auto";


  if (params.content !== undefined) {
    contentDiv.appendChild(params.content);
  }

  params.tabContentDiv.appendChild(contentDiv);

  if (params.disabled) {
    this.disableTab(params);
  }
};

AddProjectDialog.prototype.disableTab = function (params) {
  $("a[href='#" + params.id + "']", this.getElement()).hide();
};

AddProjectDialog.prototype.enableTab = function (params) {
  $("a[href='#" + params.id + "']", this.getElement()).show();
};


AddProjectDialog.prototype.showAnnotatorsDialog = function () {
  var self = this;
  var promise;
  if (self._annotatorsDialog === undefined) {
    self._annotatorsDialog = new ChooseAnnotatorsDialog({
      element: Functions.createElement({type: "div"}),
      customMap: null,
      configuration: self.getConfiguration()
    });
    promise = self._annotatorsDialog.init();
  } else {
    promise = Promise.resolve();
  }
  return promise.then(function () {
    return self._annotatorsDialog.open();
  });
};
AddProjectDialog.prototype.showValidatorsDialog = function () {
  var self = this;
  var promise;
  if (self._validatorsDialog === undefined) {
    self._validatorsDialog = new ChooseValidatorsDialog({
      element: Functions.createElement({type: "div"}),
      customMap: null
    });
    promise = self._validatorsDialog.init();
  } else {
    promise = Promise.resolve();
  }
  return promise.then(function () {
    return self._validatorsDialog.open();
  });
};
AddProjectDialog.prototype.createGeneralTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div"
  });

  var table = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  result.appendChild(table);

  var fileInput = Functions.createElement({
    type: "input",
    inputType: "file",
    name: "project-file"
  });
  fileInput.addEventListener("change", function () {
    return self.callListeners("onFileUpload", fileInput.files[0]).then(null, GuiConnector.alert);
  }, false);
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    return self.processFile(file);
  });
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    return self.setZipFileContent(file);
  });
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    if (file.name.lastIndexOf('.') > 0) {
      return self.setProjectId(file.name.substring(0, file.name.lastIndexOf('.')).replace(/\./g, '-'));
    } else {
      return file.name;
    }
  });
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    return self.setFileParserForFilename(file.name);
  });

  var manualUrl = self.getConfiguration().getOption(ConfigurationType.USER_MANUAL_FILE).getValue();
  var help = 'File with the map. For a quick start, CellDesigner files are accepted directly. Available options and configurations of the source file are discussed in <a href="' + manualUrl + '">manual</a>.';
  table.appendChild(self.createRow([guiUtils.createLabel("Upload file: "), fileInput, guiUtils.createHelpButton(help)]));

  var fileFormatSelect = Functions.createElement({
    type: "select",
    name: "project-format"
  });

  table.appendChild(self.createRow([guiUtils.createLabel("File format: "), fileFormatSelect]));

  var mapCanvasTypeSelect = Functions.createElement({
    type: "select",
    name: "project-map-canvas-type"
  });

  table.appendChild(self.createRow([guiUtils.createLabel("Map canvas type: "), mapCanvasTypeSelect]));

  table.appendChild(self.createCheckboxRow({
    labelName: "I agree to <a target='_license' href='https://developers.google.com/maps/terms'>Google Maps APIs Terms of Service</a>:",
    defaultValue: false,
    inputName: "project-google-maps-license"
  }));

  table.appendChild(self.createInputRow({
    labelName: "ProjectId:",
    defaultValue: "id",
    inputName: "project-id",
    help: 'A working name of the uploaded project on the MINERVA platform. Unique in the platform.'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Project name:",
    defaultValue: "NEW DISEASE MAP",
    inputName: "project-name",
    help: 'The name of the uploaded project displayed in the top left corner of the Admin and User panels; your official name of the project.'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Project Disease:",
    inputName: "project-disease",
    help: 'Mesh ID identifying disease connected to this map (ie. for Parkinson\'s Disease it would be D010300).'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Organism:",
    inputName: "project-organism",
    help: 'Taxonomy ID identifying organism for which project is dedicated (ie. for Human map it would be 9606).'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Version:",
    inputName: "project-version",
    help: 'A text field displayed next to the name of your project in the User panel.'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Notify email:",
    inputName: "project-notify-email",
    help: 'E-mail address that should be used for project change notifications.'
  }));

  var showAnnotatorsButton = Functions.createElement({
    type: "button",
    name: "project-show-annotators",
    content: "Advanced",
    onclick: function () {
      return self.showAnnotatorsDialog().then(null, GuiConnector.alert);
    }
  });
  var showValidatorsButton = Functions.createElement({
    type: "button",
    name: "project-show-validators",
    content: "Advanced",
    onclick: function () {
      return self.showValidatorsDialog().then(null, GuiConnector.alert);
    }
  });

  table.appendChild(self.createCheckboxRow({
    labelName: "Annotate model automatically:",
    defaultValue: false,
    inputName: "project-annotate-automatically",
    elements: [showAnnotatorsButton],
    help: 'If this checkbox is checked, elements of the uploaded map will be automatically annotated using built in ' +
    'annotators. Behavior of the annotators can be configured by clicking the Advanced button.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Verify manual annotations:",
    defaultValue: false,
    inputName: "project-verify-annotations",
    elements: [showValidatorsButton],
    help: 'If this checkbox is checked, elements and interactions of the uploaded map will be scanned for existing ' +
    'annotations; if present these existing annotations will be validated against a set of rules. Verification rules ' +
    'can be configured by clicking the Advanced button.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Cache data:",
    defaultValue: false,
    inputName: "project-cache-data",
    help: 'If this checkbox is checked, all hyperlinks in the project resolved by MIRIAM repository (e.g. cross-links ' +
    'to external bioinformatics databases) are resolved and cached.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Auto margin:",
    defaultValue: true,
    inputName: "project-auto-margin",
    help: 'If this checkbox is checked, upon generation of the graphics, empty spaces surrounding elements and ' +
    'interactions will be cropped.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Display as SBGN:",
    defaultValue: false,
    inputName: "project-sbgn-visualization",
    help: 'If this checkbox is checked, the uploaded model will be displayed in SBGN format, instead of default ' +
    'CellDesigner format.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Semantic zooming:",
    defaultValue: false,
    inputName: "project-semantic-zooming"
  }));

  var saveProjectButton = Functions.createElement({
    type: "button",
    name: "saveProject",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelProject",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);
  menuRow.appendChild(saveProjectButton);
  menuRow.appendChild(cancelButton);

  return result;
};
AddProjectDialog.prototype.createInputRow = function (params) {
  if (params.defaultValue === undefined) {
    params.defaultValue = "";
  }
  var label = Functions.createElement({
    type: "div",
    style: "display:table-cell; width:200px",
    content: xss(params.labelName)
  });
  var input = Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='" + xss(params.inputName) + "' value='" + xss(params.defaultValue) + "' style='width:100%'/>",
    xss: false
  });
  var elements = [label, input];
  if (params.help !== undefined) {
    elements.push(guiUtils.createHelpButton(params.help));
  }
  return this.createRow(elements);
};

AddProjectDialog.prototype.createCheckboxRow = function (params) {
  var labelName = params.labelName;
  var defaultValue = params.defaultValue;
  var inputName = params.inputName;
  var elements = params.elements;
  var tooltip = params.help;
  if (elements === undefined) {
    elements = [];
  }
  var label = Functions.createElement({
    type: "div",
    style: "display:table-cell;vertical-align: middle;",
    content: xss(labelName)
  });
  var checked = "";
  if (defaultValue) {
    checked = "checked";
  }
  var checkbox = Functions.createElement({
    type: "div",
    content: "<input type='checkbox' name='" + xss(inputName) + "' " + checked + "/>",
    style: "float:left;",
    xss: false
  });
  var checkBoxDiv = Functions.createElement({type: "div", style: "display:table-cell; vertical-align: middle"});
  checkBoxDiv.appendChild(checkbox);
  var rowElements = [label, checkBoxDiv];

  if (tooltip !== undefined) {
    rowElements.push(guiUtils.createHelpButton(tooltip));
  }
  for (var i = 0; i < elements.length; i++) {
    $(elements[i]).css("float", "left");
    checkBoxDiv.appendChild(elements[i]);
  }
  return this.createRow(rowElements);
};

AddProjectDialog.prototype.createRow = function (elements) {
  var result = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  for (var columnNumber = 0; columnNumber < elements.length; columnNumber++) {
    var element = elements[columnNumber];
    if (element.tagName.toLowerCase() !== 'div') {
      var style = "display:table-cell; vertical-align: middle; ";
      if (columnNumber === 0) {
        style += "width:200px;";
      }
      var labelContainer = Functions.createElement({
        type: "div",
        style: style
      });
      labelContainer.appendChild(element);
      element = labelContainer;
    }
    result.appendChild(element);
  }
  return result;
};

AddProjectDialog.prototype.createOverlaysTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "OVERLAYS",
    id: "project_overlays_tab",
    content: self.createOverlaysTabContent(),
    disabled: true
  });
};

AddProjectDialog.prototype.createOverlaysTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div"
  });
  result.appendChild(self._createOverlayTable());
  return result;
};

AddProjectDialog.prototype._createOverlayTable = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var overlaysTable = Functions.createElement({
    type: "table",
    name: "overlaysTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(overlaysTable);

  $(overlaysTable).DataTable({
    columns: [{
      title: 'File name'
    }, {
      title: 'Name'
    }, {
      title: 'Description'
    }]
  });

  $(overlaysTable).on("input", "[name='overlayName']", function () {
    var input = this;
    var filename = $(input).attr("data");
    self.getEntryByFilename(filename).getData().name = $(input).val();
  });

  $(overlaysTable).on("input", "[name='overlayDescription']", function () {
    var input = this;
    var filename = $(input).attr("data");
    self.getEntryByFilename(filename).getData().description = $(input).val();
  });

  self.addListener("onZipFileUpload", function () {
    var entries = self.getZipEntries();
    var dataTable = $($("[name='overlaysTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      if (entry.getType() === "OVERLAY") {
        var row = [];
        row[0] = entry.getFilename();
        row[1] = "<input data='" + entry.getFilename() + "' name='overlayName' value='" + entry.getData().name + "'/>";
        row[2] = "<input data='" + entry.getFilename() + "' name='overlayDescription' value='" + entry.getData().description + "'/>";
        data.push(row);
      }
    }
    dataTable.clear().rows.add(data).draw();
  });

  return result;
};

AddProjectDialog.prototype.createSubmapsTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "SUBMAPS",
    id: "project_submaps_tab",
    content: self.createSubmapsTabContent(),
    disabled: true
  });
};

AddProjectDialog.prototype.createSubmapsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });
  var submapsTable = Functions.createElement({
    type: "table",
    name: "submapsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(submapsTable);

  $(submapsTable).DataTable({
    columns: [{
      title: 'File name'
    }, {
      title: 'Name'
    }, {
      title: 'Root map'
    }, {
      title: 'Mapping file'
    }, {
      title: 'Map type'
    }]
  });

  $(submapsTable).on("input", "[name='submapName']", function () {
    var input = this;
    var filename = $(input).attr("data");
    self.getEntryByFilename(filename).getData().name = $(input).val();
  });

  $(submapsTable).on("change", "[name='submapRoot']", function () {
    var input = this;
    if (!$(input).is(":checked")) {
      this.checked = true;
      GuiConnector.info("One model must be marked as a root");
      return false;
    }

    var filename = $(input).attr("data");
    var checkboxes = $("[name='submapRoot']", submapsTable);
    for (var i = 0; i < checkboxes.length; i++) {
      var checkbox = checkboxes[i];
      if ($(checkbox).attr("data") !== filename) {
        $(checkbox).attr('checked', false);
        self.getEntryByFilename($(checkbox).attr("data")).getData().root = false;
      }
    }
    self.getEntryByFilename(filename).getData().root = $(input).is(":checked");
  });

  $(submapsTable).on("change", "[name='submapMapping']", function () {
    var input = this;
    var filename = $(input).attr("data");
    var checkboxes = $("[name='submapMapping']", submapsTable);
    for (var i = 0; i < checkboxes.length; i++) {
      var checkbox = checkboxes[i];
      if ($(checkbox).attr("data") !== filename) {
        $(checkbox).attr('checked', false);
        self.getEntryByFilename($(checkbox).attr("data")).getData().mapping = false;
      }
    }
    self.getEntryByFilename(filename).getData().mapping = $(input).is(":checked");
  });

  $(submapsTable).on("change", "[name='submapType']", function () {
    var input = this;
    var filename = $(input).attr("data");
    return ServerConnector.getConfiguration().then(function (configuration) {
      var mapTypes = configuration.getMapTypes();
      for (var j = 0; j < mapTypes.length; j++) {
        var mapType = mapTypes[j];
        if (mapType.id === $(input).val()) {
          self.getEntryByFilename(filename).getData().type = mapType;
        }
      }
    });
  });

  self.addListener("onZipFileUpload", function () {
    return ServerConnector.getConfiguration().then(function (configuration) {

      var entries = self.getZipEntries();
      var dataTable = $($("[name='submapsTable']", self.getElement())[0]).DataTable();
      var data = [];
      for (var i = 0; i < entries.length; i++) {
        var entry = entries[i];
        if (entry.getType() === "MAP") {
          var row = [];
          var rootCheckbox;
          if (entry.getData().root) {
            rootCheckbox = "<input name='submapRoot' type='checkbox' data='" + entry.getFilename() + "' checked='checked'/>";
          } else {
            rootCheckbox = "<input name='submapRoot' type='checkbox' data='" + entry.getFilename() + "'/>";
          }
          var mappingCheckbox;
          if (entry.getData().mapping) {
            mappingCheckbox = "<input name='submapMapping' type='checkbox' data='" + entry.getFilename() + "' checked='checked'/>";
          } else {
            mappingCheckbox = "<input name='submapMapping' type='checkbox' data='" + entry.getFilename() + "'/>";
          }

          var typeSelect = "<select data='" + entry.getFilename() + "' name='submapType'>";

          typeSelect += "<option value='" + entry.getData().type.id + "' selected>" + entry.getData().type.name + "</option>";
          var mapTypes = configuration.getMapTypes();
          for (var j = 0; j < mapTypes.length; j++) {
            var mapType = mapTypes[j];
            if (mapType !== entry.getData().type) {
              typeSelect += "<option value='" + mapType.id + "' >" + mapType.name + "</option>";
            }
          }
          typeSelect += "</select>";


          row[0] = entry.getFilename();
          row[1] = "<input data='" + entry.getFilename() + "' name='submapName' value='" + entry.getData().name + "'/>";
          row[2] = rootCheckbox;
          row[3] = mappingCheckbox;
          row[4] = typeSelect;
          data.push(row);
        }
      }
      dataTable.clear().rows.add(data).draw();
    });
  });

  return result;
};

AddProjectDialog.prototype.createOverviewImagesTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "IMAGES",
    id: "project_overview_images_tab",
    content: self.createOverviewImagesTabContent(),
    disabled: true
  });
};

AddProjectDialog.prototype.createOverviewImagesTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var imagesTable = Functions.createElement({
    type: "table",
    name: "imagesTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(imagesTable);

  $(imagesTable).DataTable({
    columns: [{
      title: 'File name'
    }]
  });

  self.addListener("onZipFileUpload", function () {
    var entries = self.getZipEntries();
    var dataTable = $($("[name='imagesTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      if (entry.getType() === "IMAGE") {
        var row = [];
        row[0] = entry.getFilename();
        data.push(row);
      }
    }
    dataTable.clear().rows.add(data).draw();
  });

  return result;
};

/**
 *
 * @returns {PromiseLike}
 */
AddProjectDialog.prototype.init = function () {
  var self = this;
  return ServerConnector.getConfiguration().then(function (configuration) {
    var select = $("[name='project-format']", self.getElement())[0];
    self._fillProjectFormatSelectOptions(select, configuration);

    select = $("[name='project-map-canvas-type']", self.getElement())[0];
    self._fillMapCanvasTypeSelectOptions(select, configuration);
    return ServerConnector.getLoggedUser();
  }).then(function (user) {
    self.setNotifyEmail(user.getEmail());
    self.bindProjectUploadPreferences(user, "annotateModel", "project-annotate-automatically");
    self.bindProjectUploadPreferences(user, "autoResize", "project-auto-margin");
    self.bindProjectUploadPreferences(user, "validateMiriam", "project-verify-annotations");
    self.bindProjectUploadPreferences(user, "cacheData", "project-cache-data");
    self.bindProjectUploadPreferences(user, "semanticZooming", "project-semantic-zooming");
    self.bindProjectUploadPreferences(user, "sbgn", "project-sbgn-visualization");
  });
};

/**
 *
 * @param {HTMLElement} select
 * @param {Configuration} configuration
 * @private
 */
AddProjectDialog.prototype._fillProjectFormatSelectOptions = function (select, configuration) {
  var option = Functions.createElement({
    type: "option",
    content: "---"
  });
  option.selected = true;
  select.appendChild(option);
  var converters = configuration.getModelConverters();
  for (var i = 0; i < converters.length; i++) {
    var converter = converters[i];
    option = Functions.createElement({
      type: "option",
      content: converter.name
    });
    option.data = converter.extension;
    option.value = converter.handler;
    select.appendChild(option);
  }
};

/**
 *
 * @param {HTMLElement} select
 * @param {Configuration} configuration
 * @private
 */
AddProjectDialog.prototype._fillMapCanvasTypeSelectOptions = function (select, configuration) {
  var self = this;
  var mapCanvasTypes = configuration.getMapCanvasTypes();

  var licenceCheckbox = $("[name='project-google-maps-license']", self.getElement())[0];
  var licenceDiv = licenceCheckbox.parentElement.parentElement.parentElement;

  for (var i = 0; i < mapCanvasTypes.length; i++) {
    var mapCanvasType = mapCanvasTypes[i];
    var option = Functions.createElement({
      type: "option",
      content: mapCanvasType.name,
      value: mapCanvasType.id
    });
    if (mapCanvasType.id === "OPEN_LAYERS") {
      option.selected = true;
      $(licenceDiv).css("display", "none");
    }
    select.appendChild(option);
  }


  $(select).change(function () {
    $("option:selected", select).each(function () {
      if ($(this).val() === "GOOGLE_MAPS_API") {
        $(licenceDiv).css("display", "table-row");
      } else {
        $(licenceDiv).css("display", "none");
      }
      $(licenceCheckbox).prop('checked', false);
    });
  });
};


AddProjectDialog.prototype.bindProjectUploadPreferences = function (user, type, elementName) {
  var element = $("[name='" + elementName + "']", this.getElement());

  var value = user.getPreferences().getProjectUpload()[type];
  element.prop('checked', value);

  element.change(function () {
    var data = new UserPreferences();
    data.getProjectUpload()[type] = element.is(":checked");
    return ServerConnector.updateUserPreferences({user: user, preferences: data}).then(null, GuiConnector.alert);
  });
};

AddProjectDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if (self._annotatorsDialog !== undefined) {
    self._annotatorsDialog.destroy();
  }
  if (self._validatorsDialog !== undefined) {
    self._validatorsDialog.destroy();
  }
  var overlaysTable = $("[name=overlaysTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(overlaysTable)) {
    $(overlaysTable).DataTable().destroy();
  }
  var submapsTable = $("[name=submapsTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(submapsTable)) {
    $(submapsTable).DataTable().destroy();
  }

  var imagesTable = $("[name=imagesTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(imagesTable)) {
    $(imagesTable).DataTable().destroy();
  }

};

AddProjectDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      title: "ADD PROJECT",
      width: window.innerWidth / 2,
      height: window.innerHeight / 2
    });
  }
  $(div).dialog("open");
};

AddProjectDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

AddProjectDialog.prototype.clear = function () {
  var self = this;
  $("input", self.getElement()).val("");
  self.setFileContent(undefined);
  return Promise.resolve();
};

AddProjectDialog.prototype.processFile = function (file) {
  var self = this;
  self.setFileContent(null);
  if (file) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      reader.readAsArrayBuffer(file);
      reader.onload = function (evt) {
        try {
          self.setFileContent(evt.target.result);
          resolve(self.getFileContent());
        } catch (error) {
          reject(error);
        }
      };
      reader.onerror = function () {
        reject(new Error("Problem reading file"));
      };
    });
  } else {
    return Promise.resolve(null);
  }
};

AddProjectDialog.prototype.setFileContent = function (fileContent) {
  this._fileContent = fileContent;
};

AddProjectDialog.prototype.setLicenseAccepted = function (isLicenseAccepted) {
  $("[name='project-google-maps-license']", this.getElement()).prop("checked", isLicenseAccepted);
};

/**
 *
 * @returns {boolean}
 */
AddProjectDialog.prototype.isLicenseAccepted = function () {
  return $("[name='project-google-maps-license']", this.getElement()).is(":checked");
};

AddProjectDialog.prototype.getFileContent = function () {
  return this._fileContent;
};

AddProjectDialog.prototype.setProjectId = function (projectId) {
  $("[name='project-id']", this.getElement()).val(projectId);
};

/**
 *
 * @param {string} mapCanvasTypeId
 */
AddProjectDialog.prototype.setMapCanvasTypeId = function (mapCanvasTypeId) {
  var select = $("[name='project-map-canvas-type']", this.getElement());
  select.val(mapCanvasTypeId);
  select.change();
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getMapCanvasTypeId = function () {
  return $("[name='project-map-canvas-type']", this.getElement()).val();
};


AddProjectDialog.prototype.getProjectId = function () {
  return $("[name='project-id']", this.getElement()).val();
};

AddProjectDialog.prototype.setName = function (name) {
  $("[name='project-name']", this.getElement()).val(name);
};
AddProjectDialog.prototype.setNotifyEmail = function (email) {
  $("[name='project-notify-email']", this.getElement()).val(email);
};

AddProjectDialog.prototype.getName = function () {
  return $("[name='project-name']", this.getElement()).val();
};

AddProjectDialog.prototype.getDisease = function () {
  return $("[name='project-disease']", this.getElement()).val();
};
AddProjectDialog.prototype.getOrganism = function () {
  return $("[name='project-organism']", this.getElement()).val();
};
AddProjectDialog.prototype.setOrganism = function (organism) {
  $("[name='project-organism']", this.getElement()).val(organism);
};

AddProjectDialog.prototype.getVersion = function () {
  return $("[name='project-version']", this.getElement()).val();
};
AddProjectDialog.prototype.getNotifyEmail = function () {
  return $("[name='project-notify-email']", this.getElement()).val();
};
AddProjectDialog.prototype.isAnnotateAutomatically = function () {
  return $("[name='project-annotate-automatically']", this.getElement()).is(':checked');
};
AddProjectDialog.prototype.setAnnotateAutomatically = function (value) {
  $("[name='project-annotate-automatically']", this.getElement()).prop('checked', value);
};
AddProjectDialog.prototype.isVerifyAnnotations = function () {
  return $("[name='project-verify-annotations']", this.getElement()).is(':checked');
};
AddProjectDialog.prototype.setVerifyAnnotations = function (value) {
  $("[name='project-verify-annotations']", this.getElement()).prop('checked', value);
};
AddProjectDialog.prototype.isCache = function () {
  return $("[name='project-cache-data']", this.getElement()).is(':checked');
};
AddProjectDialog.prototype.setCache = function (value) {
  return $("[name='project-cache-data']", this.getElement()).prop('checked', value).trigger("change");
};
AddProjectDialog.prototype.isAutoMargin = function () {
  return $("[name='project-auto-margin']", this.getElement()).is(':checked');
};
AddProjectDialog.prototype.setAutoMargin = function (value) {
  $("[name='project-auto-margin']", this.getElement()).prop('checked', value);
};
AddProjectDialog.prototype.isSbgn = function () {
  return $("[name='project-sbgn-visualization']", this.getElement()).is(':checked');
};
AddProjectDialog.prototype.setSbgn = function (value) {
  $("[name='project-sbgn-visualization']", this.getElement()).prop('checked', value);
};
AddProjectDialog.prototype.isSemanticZooming = function () {
  return $("[name='project-semantic-zooming']", this.getElement()).is(':checked');
};
AddProjectDialog.prototype.setSemanticZooming = function (value) {
  $("[name='project-semantic-zooming']", this.getElement()).prop('checked', value);
};

AddProjectDialog.prototype.setFileParserForFilename = function (filename) {
  var self = this;
  self._filename = filename;
  var select = $("[name='project-format']", self.getElement)[0];
  var options = select.options;
  var optionId = 0;
  for (var i = 0; i < options.length; i++) {
    var option = options[i];
    if (option.value !== undefined) {
      var extension = option.data;
      if (filename.endsWith(extension) && optionId <= 0) {
        optionId = i;
      }
    }
  }
  options[optionId].selected = true;
};
AddProjectDialog.prototype.getConverter = function () {
  var self = this;
  var select = $("[name='project-format']", self.getElement)[0];
  return ServerConnector.getConfiguration().then(function (configuration) {
    var handler = select.options[select.selectedIndex].value;
    var converters = configuration.getModelConverters();
    for (var i = 0; i < converters.length; i++) {
      var converter = converters[i];
      if (handler === converter.handler) {
        return converter;
      }
    }
    return null;
  });
};

/**
 *
 * @returns {Promise<Project>}
 */
AddProjectDialog.prototype.onSaveClicked = function () {
  var self = this;
  var parserClass;
  return self.checkValidity().then(function () {
    return self.getConverter();
  }).then(function (converter) {
    if (converter !== null) {
      parserClass = converter.handler;
    }
    GuiConnector.showProcessing("Uploading...");
    return ServerConnector.uploadFile({filename: self._filename, content: self.getFileContent()});
  }).then(function (file) {
    var options = {
      "projectId": self.getProjectId(),
      "name": self.getName(),
      "parser": parserClass,
      "file-id": file.id,
      "auto-resize": self.isAutoMargin(),
      "cache": self.isCache(),
      "notify-email": self.getNotifyEmail(),
      "disease": self.getDisease(),
      "version": self.getVersion(),
      "organism": self.getOrganism(),
      "mapCanvasType": self.getMapCanvasTypeId(),
      "sbgn": self.isSbgn(),
      "semantic-zoom": self.isSemanticZooming(),
      "annotate": self.isAnnotateAutomatically(),
      "verify-annotations": self.isVerifyAnnotations(),
      "zip-entries": self.getZipEntries()
    };
    return ServerConnector.addProject(options);
  }).then(function (project) {
    self.callListeners("onProjectAdd", project);
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

AddProjectDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>Some data is missing.</b><ul>";

  var mapCanvasTypeId = self.getMapCanvasTypeId();
  if (mapCanvasTypeId === "GOOGLE_MAPS_API") {
    if (!self.isLicenseAccepted()) {
      isValid = false;
      error += "<li>Please accept Google Maps License when choosing Google Maps API engine</li>";
    }
  }

  if (self.getFileContent() === null || self.getFileContent() === undefined) {
    error += "<li>Please select file before uploading</li>";
    isValid = false;
  }
  return self.getConverter().then(function (converter) {
    if (converter === null) {
      error += "<li>Please select converter before uploading</li>";
      isValid = false;
    }
    var projectId = self.getProjectId();
    if (!(/^[a-z0-9A-Z\-_]+$/.test(projectId))) {
      error += "<li>projectId can contain only alphanumeric characters and -_</li>";
      isValid = false;
    }
    return ServerConnector.getProject(self.getProjectId());
  }).then(function (project) {
    if (project !== null) {
      error += "<li>Project with given id already exists</li>";
      isValid = false;
    }
    error += "</ul>";
    if (isValid) {
      return Promise.resolve(true);
    } else {
      return Promise.reject(new ValidationError(error));
    }
  });
};

AddProjectDialog.prototype.setZipFileContent = function (file) {
  var self = this;
  if (file.name.toLowerCase().endsWith("zip")) {
    var jsZip = new JSZip();
    return jsZip.loadAsync(file).then(function (zip) {
      var files = zip.files;
      var promises = [];
      for (var key in files) {
        if (files.hasOwnProperty(key)) {
          promises.push(self.createZipEntry(files[key], zip));
        }
      }
      return Promise.all(promises);
    }).then(function (result) {
      var entries = [];
      var overlays = 0;
      var maps = 0;
      var images = 0;

      for (var i = 0; i < result.length; i++) {
        var entry = result[i];
        if (entry !== null) {
          entries.push(entry);
          if (entry.getType() === 'MAP') {
            maps++;
          } else if (entry.getType() === 'IMAGE') {
            images++;
          } else if (entry.getType() === 'OVERLAY') {
            overlays++;
          }
        }
      }
      if (overlays > 0) {
        self.enableTab({id: "project_overlays_tab"});
      } else {
        self.disableTab({id: "project_overlays_tab"});
      }
      if (maps > 1) {
        self.enableTab({id: "project_submaps_tab"});
      } else {
        self.disableTab({id: "project_submaps_tab"});
      }
      if (images > 1) {
        self.enableTab({id: "project_overview_images_tab"});
      } else {
        self.disableTab({id: "project_overview_images_tab"});
      }

      return self.setZipEntries(entries);
    });
  } else {
    self.disableTab({id: "project_overlays_tab"});
    self.disableTab({id: "project_submaps_tab"});
    self.disableTab({id: "project_overview_images_tab"});
    return self.setZipEntries([]);
  }
};

AddProjectDialog.prototype.createZipEntry = function (jsZipEntry, zipObject) {
  if (jsZipEntry.dir) {
    return null;
  }
  var filename = jsZipEntry.name.toLowerCase();
  var type;
  var data = {};
  var processingPromise = Promise.resolve();
  if (filename.indexOf("submaps") === 0) {
    type = "MAP";
    if (filename.endsWith("mapping.xml")) {
      data.mapping = true;
    }
  } else if (filename.indexOf("images") === 0) {
    type = "IMAGE";
  } else if (filename.indexOf("layouts") === 0 || filename.indexOf("overlays") === 0) {
    type = "OVERLAY";
    processingPromise = zipObject.file(jsZipEntry.name).async("string").then(function (content) {
      var overlayParser = new OverlayParser();
      var overlay = overlayParser.parse(content);
      if (overlay.getName()) {
        data.name = overlay.getName();
      } else {
        data.name = "";
      }
      if (overlay.getDescription()) {
        data.description = overlay.getDescription();
      } else {
        data.description = "";
      }
    });
  } else if (this.isIgnoredZipEntry(filename)) {
    type = undefined;
  } else if (filename.indexOf("\\") === -1 && filename.indexOf("/") === -1) {
    type = "MAP";
    data.root = true;
  } else {
    throw new Error("Unrecognized file: " + filename);
  }
  if (type === "MAP") {
    var name = jsZipEntry.name.toLowerCase();
    this.setFileParserForFilename(name);
    if (name.indexOf(".") > 0) {
      name = name.substr(0, name.indexOf("."));
    }
    if (name.lastIndexOf("\\") >= 0) {
      name = name.substr(name.lastIndexOf("\\") + 1);
    }
    if (name.lastIndexOf("/") >= 0) {
      name = name.substr(name.lastIndexOf("/") + 1);
    }
    data.name = name;

    processingPromise = processingPromise.then(function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var mapTypes = configuration.getMapTypes();
        for (var i = 0; i < mapTypes.length; i++) {
          if (mapTypes[i].id === "UNKNOWN") {
            data.type = mapTypes[i];
          }
        }
        if (data.type === undefined) {
          data.type = mapTypes[0];
        }
      });
    });
  }

  return processingPromise.then(function () {
    if (type !== undefined) {
      return new ZipEntry({filename: filename, type: type, data: data});
    } else {
      return null;
    }
  });
};

AddProjectDialog.prototype.getZipEntries = function () {
  return this._zipEntries;
};
AddProjectDialog.prototype.setZipEntries = function (entries) {
  var self = this;
  self._zipEntries = entries;
  return self.callListeners("onZipFileUpload", entries);

};

AddProjectDialog.prototype.isIgnoredZipEntry = function (filename) {
  var lowercaseString = filename.toLowerCase();
  // noinspection SpellCheckingInspection
  if (lowercaseString.indexOf("__macosx") === 0) {
    return true;
  } else if (lowercaseString.indexOf(".ds_store") === 0) {
    return true;
  }
  return false;
};

AddProjectDialog.prototype.getEntryByFilename = function (filename) {
  var self = this;
  var entries = self.getZipEntries();
  for (var i = 0; i < entries.length; i++) {
    var entry = entries[i];
    if (entry.getFilename() === filename) {
      return entry;
    }
  }
  return null;
};

AddProjectDialog.prototype.getFilename = function () {
  return this._filename;
};

module.exports = AddProjectDialog;
