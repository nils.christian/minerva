"use strict";

var Promise = require("bluebird");
var xss = require('xss');

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require('../../GuiConnector');
var ValidationError = require('../../ValidationError');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();

function EditUserDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.setUser(params.user);

  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
  self.registerListenerType("onSave");
}

EditUserDialog.prototype = Object.create(AbstractGuiElement.prototype);
EditUserDialog.prototype.constructor = EditUserDialog;

EditUserDialog.prototype.setUser = function (user) {
  this._user = user;
  this.setIsNewUser(user.getLogin() === undefined);
};
EditUserDialog.prototype.getUser = function () {
  return this._user;
};

EditUserDialog.prototype.setIsNewUser = function (isNewUser) {
  this._isNewUser = isNewUser;
};
EditUserDialog.prototype.getIsNewUser = function () {
  return this._isNewUser;
};

EditUserDialog.prototype.createGui = function () {
  var self = this;
  var element = self.getElement();

  var tabDiv = Functions.createElement({
    type: "div",
    name: "tabView",
    className: "tabbable boxed parentTabs",
    style: "position:absolute;top:10px;bottom:40px;left:10px;right:10px"
  });
  element.appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type: "div",
    className: "tab-content",
    style: "height:100%"
  });
  tabDiv.appendChild(tabContentDiv);

  self.createGeneralTab(tabMenuDiv, tabContentDiv);
  self.createPrivilegesTab(tabMenuDiv, tabContentDiv);
  self.createProjectsTab(tabMenuDiv, tabContentDiv);
  $("a", tabMenuDiv).bind("click", function () {
    //workaround for some css issues...
    tabDiv.style.top = "40px";
    tabDiv.style.bottom = "10px";
  });
};

EditUserDialog.prototype.createGeneralTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "DETAILS",
    content: self.createGeneralTabContent()
  });

};

var login_counter = 0;

EditUserDialog.prototype.generateTabId = function (tab_name) {
  var self = this;
  var login = self.getUser().getLogin();
  if (login === undefined) {
    login = "new_user_" + (login_counter++);
  }
  return login.replace(".", "_") + tab_name.replace(" ", "_") + "_TAB";
};

EditUserDialog.prototype.createPrivilegesTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "GLOBAL PRIVILEGES",
    content: self.createPrivilegesTabContent()
  });

};

EditUserDialog.prototype.addTab = function (params) {
  var id = this.generateTabId(params.name);

  var navLi = guiUtils.createTabMenuObject({
    id: id,
    name: params.name,
    navigationBar: params.tabMenuDiv
  });
  params.tabMenuDiv.appendChild(navLi);

  var contentDiv = guiUtils.createTabContentObject({
    id: id,
    navigationObject: navLi,
    navigationBar: params.tabMenuDiv
  });

  $(contentDiv).css("overflow","auto");
  if (params.content !== undefined) {
    contentDiv.appendChild(params.content);
  }

  params.tabContentDiv.appendChild(contentDiv);
};

function getStringIfDefined(value) {
  if (value === undefined) {
    return "";
  }
  return xss(value);
}

EditUserDialog.prototype.createGeneralTabContent = function () {

  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var table = Functions.createElement({
    type: "table",
    name: "detailsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(table);

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var saveUserButton = Functions.createElement({
    type: "button",
    name: "saveUser",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }, GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelProject",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveUserButton);
  menuRow.appendChild(cancelButton);

  return result;

};

EditUserDialog.prototype.createPrivilegesTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });
  result.appendChild(self._createPrivilegesTable());
  return result;
};

EditUserDialog.prototype._createPrivilegesTable = function () {
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var privilegesTable = Functions.createElement({
    type: "table",
    name: "privilegesTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(privilegesTable);

  return result;
};


EditUserDialog.prototype.createProjectsTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "PROJECT PRIVILEGES",
    content: self.createProjectsTabContent()
  });
};

EditUserDialog.prototype.createProjectsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });
  result.appendChild(self._createProjectsTable());
  return result;
};

EditUserDialog.prototype._createProjectsTable = function () {
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  result.appendChild(Functions.createElement({
    type: "div",
    name: "defaultProjectsRow",
    style: "width:100%"
  }));

  result.appendChild(Functions.createElement({
    type: "table",
    name: "projectsTable",
    className: "display",
    style: "width:100%"
  }));

  return result;
};

EditUserDialog.prototype.init = function () {
  var self = this;

  var detailsTable = $("[name=detailsTable]", self.getElement())[0];

  var dataTable = $(detailsTable).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });
  var data = [];

  var user = self.getUser();

  var readonly = '';
  if (user.getLogin() !== undefined) {
    readonly = ' readonly ';
  }
  data.push(['Login', '<input name="userLogin" value="' + getStringIfDefined(user.getLogin()) + '" ' + readonly + '/>']);
  data.push(['Password', '<input type="password" name="userPassword" value=""/>']);
  data.push(['Confirm password', '<input type="password" name="userPassword2" value=""/>']);
  data.push(['Name', '<input name="userName" value="' + getStringIfDefined(user.getName()) + '"/>']);
  data.push(['Surname', '<input name="userSurname" value="' + getStringIfDefined(user.getSurname()) + '"/>']);
  data.push(['Email', '<input name="userEmail" value="' + getStringIfDefined(user.getEmail()) + '"/>']);

  dataTable.clear().rows.add(data).draw();

  return self.initProjectsTab().then(function () {
    return self.refreshProjects();
  }).then(function () {
    return self.initPrivilegesTab();
  }).then(function () {
    $(window).trigger('resize');
  });
};

EditUserDialog.prototype.initProjectsTab = function () {
  var self = this;

  var usersTable = $("[name=projectsTable]", self.getElement())[0];

  var configuration;
  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    return self.createUserPrivilegeColumns()
  }).then(function (columns) {
    $(usersTable).DataTable({
      columns: columns
    });
    $(usersTable).on("click", "[name='project-privilege-checkbox']", function () {
      var data = $(this).attr("data").split("-");
      var privilegeType = data[0];
      var objectId = data[1];
      var value = $(this).is(":checked");
      for (var i = 0; i < configuration.getPrivilegeTypes().length; i++) {
        var privilege = configuration.getPrivilegeTypes()[i];
        if (privilege.getName() === privilegeType) {
          self.getUser().setPrivilege({type: privilege, value: value, objectId: objectId});
        }
      }
    });

    $("[name='defaultProjectsRow']", self.getElement()).on("click", "[name='project-privilege-checkbox']", function () {
      var data = $(this).attr("data").split("-");
      var privilegeType = data[0];
      var objectId = data[1];
      var value = $(this).is(":checked");
      for (var i = 0; i < configuration.getPrivilegeTypes().length; i++) {
        var privilege = configuration.getPrivilegeTypes()[i];
        if (privilege.getName() === privilegeType) {
          self.getUser().setPrivilege({type: privilege, value: value, objectId: objectId});
        }
      }
    });

  });
};

EditUserDialog.prototype.initPrivilegesTab = function () {
  var self = this;

  var usersTable = $("[name=privilegesTable]", self.getElement())[0];

  return ServerConnector.getConfiguration().then(function (configuration) {
    var columns = [{
      title: "Name"
    }, {
      title: "Value"
    }];
    var dataTable = $(usersTable).DataTable({
      columns: columns
    });
    var data = [];

    for (var i = 0; i < configuration.getPrivilegeTypes().length; i++) {
      var privilege = configuration.getPrivilegeTypes()[i];
      if (privilege.getObjectType() === null) {
        if (privilege.getValueType() === "boolean") {
          var checked = "";
          if (self.getUser().hasPrivilege(privilege)) {
            checked = "checked"
          }
          data.push([
            "<span>" + privilege.getCommonName() + "</span>",
            "<input type='checkbox' name='privilege-checkbox' data='" + privilege.getName() + "' " + checked + " />"
          ]);
        } else if (privilege.getValueType() === "int") {
          data.push([
            "<span>" + privilege.getCommonName() + "</span>",
            "<input name='privilege-int' data='" + privilege.getName() + "' value='" + self.getUser().getPrivilegeValue(privilege) + "' />"
          ]);

        }
      }
    }
    dataTable.clear().rows.add(data).draw();
    $(usersTable).on("click", "[name='privilege-checkbox']", function () {
      var privilegeType = $(this).attr("data");
      for (var i = 0; i < configuration.getPrivilegeTypes().length; i++) {
        var privilege = configuration.getPrivilegeTypes()[i];
        if (privilege.getName() === privilegeType) {
          self.getUser().setPrivilege({type: privilege, value: $(this).is(":checked")});
        }
      }
    });
    $(usersTable).on("change", "[name='privilege-int']", function () {
      var privilegeType = $(this).attr("data");
      var value = $(this).val();
      value = parseInt(value);
      if (Functions.isInt(value)) {
        for (var i = 0; i < configuration.getPrivilegeTypes().length; i++) {
          var privilege = configuration.getPrivilegeTypes()[i];
          if (privilege.getName() === privilegeType) {
            self.getUser().setPrivilege({type: privilege, value: value});
          }
        }
      }
    });
  });
};

EditUserDialog.prototype.refreshProjects = function () {
  var self = this;
  return ServerConnector.getProjects().then(function (projects) {
    return self.setProjects(projects);
  });
};

EditUserDialog.prototype.setProjects = function (projects) {
  var self = this;
  self._userByLogin = [];
  return self.createUserPrivilegeColumns().then(function (columns) {
    var dataTable = $("[name='projectsTable']", self.getElement()).DataTable();
    var data = [], i;

    var rowData = self.projectToTableRow(null, columns);
    var defaultRow = $("[name='defaultProjectsRow']", self.getElement())[0];
    defaultRow.innerHTML = "";
    defaultRow.appendChild(Functions.createElement({
      type: "span",
      content: rowData[0],
      xss: false
    }));
    defaultRow.appendChild(Functions.createElement({type: "br"}));
    for (i = 1; i < columns.length; i++) {
      defaultRow.appendChild(Functions.createElement({
        type: "div",
        content: rowData[i] + columns[i].title,
        style: "float:left;padding:5px;",
        xss: false
      }));
    }
    defaultRow.appendChild(Functions.createElement({type: "br"}));
    defaultRow.appendChild(Functions.createElement({type: "hr"}));
    // data.push(rowData);
    for (i = 0; i < projects.length; i++) {
      var project = projects[i];
      rowData = self.projectToTableRow(project, columns);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

EditUserDialog.prototype.projectToTableRow = function (project, columns) {
  var user = this.getUser();
  var row = [];
  var id = null;
  var projectId = "<b>DEFAULT PRIVILEGE FOR NEW PROJECT</b>";
  if (project !== null) {
    id = project.getId();
    projectId = project.getProjectId();
  }

  row[0] = "<span data='" + id + "'>" + projectId + "</span>";

  for (var i = 1; i < columns.length; i++) {
    var privilege = columns[i].privilegeType;
    var checked = "";
    if (user.hasPrivilege(privilege, id)) {
      checked = "checked";
    }
    row.push("<input type='checkbox' name='project-privilege-checkbox' data='" + privilege.getName() + "-" + id + "' " + checked + " />");
  }

  return row;
};


EditUserDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  var usersTable = $("[name=projectsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(usersTable)) {
    $(usersTable).DataTable().destroy();
  }

  var privilegesTable = $("[name=privilegesTable]", div)[0];
  if ($.fn.DataTable.isDataTable(privilegesTable)) {
    $(privilegesTable).DataTable().destroy();
  }

  var detailsTable = $("[name=detailsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(detailsTable)) {
    $(detailsTable).DataTable().destroy();
  }


  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
};

EditUserDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  var title = self.getUser().getLogin();
  if (title === undefined) {
    title = "NEW USER";
  }
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      title: title,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2
    });
  }
  $(div).dialog("open");
};

EditUserDialog.prototype.onSaveClicked = function () {
  var self = this;
  var user = self.getUser();
  return self.checkValidity().then(function () {
    user.setLogin(self.getLogin());
    user.setPassword(self.getPassword());
    user.setEmail(self.getEmail());
    user.setName(self.getName());
    user.setSurname(self.getSurname());
    if (self.getIsNewUser()) {
      return ServerConnector.addUser(user);
    } else {
      return ServerConnector.updateUser(user);
    }
  }).then(function () {
    return self.callListeners("onSave", user);
  });
};

EditUserDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>Some data is missing.</b><ul>";
  if (self.getPassword() !== self.getPassword2()) {
    error += "<li>Password doesn't match</li>";
    isValid = false;
  }
  if (self.getLogin() === "" || self.getLogin() === undefined || self.getLogin() === null) {
    error += "<li>Login must not be empty</li>";
    isValid = false;
  }
  var promise = Promise.resolve();
  if (self.getUser().getLogin() !== self.getLogin()) {
    promise = ServerConnector.getUser(self.getLogin()).then(function (remoteUser) {
      if (remoteUser !== null) {
        error += "<li>Login already used</li>";
        isValid = false;
      }
    });
  }
  return promise.then(function () {
    if (isValid) {
      return Promise.resolve(true);
    } else {
      return Promise.reject(new ValidationError(error));
    }
  });
};

EditUserDialog.prototype.getPassword = function () {
  var self = this;
  return $("[name='userPassword']", self.getElement()).val();
};
EditUserDialog.prototype.getPassword2 = function () {
  var self = this;
  return $("[name='userPassword2']", self.getElement()).val();
};

EditUserDialog.prototype.getLogin = function () {
  var self = this;
  return $("[name='userLogin']", self.getElement()).val();
};

EditUserDialog.prototype.getEmail = function () {
  var self = this;
  return $("[name='userEmail']", self.getElement()).val();
};
EditUserDialog.prototype.getName = function () {
  var self = this;
  return $("[name='userName']", self.getElement()).val();
};
EditUserDialog.prototype.getSurname = function () {
  var self = this;
  return $("[name='userSurname']", self.getElement()).val();
};


EditUserDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

EditUserDialog.prototype.createUserPrivilegeColumns = function () {
  var self = this;

  if (self._userPrivilegeColumns !== undefined) {
    return Promise.resolve(self._userPrivilegeColumns);
  }

  return ServerConnector.getConfiguration().then(function (configuration) {
    self._userPrivilegeColumns = [{
      title: "ProjectId"
    }];
    var privilegeTypes = configuration.getPrivilegeTypes();
    for (var i = 0; i < privilegeTypes.length; i++) {
      var type = privilegeTypes[i];
      if (type.getObjectType() === "Project") {
        self._userPrivilegeColumns.push({
          "title": type.getCommonName(),
          privilegeType: type
        });
      }
    }
    return self._userPrivilegeColumns;
  });

};


module.exports = EditUserDialog;
