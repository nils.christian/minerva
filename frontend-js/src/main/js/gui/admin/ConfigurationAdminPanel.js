"use strict";

/* exported Promise*/

var AbstractAdminPanel = require('./AbstractAdminPanel');
var PrivilegeType = require('../../map/data/PrivilegeType');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');

/* exported logger */
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");
var xss = require("xss");

function ConfigurationAdminPanel(params) {
  AbstractAdminPanel.call(this, params);

  var self = this;
  $(self.getElement()).addClass("minerva-configuration-tab");

  self._createGui();
}

ConfigurationAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
ConfigurationAdminPanel.prototype.constructor = ConfigurationAdminPanel;

ConfigurationAdminPanel.prototype._createGui = function () {
  var self = this;
  var configurationDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(configurationDiv);

  configurationDiv.appendChild(Functions.createElement({
    type: "h3",
    content: 'Configuration category: <select name="categorySelect"></select>',
    xss: false
  }));

  configurationDiv.appendChild(Functions.createElement({
    type: "br"
  }));

  var configurationTable = Functions.createElement({
    type: "table",
    name: "configurationTable",
    className: "display",
    style: "width:100%"
  });
  configurationDiv.appendChild(configurationTable);

  // noinspection JSUnusedGlobalSymbols
  $(configurationTable).DataTable({
    columns: [{
      title: 'Category'
    }, {
      title: 'Name'
    }, {
      title: 'Value'
    }, {
      title: 'Save',
      orderable: false
    }],
    order: [[1, "asc"]]
  });
  self.bindUserGuiPreference({
    jQueryObject: $(configurationTable),
    event: 'length.dt',
    preferenceName: 'admin-configuration-datatable-length',
    defaultValue: '10',
    getter: function () {
      return $(configurationTable).DataTable().page.len() + '';
    },
    setter: function (value) {
      return $(configurationTable).DataTable().page.len(value).draw();
    }
  });

  $(configurationTable).on("click", "[name='saveOption']", function () {
    var button = this;
    return self.saveOption($(button).attr("data")).then(null, GuiConnector.alert);
  });
  $(configurationTable).on("input", ".minerva-color-input", function () {
    var input = this;
    var value = $(input).val();
    var type = $(input).attr("data");
    if (value.length !== 6) {
      value = "FFFFFF";
    }
    $("[name='edit-color-" + type + "']", configurationTable).css("background-color", "#" + value);
  });
  $(configurationTable).on("click", ".minerva-color-button", function () {
    var button = this;
    var value = $(button).css("background-color");
    var type = $(button).attr("data");
    var colorPicker = $(button).parent().spectrum({
      color: value,
      move: function (color) {
        var value = color.toHexString().replace("#", '');
        $("[name='edit-" + type + "']", configurationTable).val(value);
        $("[name='edit-color-" + type + "']", configurationTable).css("background-color", "#" + value);
      },
      hide: function () {
        colorPicker.spectrum("destroy");
      }
    });
    return new Promise.delay(1).then(function () {
      colorPicker.show();
      colorPicker.spectrum("show");
    });
  });
};

/**
 *
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.init = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {
    var configuration = self.getConfiguration();
    var privilege = configuration.getPrivilegeType(PrivilegeType.CONFIGURATION_MANAGE);
    if (user.hasPrivilege(privilege)) {
      self.setOptions(configuration.getOptions(), true);
    } else {
      self.disablePanel("You have no privilege to manage configuration");
    }
  });
};

/**
 *
 * @param {ConfigurationOption[]} options
 * @param {boolean} editable
 */
ConfigurationAdminPanel.prototype.setOptions = function (options, editable) {
  var self = this;
  var dataTable = $("[name='configurationTable']", self.getElement()).DataTable();
  var data = [];

  var categoryDropDown = $("[name='categorySelect']", self.getElement());
  categoryDropDown.empty();

  var categories = {"": true};
  categoryDropDown.append('<option value=""></option>');
  for (var i = 0; i < options.length; i++) {
    var option = options[i];
    var rowData = self.optionToTableRow(option, editable);
    data.push(rowData);

    var group = option.getGroup();
    if (categories[group] === undefined && group !== undefined) {
      categories[group] = true;
      categoryDropDown.append('<option value="' + group + '">' + group + '</option>')
    }
  }
  categoryDropDown.change(function () {
    logger.warn($(this).val());
    dataTable.column(0).search($(this).val()).draw();
  });

  dataTable.clear().rows.add(data).draw();
};

/**
 *
 * @param {ConfigurationOption} option
 * @param {boolean} editable
 * @returns {Array}
 */
ConfigurationAdminPanel.prototype.optionToTableRow = function (option, editable) {
  var value = option.getValue();
  var row = [];
  var editOption;
  var disabled = '';
  if (!editable) {
    disabled = " disabled ";
  }

  if (option.getValueType() === "STRING" ||
    option.getValueType() === "INTEGER" ||
    option.getValueType() === "DOUBLE" ||
    option.getValueType() === "EMAIL" ||
    option.getValueType() === "PASSWORD" ||
    option.getValueType() === "URL") {
    editOption = "<input name='edit-" + option.getType() + "' value='" + value + "'/>";
  } else if (option.getValueType() === "TEXT") {
    editOption = "<textarea name='edit-" + option.getType() + "'>" + xss(value) + "</textarea>";
  } else if (option.getValueType() === "BOOLEAN") {
    var checked = "";
    if (value.toLowerCase() === "true") {
      checked = " checked ";
    }
    editOption = "<input type='checkbox' name='edit-" + option.getType() + "' " + checked + " />";
  } else if (option.getValueType() === "COLOR") {
    editOption = "<div>" +
      "<input class='minerva-color-input' name='edit-" + option.getType() + "' data='" + option.getType() + "' value='" + value + "'/>" +
      "<button class='minerva-color-button' name='edit-color-" + option.getType() + "' data='" + option.getType() + "' style='background-color: #" + value + "'>&nbsp</button>" +
      "</div>";
  } else {
    logger.warn("Don't know how to handle: " + option.getValueType());
    editOption = "<input name='edit-" + option.getType() + "' value='" + value + "' readonly/>";
  }
  var group = option.getGroup();
  if (group === undefined) {
    group = "";
  }
  row[0] = group;
  row[1] = option.getCommonName();
  row[2] = editOption;
  row[3] = "<button name='saveOption' data='" + option.getType() + "' " + disabled + "><i class='fa fa-save' style='font-size:17px'></i></button>";
  return row;
};

ConfigurationAdminPanel.prototype.saveOption = function (type) {
  var self = this;
  return self.getServerConnector().getConfiguration().then(function (configuration) {
    var option = configuration.getOption(type);
    var element = $("[name='edit-" + type + "']", self.getElement());
    var value;
    if (element.is(':checkbox')) {
      if (element.is(':checked')) {
        value = "true";
      } else {
        value = "false";
      }
    } else {
      value = element.val();
    }
    option.setValue(value);
    return self.getServerConnector().updateConfigurationOption(option);
  });
};
ConfigurationAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='configurationTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    $(table).DataTable().destroy();
  }

};

module.exports = ConfigurationAdminPanel;
