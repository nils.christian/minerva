"use strict";

/* exported logger */
/* exported Promise*/

var Panel = require('../Panel');
var UserPreferences = require('../../map/data/UserPreferences');
var GuiConnector = require('../../GuiConnector');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
// noinspection JSUnusedLocalSymbols
var Functions = require('../../Functions');
var Promise = require("bluebird");

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {CustomMap} params.customMap
 * @param {string} params.panelName
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function AbstractAdminPanel(params) {
  params["scrollable"] = true;
  Panel.call(this, params);
  this._createHeader(params.name);

  this._initPromises = [];
  this._eventBinds = [];
}

AbstractAdminPanel.prototype = Object.create(Panel.prototype);
AbstractAdminPanel.prototype.constructor = AbstractAdminPanel;

AbstractAdminPanel.prototype._createHeader = function (name) {
  // this.getElement().appendChild(Functions.createElement({
  //   type: "h1",
  //   content: name
  // }));
};

/**
 *
 * @returns {Promise}
 */
AbstractAdminPanel.prototype.init = function () {
  return Promise.all(this._initPromises);
};

/**
 *
 * @param {Object} params
 * @param {string} params.event
 * @param {jQuery} params.jQueryObject
 * @param {string} params.preferenceName
 * @param {Object} params.defaultValue
 * @param {function} params.getter
 * @param {function} params.setter
 */
AbstractAdminPanel.prototype.bindUserGuiPreference = function (params) {
  params.jQueryObject.on(params.event, function () {
    return ServerConnector.getLoggedUser().then(function (user) {
      var oldValue = user.getPreferences().getGuiPreference(params.preferenceName, params.defaultValue);
      var newValue = params.getter();
      if (oldValue !== newValue) {
        user.getPreferences().setGuiPreference(params.preferenceName, newValue);

        var data = new UserPreferences();
        data.setGuiPreference(params.preferenceName, newValue);
        return ServerConnector.updateUserPreferences({user: user, preferences: data});
      }
    }).catch(GuiConnector.alert);
  });
  this._initPromises.push(ServerConnector.getLoggedUser().then(function (user) {
    var value = user.getPreferences().getGuiPreference(params.preferenceName, params.defaultValue);
    return params.setter(value);
  }));

  this._eventBinds.push(params);
};

/**
 *
 * @returns {Promise}
 */
AbstractAdminPanel.prototype.destroy = function () {
  return Promise.each(this._eventBinds, function (params) {
    params.jQueryObject.off(params.event);
  });
};


module.exports = AbstractAdminPanel;
