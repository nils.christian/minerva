"use strict";

var Promise = require("bluebird");

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');


function LogListDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createLogListDialogGui();
  self._level = params.level;
  self._projectId = params.projectId;
}

LogListDialog.prototype = Object.create(AbstractGuiElement.prototype);
LogListDialog.prototype.constructor = LogListDialog;

LogListDialog.prototype.createLogListDialogGui = function () {
  var self = this;
  var head = Functions.createElement({
    type: "thead",
    content: "<tr>" + "<th>ID</th>" + //
    "<th>Content</th>" + //
    "</tr>"//
  });
  var body = Functions.createElement({
    type: "tbody"
  });
  var tableElement = Functions.createElement({
    type: "table",
    className: "minerva-logs-table",
    style: "width: 100%"
  });

  tableElement.appendChild(head);
  tableElement.appendChild(body);

  self.tableElement = tableElement;
  self.getElement().appendChild(tableElement);

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  self.getElement().appendChild(menuRow);

  var downloadButton = Functions.createElement({
    type: "button",
    name: "saveUser",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;DOWNLOAD',
    onclick: function () {
      GuiConnector.showProcessing();
      return self.downloadAll().then(function (result) {
        var blob = new Blob([result], {
          type: "text/plain;charset=utf-8"
        });
        var FileSaver = require("file-saver");
        return FileSaver.saveAs(blob, self._projectId + "-logs.txt");
      }).then(function () {
        GuiConnector.hideProcessing();
      }, function (error) {
        GuiConnector.hideProcessing();
        GuiConnector.alert(error);
      });
    },
    xss: false
  });
  menuRow.appendChild(downloadButton);

};

LogListDialog.prototype._dataTableAjaxCall = function (data, callback) {
  var self = this;
  return ServerConnector.getProjectLogs({
    start: data.start,
    length: data.length,
    sortColumn: self.getColumnsDefinition()[data.order[0].column].name,
    sortOrder: data.order[0].dir,
    search: data.search.value,
    projectId: self._projectId,
    level: self._level
  }).then(function (logEntries) {
    var out = [];
    for (var i = 0; i < logEntries.data.length; i++) {
      var entry = logEntries.data[i];

      var row = [];
      row[0] = entry.id;
      row[1] = entry.content;
      out.push(row);
    }
    callback({
      draw: data.draw,
      recordsTotal: logEntries.totalSize,
      recordsFiltered: logEntries.filteredSize,
      data: out
    });
  });
};

LogListDialog.prototype.downloadAll = function (data, callback) {
  var self = this;
  return ServerConnector.getProjectLogs({
    length: 2147483646,
    projectId: self._projectId,
    level: self._level
  }).then(function (logEntries) {
    var tmp = [];
    for (var i = 0; i < logEntries.data.length; i++) {
      tmp.push(logEntries.data[i].content);
    }
    return tmp.join("\n");
  });
};

LogListDialog.prototype.open = function () {
  var self = this;
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      title: "Log list",
      autoOpen: false,
      resizable: false,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  if (!$.fn.DataTable.isDataTable(self.tableElement)) {
    return new Promise(function (resolve) {
      $(self.tableElement).dataTable({
        serverSide: true,
        ordering: true,
        searching: true,
        ajax: function (data, callback, settings) {
          resolve(self._dataTableAjaxCall(data, callback, settings));
        },
        columns: self.getColumnsDefinition()
      });
    });
  } else {
    return Promise.resolve();
  }

};

LogListDialog.prototype.getColumnsDefinition = function () {
  return [{
    name: "id"
  }, {
    name: "content"
  }];
};

LogListDialog.prototype.init = function () {
  return Promise.resolve();
};
LogListDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable(self.tableElement)) {
    $(self.tableElement).DataTable().destroy();
  }
};

module.exports = LogListDialog;
