"use strict";

var Promise = require('bluebird');

var PrivilegeType = require('../../map/data/PrivilegeType');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var EditUserDialog = require('./EditUserDialog');

var User = require("../../map/data/User");

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 *
 * @constructor
 * @extends AbstractAdminPanel
 */
function UsersAdminPanel(params) {
  var self = this;
  AbstractAdminPanel.call(self, params);
  self._createGui();
  $(self.getElement()).addClass("minerva-users-tab");
}

UsersAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
UsersAdminPanel.prototype.constructor = UsersAdminPanel;

/**
 *
 * @private
 */
UsersAdminPanel.prototype._createGui = function () {
  var self = this;
  var usersDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(usersDiv);

  var dataDiv = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  usersDiv.appendChild(dataDiv);

  dataDiv.appendChild(self._createMenuRow());
  dataDiv.appendChild(self._createUsersTableRow());
  dataDiv.appendChild(self._createMenuRow());

};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
UsersAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addUserButton = Functions.createElement({
    type: "button",
    name: "addProject",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD USER',
    onclick: function () {
      return self.onAddClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshProject",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addUserButton);
  menuRow.appendChild(refreshButton);
  return menuRow;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
UsersAdminPanel.prototype._createUsersTableRow = function () {
  var self = this;
  var projectsRow = Functions.createElement({
    type: "div",
    style: "display:table-row; width:100%"
  });

  var usersTable = Functions.createElement({
    type: "table",
    name: "usersTable",
    className: "display",
    style: "width:100%"
  });
  projectsRow.appendChild(usersTable);

  // noinspection JSCheckFunctionSignatures
  $(usersTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', aData[0]);
    },
    columns: [{
      title: 'Login'
    }, {
      title: 'Name'
    }, {
      title: 'Surname'
    }, {
      title: 'Email'
    }, {
      title: 'Authentication'
    }, {
      title: 'Edit'
    }, {
      title: 'Remove'
    }]
  });

  self.bindUserGuiPreference({
    jQueryObject: $(usersTable),
    event: 'length.dt',
    preferenceName: 'admin-users-datatable-length',
    defaultValue: '10',
    getter: function () {
      return $(usersTable).DataTable().page.len() + '';
    },
    setter: function (value) {
      return $(usersTable).DataTable().page.len(value).draw();
    }
  });

  $(usersTable).on("click", "[name='removeUser']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove '" + $(button).attr("data") + "'user?",
      input: false
    }).then(function (param) {
      if (param.status) {
        return self.removeUser($(button).attr("data"))
      }
    }).catch(GuiConnector.alert);
  });

  $(usersTable).on("click", "[name='ldap-auth']", function () {
    var field = this;
    var login = $(this).attr('data');
    GuiConnector.showProcessing();
    return self.getServerConnector().getUser(login).then(function (user) {
      var newIsConnected = $('input:checked', field).val() === "LDAP";
      var isConnected = user.isConnectedToLdap();
      if (isConnected !== newIsConnected) {
        console.log(newIsConnected);
        user.setConnectedToLdap(newIsConnected);
        return self.getServerConnector().updateUser(user);
      } else {
        console.log('dont change');
      }
    }).catch(function (error) {
      GuiConnector.alert(error);
    }).finally(function(){
      GuiConnector.hideProcessing();
    });
  });

  $(usersTable).on("click", "[name='showEditDialog']", function () {
    var button = this;
    return self.showEditDialog($(button).attr("data")).then(null, GuiConnector.alert);
  });

  return projectsRow;
};

/**
 *
 * @param {String} login
 * @returns {Promise}
 */
UsersAdminPanel.prototype.showEditDialog = function (login) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().getUser(login).then(function (user) {
    return self.getDialog(user);
  }).then(function (dialog) {
    dialog.open();
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {User} user
 * @returns {Promise}
 */
UsersAdminPanel.prototype.getDialog = function (user) {
  var self = this;
  if (self._dialogs === undefined) {
    self._dialogs = [];
  }
  var dialog = self._dialogs[user.getLogin()];
  if (dialog === undefined) {
    dialog = new EditUserDialog({
      element: Functions.createElement({
        type: "div"
      }),
      user: user,
      customMap: null
    });
    self._dialogs[user.getLogin()] = dialog;
    if (user.getLogin() === undefined) {
      dialog.addListener("onSave", function () {
        return self.onRefreshClicked();
      });
    }
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

/**
 *
 * @returns {Promise}
 */
UsersAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var privilege = self.getConfiguration().getPrivilegeType(PrivilegeType.USER_MANAGEMENT);
    if (user.hasPrivilege(privilege)) {
      return self.getServerConnector().getUsers().then(function (users) {
        return self.setUsers(users);
      });
    } else {
      self.disablePanel("You have no privilege to manage users");
    }
  });
};

/**
 *
 * @param {User[]} users
 */
UsersAdminPanel.prototype.setUsers = function (users) {
  var self = this;
  var dataTable = $($("[name='usersTable']", self.getElement())[0]).DataTable();
  var page = dataTable.page();
  var data = [];
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    var rowData = self.userToTableRow(user);
    self.addUpdateListener(user, rowData);
    data.push(rowData);
  }
  //it should be simplified, but I couldn't make it work
  dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);
};


/**
 *
 * @param {User} user
 * @param {Array} dataTableRow
 */
UsersAdminPanel.prototype.addUpdateListener = function (user, dataTableRow) {
  var self = this;

  var listenerName = "USER_LIST_LISTENER";
  var listeners = user.getListeners("onreload");
  for (var i = 0; i < listeners.length; i++) {
    if (listeners[i].listenerName === listenerName) {
      user.removeListener("onreload", listeners[i]);
    }
  }
  var listener = function () {
    var login = user.getLogin().replace(".", "\\.");
    self.userToTableRow(user, dataTableRow);
    var row = $($("[name='usersTable']", self.getElement())[0]).DataTable().row("#" + login);
    if (row.length > 0) {
      row.data(dataTableRow).draw();
    }

  };
  listener.listenerName = listenerName;
  user.addListener("onreload", listener);
};

/**
 *
 * @param {User} user
 * @param {Array} [row]
 * @returns {Array}
 */
UsersAdminPanel.prototype.userToTableRow = function (user, row) {
  if (row === undefined) {
    row = [];
  }

  row[0] = user.getLogin();
  row[1] = user.getName();
  row[2] = user.getSurname();
  row[3] = user.getEmail();
  var ldapFieldId = 'ldap-auth-' + user.getLogin();
  if (user.isConnectedToLdap()) {
    row[4] = "<fieldset name='ldap-auth' id='" + ldapFieldId + "' data='" + user.getLogin() + "'> <input type='radio' name='" + ldapFieldId + "' value='LOCAL'> LOCAL <input type='radio' name='" + ldapFieldId + "' checked value='LDAP'> LDAP </fieldset>";
  } else {
    if (user.isLdapAccountAvailable()) {
      row[4] = "<fieldset name='ldap-auth' id='" + ldapFieldId + "' data='" + user.getLogin() + "'> <input type='radio' name='" + ldapFieldId + "' value='LOCAL' checked> LOCAL <input type='radio' name='" + ldapFieldId + "' value='LDAP'> LDAP </fieldset>";
      // row[4] = "LOCAL <button name='connectLdap' data='" + user.getLogin() + "'>CONNECT LDAP</button>"
    } else {
      row[4] = "<fieldset name='ldap-auth' id='" + ldapFieldId + "' data='" + user.getLogin() + "'> <input type='radio' name='" + ldapFieldId + "' value='LOCAL' checked> LOCAL</fieldset>";
    }
  }

  row[5] = "<button name='showEditDialog' data='" + user.getLogin() + "'><i class='fa fa-edit' style='font-size:17px'></i></button>";
  row[6] = "<button name='removeUser' data='" + user.getLogin() + "'><i class='fa fa-trash-o' style='font-size:17px'></i></button>";

  return row;
};

/**
 *
 * @returns {Promise}
 */
UsersAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return self.getServerConnector().getUsers(true).then(function (users) {
    return self.setUsers(users);
  });
};


/**
 *
 */
UsersAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='usersTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    $(table).DataTable().destroy();
  }

  for (var key in self._dialogs) {
    if (self._dialogs.hasOwnProperty(key)) {
      self._dialogs[key].destroy();
    }
  }
};

/**
 *
 * @returns {Promise}
 */
UsersAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  var user = new User({});
  GuiConnector.showProcessing();
  return self.getDialog(user).then(function (dialog) {
    dialog.open();
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {string} login
 * @returns {Promise}
 */
UsersAdminPanel.prototype.removeUser = function (login) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().removeUser(login).then(function () {
    return self.onRefreshClicked();
  }).then(function () {
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

module.exports = UsersAdminPanel;
