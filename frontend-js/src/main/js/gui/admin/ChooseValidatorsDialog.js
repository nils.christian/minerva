"use strict";

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var DualListbox = require('dual-listbox').DualListbox;
var GuiConnector = require("../../GuiConnector");
var UserPreferences = require("../../map/data/UserPreferences");
var MultiCheckboxList = require("multi-checkbox-list");

var Functions = require('../../Functions');
var logger = require('../../logger');

var Promise = require("bluebird");

function ChooseValidatorsDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createGui();
}

ChooseValidatorsDialog.prototype = Object.create(AbstractGuiElement.prototype);
ChooseValidatorsDialog.prototype.constructor = ChooseValidatorsDialog;

ChooseValidatorsDialog.prototype.createGui = function () {
  var self = this;
  var content = Functions.createElement({
    type: "div",
    style: "display:table;height:100%;width:100%"
  });
  content.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell;height:100%;",
    content: "<div name='elementTree' style='height:100%;overflow-y:scroll;'/>",
    xss: false
  }));

  content.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell;height:100%;width:100%;",
    content: "<div name='annotatorListBox' style='height:100%;width:100%;overflow-y:auto;'><span> </span></div>",
    xss: false
  }));

  self.getElement().appendChild(content);
};

ChooseValidatorsDialog.prototype.setElementType = function (elementType) {
  var self = this;

  var configuration;

  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    return ServerConnector.getLoggedUser();
  }).then(function (user) {
    var element = $("[name='annotatorListBox']", self.getElement())[0];
    Functions.removeChildren(element);

    var validAnnotationSelect = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    element.appendChild(validAnnotationSelect);
    self.createValidAnnotationsDualListBox(user, configuration, elementType, validAnnotationSelect);

    var verifyAnnotationSelect = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    element.appendChild(verifyAnnotationSelect);
    self.createVerifyAnnotationsDualListBox(user, configuration, elementType, verifyAnnotationSelect);
  });

};

ChooseValidatorsDialog.prototype.createValidAnnotationsDualListBox = function (user, configuration, elementType, validAnnotationSelect) {

  var miriamTypes = configuration.getMiriamTypes();

  var validAnnotations = user.getPreferences().getElementValidAnnotations(elementType.className);

  var entries = [];


  for (var i = 0; i < miriamTypes.length; i++) {
    var miriamType = miriamTypes[i];
    var entry = {name: miriamType.getCommonName(), value: miriamType.getName(), selected: false};

    var selected = false;
    for (var j = 0; j < validAnnotations.length; j++) {
      if (miriamType.getName() === validAnnotations[j]) {
        entry.selected = true;
      }
    }
    entries.push(entry);
  }

  var checkboxList = new MultiCheckboxList(validAnnotationSelect, {
    entries: entries,
    listTitle: "Available",
    selectedTitle: "Selected",
    selectedList: true
  });

  var changeSelection = function (elementId, selected) {
    var miriamTypes = configuration.getMiriamTypes();
    var miriamType;
    for (var i = 0; i < miriamTypes.length; i++) {
      if (elementId === miriamTypes[i].getName()) {
        miriamType = miriamTypes[i];
      }
    }
    if (selected) {
      validAnnotations.push(miriamType.getName());
    } else {
      var index = validAnnotations.indexOf(miriamType.getName());
      if (index > -1) {
        validAnnotations.splice(index, 1);
      }
    }

    var data = new UserPreferences();

    var elementAnnotators = {};
    elementAnnotators[elementType.className] = validAnnotations;
    data.setElementValidAnnotations(elementAnnotators);
    return ServerConnector.updateUserPreferences({user: user, preferences: data}).then(null, GuiConnector.alert);
  };

  checkboxList.addListener("select", function (element) {
    return changeSelection(element.value, true);
  });
  checkboxList.addListener("deselect", function (element) {
    return changeSelection(element.value, false);
  });

};

ChooseValidatorsDialog.prototype.createVerifyAnnotationsDualListBox = function (user, configuration, elementType, verifyAnnotationSelect) {
  var requiredAnnotationsData = user.getPreferences().getElementRequiredAnnotations(elementType.className);

  var verifyCheckboxDiv = Functions.createElement({type: "div"});
  var checkbox = Functions.createElement({
    id: "test",
    type: "input",
    inputType: "checkbox",
    onclick: function () {
      var data = new UserPreferences();

      var elementRequiredAnnotations = {};
      elementRequiredAnnotations[elementType.className] = {
        "require-at-least-one": checkbox.checked,
        "annotation-list": requiredAnnotationsData.list
      };
      data.setElementRequiredAnnotations(elementRequiredAnnotations);
      return ServerConnector.updateUserPreferences({user: user, preferences: data}).then(null, GuiConnector.alert);
    }
  });
  checkbox.checked = requiredAnnotationsData.requiredAtLeastOnce;
  verifyCheckboxDiv.appendChild(Functions.createElement({type: "br"}));
  verifyCheckboxDiv.appendChild(Functions.createElement({type: "br"}));
  verifyCheckboxDiv.appendChild(checkbox);
  verifyCheckboxDiv.appendChild(Functions.createElement({
    type: "span",
    content: "One of these miriam should be required"
  }));
  verifyAnnotationSelect.parentNode.insertBefore(verifyCheckboxDiv, verifyAnnotationSelect);

  var miriamTypes = configuration.getMiriamTypes();

  var entries = [];

  for (var i = 0; i < miriamTypes.length; i++) {
    var miriamType = miriamTypes[i];
    var entry = {name: miriamType.getCommonName(), value: miriamType.getName(), selected: false};
    for (var j = 0; j < requiredAnnotationsData.list.length; j++) {
      if (miriamType.getName() === requiredAnnotationsData.list[j]) {
        entry.selected = true;
      }
    }
    entries.push(entry);
  }

  var checkboxList = new MultiCheckboxList(verifyAnnotationSelect, {
    entries: entries,
    listTitle: "Available",
    selectedTitle: "Selected",
    selectedList: true
  });

  var changeSelection = function (elementId, selected) {

    var miriamTypes = configuration.getMiriamTypes();
    var miriamType;
    for (var i = 0; i < miriamTypes.length; i++) {
      if (elementId === miriamTypes[i].getName()) {
        miriamType = miriamTypes[i];
      }
    }
    if (selected) {
      requiredAnnotationsData.list.push(miriamType.getName());
    } else {
      var index = requiredAnnotationsData.list.indexOf(miriamType.getName());
      if (index > -1) {
        requiredAnnotationsData.list.splice(index, 1);
      }
    }

    var data = new UserPreferences();

    var elementRequiredAnnotations = {};
    elementRequiredAnnotations[elementType.className] = {
      "require-at-least-one": requiredAnnotationsData.requiredAtLeastOnce,
      "annotation-list": requiredAnnotationsData.list
    };
    data.setElementRequiredAnnotations(elementRequiredAnnotations);
    return ServerConnector.updateUserPreferences({user: user, preferences: data}).then(null, GuiConnector.alert);
  };

  checkboxList.addListener("select", function (element) {
    return changeSelection(element.value, true);
  });
  checkboxList.addListener("deselect", function (element) {
    return changeSelection(element.value, false);
  });

};

ChooseValidatorsDialog.prototype.init = function () {
  var self = this;
  return ServerConnector.getConfiguration().then(function (configuration) {

    var treeData = configuration.getElementTypeTree();

    var element = $('[name="elementTree"]', self.getElement());
    element.jstree({
      core: {
        data: treeData
      }
    }).on('ready.jstree', function () {
      element.jstree("open_all");
    }).on("select_node.jstree",
      function (evt, data) {
        return self.setElementType(data.node.data);
      }
    );
  });
};

ChooseValidatorsDialog.prototype.destroy = function () {
  $(this.getElement()).dialog("destroy");
};

ChooseValidatorsDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      title: "Select valid annotations",
      modal: true,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2

    });
  }

  $(div).dialog("open");
};

module.exports = ChooseValidatorsDialog;
