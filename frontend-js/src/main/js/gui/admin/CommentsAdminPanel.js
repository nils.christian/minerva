"use strict";

var AbstractGuiElement = require('../AbstractGuiElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");
var xss = require('xss');

function CommentsAdminPanel(params) {
  params.customMap = null;
  AbstractGuiElement.call(this, params);

  this._createGui();
}

CommentsAdminPanel.prototype = Object.create(AbstractGuiElement.prototype);
CommentsAdminPanel.prototype.constructor = CommentsAdminPanel;

CommentsAdminPanel.prototype._createGui = function () {
  var self = this;

  var commentsTable = Functions.createElement({
    type: "table",
    name: "commentsTable",
    className: "display",
    style: "width:100%"
  });

  self.getElement().appendChild(commentsTable);

  $(commentsTable).DataTable({
    columns: [{
      title: 'Id'
    }, {
      title: 'Title'
    }, {
      title: 'Author'
    }, {
      title: 'Email'
    }, {
      title: 'Content'
    }, {
      title: 'Removed'
    }, {
      title: 'Pinned'
    }]
  });

};

CommentsAdminPanel.prototype.init = function () {
  var self = this;
  return self.refreshProjects();
};

CommentsAdminPanel.prototype.refreshComments = function () {
  var self = this;

  return ServerConnector.getComments({
    projectId: self.getProject().getProjectId()
  }).then(function (comments) {
    var dataTable = $($("[name='commentsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < comments.length; i++) {
      data.push(self.commentToTableRow(comments[i]));
    }
    dataTable.clear().rows.add(data).draw();
    $("[name='commentsTable']", self.getElement()).on("click", "[name='removeComment']", function () {
      var button = this;
      return self.askConfirmRemoval({
        title: "Why do you want to remove this comment?",
        input: true
      }).then(function (param) {
        if (param.status) {
          return ServerConnector.removeComment({
            commentId: $(button).attr("data"),
            reason: param.reason,
            projectId: self.getProject().getProjectId()
          }).then(function () {
            $(button).after("<span>YES (" + param.reason + ")</span>");
            button.style.display = "none";
          });
        }
      }).catch(GuiConnector.alert)
    });
  });
};

CommentsAdminPanel.prototype.commentToTableRow = function (comment) {
  var self = this;
  var projectId = self.getProject().getProjectId();
  var toYesNo = function (val) {
    if (val) {
      return "YES";
    } else {
      return "NO";
    }
  };
  var title = null;
  if (!comment.isRemoved()) {
    var commentLink = "index.xhtml?id=" + projectId + // 
      "&x=" + comment.getCoordinates().x + //
      "&y=" + comment.getCoordinates().y + //
      "&zoom=12" + //
      "&comments=on";
    title = "<a href='" + commentLink + "' target='" + projectId + "'>" + comment.getTitle() + "</a>";
  } else {
    title = comment.getTitle();
  }

  var remove = null;
  if (comment.isRemoved()) {
    remove = "YES (" + comment.getRemoveReason() + ")";
  } else {
    remove = "<button name='removeComment' data='" + comment.getId() + "'><i class='fa fa-trash-o' style='font-size:17px'></button>";
  }

  var author = comment.getAuthor();
  if (author === undefined) {
    author = "N/A";
  }

  var email = comment.getEmail();
  if (email === undefined) {
    email = "N/A";
  }

  return [comment.getId(), //
    xss(title), //
    xss(author), //
    xss(email), //
    xss(comment.getContent()), //
    remove, //
    toYesNo(comment.isPinned())];
};

CommentsAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='commentsTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    $(table).DataTable().destroy();
  }
};


module.exports = CommentsAdminPanel;
