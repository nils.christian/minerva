"use strict";

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require("../../GuiConnector");
var MultiCheckboxList = require("multi-checkbox-list");
var UserPreferences = require("../../map/data/UserPreferences");

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

function ChooseAnnotatorsDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createGui();
}

ChooseAnnotatorsDialog.prototype = Object.create(AbstractGuiElement.prototype);
ChooseAnnotatorsDialog.prototype.constructor = ChooseAnnotatorsDialog;

ChooseAnnotatorsDialog.prototype.createGui = function () {
  var self = this;
  var content = Functions.createElement({
    type: "div",
    style: "display:table;height:100%;width:100%"
  });
  content.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell;height:100%;",
    content: "<div name='elementTree' style='height:100%;overflow-y:scroll;'/>",
    xss: false
  }));

  var annotatorsDiv = Functions.createElement({
    type: "div",
    style: "display:table-cell;width:100%",
    content: "<div style='height:100%;width:100%;overflow-y:auto;'><div name='annotatorListBox'></div><div class='minerva-annotators-params'></div></div>",
    xss: false
  });
  content.appendChild(annotatorsDiv);

  self.getElement().appendChild(content);
};

function onChangeParameterValue(element, user) {
  var name = $(element).siblings(".minerva-annotator-param-name")[0].childNodes[0].nodeValue;
  var annotatorClassName = $(element).parent().parent().attr('name');

  var data = new UserPreferences();

  var annotatorsParams = {};
  annotatorsParams[annotatorClassName] = {};
  if (element.type == 'checkbox') annotatorsParams[annotatorClassName][name] = element.checked.toString();
  else annotatorsParams[annotatorClassName][name] = element.value;
  data.setAnnotatorsParameters(annotatorsParams);

  return ServerConnector.updateUserPreferences({user: user, preferences: data}).then(null, GuiConnector.alert);
}

ChooseAnnotatorsDialog.prototype.saveAnnotatorsInfo = function (elementTypes, selectedAnnotators) {
  selectedAnnotators = selectedAnnotators.slice();
  return ServerConnector.getLoggedUser().then(function (user) {

    var data = new UserPreferences();

    var elementAnnotators = {};
    for (var i = 0; i < elementTypes.length; i++) {
      elementAnnotators[elementTypes[i].className] = selectedAnnotators;

      var userAnnotators = user.getPreferences().getElementAnnotators(elementTypes[i].className);
      userAnnotators.length = 0;
      userAnnotators.push.apply(userAnnotators, selectedAnnotators);
    }
    data.setElementAnnotators(elementAnnotators);
    return ServerConnector.updateUserPreferences({
      user: user,
      preferences: data
    });
  }).catch(GuiConnector.alert);
};

ChooseAnnotatorsDialog.prototype.getAllChildrenTypesIfNeeded = function (elementType, includeChildren) {
  var result = [elementType];
  if (includeChildren) {
    var queue = [elementType];
    var elementTypes = this.getConfiguration().getElementTypes();
    while (queue.length > 0) {
      var type = queue.shift();
      for (var i = 0; i < elementTypes.length; i++) {
        if (type.className === elementTypes[i].parentClass) {
          queue.push(elementTypes[i]);
          result.push(elementTypes[i]);
        }
      }
    }
  }
  return result;
};


ChooseAnnotatorsDialog.prototype.setElementType = function (elementType) {
  var self = this;

  var configuration = self.getConfiguration();

  return ServerConnector.getLoggedUser().then(function (user) {
    var element = $("[name='annotatorListBox']", self.getElement())[0];
    var annotatorsParams = $(".minerva-annotators-params", self.getElement())[0];
    Functions.removeChildren(element);

    var selectElement = Functions.createElement({
      type: "div",
      style: "width:50%; height:200px;float:left; OVERFLOW-Y:scroll"
    });

    element.appendChild(selectElement);
    var includeChildrenCheckbox = Functions.createElement({type: "input", inputType: "checkbox"});
    element.appendChild(includeChildrenCheckbox);
    element.appendChild(Functions.createElement({type: "span", content: "Apply to all in subtree"}));
    element.appendChild(Functions.createElement({type: "br"}));
    var copyFromButton = Functions.createElement({
      type: "button", content: "Copy from", onclick: function () {
        var typeClassName = copyFromSelect.value;
        var annotators;
        for (var i = 0; i < configuration.getElementTypes().length; i++) {
          var type = configuration.getElementTypes()[i];
          if (typeClassName === type.className) {
            annotators = user.getPreferences().getElementAnnotators(typeClassName);
          }
        }
        if (annotators === undefined) {
          return GuiConnector.alert("Invalid element type: " + copyFromSelect.value);
        } else {
          var includeChildren = includeChildrenCheckbox.checked;
          return self.saveAnnotatorsInfo(self.getAllChildrenTypesIfNeeded(elementType, includeChildren), annotators).then(function () {
            return self.setElementType(elementType);
          });
        }
      }
    });
    element.appendChild(copyFromButton);
    var copyFromSelect = Functions.createElement({type: "select", style: "margin:5px"});
    element.appendChild(copyFromSelect);
    var options = [], i;
    for (i = 0; i < configuration.getElementTypes().length; i++) {
      var type = configuration.getElementTypes()[i];
      var name = type.className;
      if (name.indexOf(".") > 0) {
        name = name.substr(name.lastIndexOf(".") + 1);
      }
      options.push(Functions.createElement({
        type: "option",
        value: type.className,
        content: name
      }));
    }
    options.sort(function (a, b) {
      return a.text === b.text ? 0 : a.text < b.text ? -1 : 1
    });
    for (i = 0; i < options.length; i++) {
      copyFromSelect.appendChild(options[i]);
    }

    var annotators = configuration.getElementAnnotators(elementType);

    var selectedAnnotators = user.getPreferences().getElementAnnotators(elementType.className);

    var entries = [];

    for (i = 0; i < annotators.length; i++) {
      var annotator = annotators[i];
      var entry = {name: annotator.getName(), value: annotator.getClassName(), selected: false};
      for (var j = 0; j < selectedAnnotators.length; j++) {
        if (annotator.getName() === selectedAnnotators[j]) {
          entry.selected = true;
        }
      }
      entries.push(entry);
    }
    var checkboxList = new MultiCheckboxList(selectElement, {
      entries: entries,
      listTitle: "Available annotators",
      selectedTitle: "Selected annotators",
      selectedList: true
    });
    var changeSelection = function (elementId, selected) {

      var annotators = configuration.getElementAnnotators();
      var annotator;
      for (var i = 0; i < annotators.length; i++) {
        if (elementId === annotators[i].getClassName()) {
          annotator = annotators[i];
        }
      }
      if (selected) {
        selectedAnnotators.push(annotator.getName());
      } else {
        var index = selectedAnnotators.indexOf(annotator.getName());
        if (index > -1) {
          selectedAnnotators.splice(index, 1);
        }
      }
      createAnnotatorsParams();
      var includeChildren = includeChildrenCheckbox.checked;
      return self.saveAnnotatorsInfo(self.getAllChildrenTypesIfNeeded(elementType, includeChildren), selectedAnnotators);
    };
    includeChildrenCheckbox.onchange = function () {
      var includeChildren = includeChildrenCheckbox.checked;
      return self.saveAnnotatorsInfo(self.getAllChildrenTypesIfNeeded(elementType, includeChildren), selectedAnnotators);
    };

    checkboxList.addListener("select", function (element) {
      return changeSelection(element.value, true);
    });
    checkboxList.addListener("deselect", function (element) {
      return changeSelection(element.value, false);
    });


    function createAnnotatorsParams() {

      var existingAnnotatorsParameters = user.getPreferences().getAnnotatorsParameters();

      Functions.removeChildren(annotatorsParams);
      for (var i = 0; i < annotators.length; i++) {
        var annotator = annotators[i];
        for (var j = 0; j < selectedAnnotators.length; j++) {
          if (annotator.getName() === selectedAnnotators[j]) {
            var paramsDefs = annotator.getParametersDefinitions();
            if (paramsDefs.length > 0) {

              annotatorsParams.appendChild(Functions.createElement({
                type: "div",
                className: "minerva-annotators-params-header",
                content: '<div>Available parameters</div>'
              }));

              var annotatorParams = Functions.createElement({
                type: "div",
                className: "minerva-annotator-params",
                name: annotator.getClassName()
              });

              annotatorParams.appendChild(Functions.createElement({
                type: "div",
                className: "minerva-annotator-params-header",
                content: annotator.getName()
              }));

              for (var k = 0; k < paramsDefs.length; k++) {
                var param = paramsDefs[k];
                var paramElement = Functions.createElement({
                  type: "div",
                  className: "minerva-annotator-param"
                });

                var paramName = Functions.createElement({
                  type: "div",
                  className: "minerva-annotator-param-name",
                  content: param.name
                });

                var tooltipContainer = Functions.createElement({
                  type: "span"
                });
                tooltipContainer.appendChild(Functions.createElement({
                  type: "span",
                  className: "glyphicon glyphicon-question-sign tooltip-icon"
                }));
                tooltipContainer.appendChild(Functions.createElement({
                  type: "span",
                  className: "annotator-tooltip",
                  content: param.description
                }));

                paramName.appendChild(tooltipContainer);
                paramElement.appendChild(paramName);

                var paramValue;

                var existingParamValue;
                if (existingAnnotatorsParameters[annotator.getClassName()]) {
                  existingParamValue = existingAnnotatorsParameters[annotator.getClassName()][param.name]
                }

                if (param.type.indexOf("String") >= 0) {
                  paramValue = Functions.createElement({
                    type: "textarea",
                    onchange: function () {
                      return onChangeParameterValue(this, user);
                    }
                  });
                  if (existingParamValue) paramValue.value = existingParamValue;
                } else if (param.type.indexOf("Integer") >= 0) {
                  paramValue = Functions.createElement({
                    type: "input",
                    inputType: "number",
                    onchange: function () {
                      return onChangeParameterValue(this, user);
                    }
                  });
                  if (existingParamValue) paramValue.value = existingParamValue;
                } else if (param.type.indexOf("Boolean") >= 0) {
                  paramValue = Functions.createElement({
                    type: "input",
                    inputType: "checkbox",
                    onchange: function () {
                      return onChangeParameterValue(this, user);
                    }
                  });
                  paramValue.checked = (existingParamValue && existingParamValue === 'true');
                } else {
                  throw new Error("Unknown annotator parameter type");
                }

                paramElement.appendChild(paramValue);
                annotatorParams.appendChild(paramElement);
              }

              annotatorsParams.appendChild(annotatorParams);
            }
          }
        }
      }
    }

    createAnnotatorsParams();

  });

};

ChooseAnnotatorsDialog.prototype.init = function () {
  var self = this;
  return ServerConnector.getConfiguration().then(function (configuration) {

    var treeData = configuration.getElementTypeTree();

    var element = $('[name="elementTree"]', self.getElement());
    element.jstree({
      core: {
        data: treeData
      }
    }).on('ready.jstree', function () {
      element.jstree("open_all");
    }).on("select_node.jstree",
      function (evt, data) {
        return self.setElementType(data.node.data);
      }
    );
  });
};

ChooseAnnotatorsDialog.prototype.destroy = function () {
  $(this.getElement()).dialog("destroy");
};

ChooseAnnotatorsDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      title: "Select annotators",
      modal: true,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2

    });
  }
  $(div).dialog("open");
  $(div).css("overflow", "hidden");
};

module.exports = ChooseAnnotatorsDialog;
