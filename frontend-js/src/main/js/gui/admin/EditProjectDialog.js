"use strict";

/* exported logger */
var Promise = require("bluebird");

var AbstractGuiElement = require('../AbstractGuiElement');
var AddOverlayDialog = require('../AddOverlayDialog');
var Annotation = require('../../map/data/Annotation');
var CommentsTab = require('./CommentsAdminPanel');
var GuiConnector = require('../../GuiConnector');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ValidationError = require("../../ValidationError");

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();
var xss = require('xss');

function EditProjectDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.setConfiguration(params.configuration);
  self.registerListenerType("onSave");
  $(self.getElement()).addClass("minerva-edit-project-dialog");
  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
}

EditProjectDialog.prototype = Object.create(AbstractGuiElement.prototype);
EditProjectDialog.prototype.constructor = EditProjectDialog;

EditProjectDialog.prototype.createGui = function () {
  var self = this;
  var element = self.getElement();

  var tabDiv = Functions.createElement({
    type: "div",
    name: "tabView",
    className: "tabbable boxed parentTabs",
    style: "position:absolute;top:10px;bottom:40px;left:10px;right:10px"
  });
  element.appendChild(tabDiv);

  var tabMenuDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs"
  });
  tabDiv.appendChild(tabMenuDiv);

  var tabContentDiv = Functions.createElement({
    type: "div",
    className: "tab-content",
    style: "height:100%"
  });
  tabDiv.appendChild(tabContentDiv);

  self.createGeneralTab(tabMenuDiv, tabContentDiv);
  self.createOverlaysTab(tabMenuDiv, tabContentDiv);
  self.createMapsTab(tabMenuDiv, tabContentDiv);
  self.createUsersTab(tabMenuDiv, tabContentDiv);
  self.createCommentsTab(tabMenuDiv, tabContentDiv);
  $("a", tabMenuDiv).bind("click", function () {
    //workaround for some css issues...
    tabDiv.style.top = "40px";
    tabDiv.style.bottom = "10px";
  });
};

EditProjectDialog.prototype.createGeneralTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "GENERAL",
    id: self.getProject().getProjectId() + "_general_tab",
    content: self.createGeneralTabContent()
  });
};

EditProjectDialog.prototype.createCommentsTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  var element = Functions.createElement({type: "div", style: "margin-top:10px;"});


  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "COMMENTS",
    id: self.getProject().getProjectId() + "_comment_tab",
    content: element
  });

  var commentsTab = new CommentsTab({
    element: element,
    project: self.getProject(),
    configuration: self.getConfiguration()
  });
  self.setCommentsTab(commentsTab);
};

EditProjectDialog.prototype.addTab = function (params) {
  var navLi = guiUtils.createTabMenuObject({
    id: params.id,
    name: params.name,
    navigationBar: params.tabMenuDiv
  });
  params.tabMenuDiv.appendChild(navLi);

  var contentDiv = guiUtils.createTabContentObject({
    id: params.id,
    navigationObject: navLi,
    navigationBar: params.tabMenuDiv
  });
  $(contentDiv).css("overflow","auto");

  if (params.content !== undefined) {
    contentDiv.appendChild(params.content);
  }

  params.tabContentDiv.appendChild(contentDiv);
  if (params.disabled) {
    this.disableTab(params);
  }
};

EditProjectDialog.prototype.disableTab = function (params) {
  $("a[href='#" + params.id + "']", this.getElement()).hide();
};

EditProjectDialog.prototype.enableTab = function (params) {
  $("a[href='#" + params.id + "']", this.getElement()).show();
};

EditProjectDialog.prototype.createGeneralTabContent = function () {
  var self = this;
  var project = self.getProject();

  var result = Functions.createElement({
    type: "div"
  });

  var table = Functions.createElement({
    type: "div",
    style: "display:table"
  });
  result.appendChild(table);

  var projectIdRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(projectIdRow);
  projectIdRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "ProjectId"
  }));
  projectIdRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: project.getProjectId()
  }));

  var mapCanvasTypeRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(mapCanvasTypeRow);
  mapCanvasTypeRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Map canvas type: "
  }));
  mapCanvasTypeRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: Functions.createElement({
      type: "select",
      name: "project-map-canvas-type"
    })
  }));

  var googleLicenseRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(googleLicenseRow);
  googleLicenseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "I agree to <a target='_license' href='https://developers.google.com/maps/terms'>Google Maps APIs Terms of Service</a>: ",
    xss: false
  }));
  googleLicenseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: Functions.createElement({
      type: "input",
      inputType: "checkbox",
      name: "project-google-maps-license"
    })
  }));

  var nameRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(nameRow);
  nameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Name"
  }));
  nameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectName' value='" + xss(project.getName()) + "'/>",
    xss: false
  }));

  var versionRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(versionRow);
  versionRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Version"
  }));
  versionRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectVersion' value='" + xss(project.getVersion()) + "'/>",
    xss: false
  }));

  var diseaseRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(diseaseRow);
  diseaseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Disease"
  }));
  var disease = "";
  if (project.getDisease() !== undefined) {
    disease = xss(project.getDisease().getResource());
  }
  diseaseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectDisease' value='" + disease + "'/>",
    xss: false
  }));

  var organismRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(organismRow);
  organismRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Organism"
  }));
  var organism = "";
  if (project.getOrganism() !== undefined) {
    organism = xss(project.getOrganism().getResource());
  }
  organismRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectOrganism' value='" + organism + "'/>",
    xss: false
  }));

  var emailRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(emailRow);
  emailRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Notify email"
  }));
  var email = "";
  if (project.getNotifyEmail() !== undefined) {
    email = xss(project.getNotifyEmail());
  }
  emailRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectNotifyEmail' value='" + email + "'/>",
    xss: false
  }));

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var saveProjectButton = Functions.createElement({
    type: "button",
    name: "saveProject",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.callListeners("onSave");
      }).then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelProject",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveProjectButton);
  menuRow.appendChild(cancelButton);

  return result;

};

EditProjectDialog.prototype.createMapsTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "MAPS",
    id: self.getProject().getProjectId() + "_maps_tab",
    content: self.createMapsTabContent()
  });
};

EditProjectDialog.prototype.createMapsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div"
  });
  result.appendChild(self._createMapsTable());
  return result;
};

EditProjectDialog.prototype._createMapsTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var mapsTable = Functions.createElement({
    type: "table",
    name: "mapsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(mapsTable);

  $(mapsTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "map-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Default center x'
    }, {
      title: 'Default center y'
    }, {
      title: 'Default zoom level'
    }, {
      title: 'Update'
    }]
  });

  $(mapsTable).on("click", "[name='saveMap']", function () {
    var button = this;
    GuiConnector.showProcessing("Updating");
    return self.saveMap($(button).attr("data")).then(function () {
      GuiConnector.hideProcessing();
      GuiConnector.info("Map updated successfully");
    }, function (error) {
      GuiConnector.hideProcessing();
      GuiConnector.alert(error);
    });
  });

  return result;
};


EditProjectDialog.prototype.createOverlaysTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "OVERLAYS",
    id: self.getProject().getProjectId() + "_overlays_tab",
    content: self.createOverlaysTabContent()
  });
};

EditProjectDialog.prototype.createOverlaysTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div"
  });
  result.appendChild(self._createOverlayTable());
  return result;
};

EditProjectDialog.prototype._createOverlayTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var overlaysTable = Functions.createElement({
    type: "table",
    name: "overlaysTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(overlaysTable);

  $(overlaysTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "overlay-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Description'
    }, {
      title: 'Public'
    }, {
      title: 'Default'
    }, {
      title: 'Owner'
    }, {
      title: 'Data'
    }, {
      title: 'Update'
    }, {
      title: 'Remove'
    }],
    dom: '<"minerva-datatable-toolbar">frtip',
    initComplete: function () {
      $("div.minerva-datatable-toolbar", $(result)).html('<button name="addOverlay">Add overlay</button>');
    }

  });

  $(overlaysTable).on("click", "[name='removeOverlay']", function () {
    var button = this;
    return self.removeOverlay($(button).attr("data")).then(null, GuiConnector.alert);
  });

  $(overlaysTable).on("click", "[name='saveOverlay']", function () {
    var button = this;
    GuiConnector.showProcessing("Updating");
    return self.saveOverlay($(button).attr("data")).then(function () {
      GuiConnector.hideProcessing();
      GuiConnector.info("Overlay updated successfully");
    }, function (error) {
      GuiConnector.hideProcessing();
      GuiConnector.alert(error);
    });
  });

  $(overlaysTable).on("click", "[name='downloadSource']", function () {
    var button = this;
    return ServerConnector.getOverlaySourceDownloadUrl({
      overlayId: $(button).attr("data")
    }).then(function (url) {
      return self.downloadFile(url);
    }).then(null, GuiConnector.alert);
  });

  $(result).on("click", "[name='addOverlay']", function () {
    return self.openAddOverlayDialog();
  });

  return result;
};

EditProjectDialog.prototype.createUsersTab = function (tabMenuDiv, tabContentDiv) {
  var self = this;
  self.addTab({
    tabMenuDiv: tabMenuDiv,
    tabContentDiv: tabContentDiv,
    name: "USERS",
    id: self.getProject().getProjectId() + "_users_tab",
    content: self.createUsersTabContent()
  });
};

EditProjectDialog.prototype.createUsersTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var usersTable = Functions.createElement({
    type: "table",
    name: "usersTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(usersTable);

  $(usersTable).on("click", "[name='saveUser']", function () {
    var button = this;
    GuiConnector.showProcessing("Updating");
    return self.saveUser($(button).attr("data")).then(function () {
      GuiConnector.hideProcessing();
      GuiConnector.info("User updated successfully");
    }, function (error) {
      GuiConnector.hideProcessing();
      GuiConnector.alert(error);
    });
  });

  return result;
};

EditProjectDialog.prototype.createUserPrivilegeColumns = function () {
  var self = this;

  if (self._userPrivilegeColumns !== undefined) {
    return Promise.resolve(self._userPrivilegeColumns);
  }

  return ServerConnector.getConfiguration().then(function (configuration) {
    self._userPrivilegeColumns = [{
      title: "Name"
    }];
    var privilegeTypes = configuration.getPrivilegeTypes();
    for (var i = 0; i < privilegeTypes.length; i++) {
      var type = privilegeTypes[i];
      if (type.getObjectType() === "Project") {
        self._userPrivilegeColumns.push({
          "title": type.getCommonName(),
          privilegeType: type
        });
      }
    }
    self._userPrivilegeColumns.push({
      "title": "Update"
    });
    return self._userPrivilegeColumns;
  });

};

EditProjectDialog.prototype.init = function () {
  var self = this;
  return self.initUsersTab().then(function () {
    return self.refreshUsers();
  }).then(function () {
    return self.refreshMaps();
  }).then(function () {
    return self.refreshOverlays();
  }).then(function () {
    return self.refreshComments();
  }).then(function () {
    var configuration = self.getConfiguration();
    var mapCanvasTypes = configuration.getMapCanvasTypes();
    var select = $("[name='project-map-canvas-type']", self.getElement())[0];
    var licenceCheckbox = $("[name='project-google-maps-license']", self.getElement())[0];
    var licenceDiv = licenceCheckbox.parentElement.parentElement;

    for (var i = 0; i < mapCanvasTypes.length; i++) {
      var mapCanvasType = mapCanvasTypes[i];
      var option = Functions.createElement({
        type: "option",
        content: mapCanvasType.name,
        value: mapCanvasType.id
      });
      if (mapCanvasType.id === self.getProject().getMapCanvasType()) {
        option.selected = true;
        if (mapCanvasType.id === "GOOGLE_MAPS_API") {
          $(licenceDiv).css("display", "table-row");
          self.setLicenseAccepted(true);
        } else {
          $(licenceDiv).css("display", "none");
          self.setLicenseAccepted(false);
        }
      }
      select.appendChild(option);
    }
    $(select).change(function () {
      $("option:selected", select).each(function () {
        if ($(this).val() === "GOOGLE_MAPS_API") {
          $(licenceDiv).css("display", "table-row");
        } else {
          $(licenceDiv).css("display", "none");
        }
        self.setLicenseAccepted(false);
      });
    });

    $(window).trigger('resize');
  });
};

EditProjectDialog.prototype.initUsersTab = function () {
  var self = this;

  var usersTable = $("[name=usersTable]", self.getElement())[0];

  return self.createUserPrivilegeColumns().then(function (columns) {
    $(usersTable).DataTable({
      columns: columns
    });
  });
};

EditProjectDialog.prototype.refreshOverlays = function () {
  var self = this;
  return ServerConnector.getLoggedUser().then(function (user) {
    var privilege = self.getConfiguration().getPrivilegeType(PrivilegeType.USER_MANAGEMENT);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(privilege)) {
      return ServerConnector.getOverlays({
        projectId: self.getProject().getProjectId()
      }).then(function (overlays) {
        return self.setOverlays(overlays);
      });
    } else {
      self.disableTab({id: self.getProject().getProjectId() + "_overlays_tab"});
      return Promise.resolve();
    }
  });
};

EditProjectDialog.prototype.refreshMaps = function () {
  var self = this;
  return ServerConnector.getModels(self.getProject().getProjectId()).then(function (maps) {
    return self.setMaps(maps);
  });
};

EditProjectDialog.prototype.refreshUsers = function () {
  var self = this;
  return ServerConnector.getLoggedUser().then(function (user) {
    var privilege = self.getConfiguration().getPrivilegeType(PrivilegeType.USER_MANAGEMENT);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(privilege)) {
      return ServerConnector.getUsers(true).then(function (users) {
        return self.setUsers(users);
      });
    } else {
      self.disableTab({id: self.getProject().getProjectId() + "_users_tab"});
      return Promise.resolve();
    }
  });
};

EditProjectDialog.prototype.setOverlays = function (overlays) {
  var self = this;
  self._overlayById = [];
  return ServerConnector.getUsers().then(function (users) {
    var dataTable = $($("[name='overlaysTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < overlays.length; i++) {
      var overlay = overlays[i];
      self._overlayById[overlay.getId()] = overlay;
      var rowData = self.overlayToTableRow(overlay, users);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

EditProjectDialog.prototype.setMaps = function (maps) {
  var self = this;
  self._mapsById = [];
  var dataTable = $($("[name='mapsTable']", self.getElement())[0]).DataTable();
  var data = [];
  for (var i = 0; i < maps.length; i++) {
    var map = maps[i];
    self._mapsById[map.getId()] = map;
    var rowData = self.mapToTableRow(map);
    data.push(rowData);
  }
  dataTable.clear().rows.add(data).draw();
};

EditProjectDialog.prototype.setUsers = function (users) {
  var self = this;
  self._userByLogin = [];
  return self.createUserPrivilegeColumns().then(function (columns) {
    var dataTable = $($("[name='usersTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < users.length; i++) {
      var user = users[i];
      self._userByLogin[user.getLogin()] = user;
      var rowData = self.userToTableRow(user, columns);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

EditProjectDialog.prototype.userToTableRow = function (user, columns) {
  var self = this;
  var row = [];
  var login = user.getLogin();

  row[0] = user.getName() + " " + user.getSurname() + " (" + login + ")";
  for (var i = 1; i < columns.length; i++) {
    var column = columns[i];
    if (column.privilegeType !== undefined) {
      if (column.privilegeType.getValueType() === "boolean") {
        var checked = '';
        if (user.hasPrivilege(column.privilegeType, self.getProject().getId())) {
          checked = 'checked';
        }
        row[i] = "<input type='checkbox' name='privilege-" + login + "' data='" + column.privilegeType.getName() + "' "
          + checked + "/>";
      } else {
        throw new Error("Unsupported type: " + column.privilegeType.getValueType());
      }
    }
  }

  row.push("<button name='saveUser' data='" + login + "'><i class=\"fa fa-save\" style=\"font-size:17px\"></i></button>");

  return row;
};

EditProjectDialog.prototype.overlayToTableRow = function (overlay, users) {
  var row = [];
  var id = overlay.getId();
  var creatorSelect;
  if (overlay.getCreator() === "") {
    creatorSelect = "<select name='creator-" + id + "' value=''>";
  } else {
    creatorSelect = "<select name='creator-" + id + "'>";
  }

  creatorSelect += "<option value='' >---</option>";
  for (var i = 0; i < users.length; i++) {
    var selected = "";
    var user = users[i];
    if (overlay.getCreator() === user.getLogin()) {
      selected = "selected";
    } else {
      selected = "";
    }

    creatorSelect += "<option value='" + user.getLogin() + "' " + selected + ">" + user.getLogin() + "("
      + user.getName() + " " + user.getSurname() + ")</option>";
  }
  creatorSelect += "</select>";

  var checked = '';
  if (overlay.getPublicOverlay()) {
    checked = "checked";
  }
  var publicOverlayCheckbox = "<input type='checkbox' name='publicOverlay-" + id + "' " + checked + "/>";

  checked = '';
  if (overlay.isDefaultOverlay()) {
    checked = "checked";
  }
  var defaultOverlayCheckbox = "<input type='checkbox' name='defaultOverlay-" + id + "' " + checked + "/>";

  var downloadSourceButton;
  if (overlay.getInputDataAvailable()) {
    downloadSourceButton = "<button name='downloadSource' data='" + id + "'>"
      + "<span class='ui-icon ui-icon-arrowthickstop-1-s'></span>" + "</button>";
  } else {
    downloadSourceButton = "N/A";
  }

  row[0] = id;
  row[1] = "<input name='name-" + id + "' value='" + overlay.getName() + "'/>";
  row[2] = "<input name='description-" + id + "' value='" + overlay.getDescription() + "'/>";
  row[3] = publicOverlayCheckbox;
  row[4] = defaultOverlayCheckbox;
  row[5] = creatorSelect;
  row[6] = downloadSourceButton;
  row[7] = "<button name='saveOverlay' data='" + id + "'><i class=\"fa fa-save\" style=\"font-size:17px\"></i></button>";
  row[8] = "<button name='removeOverlay' data='" + id + "'><i class='fa fa-trash-o' style='font-size:17px'></button>";

  return row;
};

function getValueOrEmpty(value) {
  if (value !== undefined && value !== null) {
    return value;
  } else {
    return "";
  }
}

EditProjectDialog.prototype.mapToTableRow = function (map, users) {
  var row = [];
  var id = map.getId();
  var centerX = getValueOrEmpty(map.getDefaultCenterX());
  var centerY = getValueOrEmpty(map.getDefaultCenterY());
  var zoomLevel = getValueOrEmpty(map.getDefaultZoomLevel());
  row[0] = id;
  row[1] = map.getName();
  row[2] = "<input name='defaultCenterX-" + id + "' value='" + centerX + "'/>";
  row[3] = "<input name='defaultCenterY-" + id + "' value='" + centerY + "'/>";
  row[4] = "<input name='defaultZoomLevel-" + id + "' value='" + zoomLevel + "'/>";
  row[5] = "<button name='saveMap' data='" + id + "'><i class=\"fa fa-save\" style=\"font-size:17px\"></i></button>";

  return row;
};

EditProjectDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      title: self.getProject().getProjectId(),
      width: window.innerWidth / 2,
      height: window.innerHeight / 2
    });
  }
  $(div).dialog("open");
};

var prepareMiriamData = function (type, resource) {
  if (resource === "" || resource === undefined || resource === null) {
    return null;
  } else {
    return new Annotation({
      type: type,
      resource: resource
    });
  }
};

/**
 *
 * @returns {PromiseLike}
 */
EditProjectDialog.prototype.onSaveClicked = function () {
  var self = this;
  var project = self.getProject();
  var element = self.getElement();
  return self.checkValidity().then(function () {
    project.setName($("[name='projectName']", element)[0].value);
    project.setVersion($("[name='projectVersion']", element)[0].value);
    project.setNotifyEmail($("[name='projectNotifyEmail']", element)[0].value);
    var organism = prepareMiriamData("TAXONOMY", $("[name='projectOrganism']", element)[0].value);
    project.setOrganism(organism);
    var disease = prepareMiriamData("MESH_2012", $("[name='projectDisease']", element)[0].value);
    project.setDisease(disease);
    project.setMapCanvasType(self.getMapCanvasTypeId());
    return ServerConnector.updateProject(project);
  });
};

EditProjectDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>Some data is missing.</b><ul>";

  var mapCanvasTypeId = self.getMapCanvasTypeId();
  if (mapCanvasTypeId === "GOOGLE_MAPS_API") {
    if (!self.isLicenseAccepted()) {
      isValid = false;
      error += "<li>Please accept Google Maps License when choosing Google Maps API engine</li>";
    }
  }

  if (isValid) {
    return Promise.resolve(true);
  } else {
    return Promise.reject(new ValidationError(error));
  }
};
EditProjectDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

EditProjectDialog.prototype.saveOverlay = function (overlayId) {
  var self = this;
  var overlay = self._overlayById[overlayId];
  overlay.setName($("[name='name-" + overlayId + "']", self.getElement())[0].value);
  overlay.setDescription($("[name='description-" + overlayId + "']", self.getElement())[0].value);
  overlay.setPublicOverlay($("[name='publicOverlay-" + overlayId + "']", self.getElement())[0].checked);
  overlay.setDefaultOverlay($("[name='defaultOverlay-" + overlayId + "']", self.getElement())[0].checked);
  overlay.setCreator($("[name='creator-" + overlayId + "']", self.getElement())[0].value);

  return ServerConnector.updateOverlay(overlay);
};

EditProjectDialog.prototype.saveMap = function (mapId) {
  var self = this;
  var map = self._mapsById[mapId];
  map.setDefaultCenterX($("[name='defaultCenterX-" + mapId + "']", self.getElement())[0].value);
  map.setDefaultCenterY($("[name='defaultCenterY-" + mapId + "']", self.getElement())[0].value);
  map.setDefaultZoomLevel($("[name='defaultZoomLevel-" + mapId + "']", self.getElement())[0].value);

  return ServerConnector.updateModel({projectId: self.getProject().getProjectId(), model: map});
};

EditProjectDialog.prototype.saveUser = function (login) {
  var self = this;
  var checkboxes = $("[name='privilege-" + login + "']", self.getElement());
  var privileges = {};
  for (var i = 0; i < checkboxes.length; i++) {
    var checkbox = checkboxes[i];
    var privilege = {};
    privilege[self.getProject().getId()] = checkbox.checked;
    privileges[$(checkbox).attr("data")] = privilege;
  }

  return ServerConnector.updateUserPrivileges({
    user: self._userByLogin[login],
    privileges: privileges
  });
};

EditProjectDialog.prototype.removeOverlay = function (overlayId) {
  var self = this;
  return ServerConnector.removeOverlay({
    overlayId: overlayId
  }).then(function () {
    return self.refreshOverlays();
  });
};

EditProjectDialog.prototype.openAddOverlayDialog = function () {
  var self = this;
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
  }
  self._addOverlayDialog = new AddOverlayDialog({
    project: self.getProject(),
    customMap: null,
    element: document.createElement("div"),
    configuration: self.getConfiguration()
  });
  self._addOverlayDialog.addListener("onAddOverlay", function () {
    return self.refreshOverlays();
  });
  return self._addOverlayDialog.init().then(function () {
    return self._addOverlayDialog.open();
  });
};

EditProjectDialog.prototype.setCommentsTab = function (commentsTab) {
  this._commentsTab = commentsTab;
};

EditProjectDialog.prototype.refreshComments = function (commentsTab) {
  return this._commentsTab.refreshComments();
};


EditProjectDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  var usersTable = $("[name=usersTable]", self.getElement())[0];
  var overlaysTable = $("[name=overlaysTable]", self.getElement())[0];
  var mapsTable = $("[name=mapsTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(usersTable)) {
    $(usersTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(overlaysTable)) {
    $(overlaysTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(mapsTable)) {
    $(mapsTable).DataTable().destroy();
  }

  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
    delete self._addOverlayDialog;
  }
  if (this._commentsTab !== undefined) {
    this._commentsTab.destroy();
  }
};

/**
 *
 * @param {string} mapCanvasTypeId
 */
EditProjectDialog.prototype.setMapCanvasTypeId = function (mapCanvasTypeId) {
  var select = $("[name='project-map-canvas-type']", this.getElement());
  select.val(mapCanvasTypeId);
  select.change();
};

/**
 *
 * @returns {string}
 */
EditProjectDialog.prototype.getMapCanvasTypeId = function () {
  return $("[name='project-map-canvas-type']", this.getElement()).val();
};


EditProjectDialog.prototype.setLicenseAccepted = function (isLicenseAccepted) {
  $("[name='project-google-maps-license']", this.getElement()).prop("checked", isLicenseAccepted);
};

/**
 *
 * @returns {boolean}
 */
EditProjectDialog.prototype.isLicenseAccepted = function () {
  return $("[name='project-google-maps-license']", this.getElement()).is(":checked");
};

module.exports = EditProjectDialog;
