"use strict";

/* exported logger */

var AbstractAdminPanel = require('./AbstractAdminPanel');
var AddProjectDialog = require('./AddProjectDialog');
var EditProjectDialog = require('./EditProjectDialog');
var LogListDialog = require('./LogListDialog');
var PrivilegeType = require('../../map/data/PrivilegeType');
var UserPreferences = require('../../map/data/UserPreferences');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var Promise = require("bluebird");

function MapsAdminPanel(params) {
  var self = this;
  AbstractAdminPanel.call(self, params);
  self._createGui();

  $(self.getElement()).addClass("minerva-projects-tab");

}

MapsAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
MapsAdminPanel.prototype.constructor = MapsAdminPanel;

MapsAdminPanel.prototype._createGui = function () {
  var self = this;
  var projectsDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(projectsDiv);

  var dataDiv = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  projectsDiv.appendChild(dataDiv);

  dataDiv.appendChild(self._createMenuRow());
  dataDiv.appendChild(self._createProjectTableRow());
  dataDiv.appendChild(self._createMenuRow());

};

MapsAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addProjectButton = Functions.createElement({
    type: "button",
    name: "addProject",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD PROJECT',
    onclick: function () {
      return self.onAddClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshProject",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addProjectButton);
  menuRow.appendChild(refreshButton);
  return menuRow;
};

MapsAdminPanel.prototype._createProjectTableRow = function () {
  var self = this;
  var projectsRow = Functions.createElement({
    type: "div",
    style: "display:table-row; width:100%"
  });

  var projectsTable = Functions.createElement({
    type: "table",
    name: "projectsTable",
    className: "display",
    style: "width:100%"
  });
  projectsRow.appendChild(projectsTable);

  $(projectsTable).DataTable({
    columns: [{
      title: 'ProjectId'
    }, {
      title: 'Name'
    }, {
      title: 'Disease'
    }, {
      title: 'Organism'
    }, {
      title: 'Status'
    }, {
      title: 'Edit'
    }, {
      title: 'Remove'
    }]
  });
  self.bindUserGuiPreference({
    jQueryObject: $(projectsTable),
    event: 'length.dt',
    preferenceName: 'admin-projects-datatable-length',
    defaultValue: '10',
    getter: function () {
      return $(projectsTable).DataTable().page.len() + '';
    },
    setter: function (value) {
      return $(projectsTable).DataTable().page.len(value).draw();
    }
  });

  $(projectsTable).on("click", "[name='removeProject']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove '" + $(button).attr("data") + "' map?",
      input: false
    }).then(function (param) {
      if (param.status) {
        return self.removeProject($(button).attr("data"))
      }
    }).catch(GuiConnector.alert);
  });

  $(projectsTable).on("click", "[name='showEditDialog']", function () {
    var button = this;
    return self.showEditDialog($(button).attr("data")).then(null, GuiConnector.alert);
  });
  $(projectsTable).on("click", "[name='showErrors']", function () {
    var button = this;
    return self.showLogs($(button).attr("data"), 'error').then(null, GuiConnector.alert);
  });
  $(projectsTable).on("click", "[name='showWarnings']", function () {
    var button = this;
    return self.showLogs($(button).attr("data"), 'warning').then(null, GuiConnector.alert);
  });

  return projectsRow;
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.init = function () {
  var self = this;
  return self.getServerConnector().getProjects().then(function (projects) {
    return self.setProjects(projects);
  });
};

/**
 *
 * @param {Project} project
 * @param {Array} [row]
 * @param {User} user
 * @returns {Array}
 */
MapsAdminPanel.prototype.projectToTableRow = function (project, row, user) {
  var self = this;
  var disease = self.getHtmlStringLink(project.getDisease());
  var organism = self.getHtmlStringLink(project.getOrganism());

  if (row === undefined) {
    row = [];
  }
  var projectId = project.getProjectId();
  var formattedProjectId;
  if (project.getStatus().toLowerCase() === "ok") {
    formattedProjectId = "<a href='" + "index.xhtml?id=" + projectId + "' target='" + projectId + "'>" + projectId + "</a>";
  } else {
    formattedProjectId = projectId;
  }
  var status = project.getStatus();
  if (project.hasErrors()) {
    status += "<a name='showErrors' href='#' data='" + project.getProjectId() + "'>" +
      "<i class='fa fa-exclamation-triangle' style='font-size:18px; font-weight:400; padding-right:10px;color:red'></i>" +
      "</a>";
  }
  if (project.hasWarnings()) {
    status += "<a name='showWarnings' href='#' data='" + project.getProjectId() + "'>" +
      "<i class='fa fa-exclamation-triangle' style='font-size:18px; font-weight:400; padding-right:10px;color:black'></i>" +
      "</a>";
  }

  row[0] = formattedProjectId;
  row[1] = project.getName();
  row[2] = disease;
  row[3] = organism;
  row[4] = status;

  var disabled = " disabled ";
  if (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.CONFIGURATION_MANAGE))) {
    disabled = "";
  }
  row[5] = "<button name='showEditDialog' data='" + project.getProjectId() + "'" + disabled + "><i class='fa fa-edit' style='font-size:17px'></i></button>";
  row[6] = "<button name='removeProject' data='" + project.getProjectId() + "'" + disabled + "><i class='fa fa-trash-o' style='font-size:17px'></button>";

  return row;
};

MapsAdminPanel.prototype.getHtmlStringLink = function (annotation) {
  var self = this;
  if (annotation !== undefined && annotation !== null) {
    var link = self.getGuiUtils().createAnnotationLink(annotation, true);
    var tmp = document.createElement("div");
    tmp.appendChild(link);
    return tmp.innerHTML;
  } else {
    return "N/A";
  }

};

/**
 *
 * @param {Project[]} projects
 * @returns {Promise}
 */
MapsAdminPanel.prototype.setProjects = function (projects) {
  var self = this;

  return self.getServerConnector().getLoggedUser().then(function (user) {
    var dataTable = $("[name='projectsTable']", self.getElement()).DataTable();
    var data = [];
    var page = dataTable.page();

    for (var i = 0; i < projects.length; i++) {
      var project = projects[i];
      var rowData = self.projectToTableRow(project, undefined, user);
      self.addUpdateListener(project, rowData);
      data.push(rowData);
    }
    //it should be simplified, but I couldn't make it work
    dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);
  });
};

MapsAdminPanel.prototype.addUpdateListener = function (project, dataTableRow) {
  var self = this;

  var listenerName = "PROJECT_LIST_LISTENER";
  var listeners = project.getListeners("onreload");
  for (var i = 0; i < listeners.length; i++) {
    if (listeners[i].listenerName === listenerName) {
      project.removeListener("onreload", listeners[i]);
    }
  }
  var listener = function () {
    return self.getServerConnector().getLoggedUser().then(function (user) {
      self.projectToTableRow(project, dataTableRow, user);
      var row = $($("[name='projectsTable']", self.getElement())[0]).DataTable().row("#" + project.getProjectId());
      if (row.length > 0) {
        row.data(dataTableRow).draw();
      }
    });
  };
  listener.listenerName = listenerName;
  project.addListener("onreload", listener);
};

MapsAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  var dialog = self._addDialog;
  if (dialog === undefined) {
    dialog = new AddProjectDialog({
      element: Functions.createElement({
        type: "div"
      }),
      customMap: null,
      configuration: self.getConfiguration()
    });
    self._addDialog = dialog;
    dialog.addListener("onProjectAdd", function () {
      return self.onRefreshClicked()
    });
    return dialog.init().then(function () {
      return dialog.open();
    });
  } else {
    dialog.clear();
    dialog.open();
    return Promise.resolve();
  }
};

MapsAdminPanel.prototype.destroy = function () {
  var promises = [];
  var self = this;
  var dialog = self._addDialog;
  if (dialog !== undefined) {
    promises.push(dialog.destroy());
  }
  var table = $("[name='projectsTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    promises.push($(table).DataTable().destroy());
  }

  var key;
  for (key in self._logDialogs) {
    if (self._logDialogs.hasOwnProperty(key)) {
      promises.push(self._logDialogs[key].destroy());
    }
  }
  for (key in self._dialogs) {
    if (self._dialogs.hasOwnProperty(key)) {
      promises.push(self._dialogs[key].destroy());
    }
  }
  return Promise.all(promises);
};

MapsAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return ServerConnector.getProjects(true).then(function (projects) {
    return self.setProjects(projects);
  }).then(function () {
    return ServerConnector.getLoggedUser();
  }).then(function (user) {
    var privilege = self.getConfiguration().getPrivilegeType(PrivilegeType.USER_MANAGEMENT);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(privilege)) {
      return ServerConnector.getUsers(true);
    } else {
      return Promise.resolve();
    }
  });
};

MapsAdminPanel.prototype.getDialog = function (project) {
  var self = this;
  if (self._dialogs === undefined) {
    self._dialogs = [];
  }
  var dialog = self._dialogs[project.getProjectId()];
  if (dialog === undefined) {
    dialog = new EditProjectDialog({
      element: Functions.createElement({
        type: "div"
      }),
      project: project,
      configuration: self.getConfiguration(),
      customMap: null
    });
    self._dialogs[project.getProjectId()] = dialog;
    return dialog.init().then(function () {
      return dialog.addListener("onSave", function () {
        return self.onRefreshClicked()
      });
    }).then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

MapsAdminPanel.prototype.getLogDialog = function (projectId, level) {
  var self = this;
  if (self._logDialogs === undefined) {
    self._logDialogs = [];
  }
  var dialog = self._logDialogs[projectId + "-" + level];
  if (dialog === undefined) {
    dialog = new LogListDialog({
      element: Functions.createElement({
        type: "div"
      }),
      projectId: projectId,
      customMap: null,
      level: level
    });
    self._logDialogs[projectId + "-" + level] = dialog;
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

MapsAdminPanel.prototype.showEditDialog = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return ServerConnector.getProject(id).then(function (project) {
    return self.getDialog(project);
  }).then(function (dialog) {

    dialog.open();
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

MapsAdminPanel.prototype.showLogs = function (id, level) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getLogDialog(id, level).then(function (dialog) {
    return dialog.open();
  }).then(function () {
    GuiConnector.hideProcessing();
  }, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};


MapsAdminPanel.prototype.removeProject = function (id) {
  var self = this;
  return ServerConnector.removeProject(id).then(function () {
    return self.onRefreshClicked();
  });
};

module.exports = MapsAdminPanel;
