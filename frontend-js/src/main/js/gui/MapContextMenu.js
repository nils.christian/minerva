"use strict";

/* exported logger */

var ContextMenu = require('./ContextMenu');

var logger = require('../logger');

function MapContextMenu(params) {
  ContextMenu.call(this, params);
  var self = this;

  self._createMapContextMenuGui();
  self.setMolArt(params.molArt);
}

MapContextMenu.prototype = Object.create(ContextMenu.prototype);
MapContextMenu.prototype.constructor = MapContextMenu;


MapContextMenu.prototype._createMapContextMenuGui = function() {
  var self = this;
  self.addOption("Add comment", function() {
    return self.getMap().openCommentDialog();
  });
  self.addOption("Select mode", function() {
    return self.getMap().toggleDrawing();
  });  
};

MapContextMenu.prototype.init = function() {
  var self = this;
  return self.createExportAsImageSubmenu().then(function(submenu){
    self.addOption(submenu);
    return self.createExportAsModelSubmenu();
  }).then(function(submenu){
    self.addOption(submenu);
  });
};

MapContextMenu.prototype.setMolArt = function(molArt){
  this._molArt = molArt;
};

MapContextMenu.prototype.getMolArt = function(){
  return this._molArt;
};

module.exports = MapContextMenu;
