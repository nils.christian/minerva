"use strict";

var AbstractGuiElement = require('./AbstractGuiElement');
var ConfigurationType = require('../ConfigurationType');
var Functions = require('../Functions');
var PanelControlElementType = require('./PanelControlElementType');

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');

function Legend(params) {
  AbstractGuiElement.call(this, params);

  this._initializeGui();
}

Legend.prototype = Object.create(AbstractGuiElement.prototype);
Legend.prototype.constructor = Legend;

Legend.prototype._initializeGui = function () {
  var self = this;

  var legendDiv = Functions.createElement({
    type: "div",
    id: "legend-div",
    className: "carousel slide"
  });
  self.getElement().appendChild(legendDiv);

  var indicators = Functions.createElement({
    type: "ol",
    name: "indicators",
    className: "carousel-indicators"
  });
  legendDiv.appendChild(indicators);
  self.setControlElement(PanelControlElementType.LEGEND_INDICATORS_OL, indicators);

  var slidesDiv = Functions.createElement({
    type: "div",
    name: "slides",
    className: "carousel-inner",
    role: "listbox"
  });
  legendDiv.appendChild(slidesDiv);
  self.setControlElement(PanelControlElementType.LEGEND_SLIDES_DIV, slidesDiv);

  var leftButton = Functions
    .createElement({
      type: "a",
      className: "left carousel-control",
      role: "button",
      href: "#legend-div",
      content: '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span>',
      xss: false
    });
  leftButton.setAttribute("data-slide", "prev");
  legendDiv.appendChild(leftButton);

  var rightButton = Functions
    .createElement({
      type: "a",
      className: "right carousel-control",
      role: "button",
      href: "#legend-div",
      content: '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span>',
      xss: false
    });
  rightButton.setAttribute("data-slide", "next");
  legendDiv.appendChild(rightButton);
};

Legend.prototype.hide = function () {
  this.getElement().style.display = "none";
};
Legend.prototype.show = function () {
  var maxZIndex = Math.max.apply(null,
    $.map($('body *'), function (e) {
      if ($(e).css('position') !== 'static') {
        return parseInt($(e).css('z-index')) || 1;
      }
    }));

  this.getElement().style.display = "block";
  $(this.getElement()).css('z-index', maxZIndex + 1);
};

function createLegendIndicator(file, index) {
  var result = document.createElement("li");
  result.setAttribute("data-target", "legend");
  result.setAttribute("data-slide-to", "" + index);
  if (index === 0) {
    result.className = "active";
  }
  return result;
}

function createLegendSlide(file, index) {
  var result = document.createElement("div");
  if (index === 0) {
    result.className = "item active";
  } else {
    result.className = "item";
  }
  var img = document.createElement("img");
  img.src = file.getValue();
  result.appendChild(img);
  return result;
}

function getValidLegendFiles(legendFiles) {
  var result = [];

  var promises = [];
  legendFiles.forEach(function (file) {
    result.push(file);
    var url = file.getValue();
    if (url.indexOf("http") !== 0 && "" !== url) {
      url = ServerConnector.getServerBaseUrl() + url;
    }
    promises.push(ServerConnector.sendGetRequest(url).catch(function (error) {
      var index = result.indexOf(file);
      if (index > -1) {
        result.splice(index, 1);
      }
    }));
  });

  return Promise.all(promises).then(function () {
    return result;
  });
}

Legend.prototype.init = function () {
  var self = this;
  var element = self.getElement();
  var menu = self.getControlElement(PanelControlElementType.LEGEND_INDICATORS_OL);
  var slides = self.getControlElement(PanelControlElementType.LEGEND_SLIDES_DIV);
  return ServerConnector.getConfigurationParam(ConfigurationType.LEGEND_FILES).then(function (legendFiles) {
    return getValidLegendFiles(legendFiles);
  }).then(function (legendFiles) {
    for (var i = 0; i < legendFiles.length; i++) {
      var legendFile = legendFiles[i];
      menu.appendChild(createLegendIndicator(legendFile, i));
      slides.appendChild(createLegendSlide(legendFile, i));
    }
    $(element).carousel();
  });

};

module.exports = Legend;
