"use strict";
var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var GuiUtils = require('./leftPanel/GuiUtils');

var Functions = require('../Functions');

function PluginDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.setPluginManager(params.pluginManager);
  self._knownPlugins = [];
}

PluginDialog.prototype = Object.create(AbstractGuiElement.prototype);
PluginDialog.prototype.constructor = PluginDialog;

PluginDialog.prototype._createPluginGui = function () {
  var self = this;
  var guiUtils = new GuiUtils();

  self.getElement().innerHTML = "";

  var pluginFormTab = Functions.createElement({
    type: "div",
    name: "minerva-plugin-list-tab",
    style: "width:100%;display: table;border-spacing: 10px;"
  });
  self.getElement().appendChild(pluginFormTab);

  var pluginUrlLabel = Functions.createElement({
    type: "span",
    content: "URL:",
    style: "color:#999999"
  });
  var pluginUrlInput = Functions.createElement({
    type: "input",
    name: "pluginUrlValue"
  });

  var loadPluginButton = Functions.createElement({
    type: "button",
    name: "loadPluginButton",
    content: "LOAD",
    onclick: function () {
      return self.getPluginManager().addPlugin({url: pluginUrlInput.value}).then(function () {
        self.close();
      }).then(null, GuiConnector.alert);
    }
  });
  pluginFormTab.appendChild(guiUtils.createTableRow([pluginUrlLabel, pluginUrlInput, loadPluginButton]));

  var pluginManager = self.getPluginManager();

  var loadedPlugins = [];
  var plugins = pluginManager.getPlugins();
  var i;
  for (i = 0; i < plugins.length; i++) {
    loadedPlugins[plugins[i].getOptions().url] = true;
  }

  for (i = 0; i < self._knownPlugins.length; i++) {
    var pluginData = self._knownPlugins[i];
    var url = pluginData.url;

    var pluginUrl = Functions.createElement({
      type: "input",
      value: url,
      style: "color:#999999"
    });
    pluginUrl.readOnly = true;
    var button;
    if (!loadedPlugins[url]) {
      button = Functions.createElement({
        type: "button",
        content: "LOAD",
        onclick: function () {
          return pluginManager.addPlugin({url: $(this).data("url")}).then(function () {
            self.close();
          }).then(null, GuiConnector.alert);
        }
      });
    }
    $(button).data("url", url);

    pluginFormTab.appendChild(guiUtils.createTableRow([Functions.createElement({type: "span"}), pluginUrl, button]));
  }
  for (i = 0; i < plugins.length; i++) {
    var plugin = plugins[i];
    var removePlugin = (function () {
      var pluginToRemove = plugin;
      return function () {
        return pluginManager.removePlugin(pluginToRemove).then(function () {
          self.close();
        }).then(null, GuiConnector.alert);
      }
    })();
    pluginUrl = Functions.createElement({
      type: "input",
      value: plugins[i].getOptions().url,
      style: "color:#999999"
    });
    pluginUrl.readOnly = true;
    button = Functions.createElement({
      type: "button",
      content: "UNLOAD",
      onclick: removePlugin
    });

    pluginFormTab.appendChild(guiUtils.createTableRow([Functions.createElement({type: "span"}), pluginUrl, button]));

  }
};

PluginDialog.prototype.init = function () {
  var self = this;
  return ServerConnector.getConfiguration().then(function (configuration) {
    var pluginsData = configuration.getPluginsData();
    for (var i = 0; i < pluginsData.length; i++) {
      var pluginData = pluginsData[i];
      self._knownPlugins.push({url: ServerConnector.getServerBaseUrl() + pluginData.url});
    }
  });
};

PluginDialog.prototype.open = function () {
  var self = this;
  self._createPluginGui();
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      autoOpen: false,
      resizable: false,
      width: 400
    });
  }

  $(div).dialog('option', 'title', 'PLUGINS');
  $(div).dialog("open");
};
PluginDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

PluginDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
};

PluginDialog.prototype.setPluginManager = function (pluginManager) {
  this._pluginManager = pluginManager;
};

PluginDialog.prototype.getPluginManager = function () {
  return this._pluginManager;
};

module.exports = PluginDialog;
