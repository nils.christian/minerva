"use strict";

/* exported logger */

var logger = require('../logger');

function GuiMessageError(message) {
  this.message = message;
  this.stack = (new Error()).stack;
}

GuiMessageError.prototype = Object.create(Error.prototype);
GuiMessageError.prototype.constructor = GuiMessageError;

module.exports = GuiMessageError;
