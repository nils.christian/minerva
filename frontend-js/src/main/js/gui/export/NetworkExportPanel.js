"use strict";

/* exported logger */

var AbstractExportPanel = require('./AbstractExportPanel');

var Promise = require("bluebird");
var logger = require('../../logger');
var GuiMessageError = require('../GuiMessageError');


function NetworkExportPanel(params) {
  params.panelName = "networkExport";
  AbstractExportPanel.call(this, params);
}

NetworkExportPanel.prototype = Object.create(AbstractExportPanel.prototype);
NetworkExportPanel.prototype.constructor = NetworkExportPanel;

NetworkExportPanel.prototype.init = function () {

  var self = this;
  var element = self.getElement();
  var configuration;
  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    var typeDiv = self._createSelectTypeDiv(configuration.getElementTypes());
    element.appendChild(typeDiv);
    element.appendChild(self._createSelectColumnDiv(self.getAllColumns()));
    return ServerConnector.getProjectStatistics(self.getProject().getProjectId());
  }).then(function (statistics) {
    return self._createMiriamTypeDiv(statistics.getReactionAnnotations());
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectIncludedCompartmentDiv();
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectExcludedCompartmentDiv();
  }).then(function (div) {
    element.appendChild(div);
    element.appendChild(self._createDownloadButton());
  }).then(function () {
    $(window).trigger('resize');
  });

};

NetworkExportPanel.prototype.getAllColumns = function () {
  return [{
    "columnName": "elements",
    "method": "getElements",
    "name": "Elements",
    "formatFunction": function (elements, reaction, elementIds) {
      var stringBuilder = [];
      var type;
      var elementFormat = function (element) {
        if (elementIds[element.getAlias().getId()]) {
          stringBuilder.push(type + ":" + element.getAlias().getId());
        }
      };
      type = "REACTANT";
      reaction.getReactants().forEach(elementFormat);
      type = "PRODUCT";
      reaction.getProducts().forEach(elementFormat);
      type = "MODIFIER";
      reaction.getModifiers().forEach(elementFormat);

      return stringBuilder.join(",");
    }
  }, {
    "columnName": "id",
    "method": "getId",
    "name": "Id"
  }, {
    "columnName": "description",
    "method": "getDescription",
    "name": "Description"
  }, {
    "columnName": "modelId",
    "method": "getModelId",
    "name": "Model"
  }, {
    "columnName": "type",
    "method": "getType",
    "name": "Type"
  }, {
    "columnName": "symbol",
    "method": "getSymbol",
    "name": "Symbol"
  }, {
    "columnName": "abbreviation",
    "method": "getAbbreviation",
    "name": "Abbreviation"
  }, {
    "columnName": "formula",
    "method": "getFormula",
    "name": "Formula"
  }, {
    "columnName": "synonyms",
    "method": "getSynonyms",
    "name": "Synonyms"
  }, {
    "columnName": "references",
    "method": "getReferences",
    "name": "References",
    "formatFunction": function (references) {
      var stringBuilder = [];
      for (var i = 0; i < references.length; i++) {
        var reference = references[i];
        stringBuilder.push(reference.getType() + ":" + reference.getResource());
      }
      return stringBuilder.join(",");
    }
  }, {
    "columnName": "reactionId",
    "method": "getReactionId",
    "name": "Element external id"
  }, {
    "columnName": "mechanicalConfidenceScore",
    "method": "getMechanicalConfidenceScore",
    "name": "Mechanical Confidence Score"
  }, {
    "columnName": "lowerBound",
    "method": "getLowerBound",
    "name": "Lower Bound"
  }, {
    "columnName": "upperBound",
    "method": "getUpperBound",
    "name": "Upper Bound"
  }, {
    "columnName": "geneProteinReaction",
    "method": "getGeneProteinReaction",
    "name": "Gene Protein Reaction"
  }, {
    "columnName": "subsystem",
    "method": "getSubsystem",
    "name": "Subsystem"
  }];
};

function matchReaction(reaction, elementIds) {
  var count = 0;
  reaction.getElements().forEach(function (element) {
    if (elementIds[element.getId()]) {
      count++;
    }
  });
  return count > 1;
}

NetworkExportPanel.prototype.createResponseString = function () {
  var self = this;
  var types, miriamTypes;
  var includedCompartmentIds = [];
  var excludedCompartmentIds = [];
  var models = self.getProject().getModels();

  var elementIds = [];
  var reactions = [];
  return self.getSelectedTypes().then(function (result) {
    if (result.length === 0) {
      return Promise.reject(new GuiMessageError("You must select at least one type"));
    }
    types = result;
    return self.getSelectedIncludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      includedCompartmentIds.push(compartment.getId());
    });
    return self.getSelectedExcludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      excludedCompartmentIds.push(compartment.getId());
    });

    var promises = [];
    for (var i = 0; i < models.length; i++) {
      promises.push(models[i].getAliases({
        type: types,
        complete: true,
        includedCompartmentIds: includedCompartmentIds,
        excludedCompartmentIds: excludedCompartmentIds
      }));
    }
    return Promise.all(promises);
  }).then(function (aliasesByModel) {
    var promises = [];
    for (var i = 0; i < models.length; i++) {
      promises.push(models[i].getReactionsForElements(aliasesByModel[i], true));
      for (var j = 0; j < aliasesByModel[i].length; j++) {
        elementIds[aliasesByModel[i][j].getId()] = true;
      }
    }
    return Promise.all(promises);
  }).then(function (reactionsByModel) {
    for (var i = 0; i < models.length; i++) {
      for (var j = 0; j < reactionsByModel[i].length; j++) {
        var reaction = reactionsByModel[i][j];
        if (matchReaction(reaction, elementIds)) {
          reactions.push(reaction);
        }
      }
    }
    return self.getSelectedMiriamTypes();
  }).then(function (result) {
    miriamTypes = result;
    return self.getSelectedColumns();
  }).then(function (selectedColumns) {
    if (selectedColumns.length === 0) {
      return Promise.reject(new GuiMessageError("You must select at least one column"));
    }

    var rows = [];
    rows.push(self.createResponseHeader(selectedColumns, miriamTypes));
    for (var i = 0; i < reactions.length; i++) {
      rows.push(self.createResponseRow(reactions[i], selectedColumns, miriamTypes, elementIds));
    }
    return rows.join("\n");
  });
};

NetworkExportPanel.prototype.createResponseRow = function (reaction, columns, miriamTypes, elementIds) {
  var stringBuilder = [];
  var i, value;
  for (i = 0; i < columns.length; i++) {
    var column = columns[i];
    value = reaction[column.method]();
    if (column.formatFunction !== undefined) {
      value = column.formatFunction(value, reaction, elementIds);
    }
    if (value instanceof String || typeof value === "string") {
      value = value.replace(/[\n\r]/g, ' ');
    }
    stringBuilder.push(value);
  }
  for (i = 0; i < miriamTypes.length; i++) {
    value = "";
    var miriamType = miriamTypes[i];
    var references = reaction.getReferences();
    for (var j = 0; j < references.length; j++) {
      var reference = references[j];
      if (reference.getType() === miriamType.getName()) {
        value += reference.getResource() + ",";
      }
    }
    stringBuilder.push(value);
  }
  return stringBuilder.join("\t");
};

module.exports = NetworkExportPanel;
