"use strict";

/* exported logger */

var Panel = require('../Panel');

var GuiConnector = require('../../GuiConnector');
var logger = require('../../logger');
var Functions = require('../../Functions');
var DualListbox = require('dual-listbox').DualListbox;
var Promise = require("bluebird");

function AbstractExportPanel(params) {
  params.scrollable = true;
  Panel.call(this, params);

}

AbstractExportPanel.prototype = Object.create(Panel.prototype);
AbstractExportPanel.prototype.constructor = AbstractExportPanel;

AbstractExportPanel.prototype.init = function () {

};

function compareSimple(val1, val2) {
  if (val1 < val2) {
    return -1;
  } else if (val1 > val2) {
    return 1;
  } else {
    return 0;
  }
}

AbstractExportPanel.prototype._createMiriamTypeDiv = function (annotations) {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "miriamSelectDiv",
    className: "minerva-export-dual-listbox-container"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " Annotations:"
  }));

  var selectElement = Functions.createElement({
    type: "select",
    className: "minerva-multi-select"
  });
  typeDiv.appendChild(selectElement);

  function compare(entry1, entry2) {
    if ((entry1.count === 0 && entry2.count === 0) || (entry1.count !== 0 && entry2.count !== 0)) {
      return compareSimple(entry1.miriamType.getCommonName(), entry2.miriamType.getCommonName());
    } else {
      return compareSimple(-entry1.count, -entry2.count);
    }
  }

  annotations.sort(compare);

  for (var i = 0; i < annotations.length; i++) {
    var miriamType = annotations[i].miriamType;
    var count = annotations[i].count;

    var option = new Option();
    option.value = miriamType.getName();
    if (count > 0) {
      option.innerHTML = "<div class='minerva-multi-select-special'>" + miriamType.getCommonName() + " (" + count
        + ")</div>";
    } else {
      option.innerHTML = "<div >" + miriamType.getCommonName() + "</div>";
    }
    selectElement.appendChild(option);
  }

  self.setMiriamTypesDualListbox(self.createDualListbox(selectElement));
  return typeDiv;
};

AbstractExportPanel.prototype.createDualListbox = function (selectElement) {
  return new DualListbox(selectElement, {
    availableTitle: 'Available',
    selectedTitle: 'Used',
    addButtonText: '>',
    removeButtonText: '<',
    addAllButtonText: '>>',
    removeAllButtonText: '<<'
  });
};

AbstractExportPanel.prototype._createSelectTypeDiv = function (elementTypes) {
  var typeDiv = Functions.createElement({
    type: "div",
    name: "typeSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " TYPE:"
  }));
  var choicesContainer = Functions.createElement({
    type: "ul",
    className: "minerva-checkbox-grid"
  });
  typeDiv.appendChild(choicesContainer);
  var i;
  var toBeExcluded = [];
  for (i = 0; i < elementTypes.length; i++) {
    toBeExcluded[elementTypes[i].parentClass] = true;
  }
  var processedNames = [];
  for (i = 0; i < elementTypes.length; i++) {
    var elementType = elementTypes[i];
    var name = elementType.name;
    if (processedNames[name] === undefined && toBeExcluded[elementType.className] === undefined) {
      processedNames[name] = true;
      var row = Functions.createElement({
        type: "li",
        content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"" + name + "\" value=\"" + name + "\" />" + name + "</label></div>",
        xss: false
      });
      choicesContainer.appendChild(row);
    }
  }
  choicesContainer.appendChild(Functions.createElement({
    type: "li",
    content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"ALL\" value=\"ALL\" />ALL</label></div>",
    xss: false
  }));
  return typeDiv;
};

AbstractExportPanel.prototype.getSelectedTypes = function () {
  var self = this;

  var div = $("div[name='typeSelectDiv']", $(self.getElement()))[0];
  var result = [];
  var selectedAll = false;
  $(":checked", $(div)).each(function (index, element) {
    if (element.value === "ALL") {
      selectedAll = true;
    }
    result.push(element.value);
  });
  if (selectedAll) {
    return ServerConnector.getConfiguration().then(function (configuration) {
      return configuration.getSimpleElementTypeNames();
    });
  }

  return Promise.resolve(result);
};

AbstractExportPanel.prototype.setMiriamTypesDualListbox = function (dualListbox) {
  this._miriamTypesDualListbox = dualListbox;
};
AbstractExportPanel.prototype.getMiriamTypesDualListbox = function () {
  return this._miriamTypesDualListbox;
};

AbstractExportPanel.prototype._createSelectColumnDiv = function (columnTypes) {
  var columnDiv = Functions.createElement({
    type: "div",
    name: "columnSelectDiv"
  });
  columnDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " COLUMN:"
  }));
  var choicesContainer = Functions.createElement({
    type: "ul",
    className: "minerva-checkbox-grid"
  });

  columnDiv.appendChild(choicesContainer);
  for (var i = 0; i < columnTypes.length; i++) {
    var columnType = columnTypes[i];
    var row = Functions.createElement({
      type: "li",
      content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"column_" + columnType.columnName
      + "\" value=\"" + columnType.columnName + "\" />" + columnType.name + "</label></div>",
      xss: false
    });
    choicesContainer.appendChild(row);
  }
  choicesContainer.appendChild(Functions.createElement({
    type: "li",
    content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"ALL\" value=\"ALL\" />ALL</label></div>",
    xss: false
  }));
  return columnDiv;
};

AbstractExportPanel.prototype.getSelectedMiriamTypes = function () {
  var self = this;
  return ServerConnector.getConfiguration().then(function (configuration) {
    var selected = self.getMiriamTypesDualListbox().selected;
    var result = [];
    for (var i = 0; i < selected.length; i++) {
      var miriamType = configuration.getMiriamTypeByName(selected[i].dataset.id);
      result.push(miriamType);
    }
    return result;
  });
};

AbstractExportPanel.prototype.getSelectedColumns = function () {
  var self = this;

  var div = $("div[name='columnSelectDiv']", $(self.getElement()))[0];
  var selectedColumnMap = [];
  var selectedAll = false;
  $(":checked", $(div)).each(function (index, element) {
    if (element.value === "ALL") {
      selectedAll = true;
    }
    selectedColumnMap[element.value] = true;
  });
  var columnTypes = self.getAllColumns();

  if (selectedAll) {
    return Promise.resolve(columnTypes);
  }

  var result = [];

  for (var i = 0; i < columnTypes.length; i++) {
    var columnType = columnTypes[i];
    if (selectedColumnMap[columnType.columnName] === true) {
      result.push(columnType);
    }
  }
  return Promise.resolve(result);
};

AbstractExportPanel.prototype._getCompartmentNames = function () {
  var self = this;
  var compartments = [];
  return self._getAllCompartments().then(function (result) {

    var addedNames = [];
    for (var i = 0; i < result.length; i++) {
      if (addedNames[result[i].getName()] === undefined) {
        compartments.push(result[i].getName());
        addedNames[result[i].getName()] = true;
      }
    }
    compartments.sort(compareSimple);
    return compartments;
  });
};

AbstractExportPanel.prototype.getModels = function () {
  return this.getProject().getModels();
};

AbstractExportPanel.prototype._getAllCompartments = function () {
  var self = this;
  var compartments = [];

  var models = self.getModels();

  var promises = [];
  for (var i = 0; i < models.length; i++) {
    promises.push(models[i].getCompartments());
  }

  return Promise.all(promises).then(function (result) {
    for (var i = 0; i < result.length; i++) {
      var modelCompartments = result[i];
      for (var j = 0; j < modelCompartments.length; j++) {
        compartments.push(modelCompartments[j]);
      }
    }
    return compartments;
  });
};

AbstractExportPanel.prototype._createDownloadButton = function () {
  var self = this;
  var downloadDiv = Functions.createElement({
    type: "div",
    name: "downloadDiv",
    style: "clear:both; padding: 10px;"
  });
  var button = Functions.createElement({
    type: "button",
    name: "downloadButton",
    content: " Download",
    onclick: function () {
      GuiConnector.showProcessing();
      return self.createResponseString().then(function (result) {
        var blob = new Blob([result], {
          type: "text/plain;charset=utf-8"
        });
        var FileSaver = require("file-saver");
        return FileSaver.saveAs(blob, self.getSaveFilename());
      }).then(function () {
        GuiConnector.hideProcessing();
      }, function (error) {
        GuiConnector.hideProcessing();
        GuiConnector.alert(error);
      });
    }
  });
  downloadDiv.appendChild(button);

  return downloadDiv;
};

AbstractExportPanel.prototype.getSaveFilename = function () {
  var self = this;
  return self.getProject().getProjectId() + "-" + self.getPanelName() + ".txt";
};

AbstractExportPanel.prototype._createSelectIncludedCompartmentDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "includedCompartmentSelectDiv",
    className: "minerva-export-dual-listbox-container"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " Included compartment/pathways:"
  }));

  return self._getCompartmentNames().then(function (compartmentNames) {

    var selectElement = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    typeDiv.appendChild(selectElement);

    for (var i = 0; i < compartmentNames.length; i++) {
      var name = compartmentNames[i];

      var option = new Option();
      option.value = name;
      option.innerHTML = "<div >" + name + "</div>";
      selectElement.appendChild(option);
    }
    self.setIncludedCompartmentsDualListbox(self.createDualListbox(selectElement));
    return typeDiv;
  });
};

AbstractExportPanel.prototype._createSelectExcludedCompartmentDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "excludedCompartmentSelectDiv",
    className: "minerva-export-dual-listbox-container"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " Excluded compartment/pathways:"
  }));

  return self._getCompartmentNames().then(function (compartmentNames) {

    var selectElement = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    typeDiv.appendChild(selectElement);

    for (var i = 0; i < compartmentNames.length; i++) {
      var name = compartmentNames[i];

      var option = new Option();
      option.value = name;
      option.innerHTML = "<div >" + name + "</div>";
      selectElement.appendChild(option);
    }
    self.setExcludedCompartmentsDualListbox(self.createDualListbox(selectElement));
    return typeDiv;
  });
};

AbstractExportPanel.prototype.setIncludedCompartmentsDualListbox = function (dualListbox) {
  this._includedCompartmentsDualListbox = dualListbox;
};
AbstractExportPanel.prototype.getIncludedCompartmentsDualListbox = function () {
  return this._includedCompartmentsDualListbox;
};

AbstractExportPanel.prototype.getSelectedIncludedCompartments = function () {
  var self = this;
  var list = self.getIncludedCompartmentsDualListbox().selected;
  var names = [];
  var result = [];
  for (var i = 0; i < list.length; i++) {
    var element = list[i];
    names[element.dataset.id] = true;
  }
  return self._getAllCompartments().then(function (compartments) {
    for (var i = 0; i < compartments.length; i++) {
      var compartment = compartments[i];
      if (names[compartment.getName()]) {
        result.push(compartment);
      }
    }
    return result;
  });
};

AbstractExportPanel.prototype.setExcludedCompartmentsDualListbox = function (dualListbox) {
  this._excludedCompartmentsDualListbox = dualListbox;
};
AbstractExportPanel.prototype.getExcludedCompartmentsDualListbox = function () {
  return this._excludedCompartmentsDualListbox;
};

AbstractExportPanel.prototype.getSelectedExcludedCompartments = function () {
  var self = this;
  var list = self.getExcludedCompartmentsDualListbox().selected;
  var names = [];
  var result = [];
  for (var i = 0; i < list.length; i++) {
    var element = list[i];
    names[element.dataset.id] = true;
  }
  return self._getAllCompartments().then(function (compartments) {
    for (var i = 0; i < compartments.length; i++) {
      var compartment = compartments[i];
      if (names[compartment.getName()]) {
        result.push(compartment);
      }
    }
    return result;
  });
};

AbstractExportPanel.prototype.createResponseHeader = function (columns, miriamTypes) {
  var stringBuilder = [];
  var i;
  for (i = 0; i < columns.length; i++) {
    var column = columns[i];
    stringBuilder.push(column.name);
  }
  for (i = 0; i < miriamTypes.length; i++) {
    var miriamType = miriamTypes[i];
    stringBuilder.push(miriamType.getCommonName());
  }
  return stringBuilder.join("\t");

};

module.exports = AbstractExportPanel;
