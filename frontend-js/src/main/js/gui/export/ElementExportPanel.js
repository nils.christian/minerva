"use strict";

/* exported logger */

var AbstractExportPanel = require('./AbstractExportPanel');
var GuiMessageError = require('../GuiMessageError');

var logger = require('../../logger');

var Promise = require("bluebird");

function ElementExportPanel(params) {
  params.panelName = "elementExport";
  AbstractExportPanel.call(this, params);
}

ElementExportPanel.prototype = Object.create(AbstractExportPanel.prototype);
ElementExportPanel.prototype.constructor = ElementExportPanel;

ElementExportPanel.prototype.init = function () {
  var self = this;
  var element = self.getElement();
  var configuration;
  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    var typeDiv = self._createSelectTypeDiv(configuration.getElementTypes());
    element.appendChild(typeDiv);
    element.appendChild(self._createSelectColumnDiv(self.getAllColumns()));
    return ServerConnector.getProjectStatistics(self.getProject().getProjectId());
  }).then(function (statistics) {
    return self._createMiriamTypeDiv(statistics.getElementAnnotations());
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectIncludedCompartmentDiv();
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectExcludedCompartmentDiv();
  }).then(function (div) {
    element.appendChild(div);
    element.appendChild(self._createDownloadButton());
  }).then(function () {
    $(window).trigger('resize');
  });
};

ElementExportPanel.prototype.createResponseString = function () {
  var self = this;
  var types, miriamTypes;
  var includedCompartmentIds = [];
  var excludedCompartmentIds = [];
  var models = self.getProject().getModels();

  var elements = [];
  return self.getSelectedTypes().then(function (result) {
    if (result.length === 0) {
      return Promise.reject(new GuiMessageError("You must select at least one type"));
    }
    types = result;
    return self.getSelectedIncludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      includedCompartmentIds.push(compartment.getId());
    });
    return self.getSelectedExcludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      excludedCompartmentIds.push(compartment.getId());
    });

    var promises = [];
    for (var i = 0; i < models.length; i++) {
      promises.push(models[i].getAliases({
        type: types,
        complete: true,
        includedCompartmentIds: includedCompartmentIds,
        excludedCompartmentIds: excludedCompartmentIds
      }));
    }
    return Promise.all(promises);
  }).then(function (aliasesByModel) {
    for (var i = 0; i < aliasesByModel.length; i++) {
      for (var j = 0; j < aliasesByModel[i].length; j++) {
        elements.push(aliasesByModel[i][j]);
      }
    }

    return self.getSelectedMiriamTypes();
  }).then(function (result) {
    miriamTypes = result;
    return self.getSelectedColumns();
  }).then(function (selectedColumns) {
    if (selectedColumns.length === 0) {
      return Promise.reject(new GuiMessageError("You must select at least one column"));
    }

    var rows = [];
    rows.push(self.createResponseHeader(selectedColumns, miriamTypes));
    for (var i = 0; i < elements.length; i++) {
      rows.push(self.createResponseRow(elements[i], selectedColumns, miriamTypes));
    }
    return rows.join("\n");
  });
};

ElementExportPanel.prototype.createResponseRow = function (alias, columns, miriamTypes) {
  var stringBuilder = [];
  var i, value;
  for (i = 0; i < columns.length; i++) {
    var column = columns[i];
    value = alias[column.method]();
    if (column.formatFunction !== undefined) {
      value = column.formatFunction(value);
    }
    if (value instanceof String || typeof value === "string") {
      value = value.replace(/[\n\r]/g, ' ');
    }
    stringBuilder.push(value);
  }
  for (i = 0; i < miriamTypes.length; i++) {
    value = "";
    var miriamType = miriamTypes[i];
    var references = alias.getReferences();
    for (var j = 0; j < references.length; j++) {
      var reference = references[j];
      if (reference.getType() === miriamType.getName()) {
        value += reference.getResource() + ",";
      }
    }
    stringBuilder.push(value);
  }
  return stringBuilder.join("\t");
};

ElementExportPanel.prototype.getAllColumns = function () {
  return [{
    "columnName": "id",
    "method": "getId",
    "name": "Id"
  }, {
    "columnName": "name",
    "method": "getName",
    "name": "Name"
  }, {
    "columnName": "description",
    "method": "getDescription",
    "name": "Description"
  }, {
    "columnName": "modelId",
    "method": "getModelId",
    "name": "Model"
  }, {
    "columnName": "type",
    "method": "getType",
    "name": "Type"
  }, {
    "columnName": "complexId",
    "method": "getComplexId",
    "name": "Complex"
  }, {
    "columnName": "compartmentId",
    "method": "getCompartmentId",
    "name": "Compartment"
  }, {
    "columnName": "charge",
    "method": "getCharge",
    "name": "Charge"
  }, {
    "columnName": "symbol",
    "method": "getSymbol",
    "name": "Symbol"
  }, {
    "columnName": "fullName",
    "method": "getFullName",
    "name": "Full name"
  }, {
    "columnName": "abbreviation",
    "method": "getAbbreviation",
    "name": "Abbreviation"
  }, {
    "columnName": "formula",
    "method": "getFormula",
    "name": "Formula"
  }, {
    "columnName": "synonyms",
    "method": "getSynonyms",
    "name": "Synonyms"
  }, {
    "columnName": "formerSymbols",
    "method": "getFormerSymbols",
    "name": "Former symbols"
  }, {
    "columnName": "references",
    "method": "getReferences",
    "name": "References",
    "formatFunction": function (references) {
      var stringBuilder = [];
      for (var i = 0; i < references.length; i++) {
        var reference = references[i];
        stringBuilder.push(reference.getType() + ":" + reference.getResource());
      }
      return stringBuilder.join(",");
    }
  }, {
    "columnName": "linkedSubmodelId",
    "method": "getLinkedSubmodelId",
    "name": "Linked submodel"
  }, {
    "columnName": "elementId",
    "method": "getElementId",
    "name": "Element external id"
  }];
};


module.exports = ElementExportPanel;
