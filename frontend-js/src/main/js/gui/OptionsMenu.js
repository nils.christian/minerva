"use strict";

var ContextMenu = require('./ContextMenu');
var PluginDialog = require('./PluginDialog');
var Promise = require('bluebird');

function OptionsMenu(params) {
  ContextMenu.call(this, params);
  var self = this;

  self._createMenuGui();
}

OptionsMenu.prototype = Object.create(ContextMenu.prototype);
OptionsMenu.prototype.constructor = OptionsMenu;


OptionsMenu.prototype._createMenuGui = function () {
  var self = this;
  self.addOption("Plugins", function () {
    var initPromise = Promise.resolve();
    if (self._pluginDialog === undefined) {
      self._pluginDialog = new PluginDialog({
        element: document.createElement("div"),
        customMap: self.getMap(),
        pluginManager: self.getPluginManager()
      });
      initPromise = self._pluginDialog.init();
    }
    return initPromise.then(function () {
      return self._pluginDialog.open();
    })
  });
};

OptionsMenu.prototype.init = function () {
  return Promise.resolve();
};
OptionsMenu.prototype.destroy = function () {
  var self = this;
  var promises = [];
  if (self._pluginDialog !== undefined) {
    promises.push(self._pluginDialog.destroy());
  }
  return Promise.all(promises);
};

OptionsMenu.prototype.setPluginManager = function (pluginManager) {
  this._pluginManager = pluginManager;
};

OptionsMenu.prototype.getPluginManager = function () {
  return this._pluginManager;
};

module.exports = OptionsMenu;
