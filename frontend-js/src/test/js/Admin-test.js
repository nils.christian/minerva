"use strict";

require("./mocha-config");

var Admin = require('../../main/js/Admin');
var ServerConnector = require('./ServerConnector-mock');
var logger = require('./logger');

var assert = require('assert');

describe('Admin', function () {
  describe('constructor', function () {
    it('default', function () {
      var admin = new Admin(helper.createCustomMapOptions(null));
      assert.ok(admin);
      return admin.init().then(function(){
        return admin.destroy();
      });
    });
  });

  describe('logout', function () {
    it('default', function () {
      helper.loginAsAdmin();
      var admin = new Admin(helper.createCustomMapOptions(null));
      var token = ServerConnector.getSessionData().getToken();
      return admin.init().then(function () {
        assert.ok(token === ServerConnector.getSessionData().getToken());
        var link = $("#logoutLink", testDiv)[0];
        return link.onclick();
      }).then(function () {
        assert.ok(token !== ServerConnector.getSessionData().getToken());
        assert.equal(0, logger.getWarnings().length, "Didn't expect any warning");
        return admin.destroy();
      })
    });
  });

});
