"use strict";

require("./mocha-config");

var Configuration = require('../../main/js/Configuration');
var ConfigurationType = require('../../main/js/ConfigurationType');
var ServerConnector = require('./ServerConnector-mock');
var logger = require('./logger');

var assert = require('assert');

describe('Configuration', function () {
  describe('constructor', function () {
    it('default', function () {
      var configuration = new Configuration({
        options: {},
        overlayTypes: {},
        imageFormats: {},
        modelFormats: {},
        elementTypes: {},
        reactionTypes: {},
        miriamTypes: {},
        mapTypes: {},
        modificationStateTypes: {},
        privilegeTypes: {},
        annotators: {}
      });
      assert.ok(configuration);
      assert.equal(0, logger.getWarnings().length);
    });
    it('copy', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var copy = new Configuration(configuration);

        assert.equal(configuration.getElementTypes().length, copy.getElementTypes().length)
      });
    });
  });

  describe('getParentType', function () {
    it('for element type', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var elementType = configuration.getElementTypes()[0];
        if (elementType.parentClass.indexOf("BioEntity") > 0) {
          elementType = configuration.getElementTypes()[1];
        }
        var parent = configuration.getParentType(elementType);
        assert.ok(parent);
      });
    });
    it('for reaction type', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var elementType = configuration.getReactionTypes()[0];
        if (elementType.parentClass.indexOf("BioEntity") > 0) {
          elementType = configuration.getReactionTypes()[1];
        }
        var parent = configuration.getParentType(elementType);
        assert.ok(parent);
      });
    });
  });

  describe('getElementAnnotators', function () {
    it('get all', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var annotators = configuration.getElementAnnotators();
        assert.ok(annotators);
        assert.ok(annotators.length > 0);
      });
    });
    it('get for element', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var found = false;
        var types = configuration.getElementTypes();
        for (var i = 0; i < types.length; i++) {
          var annotators = configuration.getElementAnnotators(types[i]);
          if (annotators.length > 0) {
            found = true;
          }
        }
        assert.ok(found);
      });
    });
  });

  describe('getOption', function () {
    it('DEFAULT_MAP', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var option = configuration.getOption(ConfigurationType.DEFAULT_MAP);
        assert.ok(option);
        assert.ok(option.getType());
        assert.ok(option.getValue());
        assert.ok(option.getCommonName());
      });
    });
    it('get for element', function () {
      return ServerConnector.getConfiguration().then(function (configuration) {
        var found = false;
        var types = configuration.getElementTypes();
        for (var i = 0; i < types.length; i++) {
          var annotators = configuration.getElementAnnotators(types[i]);
          if (annotators.length > 0) {
            found = true;
          }
        }
        assert.ok(found);
      });
    });
  });

  it('getElementTypeTree', function () {

    return ServerConnector.getConfiguration().then(function (configuration) {
      var treeData = configuration.getElementTypeTree();

      assert.ok(treeData);
      assert.ok(treeData.text);
      assert.ok(treeData.children);
      assert.equal(2, treeData.children.length);
    })
  });


});
