"use strict";

var ObjectWithListeners = require('../../main/js/ObjectWithListeners');

var logger = require('./logger');

var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

describe('ObjectWithListeners', function() {
  beforeEach(function() {
    logger.flushBuffer();
  });
  
  it("ObjectWithListeners constructor", function() {
    var obj = new ObjectWithListeners();
    assert.ok(obj);
  });

  it("add invalid listener", function() {
    var obj = new ObjectWithListeners();
    obj.registerListenerType("t");
    var code = function(){obj.addListener("t", "not a function");};
    expect(code).to.throw(/string/);
    
  });

  it("add invalid property change listener", function() {
    var obj = new ObjectWithListeners();
    var code = function(){obj.addPropertyChangeListener ("t", function(){});};
    expect(code).to.throw(/Unknown property/);
    
  });

  it("add invalid property change listener 2", function() {
    var obj = new ObjectWithListeners();
    obj.registerPropertyType("t");
    var code = function(){obj.addPropertyChangeListener ("t", "not a function");};
    expect(code).to.throw(/string/);
    
  });

  it("re-register listener type", function() {
    var obj = new ObjectWithListeners();
    obj.registerListenerType("t");
    var code = function(){obj.registerListenerType("t");};
    expect(code).to.throw(/already registered/);
    
  });

  it("re-register property type", function() {
    var obj = new ObjectWithListeners();
    obj.registerPropertyType ("t");
    var code = function(){obj.registerPropertyType ("t");};
    expect(code).to.throw(/already registered/);
    
  });

  it("remove invalid listener", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var fun = function(){};
    var code = function(){obj.removeListener(name, fun);};
    expect(code).to.throw(/Unknown listener type/);
    
  });

  it("remove invalid listener 2", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var fun = function(){};
    obj.registerListenerType(name, fun);
    var code = function(){obj.removeListener(name, "not a function");};
    expect(code).to.throw(/string/);
    
  });

  it("remove invalid listener 3", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var fun = function(){};
    obj.registerListenerType(name, fun);
    obj.removeListener(name, function(){});
    assert.equal(logger.getWarnings().length, 1);
    
  });

  it("remove invalid property listener", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var fun = function(){};
    var code = function(){obj.removePropertyListener (name, fun);};
    expect(code).to.throw(/Unknown property/);
    
  });

  it("remove invalid property listener 2", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    obj.registerPropertyType(name);
    var code = function(){obj.removePropertyListener (name, "not a function");};
    expect(code).to.throw(/string/);
    
  });

  it("remove invalid property listener 3", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var fun = function(){};
    obj.registerPropertyType(name);
    obj.removePropertyListener (name, fun);
    assert.equal(logger.getWarnings().length, 1);
    
  });

  it("call invalid listeners", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var code = function(){obj.callListeners(name);};
    expect(code).to.throw(/Unknown listener/);
    
  });
  it("fire invalid property change listeners", function() {
    var obj = new ObjectWithListeners();
    var name = "t";
    var code = function(){obj.firePropertyChangeListener (name,"old","new");};
    expect(code).to.throw(/Unknown property/);
    
  });

  it("ObjectWithListeners property changes", function() {
    var handlerCalled = 0;
    var obj = new ObjectWithListeners();

    var handler = function() {
      handlerCalled++;
    };

    obj.registerPropertyType("test");

    obj.addPropertyChangeListener("test", handler);

    obj.firePropertyChangeListener("test", "old val", "new val");
    assert.equal(1, handlerCalled);

    obj.firePropertyChangeListener("test", "old val", "new val");
    assert.equal(2, handlerCalled);

    obj.removePropertyListener("test", handler);

    obj.firePropertyChangeListener("test", "old val", "new val");
    assert.equal(2, handlerCalled);
  });
});
