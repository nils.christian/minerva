"use strict";

require("../mocha-config.js");

var OptionsMenu = require('../../../main/js/gui/OptionsMenu');
var PluginManager = require('../../../main/js/plugin/PluginManager');
var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('OptionsMenu', function () {

  it('init', function () {
    var map = helper.createCustomMap();

    var menu = new OptionsMenu({
      element: testDiv,
      customMap: map
    });
    return menu.init();
  });

  it('open dialog', function () {
    var map = helper.createCustomMap();

    var div = document.createElement("div");
    var menu = new OptionsMenu({
      element: div,
      customMap: map
    });
    var pluginManager;
    return ServerConnector.getProject().then(function (project) {

      pluginManager = new PluginManager({project: project});
      return menu.init();
    }).then(function () {
      menu.setPluginManager(pluginManager);
      return menu.open(10, 10, 20);
    }).then(function () {
      return $("a", div).data("handler")();
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      return menu.destroy();
    }).then(function () {
      return pluginManager.destroy();
    });
  });
});
