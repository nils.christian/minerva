"use strict";

require("../../mocha-config.js");

var ElementExportPanel = require('../../../../main/js/gui/export/ElementExportPanel');
var GuiMessageError = require('../../../../main/js/gui/GuiMessageError');
var MiriamType = require('../../../../main/js/map/data/MiriamType');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var assert = require('assert');

describe('ElementExportPanel', function () {

  describe('getSelectedTypes', function () {
    it('select single element', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        $("input[name='Degraded']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedTypes();
      }).then(function (selectedTypes) {
        assert.equal(0, logger.getWarnings().length);
        assert.deepEqual(["Degraded"], selectedTypes);
      });
    });
    it('select all', function () {
      var exportObject;
      var project;
      var elementTypes;

      var configuration;

      return ServerConnector.getConfiguration().then(function (result) {
        configuration = result;
        elementTypes = configuration.getSimpleElementTypeNames();
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        $("input[name='Degraded']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        $("input[name='ALL']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedTypes();
      }).then(function (selectedTypes) {
        assert.deepEqual(elementTypes, selectedTypes);
      });
    });
  });

  describe('getSelectedColumns', function () {
    it('select single element', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        $("input[name='column_name']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedColumns();
      }).then(function (selectedTypes) {
        assert.equal(["name"], selectedTypes[0].columnName);
      });
    });
    it('select all', function () {
      var exportObject;
      var project;
      var elementTypes;
      var configuration;

      return ServerConnector.getConfiguration().then(function (result) {
        configuration = result;
        elementTypes = configuration.getElementTypes();
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        $("input[name='column_name']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        $("input[name='ALL']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedColumns();
      }).then(function (selectedTypes) {
        assert.equal(exportObject.getAllColumns().length, selectedTypes.length);
      });
    });
  });

  describe('getSelectedMiriamTypes', function () {
    it('select single element', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        var dlb = exportObject.getMiriamTypesDualListbox();
        var listItem = dlb.available[0];
        dlb.addSelected(listItem);
        return exportObject.getSelectedMiriamTypes();
      }).then(function (result) {
        assert.equal(result.length, 1);
        assert.ok(result[0] instanceof MiriamType);
      });
    });
  });

  describe('getSelectedIncludedCompartments', function () {
    it('empty selection', function () {
      helper.setUrl("http://test/?id=complex_model_with_submaps");

      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        assert.ok(exportObject.getIncludedCompartmentsDualListbox().available.length > 0);
        return exportObject.getSelectedIncludedCompartments();
      }).then(function (result) {
        assert.equal(result.length, 0);
      });
    });
  });

  describe('getSelectedExcludedCompartments', function () {
    it('empty selection', function () {
      helper.setUrl("http://test/?id=complex_model_with_submaps");

      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        var dlb = exportObject.getExcludedCompartmentsDualListbox();
        var listItem = dlb.available[0];
        dlb.addSelected(listItem);
        return exportObject.getSelectedExcludedCompartments();
      }).then(function (result) {
        assert.equal(result.length, 1);
      });
    });
  });

  describe('createResponseString', function () {
    it('get all', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        $("input[name='ALL']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        var dlb = exportObject.getMiriamTypesDualListbox();
        var listItem = dlb.available[0];
        dlb.addSelected(listItem);
        return exportObject.createResponseString();
      }).then(function (result) {
        // protein id
        assert.equal(result.indexOf("[object Object]"), -1);
        assert.ok(result.indexOf("329156") >= 0);
      });
    });
    it('get invalid', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getConfiguration();
      }).then(function (configuration) {
        exportObject = new ElementExportPanel({
          element: testDiv,
          project: project,
          configuration: configuration
        });
        return exportObject.init();
      }).then(function () {
        return exportObject.createResponseString();
      }).then(null, function (error) {
        assert.ok(error instanceof GuiMessageError);
      });
    });
  });

  it('download file', function () {
    var exportObject;
    var project;
    return ServerConnector.getProject().then(function (result) {
      project = result;
      return ServerConnector.getConfiguration();
    }).then(function (configuration) {
      exportObject = new ElementExportPanel({
        element: testDiv,
        project: project,
        configuration: configuration
      });
      return exportObject.init();
    }).then(function () {
      $("input[name='ALL']", $(testDiv)).each(function (index, element) {
        $(element).prop("checked", true);
      });
      var element = $("[name='downloadButton']", $(testDiv))[0];
      return element.onclick();
    });
  });

  describe('createResponseRow', function () {
    it('description with new line', function () {
      var exportObject = new ElementExportPanel({
        element: testDiv,
        project: helper.createProject(),
        configuration: helper.getConfiguration()
      });
      var alias = helper.createAlias();
      var desc = "test\ntest2\n";
      alias.setDescription(desc);
      var rowString = exportObject.createResponseRow(alias, exportObject.getAllColumns(), []);
      assert.ok(rowString.indexOf("test\ntest2\n") === -1);
    });
  });

});
