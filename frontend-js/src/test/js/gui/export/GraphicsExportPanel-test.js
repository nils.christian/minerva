"use strict";
require("../../mocha-config");

var GraphicsExportPanel = require('../../../../main/js/gui/export/GraphicsExportPanel');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');
var assert = require('assert');

describe('GraphicsExportPanel', function() {

  it('getModelId', function() {
    var exportObject;
    var project;
    return ServerConnector.getProject().then(function(result) {
      project = result;
      return ServerConnector.getConfiguration();
    }).then(function(configuration) {
      exportObject = new GraphicsExportPanel({
        element : testDiv,
        project : project,
        configuration : configuration
      });
      return exportObject.init();
    }).then(function() {
      var id = exportObject.getSubmapId();
      assert.ok(id);
      assert.equal(0,logger.getWarnings().length)
    });
  });

  it('getFormatHandler', function() {
    var exportObject;
    var project;
    return ServerConnector.getProject().then(function(result) {
      project = result;
      return ServerConnector.getConfiguration();
    }).then(function(configuration) {
      exportObject = new GraphicsExportPanel({
        element : testDiv,
        project : project,
        configuration : configuration
      });
      return exportObject.init();
    }).then(function() {
      var format = exportObject.getFormatHandler();
      assert.ok(format);
    });
  });

  it('download file', function() {
    var exportObject;
    var project;
    return ServerConnector.getProject().then(function(result) {
      project = result;
      return ServerConnector.getConfiguration();
    }).then(function(configuration) {
      exportObject = new GraphicsExportPanel({
        element : testDiv,
        project : project,
        configuration : configuration
      });
      return exportObject.init();
    }).then(function() {
      var element = $("[name='downloadButton']", $(testDiv))[0];
      return element.onclick();
    });
  });

});
