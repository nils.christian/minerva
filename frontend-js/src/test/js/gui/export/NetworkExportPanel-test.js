"use strict";

require("../../mocha-config");

var NetworkExportPanel = require('../../../../main/js/gui/export/NetworkExportPanel');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');
var chai = require('chai');
var assert = chai.assert;

describe('NetworkExportPanel', function () {

  it('download file', function () {
    var exportObject;
    var project;
    return ServerConnector.getProject().then(function (result) {
      project = result;
      return ServerConnector.getConfiguration();
    }).then(function (configuration) {
      exportObject = new NetworkExportPanel({
        element: testDiv,
        project: project,
        configuration: configuration
      });
      return exportObject.init();
    }).then(function () {
      $("input[name='ALL']", $(testDiv)).each(function (index, element) {
        $(element).prop("checked", true);
      });
      var element = $("[name='downloadButton']", $(testDiv))[0];
      assert.equal(0, logger.getWarnings().length);
      return element.onclick();
    });
  });

});
