"use strict";

/* exported logger */

require('../../mocha-config.js');

var Legend = require('../../../../main/js/gui/Legend');
var TopMenu = require('../../../../main/js/gui/topMenu/TopMenu');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('TopMenu', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    new TopMenu({
      element: testDiv,
      customMap: map
    });

    assert.equal(logger.getWarnings().length, 0);

  });

  it('init', function () {
    var map = helper.createCustomMap();

    var topMenu = new TopMenu({
      element: testDiv,
      customMap: map
    });

    return topMenu.init();

  });

  it('toggleLegend', function () {
    var map = helper.createCustomMap();

    var topMenu = new TopMenu({
      element: testDiv,
      customMap: map
    });
    var legend = new Legend({element: document.createElement("div"), customMap: map});
    topMenu.setLegend(legend);
    return topMenu.init().then(function () {
      $("input", testDiv).prop('checked', true);
      return topMenu.toggleLegend();
    }).then(function () {
      $("input", testDiv).prop('checked', false);
      return topMenu.toggleLegend();
    });
  });

});
