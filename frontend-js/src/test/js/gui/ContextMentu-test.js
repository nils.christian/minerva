"use strict";

require("../mocha-config.js");

var ContextMenu = require('../../../main/js/gui/ContextMenu');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('ContextMenu', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    new ContextMenu({
      element: testDiv,
      customMap: map
    });

    assert.equal(logger.getWarnings().length, 0);
  });

  it('open', function () {
    var map = helper.createCustomMap();

    var menu = new ContextMenu({
      element: testDiv,
      customMap: map
    });

    menu.open(0, 0, new Date().getDate());
    assert.equal(logger.getWarnings().length, 0);
  });

  it('hide', function () {
    var map = helper.createCustomMap();

    var menu = new ContextMenu({
      element: testDiv,
      customMap: map
    });

    menu.hide();
    assert.equal(logger.getWarnings().length, 0);
    return menu.destroy();
  });

  it('documentClickListener', function () {
    var map = helper.createCustomMap();

    var menu = new ContextMenu({
      element: testDiv,
      customMap: map
    });

    menu._documentClickListener({target: {className: {}}});

    return menu.destroy();

  });

});
