"use strict";

require("../mocha-config.js");

var Header = require('../../../main/js/gui/Header');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('Header', function () {

  describe('constructor', function () {
    it('default', function () {
      var map = helper.createCustomMap();

      new Header({
        element: testDiv,
        customMap: map
      });
      assert.equal(logger.getWarnings().length, 0);
      assert.equal(1, $(".fa-lock", $(testDiv)).length);
    });
    it('without admin-link', function () {
      var map = helper.createCustomMap();

      new Header({
        element: testDiv,
        customMap: map,
        adminLink: false
      });
      assert.equal(logger.getWarnings().length, 0);
      assert.equal(0, $(".fa-lock", $(testDiv)).length);
    });
    it('with options-link', function () {
      var map = helper.createCustomMap();

      var header = new Header({
        element: testDiv,
        customMap: map,
        optionsMenu: true
      });
      assert.equal(logger.getWarnings().length, 0);
      assert.equal(1, $(".fa-bars", $(testDiv)).length);
      return header.destroy();
    });
  });

  it('init', function () {
    var map = helper.createCustomMap();

    var header = new Header({
      element: testDiv,
      customMap: map
    });

    return header.init().then(function () {
      return header.destroy();
    });
  });

});
