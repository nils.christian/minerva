"use strict";

require("../mocha-config");

var AddOverlayDialog = require('../../../main/js/gui/AddOverlayDialog');
var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('AddOverlayDialog', function () {

  it('addOverlay', function () {
    var dialog;
    return ServerConnector.getProject().then(function (project) {
      dialog = new AddOverlayDialog({
        element: testDiv,
        project: project,
        customMap: null,
        configuration: helper.getConfiguration(),
        serverConnector: ServerConnector
      });

      dialog.setFileContent("s1\n");
      assert.equal(0, logger.getWarnings().length);
      return dialog.addOverlay();
    });
  });

});
