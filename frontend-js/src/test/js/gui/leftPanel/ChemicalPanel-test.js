"use strict";

require('../../mocha-config');

var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');
var Annotation = require('../../../../main/js/map/data/Annotation');
var Chemical = require('../../../../main/js/map/data/Chemical');
var ChemicalPanel = require('../../../../main/js/gui/leftPanel/ChemicalPanel');
var PanelControlElementType = require('../../../../main/js/gui/PanelControlElementType');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('ChemicalPanel', function () {
  var parkinsonDiseaseAnnotation = new Annotation({
    link: "http://bioportal.bioontology.org/ontologies/1351?p=terms&conceptid=D010300",
    type: "MESH_2012",
    resource: "D010300"
  });
  var alzheimerDiseaseAnnotation = new Annotation({
    link: "http://bioportal.bioontology.org/ontologies/1351?p=terms&conceptid=D000544",
    type: "MESH_2012",
    resource: "D000544"
  });

  function createPanel() {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    helper.createChemicalDbOverlay(map);

    return new ChemicalPanel({
      element: testDiv,
      customMap: map
    });
  }

  describe('constructor', function () {
    it('default', function () {
      var map = helper.createCustomMap();
      helper.createChemicalDbOverlay(map);

      var panel = new ChemicalPanel({
        element: testDiv,
        customMap: map
      });
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(panel.isDisabled());
    });
    it('map with disease', function () {
      return ServerConnector.getProject().then(function (project) {
        project.setDisease(parkinsonDiseaseAnnotation);

        var map = helper.createCustomMap(project);
        helper.createChemicalDbOverlay(map);

        var panel = new ChemicalPanel({
          element: testDiv,
          customMap: map
        });
        assert.equal(logger.getWarnings().length, 0);
        assert.notOk(panel.isDisabled());
      });
    });
  });

  describe('init', function () {
    it('Parkinson Disease', function () {
      var panel;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        helper.createChemicalDbOverlay(map);

        panel = new ChemicalPanel({
          element: testDiv,
          customMap: map
        });
        return panel.init();
      }).then(function () {
        panel.destroy();
      });
    });
    it('with chemical query in session', function () {
      var panel;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        helper.createChemicalDbOverlay(map);

        panel = new ChemicalPanel({
          element: testDiv,
          customMap: map
        });
        var query = panel.getOverlayDb().encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, "rotenone");
        ServerConnector.getSessionData(project).setQuery({
          type: "chemical",
          query: query
        });

        return panel.init();
      }).then(function () {
        panel.destroy();
      });
    });
  });

  describe('createPreamble', function () {
    it('default', function () {
      var map = helper.createCustomMap();
      helper.createChemicalDbOverlay(map);

      var panel = new ChemicalPanel({
        element: testDiv,
        customMap: map
      });

      assert.ok(panel.createPreamble().innerHTML.indexOf("NOT FOUND") > 0);
    });

    it('for empty', function () {
      var map = helper.createCustomMap();
      helper.createChemicalDbOverlay(map);

      var panel = new ChemicalPanel({
        element: testDiv,
        customMap: map
      });

      assert.ok(panel.createPreamble(new Chemical()).innerHTML.indexOf("NOT FOUND") > 0);
    });
  });

  it('on searchResults changed', function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var chemicalDbOverlay = helper.createChemicalDbOverlay(map);
      new ChemicalPanel({
        element: testDiv,
        customMap: map
      });

      return chemicalDbOverlay.searchByQuery("rotenone");
    }).then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("marker/mechanism") >= 0);
    });
  });

  it('searchByQuery', function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      helper.createChemicalDbOverlay(map);

      var panel = new ChemicalPanel({
        element: testDiv,
        customMap: map
      });

      panel.getControlElement(PanelControlElementType.SEARCH_INPUT).value = "rotenone";

      return panel.searchByQuery();
    }).then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("Rotenone ") >= 0);
    });
  });

  it("refreshSearchAutocomplete", function () {
    var panel = createPanel();

    return panel.refreshSearchAutocomplete("s").then(function (data) {
      assert.ok(data);
    });
  });

  describe("getToolTipForAnnotation", function () {
    it("for null argument", function () {
      var panel = createPanel();

      return panel.getToolTipForAnnotation(null).then(function (tooltip) {
        assert.ok(tooltip.indexOf("Comparative Toxicogenomics Database") >= 0);
        assert.equal(tooltip.indexOf("Parkinson"), -1);
      });
    });
    it("for Parkinson Disease", function () {
      var panel = createPanel();

      return panel.getToolTipForAnnotation(parkinsonDiseaseAnnotation).then(function (tooltip) {
        assert.ok(tooltip.indexOf("Comparative Toxicogenomics Database") >= 0);
        assert.ok(tooltip.indexOf("Parkinson") >= 0);
      });
    });
    it("for Alzheimer Disease", function () {
      var panel = createPanel();

      return panel.getToolTipForAnnotation(alzheimerDiseaseAnnotation).then(function (tooltip) {
        assert.ok(tooltip.indexOf("Comparative Toxicogenomics Database") >= 0);
        assert.equal(tooltip.indexOf("Parkinson"), -1);
      });
    });
  });


})
;
