"use strict";

require('../../mocha-config.js');

var SubmapPanel = require('../../../../main/js/gui/leftPanel/SubmapPanel');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('SubmapPanel', function () {

  it('constructor', function () {
    var div = document.createElement("div");

    var map = helper.createCustomMap();

    new SubmapPanel({
      element: div,
      customMap: map,
      parent: {
        getMap: function () {
          return map;
        }
      }
    });
    assert.equal(logger.getWarnings().length, 0);
    var buttons = div.getElementsByTagName("button");
    assert.equal(buttons.length, 1);
  });

  describe('init', function () {
    it('simple', function () {
      var div = document.createElement("div");

      var map = helper.createCustomMap();

      var panel = new SubmapPanel({
        element: div,
        customMap: map,
        parent: {
          getMap: function () {
            return map;
          },
          hideTab: function () {
          }
        }
      });
      return panel.init().then(function () {
        assert.equal(logger.getWarnings().length, 0);
        var buttons = div.getElementsByTagName("button");
        assert.equal(buttons.length, 1);
      }).finally(function () {
        return panel.destroy();
      })
    });
    it('with submaps', function () {
      var div = document.createElement("div");
      var panel;
      return ServerConnector.getProject("complex_model_with_submaps").then(function (project) {
        var map = helper.createCustomMap(project);

        panel = new SubmapPanel({
          element: div,
          customMap: map,
          parent: {
            getMap: function () {
              return map;
            },
            hideTab: function () {
            }
          }
        });
        return panel.init();
      }).then(function () {
        assert.equal(logger.getWarnings().length, 0);
        assert.equal(panel.getElement().innerHTML.indexOf("UNKNOWN submaps"), -1, "UNKNOWN submaps table should have no title");
      }).finally(function () {
        return panel.destroy();
      })
    });
  });

});
