"use strict";

require('../../mocha-config.js');

var PublicationListDialog = require('../../../../main/js/gui/leftPanel/PublicationListDialog');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('PublicationListDialog', function () {

  it('constructor', function () {
    var div = testDiv;

    var map = helper.createCustomMap();

    var dialog = new PublicationListDialog({
      element: div,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);

    dialog.destroy();
  });

  it('_dataTableAjaxCall', function () {
    var dialog;
    var callbackCalled = false;
    return ServerConnector.getProject().then(function (project) {
      dialog = new PublicationListDialog({
        element: testDiv,
        customMap: helper.createCustomMap(project)
      });
      return dialog._dataTableAjaxCall({
        start: 0,
        length: 10,
        order: [{column: 0, dir: 'asc'}],
        search: {value: '', regex: false}
      }, function () {
        callbackCalled = true;
      });
    }).then(function () {
      assert.ok(callbackCalled);
    }).finally(function () {
      dialog.destroy();
    });
  });

});
