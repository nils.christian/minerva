"use strict";

require("../../mocha-config");

var CommentsAdminPanel = require('../../../../main/js/gui/admin/CommentsAdminPanel');
var ServerConnector = require('../../ServerConnector-mock');


var logger = require('../../logger');

var fs = require("fs");
var chai = require('chai');
var assert = chai.assert;

describe('CommentsAdminPanel', function () {
  function createDialog() {
    return ServerConnector.getConfiguration().then(function (configuration) {
      return new CommentsAdminPanel({
        element: testDiv,
        configuration: configuration,
        customMap: null
      });
    });
  }

  describe('askConfirmRemoval', function () {
    it('check question', function () {
      helper.loginAsAdmin();
      return createDialog().then(function (dialog) {
        var title = "Question?";
        dialog.askConfirmRemoval({title: title});
        assert.ok($(".ui-dialog-title").html().indexOf(title) >= 0);
        $("button:contains(Cancel)").click();

        return dialog.destroy();
      });
    });
    it('check ok status', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        var promise = dialog.askConfirmRemoval({title: "Question?"});
        $("button:contains(OK)").click();
        return promise;
      }).then(function (result) {
        assert.ok(result.status);
        return dialog.destroy();
      });
    });
    it('check cancel status', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        var promise = dialog.askConfirmRemoval({title: "Question?"});
        $("button:contains(Cancel)").click();
        return promise;
      }).then(function (result) {
        assert.notOk(result.status);
        return dialog.destroy();
      });
    });
    it('check close confirmation status', function () {
      helper.loginAsAdmin();
      var dialog;
      return createDialog().then(function (result) {
        dialog = result;
        var promise = dialog.askConfirmRemoval({title: "Question?"});
        $(".ui-dialog-titlebar-close").click();
        return promise;
      }).then(function (result) {
        assert.notOk(result.status);
        return dialog.destroy();
      });
    });
  });
});
