"use strict";

require("../../mocha-config");

var AddProjectDialog = require('../../../../main/js/gui/admin/AddProjectDialog');
var ConfigurationType = require('../../../../main/js/ConfigurationType');
var ServerConnector = require('../../ServerConnector-mock');
var ValidationError = require('../../../../main/js/ValidationError');


var logger = require('../../logger');

var fs = require("fs");
var chai = require('chai');
var assert = chai.assert;

describe('AddProjectDialog', function () {
  /**
   *
   * @returns {AddProjectDialog}
   */
  var createDialog = function () {
    return new AddProjectDialog({
      element: testDiv,
      customMap: null,
      configuration: helper.getConfiguration()
    });
  };
  it('init', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    var manualUrl;
    return dialog.init().then(function () {
      assert.ok(dialog.getNotifyEmail() !== "");
      assert.equal(0, logger.getWarnings().length);
      return dialog.destroy();
    });
  });

  it('check help buttons', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    return dialog.init().then(function () {
      assert.ok(testDiv.innerHTML.indexOf("minerva-help-button") >= 0);
      return dialog.destroy();
    });
  });

  it('onUserPreferencesChange', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    return dialog.init().then(function () {
      dialog.setCache(false);
      assert.notOk(dialog.isCache());
      dialog.setCache(true);
      assert.ok(dialog.isCache());
      return dialog.destroy();
    });
  });

  describe('onFileUpload', function () {
    it('default', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        assert.ok(dialog.getFileContent());
        assert.equal("test", dialog.getProjectId());
        return dialog.getConverter();
      }).then(function (converter) {
        assert.equal("xml", converter.extension);
        return dialog.destroy();
      });
    });
  });

  describe('setZipFileContent', function () {
    it('submaps', function () {
      var dialog = createDialog();
      var buf = fs.readFileSync("testFiles/map/complex_model_with_submaps.zip");
      buf.name = "complex_model_with_submaps.zip";
      return dialog.init().then(function () {
        return dialog.setZipFileContent(buf);
      }).then(function () {
        assert.equal(5, dialog.getZipEntries().length);
        return dialog.destroy();
      });
    });
    it('overlays', function () {
      var dialog = createDialog();
      var buf = fs.readFileSync("testFiles/map/complex_model_with_overlays.zip");
      buf.name = "complex_model_with_overlays.zip";
      return dialog.init().then(function () {
        var dataTable = $($("[name='overlaysTable']", testDiv)[0]).DataTable();
        assert.equal(0, dataTable.data().count());
        return dialog.setZipFileContent(buf);
      }).then(function () {
        var dataTable = $($("[name='overlaysTable']", testDiv)[0]).DataTable();
        assert.ok(dataTable.data().count() > 0);
        return dialog.destroy();
      });
    });
  });

  describe('showAnnotatorsDialog', function () {
    it('default', function () {
      var dialog = createDialog();

      return dialog.showAnnotatorsDialog().then(function () {
        return dialog.destroy();
      });
    });
  });


  describe('onSaveClicked', function () {
    var originalAddProject;
    beforeEach(function () {
      originalAddProject = ServerConnector.addProject;
    });
    afterEach(function () {
      ServerConnector.addProject = originalAddProject;
    });
    it('default', function () {
      var options;
      ServerConnector.addProject = function (params) {
        options = params;
      };
      var dialog = createDialog();
      var projectAdded = false;
      dialog.addListener("onProjectAdd", function(){
        projectAdded = true;
      });

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        return dialog.onSaveClicked();
      }).then(function () {
        assert.ok(options["name"] !== undefined);
        assert.ok(options["projectId"] !== undefined);
        assert.ok(options["file-id"] !== undefined);
        assert.ok(options["parser"] !== undefined);
        assert.ok(options["auto-resize"] !== undefined);
        assert.ok(options["cache"] !== undefined);
        assert.ok(options["notify-email"] !== undefined);
        assert.ok(options["disease"] !== undefined);
        assert.ok(options["organism"] !== undefined);
        assert.ok(options["sbgn"] !== undefined);
        assert.ok(options["version"] !== undefined);

        assert.ok(options["semantic-zoom"] !== undefined);

        assert.ok(projectAdded);
      }).finally(function () {
        return dialog.destroy();
      });
    });
  });

  it('getOrganism', function () {
    var dialog = createDialog();
    dialog.setOrganism("9606");
    assert.equal("9606", dialog.getOrganism());
    return dialog.destroy();
  });

  describe('checkValidity', function () {
    it('invalid project name', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setProjectId("(invalid id)");
        return dialog.checkValidity().then(function () {
          assert.notOk("Expected validity to reject");
        }).catch(function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).then().finally(function () {
        return dialog.destroy();
      });
    });
    it('valid project name', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setProjectId("-valid_id");
        return dialog.checkValidity();
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('google license not selected', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setMapCanvasTypeId("GOOGLE_MAPS_API");
        return dialog.checkValidity().then(function () {
          assert.notOk("Expected validation error: license is not accepted");
        }).catch(function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('google license selected', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setMapCanvasTypeId("GOOGLE_MAPS_API");
        dialog.setLicenseAccepted(true);
        return dialog.checkValidity();
      }).finally(function () {
        return dialog.destroy();
      });
    });
  });

  describe('isIgnoredZipEntry', function () {
    it('valid entry', function () {
      var dialog = createDialog();

      assert.notOk(dialog.isIgnoredZipEntry("images/a.png"));
      assert.notOk(dialog.isIgnoredZipEntry("main.xml"));
      return dialog.destroy();
    });
    it('invalid MAC OS entry', function () {
      var dialog = createDialog();

      // noinspection SpellCheckingInspection
      assert.ok(dialog.isIgnoredZipEntry("__MACOSX/.desc"));
      assert.ok(dialog.isIgnoredZipEntry("__macosx/.desc"));
      return dialog.destroy();
    });
    it('invalid old MAC OS entry', function () {
      var dialog = createDialog();

      assert.ok(dialog.isIgnoredZipEntry(".DS_Store/.desc"));
      assert.ok(dialog.isIgnoredZipEntry(".ds_store/.desc"));
      return dialog.destroy();
    });
  });

  describe('createInputRow', function () {
    it('with help button', function () {
      var dialog = createDialog();
      var row = dialog.createInputRow({help: "help me"});
      assert.ok(row.innerHTML.indexOf("minerva-help-button") >= 0);
      return dialog.destroy();
    });
  });

});
