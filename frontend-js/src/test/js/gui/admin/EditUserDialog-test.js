"use strict";

require("../../mocha-config");

var EditUserDialog = require('../../../../main/js/gui/admin/EditUserDialog');
var User = require('../../../../main/js/map/data/User');
var ValidationError = require('../../../../main/js/ValidationError');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

describe('EditUserDialog', function () {

  describe('init', function () {
    it('empty user', function () {
      var dialog;
      var project;
      var user = new User({});
      return ServerConnector.getProject().then(function (result) {
        project = result;
        dialog = new EditUserDialog({
          element: testDiv,
          project: project,
          user: user,
          customMap: null
        });
        return dialog.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok(testDiv.innerHTML.indexOf("DEFAULT PRIVILEGE FOR NEW PROJECT") >= 0);
        assert.equal(testDiv.innerHTML.indexOf("DEFAULT PRIVILEGE FOR NEW PROJECT"), testDiv.innerHTML.lastIndexOf("DEFAULT PRIVILEGE FOR NEW PROJECT"));
        dialog.destroy();
      });
    });
  });

  describe('checkValidity', function () {
    it('empty user', function () {
      var dialog;
      var project;
      var user = new User({});
      return ServerConnector.getProject().then(function (result) {
        project = result;
        dialog = new EditUserDialog({
          element: testDiv,
          project: project,
          user: user,
          customMap: null
        });
        return dialog.init();
      }).then(function () {
        return dialog.checkValidity().then(function () {
          assert.ok(null);
        }, function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).then(function () {
        dialog.destroy();
      });
    });

    it('existing user', function () {
      var dialog;
      var project;
      var user;
      return ServerConnector.getUser("anonymous").then(function (result) {
        user = result;
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        dialog = new EditUserDialog({
          element: testDiv,
          project: project,
          user: user,
          customMap: null
        });
        return dialog.init();
      }).then(function () {
        return dialog.checkValidity();
      }).then(function (result) {
        assert.ok(result);
      }).then(function () {
        dialog.destroy();
      });
    });
  });

  describe('onSaveClicked', function () {
    it('existing user', function () {
      var dialog;
      var project;
      var user;
      return ServerConnector.getUser("anonymous").then(function (result) {
        user = result;
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        dialog = new EditUserDialog({
          element: testDiv,
          project: project,
          user: user,
          customMap: null
        });
        return dialog.init();
      }).then(function () {
        return dialog.onSaveClicked();
      }).then(function () {
        dialog.destroy();
      });
    });

  });
  describe('click privilege checkbox', function () {
    it('existing user', function () {
      helper.loginAsAdmin();
      var dialog;
      var project;
      var user;
      var serializedPrivileges;
      return ServerConnector.getUser("anonymous").then(function (result) {
        user = result;
        serializedPrivileges = user.privilegesToExport(helper.getConfiguration());
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        dialog = new EditUserDialog({
          element: testDiv,
          project: project,
          user: user,
          customMap: null
        });
        return dialog.init();
      }).then(function () {
        return $("[name=privilege-checkbox]", dialog.getElement()).click();
      }).then(function () {
        expect(serializedPrivileges).not.to.deep.equal(user.privilegesToExport(helper.getConfiguration()));
        dialog.destroy();
      });
    });

  });
  describe('change privilege int value', function () {
    it('existing user', function () {
      helper.loginAsAdmin();
      var dialog;
      var project;
      var user;
      var serializedPrivileges;
      return ServerConnector.getUser("anonymous").then(function (result) {
        user = result;
        serializedPrivileges = user.privilegesToExport(helper.getConfiguration());
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        dialog = new EditUserDialog({
          element: testDiv,
          project: project,
          user: user,
          customMap: null
        });
        return dialog.init();
      }).then(function () {
        $("[name=privilege-int]", dialog.getElement()).val("101010");
        $("[name=privilege-int]", dialog.getElement()).trigger("change");
      }).then(function () {
        expect(serializedPrivileges).not.to.deep.equal(user.privilegesToExport(helper.getConfiguration()));
        dialog.destroy();
      });
    });

  });
});
