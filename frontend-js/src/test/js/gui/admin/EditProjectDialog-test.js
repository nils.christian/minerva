"use strict";

require("../../mocha-config");

var EditProjectDialog = require('../../../../main/js/gui/admin/EditProjectDialog');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var assert = require('assert');

describe('EditProjectDialog', function () {

  function createDialog() {
    return ServerConnector.getProject().then(function (project) {
      return new EditProjectDialog({
        element: testDiv,
        project: project,
        customMap: null,
        configuration: helper.getConfiguration()
      });
    });
  }

  it('open', function () {
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.open();
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      dialog.destroy();
    });
  });

  it('init', function () {
    helper.loginAsAdmin();
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.init();
    }).then(function () {
      dialog.destroy();
    });
  });
  it('saveOverlay', function () {
    helper.loginAsAdmin();
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.init();
    }).then(function () {
      return dialog.saveOverlay(14081);
    }).then(function () {
      dialog.destroy();
    });
  });

  it('saveMap', function () {
    helper.loginAsAdmin();
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.init();
    }).then(function () {
      return dialog.saveMap(15781);
    }).then(function () {
      dialog.destroy();
    });
  });

  it('saveUser', function () {
    helper.loginAsAdmin();
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.init();
    }).then(function () {
      return dialog.saveUser("anonymous");
    }).then(function () {
      dialog.destroy();
    });
  });


  it('onSaveClicked', function () {
    var dialog;
    var project;
    return ServerConnector.getProject().then(function (result) {
      project = result;
      project.setVersion("2.01");
      dialog = new EditProjectDialog({
        element: testDiv,
        project: project,
        customMap: null,
        configuration: helper.getConfiguration()
      });
      return dialog.init();
    }).then(function () {
      return dialog.onSaveClicked();
    }).then(function (result) {
      assert.ok(project === result);
      dialog.destroy();
    });
  });

  it('openAddOverlayDialog', function () {
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.openAddOverlayDialog();
    }).then(function () {
      dialog.destroy();
    });
  });

});
