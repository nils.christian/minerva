"use strict";

require("../../mocha-config");

var GenomeAdminPanel = require('../../../../main/js/gui/admin/GenomeAdminPanel');
var ConfigurationType = require('../../../../main/js/ConfigurationType');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');
var Promise = require('bluebird');

var chai = require('chai');
var assert = chai.assert;

describe('GenomeAdminPanel', function () {
  describe('init', function () {
    it('admin', function () {
      helper.loginAsAdmin();
      var mapTab;
      return ServerConnector.getConfiguration().then(function (configuration) {
        mapTab = new GenomeAdminPanel({
          element: testDiv,
          configuration: configuration,
          serverConnector: ServerConnector
        });
        return mapTab.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        return mapTab.destroy();
      });
    });
  });
  it('open edit dialog', function () {
    helper.loginAsAdmin();
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = new GenomeAdminPanel({
        element: testDiv,
        configuration: configuration,
        serverConnector: ServerConnector
      });
      return mapTab.init();
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      var element = $("[name='editGenome']")[0];
      return helper.triggerJqueryEvent(element, "click");
    }).then(function () {
      return mapTab.destroy();
    });
  });

  it('open add genome dialog', function () {
    helper.loginAsAdmin();
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = new GenomeAdminPanel({
        element: testDiv,
        configuration: configuration,
        serverConnector: ServerConnector
      });
      return mapTab.init();
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      var element = $("[name='addGenome']")[0];
      return element.onclick();
    }).then(function () {
      return mapTab.destroy();
    });
  });

  it('remove genome', function () {
    helper.loginAsAdmin();
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = new GenomeAdminPanel({
        element: testDiv,
        configuration: configuration,
        serverConnector: ServerConnector
      });
      return mapTab.init();
    }).then(function () {
      mapTab.askConfirmRemoval = function () {
        return Promise.resolve({status: true});
      };
      assert.equal(0, logger.getWarnings().length);
      var element = $("[name='removeGenome']")[0];
      return helper.triggerJqueryEvent(element, "click");
    }).then(function () {
      return mapTab.destroy();
    });
  });
});
