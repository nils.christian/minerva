"use strict";

require("../../mocha-config");

var MapsAdminPanel = require('../../../../main/js/gui/admin/MapsAdminPanel');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var assert = require('assert');

function createMapsAdminPanel(configuration) {
  return new MapsAdminPanel({
    element: testDiv,
    configuration: configuration,
    serverConnector: ServerConnector
  });
}

describe('MapsAdminPanel', function () {

  it('refresh', function () {
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = createMapsAdminPanel(configuration);
      return mapTab.init();
    }).then(function () {
      return mapTab.onRefreshClicked();
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      return mapTab.destroy();
    });
  });
  it('showLogs', function () {
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = createMapsAdminPanel(configuration);
      return mapTab.init();
    }).then(function () {
      return mapTab.showLogs("sample", 'error');
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      return mapTab.destroy();
    });
  });

  it('getDialog', function () {
    helper.loginAsAdmin();
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = createMapsAdminPanel(configuration);
      return mapTab.init();
    }).then(function () {
      return ServerConnector.getProject();
    }).then(function (project) {
      return mapTab.getDialog(project);
    }).then(function (dialog) {
      assert.ok(dialog.getListeners("onSave").length > 0);
      return mapTab.destroy();
    });
  });


  describe('onAddClicked', function () {
    it('default', function () {
      var mapTab;
      return ServerConnector.getConfiguration().then(function (configuration) {
        mapTab = createMapsAdminPanel(configuration);
        return mapTab.init();
      }).then(function () {
        return mapTab.onAddClicked();
      }).then(function () {
        return mapTab.destroy();
      });
    });
    it('close and reopen', function () {
      var mapTab;
      return ServerConnector.getConfiguration().then(function (configuration) {
        mapTab = createMapsAdminPanel(configuration);
        return mapTab.init();
      }).then(function () {
        return mapTab.onAddClicked();
      }).then(function () {
        mapTab._addDialog.close();
        return mapTab.onAddClicked();
      }).then(function () {
        return mapTab.destroy();
      });
    });
  });

});
