"use strict";

require("../../mocha-config");

var ChooseAnnotatorsDialog = require('../../../../main/js/gui/admin/ChooseAnnotatorsDialog');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('ChooseAnnotatorsDialog', function () {
  function createDialog() {
    return new ChooseAnnotatorsDialog({
      element: testDiv,
      customMap: null,
      configuration: helper.getConfiguration()
    });
  }

  it('init', function () {
    var dialog = createDialog();
    assert.equal(0, logger.getWarnings().length);
    return dialog.init();
  });

  it('setElementType', function () {
    var dialog = createDialog();
    return ServerConnector.getConfiguration().then(function (configuration) {
      return dialog.setElementType(configuration.getReactionTypes()[0]);
    })
  });

  it('saveAnnotatorsInfo', function () {
    var dialog = createDialog();
    var user;
    return ServerConnector.getLoggedUser().then(function (result) {
      user = result;
      return ServerConnector.getConfiguration();
    }).then(function (configuration) {
      var elementTypes = configuration.getElementTypes();
      return dialog.saveAnnotatorsInfo(elementTypes, []);
    });
  });

  it('getAllChildrenTypesIfNeeded', function () {
    var dialog = createDialog();
    var configuration = helper.getConfiguration();
    var elementTypes = configuration.getElementTypes();
    var elementType;
    for (var i = 0; i < elementTypes.length; i++) {
      if (elementTypes[i].className === "lcsb.mapviewer.model.map.species.Protein") {
        elementType = elementTypes[i];
      }
    }
    assert.equal(1, dialog.getAllChildrenTypesIfNeeded(elementType, false).length);
    assert.ok(dialog.getAllChildrenTypesIfNeeded(elementType, true).length >= 5);
  });
});
