"use strict";

require("../../mocha-config");

var ChooseValidatorsDialog = require('../../../../main/js/gui/admin/ChooseValidatorsDialog');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('ChooseValidatorsDialog', function () {
  it('init', function () {
    var dialog = new ChooseValidatorsDialog({
      element: testDiv,
      customMap: null
    });
    assert.equal(0, logger.getWarnings().length);
    return dialog.init();
  });

  it('setElementType', function () {
    var dialog = new ChooseValidatorsDialog({
      element: testDiv,
      customMap: null
    });
    return ServerConnector.getConfiguration().then(function (configuration) {
      return dialog.setElementType(configuration.getReactionTypes()[0]);
    })
  });

});
