"use strict";

require("../../mocha-config");

var ConfigurationAdminPanel = require('../../../../main/js/gui/admin/ConfigurationAdminPanel');
var ConfigurationType = require('../../../../main/js/ConfigurationType');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

function createConfigurationTab(configuration) {
  return new ConfigurationAdminPanel({
    element: testDiv,
    configuration: configuration,
    serverConnector: ServerConnector
  });
}

describe('ConfigurationAdminPanel', function () {

  describe('init', function () {
    it('admin', function () {
      helper.loginAsAdmin();
      var mapTab;
      return ServerConnector.getConfiguration().then(function (configuration) {
        mapTab = createConfigurationTab(configuration);
        return mapTab.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok($("[name='saveOption']", testDiv).length > 0);
        assert.notOk($("[name='saveOption']", testDiv).prop('disabled'));
        return mapTab.destroy();
      });
    });
    it('no access', function () {
      helper.loginWithoutAccess();
      var mapTab;
      return ServerConnector.getConfiguration().then(function (configuration) {
        mapTab = createConfigurationTab(configuration);
        return mapTab.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.equal($("[name='saveOption']", testDiv).length, 0);
        return mapTab.destroy();
      });
    });
  });
  it('saveOption', function () {
    helper.loginAsAdmin();
    var mapTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      mapTab = createConfigurationTab(configuration);
      return mapTab.init();
    }).then(function () {
      return mapTab.saveOption(ConfigurationType.DEFAULT_MAP);
    }).then(function () {
      return mapTab.destroy();
    });
  });

});
