"use strict";

require("../../mocha-config");

var Promise = require("bluebird");

var UsersAdminPanel = require('../../../../main/js/gui/admin/UsersAdminPanel');
var SecurityError = require('../../../../main/js/SecurityError');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var assert = require('assert');

function createUserAdminPanel(configuration) {
  return new UsersAdminPanel({
    element: testDiv,
    configuration: configuration,
    serverConnector: ServerConnector
  });
}

describe('UsersAdminPanel', function () {

  describe('init', function () {
    it('default', function () {
      helper.loginAsAdmin();
      var usersTab;
      return ServerConnector.getConfiguration().then(function (configuration) {
        usersTab = createUserAdminPanel(configuration);
        return usersTab.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        return usersTab.destroy();
      });
    });
    it('user without access', function () {
      var usersTab;
      var oldFun = ServerConnector.getUsers;
      ServerConnector.getUsers = function () {
        return Promise.reject(new SecurityError("Access denied."));
      };
      return ServerConnector.getConfiguration().then(function (configuration) {
        usersTab = createUserAdminPanel(configuration);
        return usersTab.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok(usersTab.getElement().innerHTML.indexOf("no privilege") >= 0);
        return usersTab.destroy();
      }).finally(function () {
        ServerConnector.getUsers = oldFun;
      });
    });
  });

  it('refresh', function () {
    helper.loginAsAdmin();
    var usersTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      usersTab = createUserAdminPanel(configuration);
      return usersTab.init();
    }).then(function () {
      return usersTab.onRefreshClicked();
    }).then(function () {
      return usersTab.destroy();
    });
  });

  it('showEditDialog', function () {
    helper.loginAsAdmin();
    var usersTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      usersTab = createUserAdminPanel(configuration);
      return usersTab.init();
    }).then(function () {
      return usersTab.showEditDialog("anonymous");
    }).then(function () {
      return usersTab.destroy();
    });
  });
  it('onAddClicked', function () {
    helper.loginAsAdmin();
    var usersTab;
    return ServerConnector.getConfiguration().then(function (configuration) {
      usersTab = createUserAdminPanel(configuration);
      return usersTab.init();
    }).then(function () {
      return usersTab.onAddClicked();
    }).then(function () {
      return usersTab.destroy();
    });
  });


});
