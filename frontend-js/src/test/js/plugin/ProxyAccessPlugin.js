"use strict";

var Plugin = require('../../../main/js/plugin/Plugin');
var MinervaPluginProxy = require('../../../main/js/plugin/MinervaPluginProxy');

var Promise = require('bluebird');

var logger = require('../logger');

function ProxyAccessPlugin(options) {
  Plugin.call(this);
}

ProxyAccessPlugin.prototype = Object.create(Plugin.prototype);
ProxyAccessPlugin.prototype.constructor = ProxyAccessPlugin;

ProxyAccessPlugin.prototype.setOptions = function (options) {
  if (options !== undefined) {
    options.plugin = this;
    this.setMinervaPluginProxy(new MinervaPluginProxy(options));
  }
};

ProxyAccessPlugin.prototype.load = function () {
  this.setLoadedPluginData({
    getName: function () {
      return "ProxyAccessPlugin"
    },
    getVersion: function () {
      return "0.0.1";
    },
    unregister: function () {
      logger.debug("un-registering ProxyAccessPlugin");
    },
    register: function () {
      logger.debug("registering ProxyAccessPlugin");
    }
  });
  return Promise.resolve();
};
ProxyAccessPlugin.prototype.unload = function () {
  return Promise.resolve();
};
module.exports = ProxyAccessPlugin;
