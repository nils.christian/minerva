"use strict";

require("../mocha-config");

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");
var Plugin = require('../../../main/js/plugin/Plugin');

var logger = require('../logger');
var chai = require('chai');
var assert = chai.assert;

var fs = require('fs');

describe('Plugin', function () {
  it('constructor', function () {
    var plugin = new Plugin({
      url: "./testFiles/plugin/empty.js",
      configuration: helper.getConfiguration()
    });
    assert.ok(plugin);
  });

  it('test plugins', function () {
    var map = helper.createCustomMap();
    var promises = [];

    fs.readdirSync('./testFiles/plugin/').forEach(function (file) {
      var element = document.createElement("div");
      testDiv.appendChild(element);
      var plugin = new Plugin({
        url: "./testFiles/plugin/" + file,
        map: map,
        element: element,
        configuration: helper.getConfiguration()
      });
      promises.push(plugin.load());
    });
    return Promise.all(promises);
  });

  describe('load', function () {
    it('default', function () {
      var map = helper.createCustomMap();

      var plugin = new Plugin({
        url: "./testFiles/plugin/empty.js",
        map: map,
        configuration: helper.getConfiguration()
      });
      return plugin.load().then(function () {
        assert.equal("test plugin", plugin.getName());
        assert.equal("0.0.1", plugin.getVersion());
        assert.equal(0, logger.getWarnings().length);
      });
    });

    it('invalid javascript code', function () {
      var plugin = new Plugin({
        url: "./testFiles/plugin-invalid/invalid_javascript.js",
        map: helper.createCustomMap(),
        configuration: helper.getConfiguration()
      });
      return plugin.load().then(function () {
        assert.notOk("expected error");
      }, function (error) {
        assert.ok(error.message.indexOf("Unexpected identifier") >= 0, "Wrong message: " + error.message);
      });
    });

    it('plugin register crash', function () {
      var plugin = new Plugin({
        url: "./testFiles/plugin-invalid/invalid_register.js",
        map: helper.createCustomMap(),
        configuration: helper.getConfiguration()
      });
      return plugin.load().then(function () {
        assert.false("expected error");
      }, function (error) {
        assert.ok(error.message.indexOf("Let's crash") >= 0, "Wrong message: " + error.message);
      });
    });
    it('plugin register promise reject', function () {
      var plugin = new Plugin({
        url: "./testFiles/plugin-invalid/result-promise-crash.js",
        map: helper.createCustomMap(),
        configuration: helper.getConfiguration()
      });
      return plugin.load().then(function () {
        assert.false("expected error");
      }).catch( function (error) {
        assert.ok(error.message.indexOf("Let's reject") >= 0, "Wrong message: " + error.message);
      });
    });
  });
  describe('unload', function () {
    it('warning about cleaning', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var plugin = new Plugin({
        url: "./testFiles/plugin-invalid/unclean-unregister.js",
        map: map,
        configuration: helper.getConfiguration()
      });
      return plugin.load().then(function () {
        assert.equal(0, logger.getWarnings().length);
        return plugin.unload();
      }).then(function () {
        assert.equal(1, logger.getWarnings().length);
      });
    });
  });
});
