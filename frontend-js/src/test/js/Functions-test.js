"use strict";

require('./mocha-config');

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");

var functions = require('../../main/js/Functions');
var logger = require('./logger');

var assert = require('assert');

var originalNavigator = null;
describe('functions', function () {

  beforeEach(function () {
    originalNavigator = global.navigator;
  });

  afterEach(function () {
    global.navigator = originalNavigator;
  });

  it('Point inside polygon 1', function () {
    var point = {
      x: 123,
      y: 124
    };
    var polygon = [];
    polygon[0] = {
      x: 0,
      y: 0
    };
    polygon[1] = {
      x: 100,
      y: 0
    };
    polygon[2] = {
      x: 100,
      y: 100
    };
    polygon[3] = {
      x: 0,
      y: 100
    };

    assert.equal(false, functions.pointInsidePolygon(point, polygon));
  });

  it('Point inside polygon 2', function () {
    var point = {
      x: 123,
      y: 124
    };
    var polygon = [];
    polygon[0] = {
      x: 0,
      y: 0
    };
    polygon[1] = {
      x: 1000,
      y: 0
    };
    polygon[2] = {
      x: 1000,
      y: 1000
    };
    polygon[3] = {
      x: 0,
      y: 1000
    };

    assert.ok(functions.pointInsidePolygon(point, polygon));
  });

  it('Point inside polygon 3', function () {
    var point = {
      x: 20,
      y: 50
    };
    var polygon = [];
    polygon[0] = {
      x: 0,
      y: 0
    };
    polygon[1] = {
      x: 100,
      y: 0
    };
    polygon[2] = {
      x: 10,
      y: 50
    };
    polygon[3] = {
      x: 100,
      y: 100
    };
    polygon[4] = {
      x: 0,
      y: 100
    };

    assert.equal(false, functions.pointInsidePolygon(point, polygon));
  });

  it('Point inside polygon 4', function () {
    var point = {
      x: 5,
      y: 50
    };
    var polygon = [];
    polygon[0] = {
      x: 0,
      y: 0
    };
    polygon[1] = {
      x: 100,
      y: 0
    };
    polygon[2] = {
      x: 10,
      y: 50
    };
    polygon[3] = {
      x: 100,
      y: 100
    };
    polygon[4] = {
      x: 0,
      y: 100
    };

    assert.ok(functions.pointInsidePolygon(point, polygon));
  });

  it('Integer to html color', function () {
    var integer = -16711936;
    var color = functions.intToColorString(integer);
    assert.equal(7, color.length);
    assert.equal(color.charAt(0), '#');
  });

  it('isInt', function () {
    assert.ok(functions.isInt(12));
    assert.ok(functions.isInt(-12));
    assert.ok(functions.isInt(0));
    assert.equal(functions.isInt(12.1), false);
    assert.equal(functions.isInt(null), false);
    assert.equal(functions.isInt(""), false);
  });

  it('stackTrace', function () {
    assert.ok(functions.stackTrace());
  });

  it('getPosition', function () {
    var div = document.createElement('div');
    div.scrollLeft = 0;
    div.scrollTop = 0;
    var pos = functions.getPosition(div);
    assert.equal(0, pos.x);
    assert.equal(0, pos.y);
  });

  it('isDomElement', function () {
    assert.strictEqual(false, functions.isDomElement(12));
    assert.strictEqual(false, functions.isDomElement(-12));
    assert.strictEqual(false, functions.isDomElement(0));
    assert.strictEqual(false, functions.isDomElement(12.1));
    assert.strictEqual(false, functions.isDomElement(null));
    assert.strictEqual(false, functions.isDomElement(""));
    assert.strictEqual(false, functions.isDomElement({}));
    assert.strictEqual(true, functions.isDomElement(document.createElement('div')));
  });

  describe('overlayToColor', function () {
    it('positive value', function () {
      var overlay = {
        value: 0.5
      };
      return functions.overlayToColor(overlay).then(function (colorString) {
        // check the format of the html color
        assert.equal(colorString.charAt(0), "#");
        assert.equal(colorString.length, 7);
      });
    });
    it('negative value', function () {
      var overlay = {
        value: -0.5
      };
      return functions.overlayToColor(overlay).then(function (colorString) {
        // check the format of the html color
        assert.equal(colorString.charAt(0), "#");
        assert.equal(colorString.length, 7);
      });
    });
    it('invalid arg', function () {
      return functions.overlayToColor(null).then(function (colorString) {
        throw new Error('Promise was unexpectedly fulfilled. Result: ' + colorString);
      }, function rejected(error) {
        assert.ok(error.indexOf("cannot be null") >= 0);
      });
    });

    it('invalid arg 2', function () {
      return functions.overlayToColor({}).then(function (colorString) {
        assert.ok(colorString);
      });
    });
  });


  it('bound with null params', function () {
    var value = 12;
    var result = functions.bound(value, 13, null);
    assert.equal(result, 13);

    var value2 = 12;
    var result2 = functions.bound(value2, null, -1);
    assert.equal(result2, -1);
  });

  it('browser detection simulation with unsupported detection browser', function () {
    var browser = functions.browser;
    browser.init();
    assert.equal(browser.name, "Unknown");
    assert.equal(browser.version, "Unknown");
  });

  it('browser detection simulation with IE', function () {
    global.navigator = {
      appName: 'Microsoft Internet Explorer',
      userAgent: "MSIE 7.0"
    };

    var browser = functions.browser;
    browser.init();
    assert.equal(browser.name, "IE");
    assert.equal(browser.version, "7.0");
  });

  it('browser detection simulation with other', function () {
    global.navigator = {
      appName: 'Netscape',
      userAgent: "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv 11.0) like Gecko"
    };

    var browser = functions.browser;
    browser.init();
    assert.equal(browser.name, "Other");
    assert.equal(browser.version, "11.0");
    assert.equal(logger.getWarnings().length, 0);
  });

});
