"use strict";

var Promise = require("bluebird");
var Cookies = require('js-cookie');


var Helper = require('./helper');

var GuiConnector = require('./GuiConnector-mock');

var path = require('path');

// -----------------------------

var logger = require('./logger');

function removeCookies() {
  var cookies = Cookies.get();
  for (var cookie in cookies) {
    Cookies.remove(cookie);
  }
}

function mockBootstrap() {
  $.fn.typeahead = function () {
    logger.debug("Mock typeahead function call");
  };
}

before(function () {
  require('../../main/js/Functions').loadScript = function () {
    global.MathJax = {
      Hub: {
        Config: function () {
        },
        Queue: function(){

        }
      }
    };
    return Promise.resolve();
  };
  // requirejs.config({
  //   baseUrl: path.dirname(require.main.filename) + "/../../../"
  // });

// GLOBAL configuration
  global.navigator = {
    userAgent: 'node.js',
    appName: 'MinervaUnitTest',
    appVersion: '0.0.1'

  };

  var jsdom = require('jsdom');
  global.dom = new jsdom.JSDOM();
  global.window = global.dom.window;
  global.document = window.document;
  global.document.elementFromPoint = function () {
  };

  global.$ = require('jquery');
  global.jQuery = $;
  global.window.$ = $;

// additions to jsdom implementations:
  global.Option = window.Option;
  global.Blob = window.Blob;
  global.MouseEvent = window.MouseEvent;
  global.FileReader = window.FileReader;

  global.getComputedStyle = window.getComputedStyle;
  global.Image = window.Image;

  window.open = function () {
    var result = {};
    result.focus = function () {

    };
    return result;
  };
  window.URL.createObjectURL = function () {

  };

  //CANVAS mocking
  window.HTMLCanvasElement.prototype.getContext = function () {
    return {
      fillRect: function () {
      },
      clearRect: function () {
      },
      getImageData: function (x, y, w, h) {
        return {
          data: new Array(w * h * 4)
        };
      },
      putImageData: function () {
      },
      createImageData: function () {
        return []
      },
      setTransform: function () {
      },
      drawImage: function () {
      },
      save: function () {
      },
      fillText: function () {
      },
      restore: function () {
      },
      beginPath: function () {
      },
      moveTo: function () {
      },
      lineTo: function () {
      },
      closePath: function () {
      },
      stroke: function () {
      },
      translate: function () {
      },
      scale: function () {
      },
      rotate: function () {
      },
      arc: function () {
      },
      fill: function () {
      },
      measureText: function () {
        return {width: 0};
      },
      transform: function () {
      },
      rect: function () {
      },
      clip: function () {
      },
      canvas: document.createElement("div")
    };
  };

  window.HTMLCanvasElement.prototype.toDataURL = function () {
    return "";
  };

  //open layers uses this function
  global.requestAnimationFrame = function () {
  };
  global.CanvasPattern = function(){

  };
  global.CanvasGradient = function(){

  };

// pileup is using heavily some browser defined javascript
  var pileup = require('pileup');
  pileup.create = function () {
    return {
      destroy: function () {
      }
    };
  };
  pileup.formats.twoBit = function () {
    return {};
  };
  pileup.formats.bigBed = function () {
    return {};
  };
// ---
  // MolArt is inproperly packed
  global.MolArt = function () {

  };
// ---

  require('jquery-ui-dist/jquery-ui.js');

  require("bootstrap");

  require('datatables.net')(window, $);
  require('datatables.net-rowreorder')(window, $);
  require('spectrum-colorpicker');
  global.tinycolor = window.tinycolor;
  require('jstree');

  global.google = require('./google-map-mock');

  global.ServerConnector = require('./ServerConnector-mock');

  Promise.longStackTraces();

  mockBootstrap();
});

beforeEach(function () {
  window.onresize = undefined;

  logger.flushBuffer();

  removeCookies();

  ServerConnector.init();

  ServerConnector.getSessionData(null).setToken("MOCK_TOKEN_ID");
  ServerConnector.getSessionData(null).setLogin("anonymous");


  // @type HTMLElement
  global.testDiv = document.createElement("div");
  global.testDiv.id = "test";
  document.body.appendChild(testDiv);

  return ServerConnector.getConfiguration().then(function (configuration) {
    global.helper = new Helper(configuration);
    helper.setUrl("http://test/");
    GuiConnector.init();
  }).catch(function (error) {
    logger.error(error);
    throw error;
  });
});

afterEach(function () {
  GuiConnector.destroy();
  document.body.removeChild(global.testDiv);
  delete global.testDiv;
  if (this.currentTest.state !== 'failed') {
    if (document.body.hasChildNodes()) {
      var content = document.body.innerHTML;
      document.body.innerHTML = "";
      this.test.error(new Error("Test didn't left clean document. Found: " + content));
    } else if ($._data(window, "events").resize) {
      $(window).off("resize");
      this.test.error(new Error("Test didn't left clean resize events handlers."));
    }
  } else {
    //when failed don't forget to clean events
    if ($._data(window, "events").resize) {
      logger.warn("Clearing events: " + $._data(window, "events").resize);
      $(window).off("resize");
    }
    if (document.body.hasChildNodes()) {
      logger.warn("Clearing html");
      document.body.innerHTML = "";
    }
  }
});
