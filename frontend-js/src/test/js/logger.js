"use strict";

var log4js = require('log4js');

var $depth = 10;

log4js.configure({
  appenders : [ {
    type : "console",
    layout : {
      type : "pattern",
      pattern : "%[%p {%x{ln}} -%]\t%m",
      tokens : {
        ln : function() {
          // The caller:
          var filePath = (new Error()).stack.split("\n")[$depth].split("\\");
          filePath = filePath[filePath.length - 1].split("/");
          return filePath[filePath.length - 1];
        }
      }
    }
  } ]
});
var appender = require('log4js-memory-appender');

var memAppender = appender({
  maxBufferSize : 1000
});

log4js.loadAppender('memory', memAppender);
log4js.addAppender(log4js.appenders.memory());

var logger = log4js.getLogger();
logger.getEvents = function() {
  return memAppender.getBuffer();
};
logger.getErrors = function() {
  var result = [];
  for (var i = 0; i < memAppender.getBuffer().length; i++) {
    var message = memAppender.getBuffer()[i];
    if (message.indexOf("[ERROR]") !== -1) {
      result.push(message);
    }
  }
  return result;
};
logger.getWarnings = function() {
  var result = [];
  for (var i = 0; i < memAppender.getBuffer().length; i++) {
    var message = memAppender.getBuffer()[i];
    if (message.indexOf("[WARN]") !== -1) {
      result.push(message);
    }
  }
  return result;
};
logger.flushBuffer = function() {
  return memAppender.flushBuffer();
};

module.exports = logger;
