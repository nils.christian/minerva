"use strict";

require("./mocha-config.js");

var SessionData = require('../../main/js/SessionData');

var chai = require('chai');
var assert = chai.assert;
var logger = require('./logger');

describe('SessionData', function() {
  it('setShowComments', function() {
    var project = helper.createProject();
    var session = new SessionData(project);
    session.setShowComments(true);
    assert.ok(session.getShowComments());
    session.setShowComments(false);
    assert.notOk(session.getShowComments());
    assert.equal(logger.getWarnings().length, 0);
  });
  
  it('setVisibleOverlays', function() {
    var project = helper.createProject();
    var session = new SessionData(project);
    
    var overlays = session.getVisibleOverlays();
    
    assert.ok(overlays);
    assert.equal(overlays.length, 0);
    
    session.setVisibleOverlays([1,3]);
    overlays = session.getVisibleOverlays();
    
    assert.ok(overlays);
    assert.equal(overlays.length, 2);
    assert.equal(overlays[0],1);
    assert.equal(overlays[1],3);
  });
});
