"use strict";
require("../mocha-config");

var CustomMapOptions = require('../../../main/js/map/CustomMapOptions');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('CustomMapOptions', function () {
  describe("constructor", function () {
    it("with invalid arg", function () {
      try {
        new CustomMapOptions({
          bigLogo: "some strange string",
          hideDiv: "some div ide",
          debug: "unk boolean"
        });
        assert.ok(false);
      } catch (exception) {
        assert.ok(exception.message.indexOf("element must be defined") >= 0);
      }
    });
    it("with invalid arg 3", function () {
      try {
        new CustomMapOptions({
          element: testDiv
        });
      } catch (exception) {
        assert.ok(exception.message.indexOf("project must be defined") >= 0);
      }
    });

    it("with valid arg 2", function () {
      var project = helper.createProject();
      new CustomMapOptions({
        element: testDiv,
        project: project,
        debug: false
      });

      assert.equal(0, logger.getErrors().length);
      assert.equal(0, logger.getWarnings().length);
    });

  });

  it("getDebug", function () {
    var project = helper.createProject();
    var options = new CustomMapOptions({
      element: testDiv,
      project: project,
      debug: true
    });

    assert.ok(options.isDebug());
  });
});
