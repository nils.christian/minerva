"use strict";

require("../../mocha-config");
var logger = require('../../logger');

var AliasSurface = require('../../../../main/js/map/surface/AliasSurface');
var ServerConnector = require('../../ServerConnector-mock');

var assert = require('assert');

describe('AliasSurface', function () {
  describe("init", function () {
    it("default", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var layoutAlias = helper.createLayoutAlias(alias);

      var result = new AliasSurface({
        overlayAlias: layoutAlias,
        alias: alias,
        map: map,
        startX: 1,
        endX: 2
      });
      return result.init().then(function () {
        assert.equal(logger.getWarnings.length, 0);
        assert.ok(result.getIdentifiedElement());
      });
    });
    it("no bounds", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var layoutAlias = helper.createLayoutAlias(alias);

      var result = new AliasSurface({
        overlayAlias: layoutAlias,
        alias: alias,
        map: map
      });
      return result.init().then(function () {
        assert.equal(0, result.getStartX());
        assert.equal(1, result.getEndX());
      });
    });
  });

  describe("setBoundsForAlias", function () {
    it("from simple element", function () {
      var map;
      var alias, surface;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        return map.getModel().getAliasById(329171);
      }).then(function (result) {
        alias = result;
        surface = new AliasSurface({
          alias: result,
          map: map
        });
        return surface.init();
      }).then(function () {
        var bounds = surface.getBounds();
        surface.setBoundsForAlias(1, 3);
        var bounds2 = surface.getBounds();
        assert.equal(bounds.getRightBottom().y, bounds2.getRightBottom().y);
        assert.ok(bounds.getRightBottom().x !== bounds2.getRightBottom().x);
      });
    });

    it("from element with data overlay", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var surface = new AliasSurface({
        overlayAlias: helper.createLayoutAlias(alias),
        alias: alias,
        map: map,
        startX: 1,
        endX: 2
      });
      return surface.init().then(function () {
        return surface.setBoundsForAlias(0, 1);
      }).then(function () {
        assert.ok(surface.getBounds());
      });
    });
  });

  describe('click', function () {
    it("default", function () {
      var map;
      var alias, surface;
      var clicked = false;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        return map.getModel().getAliasById(329171);
      }).then(function (result) {
        alias = result;
        surface = new AliasSurface({
          alias: result,
          map: map,
          onClick: function () {
            clicked = true;
          }
        });
        return surface.init();
      }).then(function () {
        return surface.onClickHandler();
      }).then(function () {
        assert.ok(clicked);
      });
    });
  });
});
