"use strict";
require("../../mocha-config");
var ServerConnector = require('../../ServerConnector-mock');

var Promise = require("bluebird");

var functions = require('../../../../main/js/Functions');

var Alias = require('../../../../main/js/map/data/Alias');
var LayoutAlias = require('../../../../main/js/map/data/LayoutAlias');
var Drug = require('../../../../main/js/map/data/Drug');
var GeneVariant = require('../../../../main/js/map/data/GeneVariant');
var AliasInfoWindow = require('../../../../main/js/map/window/AliasInfoWindow');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var DataOverlay = require('../../../../main/js/map/data/DataOverlay');

var assert = require('assert');

var logger = require('../../logger');

describe('AliasInfoWindow', function () {
  describe('constructor', function () {
    it("default", function () {
      var map = helper.createCustomMap();

      var alias = helper.createAlias(map);
      alias.setIsComplete(false);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      assert.equal(alias, aliasWindow.getAlias());
      assert.equal(map, aliasWindow.getCustomMap());
      assert.ok(aliasWindow.getContent().innerHTML.indexOf("loading") >= 0);
      assert.equal(logger.getWarnings().length, 0);
    });
    it("loading data", function () {
      var map = helper.createCustomMap();
      map.getOverlayDataForAlias = function () {
        return Promise.resolve([]);
      };

      var javaObject = {
        bounds: {
          x: 190,
          y: 44,
          width: 80,
          height: 40
        },
        modelId: map.getId(),
        idObject: 30001
      };
      var alias = new Alias(javaObject);
      map.getModel().addAlias(alias);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      assert.equal(alias, aliasWindow.alias);
      assert.equal(map, aliasWindow.customMap);
      assert.ok(aliasWindow.getContent().innerHTML.indexOf("loading") > -1);
    });
  });

  it("createOverlayInfoDiv", function () {
    var map = helper.createCustomMap();

    var oc = helper.createDrugDbOverlay(map);

    var alias = helper.createAlias(map);
    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    oc.searchNamesByTarget = function () {
      return Promise.resolve(["xField"]);
    };
    oc.getElementsByQueryFromServer = function () {
      return Promise.resolve([new Drug({
        name: "xField",
        references: [],
        targets: []
      })]);
    };
    return aliasWindow.init().then(function () {
      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement(alias), true);
    }).then(function (data) {
      var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
      assert.ok(functions.isDomElement(overlayDiv));
      assert.ok(overlayDiv.innerHTML.indexOf('xField') >= 0);
    });

  });

  it("createDrugOverlayInfoDiv", function () {
    helper.setUrl("http://test/?id=drug_target_sample");

    var map, ie, aliasWindow, oc;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      oc = helper.createDrugDbOverlay(map);

      ie = new IdentifiedElement({
        id: 436152,
        modelId: map.getId(),
        type: "ALIAS"
      });
      return map.getModel().getByIdentifiedElement(ie, true);
    }).then(function (alias) {
      aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });
      return aliasWindow.init()
    }).then(function () {
      return oc.getDetailDataByIdentifiedElement(ie, true);
    }).then(function (data) {
      var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
      assert.ok(functions.isDomElement(overlayDiv));
      assert.ok(overlayDiv.innerHTML.indexOf('NADH') >= 0);
    });
  });

  it("createChemicalOverlayInfoDiv", function () {
    var map, ie, aliasWindow, oc;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      oc = helper.createChemicalDbOverlay(map);

      ie = new IdentifiedElement({
        id: 329170,
        modelId: map.getId(),
        type: "ALIAS"
      });

      return map.getModel().getByIdentifiedElement(ie, true);
    }).then(function (alias) {
      aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });
      return oc.getDetailDataByIdentifiedElement(ie, true);
    }).then(function (data) {
      var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
      assert.ok(functions.isDomElement(overlayDiv));
    });
  });

  describe("createOverlayInfoDiv", function () {
    it("for comment", function () {
      var map = helper.createCustomMap();

      var oc = helper.createCommentDbOverlay(map);

      var alias = helper.createAlias();
      alias.setId(329157);
      alias.setIsComplete(true);
      alias.setModelId(map.getId());
      map.getModel().addAlias(alias);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement(alias), true).then(function (data) {
        var comment = helper.createComment(alias);
        comment.setContent("test comment Content");

        data[0] = comment;
        data['__FULL__'] = null;

        var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);

        assert.ok(functions.isDomElement(overlayDiv));
        assert.ok(overlayDiv.innerHTML.indexOf(comment.getContent()) >= 0);
        assert.ok(overlayDiv.innerHTML.indexOf(comment.getId()) >= 0);
      });

    });
    it("xss", function () {
      var map = helper.createCustomMap();

      var oc = helper.createCommentDbOverlay(map);

      var alias = helper.createAlias(map);
      alias.setId(329157);
      alias.setIsComplete(true);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement(alias), true).then(function (data) {
        var comment = helper.createComment(alias);
        // noinspection HtmlUnknownTarget
        comment.setContent("<img id=\"xss-id\" src=\"invalid/path\" onerror='alert(\"XSS test\")' />");

        data[0] = comment;
        data['__FULL__'] = null;

        var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
        assert.ok(overlayDiv.innerHTML.indexOf("alert") === -1);
      });

    });
  });

  describe("createGeneticsDiv", function () {
    it("on top map", function () {
      var map;
      var overlay;

      var layoutAlias;
      var win;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        overlay = new DataOverlay(18077, "xxx");
        return overlay.init();
      }).then(function () {
        return overlay.getFullAliasById(overlay.getAliases()[0].getId());
      }).then(function (data) {
        layoutAlias = data;
        return map.getModel().getAliasById(layoutAlias.getId());
      }).then(function (alias) {
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        win.layoutAliases = [layoutAlias];
        win.layoutNames = ["xxx"];
        return win.createGenomicDiv();
      }).then(function (div) {
        assert.ok(div);
        assert.ok(div.innerHTML.indexOf("No reference genome data available on minerva platform") === -1);
        win.destroy();
      });

    });

    it("on submap map", function () {
      helper.setUrl("http://test/?id=complex_model_with_submaps");

      var map, submap, overlay, win, alias;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        submap = map.getSubmapById(16731);
        overlay = new DataOverlay(18077, "xxx");
        overlay.setInitialized(true);
        return map.openSubmap(submap.getId());
      }).then(function () {
        return submap.getModel().getAliasById(345337);
      }).then(function (data) {
        alias = data;
        win = new AliasInfoWindow({
          alias: alias,
          map: submap,
          marker: helper.createMarker({element: alias, map: submap})
        });
        return win.init();
      }).then(function () {
        var layoutAlias = helper.createLayoutAlias(alias);
        layoutAlias.setType(LayoutAlias.GENETIC_VARIANT);
        win.layoutAliases = [layoutAlias];
        win.layoutNames = ["xxx"];
        return win.createGenomicDiv();
      }).then(function (div) {
        assert.ok(div);
        win.destroy();
        return map.destroy();
      });

    });

    it("with no genetic data", function () {
      var map;

      var win;
      var aliasId = 329173;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var overlay = new DataOverlay(18077, "xxx");
        return overlay.init();
      }).then(function () {
        return map.getModel().getAliasById(aliasId);
      }).then(function (alias) {
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        win.layoutAliases = [undefined];
        return win.createGenomicDiv();
      }).then(function (div) {
        assert.ok(div);
        assert.ok(div.innerHTML.indexOf("No reference genome data available on minerva platform") === -1);
        win.destroy();
      });

    });

    it("for unknown organism", function () {
      var map;
      var overlay;

      var layoutAlias;
      var win;

      return ServerConnector.getProject().then(function (project) {
        project.setOrganism({
          type: "TAXONOMY",
          resource: "123456"
        });
        map = helper.createCustomMap(project);
        overlay = new DataOverlay(18077, "xxx");
        return overlay.init();
      }).then(function () {
        return overlay.getFullAliasById(overlay.getAliases()[0].getId());
      }).then(function (data) {
        layoutAlias = data;
        return map.getModel().getAliasById(layoutAlias.getId());
      }).then(function (alias) {
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        win.layoutAliases = [layoutAlias];
        return win.createGenomicDiv();
      }).then(function (div) {
        assert.ok(div);
        assert.ok(div.innerHTML.indexOf("No reference genome data available on minerva platform") >= -1);
        win.destroy();
      });

    });
  });

  it("createWaitingContentDiv", function () {
    var map = helper.createCustomMap();
    var alias = helper.createAlias(map);
    alias.setIsComplete(true);

    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    assert.ok(functions.isDomElement(aliasWindow.createWaitingContentDiv()));
  });

  it("createChartDiv ", function () {
    var map, ie, aliasWindow;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      ie = new IdentifiedElement({
        id: 329170,
        modelId: map.getId(),
        type: "ALIAS"
      });

      return map.getModel().getByIdentifiedElement(ie, true);
    }).then(function (alias) {
      aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });
      aliasWindow.layoutAliases = [helper.createLayoutAlias(alias), null];
      aliasWindow.layoutNames = ["x", "y"];
      return aliasWindow.createChartDiv();
    }).then(function (div) {
      assert.ok(div);
    });
  });

  it("createVcfString ", function () {
    var map = helper.createCustomMap();
    var alias = helper.createAlias(map);
    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    var nullVal = null;

    var variant = new GeneVariant({
      contig: "11",
      modifiedDna: "T",
      originalDna: "C",
      position: 233067,
      referenceGenomeType: "UCSC",
      referenceGenomeVersion: "hg19",
      allelFrequency: nullVal,
      variantIdentifier: nullVal
    });
    var variant2 = new GeneVariant({
      contig: "11",
      modifiedDna: "T",
      originalDna: "C",
      position: 233068,
      referenceGenomeType: "UCSC",
      referenceGenomeVersion: "hg19"
    });
    var str = aliasWindow.createVcfString([variant, variant2]);
    assert.ok(str.indexOf("null") === -1, "null shouldn't appear in vcf format");
  });

});
