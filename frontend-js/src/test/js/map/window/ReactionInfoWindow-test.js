"use strict";
require("../../mocha-config");
// noinspection JSUnusedLocalSymbols
var ServerConnector = require('../../ServerConnector-mock');

var KineticLaw = require('../../../../main/js/map/data/KineticLaw');
var Point = require('../../../../main/js/map/canvas/Point');
var ReactionInfoWindow = require('../../../../main/js/map/window/ReactionInfoWindow');

var chai = require('chai');
var assert = chai.assert;

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

describe('ReactionInfoWindow', function () {
  describe('constructor', function () {
    it("default", function () {
      var map = helper.createCustomMap();

      var reaction = helper.createReaction(map);

      var reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map
      });

      assert.equal(reaction, reactionWindow.getReactionData());
    });
  });

  describe('open', function () {
    it("default", function () {
      var reactionWindow;

      var map = helper.createCustomMap();

      var reaction = helper.createReaction(map, true);

      var marker = map.getMapCanvas().createMarker({position: new Point(0, 0), id: "1", icon: "temp.png"});
      reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map,
        marker: marker
      });
      return reactionWindow.init().then(function () {
        return reactionWindow.open();
      })
    });
  });

  describe('createContentDiv', function () {
    it("with invalid kinetics", function () {
      var map = helper.createCustomMap();

      var reaction = helper.createReaction(map, true);
      reaction.setKineticLaw(new KineticLaw({
        definition: '<math xmlns="http://www.w3.org/1998/Math/MathML">' +
        '<apply xmlns="http://www.w3.org/1998/Math/MathML">' +
        '<divide/><apply><times/><ci>ca1</ci><apply><plus/><ci>sa1</ci><ci>sa2</ci></apply></apply><cn type=\"integer\"> 2 </cn>' +
        '</apply></math>',
        functionIds: [],
        parameterIds: []
      }));
      var reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map
      });

      return reactionWindow.createContentDiv().then(function (div) {
        assert.ok(div.innerHTML.indexOf("Problematic MathML") >= 0);
      });
    });
    it("with kinetics", function () {
      var map = helper.createCustomMap();

      var reaction = helper.createReaction(map, true);
      reaction.setKineticLaw(new KineticLaw({
        definition: '<math xmlns="http://www.w3.org/1998/Math/MathML">' +
        '<apply xmlns="http://www.w3.org/1998/Math/MathML">' +
        '<divide/><apply><times/><ci>ca1</ci><apply><plus/><ci>sa1</ci><ci>sa2</ci></apply></apply><cn type=\"integer\"> 2 </cn>' +
        '</apply></math>',
        mathMlPrenestation: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n" +
        "<mrow><mfrac><mrow><mrow><mi> c1 </mi><mo>⁢</mo><mfenced separators=\"\"><mrow><mi> s1 </mi><mo>+</mo><mi> s2 </mi></mrow></mfenced></mrow></mrow><mrow><mn> 2 </mn></mrow></mfrac></mrow>\n" +
        "</math>\n",
        functionIds: [],
        parameterIds: []
      }));
      var reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map
      });

      return reactionWindow.createContentDiv().then(function (div) {
        assert.ok(div.innerHTML.indexOf("Problematic MathML") >= 0);
        assert.notOk(div.innerHTML.indexOf("apply") >= 0);
      });
    });

    it("with kinetic function", function () {
      var map = helper.createCustomMap();
      var sbmlFunction = helper.createSbmlFunction();
      map.getModel().addSbmlFunction(sbmlFunction);

      var reaction = helper.createReaction(map, true);
      reaction.setKineticLaw(helper.createKineticLaw());
      reaction.getKineticLaw().getFunctionIds().push(sbmlFunction.getId());

      var reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map
      });

      return reactionWindow.createContentDiv().then(function (div) {
        assert.ok(div.innerHTML.indexOf("Functions") >= 0);
        assert.ok(div.innerHTML.indexOf(sbmlFunction.getName()) >= 0, "Function name doesn't exist in the output div");
        assert.ok(div.innerHTML.indexOf(sbmlFunction.getArguments()[0]) >= 0, "Function argument doesn't exist in the output div");
      });
    });
    it("with kinetic parameters", function () {
      var map = helper.createCustomMap();
      var sbmlParameter = helper.createSbmlParameter();
      map.getModel().addSbmlParameter(sbmlParameter);

      var reaction = helper.createReaction(map, true);
      reaction.setKineticLaw(helper.createKineticLaw());
      reaction.getKineticLaw().getParameterIds().push(sbmlParameter.getId());

      var reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map
      });

      return reactionWindow.createContentDiv().then(function (div) {
        assert.ok(div.innerHTML.indexOf("Parameters") >= 0);
        assert.ok(div.innerHTML.indexOf(sbmlParameter.getName()) >= 0, "Parameter name doesn't exist in the output div");
        assert.ok(div.innerHTML.indexOf(sbmlParameter.getValue()[0]) >= 0, "Parameter value doesn't exist in the output div");
      });
    });
    it("elements", function () {
      var map = helper.createCustomMap();

      var reaction = helper.createReaction(map, true);
      reaction.setKineticLaw(helper.createKineticLaw());

      var reactionWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: map
      });

      return reactionWindow.createContentDiv().then(function (div) {
        assert.ok(div.innerHTML.indexOf("Elements") >= 0);
        assert.ok(div.innerHTML.indexOf(reaction.getElements()[0].getName()) >= 0, "Element name doesn't exist in the output div");
      });
    });
  });
});
