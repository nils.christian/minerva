"use strict";
require("../../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var GoogleMapsApiCanvas = require('../../../../../main/js/map/canvas/GoogleMaps/GoogleMapsApiCanvas');
var Point = require('../../../../../main/js/map/canvas/Point');
var Bounds = require('../../../../../main/js/map/canvas/Bounds');

var SelectionContextMenu = require('../../../../../main/js/gui/SelectionContextMenu');

var chai = require('chai');
var assert = chai.assert;

describe('GoogleMapsApiCanvas', function () {
  var testOptions = {
    center: new Point(0, 0),
    tileSize: 256,
    width: 300,
    height: 600,
    minZoom: 2,
    zoom: 5,
    backgroundOverlays: [{
      id: 1,
      name: "overlay",
      directory: "overlay_dir"
    }]
  };
  it("constructor", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);
    assert.ok(canvas.pixelOrigin_);
    assert.ok(canvas.pixelsPerLonDegree_);
    assert.ok(canvas.pixelsPerLonRadian_);
    assert.ok(canvas.zoomFactor);

  });

  it("centerButton", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);
    return canvas.getGoogleMap().controls[google.maps.ControlPosition.RIGHT_TOP][0].firstChild.onclick();
  });

  it("latLngToTile", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

    var latLng = new google.maps.LatLng(0, 1);

    var tileCoordinates = canvas.latLngToTile(latLng, 3);
    assert.ok(tileCoordinates);
    assert.ok(tileCoordinates instanceof Point);

  });

  describe("createMapTypeOption", function () {
    it("default", function () {
      var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

      var overlayDataObj = testOptions.backgroundOverlays[0];

      var result = canvas.createMapTypeOption(overlayDataObj);

      assert.ok(result);

    });
    it("getTileUrl", function () {
      var coordinates = {x: 0, y: 0};
      var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

      var overlayDataObj = testOptions.backgroundOverlays[0];

      var typeOptions = canvas.createMapTypeOption(overlayDataObj);
      var url = typeOptions.getTileUrl(coordinates, 3);
      assert.ok(url);
      assert.ok(url.indexOf(coordinates.x) >= 0);
      assert.ok(url.indexOf(coordinates.y) >= 0);
    });
  });


  it("Point - Lat,Lng conversion", function () {

    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

    var x = 199;
    var y = 110;
    var point = new Point(x, y);

    var coordinates = canvas.fromPointToLatLng(point);

    var point2 = canvas.fromLatLngToPoint(coordinates);

    assert.closeTo(point2.x, point.x, 0.5, "X coordinate is invalid after transformation");
    assert.closeTo(point2.y, point.y, 0.5, "Y coordinate is invalid after transformation");

  });

  it("create polygon through drawingManager", function () {
    var project = helper.createProject({mapCanvasType: "GOOGLE_MAPS_API"});
    var map = helper.createCustomMap(project);
    var canvas = map.getMapCanvas();

    map.setSelectionContextMenu(new SelectionContextMenu({
      customMap: map,
      element: testDiv
    }));

    canvas.turnOnDrawing();

    var triangleCoordinates = [new google.maps.LatLng(25.774, -80.190), new google.maps.LatLng(18.466, -66.118),
      new google.maps.LatLng(32.321, -64.757), new google.maps.LatLng(25.774, -80.190)];

    // Construct the polygon.
    var bermudaTriangle = new google.maps.Polygon({
      paths: triangleCoordinates,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35
    });
    bermudaTriangle.setMap(canvas.getGoogleMap());

    var eventParam = {
      type: google.maps.drawing.OverlayType.POLYGON,
      overlay: bermudaTriangle
    };

    // check creation complete behaviour
    assert.ok(canvas.isDrawingOn());
    assert.equal(google.maps.drawing.OverlayType.POLYGON, canvas._drawingManager.getDrawingMode());
    google.maps.event.trigger(canvas._drawingManager, 'overlaycomplete', eventParam);
    assert.ok(canvas.isDrawingOn());
    assert.equal(null, canvas._drawingManager.getDrawingMode());

    // and now check how it behaves when we click on the polygon
    var clickEventParam = {
      stop: null,
      latLng: new google.maps.LatLng(40.0, -90.0)
    };

    assert.notOk(map.getSelectedPolygon());

    assert.notOk(map.getActiveSubmapId());
    assert.notOk(canvas.getSelectedArea());
    google.maps.event.trigger(bermudaTriangle, 'rightclick', clickEventParam);
    assert.ok(canvas.getSelectedArea());
    assert.equal(map.getId(), map.getActiveSubmapId());

    assert.ok(map.getSelectedPolygon());

  });

  it("toggleDrawing", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

    assert.equal(canvas.isDrawingOn(), false);
    canvas.toggleDrawing();
    assert.ok(canvas.isDrawingOn());
    canvas.toggleDrawing();
    assert.equal(canvas.isDrawingOn(), false);
  });

  it("turnOnOff", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

    assert.equal(canvas.isDrawingOn(), false);
    canvas.turnOnDrawing();
    assert.ok(canvas.isDrawingOn());
    canvas.turnOffDrawing();
    assert.equal(canvas.isDrawingOn(), false);
  });

  it("areaToString", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);

    var triangleCoordinates = [new google.maps.LatLng(25.774, -80.190), new google.maps.LatLng(18.466, -66.118),
      new google.maps.LatLng(32.321, -64.757), new google.maps.LatLng(25.774, -80.190)];

    // Construct the polygon.
    var bermudaTriangle = new google.maps.Polygon({
      paths: triangleCoordinates,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35
    });

    assert.ok(canvas.areaToString(bermudaTriangle));

  });

  it("removeSelection", function () {
    var project = helper.createProject({mapCanvasType: "GOOGLE_MAPS_API"});
    var map = helper.createCustomMap(project);
    map.setActiveSubmapId(map.getId());
    map.getMapCanvas().setSelectedArea(new google.maps.Rectangle({}));

    map.removeSelection();
    assert.equal(map.getMapCanvas().getSelectedArea(), null);
  });

  it("click on rectangle", function () {
    var canvas = new GoogleMapsApiCanvas(testDiv, testOptions);
    var rectangle = canvas.createRectangle({
      fillOpacity: 0.7,
      strokeColor: "#330000",
      strokeOpacity: 0.5,
      strokeWeight: 1.0,
      id:"1",
      fillColor: "#00FF00",
      bounds: new Bounds(new Point(0,0), new Point(2,2))
    });
    return google.maps.event.trigger(rectangle.getGoogleRectangle(), "click");
  });

});
