"use strict";
require("../../../mocha-config");

var logger = require('../../../logger');

var GoogleMapsApiInfoWindow = require('../../../../../main/js/map/canvas/GoogleMaps/GoogleMapsApiInfoWindow');
var Point = require('../../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('GoogleMapsApiInfoWindow', function () {

  it("setMarker", function () {
    var canvas = helper.createGoogleCanvas();

    var marker = canvas.createMarker({position: new Point(0,0)});
    var infoWindow = canvas.createInfoWindow({position: new Point(0,0)});

    infoWindow.setMarker(marker);
  });
});
