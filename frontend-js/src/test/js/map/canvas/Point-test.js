"use strict";
require("../../mocha-config");

var logger = require('../../logger');

var Point = require('../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('Point', function () {
  describe("constructor", function () {
    it("from coordinates", function () {
      var point = new Point(12, 3);
      assert.equal(12, point.x);
      assert.equal(3, point.y);
    });
    it("from point", function () {
      var point = new Point(12, 3);
      var point2 = new Point(point);
      assert.equal(point.x, point2.x);
      assert.equal(point.y, point2.y);
    });
    it("from dictionary", function () {
      var point = {x: 12, y: 3};
      var point2 = new Point(point);
      assert.equal(point.x, point2.x);
      assert.equal(point.y, point2.y);
    });
  });

});
