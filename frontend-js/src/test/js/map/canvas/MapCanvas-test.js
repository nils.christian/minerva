"use strict";
require("../../mocha-config");

var MapCanvas = require('../../../../main/js/map/canvas/MapCanvas');

var chai = require('chai');
var assert = chai.assert;

describe('MapCanvas', function () {
  it("test existence of abstract methods", function () {

    var canvas = new MapCanvas(testDiv, {
      tileSize: 256,
      minZoom: 2,
      height: 600,
      width: 800
    });
    assert.ok(canvas.createMarker);
    assert.ok(canvas.createInfoWindow);
    assert.ok(canvas.createRectangle);
    assert.ok(canvas.createPolyline);
    assert.ok(canvas.addLeftBottomControl);
    assert.ok(canvas.addRightBottomControl);
    assert.ok(canvas.fitBounds);
    assert.ok(canvas.setCenter);
    assert.ok(canvas.getCenter);
    assert.ok(canvas.getZoom);
    assert.ok(canvas.getBackgroundId);
    assert.ok(canvas.getSelectedArea);
    assert.ok(canvas.getBounds);
    assert.ok(canvas.setSelectedArea);
    assert.ok(canvas.removeSelection);
    assert.ok(canvas.triggerListeners);
  });

});
