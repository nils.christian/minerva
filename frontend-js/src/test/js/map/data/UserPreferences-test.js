"use strict";

require("../../mocha-config");

var UserPreferences = require('../../../../main/js/map/data/UserPreferences');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('UserPreferences', function () {
  it("constructor", function () {
    return helper.readFile("testFiles/preferences.json").then(function (content) {
      var object = new UserPreferences(JSON.parse(content));
      assert.ok(object);
      assert.notOk(object.getProjectUpload().autoResize);
      assert.ok(object.getProjectUpload().cacheData);

      assert.ok(object.getElementAnnotators("lcsb.mapviewer.model.map.species.Protein").length > 0);
      assert.ok(object.getElementValidAnnotations("lcsb.mapviewer.model.map.species.Protein").length > 0);
      assert.ok(object.getElementRequiredAnnotations("lcsb.mapviewer.model.map.species.Protein").requiredAtLeastOnce);
      assert.ok(object.getElementRequiredAnnotations("lcsb.mapviewer.model.map.species.Protein").list.length > 0);
      assert.equal(0, logger.getWarnings().length);
    });
  });

  describe("getGuiPreference", function () {
    it("non existing", function () {
      var object = new UserPreferences();
      assert.ok(object.getGuiPreference("test") === undefined);
    });
    it("with default value", function () {
      var object = new UserPreferences();
      assert.ok(object.getGuiPreference("test", 'val') === 'val');
    });
  });
  it("setGuiPreference", function () {
    var object = new UserPreferences();
    object.setGuiPreference("test", "val");
    assert.ok(object.getGuiPreference("test") === "val");
  });
});
