"use strict";

require("../../mocha-config");

var User = require('../../../../main/js/map/data/User');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('Project', function () {
  describe("constructor", function () {
    it("empty data", function () {
      var user = new User({});
      assert.ok(user);

      assert.equal(logger.getWarnings().length, 0);
    });
  });

  describe("setPrivilege", function () {
    it("non existing data", function () {
      var user = new User({});
      assert.ok(user);

      return ServerConnector.getConfiguration().then(function (configuration) {
        var privilegeType = configuration.getPrivilegeTypes()[0];
        user.setPrivilege({type: privilegeType, value: true});
        assert.ok(user.hasPrivilege(privilegeType));
      });
    });

    it("existing data", function () {
      var user = new User({});
      assert.ok(user);

      return ServerConnector.getConfiguration().then(function (configuration) {
        var privilegeType = configuration.getPrivilegeTypes()[0];
        user.setPrivilege({type: privilegeType, value: false});
        user.setPrivilege({type: privilegeType, value: true});
        assert.ok(user.hasPrivilege(privilegeType));
      });
    });

    it("undefined value", function () {
      var user = new User({});
      assert.ok(user);

      return ServerConnector.getConfiguration().then(function (configuration) {
        var privilegeType = configuration.getPrivilegeTypes()[0];
        user.setPrivilege({type: privilegeType, value: false});
        user.setPrivilege({type: privilegeType});
        assert.ok(user.hasPrivilege(privilegeType));
      });
    });
  });
});
