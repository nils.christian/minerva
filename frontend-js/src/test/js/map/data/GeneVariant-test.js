"use strict";

var GeneVariant = require('../../../../main/js/map/data/GeneVariant');
var assert = require('assert');

describe('GeneVariant', function () {
  it("constructor", function () {
    var data = {
      position: 12,
      originalDna: "A",
      modifiedDna: "C",
      referenceGenomeType: "unk",
      referenceGenomeVersion: "v1",
      contig: "1",
      allelFrequency: "0.2",
      variantIdentifier: "id"
    };
    var variant = new GeneVariant(data);
    assert.equal(variant.getPosition(), data.position);
    assert.equal(variant.getOriginalDna(), data.originalDna);
    assert.equal(variant.getModifiedDna(), data.modifiedDna);
    assert.equal(variant.getReferenceGenomeType(), data.referenceGenomeType);
    assert.equal(variant.getContig(), data.contig);
    assert.equal(variant.getAllelFrequency(), data.allelFrequency);
    assert.equal(variant.getVariantIdentifier(), data.variantIdentifier);
    assert.equal(variant.getReferenceGenomeVersion(), data.referenceGenomeVersion);
  });
});
