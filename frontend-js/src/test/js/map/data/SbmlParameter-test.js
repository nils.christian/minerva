"use strict";

var SbmlParameter = require('../../../../main/js/map/data/SbmlParameter');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('SbmlParameter', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  it("constructor", function () {
    var object = new SbmlParameter({
      "parameterId": "local_param",
      "unitsId": 24,
      "name": "local param",
      "global": false,
      "id": 7,
      "value": 5.0
    });
    assert.ok(object);
    assert.equal("local_param", object.getParameterId());
    assert.equal(24, object.getUnitsId());
    assert.equal("local param", object.getName());
    assert.equal(false, object.getGlobal());
    assert.equal(7, object.getId());
    assert.equal(5.0, object.getValue());

  });
});
