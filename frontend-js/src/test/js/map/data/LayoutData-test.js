"use strict";

require("../../mocha-config");

var LayoutAlias = require('../../../../main/js/map/data/LayoutAlias');
var DataOverlay = require('../../../../main/js/map/data/DataOverlay');

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('DataOverlay', function () {
  describe("constructor", function () {
    it("default", function () {
      var layoutId = 3;
      var name = "nm";
      var overlay = new DataOverlay(layoutId, name);
      assert.equal(overlay.getId(), layoutId);
      assert.equal(overlay.getName(), name);
    });

    it("with content", function () {
      var overlay = new DataOverlay({
        content: "test"
      });
      assert.ok(overlay.getContent());
    });

    it("from json obj", function () {
      var obj = {
        modelId: 15781,
        name: "test",
        description: "test",
        status: "OK",
        progress: "0.00",
        images: [{modelId: "15781", path: "subDir"}],
        creator: "admin ",
        inputDataAvailable: "true",
        idObject: 14852
      };
      var data = new DataOverlay(obj);
      assert.ok(data);
      assert.equal(data.getInputDataAvailable(), true);
      assert.equal(data.getImagesDirectory(15781), "subDir");
      assert.equal(data.getImagesDirectory(15782), null);
    });

    it("from problematic json", function () {
      var obj = {
        modelId: 15781,
        name: "test",
        inputDataAvailable: "wtf",
        idObject: 14852
      };
      var data = new DataOverlay(obj);
      assert.ok(data);
      assert.equal(logger.getWarnings().length, 1);
    });
  });

  it("updateAlias", function () {
    var layoutId = 3;
    var name = "nm";
    var overlay = new DataOverlay(layoutId, name);

    var aliasId = 4;
    var alias = new LayoutAlias({
      idObject: aliasId
    });

    overlay.addAlias(alias);

    assert.equal(overlay.getAliasById(aliasId).getValue(), null);

    var val = 5;
    var alias2 = new LayoutAlias({
      idObject: aliasId,
      value: val
    });
    overlay.updateAlias(alias2);

    assert.equal(overlay.getAliasById(aliasId).getValue(), val);

    assert.equal(logger.getWarnings().length, 0);
  });

  it("getFullAliasById", function () {
    var layoutId = 18076;
    var name = "nm";
    var overlay = new DataOverlay(layoutId, name);

    var aliasId = 329163;
    var alias = new LayoutAlias({
      idObject: aliasId,
      modelId: 15781
    });

    overlay.addAlias(alias);

    return overlay.getFullAliasById(aliasId).then(function (data) {
      assert.equal(data.getType(), LayoutAlias.GENERIC);
    });
  });

  it("update invalid alias", function () {
    var layoutId = 3;
    var name = "nm";
    var overlay = new DataOverlay(layoutId, name);

    var aliasId = 4;
    var alias = new LayoutAlias({
      idObject: aliasId
    });

    overlay.updateAlias(alias);

    assert.equal(logger.getWarnings().length, 1);
  });
});
