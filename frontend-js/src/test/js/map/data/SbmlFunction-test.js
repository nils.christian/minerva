"use strict";

var SbmlFunction = require('../../../../main/js/map/data/SbmlFunction');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('SbmlFunction', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  it("constructor", function () {
    var object = new SbmlFunction({
      "functionId": "fun",
      "name": "fun name",
      "definition": "<lambda>" +
      "<bvar><ci> x </ci></bvar>" +
      "<bvar><ci> y </ci></bvar>" +
      "<apply><plus/><ci> x </ci><ci> y </ci><cn type=\"integer\"> 2 </cn></apply>" +
      "</lambda>\n\n",
      "arguments": ["x", "y"],
      "id": 5
    });
    assert.ok(object);
    assert.ok(object.getDefinition().indexOf("apply") >= 0);
    assert.equal("fun name",object.getName());
    assert.equal("fun",object.getFunctionId());
    assert.equal(5,object.getId());
    assert.deepEqual(["x","y"],object.getArguments());
  });
});
