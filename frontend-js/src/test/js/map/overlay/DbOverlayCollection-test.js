"use strict";

var logger = require('../../logger');
require("../../mocha-config");

var DbOverlayCollection = require('../../../../main/js/map/overlay/DbOverlayCollection');

var assert = require('assert');

describe('DbOverlayCollection', function () {
  it("constructor", function () {
    var map = helper.createCustomMap();
    new DbOverlayCollection({
      map: map
    });

    assert.equal(logger.getWarnings.length, 0);
  });
});
