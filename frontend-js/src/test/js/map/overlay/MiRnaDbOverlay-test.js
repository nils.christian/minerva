"use strict";

var logger = require('../../logger');
require("../../mocha-config");

var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var MiRnaDbOverlay = require('../../../../main/js/map/overlay/MiRnaDbOverlay');
var ServerConnector = require('../../ServerConnector-mock');

var assert = require('assert');

describe('MiRnaDbOverlay', function() {
  it("constructor 1", function() {
    var map = helper.createCustomMap();
    var oc = new MiRnaDbOverlay({
      map : map,
      name : 'miRna'
    });
    assert.ok(oc);
    assert.equal(oc.getName(), 'miRna');

    assert.equal(logger.getWarnings.length, 0);
  });

  it("searchByQuery", function() {
    var map, searchDb;
    return ServerConnector.getProject().then(function(project) {
      map = helper.createCustomMap(project);
      searchDb = helper.createMiRnaDbOverlay(map);
      return searchDb.searchByQuery("hsa-miR-125a-3p");
    }).then(function(miRnas) {
      assert.equal(miRnas.length, 1);
      assert.equal(searchDb.getQueries().length, 1);
      return searchDb.getElementsByQuery(searchDb.getQueries()[0]);
    }).then(function(elements) {
      assert.equal(elements.length, 3);
      return searchDb.getIdentifiedElements();
    }).then(function(elements) {
      assert.equal(elements.length, 0);
    });
  });

  it("searchNamesByTarget", function() {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    var searchDb = helper.createMiRnaDbOverlay(map);

    var target = new IdentifiedElement({
      type : "ALIAS",
      id : 329170,
      modelId : 15781
    });

    return searchDb.searchNamesByTarget(target).then(function(miRnaNames) {
      assert.equal(miRnaNames.length, 0);
    });
  });

});
