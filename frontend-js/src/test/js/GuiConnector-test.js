"use strict";

require("./mocha-config");

var GuiConnector = require('../../main/js/GuiConnector');
var SecurityError = require('../../main/js/SecurityError');
var ServerConnector = require('./ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('./logger');

describe('GuiConnector', function () {
  it('destroy', function () {
    var length = document.body.children.length;
    var connector = new (GuiConnector.constructor)();
    connector.showProcessing("test");
    connector.destroy();
    assert.equal(length, document.body.children.length);
  });

  it('hideProcessing', function () {
    var connector = new (GuiConnector.constructor)();
    connector.showProcessing();
    connector.hideProcessing();
    $(".ui-dialog-content").each(function () {
      assert.notOk($(this).dialog('isOpen'));
    });
    connector.destroy();
  });

  it('alert', function () {
    var connector = new (GuiConnector.constructor)();
    connector.alert("some error");
    $(".ui-dialog-content").each(function () {
      assert.ok($(this).dialog('isOpen'));
    });
    connector.destroy();
  });
  describe('getErrorMessageForError', function () {
    it('SecurityError when logged in', function () {
      var connector = new (GuiConnector.constructor)();
      ServerConnector.getSessionData().setLogin("testUser");
      var message = connector.getErrorMessageForError(new SecurityError());
      assert.ok(message.indexOf("ask your administrator") >= 0);
      connector.destroy();
    });
    it('SecurityError when not logged in', function () {
      var connector = new (GuiConnector.constructor)();
      ServerConnector.getSessionData().setLogin("anonymous");
      var message = connector.getErrorMessageForError(new SecurityError());
      assert.ok(message.indexOf("to access this resource") >= 0);
      connector.destroy();
    });
  });


  it('info', function () {
    var connector = new (GuiConnector.constructor)();
    connector.info("info message");
    $(".ui-dialog-content").each(function () {
      assert.ok($(this).dialog('isOpen'));
    });
    connector.destroy();
  });

  describe('removeWindowResizeEvent', function () {
    it('valid handler', function () {
      var connector = new (GuiConnector.constructor)();
      connector.init();
      var handler = function () {

      };
      connector.addWindowResizeEvent(handler);
      connector.removeWindowResizeEvent(handler);
      assert.equal(0, logger.getWarnings().length);
      connector.destroy();
    });
    it('invalid handler', function () {
      var connector = new (GuiConnector.constructor)();
      connector.init();
      var handler = function () {

      };
      connector.addWindowResizeEvent(handler);
      connector.removeWindowResizeEvent(function () {

      });
      assert.equal(1, logger.getWarnings().length);
      connector.destroy();
    });
  });

  describe('showConfirmationDialog', function () {
    it('when answer is Yes', function () {
      var connector = new (GuiConnector.constructor)();
      // click Yes button in coming ms
      setTimeout(function () {
        $("button:contains(Yes)").click()
      }, 50);

      return connector.showConfirmationDialog({message: "hi there", title: "some"}).then(function (result) {
        assert.ok(result);
      });
    });
    it('when answer is No', function () {
      var connector = new (GuiConnector.constructor)();
      // click Yes button in coming ms
      setTimeout(function () {
        $("button:contains(No)").click()
      }, 50);

      return connector.showConfirmationDialog({message: "hi there", title: "some"}).then(function (result) {
        assert.notOk(result);
      });
    });
  });


});