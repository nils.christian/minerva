minervaDefine(function () {
  return {
    register: function (object) {
      return Promise.resolve();
    },
    unregister: function () {
      console.log("unregistering test plugin");
    },
    getName: function () {
      return "plugin with promise";
    },
    getVersion: function () {
      return "0.0.1";
    }
  };
});