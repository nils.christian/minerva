minervaDefine(function () {
  return {
    register: function (object) {
      console.log("registering test plugin with min width");
      object.project.map.addListener({
        object: "plugin", type: "onResize", callback: function () {
          console.log("Resize of plugin tab called");
        }
      });

    },
    unregister: function () {
      console.log("unregistering test plugin");
    },
    getName: function () {
      return "test plugin";
    },
    minWidth: function () {
      return 200;
    },
    getVersion: function () {
      return "0.0.1";
    }
  };
});