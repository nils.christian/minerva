MINERVA installation instructions using RPM Package Manager
===========================================================

These instructions guide you through the installation process of
`MINERVA <https://git-r3lab.uni.lu/piotr.gawron/minerva>`_ on Red Hat
Enterprise Linux 7 or CentOS 7. The server will also host the database
required for the functioning of MINERVA.

.. note::

   Content visualization of MINERVA platform is supported by Google
   Maps API. Users of MINERVA platform are obliged to comply with the
   `Google Maps/Google Earth APIs Terms of Service
   <https://www.google.com/intl/en-US_US/help/terms_maps.html>`_.


Overview
--------

The main steps are

* Install `PostgreSQL <https://www.postgresql.org/>`_ and set up a
  database.
* Install and configure `Apache Tomcat <https://tomcat.apache.org/>`_.
* Install the MINERVA RPM.


PostgreSQL
----------

Install PostgreSQL and initialise it with

.. code:: shell

    yum install -y postgresql-server
    postgresql-setup initdb

Ensure that the database authentication on IPv4 on ``localhost`` is
done with md5-based passwords by adding the following line to
``/var/lib/pgsql/data/pg_hba.conf``

.. code::

    host    all             all             127.0.0.1/32            md5


Enable and start postgresql

.. code:: shell

    systemctl enable postgresql
    systemctl start postgresql

Create the MINERVA database user and the database

.. code:: shell

    su - postgres -c "createuser -d -r -s map_viewer"
    su - postgres -c "echo \"ALTER USER map_viewer WITH PASSWORD '123qweasdzxc';\"| psql"
    su - postgres -c "createdb -O map_viewer map_viewer"

.. warning::

    Currently the password for the database user ``map_viewer`` is
    hardcoded, therefore make sure the database cannot be accessed
    from distrusted hosts.


Apache Tomcat
-------------

Install and enable (don't start yet) Apache Tomcat with

.. code:: shell

    yum install -y tomcat
    systemctl enable tomcat

In ``/etc/sysconfig/tomcat``, adjust the memory settings, e.g.

.. code::

    JAVA_OPTS="-Xms2048M -Xmx4096M"

Make sure to open the port (by default port 8080) to allow incoming
connections, e.g. using ``firewalld`` this can be accomplished with

.. code:: shell

    firewall-cmd --permanent --zone=public --add-port=8080/tcp

.. warning::

    Currently the administrator password of MINERVA is hardcoded,
    therefore make sure the MINERVA cannot be accessed from distrusted
    hosts until the password was changed.

MINERVA
-------

Install MINERVA using ``yum``

.. code:: shell

    yum install -y minerva-X.Y.Z-1.el7.centos.noarch.rpm

and start tomcat

.. code:: shell

    systemctl start tomcat

Now point your browser to the newly installed service, e.g. on a local
network this could be `<http://192.168.0.42:8080/minerva/>`_.

.. warning::

    This way of installing MINERVA does **not** ensure that security
    updates are installed with ``yum update``, therefore you have to
    establish a process to install such updates manually.
