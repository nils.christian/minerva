#!/bin/bash

set -e

MINERVA_SRC_DIR="$(dirname "$(dirname "$0")")"

# where generated files will be written
RPMBUILD_TEMP="${RPMBUILD_TEMP:-$MINERVA_SRC_DIR/rpm/rpmbuildtemp}"

if [[ "$RPMBUILD_TEMP" =~ ' ' ]]; then
    echo "RPMBUILD_TEMP contains whitespace: '$RPMBUILD_TEMP'"
    echo "This is not allowed. Please provide a different directory to create the RPM with"
    echo "    RPMBUILD_TEMP=/path/without/whitespace/ \"$0\""
    exit 1
fi

# current date (for automatic changelog)
CURDATE=$(date +"%a %b %d %Y")

# file that should be deployed on tomcat
TOMCAT_FILE="$MINERVA_SRC_DIR/web/target/web-1.0.war"

# file with bash code generating DB schema diffs for different versions
dbschemadiff="$MINERVA_SRC_DIR/management_scripts/db_schema_and_diff.sh"

# directory with database schema
DB_SCHEMA_DIR="$MINERVA_SRC_DIR/persist/src/db"

# destination directory for database upgrade scripts
DBSCRIPT_DEST_DIR="$RPMBUILD_TEMP/BUILD/sql"

# clean build directories
rm -rf "$RPMBUILD_TEMP"

# create directory
mkdir -p "$RPMBUILD_TEMP/"{BUILD,SPECS}

# copy war file and other files
cp "$TOMCAT_FILE" "$RPMBUILD_TEMP/BUILD/minerva.war"
cp "$MINERVA_SRC_DIR/README.md" "$MINERVA_SRC_DIR/CHANGELOG" "$MINERVA_SRC_DIR/rpm/INSTALL.rst" "$MINERVA_SRC_DIR/rpm/logrotate_minerva" "$RPMBUILD_TEMP/BUILD"

mkdir -p "$DBSCRIPT_DEST_DIR"
# copy base sql schema
cp "$DB_SCHEMA_DIR/base.sql" "$DBSCRIPT_DEST_DIR/db_0.sql"

# set $versions and $current_version; generate upgrade scripts in DBSCRIPT_DEST_DIR
source "$dbschemadiff"

# create RPM spec file
cp "$MINERVA_SRC_DIR/rpm/minerva.spec.in" "$RPMBUILD_TEMP/SPECS/minerva.spec"
sed -i "s/__CURRENT_VERSION__/$current_version/g" "$RPMBUILD_TEMP/SPECS/minerva.spec"
sed -i "s/__DATE__/$CURDATE/g" "$RPMBUILD_TEMP/SPECS/minerva.spec"

# build RPM
RPMBUILD_TEMP_ABS="$(readlink -f "$RPMBUILD_TEMP")"
echo "****** Building miverva $current_version RPM in $RPMBUILD_TEMP_ABS ****** "
set -x
rpmbuild -bb --define "_topdir $RPMBUILD_TEMP_ABS" "$RPMBUILD_TEMP_ABS/SPECS/minerva.spec"
set +x
echo "****** Finished building RPM ****** "
