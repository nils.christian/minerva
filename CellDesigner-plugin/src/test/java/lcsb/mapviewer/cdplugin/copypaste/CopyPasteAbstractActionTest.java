package lcsb.mapviewer.cdplugin.copypaste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Set;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CopyPasteAbstractActionTest {
	Logger	logger	= Logger.getLogger(CopyPasteAbstractActionTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPaste() throws Exception {
		try {
			CopyPasteAbstractAction pa = new PasteAction();
			String clipboardString = "[MIRIAM]\t" + MiriamRelationType.BQ_BIOL_IS.getStringRepresentation() + "\t" + MiriamType.CHEBI.getUris().get(0)
					+ "\tCHEBI:12345\nasd";
			Pair<Set<MiriamData>, String> res = pa.getAnnotationDataFromClipboardString(clipboardString);
			assertNotNull(res);
			assertNotNull(res.getLeft());
			assertEquals(1, res.getLeft().size());
			assertNotNull(res.getRight());
			assertEquals("asd", res.getRight());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopyPaste() throws Exception {
		try {
			CopyPasteAbstractAction pa = new PasteAction();
			String annString = "<rdf:RDF xmlns:bqbiol=\"http://biomodels.net/biology-qualifiers/\" "
					+ "xmlns:bqmodel=\"http://biomodels.net/model-qualifiers/\" "
					+ "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"//
					+ "<rdf:Description rdf:about=\"#s1\">" + "<bqmodel:is>" + "<rdf:Bag>" + "<rdf:li rdf:resource=\"urn:miriam:hgnc:SNCA\"/>" + "</rdf:Bag>"
					+ "</bqmodel:is>" + "</rdf:Description>" + "</rdf:RDF>";

			String str = pa.getCopyString(annString, "notes");

			Pair<Set<MiriamData>, String> res = pa.getAnnotationDataFromClipboardString(str);
			assertNotNull(res);
			assertNotNull(res.getLeft());
			assertEquals(1, res.getLeft().size());
			assertNotNull(res.getRight());
			assertEquals("notes", res.getRight());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSerializeMiriam() throws Exception {
		try {
			CopyPasteAbstractAction pa = new PasteAction();
			MiriamData md = new MiriamData(MiriamType.CHEBI, "CH:12");

			String string = pa.serialize(md);
			MiriamData md2 = pa.deserialize(string);
			assertEquals(md, md2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDeserializeMiriam() throws Exception {
		try {
			CopyPasteAbstractAction pa = new PasteAction();

			MiriamData md = pa.deserialize("ASDASDAS");
			assertNull(md);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDeserializeMiriam2() throws Exception {
		try {
			CopyPasteAbstractAction pa = new PasteAction();

			MiriamData md = pa.deserialize(null);
			assertNull(md);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDeserializeMiriam3() throws Exception {
		try {
			CopyPasteAbstractAction pa = new PasteAction();

			MiriamData md = pa.deserialize("[MIRIAM]\tinvalid\ttype\tresource");
			assertNull(md);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
