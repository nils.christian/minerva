package lcsb.mapviewer.cdplugin.copypaste;

import java.util.Set;

import org.apache.log4j.Logger;

import jp.sbi.celldesigner.MainWindow;
import jp.sbi.celldesigner.plugin.PluginListOf;
import jp.sbi.celldesigner.plugin.PluginReaction;
import jp.sbi.celldesigner.plugin.PluginSBase;
import jp.sbi.celldesigner.plugin.PluginSpecies;
import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.SystemClipboard;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * CD Plugin action resposible for translating annotation stored in notes to xml
 * format stored in annotations.
 * 
 * @author Piotr Gawron
 * 
 */
public class PasteAction extends CopyPasteAbstractAction {
	/**
	 * Default class logger.
	 */
	private final Logger	logger	= Logger.getLogger(PasteAction.class.getName());

	/**
	 * Method that perform annotation of a celldesigner species listed in
	 * speciesList.
	 * 
	 * @param plug
	 *          - a plugin object
	 * @param speciesList
	 *          - list of species to annotate
	 * @param win
	 *          - main window of cell designer
	 */
	public void performAnnotation(CopyPastePlugin plug, PluginListOf speciesList, MainWindow win) {

		XmlAnnotationParser xap = new XmlAnnotationParser();

		// if list is empty then just return from the method
		if (speciesList.size() == 0) {
			return;
		}

		SystemClipboard sc = new SystemClipboard();
		String value = sc.getClipboardContents();

		Pair<Set<MiriamData>, String> data = getAnnotationDataFromClipboardString(value);
		String notes = data.getRight();
		Set<MiriamData> set = data.getLeft();

		this.setWindow(win);
		this.setPlug(plug);

		int size = speciesList.size();
		for (int i = 0; i < size; i++) {
			PluginSBase element = speciesList.get(i);
			if (element instanceof PluginSpeciesAlias) {
				PluginSpeciesAlias alias = (PluginSpeciesAlias) element;
				PluginSpecies species = alias.getSpecies();
				species.setNotes(notes);
				species.setAnnotationString(xap.dataSetToXmlString(set));
				plug.notifySBaseChanged(element);
				plug.notifySBaseChanged(species);
			} else if (element instanceof PluginReaction) {
				PluginReaction reaction = (PluginReaction) element;
				reaction.setNotes(notes);
				reaction.setAnnotationString(xap.dataSetToXmlString(set));
				plug.notifySBaseChanged(element);
			} else {
				logger.warn("Unknown class type :" + element.getClass());
			}
		}
	}

}
