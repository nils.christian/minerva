package lcsb.mapviewer.cdplugin.copypaste;

import java.awt.event.ActionEvent;

import jp.sbi.celldesigner.MainWindow;
import jp.sbi.celldesigner.plugin.PluginAction;
import jp.sbi.celldesigner.plugin.PluginListOf;

import org.apache.log4j.Logger;

/**
 * This class represent action that paste annotations and notes from clipboard
 * into selected species/reactions.
 * 
 * @author Piotr Gawron
 * 
 */
public class PastePluginAction extends PluginAction {

	/**
	 * Default class logger.
	 */
	private Logger						logger						= Logger.getLogger(PasteAction.class.getName());
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	/**
	 * Plugin that access this action.
	 */
	private CopyPastePlugin	plug							= null;
	/**
	 * Main window of CellDesigner.
	 */
	private MainWindow				window;

	/**
	 * Default constructor.
	 * 
	 * @param plugin
	 *          {@link #plug}
	 * @param win
	 *          {@link #window}
	 */
	public PastePluginAction(CopyPastePlugin plugin, MainWindow win) {
		this.plug = plugin;
		this.window = win;
	}

	@Override
	public void myActionPerformed(ActionEvent e) {
		try {
			PasteAction annotateAction = new PasteAction();
			PluginListOf list = plug.getSelectedAllNode();
			annotateAction.performAnnotation(plug, list, window);
		} catch (Exception ex) {
			logger.error(ex, ex);
		}
	}

}