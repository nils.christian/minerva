package lcsb.mapviewer.cdplugin.copypaste;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import jp.sbi.celldesigner.MainWindow;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * This abstract class defines common function for {@link CopyAction} and
 * {@link PasteAction}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CopyPasteAbstractAction extends XmlParser {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger				logger	= Logger.getLogger(CopyPasteAbstractAction.class);

	/**
	 * Prefix used for serialization/deserialization of {@link MiriamData} to
	 * string stored in system clipboard.
	 */
	private static final String	PREFIX	= "[MIRIAM]";
	/**
	 * Plugin object which ran this action. This object is used to update
	 * information about species
	 */
	private CopyPastePlugin			plug;
	/**
	 * Object of CellDesigner main form.
	 */
	private MainWindow					window;

	/**
	 * Serializes {@link MiriamData} into string that will be stored into system
	 * clipboard.
	 * 
	 * @param md
	 *          object to serialize
	 * @return string representation of {@link MiriamData}
	 */
	protected String serialize(MiriamData md) {
		return PREFIX + "\t" + md.getRelationType().getStringRepresentation() + "\t" + md.getDataType().getUris().get(0) + "\t" + md.getResource();
	}

	/**
	 * Deserialize {@link MiriamData} from string creted by
	 * {@link #serialize(MiriamData)} method.
	 * 
	 * @param string
	 *          string representation of {@link MiriamData}
	 * @return {@link MiriamData} obtained from input string
	 */
	protected MiriamData deserialize(String string) {
		if (string == null) {
			return null;
		}
		String[] tmp = string.split("\t");

		// CHECKSTYLE:OFF
		if (tmp.length == 4) {
			if (PREFIX.equals(tmp[0])) {
				String relation = tmp[1];
				String uri = tmp[2];
				String resource = tmp[3];
				MiriamType mt = MiriamType.getTypeByUri(uri);
				MiriamRelationType mrt = MiriamRelationType.getTypeByStringRepresentation(relation);
				if (mt == null || mrt == null) {
					return null;
				}
				return new MiriamData(mrt, mt, resource);
			}
		}
		// CHECKSTYLE:ON
		return null;
	}

	/**
	 * Returns serialized string of annotations and notes.
	 * 
	 * @param annotationString
	 *          xml string representing annotations in CellDesigner
	 * @param notesString
	 *          flat notes String
	 * @return serialized string
	 * @throws InvalidXmlSchemaException
	 *           thrown when xmlString is invalid
	 */
	protected String getCopyString(String annotationString, String notesString) throws InvalidXmlSchemaException {
		XmlAnnotationParser xap = new XmlAnnotationParser();
		Set<MiriamData> set = xap.parse(annotationString);
		StringBuilder result = new StringBuilder();
		for (MiriamData md : set) {
			result.append(serialize(md) + "\n");
		}
		result.append(notesString);
		return result.toString();
	}

	/**
	 * Deserialize string created by {@link #getCopyString(String, String)}
	 * method. Used to create {@link MiriamData} set and notes from clipboard.
	 * 
	 * @param value
	 *          string for deserialization
	 * 
	 * @return {@link Pair} of {@link MiriamData} set and notes string
	 */
	protected Pair<Set<MiriamData>, String> getAnnotationDataFromClipboardString(String value) {
		if (value == null) {
			return new Pair<Set<MiriamData>, String>(null, null);
		}
		String[] rows = value.split("\n");

		StringBuilder sb = new StringBuilder("");

		boolean miriam = true;

		Set<MiriamData> set = new HashSet<MiriamData>();
		for (int i = 0; i < rows.length; i++) {
			String row = rows[i];
			if (miriam) {
				MiriamData md = deserialize(row);
				if (md != null) {
					set.add(md);
				} else {
					miriam = false;
				}
			}
			if (!miriam) {
				sb.append(row);
				sb.append("\n");
			}
		}
		String notes = sb.toString().trim();

		return new Pair<Set<MiriamData>, String>(set, notes);
	}

	/**
	 * @return the plug
	 * @see #plug
	 */
	protected CopyPastePlugin getPlug() {
		return plug;
	}

	/**
	 * @param plug
	 *          the plug to set
	 * @see #plug
	 */
	protected void setPlug(CopyPastePlugin plug) {
		this.plug = plug;
	}

	/**
	 * @return the window
	 * @see #window
	 */
	protected MainWindow getWindow() {
		return window;
	}

	/**
	 * @param window
	 *          the window to set
	 * @see #window
	 */
	protected void setWindow(MainWindow window) {
		this.window = window;
	}

}