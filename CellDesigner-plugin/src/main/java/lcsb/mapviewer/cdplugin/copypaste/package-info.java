/**
 * In this package there is implementation of simple plugin that allows to copy
 * and paste annotations and notes from CellDesigner elements into other
 * CellDesigner elements. It uses system clipboard to store data. More
 * information can be found in
 * {@link lcsb.mapviewer.cdplugin.copypaste.CopyPastePlugin}.
 */
package lcsb.mapviewer.cdplugin.copypaste;

