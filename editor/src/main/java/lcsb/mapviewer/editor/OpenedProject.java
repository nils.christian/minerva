package lcsb.mapviewer.editor;

import lcsb.mapviewer.model.Project;

/**
 * Project opened in the editor.
 * 
 * @author Piotr Gawron
 *
 */
public class OpenedProject extends Project {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name from which project was opened.
	 */
	private String						fileName;

	/**
	 * @return the fileName
	 * @see #fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *          the fileName to set
	 * @see #fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
