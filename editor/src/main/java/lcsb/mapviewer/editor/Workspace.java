package lcsb.mapviewer.editor;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains all projects opened in the application.
 * 
 * @author Piotr Gawron
 *
 */
public class Workspace {
	/**
	 * List of opened projects.
	 */
	private List<OpenedProject>			 openedProjects			= new ArrayList<>();

	/**
	 * List of listeners that should be thrown when new project is opened.
	 */
	private List<OpenProjectListener> addProjectListeners = new ArrayList<>();

	/**
	 * Method that adds new project to workspace.
	 * 
	 * @param project
	 *          new object to add
	 */
	public void addProject(OpenedProject project) {
		openedProjects.add(project);
		OpenProjectEvent event = new OpenProjectEvent(this, project);
		for (OpenProjectListener listener : addProjectListeners) {
			listener.actionPerformed(event);
		}
	}

	/**
	 * MEthods that adds listeners to be called after new project is opened.
	 * 
	 * @param listener
	 *          listener to add
	 */
	public void addProjectAddedListener(OpenProjectListener listener) {
		addProjectListeners.add(listener);
	}
}
