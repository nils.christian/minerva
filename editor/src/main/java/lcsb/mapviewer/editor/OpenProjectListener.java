package lcsb.mapviewer.editor;

/**
 * Listener handling {@link OpenProjectEvent} (event that happens when new
 * project is opened).
 * 
 * @author Piotr Gawron
 *
 */
public interface OpenProjectListener {

	/**
	 * Performs some action when event occurred.
	 * 
	 * @param e
	 *          event to deal with
	 */
	void actionPerformed(OpenProjectEvent e);

}
