package lcsb.mapviewer.editor;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * Class with data buffered during renedering content of the map. Used in
 * {@link lcsb.mapviewer.editor.gui.canvas.MinervaCanvas} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class BufferedImageData {

	/**
	 * Buffered data.
	 */
	private BufferedImage grid = null;

	/**
	 * X coordinate from which map was drawn.
	 */
	private Integer			 x;

	/**
	 * Y coordinate from which map was drawn.
	 */
	private Integer			 y;

	/**
	 * Width of the drawn fragment.
	 */
	private Integer			 width;

	/**
	 * Height of the drawn fragment.
	 */
	private Integer			 height;

	/**
	 * Zoom level at which map was drawn.
	 */
	private Double				zoom;

	/**
	 * Default constructor.
	 * 
	 * @param x
	 *          {@link #x}
	 * @param y
	 *          {@link #y}
	 * @param width
	 *          {@link #width}
	 * @param height
	 *          {@link #height}
	 * @param zoom
	 *          {@link #zoom}
	 */
	public BufferedImageData(int x, int y, int width, int height, double zoom) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.zoom = zoom;
		grid = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	}

	/**
	 * @return the x
	 * @see #x
	 */
	public Integer getX() {
		return x;
	}

	/**
	 * @return the y
	 * @see #y
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * @return the width
	 * @see #width
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @return the height
	 * @see #height
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * @return the zoom
	 * @see #zoom
	 */
	public Double getZoom() {
		return zoom;
	}

	/**
	 * Return graphics for printing (to obtain buffered data).
	 * 
	 * @return graphics for printing (to obtain buffered data)
	 */
	public Graphics2D getGraphics() {
		return (Graphics2D) grid.getGraphics();
	}

	/**
	 * Returns buffered image.
	 * 
	 * @return buffered image
	 */
	public BufferedImage getImg() {
		return grid;
	}
}
