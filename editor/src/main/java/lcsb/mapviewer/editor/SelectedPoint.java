package lcsb.mapviewer.editor;

import java.awt.geom.Point2D;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class represeting selected {@link Point2D} on {@link Element} or
 * {@link Reaction}.
 * 
 * @author Piotr Gawron
 *
 */
public class SelectedPoint {

	/**
	 * Point that is selected.
	 */
	private Point2D point;

	/**
	 * Object where the point is located.
	 */
	private Object	container;

	/**
	 * Default constructor.
	 * 
	 * @param container
	 *          {@link #container}
	 * @param point
	 *          {@link #point}
	 */
	public SelectedPoint(Object container, Point2D point) {
		if (!(container instanceof Element) && !(container instanceof Reaction)) {
			throw new InvalidArgumentException("Unknown container class: " + container);
		}
		this.container = container;
		this.point = point;
	}

	/**
	 * @return the point
	 * @see #point
	 */
	public Point2D getPoint() {
		return point;
	}

	/**
	 * @return the container
	 * @see #container
	 */
	public Object getContainer() {
		return container;
	}
}
