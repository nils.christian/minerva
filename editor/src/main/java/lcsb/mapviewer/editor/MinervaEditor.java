package lcsb.mapviewer.editor;

import lcsb.mapviewer.editor.gui.MinervaWindow;

import org.apache.log4j.Logger;

/**
 * Entry point to MINERVA editor.
 * 
 * @author Piotr Gawron
 *
 */
public final class MinervaEditor {

	/**
	 * Default class logger.
	 */
	private Logger				logger = Logger.getLogger(MinervaEditor.class);

	/**
	 * Window where the software will be running.
	 */
	private MinervaWindow window;

	/**
	 * Main method executed from command line.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		try {
			MinervaEditor editor = new MinervaEditor();
			editor.run();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.getLogger(MinervaEditor.class).error(e, e);
		}
	}

	/**
	 * Default constructor.
	 */
	private MinervaEditor() {
		logger.debug("Initialize window...");
		window = new MinervaWindow();
	}

	/**
	 * Method that starts application.
	 */
	private void run() {
		window.setVisible(true);
	}
}
