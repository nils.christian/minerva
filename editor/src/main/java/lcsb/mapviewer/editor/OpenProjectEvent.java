package lcsb.mapviewer.editor;

import java.awt.event.ActionEvent;

/**
 * Event object created when new map is opened.
 * 
 * @see OpenProjectListener
 * 
 * @author Piotr Gawron
 *
 */
public class OpenProjectEvent extends ActionEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Project that was opened.
	 */
	private OpenedProject		 project;

	/**
	 * Default constructor.
	 * 
	 * @param source
	 *          object that caused the event
	 * @param project
	 *          {@link #project}
	 */
	public OpenProjectEvent(Object source, OpenedProject project) {
		super(source, 0, "Open project");
		this.project = project;
	}

	/**
	 * @return the project
	 * @see #project
	 */
	public OpenedProject getProject() {
		return project;
	}
}
