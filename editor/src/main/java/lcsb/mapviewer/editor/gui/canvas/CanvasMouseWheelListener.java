package lcsb.mapviewer.editor.gui.canvas;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * {@link MouseWheelListener} used to perform specific operations on
 * {@link MinervaCanvas} when mouse wheel is used (by default it's zoom in zoom
 * out).
 * 
 * @author Piotr Gawron
 *
 */
public class CanvasMouseWheelListener implements MouseWheelListener {
	/**
	 * {@link MinervaCanvas} where map is drawn.
	 */
	private MinervaCanvas canvas;

	/**
	 * Default constructor.
	 * 
	 * @param canvas
	 *          {@link #canvas}
	 */
	public CanvasMouseWheelListener(MinervaCanvas canvas) {
		this.canvas = canvas;
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
			int notches = e.getWheelRotation();
			if (notches < 0) {
				this.canvas.setZoom(this.canvas.getZoom() * MinervaCanvas.ZOOM_IN_FACTOR);
			} else {
				this.canvas.setZoom(this.canvas.getZoom() * MinervaCanvas.ZOOM_OUT_FACTOR);
			}
		}
	}

}
