package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.ChangeElementFullNameCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that creates {@link ChangeElementFullNameCommand} object from gui input
 * for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeFullNameCommand extends CreatePropertyChangeCommand {
	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateChangeFullNameCommand(JTextField field) {
		super(field);
	}

	@Override
	public ModelCommand createCommand(Model model, Element alias) {
		return new ChangeElementFullNameCommand(model, alias, ((JTextComponent) getField()).getText());
	}
}
