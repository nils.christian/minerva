/**
 * This package contains classes that can be used to open
 * {@link javax.swing.JDialog dialog} boxes to edit different types of objects.
 * The abstract class that defines usability of this dialog editors is places in
 * {@link lcsb.mapviewer.editor.gui.objectedit.ObjectEditDialog} class.
 */
package lcsb.mapviewer.editor.gui.objectedit;

