package lcsb.mapviewer.editor.gui;

import lcsb.mapviewer.commands.CommandExecutionException;

/**
 * Listener that handles {@link ElementModifiedEvent}.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *
 */
public interface ElementModifiedEventListener<T> {

	/**
	 * Performs some action when event occurred.
	 * 
	 * @param e
	 *          event to deal with
	 * @throws CommandExecutionException
	 *           thrown when the event data is invalid and cannot be executed
	 *           properly as {@link lcsb.mapviewer.commands.ModelCommand}
	 */
	void actionPerformed(ElementModifiedEvent<T> e) throws CommandExecutionException;

}
