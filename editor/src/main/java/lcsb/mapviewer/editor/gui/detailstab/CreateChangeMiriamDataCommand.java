package lcsb.mapviewer.editor.gui.detailstab;

import java.util.List;

import javax.swing.JList;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.AddElementMiriamDataCommand;
import lcsb.mapviewer.commands.properties.ChangeElementMiriamDataCommand;
import lcsb.mapviewer.commands.properties.RemoveElementMiriamDataCommand;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that creates commands that modifies {@link MiriamData miriam
 * annotationd} from gui input for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeMiriamDataCommand extends CreateListPropertyChangeCommand<MiriamData> {

	/**
	 * Default constructor.
	 * 
	 * @param list
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateChangeMiriamDataCommand(JList<MiriamData> list) {
		super(list);
	}

	@Override
	protected ModelCommand createRemoveCommand(Model model, Element alias, List<MiriamData> values) {
		return new RemoveElementMiriamDataCommand(model, alias, values);
	}

	@Override
	protected ModelCommand createChangeCommand(Model model, Element alias, MiriamData newValue, MiriamData oldValue) {
		return new ChangeElementMiriamDataCommand(model, alias, newValue, oldValue);
	}

	@Override
	protected ModelCommand createAddCommand(Model model, Element alias, List<MiriamData> newValues) {
		return new AddElementMiriamDataCommand(model, alias, newValues);
	}

}
