package lcsb.mapviewer.editor.gui.detailstab;

import java.util.List;

import javax.swing.JList;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.AddElementFormerSymbolsCommand;
import lcsb.mapviewer.commands.properties.ChangeElementFormerSymbolCommand;
import lcsb.mapviewer.commands.properties.RemoveElementFormerSymbolsCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that creates commands that modifies {@link Element#formerSymbols} from
 * gui input for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeFormerSymbolCommand extends CreateListPropertyChangeCommand<String> {

	/**
	 * Default constructor.
	 * 
	 * @param list
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateChangeFormerSymbolCommand(JList<String> list) {
		super(list);
	}

	@Override
	protected ModelCommand createRemoveCommand(Model model, Element alias, List<String> values) {
		return new RemoveElementFormerSymbolsCommand(model, alias, values);
	}

	@Override
	protected ModelCommand createChangeCommand(Model model, Element alias, String newValue, String oldValue) {
		return new ChangeElementFormerSymbolCommand(model, alias, newValue, oldValue);
	}

	@Override
	protected ModelCommand createAddCommand(Model model, Element alias, List<String> newValues) {
		return new AddElementFormerSymbolsCommand(model, alias, newValues);
	}

}
