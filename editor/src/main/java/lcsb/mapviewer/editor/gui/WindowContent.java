package lcsb.mapviewer.editor.gui;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import lcsb.mapviewer.editor.OpenProjectEvent;
import lcsb.mapviewer.editor.OpenProjectListener;
import lcsb.mapviewer.editor.OpenedProject;
import lcsb.mapviewer.editor.Workspace;
import lcsb.mapviewer.editor.gui.canvas.MinervaCanvas;
import lcsb.mapviewer.editor.gui.detailstab.ElementDetailsPanel;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

/**
 * Panel representing content of the gui window.
 * 
 * @author Piotr Gawron
 *
 */
public class WindowContent extends JPanel {

	/**
	 * 
	 */
	private static final long	 serialVersionUID = 1L;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger				logger					 = Logger.getLogger(WindowContent.class);

	/**
	 * Tabs with projects content.
	 */
	private JTabbedPane				 projectsTab;

	/**
	 * {@link JPanel} with detail information about selected element.
	 */
	private ElementDetailsPanel elementDetailsPanel;

	/**
	 * Currently selected tab on {@link #projectsTab}.
	 */
	private ProjectTab					selectedTab;

	/**
	 * Workspace with all opened projects.
	 */
	private Workspace					 workspace;

	/**
	 * Default constructor.
	 * 
	 * @param workspace
	 *          {@link #workspace}
	 */
	public WindowContent(Workspace workspace) {
		this.workspace = workspace;
		this.setLayout(new BorderLayout(GuiStyle.GAP_BETWEEN_COMPONENTS, GuiStyle.GAP_BETWEEN_COMPONENTS));

		projectsTab = new JTabbedPane();

		this.add(projectsTab, BorderLayout.CENTER);

		elementDetailsPanel = new ElementDetailsPanel();

		this.add(new JScrollPane(elementDetailsPanel), BorderLayout.EAST);

		projectsTab.addPropertyChangeListener(MinervaCanvas.SELECTED_ELEMENTS, new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				selectedTab = (ProjectTab) (evt.getSource());
				List<?> selectedElements = (List<?>) evt.getNewValue();
				if (selectedElements.size() == 1) {
					elementDetailsPanel.setElement(selectedElements.get(0));
				} else {
					elementDetailsPanel.setElement(null);
				}
			}
		});

		elementDetailsPanel.addAliasModifiedEventListener(new ElementModifiedEventListener<Element>() {
			@Override
			public void actionPerformed(ElementModifiedEvent<Element> e) {
				if (e.getCommand().getAffectedRegion() != null) {
					selectedTab.addInvalidRegion(e.getCommand().getAffectedRegion());
					selectedTab.repaint();
				}
			}
		});

		elementDetailsPanel.addReactionModifiedEventListener(new ElementModifiedEventListener<Reaction>() {
			@Override
			public void actionPerformed(ElementModifiedEvent<Reaction> e) {
				if (e.getCommand().getAffectedRegion() != null) {
					selectedTab.addInvalidRegion(e.getCommand().getAffectedRegion());
					selectedTab.repaint();
				}
			}
		});

		this.workspace.addProjectAddedListener(new OpenTabListener());
	}

	/**
	 * Listener used to open new tab.
	 * 
	 * @author Piotr Gawron
	 *
	 */
	private class OpenTabListener implements OpenProjectListener {

		@Override
		public void actionPerformed(OpenProjectEvent e) {
			OpenedProject project = e.getProject();
			ProjectTab tab = new ProjectTab(project);
			projectsTab.addTab(FilenameUtils.getName(project.getFileName()), tab);
			for (PropertyChangeListener listener : projectsTab.getPropertyChangeListeners(MinervaCanvas.SELECTED_ELEMENTS)) {
				tab.addPropertyChangeListener(MinervaCanvas.SELECTED_ELEMENTS, listener);
			}
		}

	}
}
