package lcsb.mapviewer.editor.gui.canvas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.editor.BufferedImageData;
import lcsb.mapviewer.editor.gui.GuiStyle;
import lcsb.mapviewer.model.map.model.Model;

import org.apache.log4j.Logger;

/**
 * Canvas used for vizualization of the map.
 * 
 * @author Piotr Gawron
 *
 */
public class MinervaCanvas extends JComponent {

	/**
	 * 
	 */
	private static final long	 serialVersionUID	= 1L;

	/**
	 * String representing {@link #startX} property of the component.
	 */
	private static final String START_X					 = "START_X";

	/**
	 * String representing {@link #startY} property of the component.
	 */
	private static final String START_Y					 = "START_Y";

	/**
	 * String representing {@link #zoom} property of the component.
	 */
	private static final String ZOOM_LEVEL				= "ZOOM_LEVEL";

	/**
	 * String representing {@link #selectedElements} property of the component.
	 */
	public static final String	SELECTED_ELEMENTS = "SELECTED_ELEMENTS";

	/**
	 * What should be the ratio of zoom when zooming out. As a good rule following
	 * equation should be fulfilled: <br/>
	 * {@link #ZOOM_IN_FACTOR}* {@value #ZOOM_OUT_FACTOR} == 1.
	 */
	public static final double	ZOOM_OUT_FACTOR	 = 1.25;

	/**
	 * What should be the ratio when zooming in.
	 */
	public static final double	ZOOM_IN_FACTOR		= 0.8;

	/**
	 * Default class logger.
	 */
	private final Logger				logger						= Logger.getLogger(MinervaCanvas.class);

	/**
	 * Bar representing x coordinate from which we visualize map.
	 */
	private JScrollBar					hbar;

	/**
	 * Bar representing y coordinate from which we visualize map.
	 */
	private JScrollBar					vbar;

	/**
	 * X coordinate from which we visualize (draw) map.
	 * 
	 * @see #hbar
	 */
	private int								 startX						= 0;

	/**
	 * Y coordinate from which we visualize (draw) map.
	 * 
	 * @see #vbar
	 */
	private int								 startY						= 0;

	/**
	 * Zoom level at which we visualize map.
	 */
	private double							zoom							= 1.0;

	/**
	 * Model that we vizualize.
	 */
	private Model							 model;

	/**
	 * List of selected elements.
	 */
	private List<Object>				selectedElements	= new ArrayList<>();

	/**
	 * Internat structure repesenting physical component on which we draw
	 * {@link #model}.
	 */
	private InternalCanvas			canvas;

	/**
	 * Region that should be redrawn (without using buffered image from previous
	 * paint call) during next {@link InternalCanvas#paintComponent(Graphics)}
	 * call.
	 */
	private Rectangle2D				 invalidRegion		 = null;

	/**
	 * Default constructor.
	 * 
	 * @param model
	 *          {@link #model}
	 */
	public MinervaCanvas(Model model) {
		this.model = model;
		setPreferredSize(new Dimension(GuiStyle.DEFAULT_WIDTH, GuiStyle.DEFAULT_HEIGHT));

		setLayout(new BorderLayout());

		hbar = new JScrollBar(JScrollBar.HORIZONTAL, startX, 0, 0, model.getWidth().intValue());
		vbar = new JScrollBar(JScrollBar.VERTICAL, startY, 0, 0, model.getHeight().intValue());

		hbar.addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				setStartX(e.getValue());
			}
		});
		vbar.addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				setStartY(e.getValue());
			}
		});
		canvas = new InternalCanvas(this);
		add(canvas, BorderLayout.CENTER);
		add(hbar, BorderLayout.SOUTH);
		add(vbar, BorderLayout.EAST);

		addPropertyChangeListener(ZOOM_LEVEL, new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				double oldVal = (double) evt.getOldValue();
				double newVal = (double) evt.getNewValue();
				hbar.setMaximum((int) (model.getWidth().intValue() / newVal));
				hbar.setVisibleAmount((int) (MinervaCanvas.this.canvas.getWidth()));

				vbar.setMaximum((int) (model.getHeight().intValue() / newVal));
				vbar.setVisibleAmount((int) (MinervaCanvas.this.canvas.getHeight()));

				hbar.setValue((int) (hbar.getValue() * newVal / oldVal));
				vbar.setValue((int) (vbar.getValue() * newVal / oldVal));
				repaint();
			}
		});

		repaint();
	}

	/**
	 * Shows error message.
	 * 
	 * @param message
	 *          error message
	 * @param exception
	 *          {@link Exception} that caused the error
	 */
	private void showError(String message, Exception exception) {
		logger.error(exception, exception);
		JOptionPane.showMessageDialog(getParent(), message);
	}

	/**
	 * @return the startX
	 * @see #startX
	 */
	public int getStartX() {
		return startX;
	}

	/**
	 * @param startX
	 *          the startX to set
	 * @see #startX
	 */
	public void setStartX(int startX) {
		int oldValue = this.startX;
		this.startX = startX;
		firePropertyChange(START_X, oldValue, startX);
	}

	/**
	 * @return the startY
	 * @see #startY
	 */
	public int getStartY() {
		return startY;
	}

	/**
	 * @param startY
	 *          the startY to set
	 * @see #startY
	 */
	public void setStartY(int startY) {
		int oldValue = this.startY;
		this.startY = startY;
		firePropertyChange(START_Y, oldValue, startY);
	}

	/**
	 * @return the zoom
	 * @see #zoom
	 */
	public double getZoom() {
		return zoom;
	}

	/**
	 * @param zoom
	 *          the zoom to set
	 * @see #zoom
	 */
	public void setZoom(double zoom) {
		double oldValue = this.zoom;
		this.zoom = zoom;
		firePropertyChange(ZOOM_LEVEL, oldValue, zoom);
	}

	/**
	 * This component is resposnible exlusivelly for drawing map on it (nothing
	 * else should exist on it, no {@link JScrollBar}, etc).
	 * 
	 * @author Piotr Gawron
	 *
	 */
	private class InternalCanvas extends JComponent {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Data that was buffered during last drawing (
		 * {@link #paintComponent(Graphics)}).
		 */
		private BufferedImageData lastBufferedData;

		/**
		 * Temporary buffer used to generat part of the map.
		 */
		private BufferedImageData tmpBufferedData	= null;

		/**
		 * Default converter.
		 * 
		 * @param parentCanvas
		 *          parent {@link JComponent} on which canvas is located
		 */
		protected InternalCanvas(MinervaCanvas parentCanvas) {
			PropertyChangeListener boundsChangedListener = new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					logger.debug("Property changed: " + evt.getPropertyName());
					logger.debug("Zoom level: " + zoom);
					repaint();
				}
			};
			parentCanvas.addPropertyChangeListener(START_X, boundsChangedListener);
			parentCanvas.addPropertyChangeListener(START_Y, boundsChangedListener);
			parentCanvas.addPropertyChangeListener(ZOOM_LEVEL, boundsChangedListener);

			addComponentListener(new ComponentListener() {

				@Override
				public void componentShown(ComponentEvent e) {
				}

				@Override
				public void componentResized(ComponentEvent e) {
					hbar.setVisibleAmount((int) (e.getComponent().getWidth()));
					hbar.setMaximum((int) (getModel().getWidth().intValue() / zoom));
					if (hbar.getValue() > hbar.getMaximum() - e.getComponent().getWidth()) {
						hbar.setValue(Math.max(0, hbar.getMaximum() - e.getComponent().getWidth()));
					}

					vbar.setVisibleAmount((int) (e.getComponent().getHeight()));
					vbar.setMaximum((int) (getModel().getHeight().intValue() / zoom));
					if (vbar.getValue() > vbar.getMaximum() - e.getComponent().getHeight()) {
						vbar.setValue((int) Math.max(0, vbar.getMaximum() - e.getComponent().getHeight()));
					}
				}

				@Override
				public void componentMoved(ComponentEvent e) {
				}

				@Override
				public void componentHidden(ComponentEvent e) {
				}
			});

			addMouseWheelListener(new CanvasMouseWheelListener(parentCanvas));

			addMouseListener(new CanvasSelectMouseListener(parentCanvas));

		}

		@Override
		public void paintComponent(Graphics g) {

			BufferedImageData newBufferedData = new BufferedImageData(getStartX(), getStartY(), getWidth(), getHeight(), getZoom());

			logger.debug("Repaint...");

			/**
			 * {@link AbstractImageGenerator} implementation that will draw a map on
			 * our buffer.
			 * 
			 * @author Piotr Gawron
			 *
			 */
			final class Test extends AbstractImageGenerator {

				/**
				 * Default constructor.
				 * 
				 * @param params
				 *          {@link AbstractImageGenerator.Params} used to generate. They
				 *          contain information about map, position of the fragment to
				 *          print, scale, etc.
				 * @throws DrawingException
				 *           thrown when there is a problem with drawing
				 */
				private Test(AbstractImageGenerator.Params params) throws DrawingException {
					super(params);
				}

				@Override
				protected void createImageObject(double width, double height) {
					super.setGraphics(InternalCanvas.this.getImageTmpGraphics());
				}

				@Override
				public void saveToFileImplementation(String fileName) throws IOException {
					throw new NotImplementedException();
				}

				@Override
				public void saveToOutputStreamImplementation(OutputStream os) throws IOException {
					throw new NotImplementedException();
				}

				@Override
				public void savePartToFileImplementation(int x, int y, int width, int height, String fileName) throws IOException {
					throw new NotImplementedException();
				}

				@Override
				public void savePartToOutputStreamImplementation(int x, int y, int width, int height, OutputStream os) throws IOException {
					throw new NotImplementedException();
				}

				@Override
				public String getFormatName() {
					throw new NotImplementedException();
				}

				@Override
				public MimeType getMimeType() {
					throw new NotImplementedException();
				}

				@Override
				public String getFileExtension() {
					throw new NotImplementedException();
				}

				@Override
				protected void closeImageObject() {
				}

			}
			try {
				if (lastBufferedData == null || Math.abs(lastBufferedData.getZoom() - zoom) > Configuration.EPSILON) {
					tmpBufferedData = newBufferedData;
					new Test(new Params().height(getHeight()).width(getWidth()).model(getModel()).x(getStartX() * zoom).y(getStartY() * zoom).scale(zoom));
				} else {
					// TOP PART (with extension to left and right)
					if (startY < lastBufferedData.getY()) {
						int y = startY;
						int height = Math.min(lastBufferedData.getY() - y, getHeight());
						int x = startX;
						int width = getWidth();
						tmpBufferedData = new BufferedImageData(x, y, width, height, getZoom());
						new Test(new Params().height(height).width(width).model(getModel()).x(x * zoom).y(y * zoom).scale(zoom));
						newBufferedData.getGraphics().drawImage(tmpBufferedData.getImg(), null, 0, 0);
					}
					// BOTTOM PART (with extension to left and right)
					if (startY + getHeight() > lastBufferedData.getY() + lastBufferedData.getHeight()) {
						int y = lastBufferedData.getY() + lastBufferedData.getHeight();
						int height = startY + getHeight() - y;
						int x = startX;
						int width = getWidth();
						tmpBufferedData = new BufferedImageData(x, y, width, height, getZoom());
						new Test(new Params().height(height).width(width).model(getModel()).x(x * zoom).y(y * zoom).scale(zoom));
						newBufferedData.getGraphics().drawImage(tmpBufferedData.getImg(), null, 0, y - startY);
					}

					// LEFT PART (without extension to top and bottom corner)
					if (startX < lastBufferedData.getX()) {
						int y = Math.max(lastBufferedData.getY(), startY);
						int height = Math.min(startY + getHeight(), lastBufferedData.getY() + lastBufferedData.getHeight()) - y;
						int x = startX;
						int width = lastBufferedData.getX() - startX;
						tmpBufferedData = new BufferedImageData(x, y, width, height, getZoom());
						new Test(new Params().height(height).width(width).model(getModel()).x(x * zoom).y(y * zoom).scale(zoom));
						newBufferedData.getGraphics().drawImage(tmpBufferedData.getImg(), null, 0, y - startY);
					}

					// RIGHT PART (without extension to top and bottom corner)
					if (startX + getWidth() > lastBufferedData.getX() + lastBufferedData.getWidth()) {
						int y = Math.max(lastBufferedData.getY(), startY);
						int height = Math.min(startY + getHeight(), lastBufferedData.getY() + lastBufferedData.getHeight()) - y;
						int x = Math.max(lastBufferedData.getX() + lastBufferedData.getWidth(), startX);
						int width = startX + getWidth() - x;
						tmpBufferedData = new BufferedImageData(x, y, width, height, getZoom());
						new Test(new Params().height(height).width(width).model(getModel()).x(x * zoom).y(y * zoom).scale(zoom));
						newBufferedData.getGraphics().drawImage(tmpBufferedData.getImg(), null, x - startX, y - startY);
					}

					// print old part
					newBufferedData.getGraphics().drawImage(lastBufferedData.getImg(), null, lastBufferedData.getX() - startX, lastBufferedData.getY() - startY);

					// some parts might be invalidated (because of map edit), if so then
					// repaint it
					if (invalidRegion != null) {
						int y = (int) Math.max(invalidRegion.getY() / zoom, startY);
						int height = (int) Math.min(invalidRegion.getMaxY() / zoom, startY + getHeight()) - y;
						int x = (int) Math.max(invalidRegion.getX() / zoom, startX);
						int width = (int) Math.min(invalidRegion.getMaxX() / zoom, startX + getWidth()) - x;
						tmpBufferedData = new BufferedImageData(x, y, width, height, getZoom());
						new Test(new Params().height(height).width(width).model(getModel()).x(x * zoom).y(y * zoom).scale(zoom));
						newBufferedData.getGraphics().drawImage(tmpBufferedData.getImg(), null, x - startX, y - startY);
						invalidRegion = null;
					}
				}

				((Graphics2D) g).drawImage(newBufferedData.getImg(), null, 0, 0);
				lastBufferedData = newBufferedData;
				tmpBufferedData = null;
			} catch (DrawingException e) {
				showError(e.getMessage(), e);
			}

		}

		/**
		 * Returns graphics for temporary buffer to draw map.
		 * 
		 * @see #tmpBufferedData
		 * @return graphics for temporary buffer to draw map
		 */
		public Graphics2D getImageTmpGraphics() {
			return tmpBufferedData.getGraphics();
		}

	}

	/**
	 * Transforms mouse coordinates on the {@link JComponent} into coordinates on
	 * the {@link Model}.
	 * 
	 * @param point
	 *          coordinates on {@link JComponent}
	 * @return cooridnates that correspond to position on the {@link Model}
	 */
	public Point2D mouseCoordinatesToPoint2D(Point point) {
		double x = point.getX() + startX;
		double y = point.getY() + startY;
		Point2D result = new Point2D.Double(x * zoom, y * zoom);
		return result;
	}

	/**
	 * @return the model
	 * @see #model
	 */
	Model getModel() {
		return model;
	}

	/**
	 * @return the selectedElements
	 * @see #selectedElements
	 */
	public List<Object> getSelectedElements() {
		return selectedElements;
	}

	/**
	 * @param selectedElements
	 *          the selectedElements to set
	 * @see #selectedElements
	 */
	void setSelectedElements(List<Object> selectedElements) {
		List<Object> oldValue = this.selectedElements;
		this.selectedElements = selectedElements;
		firePropertyChange(SELECTED_ELEMENTS, oldValue, selectedElements);
	}

	/**
	 * Sets new list of selected elements that will contain at most one element.
	 * If the parameter is null then list will be empty.
	 * 
	 * @param object
	 *          object to be included in the new {@link #selectedElements} list
	 */
	void setSelectedElement(Object object) {
		List<Object> elements = new ArrayList<>();
		if (object != null) {
			elements.add(object);
		}
		setSelectedElements(elements);
	}

	/**
	 * Adds region to be redrawn during next
	 * {@link InternalCanvas#paintComponent(Graphics)} call.
	 * 
	 * @param affectedRegion
	 *          region to be redrawn
	 */
	public void addInvalidRegion(Rectangle2D affectedRegion) {
		if (invalidRegion == null) {
			invalidRegion = new Rectangle2D.Double(affectedRegion.getX(), affectedRegion.getY(), affectedRegion.getWidth(), affectedRegion.getHeight());
		} else {
			invalidRegion.add(affectedRegion);
		}
	}

}
