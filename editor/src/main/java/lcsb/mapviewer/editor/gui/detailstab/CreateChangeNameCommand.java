package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.ChangeElementNameCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class with method executed when text property of {@link Element} is changed.
 * 
 */
public class CreateChangeNameCommand extends CreatePropertyChangeCommand {
	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateChangeNameCommand(JTextField field) {
		super(field);
	}

	@Override
	public ModelCommand createCommand(Model model, Element alias) {
		return new ChangeElementNameCommand(model, alias, ((JTextComponent) getField()).getText());
	}
}
