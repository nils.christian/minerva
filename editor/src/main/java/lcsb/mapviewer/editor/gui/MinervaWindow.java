package lcsb.mapviewer.editor.gui;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import lcsb.mapviewer.editor.Workspace;

/**
 * Window with the editor application.
 * 
 * @author Piotr Gawron
 *
 */
public class MinervaWindow extends JFrame {

	/**
	 * {@link javax.swing.JComponent} with content of the application.
	 */
	private WindowContent		 windowContent;

	/**
	 * {@link JMenuBar Menu bar}.
	 */
	private JMenuBar					menuBar;

	/**
	 * File {@link JMenu menu}.
	 */
	private JMenu						 fileMenu;

	/**
	 * Open map {@link JMenuItem menu item}.
	 */
	private JMenuItem				 openMenuItem;

	/**
	 * Exit application {@link JMenuItem menu item}.
	 */
	private JMenuItem				 exitMenuItem;

	/**
	 * {@link Workspace} where all opened projects are stored.
	 */
	private Workspace				 workspace;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public MinervaWindow() {
		workspace = new Workspace();

		setTitle(GuiStyle.EDITOR_TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(GuiStyle.DEFAULT_WIDTH, GuiStyle.DEFAULT_HEIGHT);

		createMenu();

		windowContent = new WindowContent(workspace);

		setContentPane(windowContent);

	}

	/**
	 * Creates {@link JMenuBar}.
	 */
	private void createMenu() {
		menuBar = new JMenuBar();

		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);

		openMenuItem = new JMenuItem("Open file");
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		openMenuItem.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		openMenuItem.addActionListener(new OpenProjectListener(this, workspace));

		fileMenu.add(openMenuItem);

		exitMenuItem = new JMenuItem("Exit");
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new CloseWindowListener(this));

		fileMenu.add(exitMenuItem);

		menuBar.add(fileMenu);

		setJMenuBar(menuBar);
	}
}
