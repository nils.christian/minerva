package lcsb.mapviewer.editor.gui;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * {@link javax.swing.ListCellRenderer Cell renderer} that present
 * {@link MiriamType} in a list.
 * 
 * @author Piotr Gawron
 *
 */
public class MiriamTypeCellRender extends DefaultListCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		if (value instanceof MiriamType) {
			MiriamType mt = (MiriamType) value;
			setText(mt.getCommonName());
		}
		return this;
	}
}
