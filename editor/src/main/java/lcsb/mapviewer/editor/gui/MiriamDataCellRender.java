package lcsb.mapviewer.editor.gui;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * {@link javax.swing.ListCellRenderer Cell renderer} that present
 * {@link MiriamData} in a list.
 * 
 * @author Piotr Gawron
 *
 */
public class MiriamDataCellRender extends DefaultListCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		if (value instanceof MiriamData) {
			MiriamData md = (MiriamData) value;
			setText(md.getDataType().getCommonName() + " (" + md.getResource() + ")");
		}
		return this;
	}
}
