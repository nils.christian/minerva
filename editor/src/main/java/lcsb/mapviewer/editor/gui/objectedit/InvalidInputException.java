package lcsb.mapviewer.editor.gui.objectedit;

/**
 * Exception to be thrown when input data is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidInputException extends Exception {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Default constructor - initializes instance variable to unknown.
	 */

	public InvalidInputException() {
		super(); // call superclass constructor
	}

	/**
	 * Public constructor with parent exception that was catched.
	 * 
	 * @param e
	 *          parent exception
	 */

	public InvalidInputException(final String e) {
		super(e);
	}

	/**
	 * Public constructor with parent exception that was catched.
	 * 
	 * @param e
	 *          parent exception
	 */
	public InvalidInputException(final Exception e) {
		super(e);
	}

	/**
	 * Public constructor with parent exception that was catched.
	 * 
	 * @param message
	 *          exception message
	 * @param e
	 *          parent exception
	 */
	public InvalidInputException(final String message, final Exception e) {
		super(message, e);
	}

}
