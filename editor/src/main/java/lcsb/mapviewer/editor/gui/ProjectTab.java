package lcsb.mapviewer.editor.gui;

import java.awt.BorderLayout;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;

import lcsb.mapviewer.editor.OpenedProject;
import lcsb.mapviewer.editor.gui.canvas.MinervaCanvas;

import org.apache.log4j.Logger;

/**
 * Component representing ab for single project.
 * 
 * @author Piotr Gawron
 *
 */
public class ProjectTab extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger			logger					 = Logger.getLogger(ProjectTab.class);

	/**
	 * Project that is visualized in this tab.
	 */
	private OpenedProject		 project;

	/**
	 * Canvas responsible for visualization.
	 */
	private MinervaCanvas		 canvas;

	/**
	 * Default constructor.
	 * 
	 * @param project
	 *          {@link #project}
	 */
	public ProjectTab(OpenedProject project) {
		this.project = project;

		this.setLayout(new BorderLayout(GuiStyle.GAP_BETWEEN_COMPONENTS, GuiStyle.GAP_BETWEEN_COMPONENTS));

		canvas = new MinervaCanvas(this.project.getModels().iterator().next().getModel());
		this.add(canvas, BorderLayout.CENTER);

		canvas.addPropertyChangeListener(MinervaCanvas.SELECTED_ELEMENTS, new ForwardSelectedElementChangedListener());
	}

	/**
	 * {@link PropertyChangeListener} that forwards calls from
	 * {@link MinervaCanvas#SELECTED_ELEMENTS}.
	 * 
	 * @author Piotr Gawron
	 *
	 */
	class ForwardSelectedElementChangedListener implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			ProjectTab.this.firePropertyChange(MinervaCanvas.SELECTED_ELEMENTS, evt.getOldValue(), evt.getNewValue());
		}

	}

	/**
	 * Adds region to be redrawn during next
	 * {@link MinervaCanvas#paintComponent(java.awt.Graphics)} call.
	 * 
	 * @param affectedRegion
	 *          region to be redrawn
	 */
	public void addInvalidRegion(Rectangle2D affectedRegion) {
		canvas.addInvalidRegion(affectedRegion);
	}
}
