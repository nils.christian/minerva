package lcsb.mapviewer.editor.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

/**
 * {@link ActionListener} that closes application.
 * 
 * @author Piotr Gawron
 *
 */
public class CloseWindowListener implements ActionListener {

	/**
	 * Window which should be closed.
	 */
	private JFrame windowToClose;

	/**
	 * Default constructor.
	 * 
	 * @param windowToClose
	 *          {@link #windowToClose}
	 */
	public CloseWindowListener(JFrame windowToClose) {
		this.windowToClose = windowToClose;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		windowToClose.dispose();
	}

}
