package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.JButton;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.ChangeElementColorCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that creates {@link ChangeElementColorCommand} object from gui input
 * for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeColorCommand extends CreatePropertyChangeCommand {
	/**
	 * Default constructor.
	 * 
	 * @param button
	 *          GUI buton where new color value will be defined
	 */
	CreateChangeColorCommand(JButton button) {
		super(button);
	}

	@Override
	public ModelCommand createCommand(Model model, Element alias) {
		return new ChangeElementColorCommand(model, alias, getField().getBackground());
	}
}
