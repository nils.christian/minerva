package lcsb.mapviewer.editor.gui.objectedit;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Class used to edit {@link String} objects (opens dialog, etc).
 * 
 * @author Piotr Gawron
 *
 */
public class StringEditDialog extends ObjectEditDialog<String> {

	/**
	 * Label of the string to edit.
	 */
	private JLabel					 label;

	/**
	 * Current value of the string entered by user.
	 */
	private JTextField			 text;

	/**
	 * List of objects returned by {@link #getInputs()}.
	 */
	private List<JComponent> inputs = new ArrayList<>();

	/**
	 * Default constructor.
	 * 
	 * @param owner
	 *          {@link ObjectEditDialog#owner}
	 * @param title
	 *          {@link ObjectEditDialog#dialogTitle}
	 * @param propertyName
	 *          name of the String that we edit used in {@link #label}
	 */
	public StringEditDialog(Frame owner, String title, String propertyName) {
		super(owner, title, String.class);
		label = new JLabel(propertyName + ":");
		text = new JTextField();
		inputs.add(label);
		inputs.add(text);
	}

	@Override
	protected List<JComponent> getInputs() {
		return inputs;
	}

	@Override
	protected void updateObjectFromInputs() {
		setObject(text.getText());
	}

	@Override
	protected void updateInputsFromObject() {
		text.setText(getObject());
	}

	@Override
	protected String copyElement(String element) {
		return new String(element);
	}

	@Override
	protected void validateInput() throws InvalidInputException {
		if (text.getText().isEmpty()) {
			throw new InvalidInputException("Value cannot be empty");
		}
	}
}
