package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.ChangeElementAbbreviationCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that creates {@link ChangeElementAbbreviationCommand} object from gui input
 * for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeAbbreviationCommand extends CreatePropertyChangeCommand {
	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateChangeAbbreviationCommand(JTextField field) {
		super(field);
	}

	@Override
	public ModelCommand createCommand(Model model, Element alias) {
		return new ChangeElementAbbreviationCommand(model, alias, ((JTextComponent) getField()).getText());
	}
}
