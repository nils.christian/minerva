package lcsb.mapviewer.editor.gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;

import lcsb.mapviewer.converter.IConverter;

/**
 * This is a {@link FileFilter} that list only files that can be parsed by
 * specific {@link IConverter}.
 * 
 * @author Piotr Gawron
 *
 */
public class ConverterFilter extends FileFilter {

	/**
	 * Converter (parser) that can parse files returned by filter.
	 */
	private IConverter converter;

	/**
	 * Default constructor.
	 * 
	 * @param converter
	 *          {@link #converter}
	 */
	public ConverterFilter(IConverter converter) {
		this.converter = converter;
	}

	@Override
	public boolean accept(File f) {
		return f.isDirectory() || FilenameUtils.getExtension(f.getAbsolutePath()).equalsIgnoreCase(converter.getFileExtension());
	}

	@Override
	public String getDescription() {
		return converter.getCommonName() + " (*." + converter.getFileExtension() + ")";
	}

	/**
	 * @return the converter
	 * @see #converter
	 */
	public IConverter getConverter() {
		return converter;
	}
}
