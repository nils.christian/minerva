package lcsb.mapviewer.editor.gui.detailstab;

import java.util.List;

import javax.swing.JList;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Abstract class that creates {@link ModelCommand commands} to
 * add/remove/modify positions on {@link Element} property that is a list.
 * 
 * @author Piotr Gawron
 *
 * @param <T>
 *          type of the property element
 */
public abstract class CreateListPropertyChangeCommand<T> {

	/**
	 * List where elements are stored.
	 */
	private JList<T> field;

	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateListPropertyChangeCommand(JList<T> field) {
		this.field = field;
	}

	/**
	 * Creates command that should add element to the property list.
	 * 
	 * @param model
	 *          model where element is located
	 * @param alias
	 *          {@link Element} that should be modified
	 * @param newValues
	 *          new values that should be added
	 * @return {@link ModelCommand} that will add new values
	 */
	protected abstract ModelCommand createAddCommand(Model model, Element alias, List<T> newValues);

	/**
	 * Creates command that should remove elements from the property list.
	 * 
	 * @param model
	 *          model where element is located
	 * @param alias
	 *          {@link Element} that should be modified
	 * @param values
	 *          values that should be removed
	 * @return {@link ModelCommand} that will add new values
	 */
	protected abstract ModelCommand createRemoveCommand(Model model, Element alias, List<T> values);

	/**
	 * Creates command that should modify position on property list.
	 * 
	 * @param model
	 *          model where element is located
	 * @param alias
	 *          {@link Element} that should be modified
	 * @param newValue
	 *          new value of the property element
	 * @param oldValue
	 *          old value of the property element
	 * @return {@link ModelCommand} that will add new values
	 */
	protected abstract ModelCommand createChangeCommand(Model model, Element alias, T newValue, T oldValue);

	/**
	 * @return the field
	 * @see #field
	 */
	protected JList<T> getField() {
		return field;
	}

	/**
	 * @param field
	 *          the field to set
	 * @see #field
	 */
	protected void setField(JList<T> field) {
		this.field = field;
	}
}
