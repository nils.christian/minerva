package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.ChangeElementFontSizeCommand;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * Class that creates {@link ChangeElementFontSizeCommand} object from gui input
 * for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeFontSizeCommand extends CreatePropertyChangeCommand {
	
	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	protected CreateChangeFontSizeCommand(JTextField field) {
		super(field);
	}

	@Override
	public ModelCommand createCommand(Model model, Element alias) {
		if (NumberUtils.isNumber(((JTextComponent) getField()).getText())) {
			return new ChangeElementFontSizeCommand(model, alias, Double.valueOf(((JTextComponent) getField()).getText()));
		} else {
			throw new InvalidStateException("Input text is not a double value:" + ((JTextComponent) getField()).getText());
		}
	}
}
