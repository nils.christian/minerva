package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.JComponent;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Abstract class that creates {@link ModelCommand commands} to remove property
 * of {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public abstract class CreatePropertyChangeCommand {

	/**
	 * GUI component that stores the data.
	 */
	private JComponent field;

	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreatePropertyChangeCommand(JComponent field) {
		this.field = field;
	}

	/**
	 * Method that creates a command to modify {@link Element} property.
	 * 
	 * @param model
	 *          {@link Model} where alias is stored
	 * @param alias
	 *          {@link Element} to modify
	 * @return {@link ModelCommand} that will modify alias
	 */
	protected abstract ModelCommand createCommand(Model model, Element alias);

	/**
	 * @return the field
	 * @see #field
	 */
	protected JComponent getField() {
		return field;
	}

	/**
	 * @param field
	 *          the field to set
	 * @see #field
	 */
	protected void setField(JComponent field) {
		this.field = field;
	}
}
