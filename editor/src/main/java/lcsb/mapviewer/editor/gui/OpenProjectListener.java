package lcsb.mapviewer.editor.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.editor.OpenedProject;
import lcsb.mapviewer.editor.Workspace;
import lcsb.mapviewer.model.map.model.Model;

import org.apache.log4j.Logger;

/**
 * Listener used to open files with {@link OpenedProject}.
 * 
 * @author Piotr Gawron
 *
 */
public class OpenProjectListener implements ActionListener {

	/**
	 * Default class logger.
	 */
	private final Logger			 logger		 = Logger.getLogger(OpenProjectListener.class);

	/**
	 * {@link Workspace} where opened project should be added.
	 */
	private Workspace					workspace;

	/**
	 * {@link JFrame Window} that fire this listener.
	 */
	private JFrame						 window;

	/**
	 * Selector used to filter files that are available for opening.
	 */
	private final JFileChooser fc				 = new JFileChooser();

	/**
	 * List of {@link IConverter converters} that can be used to open files.
	 */
	private List<IConverter>	 converters = new ArrayList<>();

	/**
	 * Default constructor.
	 * 
	 * @param window
	 *          {@link JFrame window} from which listener will be fired
	 * @param workspace
	 *          {@link #workspace}
	 */
	public OpenProjectListener(JFrame window, Workspace workspace) {
		this.workspace = workspace;
		this.window = window;
		fillConverters();
		for (IConverter converter : converters) {
			fc.addChoosableFileFilter(new ConverterFilter(converter));
		}
		fc.setAcceptAllFileFilterUsed(false);

	}

	/**
	 * Adds objects to {@link #converters} list.
	 */
	private void fillConverters() {
		CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
		converters.add(cellDesignerXmlParser);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			int returnVal = fc.showOpenDialog(window);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				FileFilter filter = fc.getFileFilter();
				if (filter instanceof ConverterFilter) {
					Model model = ((ConverterFilter) filter).getConverter().createModel(new ConverterParams().filename(file.getAbsolutePath()));
					OpenedProject project = new OpenedProject();
					project.setFileName(file.getAbsolutePath());
					project.addModel(model);
					workspace.addProject(project);
				} else {
					logger.warn("Unknwon filter type: " + filter);
				}
			} else {
				logger.debug("Open command cancelled by user.");
			}
		} catch (FileNotFoundException e1) {
			showError("File doesn't exist.", e1);
		} catch (InvalidInputDataExecption e1) {
			showError("Problem with input file.", e1);
		} catch (Exception e1) {
			showError("Internal error. More information can be found in log file.", e1);
		}
	}

	/**
	 * Shows a message error.
	 * 
	 * @param message
	 *          message error
	 * @param exception
	 *          exception that caused {@link Exception}
	 */
	private void showError(String message, Exception exception) {
		logger.error(exception, exception);
		JOptionPane.showMessageDialog(window, message);
	}

}
