package lcsb.mapviewer.editor.gui;

/**
 * Class with constatnts used in GUI.
 * 
 * @author Piotr Gawron
 *
 */
public final class GuiStyle {
	/**
	 * Title of the editor window.
	 */
	public static final String	EDITOR_TITLE					 = "MINERVa editor";

	/**
	 * Gap between components used in {@link java.awt.BorderLayout layout}
	 * managers.
	 */
	public static final int		 GAP_BETWEEN_COMPONENTS = 5;

	/**
	 * Default width of the window.
	 */
	public static final Integer DEFAULT_WIDTH					= 800;

	/**
	 * Default height of the window.
	 */
	public static final Integer DEFAULT_HEIGHT				 = 600;

	/**
	 * Default height of the
	 * {@link lcsb.mapviewer.editor.gui.detailstab.ElementDetailsPanel
	 * ElementDetailsPanel}.
	 */
	public static final int		 DETAIL_PANEL_HEIGHT		= 400;

	/**
	 * Default width of the
	 * {@link lcsb.mapviewer.editor.gui.detailstab.ElementDetailsPanel
	 * ElementDetailsPanel}.
	 */
	public static final int		 DETAIL_PANEL_WIDTH		 = 300;

	/**
	 * Default height of the {@link javax.swing.JScrollPane JScrollPane} that wrap
	 * text areas.
	 */
	public static final int		 SCROLL_LIST_HEIGHT		 = 80;

	/**
	 * Default constructor preventing instantization.
	 */
	private GuiStyle() {

	}
}
