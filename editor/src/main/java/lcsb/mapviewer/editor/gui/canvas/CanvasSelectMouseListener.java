package lcsb.mapviewer.editor.gui.canvas;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

import org.apache.log4j.Logger;

/**
 * {@link MouseListener} used when canvas is in select mode (allows to select
 * element on the map).
 * 
 * @author Piotr Gawron
 *
 */
public class CanvasSelectMouseListener implements MouseListener {

	/**
	 * When clicking on the map and the click is not perfectly on the line this
	 * distance describe how far from the line click is still considered as
	 * clicking on the line.
	 */
	private static final double MAX_DISTANCE_TO_SELECTED_ELEMENT = 5;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger							logger													 = Logger.getLogger(CanvasSelectMouseListener.class);

	/**
	 * Canvas where drawing is performed.
	 */
	private MinervaCanvas			 canvas;

	/**
	 * Default constructor.
	 * 
	 * @param canvas
	 *          {@link #canvas}
	 */
	public CanvasSelectMouseListener(MinervaCanvas canvas) {
		this.canvas = canvas;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point2D point = canvas.mouseCoordinatesToPoint2D(e.getPoint());

		List<Object> result = new ArrayList<Object>();

		Set<Element> aliases = canvas.getModel().getElements();
		for (Element alias : aliases) {
			if (alias instanceof Species) {
				if (alias.contains(point)) {
					result.add(alias);
				}
			}
		}
		double dist = MAX_DISTANCE_TO_SELECTED_ELEMENT;
		if (result.size() == 0) {
			Reaction r = null;
			Set<Reaction> reactions = canvas.getModel().getReactions();
			for (Reaction reaction : reactions) {
				double newDist = reaction.getDistanceFromPoint(point);
				if (newDist < dist) {
					r = reaction;
					dist = reaction.getDistanceFromPoint(point);
				}
			}
			if (r != null) {
				canvas.setSelectedElement(r);
				return;
			}
		}

		if (result.size() > 1) {
			Element alias = (Element) result.get(0);
			for (Object obj : result) {
				if (((Element) (obj)).getSize() < alias.getSize()) {
					alias = (Element) obj;
				}
			}
			result.clear();
			result.add(alias);
		}
		if (result.size() == 0) {
			canvas.setSelectedElement(null);
		} else {
			canvas.setSelectedElement(result.get(0));
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
