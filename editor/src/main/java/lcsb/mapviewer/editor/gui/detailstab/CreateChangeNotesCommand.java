package lcsb.mapviewer.editor.gui.detailstab;

import javax.swing.text.JTextComponent;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.commands.properties.ChangeElementNotesCommand;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Class that creates {@link ChangeElementNotesCommand} object from gui input
 * for given {@link Model} and {@link Element}.
 * 
 * @author Piotr Gawron
 *
 */
public class CreateChangeNotesCommand extends CreatePropertyChangeCommand {
	/**
	 * Default constructor.
	 * 
	 * @param field
	 *          GUI input from which {@link ModelCommand} will be created
	 */
	CreateChangeNotesCommand(JTextComponent field) {
		super(field);
	}

	@Override
	public ModelCommand createCommand(Model model, Element alias) {
		return new ChangeElementNotesCommand(model, alias, ((JTextComponent) getField()).getText());
	}
}
