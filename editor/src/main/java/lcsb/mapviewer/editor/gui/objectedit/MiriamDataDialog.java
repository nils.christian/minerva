package lcsb.mapviewer.editor.gui.objectedit;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import lcsb.mapviewer.editor.gui.MiriamTypeCellRender;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.MiriamTypeNameComparator;

/**
 * Class used to edit {@link MiriamData} objects (opens dialog, etc).
 * 
 * @author Piotr Gawron
 *
 */
public class MiriamDataDialog extends ObjectEditDialog<MiriamData> {

	/**
	 * Label for {@link #typeList}.
	 */
	private JLabel								typeLabel;

	/**
	 * {@link MiriamType} for currently edited element.
	 */
	private JComboBox<MiriamType> typeList;

	/**
	 * Label for {@link #valueText}.
	 */
	private JLabel								valueLabel;

	/**
	 * Current value of {@link MiriamData#resource}.
	 */
	private JTextField						valueText;

	/**
	 * List of objects returned by {@link #getInputs()}.
	 */
	private List<JComponent>			inputs = new ArrayList<>();

	/**
	 * Default constructor.
	 * 
	 * @param owner
	 *          {@link ObjectEditDialog#owner}
	 * @param title
	 *          {@link ObjectEditDialog#dialogTitle}
	 * @param propertyName
	 *          name of the String that we edit used in {@link #typeLabel}
	 */
	public MiriamDataDialog(Frame owner, String title, String propertyName) {
		super(owner, title, MiriamData.class);
		typeLabel = new JLabel("Entry type: ");
		MiriamType[] types = MiriamType.values();
		Arrays.sort(types, new MiriamTypeNameComparator());
		typeList = new JComboBox<MiriamType>(MiriamType.values());
		typeList.setSelectedIndex(0);
		typeList.setRenderer(new MiriamTypeCellRender());

		valueLabel = new JLabel("Identifier: ");
		valueText = new JTextField();

		inputs.add(typeLabel);
		inputs.add(typeList);

		inputs.add(valueLabel);
		inputs.add(valueText);
	}

	@Override
	protected List<JComponent> getInputs() {
		return inputs;
	}

	@Override
	protected void updateObjectFromInputs() {
		getObject().setDataType((MiriamType) typeList.getSelectedItem());
		getObject().setResource(valueText.getText());
		if (getObject().getRelationType() == null) {
			getObject().setRelationType(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY);
		}
	}

	@Override
	protected void updateInputsFromObject() {
		typeList.setSelectedItem(getObject().getDataType());
		valueText.setText(getObject().getResource());
	}

	@Override
	protected MiriamData copyElement(MiriamData element) {
		MiriamData result = new MiriamData(element);
		return result;
	}

	@Override
	protected void validateInput() throws InvalidInputException {
		if (typeList.getSelectedItem() == null) {
			throw new InvalidInputException("Miriam type cannot be empty");
		} else if (valueText.getText().isEmpty()) {
			throw new InvalidInputException("Identifier cannot be empty");
		}
	}
}
