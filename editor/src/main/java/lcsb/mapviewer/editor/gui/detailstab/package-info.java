/**
 * This package contains classes that implements behaviour of detail s tab about
 * element(s) currently selected on the map. The main class is
 * {@link lcsb.mapviewer.editor.gui.detailstab.ElementDetailsPanel} which
 * agregates the functionality.
 */
package lcsb.mapviewer.editor.gui.detailstab;

