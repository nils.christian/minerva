package lcsb.mapviewer.editor.gui;

import java.awt.event.ActionEvent;

import lcsb.mapviewer.commands.ModelCommand;

/**
 * {@link ActionEvent} represeint event when
 * {@link lcsb.mapviewer.model.map.species.Element} or
 * {@link lcsb.mapviewer.model.map.reaction.Reaction} changed.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *
 */
public class ElementModifiedEvent<T> extends ActionEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Object that changed.
	 */
	private T								 element;

	/**
	 * {@link ModelCommand} used to change the {@link #element}.
	 */
	private ModelCommand			command;

	/**
	 * Default constructor.
	 * 
	 * @param source
	 *          object that caused the event
	 * @param element
	 *          {@link #element}
	 * @param command
	 *          {@link #command}
	 */
	public ElementModifiedEvent(Object source, T element, ModelCommand command) {
		super(source, 0, "Element modified");
		this.element = element;
		this.command = command;
	}

	/**
	 * @return the project
	 * @see #project
	 */
	public T getElement() {
		return element;
	}

	/**
	 * @return the command
	 * @see #command
	 */
	public ModelCommand getCommand() {
		return command;
	}
}
