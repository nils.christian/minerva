package lcsb.mapviewer.editor.gui.objectedit;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.editor.gui.ElementModifiedEvent;
import lcsb.mapviewer.editor.gui.ElementModifiedEventListener;

import org.apache.log4j.Logger;

/**
 * Generic class that is a dialog used for editing objects of a given class.
 * 
 * @author Piotr Gawron
 *
 * @param <T>
 *          type of object to edit
 */
public abstract class ObjectEditDialog<T> {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger													logger		= Logger.getLogger(ObjectEditDialog.class);

	/**
	 * Object that is currently edited.
	 */
	private T																		 object;
	// private T originalObject;

	/**
	 * Type of the object to edit.
	 */
	private Class<T>															clazz;

	/**
	 * Button that accepts edit (or creation of the new object).
	 */
	private JButton															 okButton;

	/**
	 * Button that cancels edit (or creation of the new object).
	 */
	private JButton															 cancelButton;

	/**
	 * Parent {@link Frame} where dialog was created.
	 */
	private Frame																 owner;

	/**
	 * List of listeners that should be fired when edit/creation is finished.
	 */
	private List<ElementModifiedEventListener<T>> listeners = new ArrayList<>();

	/**
	 * Dialog object where edit is performed.
	 */
	private JDialog															 dialog;

	/**
	 * Title of dialog when editing/adding element.
	 */
	private String																dialogTitle;

	/**
	 * Status of closing dialog (was it ok, cancel or maybe there was an error).
	 */
	private DialogResult													resultStatus;

	/**
	 * Default constructor that should be called by specific implementation.
	 * 
	 * @param owner
	 *          {@link #owner}
	 * @param title
	 *          {@link #dialogTitle}
	 * @param clazz
	 *          {@link #clazz}
	 */
	protected ObjectEditDialog(Frame owner, String title, Class<T> clazz) {
		this.clazz = clazz;
		this.owner = owner;
		this.dialogTitle = title;
		createObject();
		okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					validateInput();
					updateObjectFromInputs();
					ElementModifiedEvent<T> event = new ElementModifiedEvent<T>(this, object, null);
					for (ElementModifiedEventListener<T> listener : listeners) {
						listener.actionPerformed(event);
					}
					dialog.setVisible(false);
					setResultStatus(DialogResult.OK);
				} catch (InvalidInputException exception) {
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Validation Error", JOptionPane.ERROR_MESSAGE);
					setResultStatus(DialogResult.ERROR);
				} catch (CommandExecutionException exception) {
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					setResultStatus(DialogResult.ERROR);
				}
			}
		});
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				setResultStatus(DialogResult.CANCEL);
			}
		});

	}

	/**
	 * Creates new empty instance of type {@link #clazz}.
	 * 
	 * @return new empty instance of type {@link #clazz}
	 */
	private T createObject() {
		try {
			Constructor<T> ctor = clazz.getConstructor();
			return ctor.newInstance(new Object[] {});
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new InvalidArgumentException("Cannot create new instance of " + clazz, e);
		}

	}

	/**
	 * Opens dialog to edit parameter object.
	 * 
	 * @param object
	 *          object to edit
	 */
	public void editObject(T object) {
		if (object == null) {
			// this.originalObject = null;
			this.object = createObject();
		} else {
			// this.originalObject = object;
			this.object = copyElement(object);
		}
		dialog = new JDialog(owner, true);
		dialog.setTitle(dialogTitle);
		dialog.setLayout(new GridLayout(0, 2));
		for (JComponent component : getInputs()) {
			dialog.add(component);
		}
		dialog.add(okButton);
		dialog.add(cancelButton);
		dialog.pack();

		updateInputsFromObject();
		dialog.setVisible(true);
	}

	/**
	 * Gets list of components that should be used when editing object.
	 * 
	 * @return list of components that should be used when editing object
	 */
	protected abstract List<JComponent> getInputs();

	/**
	 * Validates input int he {@link #dialog dialog window}. If input is invalid
	 * then {@link InvalidInputException} with proper message is thrown.
	 * 
	 * @throws InvalidInputException
	 *           thrown when data is invalid
	 */
	protected abstract void validateInput() throws InvalidInputException;

	/**
	 * Updates edited object from gui input components.
	 */
	protected abstract void updateObjectFromInputs();

	/**
	 * Updates data in gui input components from edited object.
	 */
	protected abstract void updateInputsFromObject();

	/**
	 * Creates a copy of parameter element.
	 * 
	 * @param element
	 *          object to be copied
	 * @return a copy of parameter element
	 */
	protected abstract T copyElement(T element);

	/**
	 * @return the object
	 * @see #object
	 */
	public T getObject() {
		return object;
	}

	/**
	 * @param object
	 *          the object to set
	 * @see #object
	 */
	protected void setObject(T object) {
		this.object = object;
	}

	/**
	 * Adds listener that should be fired when user finish editing element.
	 * 
	 * @param listener
	 *          new listener to add
	 */
	public void addOkListener(ElementModifiedEventListener<T> listener) {
		listeners.add(listener);
	}

	/**
	 * @return the resultStatus
	 * @see #resultStatus
	 */
	public DialogResult getResultStatus() {
		return resultStatus;
	}

	/**
	 * @param resultStatus
	 *          the resultStatus to set
	 * @see #resultStatus
	 */
	public void setResultStatus(DialogResult resultStatus) {
		this.resultStatus = resultStatus;
	}

	/**
	 * @return the dialogTitle
	 * @see #dialogTitle
	 */
	public String getDialogTitle() {
		return dialogTitle;
	}

	/**
	 * @param dialogTitle
	 *          the dialogTitle to set
	 * @see #dialogTitle
	 */
	public void setDialogTitle(String dialogTitle) {
		this.dialogTitle = dialogTitle;
	}

	/**
	 * Type of status in which {@link ObjectEditDialog} finsihed editing.
	 * 
	 * @author Piotr Gawron
	 *
	 */
	public enum DialogResult {

		/**
		 * User finished editing.
		 */
		OK,

		/**
		 * User canceled editing.
		 */
		CANCEL,

		/**
		 * After user finished editing the error was detected.
		 */
		ERROR;
	}

}
