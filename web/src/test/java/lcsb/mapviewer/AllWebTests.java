package lcsb.mapviewer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.validator.AllValidatorTests;

@RunWith(Suite.class)
@SuiteClasses({ AllValidatorTests.class,//
})
public class AllWebTests {

}
