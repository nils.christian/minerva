package lcsb.mapviewer.validator;

import static org.junit.Assert.fail;

import javax.faces.validator.ValidatorException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class LoginValidatorTest {
	LoginValidator validator = new LoginValidator();

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCorrect() {
		validator.validate(null, null, "piotr.gawron");
	}

	@Test
	public void testIncorrect() {
		try {
			validator.validate(null, null, "piotr gawron");
			fail("Exception expected");
		} catch (ValidatorException e) {

		}
	}

}
