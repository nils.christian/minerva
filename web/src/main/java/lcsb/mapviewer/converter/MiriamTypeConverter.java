package lcsb.mapviewer.converter;

import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.WeakHashMap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

/**
 * Converter used for {@link lcsb.mapviewer.model.map.MiriamType} in primefaces
 * pick list.
 * 
 * @author Piotr Gawron
 * 
 */
@FacesConverter(value = "miriamTypeConverter")
public class MiriamTypeConverter implements Converter {

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger										 logger		= Logger.getLogger(MiriamTypeConverter.class);

	/**
	 * Map between custom uuid and MiriamType object.
	 */
	private static Map<Object, String> entities	= new WeakHashMap<Object, String>();

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object entity) {
		synchronized (entities) {
			if (!entities.containsKey(entity)) {
				String uuid = UUID.randomUUID().toString();
				entities.put(entity, uuid);
				return uuid;
			} else {
				return entities.get(entity);
			}
		}
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String uuid) {
		for (Entry<Object, String> entry : entities.entrySet()) {
			if (entry.getValue().equals(uuid)) {
				return entry.getKey();
			}
		}
		return null;
	}

}
