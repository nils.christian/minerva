package lcsb.mapviewer.validator;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validator of login field.
 * 
 * @author Piotr Gawron
 *
 */
@FacesValidator("loginValidator")
public class LoginValidator implements Validator {

	/**
	 * Regex pattern used for login validation.
	 */
	private static final Pattern LOGIN_PATTERN = Pattern.compile("[a-zA-Z0-9_\\.\\-]+");

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value == null) {
			return; // Let required="true" handle.
		}
		if (!LOGIN_PATTERN.matcher((String) value).matches()) {
			String summary = "Incorrect login.";
			String detail = "Only alphanumeric characters and \"-\", \"_\", \".\" special characters are allowed";
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
		}
	}

}