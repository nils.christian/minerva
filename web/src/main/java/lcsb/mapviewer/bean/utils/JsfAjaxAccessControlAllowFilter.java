package lcsb.mapviewer.bean.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * This filter enables ajax queries from all domains. It should be used for
 * restfull API.
 * 
 * @author Piotr Gawron
 *
 */
public class JsfAjaxAccessControlAllowFilter implements Filter {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(JsfAjaxAccessControlAllowFilter.class);

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		response.addHeader("Access-Control-Allow-Origin", "*");
		chain.doFilter(req, response);
	}

	@Override
	public void destroy() {
	}

}
