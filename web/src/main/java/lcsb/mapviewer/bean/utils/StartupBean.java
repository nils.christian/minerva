package lcsb.mapviewer.bean.utils;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;

/**
 * Bean where init script of the application is placed. The method in this bean
 * should be called only once, during application start.
 * 
 * @author Piotr Gawron
 *
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class StartupBean {

  /**
   * Default class logger.
   */
  private final transient Logger logger = Logger.getLogger(StartupBean.class);

  /**
   * Dao used to access information about projects.
   * 
   */
  @ManagedProperty(value = "#{ProjectService}")
  private transient IProjectService projectService;

  /**
   * Service used to access configuration params from db.
   * 
   * @see IProjectService
   */
  @ManagedProperty(value = "#{ConfigurationService}")
  private transient IConfigurationService configurationService;

  /**
   * Service used to access information about reference genomes.
   * 
   * @see IReferenceGenomeService
   */
  @ManagedProperty(value = "#{ReferenceGenomeService}")
  private transient IReferenceGenomeService referenceGenomeService;

  /**
   * Method that process initial script of application.
   */
  @PostConstruct
  public void init() {
    logger.debug("Application startup script starts");
    ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
        .getContext();
    Configuration.setWebAppDir(servletContext.getRealPath(".") + "/../");

    setInterruptedProjectsStatuses();
    modifyXFrameDomain();
    removeInterruptedReferenceGenomeDownloads();
    logger.debug("Application startup script ends");
  }

  private void modifyXFrameDomain() {
    try {
      for (String domain : configurationService.getConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN)
          .split(";")) {
        Configuration.getxFrameDomain().add(domain);
      }
    } catch (Exception e) {
      logger.error("Problem with modyfing x frame domain...", e);
    }
  }

  /**
   * Removes downloads of reference genomes that were interrupted by tomcat
   * restart.
   */
  private void removeInterruptedReferenceGenomeDownloads() {
    try {
      for (ReferenceGenome genome : referenceGenomeService.getDownloadedGenomes()) {
        if (genome.getDownloadProgress() < IProgressUpdater.MAX_PROGRESS) {
          logger.warn("Removing genome that was interrupted: " + genome);
          try {
            referenceGenomeService.removeGenome(genome);
          } catch (IOException e) {
            logger.error("Problem with removing genome: " + genome);
          }
        }
      }
    } catch (Exception e) {
      logger.error("Problem with removing interrupted downloads...", e);
    }

  }

  /**
   * Set {@link ProjectStatus#FAIL} statuses to projects that were uploading when
   * application was shutdown.
   */
  private void setInterruptedProjectsStatuses() {
    try {
      for (Project project : projectService.getAllProjects()) {
        if (!ProjectStatus.DONE.equals(project.getStatus()) && !ProjectStatus.FAIL.equals(project.getStatus())) {
          String errors = project.getErrors();
          if (errors == null || errors.trim().isEmpty()) {
            errors = "";
          } else {
            errors = errors.trim() + "\n";
          }
          errors += "Project uploading was interrupted by application restart (at the stage: " + project.getStatus()
              + ").";
          project.setStatus(ProjectStatus.FAIL);
          project.setErrors(errors);
          projectService.updateProject(project, null);
          logger.info("Status of project: " + project.getProjectId() + " changed to fail. Errors: " + errors);
        }
      }
    } catch (Exception e) {
      logger.error("Problem with changing project status ...", e);
    }
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * @return the referenceGenomeService
   * @see #referenceGenomeService
   */
  public IReferenceGenomeService getReferenceGenomeService() {
    return referenceGenomeService;
  }

  /**
   * @param referenceGenomeService
   *          the referenceGenomeService to set
   * @see #referenceGenomeService
   */
  public void setReferenceGenomeService(IReferenceGenomeService referenceGenomeService) {
    this.referenceGenomeService = referenceGenomeService;
  }

  public IProjectService getProjectService() {
    return projectService;
  }

  public void setProjectService(IProjectService projectService) {
    this.projectService = projectService;
  }

}
