package lcsb.mapviewer.bean.utils;

import java.util.Map;

import javax.faces.component.UIComponent;

/**
 * Intefrace of the class that access data about connection and PF framework in
 * general.
 * 
 * @author Piotr Gawron
 *
 */
public interface IPrimefacesUtils {
	/**
	 * Returns value of the request parameter.
	 * 
	 * @param name
	 *          name of the parameter
	 * @return value of the request parameter
	 */
	String getRequestParameter(String name);

	/**
	 * Returns path on hard drive to the project.
	 * 
	 * @return path where the projest is run
	 */
	String getPath();

	/**
	 * Returns ip address of current client.
	 * 
	 * @return IP addresss of the client
	 */
	String getClientIpAddress();

	/**
	 * Sends an error message to the client.
	 * 
	 * @param message
	 *          error message to be sent
	 */
	void error(String message);

	/**
	 * Sends an info message to client.
	 * 
	 * @param message
	 *          info message to be sent
	 */
	void info(String message);

	/**
	 * Returns version of primefaces.
	 * 
	 * @return version of primefaces library
	 */
	String getVersion();

	/**
	 * Returns a {@link UIComponent} corresponding to the client side component
	 * with the identifier given in the parameter.
	 * 
	 * @param id
	 *          identifier of a component
	 * @return {@link UIComponent} corresponding to the client side component with
	 *         the identifier given in the parameter
	 */
	UIComponent findComponent(String id);

	/**
	 * Adds parameter to a ajax response.
	 * 
	 * @param key
	 *          name of the parameter
	 * @param value
	 *          value of the parameter
	 */
	void addCallbackParam(String key, Object value);

	/**
	 * Returns request parameters map.
	 * 
	 * @return request parameters map
	 */
	Map<String, String> getRequestParameterMap();

	/**
	 * Adds a parameter that will be stored in user session.
	 * 
	 * @param key
	 *          name of the parameter
	 * @param value
	 *          value of the parameter
	 */
	void addSessionParam(String key, Object value);

	/**
	 * Gets a value of the parameter stored in user session.
	 * 
	 * @param key
	 *          name of the parameter
	 * @return value of the user session parameter
	 */
	Object getSessionParam(String key);

	/**
	 * Creates http session if session hasn't been created yet.
	 */
	void createSession();

	/**
	 * Returns bean identified by {@link Class}.
	 * 
	 * @param clazz
	 *          class of the bean
	 * @param <T>
	 *          class of the bean
	 * @return bean identified by {@link Class}
	 */
	<T> T findBean(Class<T> clazz);

	/**
	 * Executes javascript code on client.
	 * 
	 * @param javascript
	 *          code to execute
	 */
	void executeJavascript(String javascript);

}