package lcsb.mapviewer.bean.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * This class should be used only for debug purpose. It wrapps
 * {@link ServletResponse} into {@link CustomHttpServletResponseWrapper} which
 * allows to follow what is going on in {@link Filter filters} and how data is
 * obtained by JSF.
 * 
 * @author Piotr Gawron
 *
 */
public class CustomFilter implements Filter {
	/**
	 * Default class logger.
	 */
	private final Logger logger = Logger.getLogger(CustomFilter.class);

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		logger.debug("URI: " + request.getRequestURI());
		logger.debug("URL: " + request.getRequestURL());
		logger.debug("val: " + response.getBufferSize());
		chain.doFilter(req, new CustomHttpServletResponseWrapper(response));
		logger.debug("Content after: " + response.getContentType());
	}

	@Override
	public void destroy() {
	}

}
