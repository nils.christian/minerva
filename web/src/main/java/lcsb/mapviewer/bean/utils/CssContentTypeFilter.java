package lcsb.mapviewer.bean.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lcsb.mapviewer.common.MimeType;

import org.apache.log4j.Logger;

/**
 * This class sets content type for requests accessing css files. We have to do
 * it as sometimes JSF forgets to set content type and Internet Explorer is
 * going crazy because of that.
 * 
 * @author Piotr Gawron
 *
 */
public class CssContentTypeFilter implements Filter {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private final Logger logger = Logger.getLogger(CssContentTypeFilter.class);

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		if (request.getContentType() == null && request.getRequestURL().toString().contains("css.xhtml")) {
			response.setContentType(MimeType.CSS.getTextRepresentation());
			response.setHeader("Content-Type", MimeType.CSS.getTextRepresentation());
		}
		chain.doFilter(req, response);
	}

	@Override
	public void destroy() {
	}

}
