package lcsb.mapviewer.bean.utils;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;
import javax.faces.application.ResourceWrapper;

import lcsb.mapviewer.services.impl.ConfigurationService;

import org.apache.log4j.Logger;

/**
 * This class is used to provide url for jsf resource files with "?v=version"
 * addition that will make sure that when we update files from git repository
 * browser will reload them.
 * 
 * @author Piotr Gawron
 *
 */
public class VersionResourceHandler extends ResourceHandlerWrapper {
	/**
	 * Default class logger.
	 */
	private final Logger		logger	= Logger.getLogger(VersionResourceHandler.class);

	/**
	 * SVN version of the system.
	 */
	private static String		version	= null;

	/**
	 * Resource for which we generate modified url.
	 */
	private ResourceHandler	wrapped;

	/**
	 * Default constructor.
	 * 
	 * @param wrapped
	 *          {@link #wrapped}
	 */
	public VersionResourceHandler(ResourceHandler wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public Resource createResource(String resourceName) {
		return createResource(resourceName, null, null);
	}

	@Override
	public Resource createResource(String resourceName, String libraryName) {
		return createResource(resourceName, libraryName, null);
	}

	@Override
	public Resource createResource(String resourceName, String libraryName, String contentType) {
		final Resource resource = super.createResource(resourceName, libraryName, contentType);

		if (resource == null) {
			return null;
		}

		return new ResourceWrapper() {

			@Override
			public String getRequestPath() {
				return super.getRequestPath() + "&m_version=" + getVersion();
			}

			@Override
			public Resource getWrapped() {
				return resource;
			}
			@Override
			public String getContentType() {
				return getWrapped().getContentType();
			}
			@Override
			public String getResourceName() {
				return getWrapped().getResourceName();
			}
			@Override
			public String getLibraryName() {
				return getWrapped().getLibraryName();
			}
		};
	}

	@Override
	public ResourceHandler getWrapped() {
		return wrapped;
	}

	/**
	 * This method returns git version of the framework.
	 * 
	 * @return git version of the framework
	 */
	private String getVersion() {
		if (version == null) {
			try {
				version = new ConfigurationService().getSystemGitVersion(new PrimefacesUtils().getPath());
			} catch (Exception e) {
				logger.error(e, e);
				version = "UNKNOWN";
			}
		}
		return version;
	}

}