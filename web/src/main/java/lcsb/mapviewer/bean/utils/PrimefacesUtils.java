package lcsb.mapviewer.bean.utils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.springframework.web.jsf.FacesContextUtils;

/**
 * Util class containing some useful methods in Primefaces.
 * 
 * @author Piotr Gawron
 * 
 */
public class PrimefacesUtils implements IPrimefacesUtils {
	/**
	 * Default class logger.
	 */
	private final transient Logger	logger	= Logger.getLogger(PrimefacesUtils.class);

	/**
	 * Default constructor.
	 */
	public PrimefacesUtils() {

	}

	@Override
	public String getRequestParameter(final String name) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(name);
	}

	@Override
	public String getPath() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context != null) {
			return context.getExternalContext().getRealPath("/");
		} else {
			String path;
			try {
				path = new File(".").getCanonicalPath() + "/";
			} catch (IOException e) {
				logger.warn("Problem with detecting current path", e);
				path = "";
			}
			return path;
		}

	}

	@Override
	public String getClientIpAddress() {
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String ipAddress = httpServletRequest.getRemoteAddr();
		if (ipAddress == null) {
			ipAddress = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("X-FORWARDED-FOR");
		}
		return ipAddress;
	}

	@Override
	public void error(final String message) {
		logger.debug("Error sent to PF: " + message);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", message));
	}

	@Override
	public void info(final String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message));
	}

	@Override
	public String getVersion() {
		return RequestContext.getCurrentInstance().getApplicationContext().getConfig().getBuildVersion();
	}

	@Override
	public UIComponent findComponent(String id) {
		return FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
	}

	@Override
	public void addCallbackParam(String key, Object value) {
		RequestContext.getCurrentInstance().addCallbackParam(key, value);
	}

	@Override
	public Map<String, String> getRequestParameterMap() {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
	}

	@Override
	public void addSessionParam(String key, Object value) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
	}

	@Override
	public Object getSessionParam(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
	}

	@Override
	public void createSession() {
		FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	}

	@Override
	public <T> T findBean(Class<T> clazz) {
		return FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(clazz);
	}

	@Override
	public void executeJavascript(String javascript) {
		RequestContext.getCurrentInstance().execute(javascript);		
	}

}
