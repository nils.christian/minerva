package lcsb.mapviewer.bean.utils;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.UrlValidator;
import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Configuration;

/**
 * This filter enables x-frames from another domain if necessary.
 * 
 * @author Piotr Gawron
 *
 */
public class XFrameAccessControlFilter implements Filter {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = Logger.getLogger(XFrameAccessControlFilter.class);

  @Override
  public void init(FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    HttpServletResponse response = (HttpServletResponse) res;
    List<String> domains = Configuration.getxFrameDomain();

    String value = "frame-ancestors ";
    for (String domain : domains) {
      if (new UrlValidator().isValid(domain) || (domain != null && domain.contains("localhost"))) {
        value += domain + " ";
      }
    }
    
    if (!value.equals("frame-ancestors ")) {
      response.addHeader("Content-Security-Policy", value);
    } else {
      response.addHeader("X-Frame-Options", "DENY");
    }
    chain.doFilter(req, response);
  }

  @Override
  public void destroy() {
  }

}
