package lcsb.mapviewer.security;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.services.interfaces.IUserService;

@Transactional(readOnly = false)
public class CustomAuthenticationProvider implements AuthenticationProvider {
  Logger logger = Logger.getLogger(CustomAuthenticationProvider.class);

  private IUserService userService;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    // login
    String name = authentication.getName();

    // password
    String password = null;
    if (authentication.getCredentials() != null) {
      password = authentication.getCredentials().toString();
    }

    // Your custom authentication logic here
    if (userService.login(name, password) != null) {
      Authentication auth = new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
      return auth;
    }
    throw new BadCredentialsException("Invalid credentials");
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }

  public IUserService getUserService() {
    return userService;
  }

  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

}
