package lcsb.mapviewer.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lcsb.mapviewer.common.Configuration;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

/**
 * Implementation of Spring Authentication Success Handler. When authentication
 * is success, it will redirect user to the web page that had accessed before
 * authentication took place.
 *
 * @author Piotr Gawron
 */

public class MvAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
  /**
   * Default session expire time in seconds (120 minutes).
   */
  private static final Integer MAX_INACTIVE_INTERVAL = 120 * 60;

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException,
          ServletException {

    request.getSession().setMaxInactiveInterval(MAX_INACTIVE_INTERVAL);

    SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
    String url = request.getParameter("from");


    // if we are not redirecting from somewhere then
    if ((url != null && !url.isEmpty())) {
      logger.debug("Found redirect URL");
    } else if (savedRequest == null) {
      logger.debug(request.getRequestURL());
      //redirect to the main page
      url = request.getRequestURL().toString().replace(request.getServletPath(), "") + Configuration.MAIN_PAGE;

      String queryString = request.getQueryString();
      // and don't forget about parameters
      if (queryString != null) {
        url += "?" + queryString;
      }
    } else {
      url = savedRequest.getRedirectUrl();
    }

    logger.debug("Redirecting: " + url);

    if (!response.isCommitted()) {
      response.sendRedirect(url);
      return;
    } else {
      logger.warn("If you see this then something is wrong...");
    }

    super.onAuthenticationSuccess(request, response, authentication);
  }
}
