package lcsb.mapviewer.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * Implementation of Spring Authentication Failure Handler. When authentication
 * fail, it will redirect user to the webpage defined in configuration xml.
 * 
 * @author Piotr Gawron
 * 
 */
public class MvAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	/**
	 * Constructor with the redirect url.
	 * 
	 * @param defaultFailureUrl
	 *          default redirect url
	 */
	public MvAuthenticationFailureHandler(String defaultFailureUrl) {
		super(defaultFailureUrl);
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException,
			ServletException {
		super.onAuthenticationFailure(request, response, exception);
	}
}
