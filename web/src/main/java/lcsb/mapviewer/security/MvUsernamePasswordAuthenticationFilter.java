package lcsb.mapviewer.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * No idea about the reason why this class is put here...
 * 
 * @author Piotr Gawron
 * 
 */
public class MvUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	/**
	 * Default class logger.
	 */
	private static Logger	logger	= Logger.getLogger(MvUsernamePasswordAuthenticationFilter.class);

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
			throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		logger.info("successfulAuthentication");

	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException,
			ServletException {
		super.unsuccessfulAuthentication(request, response, failed);
		logger.info("unsuccessfulAuthentication");
	}

}
