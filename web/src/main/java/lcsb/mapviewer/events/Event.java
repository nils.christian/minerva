package lcsb.mapviewer.events;


/**
 * Abstract event object that might be handled by {@link Listener} class.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class Event {
}
