package lcsb.mapviewer.run;

import java.io.File;
import java.io.FileNotFoundException;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * Class with some PD map specific information.
 * 
 * @author Piotr Gawron
 * 
 */
public final class PdMapAnnotations {

	/**
	 * Default class constructor. Prevent initialization.
	 */
	private PdMapAnnotations() {

	}

	/**
	 * Returns url to pdmap with reaction selected on it.
	 * 
	 * @param reaction
	 *          reaction to be selected
	 * @return url to the reaction in pd map
	 */
	public static String getLinkForReaction(Reaction reaction) {
		return Configuration.PUBLICALY_AVAILABLE_PD_MAP + "&search=reaction:" + reaction.getIdReaction();
	}

	/**
	 * Returns filename of the last available version of pd project from testFiles
	 * directory.
	 * 
	 * @return filename of the last available version of pd project
	 * @throws FileNotFoundException
	 *           thrown when file cannot be found
	 */
	public static String getLastPdFilename() throws FileNotFoundException {
		String result = null;
		File folder = new File("testFiles/pd_full/");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {

			if (listOfFiles[i].isFile() && listOfFiles[i].getAbsolutePath().toLowerCase().endsWith("xml")) {
				if (result == null) {
					result = listOfFiles[i].getAbsolutePath();
				} else {
					if (result.compareTo(listOfFiles[i].getAbsolutePath()) < 0) {
						result = listOfFiles[i].getAbsolutePath();
					}
				}
			}
		}
		if (result == null) {
			throw new FileNotFoundException();
		} else {
			return result;
		}
	}

	/**
	 * Returns version of the last pd file project.
	 * 
	 * @return version of the last pd file project
	 */
	public static String getLastPdVersion() {
		String result = null;
		try {
			String[] tokens = getLastPdFilename().split("[\\/\\\\]");
			result = tokens[tokens.length - 1].replace(".xml", "");
		} catch (FileNotFoundException e) {
			return null;
		}
		return result;
	}
}
