package lcsb.mapviewer.run;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class is a snippet for Vibine collaboration project. It shows how to
 * access our internal data structures and source files.
 * 
 * @author Piotr Gawron
 * 
 */
public class VibineSnippet {

	/**
	 * Number of nanoseconds in second.
	 */
	private static final double	NANOSECONDS_IN_SECOND	= 1000000000.0;

	/**
	 * CellDesigner file on with some data that is used in this snippet (it's PD
	 * map from June 2015).
	 */
	private static final String	INPUT_FILE						= "testFiles/pd_full/PD_150625_3.xml";

	/**
	 * Default class logger.
	 */
	private final Logger				logger								= Logger.getLogger(VibineSnippet.class);

	/**
	 * Main entry point to the program.
	 * 
	 * @param args
	 *          parameters with which the program was run
	 */
	public static void main(String[] args) {
		VibineSnippet main = new VibineSnippet();
		long startTime = System.nanoTime();

		try {
			main.run();
		} catch (Exception e) {
			main.logger.error(e, e);
		} finally {
			long endTime = System.nanoTime();
			long duration = endTime - startTime;
			double sec = duration / NANOSECONDS_IN_SECOND;
			System.out.println("Duration: " + new DecimalFormat("#.###").format(sec) + "s");
		}
	}

	/**
	 * This method transform {@link #inputOptions input data}.
	 * 
	 * @throws FileNotFoundException
	 * 
	 * @throws Exception
	 *           thrown when the there is a problem...
	 */
	private void run() throws Exception {
		logger.debug("Opening file: " + INPUT_FILE);

		CellDesignerXmlParser parser = new CellDesignerXmlParser();
		Model model = parser.createModel(new ConverterParams().filename(INPUT_FILE));

		logger.debug("File processed.");

		logger.debug("-------------------------------");
		logger.debug("Number of elements: " + model.getElements().size());

		logger.debug("Number of reactions: " + model.getReactions().size());

		logger.debug("-------------------------------");
		String speciesId = "sa7208";
		String reactionId = "re332";

		Species species = model.getElementByElementId(speciesId);

		logger.debug("Element with id: " + speciesId);
		logger.debug("Element type: " + species.getClass());
		logger.debug("Element name: " + species.getName());

		logger.debug("-------------------------------");
		Reaction reaction = model.getReactionByReactionId(reactionId);

		logger.debug("Reaction with id: " + reactionId);
		logger.debug("Reaction has " + reaction.getReactionNodes().size() + " members.");
		for (ReactionNode node : reaction.getReactionNodes()) {
			logger.debug(
					node.getClass().getSimpleName() + "; points to " + node.getElement().getClass().getSimpleName() + "[" + node.getElement().getElementId() + "]");
		}
		logger.debug("-------------------------------");
	}

}
