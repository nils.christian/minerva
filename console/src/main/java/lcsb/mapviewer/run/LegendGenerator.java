package lcsb.mapviewer.run;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;

/**
 * This class is responsible for creation of legend.PNG from CellDesigner file.
 * 
 * @author Piotr Gawron
 * 
 */
public class LegendGenerator {

	/**
	 * This enum contains predefined legend images as CellDEsigner files.
	 * 
	 * @author Piotr Gawron
	 * 
	 */
	private enum LegendObject {
		// /**
		// * First legend element.
		// */
		// LEGEND_A("testFiles/legend/legend_5_2.xml", "legend_a.png", 50, 0, 600,
		// 480),
		// /**
		// * Second legend element.
		// */
		// LEGEND_B("testFiles/legend/legend_4_3.xml", "legend_b.png", 920, 0, 600,
		// 480),
		// /**
		// * Third legend element.
		// */
		// LEGEND_C("testFiles/legend/legend_4_4.xml", "legend_c.png", 500, 0, 600,
		// 480),
		/**
		 * Fourth legend element.
		 */
		LEGEND_D("C:/Users/piotr.gawron/Desktop/test.xml", "legend_d.png", 0, 0, 1280, 480);

		/**
		 * File with the legend data in CellDEsigner format.
		 */
		private String	inputFile	 = null;
		/**
		 * Where the legend should be saved.
		 */
		private String	outputFile = null;
		/**
		 * X coordinate where the image in input file starts.
		 */
		private int			startX		 = 0;
		/**
		 * Y coordinate where the image in input file starts.
		 */
		private int			startY		 = 0;
		/**
		 * Width of the legend image.
		 */
		private Integer	width			 = null;
		/**
		 * Height of the legend image.
		 */
		private Integer	height		 = null;

		/**
		 * Default constructor.
		 * 
		 * @param input
		 *          {@link #inputFile}
		 * @param output
		 *          {@link #outputFile}
		 * @param startX
		 *          {@link #startX}
		 * @param startY
		 *          {@link #startY}
		 * @param width
		 *          {@link #width}
		 * @param height
		 *          {@link #height}
		 */
		LegendObject(String input, String output, int startX, int startY, int width, int height) {
			this.inputFile = input;
			this.outputFile = output;
			this.startX = startX;
			this.startY = startY;
			this.width = width;
			this.height = height;
		}
	}

	/**
	 * Default class logger.
	 */
	private static Logger				logger			= Logger.getLogger(LegendGenerator.class.getName());

	/**
	 * What zoom factor should be used to generate legend files.
	 */
	private static final double	ZOOM_FACTOR	= 1.4;

	/**
	 * Static main method used to run this stand alone code.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		try {
			new LegendGenerator().run();

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Unknown exception: " + e.getMessage());
		}
	}

	/**
	 * Creates all legend images.
	 * 
	 * @throws IOException
	 *           thrown when there are some general problems with files
	 * @throws DrawingException
	 *           thrown when there was a problem with drawing one of the maps
	 * @throws InvalidInputDataExecption
	 */
	public void run() throws Exception {
		for (LegendObject lo : LegendObject.values()) {
			createLegend(lo);
		}
	}

	/**
	 * Creates single legend file.
	 * 
	 * @param lo
	 *          type of the legend file to create
	 * @throws IOException
	 *           thrown when there are some general problems with input/output
	 *           file
	 * @throws DrawingException
	 *           thrown when there was a problem with drawing a legend
	 * @throws InvalidInputDataExecption
	 *           thrown when xml file is invalid
	 */
	private void createLegend(LegendObject lo) throws Exception {
		Model model = new CellDesignerXmlParser().createModel(new ConverterParams().filename(lo.inputFile));

		// and save it to leged.png
		String fileName = lo.outputFile;

		Model copy = new CopyCommand(model).execute();
		PngImageGenerator generator = new PngImageGenerator(
				new Params().scale(ZOOM_FACTOR).x(lo.startX).y(lo.startY).width(lo.width).height(lo.height).model(copy).level(2).nested(true));
		generator.saveToFile(fileName);
		Desktop.getDesktop().open(new File(fileName));
	}
}
