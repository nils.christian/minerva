/**
 * Provides some clases that can be run from command line and perform some operations on the data:<ul> 
 * <li>extract information,</li>
 * <li>add information,</li>
 * <li>remove data,</li>
 * <li>validate data.</li>
 * </ul>
 */
package lcsb.mapviewer.run;

