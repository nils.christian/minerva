package lcsb.mapviewer.run;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.ApplicationContextLoader;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.utils.DataSourceUpdater;
import lcsb.mapviewer.reactome.utils.ReactomeQueryUtil;

/**
 * This class create a data that could be used by visualized in reactome
 * visualization framework. It was developed only for test purpose and has no
 * real value.
 */
public class ReactomeExport {
	/**
	 * Size of the map to vizualize.
	 */
	private static final int	DEFAULT_SIZE = 30000;
	/**
	 * Bounds of the map to vizualize.
	 */
	private static Rectangle	bound				 = new Rectangle(0, 0, DEFAULT_SIZE, DEFAULT_SIZE);
	/**
	 * Default class logger.
	 */
	private static Logger			logger			 = Logger.getLogger(ReactomeExport.class.getName());

	/**
	 * Class used for accessing reactome data.
	 */
	@Autowired
	private DataSourceUpdater	rc;

	/**
	 * Util class used for manipulating information in reactome objects.
	 */
	@Autowired
	private ReactomeQueryUtil	rcu;

	/**
	 * Static main method used to run this stand alone code.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		PropertyConfigurator.configure("src/main/webapp/WEB-INF/resources/log4j.properties");
		ReactomeExport main = new ReactomeExport();
		ApplicationContextLoader.loadApplicationContext("consoleApplicationContext.xml");
		ApplicationContextLoader.injectDependencies(main);
		main.run();

	}

	/**
	 * String containing the result of export.
	 */
	private String result = "";

	/**
	 * Adds text to the output.
	 * 
	 * @param str
	 *          text to add
	 */
	private void print(String str) {
		result += str;
	}

	/**
	 * Identifier counter used for generating identifiers in the reactome
	 * visualization model.
	 */
	private int									id			= 0;

	/**
	 * Map between aliases in our model and identifiers that will be used in the
	 * reactome vizualization model.
	 */
	private Map<Element, Integer>	tempIds	= new HashMap<Element, Integer>();

	/**
	 * Creates reactome identifier for alias.
	 * 
	 * @param alias
	 *          object for which identifier is created
	 * @return identifier of the alias
	 */
	public int addElement(Element alias) {
		int result = -1;
		if (tempIds.containsKey(alias)) {
			result = tempIds.get(alias);
		} else {
			tempIds.put(alias, ++id);
			result = id;

			if (alias instanceof Complex) {
				for (Element a : ((Complex) alias).getElements()) {
					addElement(a);
				}
			} else if (alias instanceof Compartment) {
				for (Element a : ((Compartment) alias).getElements()) {
					addElement(a);
				}
			}
		}

		return result;
	}

	/**
	 * Creates file with reactome vizualization.
	 */
	private void run() {
		CellDesignerXmlParser p = new CellDesignerXmlParser();
		try {
			Model model = p.createModel(new ConverterParams().filename(PdMapAnnotations.getLastPdFilename()));
			print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			print("<Process reactomeId=\"0\">\n");
			print("<Properties>\n");
			print("<displayName>PD map</displayName>\n");
			print("</Properties>\n");
			printAliases(model);
			printReactions(model);
			print("</Process>\n");

			PrintWriter writer = new PrintWriter("out/report/reactome-viewer.xml", "UTF-8");
			writer.print(result);
			writer.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * Creates representation of the aliases in the reactome visualization model.
	 * 
	 * @param model
	 *          our model to be visualized
	 * @throws IOException
	 *           thrown when there are problems accessing reactome database
	 */
	private void printAliases(Model model) throws IOException {
		print("<Nodes>\n");
		List<Element> aliases = new ArrayList<Element>();
		aliases.addAll(model.getElements());
		Collections.sort(aliases, Element.SIZE_COMPARATOR);
		for (Element alias : aliases) {
			if (!aliasFeasible(alias)) {
				continue;
			}
			int currId = addElement(alias);
			Map<String, String> parameters = new HashMap<>();
			Map<String, String> attributes = new HashMap<>();

			attributes.put("id", currId + "");
			attributes.put("position", (int) alias.getCenterX() + " " + (int) alias.getCenterY());
			attributes
					.put("bounds", alias.getX().intValue() + " " + alias.getY().intValue() + " " + alias.getWidth().intValue() + " " + alias.getHeight().intValue());
			attributes.put("textPosition", (int) alias.getCenterX() + " " + (int) alias.getCenterY());
			attributes.put("reactomeId", "0");

			Set<Integer> components = new HashSet<Integer>();
			String type = "";
			if (alias instanceof Species) {
				String stableIdentifier = rcu.getReactomeIdentifierForSpecies(alias);
				if (stableIdentifier != null) {
					ReactomeDatabaseObject obj = rc.getFullObjectForStableIdentifier(stableIdentifier);
					if (obj != null && obj.getDbId() != null) {
						attributes.put("reactomeId", obj.getDbId() + "");
					}
				}
				parameters.put("displayName", alias.getName());
				if (alias instanceof Complex) {
					for (Element a : ((Complex) alias).getElements()) {
						components.add(addElement(a));
					}
				}
			}
			if (alias instanceof Compartment) {
				parameters.put("displayName", alias.getName());
				for (Element a : ((Compartment) alias).getElements()) {
					components.add(addElement(a));
				}
			}

			if (alias instanceof Complex) {
				type = "org.gk.render.RenderableComplex";
				attributes.put("hideComponents", "true");
				attributes.put("textPosition", (int) alias.getCenterX() + " " + (int) (alias.getY() + alias.getHeight()));
			} else if (alias instanceof SimpleMolecule) {
				type = "org.gk.render.RenderableChemical";
			} else if (alias instanceof Ion) {
				type = "org.gk.render.RenderableChemical";
			} else if (alias instanceof Protein) {
				type = "org.gk.render.RenderableProtein";
			} else if (alias instanceof Species) {
				type = "org.gk.render.RenderableEntity";
			} else if (alias instanceof Compartment) {
				type = "org.gk.render.RenderableCompartment";
			} else {
				logger.debug("Ignoring: " + parameters.get("displayName") + "(" + alias.getClass() + ")");
				continue;
			}
			print("<" + type);
			for (String string : attributes.keySet()) {
				print(" " + string + "=\"" + attributes.get(string) + "\"");
			}
			print(">");
			for (String key : parameters.keySet()) {
				print("<" + key + ">" + parameters.get(key) + "</" + key + ">");
			}

			if (components.size() != 0) {
				print("\n <Components>\n");
				for (Integer i : components) {
					print("  <Component id=\"" + i + "\"/>\n");
				}
				print(" </Components>\n");
			}
			print("</" + type + ">\n");

		}
		print("</Nodes>\n");
	}

	/**
	 * Creates reactions in reactome visuazliation model from our model.
	 * 
	 * @param model
	 *          model to be visualized
	 * @throws IOException
	 *           thrown when there are problems with accessing reactome database
	 */
	private void printReactions(Model model) throws IOException {
		print("<Edges>\n");
		for (Reaction reaction : model.getReactions()) {
			if (!isReactionFeasible(reaction)) {
				continue;
			}
			int currId = ++id;
			Map<String, String> parameters = new HashMap<String, String>();
			Map<Integer, String> inputs = new HashMap<Integer, String>();
			Map<Integer, String> outputs = new HashMap<Integer, String>();
			Map<Integer, String> catalysts = new HashMap<Integer, String>();

			Map<String, String> attributes = new HashMap<String, String>();

			attributes.put("id", currId + "");
			attributes.put("position", (int) reaction.getCenterPoint().getX() + " " + (int) reaction.getCenterPoint().getY());
			attributes.put("points", getReactionMiddlePoints(reaction));
			attributes.put("lineWidth", "1.0");
			attributes.put("reactomeId", "0");

			parameters.put("displayName", reaction.getIdReaction());

			String stableIdentifier = rcu.getReactomeIdentifierForReaction(reaction);
			if (stableIdentifier != null) {
				ReactomeDatabaseObject obj = rc.getFullObjectForStableIdentifier(stableIdentifier);
				if (obj != null && obj.getDbId() != null) {
					attributes.put("reactomeId", obj.getDbId() + "");
				}
			}

			for (ReactionNode node : reaction.getReactionNodes()) {
				Integer id = tempIds.get(node.getElement());
				if (node instanceof Reactant) {
					inputs.put(id, getInputString((Reactant) node));
				} else if (node instanceof Product) {
					outputs.put(id, getOutputString((Product) node));
				} else if (node instanceof Modifier) {
					catalysts.put(id, getModifierString((Modifier) node));
				}
			}

			String type = "org.gk.render.RenderableReaction";

			print("<" + type);
			for (String string : attributes.keySet()) {
				print(" " + string + "=\"" + attributes.get(string) + "\"");
			}
			print(">\n");
			print(" <Properties>\n");
			for (String key : parameters.keySet()) {
				print("  <" + key + ">" + parameters.get(key) + "</" + key + ">\n");
			}
			print(" </Properties>\n");

			if (inputs.size() != 0) {
				print(" <Inputs>\n");
				for (Integer i : inputs.keySet()) {
					print("  <Input  id=\"" + i + "\" points=\"" + inputs.get(i) + "\"/>\n");
				}
				print(" </Inputs>\n");
			}
			if (outputs.size() != 0) {
				print(" <Outputs>\n");
				for (Integer i : outputs.keySet()) {
					print("  <Output  id=\"" + i + "\" points=\"" + outputs.get(i) + "\"/>\n");
				}
				print(" </Outputs>\n");
			}
			if (catalysts.size() != 0) {
				print(" <Catalysts>\n");
				for (Integer i : catalysts.keySet()) {
					print("  <Catalyst  id=\"" + i + "\" points=\"" + catalysts.get(i) + "\"/>\n");
				}
				print(" </Catalysts>\n");
			}
			print("</" + type + ">\n");

		}

		print("</Edges>\n");
	}

	/**
	 * Returns string representing modifier.
	 * 
	 * @param node
	 *          modifier in our model
	 * @return string representing modifier that can be used in reactome
	 *         visualization
	 */
	private String getModifierString(ReactionNode node) {
		String result = null;
		List<Point2D> points = node.getLine().getPoints();
		for (int i = 0; i < points.size() - 1; i++) {
			if (result == null) {
				result = "";
			} else {
				result += ", ";
			}
			Point2D point = points.get(i);
			result += (int) point.getX() + " " + (int) point.getY();
		}
		return result;
	}

	/**
	 * Returns string representing product.
	 * 
	 * @param node
	 *          product in our model
	 * @return string representing product that can be used in reactome
	 *         visualization
	 */
	private String getOutputString(ReactionNode node) {
		for (NodeOperator tmpNode : node.getReaction().getOperators()) {
			if (tmpNode.isProductOperator()) {
				String result = null;
				List<Point2D> points = node.getLine().getPoints();
				for (int i = points.size() - 1; i >= 0; i--) {
					if (result == null) {
						result = "";
					} else {
						result += ", ";
					}
					Point2D point = points.get(i);
					result += (int) point.getX() + " " + (int) point.getY();
				}
				return result;
			}
		}
		Point2D point = node.getLine().getEndPoint();
		String result = (int) point.getX() + " " + (int) point.getY();
		return result;
	}

	/**
	 * Returns string representing reactant.
	 * 
	 * @param node
	 *          reactant in our model
	 * @return string representing reactant that can be used in reactome
	 *         visualization
	 */
	private String getInputString(ReactionNode node) {
		for (NodeOperator tmpNode : node.getReaction().getOperators()) {
			if (tmpNode.isReactantOperator()) {
				String result = null;
				List<Point2D> points = node.getLine().getPoints();
				for (int i = 0; i < points.size(); i++) {
					if (result == null) {
						result = "";
					} else {
						result += ", ";
					}
					Point2D point = points.get(i);
					result += (int) point.getX() + " " + (int) point.getY();
				}
				return result;
			}
		}
		Point2D point = node.getLine().getPoints().get(0);
		String result = (int) point.getX() + " " + (int) point.getY();
		return result;
	}

	/**
	 * Check if reaction can be easily transformed into reactome vizualization
	 * tool.
	 * 
	 * @param reaction
	 *          reaction to be checked
	 * @return <code>true</code> if reaction can be easily transformed,
	 *         <code>false</code> otherwise
	 */
	private boolean isReactionFeasible(Reaction reaction) {
		for (ReactionNode node : reaction.getReactionNodes()) {
			if (!aliasFeasible(node.getElement())) {
				return false;
			}
		}
		int counter = 0;
		for (NodeOperator node : reaction.getOperators()) {
			if (node.isProductOperator()) {
				counter++;
			}
			if (counter > 1) {
				logger.debug("To many operators in the input: " + reaction.getIdReaction());
				return false;
			}
		}
		counter = 0;
		for (NodeOperator node : reaction.getOperators()) {
			if (node.isReactantOperator()) {
				counter++;
			}
			if (counter > 1) {
				logger.debug("To many operators in the output: " + reaction.getIdReaction());
				return false;
			}
		}
		counter = 0;
		for (NodeOperator node : reaction.getOperators()) {
			if (node.isModifierOperator()) {
				logger.debug("To complicated catalyst: " + reaction.getIdReaction());
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns string representing reaction line.
	 * 
	 * @param reaction
	 *          reaction to be transformed
	 * @return string representing reaction line
	 */
	private String getReactionMiddlePoints(Reaction reaction) {
		String result = null;
		NodeOperator node = null;
		for (NodeOperator n : reaction.getOperators()) {
			if (n.isReactantOperator()) {
				node = n;
			}
		}
		if (node != null) {
			for (Point2D point : node.getLine().getPoints()) {
				if (result == null) {
					result = (int) point.getX() + " " + (int) point.getY();
				} else {
					result += ", " + (int) point.getX() + " " + (int) point.getY();
				}
			}
		} else {
			AbstractNode tmp = null;
			for (AbstractNode reactionNode : reaction.getNodes()) {
				if (reactionNode instanceof Reactant) {
					tmp = reactionNode;
				}
			}
			List<Point2D> points = tmp.getLine().getPoints();
			for (int i = 0; i < points.size(); i++) {
				Point2D point = points.get(i);
				if (result == null) {
					result = (int) point.getX() + " " + (int) point.getY();
				} else {
					result += ", " + (int) point.getX() + " " + (int) point.getY();
				}
			}
		}

		node = null;
		for (NodeOperator n : reaction.getOperators()) {
			if (n.isProductOperator()) {
				node = n;
			}
		}
		if (node != null) {
			List<Point2D> points = node.getLine().getPoints();
			for (int i = points.size() - 1; i >= 0; i--) {
				Point2D point = points.get(i);
				if (result == null) {
					result = (int) point.getX() + " " + (int) point.getY();
				} else {
					result += ", " + (int) point.getX() + " " + (int) point.getY();
				}
			}
		} else {
			AbstractNode tmp = null;
			for (AbstractNode reactionNode : reaction.getNodes()) {
				if (reactionNode instanceof Product) {
					tmp = reactionNode;
				}
			}
			List<Point2D> points = tmp.getLine().getPoints();
			for (int i = 0; i < points.size(); i++) {
				Point2D point = points.get(i);
				if (result == null) {
					result = (int) point.getX() + " " + (int) point.getY();
				} else {
					result += ", " + (int) point.getX() + " " + (int) point.getY();
				}
			}
		}

		return result;
	}

	/**
	 * Checks if alias can be put into reactome vizualization.
	 * 
	 * @param alias
	 *          alias to check
	 * @return <code>true</code> if alias can be put into reactome vizualization,
	 *         <code>false</code> otherwise
	 */
	public boolean aliasFeasible(Element alias) {
		Point2D point = new Point2D.Double(alias.getX(), alias.getY());
		Point2D point2 = new Point2D.Double(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight());
		if (bound.contains(point) && bound.contains(point2)) {
			return true;
		}
		return false;
	}

	/**
	 * @return the rc
	 * @see #rc
	 */
	public DataSourceUpdater getRc() {
		return rc;
	}

	/**
	 * @param rc
	 *          the rc to set
	 * @see #rc
	 */
	public void setRc(DataSourceUpdater rc) {
		this.rc = rc;
	}

	/**
	 * @return the rcu
	 * @see #rcu
	 */
	public ReactomeQueryUtil getRcu() {
		return rcu;
	}

	/**
	 * @param rcu
	 *          the rcu to set
	 * @see #rcu
	 */
	public void setRcu(ReactomeQueryUtil rcu) {
		this.rcu = rcu;
	}

}
