package lcsb.mapviewer.run;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.ProblematicAnnotation;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.ApplicationContextLoader;
import lcsb.mapviewer.persist.DbUtils;

/**
 * Class used for analyzing the CellDEsigner model and generating some statistic
 * about it.
 * 
 * @author Piotr Gawron
 * 
 */
public class Statistics {
	/**
	 * Number of nanoseconds in second.
	 */
	private static final double	NANOSECONDS_IN_SECOND	= 1000000000.0;

	/**
	 * Default class logger.
	 */
	private static Logger				logger								= Logger.getLogger(Statistics.class.getName());

	/**
	 * Module that allows to annotate maps.
	 */
	@Autowired
	private ModelAnnotator			modelAnnotator;

	/**
	 * Local backend to the pubmed data.
	 */
	@Autowired
	private PubmedParser				pubmedBackend;

	/**
	 * Utils that help to manage the sessions in custom multithreaded
	 * implementation.
	 */
	@Autowired
	private DbUtils							dbUtils;

	/**
	 * Static main method used to run this stand alone code.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		try {
			String[] modelFileNames = new String[] { "testFiles/pd_full/PD_160714_2/PD_160714_2.xml", //
					"testFiles/pd_full/PD_160714_2/submaps/PARK2_substrates.xml", //
					"testFiles/pd_full/PD_160714_2/submaps/Ubiquitin_proteasome_system.xml", //
					"testFiles/pd_full/PD_160714_2/submaps/Fatty_acid_and_ketone_body_metabolism.xml", //
			};

			long startTime = System.nanoTime();

			Statistics main = new Statistics();
			ApplicationContextLoader.loadApplicationContext("consoleApplicationContext.xml");
			ApplicationContextLoader.injectDependencies(main);

			main.run(modelFileNames);
			long endTime = System.nanoTime();

			long duration = endTime - startTime;
			double sec = duration / NANOSECONDS_IN_SECOND;
			System.out.println("Duration: " + new DecimalFormat("#.###").format(sec) + "s");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Generate statistics.
	 * 
	 * @param fileNames
	 *          filenames for the models used for statistic generation
	 */
	public void run(String[] fileNames) {
		dbUtils.createSessionForCurrentThread();
		List<Model> models = new ArrayList<>();
		try {
			// String modelName = PdMapAnnotations.getLastPdFilename();
			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			for (String name : fileNames) {
				Model model = parser.createModel(new ConverterParams().filename(name));
				models.add(model);
			}

			IProgressUpdater updater = new IProgressUpdater() {
				@Override
				public void setProgress(double progress) {
					logger.debug("Progress: " + progress);
				}
			};
			for (Model model : models) {
				modelAnnotator.performAnnotations(model, updater);
			}
			// modelAnnotator.removeIncorrectAnnotations(model, updater);

			printStatistics(models);

			// printAnnotationInformation(model);
			//
			// PrintWriter writer = new PrintWriter("tmp.html", "UTF-8");
			// writer.println(createPubmedExportString(model));
			// writer.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		dbUtils.closeSessionForCurrentThread();
	}

	/**
	 * Prints statistics for the model.
	 * 
	 * @param models
	 *          models under analysis
	 */
	protected void printStatistics(List<Model> models) {
		int reactionNumber = 0;
		for (Model model2 : models) {
			reactionNumber += model2.getReactions().size();
		}
		print("Reactions: ");
		print("  number: " + reactionNumber);
		Map<String, Set<String>> ids = new HashMap<String, Set<String>>();
		int pubmedReactions = 0;
		for (Model model : models) {
			for (Reaction reaction : model.getReactions()) {
				for (MiriamData md : reaction.getMiriamData()) {
					String type = md.getDataType().getCommonName();
					Set<String> set = ids.get(type);
					if (set == null) {
						set = new HashSet<String>();
						ids.put(type, set);
					}
					set.add(md.getResource());
				}
				if (reaction.getMiriamData().size() == 1) {
					if (reaction.getMiriamData().iterator().next().getDataType().equals(MiriamType.PUBMED)) {
						pubmedReactions++;
					}
				}
			}
		}

		for (String string : ids.keySet()) {
			print("  " + string + ": " + ids.get(string).size());
		}
		print("  pubmed reactions: " + pubmedReactions);

		ids.clear();
		print("-----------------------------");
		print("Elements: ");
		Set<String> names = new HashSet<>();
		Map<String, Set<String>> typeNames = new HashMap<>();

		for (Model model : models) {
			for (Element element : model.getElements()) {
				if (element instanceof Species) {
					names.add(element.getName());
					Set<String> tmp = typeNames.get(element.getClass().getName());
					if (tmp == null) {
						tmp = new HashSet<String>();
						typeNames.put(element.getClass().getName(), tmp);
					}
					tmp.add(element.getName());
					for (MiriamData md : element.getMiriamData()) {
						String type = md.getDataType().getCommonName();
						Set<String> set = ids.get(type);
						if (set == null) {
							set = new HashSet<String>();
							ids.put(type, set);
						}
						set.add(md.getResource());
					}
				}
			}
		}
		print("  distinct names: " + names.size());

		for (String string : ids.keySet()) {
			print("  " + string + ": " + ids.get(string).size());
		}

		print("-----------------------------");
		print("  distinct elements types: ");
		for (String string : typeNames.keySet()) {
			print("   " + string + ": " + typeNames.get(string).size());
		}

		Set<String> set = new HashSet<String>();
		for (Model model : models) {
			for (Compartment comp : model.getCompartments()) {
				set.add(comp.getName());
			}
		}
		print("   compartments: " + set.size());
	}

	/**
	 * Prints information about inproper annotations.
	 * 
	 * @param model
	 *          model under analysis
	 */
	protected void printAnnotationInformation(Model model) {
		print("-----------------------------");
		print("Improper annotations:");
		Collection<? extends ProblematicAnnotation> improper = modelAnnotator.findImproperAnnotations(model, new IProgressUpdater() {
			@Override
			public void setProgress(double progress) {
			}
		}, null);
		for (ProblematicAnnotation improperAnnotation : improper) {
			print(improperAnnotation.toString());
		}
		print("-----------------------------");
		print("Missing annotations:");
		Collection<ProblematicAnnotation> missing = modelAnnotator.findMissingAnnotations(model, null);
		for (ProblematicAnnotation improperAnnotation : missing) {
			print(improperAnnotation.toString());
		}
		print("-----------------------------");
		print("Complex annotations:");
		for (Element element : model.getElements()) {
			if (element instanceof Complex && element.getMiriamData().size() > 0) {
				print(element.getClass().getSimpleName() + ";\t\tname=" + element.getName() + ";\t\tid=" + element.getElementId());
			}
		}
		print("-----------------------------");
		print("Pubmed annotated elements:");
		// Collection<? extends ProblematicAnnotation> pubmes =
		// modelAnnotator.findPubmedAnnotatedElements(model);
		// for (ProblematicAnnotation improperAnnotation : pubmes) {
		//
		// if (improperAnnotation.getObject() != null) {
		// AnnotatedObject element = improperAnnotation.getObject();
		// String ids = "";
		// for (MiriamData md : improperAnnotation.getMd()) {
		// ids += md.getResource() + ", ";
		// }
		// print(element.getClass().getSimpleName() + ";\t\tname=" +
		// element.getName() + ";\t\tid=" + element.getElementId() + ";\t\tpubmed= "
		// + ids);
		// }
		// }
	}

	/**
	 * Prints line of the report.
	 * 
	 * @param string
	 *          text to print
	 */
	void print(String string) {
		System.out.println(string);
	}

	/**
	 * @return the modelAnnotator
	 */
	public ModelAnnotator getModelAnnotator() {
		return modelAnnotator;
	}

	/**
	 * @param modelAnnotator
	 *          the modelAnnotator to set
	 */
	public void setModelAnnotator(ModelAnnotator modelAnnotator) {
		this.modelAnnotator = modelAnnotator;
	}

	/**
	 * Create tab separated string with information about pubmed articles used in
	 * the map.
	 * 
	 * @param model
	 *          analyzed model
	 * @return tab separated string with information about pubmed articles
	 * @throws Exception
	 *           thrown when there is a problem with pubmed
	 */
	protected String createPubmedExportString(Model model) throws Exception {
		Map<String, List<String>> links = new HashMap<String, List<String>>();
		for (Element element : model.getElements()) {
			for (MiriamData md : element.getMiriamData()) {
				if (MiriamType.PUBMED.equals(md.getDataType())) {
					List<String> list = links.get(md.getResource());
					if (list == null) {
						list = new ArrayList<String>();
						links.put(md.getResource(), list);
					}
					String address = "<a href =\"" + Configuration.PUBLICALY_AVAILABLE_PD_MAP + "&search=species:" + element.getElementId() + "\">" + element.getName()
							+ "</a>";
					list.add(address);
				}
			}
		}

		for (Reaction reaction : model.getReactions()) {
			for (MiriamData md : reaction.getMiriamData()) {
				if (MiriamType.PUBMED.equals(md.getDataType())) {
					List<String> list = links.get(md.getResource());
					if (list == null) {
						list = new ArrayList<String>();
						links.put(md.getResource(), list);
					}
					String address = "<a href =\"" + PdMapAnnotations.getLinkForReaction(reaction) + "\">Reaction: " + reaction.getIdReaction() + "</a>";
					list.add(address);
				}
			}
		}

		StringBuilder result = new StringBuilder();

		result.append("<html><head/><body><table>");
		for (String string : links.keySet()) {
			Article article = pubmedBackend.getPubmedArticleById(Integer.valueOf(string));
			result.append("<tr><td><a href =\"" + article.getLink() + "\">" + string + "</a></td>");
			result.append("<td>" + article.getTitle() + "</td>");
			result.append("<td>" + article.getStringAuthors() + "</td>");
			result.append("<td>" + article.getJournal() + "</td>");
			result.append("<td>" + article.getYear() + "</td>");
			for (String str : links.get(string)) {
				result.append("<td>");
				result.append(str);
				result.append("</td>");
			}
			result.append("</tr>\n");
		}
		result.append("</table></body></html>");

		return result.toString();
	}

	/**
	 * @return the pubmedBackend
	 * @see #pubmedBackend
	 */
	public PubmedParser getPubmedBackend() {
		return pubmedBackend;
	}

	/**
	 * @param pubmedBackend
	 *          the pubmedBackend to set
	 * @see #pubmedBackend
	 */
	public void setPubmedBackend(PubmedParser pubmedBackend) {
		this.pubmedBackend = pubmedBackend;
	}

}
