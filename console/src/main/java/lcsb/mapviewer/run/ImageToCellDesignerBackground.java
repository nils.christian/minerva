package lcsb.mapviewer.run;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.persist.ApplicationContextLoader;

import org.apache.log4j.Logger;

/**
 * This class creates celldesigner file from file with lines. The file is put as
 * an layer "image".
 * 
 * @author Piotr Gawron
 * 
 */
public final class ImageToCellDesignerBackground {
	/**
	 * MArgin of the image.
	 */
	private static final double	MARGIN	= 100;
	/**
	 * Scale of the image.
	 */
	private static final double	SCALE		= 8;

	/**
	 * Default class logger.
	 */
	private static Logger				logger	= Logger.getLogger(ImageToCellDesignerBackground.class.getName());

	/**
	 * Static main method used to run this stand alone code.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		ApplicationContextLoader.loadApplicationContext("consoleApplicationContext.xml");
		ImageToCellDesignerBackground main = new ImageToCellDesignerBackground();
		ApplicationContextLoader.injectDependencies(main);
		main.run();
	}

	/**
	 * Default constructor.
	 */
	private ImageToCellDesignerBackground() {

	}

	/**
	 * Generate CD file with image.
	 */
	private void run() {
		BufferedReader br = null;
		try {
			String fileName = "testFiles/simple_brain_coords.txt";

			br = new BufferedReader(new FileReader(fileName));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			String id = null;
			Point2D point = null;
			Model model = new ModelFullIndexed(null);
			Layer layer = new Layer();
			layer.setLayerId("1");
			layer.setName("image");
			layer.setVisible(true);
			line = br.readLine();
			double maxX = 0;
			double maxY = 0;
			while (line != null) {
				String[] tmp = line.split(" ");
				String newId = tmp[0];
				double x = SCALE * Double.parseDouble(tmp[1]);
				double y = SCALE * Double.parseDouble(tmp[2]);

				maxX = Math.max(maxX, x);
				maxY = Math.max(maxY, y);
				Point2D newPoint = new Point2D.Double(x, y);
				if (newId.equals(id)) {
					layer.addLayerLine(new PolylineData(point, newPoint));
				}
				point = newPoint;
				id = newId;

				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			for (PolylineData pd : layer.getLines()) {
				double x = MARGIN + pd.getBeginPoint().getX();
				double y = MARGIN + maxY - pd.getBeginPoint().getY();
				pd.setStartPoint(new Point2D.Double(x, y));

				x = MARGIN + pd.getEndPoint().getX();
				y = MARGIN + maxY - pd.getEndPoint().getY();
				pd.setEndPoint(new Point2D.Double(x, y));
			}
			model.addLayer(layer);
			model.setWidth(maxX + 2 * MARGIN);
			model.setHeight(maxY + 2 * MARGIN);

			CellDesignerXmlParser parser = new CellDesignerXmlParser();
			String xml = parser.toXml(model);

			PrintWriter writer = new PrintWriter("tmp.xml", "UTF-8");
			writer.println(xml);
			logger.debug(xml);
			writer.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

}
