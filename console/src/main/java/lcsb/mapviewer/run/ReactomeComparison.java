package lcsb.mapviewer.run;

import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.ApplicationContextLoader;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent;
import lcsb.mapviewer.reactome.utils.ComparatorException;
import lcsb.mapviewer.reactome.utils.DataFormatter;
import lcsb.mapviewer.reactome.utils.DataSourceUpdater;
import lcsb.mapviewer.reactome.utils.ReactionComparator;
import lcsb.mapviewer.reactome.utils.ReactomeQueryUtil;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult.MatchStatus;

/**
 * This class generates report of comparison between data on PD-map and
 * reactome.
 * 
 * @author Piotr Gawron
 * 
 */

public class ReactomeComparison {
	/**
	 * Number of nanoseconds in second :).
	 */
	private static final double	NANOSECONDS_IN_SECOND	= 1000000000.0;

	/**
	 * Local model used for comparison.
	 */
	private Model								model;

	/**
	 * Default class logger.
	 */
	private static Logger				logger								= Logger.getLogger(ReactomeComparison.class);

	/**
	 * Comparator of reactions between our model and reactome.
	 */
	@Autowired
	private ReactionComparator	reactionComparator;

	/**
	 * Class used for accessing reactome data.
	 */
	@Autowired
	private DataSourceUpdater		rc;

	/**
	 * Util class used for manipulating information in reactome objects.
	 */
	@Autowired
	private ReactomeQueryUtil		rcu;

	/**
	 * Formatter used for reaction comparison.
	 */
	@Autowired
	private DataFormatter				dataFormatter;

	/**
	 * Static main method used to run this stand alone code.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		PropertyConfigurator.configure("src/main/webapp/WEB-INF/resources/log4j.properties");
		ReactomeComparison main = new ReactomeComparison();
		ApplicationContextLoader.loadApplicationContext("consoleApplicationContext.xml");
		ApplicationContextLoader.injectDependencies(main);
		main.run();
		long endTime = System.nanoTime();

		long duration = endTime - startTime;
		double sec = duration / NANOSECONDS_IN_SECOND;
		System.out.println("Duration: " + new DecimalFormat("#.###").format(sec) + "s");
	}

	/**
	 * Executes comparison between model and reactome representation.
	 */
	public void run() {
		try {
			model = new CellDesignerXmlParser().createModel(new ConverterParams().filename(PdMapAnnotations.getLastPdFilename()));

			Set<String> list = new HashSet<String>();

			for (Species element : model.getSpeciesList()) {
				list.add(element.getName());
			}
			logger.debug("Species: " + list.size());

			List<String> ids = new ArrayList<String>();
			for (Reaction reaction : model.getReactions()) {
				ids.add(reaction.getIdReaction());
			}
			Collections.sort(ids);
			List<MatchResult> results = new ArrayList<MatchResult>();

			printHeader();
			int i = 0;
			for (String string : ids) {
				Reaction reaction = model.getReactionByReactionId(string);
				logger.info("Reaction: " + string + "; " + (i++) + "/" + ids.size());
				if (rcu.getReactomeIdentifierForReaction(reaction) != null) {
					MatchResult result = checkReaction(string);
					if (result != null) {
						results.add(result);
						printResult(result);
					}
				} else {
					ReactomeReactionlikeEvent reactomReaction = rcu.getSimilarReaction(reaction);
					if (reactomReaction != null) {
						MatchResult matchResult = new MatchResult();
						matchResult.setStatus(MatchStatus.SIMILAR_REACTION_FOUND);
						matchResult.setLocalReaction(reaction);
						matchResult.setReactomeReaction(reactomReaction);
						results.add(matchResult);
						printResult(matchResult);

					}
				}
			}
			printFooter(results);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}

	}

	/**
	 * Prints report footer.
	 * 
	 * @param results
	 *          results used in the report
	 */
	private void printFooter(List<MatchResult> results) {
		writer.println("</table>");

		int ok = 0;
		int mismatch = 0;
		int severe = 0;
		int similar = 0;
		for (MatchResult matchResult : results) {
			if (matchResult.getStatus() == MatchStatus.OK) {
				ok++;
			} else if (matchResult.getStatus() == MatchStatus.MISMATCH) {
				mismatch++;
			} else if (matchResult.getStatus() == MatchStatus.SIMILAR_REACTION_FOUND) {
				similar++;
			} else {
				severe++;
			}
		}
		writer.println("<p>Reactions checked: " + (results.size()) + "</p>");
		writer.println("<p>OK: " + ok + "</p>");
		writer.println("<p>MISMATCH: " + mismatch + "</p>");
		writer.println("<p>SEVERE PROBLEMS: " + severe + "</p>");
		writer.println("<p>SIMILAR: " + similar + "</p>");

		writer.println("</body></html>");
		writer.close();

	}

	/**
	 * Object to which the report is written.
	 */
	private PrintWriter writer;

	/**
	 * Creates report stream and prints header of the report.
	 * 
	 * @throws FileNotFoundException
	 *           if there is a problem with creating report file
	 * @throws UnsupportedEncodingException
	 *           thrown when there are problems with encoding
	 */
	private void printHeader() throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("out/report/report.html", "UTF-8");
		writer.println("<html><head></head><body>");
		writer.println("<table cellspacing=\"0\">");

		String resultString = "<tr>";
		resultString += "<td" + style + ">Reaction</td>";
		resultString += "<td" + style + ">REACTOME ID</td>";
		resultString += "<td" + style + ">Status</td>";
		resultString += "<td" + style + ">Similarity</td>";
		resultString += "<td" + style + ">Unknown reaction input</td>";
		resultString += "<td" + style + ">Unknown reaction modifier</td>";
		resultString += "<td" + style + ">Unknown reaction output</td>";
		resultString += "<td" + style + ">Unknown reactome input</td>";
		resultString += "<td" + style + ">Unknown reactome modifier</td>";
		resultString += "<td" + style + ">Unknown reactome output</td>";
		resultString += "</tr>";
		writer.println(resultString);

	}

	/**
	 * Formatter of decimal numbers.
	 */
	private DecimalFormat	df		= new DecimalFormat("#.##");

	/**
	 * Style used by report cells.
	 */
	private String				style	= " style=\"border-width:1;border-style:solid;border-color:#000000;\" ";

	/**
	 * Prints result row.
	 * 
	 * @param result
	 *          result to be printed
	 * @throws AnnotatorException
	 *           thrown when there is a problem accessing external annotation
	 *           service
	 */
	private void printResult(MatchResult result) throws AnnotatorException {
		String color = "#000000";
		String status = "";
		String error = "";
		switch (result.getStatus()) {
			case OK:
				color = "#52D017";
				status = "OK";
				error = "<td colspan=\"6\" " + style + "></td>";
				return;
			case SIMILAR_REACTION_FOUND:
				color = "#5757E0";
				status = "SIMILAR";
				error = "<td colspan=\"6\" " + style + "></td>";
				return;
			case MISMATCH:
				color = "#FFFF00";
				status = "MISMATCH";
				error += "<td" + style + ">" + dataFormatter.getInvalidLocalInputString(result) + "&nbsp;</td>";
				error += "<td" + style + ">" + dataFormatter.getInvalidLocalModifierString(result) + "&nbsp;</td>";
				error += "<td" + style + ">" + dataFormatter.getInvalidLocalOutputString(result) + "&nbsp;</td>";
				error += "<td" + style + ">" + dataFormatter.getInvalidReactomeInputString(result) + "&nbsp;</td>";
				error += "<td" + style + ">" + dataFormatter.getInvalidReactomeModifierString(result) + "&nbsp;</td>";
				error += "<td" + style + ">" + dataFormatter.getInvalidReactomeOutputString(result) + "&nbsp;</td>";
				break;
			case INVALID_REACTOME_ID:
				color = "#FF0000";
				status = "REACTOME id does not point to reaction";
				error = "<td colspan=\"6\" " + style + "></td>";
				return;
			case INVALID_REACTION_WITH_PHEONTYPE:
				color = "#FF0000";
				status = "Impossible to map reaction to reactome - reaction contains Phenotype";
				error = "<td colspan=\"6\" " + style + "></td>";
				return;
			default:
				return;
		}
		String resultString = "<tr bgcolor = \"" + color + "\">";
		String reactionId = result.getLocalReaction().getIdReaction();
		String reactomId = null;
		reactomId = rcu.getReactomeIdentifierForReaction(result.getLocalReaction());

		resultString += "<td" + style + "><a target=\"_blank\" href=\"" + PdMapAnnotations.getLinkForReaction(result.getLocalReaction()) + "\">" + reactionId
				+ "</a> ";
		resultString += "<a target=\"_blank\" href=\"" + rcu.getReactomeUrlForStableIdentifier(reactomId) + "\">" + reactomId + "</a></td>";

		if (result.getReactomeReaction() != null) {
			reactomId = result.getReactomeReaction().getStableIdentifier().getIdentifier() + "."
					+ result.getReactomeReaction().getStableIdentifier().getIdentifierVersion();
		} else {
			reactomId = rcu.getReactomeIdentifierForReaction(result.getLocalReaction());
		}

		resultString += "<td" + style + "><a target=\"_blank\" href=\"" + rcu.getReactomeUrlForStableIdentifier(reactomId) + "\">" + reactomId + "</a></td>";
		resultString += "<td" + style + ">" + status + "</td>";
		resultString += "<td" + style + ">" + df.format(result.getScore() * IProgressUpdater.MAX_PROGRESS) + "%</td>";
		resultString += error;
		resultString += "</tr>";
		writer.println(resultString);
		System.out.println(resultString);
	}

	/**
	 * Compares our model reaction to reactome representation.
	 * 
	 * @param id
	 *          identifier of our reaction
	 * @return result of reaction comparison
	 * @throws IOException
	 *           thrown whene there are problems accessing reactome reaction
	 * @throws ComparatorException
	 *           thrown when there is a problem with comparison {@link Reaction}
	 *           with Reactome object
	 * 
	 */
	private MatchResult checkReaction(String id) throws IOException, ComparatorException {
		return checkReaction(model.getReactionByReactionId(id));
	}

	/**
	 * Compares our model reaction to reactome representation.
	 * 
	 * @param reaction
	 *          our reaction
	 * @return result of reaction comparison
	 * @throws IOException
	 *           thrown whene there are problems accessing reactome reaction
	 * @throws ComparatorException
	 *           thrown when there is a problem with comparison {@link Reaction}
	 *           with Reactome object
	 */
	private MatchResult checkReaction(Reaction reaction) throws IOException, ComparatorException {
		String stableIdentifier = rcu.getReactomeIdentifierForReaction(reaction);
		if (stableIdentifier != null) {
			String identifier = stableIdentifier.split("\\.")[0];
			String version = stableIdentifier.split("\\.")[1];

			ReactomeDatabaseObject object = rc.getFullObjectForStableIdentifier(identifier, version);
			if (object != null && object instanceof ReactomeReactionlikeEvent) {
				return reactionComparator.compareReactions(reaction, (ReactomeReactionlikeEvent) object);

			} else {
				MatchResult result = new MatchResult();
				result.setLocalReaction(reaction);
				result.setStatus(MatchStatus.INVALID_REACTOME_ID);
				return result;
			}
		}
		return null;
	}

	/**
	 * @return the reactionComparator
	 */
	public ReactionComparator getReactionComparator() {
		return reactionComparator;
	}

	/**
	 * @param reactionComparator
	 *          the reactionComparator to set
	 */
	public void setReactionComparator(ReactionComparator reactionComparator) {
		this.reactionComparator = reactionComparator;
	}

	/**
	 * @return the dataFormatter
	 */
	public DataFormatter getDataFormatter() {
		return dataFormatter;
	}

	/**
	 * @param dataFormatter
	 *          the dataFormatter to set
	 */
	public void setDataFormatter(DataFormatter dataFormatter) {
		this.dataFormatter = dataFormatter;
	}

	/**
	 * @return the rc
	 * @see #rc
	 */
	public DataSourceUpdater getRc() {
		return rc;
	}

	/**
	 * @param rc
	 *          the rc to set
	 * @see #rc
	 */
	public void setRc(DataSourceUpdater rc) {
		this.rc = rc;
	}

}
