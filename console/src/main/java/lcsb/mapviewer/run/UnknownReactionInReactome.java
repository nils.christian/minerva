package lcsb.mapviewer.run;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.persist.ApplicationContextLoader;
import lcsb.mapviewer.reactome.model.ReactomeDatabaseObject;
import lcsb.mapviewer.reactome.utils.DataFormatter;
import lcsb.mapviewer.reactome.utils.ReactomeQueryUtil;
import lcsb.mapviewer.reactome.utils.comparators.MatchResult;

/**
 * This class prepare report of data that are not included in reactome and maybe
 * should be added.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownReactionInReactome {
	/**
	 * Number of nanoseconds in a second.
	 */
	private static final double	NANOSECONDS_IN_SECOND	= 1000000000.0;

	/**
	 * Model used for generating raport.
	 */
	private Model								model;

	/**
	 * Default class logger.
	 */
	private static Logger				logger								= Logger.getLogger(UnknownReactionInReactome.class);

	/**
	 * Util class used for manipulating information in reactome objects.
	 */
	@Autowired
	private ReactomeQueryUtil		rcu;

	/**
	 * Formatter used for reaction comparison.
	 */
	@Autowired
	private DataFormatter				dataFormatter;

	/**
	 * Class used for accessing data in pubmed database.
	 */
	@Autowired
	private PubmedParser				pubmedParser;

	/**
	 * Static main method used to run this stand alone code.
	 * 
	 * @param args
	 *          command line arguments
	 */
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		PropertyConfigurator.configure("src/main/webapp/WEB-INF/resources/log4j.properties");
		UnknownReactionInReactome main = new UnknownReactionInReactome();
		ApplicationContextLoader.loadApplicationContext("consoleApplicationContext.xml");
		ApplicationContextLoader.injectDependencies(main);
		main.run();
		long endTime = System.nanoTime();

		long duration = endTime - startTime;
		double sec = duration / NANOSECONDS_IN_SECOND;
		System.out.println("Duration: " + new DecimalFormat("#.###").format(sec) + "s");
	}

	/**
	 * Executes comparison between model and reactome to generate report.
	 */
	public void run() {
		try {
			MapGenerator mg = new MapGenerator();
			model = new CellDesignerXmlParser().createModel(new ConverterParams().filename(PdMapAnnotations.getLastPdFilename()));
			new CreateHierarchyCommand(model, mg.computeZoomLevels(model), mg.computeZoomFactor(model)).execute();
			List<String> ids = new ArrayList<String>();
			for (Reaction reaction : model.getReactions()) {
				if (rcu.getReactomeIdentifierForReaction(reaction) == null) {
					ids.add(reaction.getIdReaction());
				}
			}
			Collections.sort(ids);
			printHeader();
			List<MatchResult> results = new ArrayList<MatchResult>();
			for (String string : ids) {
				boolean unknown = true;
				boolean pubmed = false;
				Reaction reaction = model.getReactionByReactionId(string);
				for (MiriamData md : reaction.getMiriamData()) {
					if (md.getDataType().equals(MiriamType.PUBMED)) {
						pubmed = true;
						ReactomeDatabaseObject obj = rcu.getLiteratureReferenceByPubMedId(Integer.parseInt(md.getResource()));
						if (obj != null) {
							unknown = false;
						}
					}
				}
				if (pubmed && unknown) {
					MatchResult matchResult = new MatchResult();
					matchResult.setLocalReaction(reaction);
					for (ReactionNode node : reaction.getReactants()) {
						matchResult.addInvalidLocalInput(node.getElement());
					}
					for (ReactionNode node : reaction.getProducts()) {
						matchResult.addInvalidLocalOutput(node.getElement());
					}
					for (ReactionNode node : reaction.getModifiers()) {
						matchResult.addInvalidLocalModifier(node.getElement());
					}
					results.add(matchResult);
					printResult(matchResult);
				}
			}
			printFooter(results);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	/**
	 * Prints report footer.
	 * 
	 * @param results
	 *          results used in the report
	 */
	private void printFooter(List<MatchResult> results) {

		writer.println("</table>");
		writer.println("Reactions: " + results.size());
		writer.println("</body></html>");

		writer.close();
	}

	/**
	 * Object to which the report is written.
	 */
	private PrintWriter writer;

	/**
	 * Creates report stream and prints header of the report.
	 * 
	 * @throws FileNotFoundException
	 *           if there is a problem with creating report file
	 * @throws UnsupportedEncodingException
	 *           thrown when there are problems with encoding
	 */
	private void printHeader() throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("out/report/report-export.html", "UTF-8");
		writer.println("<html><head></head><body>");
		writer.println("<table cellspacing=\"0\"");

		String resultString = "<tr>";
		resultString += "<td" + style + ">Reaction</td>";
		resultString += "<td" + style + ">Pubmed ids</td>";
		resultString += "<td" + style + ">Ontologies</td>";
		resultString += "<td" + style + ">Input</td>";
		resultString += "<td" + style + ">Modifier</td>";
		resultString += "<td" + style + ">Output</td>";
		resultString += "</tr>";
		writer.println(resultString);

	}

	/**
	 * Style used by report cells.
	 */
	private String style = " style=\"border-width:1;border-style:solid;border-color:#000000;\" ";

	/**
	 * Prints result row.
	 * 
	 * @param result
	 *          result to be printed
	 * @throws Exception
	 *           thrown when there is a problem with printing
	 */
	private void printResult(MatchResult result) throws Exception {

		Set<String> ontologies = new HashSet<String>();

		for (Compartment alias : model.getCompartments()) {
			for (ReactionNode node : result.getLocalReaction().getReactionNodes()) {
				if (alias.contains(node.getElement())) {
					ontologies.add(alias.getName());
					break;
				}
			}
		}

		String color = "#FFFFFF";
		String resultString = "<tr bgcolor = \"" + color + "\">";
		String reactionId = result.getLocalReaction().getIdReaction();
		resultString += "<td" + style + "><a target=\"_blank\" href=\"" + PdMapAnnotations.getLinkForReaction(result.getLocalReaction()) + "\">" + reactionId
				+ "</a></td>";

		String pubmed = "";
		for (MiriamData md : result.getLocalReaction().getMiriamData()) {
			if (md.getDataType().equals(MiriamType.PUBMED)) {
				pubmed += pubmedParser.getHtmlFullLinkForId(Integer.valueOf(md.getResource()), false) + "<br/>";
			}
		}

		resultString += "<td" + style + ">" + pubmed + "</td>";

		String ont = "";
		for (String string : ontologies) {
			if (!ont.equals("")) {
				ont += "<hr/>";
			}
			ont += string;
		}
		resultString += "<td" + style + ">" + ont + "</td>";

		resultString += "<td" + style + ">" + dataFormatter.getInvalidLocalInputString(result) + "&nbsp;</td>";
		resultString += "<td" + style + ">" + dataFormatter.getInvalidLocalModifierString(result) + "&nbsp;</td>";
		resultString += "<td" + style + ">" + dataFormatter.getInvalidLocalOutputString(result) + "&nbsp;</td>";
		resultString += "</tr>";
		writer.println(resultString);
		System.out.println(resultString);
	}

	/**
	 * @return the dataFormatter
	 */
	public DataFormatter getDataFormatter() {
		return dataFormatter;
	}

	/**
	 * @param dataFormatter
	 *          the dataFormatter to set
	 */
	public void setDataFormatter(DataFormatter dataFormatter) {
		this.dataFormatter = dataFormatter;
	}

	/**
	 * @return the pubmedParser
	 */
	public PubmedParser getPubmedParser() {
		return pubmedParser;
	}

	/**
	 * @param pubmedParser
	 *          the pubmedParser to set
	 */
	public void setPubmedParser(PubmedParser pubmedParser) {
		this.pubmedParser = pubmedParser;
	}

	/**
	 * @return the rcu
	 * @see #rcu
	 */
	public ReactomeQueryUtil getRcu() {
		return rcu;
	}

	/**
	 * @param rcu
	 *          the rcu to set
	 * @see #rcu
	 */
	public void setRcu(ReactomeQueryUtil rcu) {
		this.rcu = rcu;
	}

}
