package lcsb.mapviewer.run;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.run.PdMapAnnotations;

public class PdMapAnnotationsTest {
	Logger logger = Logger.getLogger(PdMapAnnotationsTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			String fileName = PdMapAnnotations.getLastPdFilename();
			assertNotNull(fileName);
			File file = new File(fileName);
			assertTrue(file.exists());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception occurred");
		}
	}

}
