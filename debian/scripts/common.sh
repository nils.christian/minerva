#file with common script functions for postrm/prerm/postinst/preinst
log(){
	echo "[" $CURRENT_VERSION "]" "$@" >>__LOG_FILE__
}
LOG_FILE="__LOG_FILE__"

DB_SCRIPT_DIR="__DB_SCRIPT_DIR__"

#new (current) version of the package
CURRENT_VERSION="__CURRENT_VERSION__"
#if we update package then this will be the old version of the package
OLD_VERSION=$2

if [ "$OLD_VERSION" = "$CURRENT_VERSION" ]
then
	OLD_VERSION="";
fi

TOMCAT_PACKAGE="";
TOMCAT7_OK=$(dpkg-query -W --showformat='${Status}\n' tomcat7|grep "install ok installed")
TOMCAT8_OK=$(dpkg-query -W --showformat='${Status}\n' tomcat8|grep "install ok installed")
if [ "$TOMCAT7_OK" != "" ];
then
  TOMCAT_PACKAGE='tomcat7'
fi
if [ "$TOMCAT8_OK" != "" ];
then
  TOMCAT_PACKAGE='tomcat8'
fi

POSTGRES_OK=$(dpkg-query -W --showformat='${Status}\n' postgresql|grep "install ok installed")

DB_HOST="__DB_HOST__"
DB_PORT="__DB_PORT__"
DB_DATABASE_NAME="__DB_DATABASE_NAME__"
DB_USERNAME="__DB_USERNAME__"
DB_PASSWORD="__DB_PASSWORD__"


if [ -f /etc/minerva/db.properties ]; then
	DB_PROPERTIES_FILE="/etc/minerva/db.properties"
	log "$DB_PROPERTIES_FILE file found"
	DB_HOST=`cat $DB_PROPERTIES_FILE |grep "uri" |cut -f3 -d"/" |cut -f1 -d":"`
	DB_PORT=`cat $DB_PROPERTIES_FILE |grep "uri" |cut -f3 -d"/" |cut -f2 -d":"`
	DB_DATABASE_NAME=`cat $DB_PROPERTIES_FILE |grep "uri" |cut -f4 -d"/"`
	DB_USERNAME=`cat $DB_PROPERTIES_FILE |grep "username" |cut -f2 -d"="`
	DB_PASSWORD=`cat $DB_PROPERTIES_FILE |grep "password" |cut -f2 -d"="`
fi


#if we connect to something that is not in localhost then we need to provide login and password
#because we won't have access to it as postgres user
if [ "$DB_HOST" != "localhost" ] && [ "$DB_HOST" != "127.0.0.1" ]
then
	log "DB is located on the remote server: $DB_HOST"
	IS_REMOTE_DB=true
else
	log "DB is at localhost: $DB_HOST"
	IS_REMOTE_DB=false
fi

exec_sql(){
	log "Execute SQL : '$1'"
	echo "$1" |PGPASSWORD=$DB_PASSWORD psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME $DB_DATABASE_NAME >>$LOG_FILE 2>>$LOG_FILE
}
exec_sql_file(){
	log "Execute SQL file: $1"
	PGPASSWORD=$DB_PASSWORD psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME $DB_DATABASE_NAME -f $1 >>$LOG_FILE 2>>$LOG_FILE
}
create_db(){
	if [ $IS_REMOTE_DB  = false ]
	then
		su - postgres -c "createuser -d -r -s -p $DB_PORT $DB_USERNAME"  >>$LOG_FILE 2>>$LOG_FILE
		log "User created"
		su - postgres -c "echo \"ALTER USER $DB_USERNAME WITH PASSWORD '$DB_PASSWORD';\"| psql -p $DB_PORT "  >>$LOG_FILE 2>>$LOG_FILE
		log "User credentials updated"
		su - postgres -c "createdb -p $DB_PORT -O $DB_USERNAME $DB_DATABASE_NAME"  >>$LOG_FILE 2>>$LOG_FILE
		log "Db created"

		hba_conf=`su - postgres -c "psql -t -P format=unaligned -c 'show hba_file';"`;
		cp $hba_conf $hba_conf".bac"
		cat $hba_conf".bac" |grep -v "all[ \t]*peer" >$hba_conf
		printf "local   all             all                                     md5\n" >>$hba_conf
		invoke-rc.d postgresql restart
	else
		log "HOST is remote, assuming db and user are already there"
	fi
}
stop_postgres() {
	if [ $IS_REMOTE_DB  = false ]
	then
		invoke-rc.d postgresql stop || true
	fi
}
start_postgres() {
	if [ $IS_REMOTE_DB  = false ]
	then
		invoke-rc.d postgresql restart
	fi
}
