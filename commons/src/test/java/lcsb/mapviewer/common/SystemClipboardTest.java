package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SystemClipboardTest {
	Logger	logger	= Logger.getLogger(SystemClipboardTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testClipboard() throws Exception {
		try {
			SystemClipboard sc = new SystemClipboard();
			sc.setClipboardContents("TEST");

			SystemClipboard sc2 = new SystemClipboard();
			assertEquals("TEST",sc2.getClipboardContents());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidClipboard() throws Exception {
		try {
			SystemClipboard sc2 = new SystemClipboard();
			
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(new Transferable() {
				
				@Override
				public boolean isDataFlavorSupported(DataFlavor flavor) {
					return true;
				}
				
				@Override
				public DataFlavor[] getTransferDataFlavors() {
					return null;
				}
				
				@Override
				public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
					throw new UnsupportedFlavorException(flavor);
				}
			}, sc2);

			assertEquals(null,sc2.getClipboardContents());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testLostOwnership() throws Exception {
		try {
			SystemClipboard sc = new SystemClipboard();
			sc.lostOwnership(null, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEmpty() throws Exception {
		try {
			SystemClipboard sc2 = new SystemClipboard();
			sc2.setClipboardContents(null);
			assertNull(sc2.getClipboardContents());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


}
