package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class ObjectUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetIdOfObject() {
		Object obj = new Object() {
			@SuppressWarnings("unused")
			public int getId() {
				return 107;
			}
		};
		assertEquals((Integer) 107, ObjectUtils.getIdOfObject(obj));
	}

	@Test
	public void testGetIdOfObjectWithoutId() {
		Object obj = new Object();
		try {
			ObjectUtils.getIdOfObject(obj);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		}
	}

	@Test
	public void testPrivateConstructor() throws Exception {
		try {
			Constructor<?> constr = ObjectUtils.class.getDeclaredConstructor(new Class<?>[] {});
			constr.setAccessible(true);
			assertNotNull(constr.newInstance(new Object[] {}));
		} catch (Exception e) {
			throw e;
		}
	}

}
