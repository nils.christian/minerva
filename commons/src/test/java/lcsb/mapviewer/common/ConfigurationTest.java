package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationTest extends CommonTestFunctions{

	Logger logger = Logger.getLogger(ConfigurationTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetters() {
		Configuration.setApplicationCacheOn(false);
		assertFalse(Configuration.isApplicationCacheOn());
		Configuration.setAutocompleteSize(1);
		assertEquals(1, Configuration.getAutocompleteSize());
		Configuration.setDbCacheOn(false);
		assertFalse(Configuration.isDbCacheOn());
	}

	@Test
	public void testLoadSystemBuildVersion() {
		Configuration.loadSystemVersion((String) null);
		assertEquals("Unknown", Configuration.getSystemBuildVersion(null));
		assertEquals("Unknown", Configuration.getSystemBuild(null));
		assertEquals("Unknown", Configuration.getSystemVersion(null));
	}

	@Test
	public void testLoadSystemBuildVersion2() {
		Configuration.loadSystemVersion((String) null);
		Configuration.loadSystemVersion(new File("unknown.xxx"));
		assertEquals("Unknown", Configuration.getSystemBuildVersion(null));
		assertEquals("Unknown", Configuration.getSystemBuild(null));
		assertEquals("Unknown", Configuration.getSystemVersion(null));
	}

	@Test
	public void testGetSystemBuildVersion() {
		assertEquals("100", Configuration.getSystemBuildVersion("testFiles/version/", true));
		assertEquals("100", Configuration.getSystemBuildVersion(null, false));
		assertEquals("101", Configuration.getSystemVersion("testFiles/version/", true));
	}

	@Test
	public void testGetSystemBuild() {
		assertEquals("102", Configuration.getSystemBuild("testFiles/version/", true));
		assertEquals("102", Configuration.getSystemBuild(null, false));
	}

	@Test
	public void testXGetSystemVersion() {
		List<String> frame = new ArrayList<>();
		frame.add("test");
		Configuration.setxFrameDomain(frame);
		assertEquals(frame, Configuration.getxFrameDomain());
	}

	@Test
	public void testWebAppDir() {
		String dir = "test2";
		Configuration.setWebAppDir(dir);
		assertEquals(dir, Configuration.getWebAppDir());
	}


	@Test
	public void testLoadInvalidSystemBuildVersion() {
		Configuration.loadSystemBuildVersion(new File("invalid_path"));
		assertEquals(1, super.getErrors().size());
	}

	@Test
	public void testLoadInvalidSystemVersion() {
		Configuration.loadSystemVersion(new File("testFiles/version/INVALID_CHANGELOG"));
		assertEquals(1, super.getErrors().size());
	}

	@Test
	public void testGetFrameworkVersion() {
		FrameworkVersion version = Configuration.getFrameworkVersion(null);
		assertEquals("Unknown", version.getGitVersion());
		assertEquals("Unknown", version.getTime());
		assertEquals("Unknown", version.getVersion());
	}

	@Test
	public void testGetMemorySaturationRatioTriggerClean() {
		double newRatio = 33;
		double oldRatio = Configuration.getMemorySaturationRatioTriggerClean();
		try {
			Configuration.setMemorySaturationRatioTriggerClean(newRatio);
			assertEquals(newRatio, Configuration.getMemorySaturationRatioTriggerClean(), Configuration.EPSILON);
		} finally {
			Configuration.setMemorySaturationRatioTriggerClean(oldRatio);
		}
	}

	@Test
	public void testPrivateConstructor() throws Exception {
		try {
			Constructor<?> constr = Configuration.class.getDeclaredConstructor(new Class<?>[] {});
			constr.setAccessible(true);
			assertNotNull(constr.newInstance(new Object[] {}));
		} catch (Exception e) {
			throw e;
		}
	}
}
