package lcsb.mapviewer.common.comparator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BooleanComparatorTest.class, //
    ColorComparatorTest.class, //
    DoubleComparatorTest.class, //
    EnumComparatorTest.class, //
    IntegerComparatorTest.class, //
    PointComparatorTest.class, //
    StringComparatorTest.class, //
    StringListComparatorTest.class, //
    StringSetComparatorTest.class, //
    SetComparatorTest.class,//
})
public class AllComparatorTests {

}
