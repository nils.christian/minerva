package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class EventStorageLoggerAppenderTest {
	Logger logger = Logger.getLogger(EventStorageLoggerAppenderTest.class);

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLogCatching() {
		try {
			EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
			logger.addAppender(appender);
			logger.warn("test");
			logger.debug("1");
			logger.error("2");
			logger.info("3");
			logger.fatal("4");
			logger.trace("5");
			assertEquals(1, appender.getWarnings().size());
			assertEquals(1, appender.getFatals().size());
			assertEquals(1, appender.getErrors().size());
			logger.removeAppender(appender);
			logger.warn("test");
			assertEquals(1, appender.getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
			appender.requiresLayout();
			appender.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
