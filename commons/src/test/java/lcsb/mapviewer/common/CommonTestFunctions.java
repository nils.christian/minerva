package lcsb.mapviewer.common;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;

import lcsb.mapviewer.common.EventStorageLoggerAppender;

public class CommonTestFunctions {

	private EventStorageLoggerAppender appender;

	@Before
	public final void _setUp() throws Exception {
		Logger.getRootLogger().removeAppender(appender);
		appender = new EventStorageLoggerAppender();
		Logger.getRootLogger().addAppender(appender);
	}

	@After
	public final void _tearDown() throws Exception {
		Logger.getRootLogger().removeAppender(appender);
	}

	protected List<LoggingEvent> getWarnings() {
		return appender.getWarnings();
	}

	protected List<LoggingEvent> getErrors() {
		return appender.getErrors();
	}

}
