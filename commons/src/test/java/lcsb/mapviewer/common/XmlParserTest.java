package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.ElementImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

public class XmlParserTest {
	Logger		logger = Logger.getLogger(XmlParserTest.class);

	XmlParser	parser = new XmlParser();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testColorParsing() {
		try {
			String strColor = "ffcbcd09";

			Color c = parser.stringToColor(strColor);
			String resultString = parser.colorToString(c);
			assertTrue("Different string representation: " + strColor + " - " + resultString, strColor.equalsIgnoreCase(resultString));

			Color c2 = parser.stringToColor(resultString);

			assertEquals(c.getRed(), c2.getRed());
			assertEquals(c.getGreen(), c2.getGreen());
			assertEquals(c.getBlue(), c2.getBlue());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidColorParsing() {
		try {
			String strColor = "hello world";

			parser.stringToColor(strColor);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testColorParsingWithAlpha() {
		try {
			String strColor = "fecbcd09";

			Color c = parser.stringToColor(strColor);
			String resultString = parser.colorToString(c);
			assertTrue("Different string representation: " + strColor + " - " + resultString, strColor.equalsIgnoreCase(resultString));

			Color c2 = parser.stringToColor(resultString);

			assertEquals(c.getRed(), c2.getRed());
			assertEquals(c.getGreen(), c2.getGreen());
			assertEquals(c.getBlue(), c2.getBlue());
			assertEquals(c.getAlpha(), c2.getAlpha());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetXmlDocumentFromString() throws Exception {
		try {
			Document validDoc = parser.getXmlDocumentFromString("<node>test</node>");
			assertNotNull(validDoc);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testNodeToString() throws Exception {
		try {
			String xml = "<test_node>test_x</test_node>";
			Document validDoc = parser.getXmlDocumentFromString(xml);
			String str = parser.nodeToString(validDoc);
			assertEquals(xml.trim(), str.trim());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testEmptyNodeToString() throws Exception {
		try {
			String str = parser.nodeToString(null);
			assertNull(str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testNodeToStringWithHeader() throws Exception {
		try {
			String xml = "<test_node>test_x</test_node>";
			Document validDoc = parser.getXmlDocumentFromString(xml);
			String str = parser.nodeToString(validDoc, true);
			assertTrue(str.contains(xml));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidNodeToString() throws Exception {
		try {
			final DocumentImpl xmlDoc = new DocumentImpl();

			Element root = xmlDoc.createElement("booking");

			class Tmp extends ElementImpl {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Tmp() {
					this.ownerDocument = xmlDoc;
				}

				@Override
				public NamedNodeMap getAttributes() {
					return null;
				}

			}
			;
			Element el = new Tmp();
			root.appendChild(el);
			assertNotNull(parser.nodeToString(root, true));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetXmlDocumentFromInvalidString() throws Exception {
		try {
			parser.getXmlDocumentFromString("<node>test<node>");
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetXmlDocumentFromInvalidInputStream() throws Exception {
		try {
			parser.getXmlDocumentFromInputSource(new InputSource());
			fail("Exception expected");
		} catch (InvalidXmlSchemaException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testEscapeXml() throws Exception {
		try {
			String str = parser.escapeXml("<xml>node</xml>");
			assertFalse(str.contains("<"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testEscapeNullXml() throws Exception {
		try {
			String str = parser.escapeXml(null);
			assertNull(str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testFileToString() throws Exception {
		try {
			String str = parser.fileToString("testFiles/test.txt");
			assertTrue(str.contains("test"));
			assertTrue(str.contains("file"));
			assertTrue(str.contains("with some content"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testInputStreamToString() throws Exception {
		try {
			InputStream stream = new ByteArrayInputStream("stream string".getBytes(StandardCharsets.UTF_8));
			String str = parser.inputStreamToString(stream);
			assertEquals("stream string", str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNodeValue() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node>content</node>");
			Node node = parser.getNode("node", document);
			String str = parser.getNodeValue(node);
			assertEquals("content", str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNodeValue2() throws Exception {
		try {
			String str = parser.getNodeValue(null);
			assertEquals("", str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNodeValue3() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node></node>");
			Node node = parser.getNode("node", document);
			String str = parser.getNodeValue(node);
			assertEquals("", str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNodeValue4() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node><subnode/></node>");
			Node node = parser.getNode("node", document);
			String str = parser.getNodeValue(node);
			assertEquals("", str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testInputStreamToStringThrowsException() throws Exception {
		try {
			parser.inputStreamToString(new InputStream() {
				@Override
				public int read() throws IOException {
					throw new IOException();
				}
			});
			fail("Exception expected");
		} catch (IOException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testFileToStringThrowsException() throws Exception {
		try {
			parser.fileToString("testFiles/unknown file.txt");
			fail("Exception expected");
		} catch (IOException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNodeAttr() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node attr=\"val\">content</node>");
			Node node = parser.getNode("node", document);
			String str = parser.getNodeAttr("attr", node);
			assertEquals("val", str);
			str = parser.getNodeAttr("attr2", node);
			assertEquals("", str);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNodes() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node><subnode>content1</subnode><subnode>content2</subnode><other/></node>");
			Node node = parser.getNode("node", document);
			List<Node> nodes = parser.getNodes("subnode", node.getChildNodes());
			assertEquals(2, nodes.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNode() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node><subnode>content1</subnode><subnode>content2</subnode><other/></node>");
			Node node = parser.getNode("node", document);
			Node child = parser.getNode("other", node.getChildNodes());
			assertNotNull(child);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNode2() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node attr=\"x\"><subnode>content1</subnode><subnode>content2</subnode><other/></node>");
			Node node = parser.getNode("node", document);
			Node child = parser.getNode("other2", node.getChildNodes());
			assertNull(child);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetNode3() throws Exception {
		try {
			Document document = parser.getXmlDocumentFromString("<node attr=\"x\">xxx</node>");
			Node node = parser.getNode("node", document);
			Node child = parser.getNode("other2", node.getChildNodes());
			assertNull(child);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testExceptionInConstructor() throws Exception {
		DocumentBuilderFactory factory = null;
		Field privateStringField = XmlParser.class.getDeclaredField("documentBuilderFactory");
		try {

			privateStringField.setAccessible(true);

			factory = (DocumentBuilderFactory) privateStringField.get(parser);
			privateStringField.set(parser, new DocumentBuilderFactory() {

				@Override
				public void setFeature(String name, boolean value) throws ParserConfigurationException {
					// TODO Auto-generated method stub

				}

				@Override
				public void setAttribute(String name, Object value) throws IllegalArgumentException {
					// TODO Auto-generated method stub

				}

				@Override
				public DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
					throw new ParserConfigurationException();
				}

				@Override
				public boolean getFeature(String name) throws ParserConfigurationException {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public Object getAttribute(String name) throws IllegalArgumentException {
					// TODO Auto-generated method stub
					return null;
				}
			});
			new XmlParser();
			fail("Exception expected");
		} catch (InvalidStateException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			privateStringField.set(parser, factory);
		}
	}

}
