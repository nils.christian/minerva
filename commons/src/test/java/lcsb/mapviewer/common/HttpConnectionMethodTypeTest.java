package lcsb.mapviewer.common;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HttpConnectionMethodTypeTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidValues() {
		for (HttpConnectionMethodType type : HttpConnectionMethodType.values()) {
			assertNotNull(type);

			// for coverage tests
			HttpConnectionMethodType.valueOf(type.toString());
		}
	}

}
