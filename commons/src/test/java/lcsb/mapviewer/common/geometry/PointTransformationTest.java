package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;

public class PointTransformationTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRotate() {
		PointTransformation pt = new PointTransformation();
		Point2D p1 = new Point2D.Double(1, 0);
		Point2D p2 = new Point2D.Double(2, 0);
		Point2D p3 = new Point2D.Double(3, 0);
		Point2D res = pt.rotatePoint(p1, Math.PI, p2);
		assertEquals(0.0, p3.distance(res), Configuration.EPSILON);
	}

	@Test
	public void testIsValid() {
		PointTransformation pt = new PointTransformation();
		Point2D p1 = new Point2D.Double(1, 0);
		Point2D p2 = new Point2D.Double(1, Double.NEGATIVE_INFINITY);
		Point2D p3 = new Point2D.Double(Double.NEGATIVE_INFINITY, 2);
		assertTrue(pt.isValidPoint(p1));
		assertFalse(pt.isValidPoint(p2));
		assertFalse(pt.isValidPoint(p3));
	}

}
