package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.awt.Color;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class ColorParserTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testSetInvalidColor() throws Exception {
		try {
			ColorParser parser = new ColorParser();
			parser.parse("qwe");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetInvalidColor2() throws Exception {
		try {
			ColorParser parser = new ColorParser();
			parser.parse("fffffff");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetColor() throws Exception {
		try {
			ColorParser parser = new ColorParser();
			Color color = parser.parse("#ffffff");
			assertEquals(Color.WHITE, color);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
