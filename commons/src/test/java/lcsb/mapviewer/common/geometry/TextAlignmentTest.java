package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TextAlignmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidValues() {
		for (TextAlignment type : TextAlignment.values()) {
			assertNotNull(type);

			// for coverage tests
			TextAlignment.valueOf(type.toString());
		}
	}

}
