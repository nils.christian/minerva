package lcsb.mapviewer.common.geometry;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Parser clas to extract {@link Color} objects from {@link String}.
 * 
 * @author Piotr Gawron
 *
 */
public class ColorParser {

  /**
   * Base of the hex representation.
   */
  private static final int HEX_BASE = 16;

  /**
   * Length of the string describing color in RGB: "#RRGGBB".
   */
  private static final int COLOR_STRING_LENGTH = 7;

  /**
   * Where starts description of red color in string representing color.
   */
  private static final int COLOR_SUBSTRING_START_RED = 1;

  /**
   * Where starts description of green color in string representing color.
   */
  private static final int COLOR_SUBSTRING_START_GREEN = 3;

  /**
   * Where starts description of blue color in string representing color.
   */
  private static final int COLOR_SUBSTRING_START_BLUE = 5;

  /**
   * Extracts {@link Color} from input {@link String}.
   * 
   * @param string
   *          text to process
   * @return {@link Color} obtained from input text
   */
  public Color parse(String string) {
    if (string.charAt(0) != '#') {
      string = "#" + string;
    }
    if (string.length() != COLOR_STRING_LENGTH) {
      throw new InvalidArgumentException(
          "Invalid color value: " + string + ". Correct format: #xxxxxx (where x is a hex value)");
    } else {
      return new Color(
          Integer.valueOf(string.substring(COLOR_SUBSTRING_START_RED, COLOR_SUBSTRING_START_GREEN), HEX_BASE), //
          Integer.valueOf(string.substring(COLOR_SUBSTRING_START_GREEN, COLOR_SUBSTRING_START_BLUE), HEX_BASE), //
          Integer.valueOf(string.substring(COLOR_SUBSTRING_START_BLUE, COLOR_STRING_LENGTH), HEX_BASE));
    }
  }

  /**
   * Converts color into list of attributes.
   * 
   * @param color
   *          color to convert
   * @return map with list of color attributes
   */
  public Map<String, Object> colorToMap(Color color) {
    Map<String, Object> result = new HashMap<>();
    result.put("alpha", color.getAlpha());
    result.put("rgb", color.getRGB());
    return result;
  }
}
