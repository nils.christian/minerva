package lcsb.mapviewer.common;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

/**
 * Abstract class with methods which help in parsing xml using DOM.
 * 
 * @author Piotr Gawron
 * 
 */
public class XmlParser {
  /**
   * Base of the hex representation.
   */
  private static final int HEX_BASE = 16;

  /**
   * {@link DocumentBuilderFactory} used to create {@link DocumentBuilder} objects
   * that will manipulate xml nodes.
   */
  private static DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(XmlParser.class.getName());

  /**
   * DOM document builder used for xml transformations.
   */
  private DocumentBuilder db;

  /**
   * Default constructor that prevents from instatiation of the class and
   * initializes fields.
   */
  protected XmlParser() {
    try {
      db = documentBuilderFactory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw new InvalidStateException("Problem with xml parser");
    }

  }

  /**
   * Method returns the node of xml nodelist 'nodes' with 'tagName' name. If node
   * could not be found then null is returned.
   * 
   * @param tagName
   *          name of node to look for
   * @param nodes
   *          list of nodes
   * @return node from nodes list with the tagName name, <b>null</b> if such node
   *         doesn't exist
   */
  protected Node getNode(final String tagName, final NodeList nodes) {
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase(tagName)) {
          return node;
        }
      }
    }
    return null;
  }

  /**
   * Method returns the child node of xml 'parentNode' with 'tagName' name. If
   * node could not be found then null is returned.
   * 
   * @param tagName
   *          name of node to look for
   * @param parentNode
   *          parent node
   * @return node from nodes list with the tagName name, <b>null</b> if such node
   *         doesn't exist
   */
  protected Node getNode(final String tagName, final Node parentNode) {
    return getNode(tagName, parentNode.getChildNodes());
  }

  /**
   * Method returns list of nodes with 'tagName' name. If node could not be found
   * then empty list is returned.
   * 
   * @param tagName
   *          name of node to look for
   * @param nodes
   *          list of input nodes
   * @return list of nodes with 'tagName' name
   */
  protected List<Node> getNodes(final String tagName, final NodeList nodes) {
    List<Node> result = new ArrayList<Node>();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeName().equalsIgnoreCase(tagName)) {
        result.add(node);
      }
    }
    return result;
  }

  /**
   * Method returns the value of node attribute. If attribute could not be found
   * then "" is returned.
   * 
   * @param attrName
   *          name of the attribute to look for
   * @param node
   *          a node
   * @return the value of node attribute, empty string("") if attribute doesn't
   *         exist
   */
  protected String getNodeAttr(final String attrName, final Node node) {
    NamedNodeMap attrs = node.getAttributes();
    for (int y = 0; y < attrs.getLength(); y++) {
      Node attr = attrs.item(y);
      if (attr.getNodeName().equalsIgnoreCase(attrName)) {
        return attr.getNodeValue();
      }
    }
    return "";
  }

  /**
   * Method returns the text value of node. If text could not be found then "" is
   * returned.
   * 
   * @param node
   *          a node
   * @return the text value of node or empty string ("") if the text could be
   *         found.
   */
  protected String getNodeValue(final Node node) {
    if (node == null) {
      return "";
    }
    NodeList childNodes = node.getChildNodes();
    for (int x = 0; x < childNodes.getLength(); x++) {
      Node data = childNodes.item(x);
      if (data.getNodeType() == Node.TEXT_NODE) {
        return data.getNodeValue();
      }
    }
    return "";
  }

  /**
   * Method returns the xml Document from input source given as a parameter.
   * 
   * @param stream
   *          input stream with xml document
   * @return Document node for the input stream
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  protected Document getXmlDocumentFromInputSource(final InputSource stream) throws InvalidXmlSchemaException {
    try {
      synchronized (db) { // DocumentBuilder cannot parse few objects at the
                          // same time
        return db.parse(stream);
      }
    } catch (SAXException e) {
      throw new InvalidXmlSchemaException("Problem with xml parser", e);
    } catch (IOException e) {
      throw new InvalidXmlSchemaException("Problem with xml parser", e);
    }
  }

  /**
   * Method returns the xml Document from text given as a source.
   * 
   * @param text
   *          string representing xml document
   * @return Document for the xml document given in the input
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  protected Document getXmlDocumentFromString(final String text) throws InvalidXmlSchemaException {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(text));
    try {
      return getXmlDocumentFromInputSource(is);
    } catch (NullPointerException e) {
      logger.error("Problem with input xml: " + text);
      throw new InvalidXmlSchemaException(e);
    }
  }

  /**
   * Transforms node into string xml format.
   * 
   * @param node
   *          node that should be transformed into xml string
   * @return string representation of the xml node
   */
  protected String nodeToString(final Node node) {
    return nodeToString(node, false);
  }

  /**
   * Transforms node into string xml format.
   * 
   * @param node
   *          node that should be transformed into xml string
   * @param includeHeadNode
   *          should the top level node exist in the output
   * @return string representation of the xml node
   */
  protected String nodeToString(final Node node, final boolean includeHeadNode) {
    if (node == null) {
      return null;
    }
    StringWriter sw = new StringWriter();
    try {
      Transformer t = TransformerFactory.newInstance().newTransformer();
      t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      t.setOutputProperty(OutputKeys.INDENT, "yes");
      t.setOutputProperty(OutputKeys.METHOD, "xml");

      NodeList list = node.getChildNodes();
      for (int i = 0; i < list.getLength(); i++) {
        Node element = list.item(i);
        t.transform(new DOMSource(element), new StreamResult(sw));
      }
    } catch (TransformerException te) {
      logger.debug("nodeToString Transformer Exception");
    }
    if (includeHeadNode) {
      return "<" + node.getNodeName() + ">" + sw.toString() + "</" + node.getNodeName() + ">";
    }
    return sw.toString();
  }

  /**
   * This method transform color encoded in string (CellDesigner format) into
   * Color.
   * 
   * @param color
   *          string representing color
   * @return Color object for the fiven string
   */
  protected Color stringToColor(final String color) {
    try {
      String alpha = color.substring(0, 2);
      Color tmp = new Color(hexToInteger(color.substring(2)));
      return new Color(tmp.getRed(), tmp.getGreen(), tmp.getBlue(), hexToInteger(alpha));
    } catch (Exception e) {
      throw new InvalidArgumentException("Invalid color string: " + color);
    }
  }

  /**
   * Transforms hex string into Integer.
   * 
   * @param hexString
   *          string representation in hex base
   * @return Integer value of the hex string
   */
  private Integer hexToInteger(String hexString) {
    return Integer.valueOf(hexString, HEX_BASE);
  }

  /**
   * Transforms Color object into string representing this color in RGB.
   * 
   * @param color
   *          color that should be converted into string
   * @return hex string representation of the color
   */
  protected String colorToString(final Color color) {
    return String.format("%08X", color.getRGB());
  }

  /**
   * Method that reads file and transforms it into a string.
   * 
   * @param fileName
   *          path to a file
   * @return string containing data from the file (default coding is used)
   * @throws IOException
   *           thrown when there are some problems with a file
   */
  protected String fileToString(final String fileName) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(fileName));
    String line = null;
    StringBuilder stringBuilder = new StringBuilder();
    String ls = System.getProperty("line.separator");

    while ((line = reader.readLine()) != null) {
      stringBuilder.append(line);
      stringBuilder.append(ls);
    }
    reader.close();

    return stringBuilder.toString();
  }

  /**
   * Method that reads all data from inputstream and transform it into a string.
   * UTF-8 coding is used.
   * 
   * @param inputStream
   *          stream from which we read data
   * @return string representing all data from input stream
   * @throws IOException
   *           thrown if there are some problems with input stream
   */
  protected String inputStreamToString(final InputStream inputStream) throws IOException {
    StringWriter writer = new StringWriter();
    IOUtils.copy(inputStream, writer, "UTF-8");
    String result = writer.toString();
    return result;
  }

  /**
   * Method that encode string into a string that can be used in xml file.
   * 
   * @param string
   *          string to be escaped
   * @return escaped string, ready to be used in xml
   */
  protected String escapeXml(final String string) {
    if (string == null) {
      return null;
    }
    // quite expensive
    return StringEscapeUtils.escapeXml(string).replaceAll("\n", "&#10;").replace("\r", "&#13;");
  }

  public List<Node> getAllNotNecessirellyDirectChild(String tagName, Node root) {
    List<Node> result = new ArrayList<>();
    for (int x = 0; x < root.getChildNodes().getLength(); x++) {
      Node node = root.getChildNodes().item(x);
      if (node.getNodeName().equalsIgnoreCase(tagName)) {
        result.add(node);
      } else {
        result.addAll(getAllNotNecessirellyDirectChild(tagName, node));
      }
    }
    return result;
  }
}
