package lcsb.mapviewer.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;

public abstract class Comparator<T extends Object> implements java.util.Comparator<T> {
  Logger logger = Logger.getLogger(Comparator.class);
  private Class<T> comparatorClazz;
  private boolean exactClassMatch;
  private List<Comparator<? extends T>> subClassComparatorList = new ArrayList<>();

  protected Comparator(Class<T> clazz) {
    this(clazz, false);
  }

  protected Comparator(Class<T> clazz, boolean exactClassMatch) {
    this.comparatorClazz = clazz;
    this.exactClassMatch = exactClassMatch;
  }

  @Override
  public final int compare(T arg0, T arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    if (arg0.getClass().equals(arg1.getClass())) {
      if (!arg0.getClass().equals(comparatorClazz)) {
        Comparator subClassComparator = getSubClassComparatorForClass(arg0.getClass());
        if (subClassComparator != null) {
          return subClassComparator.compare(arg0, arg1);
        } else if (exactClassMatch) {
          throw new NotImplementedException("Don't know how to compare classes: " + arg0.getClass());
        }
      }
      int result = compareParents(arg0, arg1);
      if (result != 0) {
        return result;
      }
      return internalCompare(arg0, arg1);
    } else {
      return -1;
    }
  }

  private Comparator getSubClassComparatorForClass(Class<? extends Object> clazz) {
    for (Comparator comparator : subClassComparatorList) {
      if (comparator.getComparatorClazz().isAssignableFrom(clazz)) {
        return comparator;
      }
    }
    return null;
  }

  private int compareParents(T arg0, T arg1) {
    Comparator parentComparator = getParentComparator();
    while (parentComparator != null) {
      int result = parentComparator.internalCompare(arg0, arg1);
      if (result != 0) {
        return result;
      }
      parentComparator = parentComparator.getParentComparator();
    }
    return 0;

  }

  protected Comparator getParentComparator() {
    return null;
  }

  protected abstract int internalCompare(T arg0, T arg1);

  protected void addSubClassComparator(Comparator<? extends T> subClassComparator) {
    subClassComparatorList.add(subClassComparator);
  }

  public Class<T> getComparatorClazz() {
    return comparatorClazz;
  }

}
