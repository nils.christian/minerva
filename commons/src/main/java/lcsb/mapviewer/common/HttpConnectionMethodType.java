package lcsb.mapviewer.common;

/**
 * Defines type of http connection method.
 * 
 * @author Piotr Gawron
 * 
 */
public enum HttpConnectionMethodType {
	
	/**
	 * GET method.
	 */
	GET, //
	
	/**
	 * POST method.
	 */
	POST;
}
