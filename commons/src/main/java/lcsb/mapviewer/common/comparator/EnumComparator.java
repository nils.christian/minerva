package lcsb.mapviewer.common.comparator;

import java.awt.Color;
import java.util.Comparator;

/**
 * Comparator implementation for {@link Color} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class EnumComparator implements Comparator<Enum<?>> {

	@Override
	public int compare(Enum arg0, Enum arg1) {
		if (arg0 == null) {
			if (arg1 == null) {
				return 0;
			} else {
				return 1;
			}

		} else if (arg1 == null) {
			return -1;
		}
		return arg0.compareTo(arg1);
	}

}
