package lcsb.mapviewer.common.comparator;

import java.util.Comparator;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * Comparator used for comparing sets of strings.
 * 
 * @author Piotr Gawron
 * 
 */
public class SetComparator<T> implements Comparator<Set<T>> {
  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(SetComparator.class);

  private Comparator<T> objectComparator;

  public SetComparator(Comparator<T> objectComparator) {
    this.objectComparator = objectComparator;
  }

  @Override
  public int compare(Set<T> arg0, Set<T> arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    for (T objectInList1 : arg1) {
      boolean found = false;
      for (T objectInList0 : arg0) {
        if (objectComparator.compare(objectInList0, objectInList1) == 0) {
          found = true;
        }
      }
      if (!found) {
        logger.debug("Cannot find object " + objectInList1 + " in set: " + arg0);
        return 1;
      }
    }

    for (T objectInList0 : arg0) {
      boolean found = false;
      for (T objectInList1 : arg1) {
        if (objectComparator.compare(objectInList0, objectInList1) == 0) {
          found = true;
        }
      }
      if (!found) {
        logger.debug("Cannot find object " + objectInList0 + " in set: " + arg1);
        return 1;
      }
    }
    return 0;
  }

}
