package lcsb.mapviewer.utils.quadtree;

import lcsb.mapviewer.utils.quadtree.quadtrees.AreaQuadTreeTest;
import lcsb.mapviewer.utils.quadtree.quadtrees.PointQuadTreeTest;
import lcsb.mapviewer.utils.quadtree.quadtrees.SegmentQuadTreeTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SegmentQuadTreeTest.class, PointQuadTreeTest.class, AreaQuadTreeTest.class })
public class SuitedTests {

}
// //////////////////////////////////////////////////////
// //
// TODO TODO TODO TODO TODO TODO TODO TODO TODO //
// //
// TESTOWAĆ TESTOWAĆ TESTOWAĆ TESTOWAĆ TESTOWAĆ //
// //
// TODO TODO TODO TODO TODO TODO TODO TODO TODO //
// //
// //////////////////////////////////////////////////////