package lcsb.mapviewer.utils.quadtree.quadtrees;

import static org.junit.Assert.assertEquals;

import java.awt.Point;

import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;

import org.apache.log4j.Logger;
import org.junit.Test;

public class PointQuadTreeTest  {
	Logger	logger	= Logger.getLogger(PointQuadTreeTest.class);

	@Test
	public void test1() {
		PointQuadTree qt = new PointQuadTree();
		String start = qt.toString();
		MyLocation first = new MyLocation(new Point(1, 1));
		MyLocation second = new MyLocation(new Point(4, 6));
		qt.add(first);
		qt.add(second);
		qt.remove(first);
		qt.remove(second);
		String end = qt.toString();
		assertEquals(start, end);
	}
}
