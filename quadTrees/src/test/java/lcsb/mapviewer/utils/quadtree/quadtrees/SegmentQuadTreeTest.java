package lcsb.mapviewer.utils.quadtree.quadtrees;

import static org.junit.Assert.assertEquals;

import java.awt.Point;

import lcsb.mapviewer.utils.quadtree.containers.Edge;
import lcsb.mapviewer.utils.quadtree.containers.MyPolyline;

import org.apache.log4j.Logger;
import org.junit.Test;

public class SegmentQuadTreeTest  {
	Logger	logger	= Logger.getLogger(SegmentQuadTreeTest.class);

	@Test
	public void test1() {
		SegmentQuadTree qt = new SegmentQuadTree();
		String start = qt.toString();
		// Point referencePoint = new Point(2,2);
		Edge edge1 = new Edge(new Point(1, 1), new Point(2, 2));
		Edge edge2 = new Edge(new Point(1, 1), new Point(2, 3));
		Edge edge3 = new Edge(new Point(1, 2), new Point(2, 2));
		MyPolyline myPolyline1 = new MyPolyline();
		MyPolyline myPolyline2 = new MyPolyline();
		myPolyline1.add(edge1);
		myPolyline1.add(edge2);
		myPolyline2.add(edge3);
		qt.add(myPolyline1);
		qt.add(myPolyline2);
		qt.remove(myPolyline1);
		qt.remove(myPolyline2);
		String end = qt.toString();
		assertEquals(start, end);
	}
}