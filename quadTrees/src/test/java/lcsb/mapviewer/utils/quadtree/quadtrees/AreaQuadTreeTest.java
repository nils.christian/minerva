package lcsb.mapviewer.utils.quadtree.quadtrees;

import static org.junit.Assert.assertEquals;

import java.awt.Point;

import lcsb.mapviewer.utils.quadtree.containers.MySquare;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;

import org.apache.log4j.Logger;
import org.junit.Test;

public class AreaQuadTreeTest {
	Logger	logger	= Logger.getLogger(AreaQuadTreeTest.class);

	@Test
	public void test() {
		AreaQuadTree qt = new AreaQuadTree();
		MySquare ms = new MySquare(new Point(0, 0), new Point(1, 0), new Point(1, 10), new Point(0, 10));
		String before = qt.toString();
		qt.add(ms);
		MySquare ms2 = new MySquare(new Point(0, 0), new Point(1, 0), new Point(1, 11), new Point(0, 11));
		qt.add(ms2);
		logger.debug(qt.find(new MyLocation(new Point(23, 7))));
		qt.remove(ms);
		qt.remove(ms2);
		String after = qt.toString();
		assertEquals(before, after);
	}
}
