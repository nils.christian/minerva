package lcsb.mapviewer.utils.quadtree.containers;

import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;

/**
 * Implements MyShape. Abstract class, which has field with points of given
 * shape.
 * 
 * @author Artur Laskowski
 * @see MyShape
 */
public abstract class Container implements MyShape {
	private MyLocation[] points;

	@Override
	public MyLocation[] getPoints() {
		return points;
	}

	public void setPoints(MyLocation[] points) {
		this.points = points;
	}
}
