package lcsb.mapviewer.utils.quadtree.testquadtrees.baskets;

import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.containers.MySquare;

public class AliasBasket {

	public IRectangleShape	alias;
	public MySquare					square;

	public AliasBasket(IRectangleShape alias, MySquare square) {
		this.alias = alias;
		this.square = square;
	}
}
