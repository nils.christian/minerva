package lcsb.mapviewer.utils.quadtree.containers;

import java.util.ArrayList;
import java.util.List;

/**
 * Keeps set of edges.
 * 
 * @author Artur Laskowski
 * 
 */
public class MyPolyline {
	public List<Edge>	edges;

	/**
	 * Create empty set of edges.
	 */
	public MyPolyline() {
		this.edges = new ArrayList<Edge>();
	}

	/**
	 * Creates new MyPolyline object from set of edges.
	 * 
	 * @param edges objects stored in this {@link MyPolyline}
	 */
	public MyPolyline(List<Edge> edges) {
		this.edges = edges;
	}

	/**
	 * Adds edge to {@link #edges}.
	 * 
	 * @param edge
	 *          object to add
	 */
	public void add(Edge edge) {
		this.edges.add(edge);
	}

	public String toString() {
		String description = "Reaction[size: " + this.edges.size() + "] Edges: \n";
		for (Edge edge : edges) {
			description += edge.toString();
		}
		return description;
	}
}
