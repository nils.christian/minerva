package lcsb.mapviewer.utils.quadtree;

public class InvalidQuadTreeStateException extends RuntimeException {

	public InvalidQuadTreeStateException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

}
