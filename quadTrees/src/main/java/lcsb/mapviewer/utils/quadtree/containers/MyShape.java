package lcsb.mapviewer.utils.quadtree.containers;

import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;

/**
 * Interface which every shape that could be stored in tree need to implements.
 * 
 * @author Artur Laskowski
 * 
 */
public interface MyShape {
	MyLocation[] getPoints();

	boolean contains(MyLocation location);
}