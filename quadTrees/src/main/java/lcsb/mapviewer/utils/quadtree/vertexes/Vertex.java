package lcsb.mapviewer.utils.quadtree.vertexes;

import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;

/**
 * Abstract class with each significant information for inheriting class. Vertex
 * is structure that reflects part of map given by 'field'.
 * 
 * @author Artur Laskowski
 * 
 */

public abstract class Vertex {
	public MyRectangle	field;
	public int					index;
	public Kompozycja		parent;

	public abstract String toString();

	/**
	 * Constructor where you need to give every information separately. This
	 * should be used only while creating new Vertex.
	 * 
	 * @param field
	 *          - part of map
	 * @param index
	 *          - of vertex within the whole tree structure
	 * @param parent
	 *          - of this vertex
	 */
	public Vertex(MyRectangle field, int index, Kompozycja parent) {
		this.field = field;
		this.index = index;
		this.parent = parent;
	}

	/**
	 * Constructor that is used during transforming one vertex into another.
	 * 
	 * @param vertex
	 *          - previous vertex, informations about field, index and parent are
	 *          copied from it.
	 */
	public Vertex(Vertex vertex) {
		this(vertex.field, vertex.index, vertex.parent);
	}
}
