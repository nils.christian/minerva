package lcsb.mapviewer.utils.quadtree.quadtrees;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.containers.Edge;
import lcsb.mapviewer.utils.quadtree.containers.MyPolyline;
import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;
import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;
import lcsb.mapviewer.utils.quadtree.vertexes.BlackVertex;
import lcsb.mapviewer.utils.quadtree.vertexes.GreenVertex;
import lcsb.mapviewer.utils.quadtree.vertexes.Kompozycja;
import lcsb.mapviewer.utils.quadtree.vertexes.OrangeVertex;
import lcsb.mapviewer.utils.quadtree.vertexes.VertexWithShapes;
import lcsb.mapviewer.utils.quadtree.vertexes.WhiteVertex;

import org.apache.log4j.Logger;

/**
 * Extends PointQuadTree.
 * 
 * @author Artur Laskowski
 * @see PointQuadTree
 * 
 */
public class SegmentQuadTree extends PointQuadTree {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger				logger	= Logger.getLogger(SegmentQuadTree.class);

	protected List<MyPolyline>	myPolylines;

	/**
	 * 
	 * @param model
	 *          - for size of map.
	 */
	protected SegmentQuadTree(IRectangleShape model) {
		super(model);
		myPolylines = new ArrayList<MyPolyline>();
	}

	protected SegmentQuadTree() {
		super();
		myPolylines = new ArrayList<MyPolyline>();
	}

	/**
	 * 
	 * @param myPolyline
	 */
	protected void add(MyPolyline myPolyline) {
		this.myPolylines.add(myPolyline);
		for (Edge edge : myPolyline.edges) {
			add(edge);
		}
	}

	/**
	 * 
	 * @param polyline
	 */
	public void remove(MyPolyline polyline) {
		remove(polyline, true);
	}

	/**
	 * 
	 * @param myPolyline
	 * @param clean
	 *          : - false - do not use default system of online keeping tree in
	 *          good condition, highly not recommended. Faster, but tree could not
	 *          work without global cleaning before any query. - true - use
	 *          default system of online keeping tree in good condition.
	 */
	protected void remove(MyPolyline myPolyline, boolean clean) {
		this.myPolylines.remove(myPolyline);
		for (Edge edge : myPolyline.edges) {
			remove(edge, clean);
		}
	}

	/**
	 * This find reaction that is closest to given point.
	 * 
	 * @param point
	 * @return
	 */
	protected Set<MyPolyline> findReaction(Point point) {
		// TODO should be made better
		Set<MyShape> edges = findEdge(point);
		Set<MyPolyline> result = new HashSet<MyPolyline>();
		for (MyShape edge : edges) {
			for (MyPolyline myPolyline : this.myPolylines) {
				for (Edge temporaryEdge : myPolyline.edges) {
					if (temporaryEdge.equals(edge)) {
						result.add((MyPolyline) myPolyline);
					}
				}
			}
		}
		return result;
	}

	/**
	 * This find edge that is closest to given point.
	 * 
	 * @param point
	 * @return
	 */
	private Set<MyShape> findEdge(Point point) {
		MyLocation myLocation = new MyLocation(point);
		return find(myLocation);
	}

	/**
	 * Adds both points of reaction and then container.
	 * 
	 * @param edge
	 *          object to add
	 */
	private void add(Edge edge) {
		add(edge.getPoints()[0]);
		add(edge.getPoints()[1]);
		addContainer(edge, root);
	}

	@Override
	protected void addPoint(MyLocation myLocation, Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof WhiteVertex) {
			addPointToWhite(myLocation, kompozycja);
		} else if (kompozycja.vertex instanceof GreenVertex) {
			addPointToGreen(myLocation, kompozycja);
		} else if (kompozycja.vertex instanceof OrangeVertex) {
			addPointToOrange(myLocation, kompozycja);
		} else if (kompozycja.vertex instanceof BlackVertex) {
			addPointToBlack(myLocation, kompozycja);
		}
	}

	/**
	 * If point need to be added to GreenVertex, then function need to transform
	 * it into Black one.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 */
	private void addPointToGreen(MyLocation myLocation, Kompozycja kompozycja) {
		kompozycja.vertex = new BlackVertex(kompozycja.vertex, ((GreenVertex) kompozycja.vertex).containers, myLocation);
	}

	@Override
	protected void addPointToBlack(MyLocation myLocation, Kompozycja kompozycja) {
		List<Kompozycja> sons = new ArrayList<Kompozycja>();
		MyRectangle[] rectangles = kompozycja.vertex.field.split();
		for (MyRectangle rectangle : rectangles) {
			Set<MyShape> temporaryEdges = ((VertexWithShapes) kompozycja.vertex).getContainers(rectangle);
			if (rectangle.contains(((BlackVertex) kompozycja.vertex).point)) {
				sons.add(new Kompozycja(new BlackVertex(rectangle, numberOfVertices++, kompozycja, temporaryEdges, ((BlackVertex) kompozycja.vertex).point)));
			} else {
				if (temporaryEdges.size() == 0) {
					sons.add(new Kompozycja(new WhiteVertex(rectangle, numberOfVertices++, kompozycja)));
				} else {
					sons.add(new Kompozycja(new GreenVertex(rectangle, numberOfVertices++, kompozycja, temporaryEdges)));
				}
			}
		}
		kompozycja.vertex = new OrangeVertex(kompozycja.vertex, sons);
		addPoint(myLocation, kompozycja);
	}

	/**
	 * For given structure of tree. We need to put containers into proper
	 * vertexes.
	 * 
	 * @param square
	 * @param kompozycja
	 */
	protected void addContainer(MyShape square, Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof WhiteVertex) {
			addEdgeToWhite(square, kompozycja);
		} else if (kompozycja.vertex instanceof GreenVertex) {
			addEdgeToGreen(square, kompozycja);
		} else if (kompozycja.vertex instanceof OrangeVertex) {
			addEdgeToOrange(square, kompozycja);
		} else if (kompozycja.vertex instanceof BlackVertex) {
			addEdgeToBlack(square, kompozycja);
		}
	}

	/**
	 * If one add container to WhiteVertex it transforms into Green one.
	 * 
	 * @param square
	 * @param kompozycja
	 */
	private void addEdgeToWhite(MyShape square, Kompozycja kompozycja) {
		Set<MyShape> edges = new HashSet<MyShape>();
		edges.add(square);
		kompozycja.vertex = new GreenVertex(kompozycja.vertex, edges);
	}

	/**
	 * Adding containers to GreenVertex is allowed.
	 * 
	 * @param square
	 * @param kompozycja
	 */
	private void addEdgeToGreen(MyShape square, Kompozycja kompozycja) {
		((GreenVertex) kompozycja.vertex).containers.add(square);
	}

	/**
	 * Adding containers to OrangeVertex means adding it to proper sons.
	 * 
	 * @param square
	 * @param kompozycja
	 */
	private void addEdgeToOrange(MyShape square, Kompozycja kompozycja) {
		for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
			if (temporaryKompozycja.vertex.field.contains(square)) {
				addContainer(square, temporaryKompozycja);
			}
		}
	}

	/**
	 * Adding containers to BlackVertex is allowed.
	 * 
	 * @param square
	 * @param kompozycja
	 */
	private void addEdgeToBlack(MyShape square, Kompozycja kompozycja) {
		((BlackVertex) kompozycja.vertex).containers.add(square);
	}

	/**
	 * Removing edge from tree.
	 * 
	 * @param edge object to remove
	 */
	public void remove(Edge edge) {
		remove(edge, true);
	}

	/**
	 * One can set clean as false and tree will loose it proper structure. Not
	 * recommended.
	 * 
	 * @param edge
	 *          object to remove
	 * @param clean
	 *          should the tree be restored to usable state after removing or not.
	 *          If tree is not cleaned after removing then querying won't work (it
	 *          should be done manually before querying).
	 * 
	 */
	private void remove(Edge edge, boolean clean) {
		removeContainer(edge);
		remove(edge.getPoints()[0], clean);
		remove(edge.getPoints()[1], clean);
	}

	/**
	 * 
	 * @param square
	 */
	protected void removeContainer(MyShape square) {
		removeContainer(root, square);
	}

	/**
	 * 
	 * @param kompozycja
	 * @param square
	 */
	private void removeContainer(Kompozycja kompozycja, MyShape square) {
		if (kompozycja.vertex instanceof GreenVertex) {
			removeEdgeFromGreen(kompozycja, square);
		} else if (kompozycja.vertex instanceof BlackVertex) {
			removeEdgeFromBlack(kompozycja, square);
		} else if (kompozycja.vertex instanceof OrangeVertex) {
			for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
				removeContainer(temporaryKompozycja, square);
			}
		}
	}

	/**
	 * Removing containers from black is allowed.
	 * 
	 * @param kompozycja
	 * @param square
	 */
	private void removeEdgeFromBlack(Kompozycja kompozycja, MyShape square) {
		((BlackVertex) kompozycja.vertex).containers.remove(square);
	}

	/**
	 * If after removing containers list of containers is empty one need to
	 * transform this vertex to WhiteVertex.
	 * 
	 * @param kompozycja
	 * @param square
	 */
	private void removeEdgeFromGreen(Kompozycja kompozycja, MyShape square) {
		((GreenVertex) kompozycja.vertex).containers.remove(square);
		if (((GreenVertex) kompozycja.vertex).containers.size() == 0) {
			kompozycja.vertex = new WhiteVertex(kompozycja.vertex);
		}
	}

	@Override
	protected Kompozycja removePointFromBlack(MyLocation myLocation, Kompozycja kompozycja) {
		if (((BlackVertex) kompozycja.vertex).point.times == 1) {
			if (((BlackVertex) kompozycja.vertex).containers.size() == 0) {
				myLocations.remove(myLocations.indexOf(((BlackVertex) kompozycja.vertex).point));
				kompozycja.vertex = new WhiteVertex(kompozycja.vertex);
			} else {
				myLocations.remove(myLocations.indexOf(((BlackVertex) kompozycja.vertex).point));
				kompozycja.vertex = new GreenVertex(kompozycja.vertex, ((BlackVertex) kompozycja.vertex).containers);
			}
		} else {
			myLocations.get(myLocations.indexOf(((BlackVertex) kompozycja.vertex).point)).times--;
		}
		return kompozycja;
	}

	@Override
	protected void cleanTree(Queue<Kompozycja> queue) {
		while (queue.isEmpty() == false) {
			Kompozycja kompozycja = queue.poll();
			if (kompozycja.vertex instanceof OrangeVertex) {
				if (!((OrangeVertex) kompozycja.vertex).hasOrange()) {
					if (((OrangeVertex) kompozycja.vertex).countBlack() == 1) {
						MyLocation myLocation = null;
						Set<MyShape> temporaryEdges = new HashSet<MyShape>();
						for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
							if (temporaryKompozycja.vertex instanceof BlackVertex) {
								myLocation = ((BlackVertex) temporaryKompozycja.vertex).point;
								temporaryEdges.addAll(((BlackVertex) temporaryKompozycja.vertex).containers);
							} else if (temporaryKompozycja.vertex instanceof GreenVertex) {
								temporaryEdges.addAll(((GreenVertex) temporaryKompozycja.vertex).containers);
							}
						}
						kompozycja.vertex = new BlackVertex(kompozycja.vertex, temporaryEdges, myLocation);
					} else if (((OrangeVertex) kompozycja.vertex).countBlack() == 0) {
						Set<MyShape> temporaryContainers = new HashSet<MyShape>();
						for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
							if (temporaryKompozycja.vertex instanceof VertexWithShapes) {
								temporaryContainers.addAll(((VertexWithShapes) temporaryKompozycja.vertex).containers);
							}
						}
						if (temporaryContainers.size() == 0) {
							kompozycja.vertex = new WhiteVertex(kompozycja.vertex);
						} else {
							kompozycja.vertex = new GreenVertex(kompozycja.vertex, temporaryContainers);
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param myLocation
	 * @return
	 */
	private Set<MyShape> find(MyLocation myLocation) {
		// looking for edge within the same vertex where points is.
		Edge edge = find(myLocation, root);
		// checking if this found edge is closest to this point.
		return checkNearest(myLocation, edge);
	}

	/**
	 * For given edge it check if some other is closest to point.
	 * 
	 * @param myLocation
	 * @param edge
	 * @return
	 */
	private Set<MyShape> checkNearest(MyLocation myLocation, Edge edge) {
		Set<MyShape> edges = new HashSet<MyShape>();
		edges.add(edge);
		return checkNearest(myLocation, edges, root);
	}

	/**
	 * 
	 * @param myLocation
	 * @param edges
	 * @param kompozycja
	 * @return
	 */
	private Set<MyShape> checkNearest(MyLocation myLocation, Set<MyShape> edges, Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof OrangeVertex) {
			for (Kompozycja son : ((OrangeVertex) kompozycja.vertex).sons) {
				if (son.vertex.field.contains(myLocation, ((Edge) edges.iterator().next()).distance(myLocation))) {
					edges = checkNearest(myLocation, edges, son);
				}
			}
		} else if (kompozycja.vertex instanceof VertexWithShapes) {
			int distance = ((Edge) edges.iterator().next()).distance(myLocation);
			for (MyShape temporaryEdge : ((VertexWithShapes) kompozycja.vertex).containers) {
				if (((Edge) temporaryEdge).distance(myLocation) < distance) {
					distance = ((Edge) temporaryEdge).distance(myLocation);
					edges.clear();
					edges.add(temporaryEdge);
				} else if (((Edge) temporaryEdge).distance(myLocation) == distance) {
					edges.add(temporaryEdge);
				}
			}
		}
		return edges;
	}

	/**
	 * Looking for some edge inside of Vertex where point lays. If this vertex is
	 * empty for edges, then function takes one edge from neighbor of this vertex.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 * @return
	 */
	private Edge find(MyLocation myLocation, Kompozycja kompozycja) {
		Edge edge = null;
		if (kompozycja.vertex instanceof OrangeVertex) {
			if (((OrangeVertex) kompozycja.vertex).hasOrange()) {
				for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
					if (temporaryKompozycja.vertex.field.contains(myLocation)) {
						edge = find(myLocation, temporaryKompozycja);
					}
				}
			} else {
				int distance = Integer.MAX_VALUE;
				for (Kompozycja son : ((OrangeVertex) kompozycja.vertex).sons) {
					if (son.vertex instanceof VertexWithShapes) {
						for (MyShape temporaryEdge : ((VertexWithShapes) son.vertex).containers) {
							if (((Edge) temporaryEdge).distance(myLocation) < distance) {
								distance = ((Edge) temporaryEdge).distance(myLocation);
								edge = (Edge) temporaryEdge;
							}
						}
					}
				}
			}
		}
		return edge;
	}

	@Override
	public String toString() {
		String description = "\nSegmentTree: \n";
		description += root.toString();
		return description;
	}
}
