package lcsb.mapviewer.utils.quadtree.containers;

import java.awt.Point;
import java.awt.geom.Line2D;

import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;

/**
 * Extends Container. Has 2 points.
 * 
 * @author Artur Laskowski
 * @see Container
 */
public class Edge extends Container {
	/**
	 * Creates edge container.
	 * 
	 * @param firstPoint
	 *          first point of the line
	 * @param secondPoint
	 *          second point of the line
	 */
	public Edge(Point firstPoint, Point secondPoint) {
		setPoints(new MyLocation[2]);
		getPoints()[0] = new MyLocation(firstPoint);
		getPoints()[1] = new MyLocation(secondPoint);
	}

	/**
	 * Distance from edge to 'myLocation'.
	 * 
	 * @param myLocation
	 *          from this point distance is computed
	 * @return distance between this object and param myLocation
	 */
	public int distance(MyLocation myLocation) {
		Line2D.Double line = new Line2D.Double(getPoints()[0].point, getPoints()[1].point);
		return (int) line.ptSegDist(myLocation.point);
	}

	@Override
	public String toString() {
		String description = "Edge [startPoint: " + getPoints()[0] + " ][endPoint: " + getPoints()[1] + " ]\n";
		return description;
	}

	@Override
	public boolean contains(MyLocation location) {
		if (this.distance(location) == 0) {
			return true;
		}
		return false;
	}
}