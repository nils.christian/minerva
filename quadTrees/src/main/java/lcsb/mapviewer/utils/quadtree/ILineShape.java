package lcsb.mapviewer.utils.quadtree;

import java.awt.geom.Line2D;
import java.util.Collection;

public interface ILineShape {

	Collection<Line2D> getLines();

}
