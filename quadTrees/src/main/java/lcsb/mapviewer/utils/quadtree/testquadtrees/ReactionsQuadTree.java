package lcsb.mapviewer.utils.quadtree.testquadtrees;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.utils.quadtree.ILineShape;
import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.containers.Edge;
import lcsb.mapviewer.utils.quadtree.containers.MyPolyline;
import lcsb.mapviewer.utils.quadtree.quadtrees.SegmentQuadTree;
import lcsb.mapviewer.utils.quadtree.testquadtrees.baskets.ReactionBasket;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ReactionsQuadTree extends SegmentQuadTree {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger					logger	= Logger.getLogger(SegmentQuadTree.class);
	static {
		PropertyConfigurator.configure("src/main/webapp/WEB-INF/resources/log4j.properties");
	}

	private List<ReactionBasket>	listOfReactions;

	public ReactionsQuadTree() {
		super();
		listOfReactions = new ArrayList<ReactionBasket>();
	}

	public ReactionsQuadTree(IRectangleShape model) {
		super(model);
		listOfReactions = new ArrayList<ReactionBasket>();
	}

	public void add(ILineShape reaction) {
		MyPolyline result = new MyPolyline();
		for (Line2D line : reaction.getLines()) {
			result.add(new Edge(new Point((int) (double) line.getX1(), ((int) (double) line.getY1())), new Point((int) (double) line.getX2(), ((int) (double) line
					.getY2()))));
		}
		listOfReactions.add(new ReactionBasket(reaction, result));
		add(result);
	}

	public void remove(ILineShape reaction) {
		remove(reaction, true);
	}

	public void remove(ILineShape reaction, boolean clean) {
		ReactionBasket reactionBasket = null;
		for (ReactionBasket temporaryReactionBasket : listOfReactions) {
			if (temporaryReactionBasket.reaction.equals(reaction)) {
				reactionBasket = temporaryReactionBasket;
				break;
			}
		}
		listOfReactions.remove(reactionBasket);
		remove(reactionBasket.myPolyline, clean);
	}

	public Set<ILineShape> findReactions(Point point) {
		Set<MyPolyline> myPolylines = findReaction(point);
		Set<ILineShape> reactions = new HashSet<ILineShape>();
		for (ReactionBasket temporaryReactionBasket : listOfReactions) {
			for (MyPolyline temporaryReaction : myPolylines) {
				if (temporaryReactionBasket.myPolyline.equals(temporaryReaction)) {
					reactions.add(temporaryReactionBasket.reaction);
				}
			}
		}
		return reactions;
	}

}
