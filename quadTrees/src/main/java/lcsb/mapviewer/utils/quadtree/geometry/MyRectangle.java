package lcsb.mapviewer.utils.quadtree.geometry;

import java.awt.Point;
import java.awt.Rectangle;

import lcsb.mapviewer.utils.quadtree.containers.Edge;
import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.containers.MySquare;

import org.apache.log4j.Logger;

/**
 * Extends Rectangle.
 * 
 * @author Artur Laskowski
 * @see Rectangle
 * 
 */
@SuppressWarnings("serial")
public class MyRectangle extends Rectangle {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger	= Logger.getLogger(MyRectangle.class);

	/**
	 * 
	 * @param point
	 * @param width
	 * @param height
	 */
	public MyRectangle(Point point, int width, int height) {
		super(point);
		this.height = height;
		this.width = width;
	}

	/**
	 * 
	 * @param shape
	 * @return boolean information about overlapping with shape
	 */
	public boolean contains(MyShape shape) {
		if (shape instanceof Edge) {
			int x1 = ((Edge) shape).getPoints()[0].point.x;
			int x2 = ((Edge) shape).getPoints()[1].point.x;
			int y1 = ((Edge) shape).getPoints()[0].point.y;
			int y2 = ((Edge) shape).getPoints()[1].point.y;
			return super.intersectsLine(x1, y1, x2, y2);
		} else if (shape instanceof MySquare) {
			MyRectangle rectangle = new MyRectangle(((MySquare) shape).getPoints()[0].point, ((MySquare) shape).getPoints()[1].point.x
					- ((MySquare) shape).getPoints()[0].point.x, ((MySquare) shape).getPoints()[2].point.y - ((MySquare) shape).getPoints()[1].point.y);
			Rectangle result = (Rectangle) (this.createIntersection(rectangle));
			if (result.height <= 0 || result.width <= 0) {
				return false;
			}
			return true;
		} // TODO throw
		return false;
	}

	/**
	 * 
	 * @param myLocation
	 * @return boolean information about containing of location 'myLocation' by
	 *         this.
	 */
	public boolean contains(MyLocation myLocation) {
		if (this.contains(myLocation.point)) {
			return true;
		}
		return false;
	}

	/**
	 * Answers question about containing 'myLocation' with ray 'size' inside of
	 * this rectangle.
	 * 
	 * @param myLocation
	 * @param size
	 * @return boolean answer.
	 */
	public boolean contains(MyLocation myLocation, int size) {
		Point start = new Point(myLocation.point.x - size, myLocation.point.y - size);
		MyRectangle rectangle = new MyRectangle(start, 2 * size, 2 * size);
		Point point1 = new Point(myLocation.point.x - size, myLocation.point.y - size);
		Point point2 = new Point(myLocation.point.x - size, myLocation.point.y + size);
		Point point3 = new Point(myLocation.point.x + size, myLocation.point.y + size);
		Point point4 = new Point(myLocation.point.x + size, myLocation.point.y - size);

		Point point5 = new Point(this.x, this.y);
		Point point6 = new Point(this.x, this.y + this.height);
		Point point7 = new Point(this.x + this.width, this.y + this.height);
		Point point8 = new Point(this.x + this.width, this.y);
		return (this.contains(point1) || this.contains(point2) || this.contains(point3) || this.contains(point4) || rectangle.contains(point5)
				|| rectangle.contains(point6) || rectangle.contains(point7) || rectangle.contains(point8));
	}

	/**
	 * Splits rectangle into four 'equals' rectangles.
	 * 
	 * @return
	 */
	public MyRectangle[] split() {
		MyRectangle[] splitedRectangle = new MyRectangle[4];

		int firstHalfX = (int) (this.getWidth() / 2);
		int secondHalfX = (int) this.getWidth() - firstHalfX;
		int firstHalfY = (int) (this.getHeight() / 2);
		int secondHalfY = (int) this.getHeight() - firstHalfY;

		Point tl = this.getLocation();
		Point bl = new Point();
		bl.setLocation(this.getX(), this.getY() + firstHalfY);
		Point tr = new Point();
		tr.setLocation(this.getX() + firstHalfX, this.getY());
		Point br = new Point();
		br.setLocation(this.getX() + firstHalfX, this.getY() + firstHalfY);

		MyRectangle topL = new MyRectangle(tl, firstHalfX, firstHalfY);
		MyRectangle topR = new MyRectangle(tr, secondHalfX, firstHalfY);
		MyRectangle botL = new MyRectangle(bl, firstHalfX, secondHalfY);
		MyRectangle botR = new MyRectangle(br, secondHalfX, secondHalfY);

		splitedRectangle[0] = topL;
		splitedRectangle[1] = topR;
		splitedRectangle[2] = botL;
		splitedRectangle[3] = botR;

		return splitedRectangle;
	}

	@Override
	public String toString() {
		String description = "MyRectangle[x: " + x + ", y: " + y + " ][width: " + width + " ][height: " + height + "]\n";
		return description;
	}
}
