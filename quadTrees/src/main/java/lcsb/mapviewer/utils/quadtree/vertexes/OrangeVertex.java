package lcsb.mapviewer.utils.quadtree.vertexes;

import java.util.List;

/**
 * Extends Vertex. Only vertex class that is not a leaf of tree. Held four sons.
 * 
 * @author Artur Laskowski
 * @see Vertex
 */
public class OrangeVertex extends Vertex {
	public List<Kompozycja>	sons;

	/**
	 * Constructor that is used during transforming one vertex into another.
	 * 
	 * @param vertex
	 *          - previous vertex, informations about field, index and parent are
	 *          copied from it.
	 * @param sons
	 *          - list of four sons for this vertex.
	 */
	public OrangeVertex(Vertex vertex, List<Kompozycja> sons) {
		super(vertex);
		this.sons = sons;
	}

	/**
	 * 
	 * @return number of BlackVertex which are sons of this OrangeVertex.
	 */
	public int countBlack() {
		int res = 0;
		for (Kompozycja kompozycja : this.sons) {
			if (kompozycja.vertex instanceof BlackVertex) {
				res++;
			}
		}
		return res;
	}

	/**
	 * 
	 * @return boolean information of existence the orange sons of this
	 *         OrangeVertex
	 */
	public boolean hasOrange() {
		for (Kompozycja kompozycja : this.sons) {
			if (kompozycja.vertex instanceof OrangeVertex) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String description = "OrangeVertex [index: " + index + "][number of sons: " + sons.size() + "]\nlist of sons:\n";
		for (Kompozycja kompozycja : this.sons) {
			description += kompozycja.toString();
		}
		return description;
	}
}
