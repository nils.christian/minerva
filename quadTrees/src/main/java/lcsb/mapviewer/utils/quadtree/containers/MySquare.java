package lcsb.mapviewer.utils.quadtree.containers;

import java.awt.Point;

import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;

/**
 * Extends Container. Has 4 points.
 * 
 * @author Artur Laskowski
 * 
 */
public class MySquare extends Container {

	/**
	 * Creates square container from four points.
	 * 
	 * @param first
	 *          top left corner
	 * @param second
	 *          top right corner
	 * @param third
	 *          bottom right corner
	 * @param fourth
	 *          bottom left corner
	 */
	public MySquare(Point first, Point second, Point third, Point fourth) {
		setPoints(new MyLocation[4]);
		getPoints()[0] = new MyLocation(first);
		getPoints()[1] = new MyLocation(second);
		getPoints()[2] = new MyLocation(third);
		getPoints()[3] = new MyLocation(fourth);
	}

	/**
	 * Construct MySquare from alias.
	 * 
	 * @param alias
	 *          {@link Alias} from which shape is created
	 */
	public MySquare(IRectangleShape alias) {
		this(new Point((int) (double) alias.getX(), (int) (double) alias.getY()), new Point(
				(int) (double) (alias.getX() + alias.getWidth()), (int) (double) alias.getY()), new Point(
				(int) (double) (alias.getX() + alias.getWidth()), (int) (double) (alias.getY() + alias.getHeight())), new Point(
				(int) (double) alias.getX(), (int) (double) (alias.getY() + alias.getHeight())));
	}

	@Override
	public boolean contains(MyLocation location) {
		if (location.point.x >= this.getPoints()[0].point.x) {
			if (location.point.x <= this.getPoints()[1].point.x) {
				if (location.point.y >= this.getPoints()[0].point.y) {
					if (location.point.y <= this.getPoints()[2].point.y) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public String toString() {
		String description = "Squre [first: " + getPoints()[0] + " ][second: " + getPoints()[1] + " ][third: " + getPoints()[2] + " ][fourth: " + getPoints()[3]
				+ " ]\n";
		return description;
	}
}
