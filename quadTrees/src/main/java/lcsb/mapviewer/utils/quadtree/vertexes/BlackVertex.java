package lcsb.mapviewer.utils.quadtree.vertexes;

import java.util.Set;

import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;
import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;

/**
 * Extends abstract class VertexWithShapes. This class contains also exactly one
 * location.
 * 
 * @see VertexWithShapes
 * @author Artur Laskowski
 * 
 */
public class BlackVertex extends VertexWithShapes {
	// TODO change to Point2D, without MyLocation
	/**
	 * Field responsible for location of point.
	 */
	public MyLocation point;

	/**
	 * Constructor that is used during transforming one vertex into another.
	 * 
	 * @param vertex
	 *          - previous vertex, informations about field, index and parent are
	 *          copied from it.
	 * @param containers
	 *          - list of containers that lays within given vertex
	 * @param point
	 *          - location of point
	 */
	public BlackVertex(Vertex vertex, Set<MyShape> containers, MyLocation point) {
		super(vertex, containers);
		this.point = point;
	}

	/**
	 * Constructor where you need to give every information separately. This
	 * should be used only while creating new Vertex.
	 * 
	 * @param field
	 *          - part of map
	 * @param index
	 *          - of vertex within the whole tree structure
	 * @param parent
	 *          - of this vertex
	 * @param containers
	 *          - list of containers that lays within given vertex
	 * @param point
	 *          - location of point
	 */
	public BlackVertex(MyRectangle field, int index, Kompozycja parent, Set<MyShape> containers, MyLocation point) {
		super(field, index, parent, containers);
		this.point = point;
	}

	@Override
	public String toString() {
		String description = "BlackVertex [index: " + this.index + "][point: " + this.point + "][number of containers: "
				+ containers.size() + "]\n";
		if (this.containers.size() != 0) {
			description += "list of containers:\n";
		}
		for (MyShape container : this.containers) {
			description += container.toString();
		}
		return description;
	}
}
