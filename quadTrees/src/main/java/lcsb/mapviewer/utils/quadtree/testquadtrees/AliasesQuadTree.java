package lcsb.mapviewer.utils.quadtree.testquadtrees;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.containers.MySquare;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;
import lcsb.mapviewer.utils.quadtree.quadtrees.AreaQuadTree;
import lcsb.mapviewer.utils.quadtree.testquadtrees.baskets.AliasBasket;

public class AliasesQuadTree extends AreaQuadTree {

	private List<AliasBasket>	listOfAliases;

	public AliasesQuadTree() {
		super();
		listOfAliases = new ArrayList<AliasBasket>();
	}

	public AliasesQuadTree(IRectangleShape model) {
		super(model);
		listOfAliases = new ArrayList<AliasBasket>();
	}

	public void add(IRectangleShape alias) {
		MySquare square = new MySquare(alias);
		listOfAliases.add(new AliasBasket(alias, square));
		add(square);
	}

	public void remove(IRectangleShape alias) {
		remove(alias, true);
	}

	public void remove(IRectangleShape alias, boolean clean) {
		AliasBasket aliasBasket = null;
		for (AliasBasket temporaryAliasBasket : listOfAliases) {
			if (temporaryAliasBasket.alias.equals(alias)) {
				aliasBasket = temporaryAliasBasket;
				break;
			}
		}
		listOfAliases.remove(aliasBasket);
		remove(aliasBasket.square, clean);
	}

	public Set<IRectangleShape> findAliases(Point point) {
		Set<MyShape> squares = find(new MyLocation(point));
		Set<IRectangleShape> aliases = new HashSet<IRectangleShape>();
		for (AliasBasket temporaryAliasBasket : listOfAliases) {
			for (MyShape temporarySquare : squares) {
				if (temporaryAliasBasket.square.equals(temporarySquare)) {
					aliases.add(temporaryAliasBasket.alias);
				}
			}
		}
		return aliases;
	}
}
