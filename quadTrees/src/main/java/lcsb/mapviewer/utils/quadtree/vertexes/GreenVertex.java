package lcsb.mapviewer.utils.quadtree.vertexes;

import java.util.Set;

import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;

/**
 * Extends abstract class VertexWithShapes.
 * 
 * @see VertexWithShapes
 * @author Artur Laskowski
 * 
 */

public class GreenVertex extends VertexWithShapes {
	/**
	 * Constructor that is used during transforming one vertex into another.
	 * 
	 * @param vertex
	 *          - previous vertex, informations about field, index and parent are
	 *          copied from it.
	 * @param containers
	 *          - list of containers that lays within given vertex
	 */
	public GreenVertex(Vertex vertex, Set<MyShape> containers) {
		super(vertex, containers);
	}

	/**
	 * Constructor where you need to give every information separately. This
	 * should be used only while creating new Vertex.
	 * 
	 * @param field
	 *          - part of map
	 * @param index
	 *          - of vertex within the whole tree structure
	 * @param parent
	 *          - of this vertex
	 * @param containers
	 *          - list of containers that lays within given vertex
	 */
	public GreenVertex(MyRectangle field, int index, Kompozycja parent, Set<MyShape> containers) {
		super(field, index, parent, containers);
	}

	@Override
	public String toString() {
		String description = "GreenVertex [index: " + index + "][number of containers: " + containers.size()
				+ "]\nlist of containers:\n";
		for (MyShape container : this.containers) {
			description += container.toString();
		}
		return description;
	}
}
