package lcsb.mapviewer.utils.quadtree.geometry;

import java.awt.Point;

/**
 * Implements interface Comparable. Held informations about location of point.
 * Also keeps informations about times of occurrence within the tree.
 * 
 * @author Artur Laskowski
 * @see Comparable<MyLocation>
 */
public class MyLocation implements Comparable<MyLocation> {
	public Point	point;
	// TODO Point2D.Double
	public int		times;

	/**
	 * Constructor from given point.
	 * 
	 * @param point
	 */
	public MyLocation(Point point) {
		this.point = point;
		this.times = 1;
	}

	@Override
	public boolean equals(Object second) {
		if (second instanceof MyLocation) {
			if (this.point.x == ((MyLocation) second).point.x && this.point.y == ((MyLocation) second).point.y) {
				return true;
			}
		}
		return false;
	}

	public String toString() {
		String description = "Location[x=" + point.x + ",y=" + point.y + "][times: " + times + "]";
		return description;
	}

	@Override
	public int compareTo(MyLocation location) {
		if (this.point.x < location.point.x) {
			return -1;
		} else if (this.point.x > location.point.x) {
			return 1;
		} else if (this.point.y < location.point.y) {
			return -1;
		} else if (this.point.y > location.point.y) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public int hashCode() {
		int result = "START".hashCode();
		result = result | (Integer.toString(times).hashCode());
		result = result & (Integer.toString(point.x).hashCode());
		result = result ^ (Integer.toString(point.y).hashCode());
		return result;
	}
}
