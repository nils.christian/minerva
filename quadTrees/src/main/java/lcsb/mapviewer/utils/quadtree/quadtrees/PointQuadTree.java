package lcsb.mapviewer.utils.quadtree.quadtrees;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.InvalidQuadTreeStateException;
import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;
import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;
import lcsb.mapviewer.utils.quadtree.vertexes.BlackVertex;
import lcsb.mapviewer.utils.quadtree.vertexes.Kompozycja;
import lcsb.mapviewer.utils.quadtree.vertexes.OrangeVertex;
import lcsb.mapviewer.utils.quadtree.vertexes.WhiteVertex;

import org.apache.log4j.Logger;

/**
 * PointQuadTree that provides adding and removing locations from map.
 * 
 * @author Artur Laskowski
 * 
 */
public class PointQuadTree {
	private static final int		DEFAULT_RECT_SIZE	= 1024;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger				logger						= Logger.getLogger(PointQuadTree.class);

	protected Kompozycja				root;
	protected List<MyLocation>	myLocations;
	protected int								numberOfVertices;

	/**
	 * 
	 * @param model
	 *          - for size of map.
	 */
	protected PointQuadTree(IRectangleShape model) {
		root = new Kompozycja(new WhiteVertex(getWholeField(model), 0, null));
		myLocations = new ArrayList<MyLocation>();
		numberOfVertices = 1;
	}

	protected PointQuadTree() {
		root = new Kompozycja(new WhiteVertex(getWholeField(), 0, null));
		myLocations = new ArrayList<MyLocation>();
		numberOfVertices = 1;
	}

	/**
	 * 
	 * @param myLocation
	 */
	public void add(MyLocation myLocation) {
		if (!exist(myLocation)) {
			this.myLocations.add(myLocation);
			addPoint(myLocation, root);
		} else {
			this.myLocations.get(this.myLocations.indexOf(myLocation)).times++;
		}
	}

	/**
	 * 
	 * @param location
	 */
	public void remove(MyLocation location) {
		remove(location, true);
	}

	/**
	 * 
	 * @param myLocation
	 * @param clean
	 *          <ul>
	 *          <li><code>false</code>: do not use default system of online
	 *          keeping tree in good condition, highly not recommended. Faster,
	 *          but tree could not work without global cleaning before any query.</li>
	 *          <li><code>true</code>: use default system of online keeping tree
	 *          in good condition.</li>
	 *          </ul>
	 */
	public void remove(MyLocation myLocation, boolean clean) {
		// kompozycja is needed to clean tree from proper place.
		Kompozycja kompozycja = removePoint(myLocation);
		if (clean) {
			cleanTree(kompozycja);
		}
	}

	/**
	 * 
	 * @param myLocation
	 * @return
	 */
	protected Kompozycja removePoint(MyLocation myLocation) {
		return removePoint(myLocation, root);
	}

	/**
	 * Checks if hiven location is stored within the tree.
	 * 
	 * @param myLocation
	 * @return
	 */
	protected boolean exist(MyLocation myLocation) {
		return this.myLocations.contains(myLocation);
	}

	/**
	 * Adding location to given place.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 */
	protected void addPoint(MyLocation myLocation, Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof WhiteVertex) {
			addPointToWhite(myLocation, kompozycja);
		} else if (kompozycja.vertex instanceof OrangeVertex) {
			addPointToOrange(myLocation, kompozycja);
		} else if (kompozycja.vertex instanceof BlackVertex) {
			addPointToBlack(myLocation, kompozycja);
		} else {
			throw new InvalidQuadTreeStateException("In PointTree " + kompozycja + " could not be recognized as valid Vertex");
		}
	}

	/**
	 * Adding point to WhiteVertex changes it to Black one.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 */
	protected void addPointToWhite(MyLocation myLocation, Kompozycja kompozycja) {
		kompozycja.vertex = new BlackVertex(kompozycja.vertex, new HashSet<MyShape>(), myLocation);
	}

	/**
	 * Adding point to OrangeVertex mean adding it to one of its sons.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 */
	protected void addPointToOrange(MyLocation myLocation, Kompozycja kompozycja) {
		for (Kompozycja son : ((OrangeVertex) kompozycja.vertex).sons) {
			if (son.vertex.field.contains(myLocation)) {
				addPoint(myLocation, son);
				return;
			}
		}
		throw new InvalidQuadTreeStateException("OrangeVertex contains point, but none of his sons does");
	}

	/**
	 * Adding point to BlackVertex mean splitting it into four sons. Changing it
	 * into Orange one and puts this point to proper sons.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 */
	protected void addPointToBlack(MyLocation myLocation, Kompozycja kompozycja) {
		List<Kompozycja> sons = new ArrayList<Kompozycja>();
		MyRectangle[] rectangles = kompozycja.vertex.field.split();
		for (MyRectangle rectangle : rectangles) {
			if (rectangle.contains(((BlackVertex) kompozycja.vertex).point)) {
				sons.add(new Kompozycja(new BlackVertex(rectangle, numberOfVertices++, kompozycja, new HashSet<MyShape>(), ((BlackVertex) kompozycja.vertex).point)));
			} else {
				sons.add(new Kompozycja(new WhiteVertex(rectangle, numberOfVertices++, kompozycja)));
			}
		}
		kompozycja.vertex = new OrangeVertex(kompozycja.vertex, sons);
		// If point are in the same field after splitting function also need to
		// split smaller field.
		addPoint(myLocation, kompozycja);
	}

	/**
	 * 
	 * @param model
	 * @return size of map within model.
	 */
	private MyRectangle getWholeField(IRectangleShape model) {
		// ////////////////
		int height, width = height = Math.max((int) (double) model.getHeight(), (int) (double) model.getWidth());
		int x = 0;
		int y = 0;
		// ////////////////
		Point tmp = new Point(x, y);
		return new MyRectangle(tmp, width, height);
	}

	/**
	 * Setting predefined sizes as {@link #DEFAULT_RECT_SIZE} for both height and
	 * width.
	 * 
	 * @return
	 */
	private MyRectangle getWholeField() {
		// ////////////////
		int height = DEFAULT_RECT_SIZE;
		int width = DEFAULT_RECT_SIZE;
		int x = 0;
		int y = 0;
		// ////////////////
		Point tmp = new Point(x, y);
		return new MyRectangle(tmp, width, height);
	}

	/**
	 * 
	 * @param myLocation
	 * @param kompozycja
	 * @return
	 */
	protected Kompozycja removePoint(MyLocation myLocation, Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof OrangeVertex) {
			return removePointFromOrange(myLocation, kompozycja);
		} else if (kompozycja.vertex instanceof BlackVertex) {
			return removePointFromBlack(myLocation, kompozycja);
		} else {
			throw new InvalidQuadTreeStateException("Could not remove point if it wasne in map already");
		}
	}

	/**
	 * Removing point from orange means to remove point from one of its sons.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 * @return
	 */
	protected Kompozycja removePointFromOrange(MyLocation myLocation, Kompozycja kompozycja) {
		for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
			if (temporaryKompozycja.vertex.field.contains(myLocation)) {
				return removePoint(myLocation, temporaryKompozycja);
			}
		}
		throw new InvalidQuadTreeStateException("OrangeVertex contains point, but none of his sons does");
	}

	/**
	 * Remove point from black means to chang it to WhiteVertex.
	 * 
	 * @param myLocation
	 * @param kompozycja
	 * @return
	 */
	protected Kompozycja removePointFromBlack(MyLocation myLocation, Kompozycja kompozycja) {
		if ((myLocations.get(myLocations.indexOf(((BlackVertex) kompozycja.vertex).point)).times == 1)) {
			myLocations.remove(myLocations.indexOf(((BlackVertex) kompozycja.vertex).point));
			kompozycja.vertex = new WhiteVertex(kompozycja.vertex);
		} else {
			myLocations.get(myLocations.indexOf(((BlackVertex) kompozycja.vertex).point)).times--;
		}
		return kompozycja;
	}

	protected void cleanTree(Kompozycja kompozycja) {
		clearTree(kompozycja);
	}

	/**
	 * Function that cleans tree globally.
	 */
	public void cleanTree() {
		Queue<Kompozycja> queue = new LinkedList<Kompozycja>();
		prepareQueue(root, queue);
		cleanTree(queue);
	}

	/**
	 * Function that cleans tree online. After removing some point it checks if
	 * tree has correct structure. If structure was incorrect that repairs it.
	 * Starts from 'kompozycja' where it was removed and goes up with tree as long
	 * as there is something to repair.
	 * 
	 * @param kompozycja
	 */
	protected void clearTree(Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof OrangeVertex) {
			if (!((OrangeVertex) kompozycja.vertex).hasOrange()) {
				if (((OrangeVertex) kompozycja.vertex).countBlack() == 1) {
					MyLocation myLocation = null;
					for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
						if (temporaryKompozycja.vertex instanceof BlackVertex) {
							myLocation = ((BlackVertex) temporaryKompozycja.vertex).point;
						}
					}
					kompozycja.vertex = new BlackVertex(kompozycja.vertex, new HashSet<MyShape>(), myLocation);
					if (kompozycja.vertex.parent != null) {
						clearTree(kompozycja.vertex.parent);
					}
				}
			}
		} else if (kompozycja.vertex.parent != null) {
			clearTree(kompozycja.vertex.parent);
		}
	}

	/**
	 * Creating queue with leafs at top and root at the end of it.
	 * 
	 * @param kompozycja
	 * @param queue
	 */
	private void prepareQueue(Kompozycja kompozycja, Queue<Kompozycja> queue) {
		if (kompozycja.vertex instanceof OrangeVertex) {
			for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
				prepareQueue(temporaryKompozycja, queue);
			}
		}
		queue.add(kompozycja);
	}

	/**
	 * This function checks if structure of tree is correct if not then correcting
	 * it. Starting from leafs.
	 * 
	 * @param queue
	 */
	protected void cleanTree(Queue<Kompozycja> queue) {
		while (queue.isEmpty() == false) {
			Kompozycja kompozycja = queue.poll();
			if (kompozycja.vertex instanceof OrangeVertex) {
				if (!((OrangeVertex) kompozycja.vertex).hasOrange()) {
					if (((OrangeVertex) kompozycja.vertex).countBlack() == 1) {
						MyLocation myLocation = null;
						for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
							if (temporaryKompozycja.vertex instanceof BlackVertex) {
								myLocation = ((BlackVertex) temporaryKompozycja.vertex).point;
							}
						}
						kompozycja.vertex = new BlackVertex(kompozycja.vertex, new HashSet<MyShape>(), myLocation);
					} else if (((OrangeVertex) kompozycja.vertex).countBlack() == 0) {
						kompozycja.vertex = new WhiteVertex(kompozycja.vertex);
					}
				}
			}
		}
	}

	public String toString() {
		String description = "\nPointTree: \n";
		description += root.toString();
		return description;
	}
}
