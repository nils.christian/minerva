package lcsb.mapviewer.utils.quadtree.quadtrees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.utils.quadtree.IRectangleShape;
import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.geometry.MyLocation;
import lcsb.mapviewer.utils.quadtree.vertexes.Kompozycja;
import lcsb.mapviewer.utils.quadtree.vertexes.OrangeVertex;
import lcsb.mapviewer.utils.quadtree.vertexes.VertexWithShapes;

import org.apache.log4j.Logger;

public class AreaQuadTree extends SegmentQuadTree {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger		logger	= Logger.getLogger(PointQuadTree.class);

	protected List<MyShape>	listOfRectangles;

	protected AreaQuadTree() {
		super();
		listOfRectangles = new ArrayList<MyShape>();
	}

	/**
	 * Takes sizes from model.
	 * 
	 * @param model
	 */
	protected AreaQuadTree(IRectangleShape model) {
		super(model);
		listOfRectangles = new ArrayList<MyShape>();
	}

	/**
	 * 
	 * @param square
	 */
	protected void add(MyShape square) {
		for (MyLocation location : square.getPoints()) {
			add(location);
		}
		listOfRectangles.add(square);
		addContainer(square, root);
	}

	/**
	 * 
	 * @param square
	 */
	public void remove(MyShape square) {
		remove(square, true);
	}

	/**
	 * Removes shape from the quad tree.
	 * 
	 * @param square
	 *          object to remove
	 * @param clean
	 *          should the tree be restored to usable state after removing or not.
	 *          If tree is not cleaned after removing then querying won't work (it
	 *          should be done manually before querying).
	 */
	protected void remove(MyShape square, boolean clean) {
		listOfRectangles.remove(square);
		removeContainer(square);
		for (MyLocation location : square.getPoints()) {
			remove(location, clean);
		}
	}

	/**
	 * 
	 * @param location
	 * @return
	 */
	protected Set<MyShape> find(MyLocation location) {
		return find(location, root);
	}

	/**
	 * If given point points to some field then it is returned. Otherwise null.
	 * 
	 * @param location
	 * @param kompozycja
	 * @return
	 */
	private Set<MyShape> find(MyLocation location, Kompozycja kompozycja) {
		if (kompozycja.vertex instanceof OrangeVertex) {
			for (Kompozycja temporaryKompozycja : ((OrangeVertex) kompozycja.vertex).sons) {
				if (temporaryKompozycja.vertex.field.contains(location)) {
					return find(location, temporaryKompozycja);
				}
			}
		} else if (kompozycja.vertex instanceof VertexWithShapes) {
			Set<MyShape> result = new HashSet<MyShape>();
			for (MyShape container : ((VertexWithShapes) kompozycja.vertex).containers) {
				if (((MyShape) container).contains(location)) {
					result.add(container);
				}
			}
			return result;
		}
		return null;
	}

	@Override
	public String toString() {
		String description = "\nSegmentTree: \n";
		description += root.toString();
		return description;
	}
}