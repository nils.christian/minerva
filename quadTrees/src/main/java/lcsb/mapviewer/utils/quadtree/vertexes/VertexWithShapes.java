package lcsb.mapviewer.utils.quadtree.vertexes;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.utils.quadtree.containers.Edge;
import lcsb.mapviewer.utils.quadtree.containers.MyShape;
import lcsb.mapviewer.utils.quadtree.containers.MySquare;
import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;

/**
 * Abstract class. Extends abstract class Vertex. This class gives additionally
 * possibility of containing informations about containers. Should contains at
 * least one container. Without this container one should use WhiteVertex.
 * 
 * @see WhiteVertex
 * @see Vertex
 * 
 * @author Artur Laskowski
 * 
 */
public abstract class VertexWithShapes extends Vertex {
	public Set<MyShape>	containers;

	/**
	 * Constructor that is used during transforming one vertex into another.
	 * 
	 * @param vertex
	 *          - previous vertex, informations about field, index and parent are
	 *          copied from it.
	 * @param containers
	 *          - list of containers that lays within given vertex
	 */
	public VertexWithShapes(Vertex vertex, Set<MyShape> containers) {
		super(vertex);
		this.containers = containers;
	}

	/**
	 * Constructor where you need to give every information separately. This
	 * should be used only while creating new Vertex.
	 * 
	 * @param field
	 *          - part of map
	 * @param index
	 *          - of vertex within the whole tree structure
	 * @param parent
	 *          - of this vertex
	 * @param containers
	 *          - list of containers that lays within given vertex
	 */
	public VertexWithShapes(MyRectangle field, int index, Kompozycja parent, Set<MyShape> containers) {
		super(field, index, parent);
		this.containers = containers;
	}

	/**
	 * 
	 * @param rectangle
	 * @return set of containers that should be considered within given
	 *         'rectangle'. They are inside of it.
	 */
	public Set<MyShape> getContainers(MyRectangle rectangle) {
		Set<MyShape> result = new HashSet<MyShape>();
		for (MyShape container : containers) {
			if (container instanceof Edge) {
				if (rectangle.contains((Edge) container)) {
					result.add((Edge) container);
				}
			} else if (container instanceof MySquare) {
				if (rectangle.contains((MySquare) container)) {
					result.add((MySquare) container);
				}
			}
		}
		return result;
	}
}
