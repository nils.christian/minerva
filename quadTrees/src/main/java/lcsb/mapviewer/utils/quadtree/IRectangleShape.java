package lcsb.mapviewer.utils.quadtree;

public interface IRectangleShape {
	double getX();

	double getY();

	double getWidth();

	double getHeight();
}
