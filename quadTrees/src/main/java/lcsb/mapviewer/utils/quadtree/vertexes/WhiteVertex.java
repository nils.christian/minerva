package lcsb.mapviewer.utils.quadtree.vertexes;

import lcsb.mapviewer.utils.quadtree.geometry.MyRectangle;

/**
 * This class extends abstract class Vertex. WhiteVertex is leaf of the tree.
 * This one could not contains any informations about point or containers.
 * 
 * @author Artur Laskowski
 * @see Vertex
 * 
 */
public class WhiteVertex extends Vertex {
	/**
	 * Constructor where you need to give every information separately. This
	 * should be used only while creating new Vertex.
	 * 
	 * @param field
	 *          - part of map
	 * @param index
	 *          - of vertex within the whole tree structure
	 * @param parent
	 *          - of this vertex
	 */
	public WhiteVertex(MyRectangle field, int index, Kompozycja parent) {
		super(field, index, parent);
	}

	/**
	 * Constructor that is used during transforming one vertex into another.
	 * 
	 * @param vertex
	 *          - previous vertex, informations about field, index and parent are
	 *          copied from it.
	 */
	public WhiteVertex(Vertex vertex) {
		super(vertex);
	}

	@Override
	public String toString() {
		String description = "WhiteVertex [index: " + index + "]\n";
		return description;
	}
}
