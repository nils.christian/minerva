package lcsb.mapviewer.utils.quadtree.vertexes;

/**
 * This composition keeps Vertex, so one could override it and whole structure
 * of tree could still be correct.
 * 
 * @author Artur Laskowski
 * @see Vertex
 */
public class Kompozycja {
	public Vertex	vertex;

	/**
	 * Constructor with vertex that should be held within given composition.
	 * 
	 * @param vertex
	 */
	public Kompozycja(Vertex vertex) {
		this.vertex = vertex;
	}

	public String toString() {
		return vertex.toString();
	}
}
