package lcsb.mapviewer.utils.quadtree.testquadtrees.baskets;

import lcsb.mapviewer.utils.quadtree.ILineShape;
import lcsb.mapviewer.utils.quadtree.containers.MyPolyline;

public class ReactionBasket {

	public ILineShape reaction;
	public MyPolyline myPolyline;

	public ReactionBasket(ILineShape reaction, MyPolyline myPolyline) {
		this.reaction = reaction;
		this.myPolyline = myPolyline;
	}
}
