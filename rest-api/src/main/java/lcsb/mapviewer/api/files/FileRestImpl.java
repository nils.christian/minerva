package lcsb.mapviewer.api.files;

import java.util.TreeMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.QueryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.ILayoutService;

@Transactional(value = "txManager")
public class FileRestImpl extends BaseRestImpl {

  @Autowired
  private ILayoutService overlayService;

  @Autowired
  private UploadedFileEntryDao uploadedFileEntryDao;

  public Map<String, Object> createFile(String token, String filename, String length) throws SecurityException {
    User user = getUserService().getUserByToken(token);
    if (!getUserService().userHasPrivilege(user, PrivilegeType.ADD_MAP)
        && overlayService.getAvailableCustomLayoutsNumber(user) == 0) {
      throw new SecurityException("Access denied");
    }
    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName(filename);
    entry.setFileContent(new byte[] {});
    entry.setLength(Long.valueOf(length));
    entry.setOwner(user);
    uploadedFileEntryDao.add(entry);
    try {
      return getFile(token, entry.getId() + "");
    } catch (ObjectNotFoundException e) {
      throw new InvalidStateException(e);
    }
  }

  public Map<String, Object> getFile(String token, String id) throws SecurityException, ObjectNotFoundException {
    User user = getUserService().getUserByToken(token);
    int fileId = Integer.valueOf(id);
    UploadedFileEntry fileEntry = uploadedFileEntryDao.getById(fileId);
    if (fileEntry == null) {
      throw new ObjectNotFoundException("Object not found");
    }
    if (fileEntry.getOwner() == null) {
      throw new SecurityException("Access denied");
    }
    if (!fileEntry.getOwner().getLogin().equals(user.getLogin())) {
      throw new SecurityException("Access denied");
    }
    return serializeEntry(fileEntry);
  }

  private Map<String, Object> serializeEntry(UploadedFileEntry fileEntry) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", fileEntry.getId());
    result.put("filename", fileEntry.getOriginalFileName());
    result.put("length", fileEntry.getLength());
    result.put("owner", fileEntry.getOwner().getLogin());
    result.put("uploadedDataLength", fileEntry.getFileContent().length);
    return result;
  }

  public Map<String, Object> uploadContent(String token, String id, byte[] data) throws SecurityException, ObjectNotFoundException {
    User user = getUserService().getUserByToken(token);
    int fileId = Integer.valueOf(id);
    UploadedFileEntry fileEntry = uploadedFileEntryDao.getById(fileId);
    if (fileEntry == null) {
      throw new ObjectNotFoundException("Object not found");
    }
    if (fileEntry.getOwner() == null) {
      throw new SecurityException("Access denied");
    }
    if (!fileEntry.getOwner().getLogin().equals(user.getLogin())) {
      throw new SecurityException("Access denied");
    }
    long missingByteLength = fileEntry.getLength() - fileEntry.getFileContent().length;
    if (data.length > missingByteLength) {
      throw new QueryException(
          "Too many bytes sent. There are " + missingByteLength + " missing bytes, but " + data.length + " sent.");
    }
    byte[] newConent = ArrayUtils.addAll(fileEntry.getFileContent(), data);
    fileEntry.setFileContent(newConent);
    uploadedFileEntryDao.update(fileEntry);
    return serializeEntry(fileEntry);
  }

  public UploadedFileEntryDao getUploadedFileEntryDao() {
    return uploadedFileEntryDao;
  }

  public void setUploadedFileEntryDao(UploadedFileEntryDao uploadedFileEntryDao) {
    this.uploadedFileEntryDao = uploadedFileEntryDao;
  }

}
