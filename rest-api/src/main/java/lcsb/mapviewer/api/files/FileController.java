package lcsb.mapviewer.api.files;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class FileController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(FileController.class);

  @Autowired
  private FileRestImpl fileRest;

  @RequestMapping(value = "/files/", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> createFile(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @RequestParam(value = "filename") String filename, //
      @RequestParam(value = "length") String length //
  ) throws SecurityException {
    return fileRest.createFile(token, filename, length);
  }

  @RequestMapping(value = "/files/{id}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getFile(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "id") String id //
  ) throws SecurityException, ObjectNotFoundException {
    return fileRest.getFile(token, id);
  }

  @RequestMapping(value = "/files/{id}:uploadContent", method = { RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> uploadContent(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "id") String id, //
      @RequestBody byte[] data) throws SecurityException, ObjectNotFoundException {
    return fileRest.uploadContent(token, id, data);
  }

}