package lcsb.mapviewer.api.mesh;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.common.Configuration;

@RestController
public class MeshController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(MeshController.class);

  @Autowired
  private MeshRestImpl userRest;

  @RequestMapping(value = "/mesh/{id:.+}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getMesh(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "id") String id //
  ) throws Exception {
    return userRest.getMesh(token, id);
  }

  public MeshRestImpl getUserRest() {
    return userRest;
  }

  public void setUserRest(MeshRestImpl userRest) {
    this.userRest = userRest;
  }

}