package lcsb.mapviewer.api.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.utils.data.ColorSchemaType;

@Transactional(value = "txManager")
public class ConfigurationRestImpl extends BaseRestImpl {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ConfigurationRestImpl.class);

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private ModelAnnotator modelAnnotator;

  public List<Map<String, Object>> getAllValues(String token) throws SecurityException {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ConfigurationOption option : configurationService
        .getAllValues(getUserService().userHasPrivilege(token, PrivilegeType.CONFIGURATION_MANAGE))) {
      result.add(optionToMap(option));
    }
    return result;
  }

  private Map<String, Object> optionToMap(ConfigurationOption option) {
    Map<String, Object> result = new TreeMap<>();
    result.put("idObject", option.getId());
    result.put("type", option.getType());
    result.put("valueType", option.getType().getEditType());
    result.put("commonName", option.getType().getCommonName());
    result.put("value", option.getValue());
    if (option.getType().getGroup() != null) {
      result.put("group", option.getType().getGroup().getCommonName());
    }
    return result;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  public List<Map<String, Object>> getImageFormats(String token) throws SecurityException {
    verifyToken(token);

    List<Map<String, Object>> result = new ArrayList<>();
    ImageGenerators imageGenerators = new ImageGenerators();
    List<Pair<String, Class<? extends AbstractImageGenerator>>> imageGeneratorList = imageGenerators
        .getAvailableImageGenerators();

    for (Pair<String, Class<? extends AbstractImageGenerator>> element : imageGeneratorList) {
      Map<String, Object> row = new TreeMap<>();
      row.put("name", element.getLeft());
      row.put("handler", element.getRight().getCanonicalName());
      row.put("extension", imageGenerators.getExtension(element.getRight()));
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getModelFormats(String token) throws SecurityException {
    verifyToken(token);
    List<IConverter> converters = getModelConverters();

    List<Map<String, Object>> result = new ArrayList<>();

    for (IConverter converter : converters) {
      Map<String, Object> row = new TreeMap<>();
      row.put("name", converter.getCommonName());
      row.put("handler", converter.getClass().getCanonicalName());
      row.put("extension", converter.getFileExtension());
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getOverlayTypes(String token) throws SecurityException {
    verifyToken(token);
    List<Map<String, Object>> result = new ArrayList<>();
    for (ColorSchemaType type : ColorSchemaType.values()) {
      Map<String, Object> map = new TreeMap<>();
      map.put("name", type.name());
      result.add(map);
    }
    return result;
  }

  public Set<Map<String, String>> getElementTypes(String token) throws SecurityException {
    verifyToken(token);

    return getClassStringTypesList(Element.class);
  }

  private Set<Map<String, String>> getClassStringTypesList(Class<?> elementClass) {
    Set<Map<String, String>> result = new HashSet<>();
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<>();
    queue.add(top);
    while (!queue.isEmpty()) {
      ClassTreeNode clazz = queue.poll();
      for (ClassTreeNode child : clazz.getChildren()) {
        queue.add(child);
      }
      if (elementClass.isAssignableFrom(clazz.getClazz())) {
        Map<String, String> row = new TreeMap<>();
        row.put("className", clazz.getClazz().getName());
        row.put("name", clazz.getCommonName());
        if (clazz.getParent() == null) {
          row.put("parentClass", null);
        } else {
          row.put("parentClass", clazz.getParent().getClazz().getName());
        }
        result.add(row);
      }
    }
    return result;
  }

  public Set<Map<String, String>> getReactionTypes(String token) throws SecurityException {
    verifyToken(token);

    return getClassStringTypesList(Reaction.class);
  }

  public Map<String, Object> getMiriamTypes(String id) {
    Map<String, Object> result = new TreeMap<>();
    for (MiriamType type : MiriamType.values()) {
      result.put(type.name(), createMiriamTypeResponse(type));
    }
    return result;
  }

  private Map<String, Object> createMiriamTypeResponse(MiriamType type) {
    Map<String, Object> result = new TreeMap<>();
    result.put("commonName", type.getCommonName());
    result.put("homepage", type.getDbHomepage());
    result.put("registryIdentifier", type.getRegistryIdentifier());
    result.put("uris", type.getUris());

    return result;
  }

  public Object getModificationStateTypes(String token) {
    Map<String, Object> result = new TreeMap<>();
    for (ModificationState type : ModificationState.values()) {
      result.put(type.name(), createModificationStateResponse(type));
    }
    return result;
  }

  private Map<String, Object> createModificationStateResponse(ModificationState type) {
    Map<String, Object> result = new TreeMap<>();
    result.put("commonName", type.getFullName());
    result.put("abbreviation", type.getAbbreviation());
    return result;
  }

  public Map<String, Object> getPrivilegeTypes(String id) {
    Map<String, Object> result = new TreeMap<>();
    for (PrivilegeType type : PrivilegeType.values()) {
      result.put(type.name(), createPrivilegeTypeResponse(type));
    }
    return result;
  }

  private Map<String, Object> createPrivilegeTypeResponse(PrivilegeType type) {
    Map<String, Object> result = new TreeMap<>();
    result.put("commonName", type.getCommonName());
    if (type.getPrivilegeObjectType() != null) {
      result.put("objectType", type.getPrivilegeObjectType().getSimpleName());
    } else {
      result.put("objectType", null);
    }
    if (type.isNumeric()) {
      result.put("valueType", "int");
    } else {
      result.put("valueType", "boolean");
    }
    return result;
  }

  public List<Map<String, Object>> getAnnotators(String token) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ElementAnnotator annotator : modelAnnotator.getAvailableAnnotators()) {
      result.add(prepareAnnotator(annotator));
    }
    return result;
  }

  private Map<String, Object> prepareAnnotator(ElementAnnotator annotator) {
    Map<String, Object> result = new TreeMap<>();
    result.put("className", annotator.getClass().getName());
    result.put("name", annotator.getCommonName());
    result.put("description", annotator.getDescription());
    result.put("url", annotator.getUrl());
    result.put("elementClassNames", annotator.getValidClasses());
    result.put("parametersDefinitions", annotator.getParametersDefinitions());
    return result;
  }

  public List<Map<String, Object>> getMapTypes(String token) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (SubmodelType type : SubmodelType.values()) {
      Map<String, Object> row = new TreeMap<>();
      row.put("id", type.name());
      row.put("name", type.getCommonName());
      result.add(row);
    }
    return result;
  }

  public List<Map<String, Object>> getMapCanvasTypes(String token) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (MapCanvasType type : MapCanvasType.values()) {
      Map<String, Object> row = new TreeMap<>();
      row.put("id", type.name());
      row.put("name", type.getCommonName());
      result.add(row);
    }
    return result;
  }

  public Map<String, Object> updateOption(String token, String option, Map<String, Object> data)
      throws SecurityException {
    if (!getUserService().userHasPrivilege(token, PrivilegeType.CONFIGURATION_MANAGE)) {
      throw new SecurityException("Acces denied");
    }
    ConfigurationElementType type = ConfigurationElementType.valueOf(option);
    String value = (String) data.get("value");
    configurationService.setConfigurationValue(type, value);
    return optionToMap(configurationService.getValue(type));
  }

  public List<Map<String, Object>> getPlugins(String token, String rootPath) {
    String path = rootPath + "/resources/js/plugins/";
    File folder = new File(path);
    List<Map<String, Object>> result = new ArrayList<>();
    if (folder.exists()) {
      File[] listOfFiles = folder.listFiles();
      Arrays.sort(listOfFiles);

      for (int i = 0; i < listOfFiles.length; i++) {
        if (listOfFiles[i].isFile()) {
          Map<String, Object> row = new TreeMap<>();
          row.put("url", "resources/js/plugins/" + listOfFiles[i].getName());
          row.put("load-on-start", false);
          result.add(row);
        }
      }

    }
    return result;
  }

  public List<Map<String, Object>> getUnitTypes(String token) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (SbmlUnitType type : SbmlUnitType.values()) {
      Map<String, Object> row = new TreeMap<>();
      row.put("id", type.name());
      row.put("name", type.getCommonName());
      result.add(row);
    }
    return result;
  }

}
