package lcsb.mapviewer.api.configuration;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@RestController
public class ConfigurationController extends BaseController {
  @Autowired
  private ConfigurationRestImpl configurationController;

  @Autowired
  private IConfigurationService configurationService;

  /**
   * Context used to determine deployment path.
   */
  @Autowired
  private ServletContext context;

  @RequestMapping(value = "/configuration/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getConfiguration(@CookieValue(value = Configuration.AUTH_TOKEN) String token)
      throws SecurityException {
    Map<String, Object> result = new TreeMap<>();
    result.put("options", configurationController.getAllValues(token));
    result.put("imageFormats", configurationController.getImageFormats(token));
    result.put("modelFormats", configurationController.getModelFormats(token));
    result.put("overlayTypes", configurationController.getOverlayTypes(token));
    result.put("elementTypes", configurationController.getElementTypes(token));
    result.put("reactionTypes", configurationController.getReactionTypes(token));
    result.put("miriamTypes", configurationController.getMiriamTypes(token));
    result.put("mapTypes", configurationController.getMapTypes(token));
    result.put("mapCanvasTypes", configurationController.getMapCanvasTypes(token));
    result.put("unitTypes", configurationController.getUnitTypes(token));
    result.put("modificationStateTypes", configurationController.getModificationStateTypes(token));
    result.put("privilegeTypes", configurationController.getPrivilegeTypes(token));
    result.put("version", configurationService.getSystemSvnVersion(context.getRealPath("/")));
    result.put("buildDate", configurationService.getSystemBuild(context.getRealPath("/")));
    result.put("gitHash", configurationService.getSystemGitVersion(context.getRealPath("/")));
    result.put("annotators", configurationController.getAnnotators(token));
    result.put("plugins", configurationController.getPlugins(token, context.getRealPath("/")));
    return result;
  }

  @RequestMapping(value = "/configuration/options/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getOptions(@CookieValue(value = Configuration.AUTH_TOKEN) String token)
      throws SecurityException {
    return configurationController.getAllValues(token);
  }

  @RequestMapping(value = "/configuration/options/{option}", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getOption( //
      @RequestBody String body, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "option") String option //
  ) throws SecurityException, JsonParseException, JsonMappingException, IOException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "option");
    return configurationController.updateOption(token, option, data);
  }

  /**
   * @return the configurationController
   * @see #configurationController
   */
  public ConfigurationRestImpl getConfigurationController() {
    return configurationController;
  }

  /**
   * @param configurationController
   *          the configurationController to set
   * @see #configurationController
   */
  public void setConfigurationController(ConfigurationRestImpl configurationController) {
    this.configurationController = configurationController;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService
   *          the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

}