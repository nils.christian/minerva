package lcsb.mapviewer.api;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.search.ElementMatcher;

@Transactional(value = "txManager")
public abstract class BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(BaseRestImpl.class);

  @Autowired
  private IModelService modelService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IUserService userService;

  @Autowired
  private MiriamConnector miriamConnector;

  @Autowired
  private PubmedParser pubmedParser;

  private ElementMatcher elementMatcher = new ElementMatcher();

  private Transformer mathMlTransformer;

  protected Map<String, Object> okStatus() {
    Map<String, Object> result = new TreeMap<>();
    return result;
  }

  protected Map<String, Object> createMinifiedSearchResult(BioEntity object) {
    Map<String, Object> result = new TreeMap<>();
    if (object instanceof Element) {
      result.put("type", ElementIdentifierType.ALIAS);
      Element element = (Element) object;
      result.put("id", element.getId());
      result.put("modelId", element.getModel().getId());
    } else if (object instanceof Reaction) {
      result.put("type", ElementIdentifierType.REACTION);
      Reaction element = (Reaction) object;
      result.put("id", element.getId());
      result.put("modelId", element.getModel().getId());

    } else {
      throw new InvalidStateException("Unknown type of result: " + object.getClass());
    }
    return result;
  };

  protected Map<String, Object> createAnnotation(MiriamData annotation) {
    if (annotation != null && annotation.getDataType() != null) {
      Map<String, Object> result = new TreeMap<>();
      if (annotation.getDataType().getUris().size() > 0) {
        try {
          result.put("link", miriamConnector.getUrlString(annotation));
        } catch (Exception e) {
          logger.error("Problem with miriam: " + annotation, e);
        }
      }
      if (MiriamType.PUBMED.equals(annotation.getDataType())) {
        try {
          Article article = pubmedParser.getPubmedArticleById(Integer.valueOf(annotation.getResource()));
          result.put("article", article);
        } catch (PubmedSearchException e) {
          logger.error("Problem with accessing info about pubmed", e);
        }
      }
      result.put("type", annotation.getDataType().name());
      result.put("resource", annotation.getResource());
      result.put("id", annotation.getId());

      if (annotation.getAnnotator() != null) {
        try {
          result.put("annotatorClassName", annotation.getAnnotator().getName());
          result.put("descriptionByType", ((ElementAnnotator) annotation.getAnnotator().getConstructor().newInstance())
              .getDescription(annotation.getDataType()));
          result.put("descriptionByTypeRelation",
              ((ElementAnnotator) annotation.getAnnotator().getConstructor().newInstance())
                  .getDescription(annotation.getDataType(), annotation.getRelationType()));

        } catch (Exception e) {
          logger.error("Problem with retrieving description from annotator", e);
          result.put("annotatorClassName", "");
          result.put("descriptionByType", "");
          result.put("descriptionByTypeRelation", "");
        }
      } else {
        result.put("annotatorClassName", "");
        result.put("descriptionByType", "");
        result.put("descriptionByTypeRelation", "");
      }

      return result;
    } else {
      throw new InvalidArgumentException("invalid miriam data: " + annotation);
    }
  };

  protected Map<String, Object> createAnnotation(Article article) {
    Map<String, Object> result = new TreeMap<>();
    if (article != null) {
      MiriamType type = MiriamType.PUBMED;
      result.put("link", miriamConnector.getUrlString(new MiriamData(MiriamType.PUBMED, article.getId())));
      result.put("article", article);
      result.put("type", type);
      result.put("resource", article.getId());
    }
    return result;
  }

  protected List<Map<String, Object>> createAnnotations(Collection<?> references) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (Object miriamData : references) {
      if (miriamData instanceof MiriamData) {
        result.add(createAnnotation((MiriamData) miriamData));
      } else if (miriamData instanceof Article) {
        result.add(createAnnotation((Article) miriamData));
      } else {
        throw new InvalidArgumentException();
      }
    }
    return result;
  }

  protected List<Model> getModels(String projectId, String modelId, String token)
      throws SecurityException, ObjectNotFoundException {
    Model model = modelService.getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    List<Model> models = new ArrayList<>();

    if (!modelId.equals("*")) {
      for (String str : modelId.split(",")) {
        models.add(model.getSubmodelById(Integer.valueOf(str)));
      }
    } else {
      models.addAll(model.getSubmodels());
      models.add(model);
    }
    models.sort(Model.ID_COMPARATOR);
    return models;
  }

  /**
   * @return the modelService
   * @see #modelService
   */
  public IModelService getModelService() {
    return modelService;
  }

  /**
   * @param modelService
   *          the modelService to set
   * @see #modelService
   */
  public void setModelService(IModelService modelService) {
    this.modelService = modelService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  protected List<Map<String, Object>> prepareTargets(Collection<Target> targets, List<Model> models) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (Target target : targets) {
      result.add(prepareTarget(target, models));
    }
    result.sort(new Comparator<Map<String, Object>>() {

      @Override
      public int compare(Map<String, Object> o1, Map<String, Object> o2) {
        List<?> targetedObjects1 = (List<?>) o1.get("targetElements");
        List<?> targetedObjects2 = (List<?>) o2.get("targetElements");
        Integer size1 = 0;
        Integer size2 = 0;
        if (targetedObjects1 != null) {
          size1 = targetedObjects1.size();
        }
        if (targetedObjects2 != null) {
          size2 = targetedObjects2.size();
        }
        if (size1 == size2) {
          String name1 = (String) o1.get("name");
          String name2 = (String) o2.get("name");
          return new StringComparator().compare(name1, name2);
        }
        return -size1.compareTo(size2);
      }
    });
    return result;
  }

  protected Map<String, Object> prepareTarget(Target target, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();
    result.put("name", target.getName());
    result.put("references", createAnnotations(target.getReferences()));
    result.put("targetParticipants", createAnnotations(target.getGenes()));

    List<Map<String, Object>> targetedObjects = new ArrayList<>();
    List<BioEntity> bioEntities = new ArrayList<>();
    for (Model model : models) {
      bioEntities.addAll(model.getBioEntities());
    }
    bioEntities.sort(BioEntity.ID_COMPARATOR);
    for (BioEntity bioEntity : bioEntities) {
      if (elementMatcher.elementMatch(target, bioEntity)) {
        Map<String, Object> elementMapping = new TreeMap<>();
        elementMapping.put("id", bioEntity.getId());
        elementMapping.put("type", getType(bioEntity));
        elementMapping.put("modelId", bioEntity.getModel().getId());
        targetedObjects.add(elementMapping);
      }
    }

    result.put("targetElements", targetedObjects);

    return result;
  }

  private String getType(BioEntity object) {
    if (object instanceof Reaction) {
      return ElementIdentifierType.REACTION.getJsName();
    } else if (object instanceof Element) {
      return ElementIdentifierType.ALIAS.getJsName();
    } else {
      throw new InvalidArgumentException("Unknown type of element " + object.getClass());
    }
  }

  /**
   * @return the projectService
   * @see #projectService
   */
  public IProjectService getProjectService() {
    return projectService;
  }

  /**
   * @param projectService
   *          the projectService to set
   * @see #projectService
   */
  public void setProjectService(IProjectService projectService) {
    this.projectService = projectService;
  }

  protected IConverter getModelParser(String handlerClass) throws QueryException {
    for (IConverter converter : getModelConverters()) {
      if (converter.getClass().getCanonicalName().equals(handlerClass)) {
        return converter;
      }
    }
    throw new QueryException("Unknown handlerClass: " + handlerClass);
  }

  protected List<IConverter> getModelConverters() {
    List<IConverter> result = new ArrayList<>();
    result.add(new CellDesignerXmlParser());
    result.add(new SbgnmlXmlConverter());
    result.add(new SbmlParser());
    return result;
  }

  protected String getFirstValue(List<Object> list) {
    if (list == null) {
      return null;
    }
    if (list.size() > 0) {
      return (String) list.get(0);
    }
    return null;
  }

  protected String mathMLToPresentationML(String xmlContent)
      throws IOException, InvalidXmlSchemaException, TransformerException {
    Transformer transformer = getMathMLTransformer();

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    transformer.transform(new StreamSource(new ByteArrayInputStream(xmlContent.getBytes())), new StreamResult(baos));
    String result = baos.toString("UTF-8");
    if (result.startsWith("<?xml")) {
      result = result.substring(result.indexOf(">") + 1);
    }
    return result;
  }

  private Transformer getMathMLTransformer()
      throws TransformerFactoryConfigurationError, IOException, TransformerConfigurationException {
    if (this.mathMlTransformer == null) {
      TransformerFactory factory = TransformerFactory.newInstance();

      Resource resource = new ClassPathResource("mathmlc2p.xsl");
      InputStream styleInputStream = resource.getInputStream();

      StreamSource stylesource = new StreamSource(styleInputStream);
      this.mathMlTransformer = factory.newTransformer(stylesource);
    }
    return this.mathMlTransformer;
  }

  protected void verifyToken(String token) throws SecurityException {
    if (getUserService().getUserByToken(token) == null) {
      throw new SecurityException("Invalid token");
    }
  }

}
