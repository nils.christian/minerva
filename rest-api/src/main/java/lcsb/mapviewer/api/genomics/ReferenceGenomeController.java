package lcsb.mapviewer.api.genomics;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.rmi.ObjectNotFoundException;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class ReferenceGenomeController extends BaseController {

  @Autowired
  private ReferenceGenomeRestImpl referenceGenomeController;

  @RequestMapping(value = "/genomics/taxonomies/{organismId}/genomeTypes/{type}/versions/{version}/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getGenomesByQuery(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "organismId") String organism, //
      @PathVariable(value = "type") String type, //
      @PathVariable(value = "version") String version//
  ) throws SecurityException, QueryException, ObjectNotFoundException {
    return referenceGenomeController.getReferenceGenome(token, organism, type, version);
  }

  @RequestMapping(value = "/genomics/taxonomies/{organismId}/genomeTypes/{type}/versions/{version}:getAvailableRemoteUrls", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getRemoteUrls(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "organismId") String organism, //
      @PathVariable(value = "type") String type, //
      @PathVariable(value = "version") String version//
  ) throws SecurityException, QueryException, ObjectNotFoundException {
    return referenceGenomeController.getRemoteUrls(token, organism, type, version);
  }

  @RequestMapping(value = "/genomics/taxonomies/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getGenomeTaxonomies(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, QueryException, ObjectNotFoundException {
    return referenceGenomeController.getReferenceGenomeTaxonomies(token);
  }

  @RequestMapping(value = "/genomics/", method = { RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addGenome(//
      @RequestBody MultiValueMap<String, Object> formData, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, QueryException, IOException, ReferenceGenomeConnectorException  {
    return referenceGenomeController.addReferenceGenome(formData, token);
  }

  @RequestMapping(value = "/genomics/taxonomies/{organismId}/genomeTypes/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getGenomeTaxonomyTypes(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "organismId") String organism //
  ) throws SecurityException, QueryException, ObjectNotFoundException {
    return referenceGenomeController.getReferenceGenomeTypes(token, organism);
  }

  @RequestMapping(value = "/genomics/taxonomies/{organismId}/genomeTypes/{type}/versions/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getGenomeVersion(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "organismId") String organism, //
      @PathVariable(value = "type") String type //
  ) throws SecurityException, QueryException, ObjectNotFoundException {
    return referenceGenomeController.getReferenceGenomeVersions(token, organism, type);
  }

  @RequestMapping(value = "/genomics/", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getDownloaded(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, QueryException, ObjectNotFoundException {
    return referenceGenomeController.getReferenceGenomes(token);
  }

  @RequestMapping(value = "/genomics/{genomeId}/", method = { RequestMethod.DELETE }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> removeGenome(//
      @PathVariable(value = "genomeId") String genomeId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, IOException {
    return referenceGenomeController.removeGenome(genomeId, token);
  }

  @RequestMapping(value = "/genomics/{genomeId}/geneMapping/{mappingId}/", method = { RequestMethod.DELETE }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> removeGeneMapping(//
      @PathVariable(value = "genomeId") String genomeId, //
      @PathVariable(value = "mappingId") String mappingId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, IOException, lcsb.mapviewer.api.ObjectNotFoundException {
    return referenceGenomeController.removeGeneMapping(genomeId,mappingId, token);
  }

  @RequestMapping(value = "/genomics/{genomeId}/geneMapping/", method = { RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addGeneMapping(//
      @PathVariable(value = "genomeId") String genomeId, //
      @RequestBody MultiValueMap<String, Object> formData, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, QueryException, IOException, ReferenceGenomeConnectorException  {
    return referenceGenomeController.addGeneMapping(formData, genomeId, token);
  }

}