package lcsb.mapviewer.api.genomics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;

@Transactional(value = "txManager")
public class ReferenceGenomeRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ReferenceGenomeRestImpl.class);

  /**
   * Service that manages reference genomes.
   */
  @Autowired
  private IReferenceGenomeService referenceGenomeService;

  @Autowired
  private BigFileCache bigFileCache;

  public Map<String, Object> getReferenceGenome(String token, String organismId, String type, String version)
      throws SecurityException, QueryException, ObjectNotFoundException {
    MiriamData organism = null;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    try {
      ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
      version = version.replaceAll("\\*", "");
      ReferenceGenome genome = referenceGenomeService.getReferenceGenomeViewByParams(organism, genomeType, version,
          token);
      if (genome == null) {
        throw new ObjectNotFoundException("Cannot find requested reference genome");
      }
      return genomeToMap(genome);
    } catch (IllegalArgumentException e) {
      throw new QueryException("Cannot find type: " + type);
    }
  }

  public List<Map<String, Object>> getRemoteUrls(String token, String organismId, String type, String version)
      throws SecurityException, QueryException, ObjectNotFoundException {
    MiriamData organism = null;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    try {
      ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
      String url = referenceGenomeService.getUrlForGenomeVersion(genomeType, organism, version);

      List<Map<String, Object>> result = new ArrayList<>();
      if (url != null) {
        Map<String, Object> row = new TreeMap<>();
        row.put("url", url);
        result.add(row);
      }
      return result;
    } catch (IllegalArgumentException e) {
      throw new QueryException("Cannot find type: " + type);
    }
  }

  private Map<String, Object> genomeToMap(ReferenceGenome genome) {
    Map<String, Object> result = new TreeMap<>();
    result.put("organism", super.createAnnotation(genome.getOrganism()));
    result.put("version", genome.getVersion());
    result.put("type", genome.getType());
    result.put("downloadProgress", genome.getDownloadProgress());
    result.put("sourceUrl", genome.getSourceUrl());
    result.put("localUrl", getLocalUrl(genome.getSourceUrl()));
    result.put("idObject", genome.getId());
    result.put("geneMapping", geneMappingToMaps(genome.getGeneMapping()));

    return result;
  }

  private String getLocalUrl(String sourceUrl) {
    String url = null;
    try {
      url = "../" + bigFileCache.getLocalPathForFile(sourceUrl);
    } catch (FileNotFoundException e) {
      logger.warn("Cannot find local file", e);
    }
    return url;
  }

  private List<Map<String, Object>> geneMappingToMaps(List<ReferenceGenomeGeneMapping> geneMapping) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ReferenceGenomeGeneMapping referenceGenomeGeneMapping : geneMapping) {
      result.add(geneMappingToMap(referenceGenomeGeneMapping));
    }

    return result;
  }

  private Map<String, Object> geneMappingToMap(ReferenceGenomeGeneMapping mapping) {
    Map<String, Object> result = new TreeMap<>();
    result.put("downloadProgress", mapping.getDownloadProgress());
    result.put("localUrl", getLocalUrl(mapping.getSourceUrl()));
    result.put("sourceUrl", mapping.getSourceUrl());
    result.put("name", mapping.getName());
    result.put("idObject", mapping.getId());
    return result;
  }

  public List<Map<String, Object>> getReferenceGenomeTaxonomies(String token) throws QueryException {
    try {
      Set<MiriamData> organisms = new HashSet<>();
      for (ReferenceGenomeType type : ReferenceGenomeType.values()) {
        organisms.addAll(referenceGenomeService.getOrganismsByReferenceGenomeType(type));
      }
      List<Map<String, Object>> result = new ArrayList<>();
      for (MiriamData miriamData : organisms) {
        result.add(createAnnotation(miriamData));
      }
      return result;
    } catch (ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining organism list", e);
    }
  }

  public List<Map<String, Object>> getReferenceGenomeTypes(String token, String organismId) throws QueryException {
    try {
      Set<MiriamData> organisms = new HashSet<>();
      for (ReferenceGenomeType type : ReferenceGenomeType.values()) {
        organisms.addAll(referenceGenomeService.getOrganismsByReferenceGenomeType(type));
      }
      List<Map<String, Object>> result = new ArrayList<>();
      for (MiriamData miriamData : organisms) {
        if (miriamData.getResource().equals(organismId)) {
          Map<String, Object> entry = new HashMap<>();
          entry.put("type", ReferenceGenomeType.UCSC.toString());
          result.add(entry);
        }
      }
      return result;
    } catch (ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining organism list", e);
    }
  }

  public List<Map<String, Object>> getReferenceGenomeVersions(String token, String organismId, String type)
      throws QueryException {
    ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
    MiriamData organism = null;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    List<String> versions;
    try {
      versions = referenceGenomeService.getAvailableGenomeVersions(genomeType, organism);
    } catch (ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining version list", e);
    }

    List<Map<String, Object>> result = new ArrayList<>();
    for (String string : versions) {
      Map<String, Object> entry = new HashMap<>();
      entry.put("version", string);
      result.add(entry);
    }
    return result;
  }

  public List<Map<String, Object>> getReferenceGenomes(String token)
      throws SecurityException, QueryException, ObjectNotFoundException {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ReferenceGenome genome : referenceGenomeService.getDownloadedGenomes()) {
      result.add(genomeToMap(genome));
    }
    return result;
  }

  public Map<String, Object> removeGenome(String genomeId, String token) throws SecurityException, IOException {
    if (!getUserService().userHasPrivilege(token, PrivilegeType.MANAGE_GENOMES)) {
      throw new SecurityException("Access denied");
    }
    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(Integer.parseInt(genomeId), token);
    referenceGenomeService.removeGenome(genome);
    return okStatus();
  }

  public Map<String, Object> addReferenceGenome(MultiValueMap<String, Object> formData, String token)
      throws SecurityException, QueryException, IOException, ReferenceGenomeConnectorException {
    if (!getUserService().userHasPrivilege(token, PrivilegeType.MANAGE_GENOMES)) {
      throw new SecurityException("Access denied");
    }

    String organismId = getFirstValue(formData.get("organismId"));
    String type = getFirstValue(formData.get("type"));
    String version = getFirstValue(formData.get("version"));
    String url = getFirstValue(formData.get("sourceUrl"));
    MiriamData organism = null;
    if (organismId != null && !organismId.isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    } else {
      throw new QueryException("Unknown taxonomy organism: " + organismId);
    }
    ReferenceGenomeType genomeType = ReferenceGenomeType.valueOf(type);
    try {
      referenceGenomeService.addReferenceGenome(genomeType, organism, version, url);
      return okStatus();
    } catch (URISyntaxException e) {
      throw new QueryException("Problem wih given uri", e);
    }
  }

  public Map<String, Object> addGeneMapping(MultiValueMap<String, Object> formData, String genomeId, String token)
      throws SecurityException, QueryException, IOException, ReferenceGenomeConnectorException {
    if (!getUserService().userHasPrivilege(token, PrivilegeType.MANAGE_GENOMES)) {
      throw new SecurityException("Access denied");
    }

    int id = Integer.parseInt(genomeId);
    try {
      ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(id, token);
      String name = getFirstValue(formData.get("name"));
      String url = getFirstValue(formData.get("url"));
      referenceGenomeService.addReferenceGenomeGeneMapping(genome, name, url);
      return okStatus();
    } catch (URISyntaxException e) {
      throw new QueryException("Problem wih given uri", e);
    }
  }

  public Map<String, Object> removeGeneMapping(String genomeId, String mappingId, String token)
      throws SecurityException, IOException, ObjectNotFoundException {
    if (!getUserService().userHasPrivilege(token, PrivilegeType.MANAGE_GENOMES)) {
      throw new SecurityException("Access denied");
    }
    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(Integer.parseInt(genomeId), token);
    if (genome == null) {
      throw new ObjectNotFoundException("Genome doesn't exist");
    }
    int id = Integer.parseInt(mappingId);
    ReferenceGenomeGeneMapping geneMapping = null;
    for (ReferenceGenomeGeneMapping mapping : genome.getGeneMapping()) {
      if (mapping.getId() == id) {
        geneMapping = mapping;
      }
    }
    if (geneMapping == null) {
      throw new ObjectNotFoundException("Gene mapping doesn't exist");
    }
    referenceGenomeService.removeReferenceGenomeGeneMapping(geneMapping);
    return okStatus();
  }

}
