package lcsb.mapviewer.api.plugins;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class PluginController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(PluginController.class);

  @Autowired
  private PluginRestImpl pluginRest;

  @RequestMapping(value = "/plugins/", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> createPlugin(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @RequestParam(value = "hash") String hash, //
      @RequestParam(value = "name") String name, //
      @RequestParam(value = "version") String version, //
      @RequestParam(value = "url", defaultValue = "") String url //
  ) throws SecurityException {
    return pluginRest.createPlugin(token, hash, name, version, url);
  }

  @RequestMapping(value = "/plugins/", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getPlugins(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException {
    return pluginRest.getPlugins(token);
  }

  @RequestMapping(value = "/plugins/{hash}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getFile(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "hash") String hash //
  ) throws SecurityException, ObjectNotFoundException {
    return pluginRest.getPlugin(token, hash);
  }

  @RequestMapping(value = "/plugins/{hash}/data/users/{login}/{key}", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> createPluginDataEntry(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "hash") String hash, //
      @PathVariable(value = "login") String login, //
      @PathVariable(value = "key") String key, //
      @RequestParam(value = "value", defaultValue = "") String value //
  ) throws SecurityException, ObjectNotFoundException {
    return pluginRest.createPluginDataEntry(token, hash, login, key, value);
  }

  @RequestMapping(value = "/plugins/{hash}/data/users/{login}/{key}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getPluginDataEntry(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "hash") String hash, //
      @PathVariable(value = "login") String login, //
      @PathVariable(value = "key") String key //
  ) throws SecurityException, ObjectNotFoundException {
    return pluginRest.getPluginDataEntry(token, hash, login, key);
  }

  @RequestMapping(value = "/plugins/{hash}/data/global/{key}", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> createPluginDataEntry(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "hash") String hash, //
      @PathVariable(value = "key") String key, //
      @RequestParam(value = "value", defaultValue = "") String value //
  ) throws SecurityException, ObjectNotFoundException {
    return pluginRest.createPluginDataEntry(token, hash, null, key, value);
  }

  @RequestMapping(value = "/plugins/{hash}/data/global/{key}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getPluginDataEntry(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "hash") String hash, //
      @PathVariable(value = "key") String key //
  ) throws SecurityException, ObjectNotFoundException {
    return pluginRest.getPluginDataEntry(token, hash, null, key);
  }

}