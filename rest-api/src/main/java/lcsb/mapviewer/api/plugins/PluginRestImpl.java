package lcsb.mapviewer.api.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.plugin.PluginDao;
import lcsb.mapviewer.persist.dao.plugin.PluginDataEntryDao;
import lcsb.mapviewer.services.SecurityException;

@Transactional(value = "txManager")
public class PluginRestImpl extends BaseRestImpl {

  @Autowired
  private PluginDao pluginDao;

  @Autowired
  private PluginDataEntryDao pluginDataEntryDao;

  public Map<String, Object> createPlugin(String token, String hash, String name, String version, String url) {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin != null) {
      plugin.getUrls().add(url);
      pluginDao.update(plugin);
    } else {
      plugin = new Plugin();
      plugin.setHash(hash);
      plugin.setName(name);
      plugin.setVersion(version);
      if (!url.isEmpty()) {
        plugin.getUrls().add(url);
      }
      pluginDao.add(plugin);
    }
    return pluginToMap(plugin);
  }

  private Map<String, Object> pluginToMap(Plugin plugin) {
    Map<String, Object> result = new TreeMap<>();
    result.put("hash", plugin.getHash());
    result.put("name", plugin.getName());
    result.put("version", plugin.getVersion());
    List<String> urls = new ArrayList<>();
    urls.addAll(plugin.getUrls());
    Collections.sort(urls);
    result.put("urls", urls);
    return result;
  }

  public Map<String, Object> getPlugin(String token, String hash) throws ObjectNotFoundException {
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    return pluginToMap(plugin);
  }

  public Map<String, Object> createPluginDataEntry(String token, String hash, String login, String key, String value)
      throws SecurityException, ObjectNotFoundException {
    User user = null;
    if (login != null) {
      user = getUserService().getUserByToken(token);
      if (!user.getLogin().equals(login)) {
        throw new SecurityException();
      }
    }
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    PluginDataEntry entry = pluginDataEntryDao.getByKey(plugin, key, user);
    if (entry == null) {
      entry = new PluginDataEntry();
      entry.setKey(key);
      entry.setPlugin(plugin);
      entry.setUser(user);
      entry.setValue(value);
      pluginDataEntryDao.add(entry);
    } else {
      entry.setValue(value);
      pluginDataEntryDao.update(entry);
    }

    return pluginEntryToMap(entry);
  }

  private Map<String, Object> pluginEntryToMap(PluginDataEntry entry) {
    Map<String, Object> result = new TreeMap<>();
    result.put("key", entry.getKey());
    result.put("value", entry.getValue());
    if (entry.getUser() != null) {
      result.put("user", entry.getUser().getLogin());
    }
    return result;
  }

  public Map<String, Object> getPluginDataEntry(String token, String hash, String login, String key)
      throws SecurityException, ObjectNotFoundException {
    User user = null;
    if (login != null) {
      user = getUserService().getUserByToken(token);
      if (!user.getLogin().equals(login)) {
        throw new SecurityException();
      }
    }
    Plugin plugin = pluginDao.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    PluginDataEntry entry = pluginDataEntryDao.getByKey(plugin, key, user);
    if (entry == null) {
      throw new ObjectNotFoundException("Entry doesn't exist");
    }

    return pluginEntryToMap(entry);
  }

  public List<Map<String, Object>> getPlugins(String token) {
    List<Plugin> plugins = pluginDao.getAll();
    plugins.sort(Plugin.ID_COMPARATOR);

    List<Map<String, Object>> result = new ArrayList<>();
    for (Plugin plugin : plugins) {
      result.add(pluginToMap(plugin));
    }
    return result;
  }

}
