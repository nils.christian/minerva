package lcsb.mapviewer.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.TreeMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;

public abstract class BaseController {
  private Logger logger = Logger.getLogger(BaseController.class);

  private ObjectMapper mapper = new ObjectMapper();

  @ExceptionHandler({ Exception.class })
  public ResponseEntity<Object> handleException(Exception e, WebRequest request) {
    if (e instanceof lcsb.mapviewer.services.SecurityException) {
      return createErrorResponse("Access denied.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else if (e instanceof ObjectNotFoundException) {
      return createErrorResponse("Object not found.", e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    } else if (e instanceof ObjectExistsException) {
      return createErrorResponse("Object already exists.", e.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT);
    } else if (e instanceof QueryException) {
      logger.error(e, e);
      return createErrorResponse("Query server error.", e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else if (e instanceof ServletRequestBindingException && e.getMessage().indexOf(Configuration.AUTH_TOKEN) >= 0) {
      return createErrorResponse("Access denied.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else {
      logger.error(e, e);
      return createErrorResponse("Internal server error.", e.getMessage(), new HttpHeaders(),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private ResponseEntity<Object> createErrorResponse(String errorMessage, String error, HttpHeaders httpHeaders,
      HttpStatus status) {

    return new ResponseEntity<Object>(
        "{\"error\" : \"" + errorMessage + "\",\"reason\":" + new Gson().toJson(error) + "}", httpHeaders, status);
  }

  public Map<String, Object> parseBody(String body) throws IOException, JsonParseException, JsonMappingException {
    if (body == null || body.isEmpty()) {
      return new TreeMap<>();
    }
    ObjectNode result = mapper.readValue(body, ObjectNode.class);
    return mapper.convertValue(result, Map.class);
  }

  protected Map<String, Object> getData(Map<String, Object> node, String objectName) {
    return (Map<String, Object>) node.get(objectName);
  }

  protected Map<String, String> parsePostBody(String body) {
    Map<String, String> result = new TreeMap<>();
    String[] parameters = body.split("&");
    for (String string : parameters) {
      int position = string.indexOf("=");
      if (position < 0) {
        position = string.length();
      }
      String key = string.substring(0, position);
      String value = null;
      if (position < string.length()) {
        value = string.substring(position + 1, string.length());
        try {
          value = URLDecoder.decode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
          throw new InvalidStateException("Cannot decode input", e);
        }
      }
      result.put(key, value);
    }
    return result;
  }

}
