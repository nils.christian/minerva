package lcsb.mapviewer.api.users;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
public class UserController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(UserController.class);

  @Autowired
  private IUserService userService;

  @Autowired
  private UserRestImpl userRest;

  @Autowired
  private AuthenticationProvider authenticationProvider;

  @RequestMapping(value = "/doLogin", method = { RequestMethod.GET, RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> login(//
      @RequestParam(value = "login", defaultValue = Configuration.ANONYMOUS_LOGIN) String login, //
      @RequestParam(value = "password", defaultValue = "") String password, //
      HttpServletResponse response, //
      HttpServletRequest request//
  ) throws SecurityException, IOException {
    try {
      UsernamePasswordAuthenticationToken springToken = new UsernamePasswordAuthenticationToken(login, password);
      springToken.setDetails(new WebAuthenticationDetails(request));
      Authentication authentication = this.authenticationProvider.authenticate(springToken);
      SecurityContextHolder.getContext().setAuthentication(authentication);

      userService.login(login, password, request.getSession().getId());

      Map<String, Object> result = new TreeMap<>();

      result.put("info", "Login successful. TOKEN returned as a cookie");
      return result;
    } catch (AuthenticationException e) {
      throw new SecurityException("Invalid credentials");
    }
  }

  @RequestMapping(value = "/users/{login:.+}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getUser(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "login") String login, //
      @RequestParam(value = "columns", defaultValue = "") String columns//
  ) throws SecurityException, ObjectNotFoundException {
    return userRest.getUser(token, login, columns);
  }

  @RequestMapping(value = "/users/{login}:updatePrivileges", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> updatePrivileges(//
      @RequestBody String body, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "login") String login //
  ) throws SecurityException, JsonParseException, JsonMappingException, IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "privileges");
    return userRest.updatePrivileges(token, login, data);
  }

  @RequestMapping(value = "/users/{login}:updatePreferences", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> updatePreferences(//
      @RequestBody String body, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "login") String login //
  ) throws SecurityException, JsonParseException, JsonMappingException, IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "preferences");
    return userRest.updatePreferences(token, login, data);
  }

  @RequestMapping(value = "/users/", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getUsers(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @RequestParam(value = "columns", defaultValue = "") String columns//
  ) throws SecurityException, ObjectNotFoundException {
    return userRest.getUsers(token, columns);
  }

  @RequestMapping(value = "/doLogout", method = { RequestMethod.GET, RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> logout(@CookieValue(value = Configuration.AUTH_TOKEN) String token,
      HttpServletRequest request, HttpServletResponse response //
  ) throws SecurityException, IOException {
    // spring logout
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      new SecurityContextLogoutHandler().logout(request, response, auth);
    }

    Map<String, Object> result = new TreeMap<>();
    result.put("status", "OK");

    final Boolean useSecureCookie = false;
    final String cookiePath = "/";

    Cookie cookie = new Cookie("MINERVA_AUTH_TOKEN", token);

    cookie.setSecure(useSecureCookie);
    cookie.setMaxAge(0);
    cookie.setPath(cookiePath);

    response.addCookie(cookie);
    result.put("status", "OK");
    return result;
  }

  @RequestMapping(value = "/users/{login:.+}", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> updateUser(//
      @RequestBody String body, //
      @PathVariable(value = "login") String login, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, QueryException, IOException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "user");
    return userRest.updateUser(token, login, data);
  }

  @RequestMapping(value = "/users/{login:.+}", method = { RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addOverlay(//
      @RequestBody MultiValueMap<String, Object> formData, //
      @PathVariable(value = "login") String login, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, IOException, QueryException {
    return userRest.addProject(token, login, formData);

  }

  @RequestMapping(value = "/users/{login:.+}", method = { RequestMethod.DELETE }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> removeUser(//
      @PathVariable(value = "login") String login, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      HttpServletRequest request, HttpServletResponse response //
  ) throws SecurityException, IOException, QueryException {
    String currentUserLogin = getUserService().getUserByToken(token).getLogin();
    Map<String, Object> result = userRest.removeUser(token, login);
    if (login.equals(currentUserLogin)) {
      return logout(token, request, response);
    }
    return result;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  /**
   * @return the userRest
   * @see #userRest
   */
  public UserRestImpl getUserRest() {
    return userRest;
  }

  /**
   * @param userRest
   *          the userRest to set
   * @see #userRest
   */
  public void setUserRest(UserRestImpl userRest) {
    this.userRest = userRest;
  }
}