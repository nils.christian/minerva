package lcsb.mapviewer.api.projects.models.bioEntities.reactions;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class ReactionsController extends BaseController {
	@Autowired
	private ReactionsRestImpl reactionController;

	@RequestMapping(value = "/projects/{projectId}/models/{modelId}/bioEntities/reactions/", method = { RequestMethod.GET, RequestMethod.POST },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Map<String, Object>> getReactions(//
			@PathVariable(value = "projectId") String projectId, //
			@PathVariable(value = "modelId") String modelId, //
			@RequestParam(value = "id", defaultValue = "") String id, //
			@RequestParam(value = "columns", defaultValue = "") String columns, //
			@CookieValue(value = Configuration.AUTH_TOKEN) String token, //
			@RequestParam(value = "participantId", defaultValue = "") String participantId//
	) throws SecurityException,QueryException {
		return reactionController.getReactions(projectId, id, columns, modelId, token, participantId);
	}

}