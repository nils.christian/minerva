package lcsb.mapviewer.api.projects.chemicals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.ChemicalParser;
import lcsb.mapviewer.annotation.services.ChemicalSearchException;
import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ElementIdentifierType;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.chemical.IChemicalService;

@Transactional(value = "txManager")
public class ChemicalRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ChemicalRestImpl.class);

  @Autowired
  private IChemicalService chemicalService;

  @Autowired
  private ChemicalParser chemicalParser;

  @Autowired
  private MeSHParser meSHParser;

  public List<Map<String, Object>> getChemicalsByQuery(String token, String projectId, String columns, String query)
      throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Project project = model.getProject();
    if (project.getDisease() == null) {
      throw new QueryException("Project doesn't have disease associated to it");
    }

    Set<String> columnSet = createChemicalColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }
    Chemical chemical = chemicalService.getByName(query,
        new DbSearchCriteria().project(project).organisms(organism).colorSet(0).disease(project.getDisease()));
    if (chemical != null) {
      List<Model> models = getModels(projectId, "*", token);
      result.add(prepareChemical(chemical, columnSet, models));
    }

    return result;
  }

  protected Map<String, Object> prepareChemical(Chemical chemical, Set<String> columnsSet, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();

    String description = "Mesh term not available";
    List<String> synonyms = new ArrayList<>();

	try {
		MeSH mesh = meSHParser.getMeSH(chemical.getChemicalId());
		if (mesh != null) {
		  description = mesh.getDescription();
		  synonyms = mesh.getSynonyms();
		} else {
		  logger.warn("Mesh used by chemical is invalid: " + chemical.getChemicalId());
		}
	} catch (AnnotatorException e) {
		logger.error("Problem with accessing mesh database", e);
	}

    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = chemical.getChemicalId();
      } else if (column.equals("name")) {
        value = chemical.getChemicalName();
      } else if (column.equals("references")) {
        List<Map<String, Object>> references = new ArrayList<>();
        references.add(createAnnotation(chemical.getChemicalId()));
        if (chemical.getCasID() != null) {
          references.add(createAnnotation(chemical.getCasID()));
        }
        value = references;
      } else if (column.equals("directevidencereferences")) {
        value = createAnnotations(chemical.getDirectEvidencePublication());
      } else if (column.equals("description")) {
        value = description;
      } else if (column.equals("directevidence")) {
        if (chemical.getDirectEvidence() != null) {
          value = chemical.getDirectEvidence().getValue();
        }
      } else if (column.equals("synonyms")) {
        value = synonyms;
      } else if (column.equals("targets")) {
        value = prepareTargets(chemical.getInferenceNetwork(), models);
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  protected Set<String> createChemicalColumnSet(String columns) {
    Set<String> columnsSet = new HashSet<>();
    if (columns.equals("")) {
      columnsSet.add("name");
      columnsSet.add("references");
      columnsSet.add("description");
      columnsSet.add("synonyms");
      columnsSet.add("id");
      columnsSet.add("directEvidenceReferences");
      columnsSet.add("directEvidence");
      columnsSet.add("targets");
    } else {
      for (String str : columns.split(",")) {
        columnsSet.add(str);
      }
    }
    return columnsSet;
  }

  /**
   * @return the chemicalService
   * @see #chemicalService
   */
  public IChemicalService getChemicalService() {
    return chemicalService;
  }

  /**
   * @param chemicalService
   *          the chemicalService to set
   * @see #chemicalService
   */
  public void setChemicalService(IChemicalService chemicalService) {
    this.chemicalService = chemicalService;
  }

  public List<Map<String, Object>> getChemicalsByTarget(String token, String projectId, String targetType,
      String targetId, String columns) throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Project project = model.getProject();

    if (project.getDisease() == null) {
      throw new QueryException("Project doesn't have disease associated to it");
    }

    List<Model> models = getModels(projectId, "*", token);
    Element element = null;

    Integer dbId = Integer.valueOf(targetId);
    List<Element> targets = new ArrayList<>();
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      for (Model m : models) {
        if (element == null) {
          element = m.getElementByDbId(dbId);
        }
      }
      if (element == null) {
        throw new QueryException("Invalid element identifier for given project");
      }
      targets.add(element);
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }

    Set<String> columnSet = createChemicalColumnSet(columns);

    List<Chemical> chemicals = chemicalService.getForTargets(targets,
        new DbSearchCriteria().project(project).organisms(organism).disease(project.getDisease()));

    List<Map<String, Object>> result = new ArrayList<>();

    for (Chemical chemical : chemicals) {
      result.add(prepareChemical(chemical, columnSet, models));
    }

    return result;
  }

  public List<String> getSuggestedQueryList(String projectId, String token)
      throws SecurityException, ChemicalSearchException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    return chemicalParser.getSuggestedQueryList(project, project.getDisease());
  }
}
