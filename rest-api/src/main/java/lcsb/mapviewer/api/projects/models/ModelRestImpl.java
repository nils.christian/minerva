package lcsb.mapviewer.api.projects.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.services.SecurityException;

@Transactional(value = "txManager")
public class ModelRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ModelRestImpl.class);

  public List<Map<String, Object>> getModels(String projectId, String token)
      throws SecurityException, ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    List<Map<String, Object>> result = createData(project, token);
    return result;
  }

  public Map<String, Object> getModel(String projectId, String modelId, String token) throws SecurityException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    Model submodel = model.getSubmodelById(modelId);
    if (submodel == null) {
      return null;
    } else {
      Map<String, Object> result = new LinkedHashMap<>();
      result.put("version", null);
      result.put("name", submodel.getName());
      result.put("idObject", submodel.getId());
      result.put("tileSize", submodel.getTileSize());
      result.put("width", (int) (double) submodel.getWidth());
      result.put("height", (int) (double) submodel.getHeight());
      result.put("defaultCenterX", submodel.getDefaultCenterX());
      result.put("defaultCenterY", submodel.getDefaultCenterY());
      result.put("defaultZoomLevel", submodel.getDefaultZoomLevel());
      result.put("minZoom", Configuration.MIN_ZOOM_LEVEL);
      result.put("maxZoom", Configuration.MIN_ZOOM_LEVEL + submodel.getZoomLevels());
      result.put("submodelType", SubmodelType.UNKNOWN);
      return result;
    }
  }

  private List<Map<String, Object>> createData(Project project, String token) throws SecurityException {
    List<Map<String, Object>> result = new ArrayList<>();
    Model model = getModelService().getLastModelByProjectId(project.getProjectId(), token);
    if (model != null) {

      List<Model> models = new ArrayList<>();
      models.add(model);
      models.addAll(model.getSubmodels());
      models.sort(Model.ID_COMPARATOR);
      for (Model model2 : models) {
        result.add(getModel(project.getProjectId(), model2.getId() + "", token));
      }
    }
    return result;
  }

  public Map<String, Object> updateModel(String projectId, String modelId, Map<String, Object> data, String token)
      throws SecurityException, QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    boolean canModify = getUserService().userHasPrivilege(token, PrivilegeType.ADD_MAP);
    if (!canModify) {
      throw new SecurityException("You cannot update projects");
    }
    ModelData model = null;
    Integer id = Integer.valueOf(modelId);
    for (ModelData m : project.getModels()) {
      if (m.getId().equals(id)) {
        model = m;
      }
    }
    if (model == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    Set<String> fields = data.keySet();
    for (String fieldName : fields) {
      Object value = data.get(fieldName);
      if (fieldName.equalsIgnoreCase("defaultCenterX")) {
        model.setDefaultCenterX(parseDouble(value));
      } else if (fieldName.equalsIgnoreCase("defaultCenterY")) {
        model.setDefaultCenterY(parseDouble(value));
      } else if (fieldName.equalsIgnoreCase("defaultZoomLevel")) {
        model.setDefaultZoomLevel(parseInteger(value));
      } else if (fieldName.equalsIgnoreCase("id")) {
        if (!model.getId().equals(parseInteger(value))) {
          throw new QueryException("Id doesn't match: " + value + ", " + model.getId());
        }
      } else {
        throw new QueryException("Unknown field: " + fieldName);
      }
    }
    getModelService().updateModel(model, token);
    return getModel(projectId, modelId, token);
  }

  private Integer parseInteger(Object value) throws QueryException {
    if (value instanceof Integer) {
      return (Integer) value;
    } else if (value instanceof String) {
      if (((String) value).equalsIgnoreCase("")) {
        return null;
      } else {
        try {
          return Integer.parseInt((String) value);
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid double value: " + value);
        }
      }
    } else {
      throw new InvalidArgumentException();
    }

  }

  private Double parseDouble(Object value) throws QueryException {
    if (value instanceof Double) {
      return (Double) value;
    } else if (value instanceof String) {
      if (((String) value).equalsIgnoreCase("")) {
        return null;
      } else {
        try {
          return Double.parseDouble((String) value);
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid double value: " + value);
        }
      }
    } else if (value == null) {
      return null;
    } else {
      throw new InvalidArgumentException();
    }
  }
}
