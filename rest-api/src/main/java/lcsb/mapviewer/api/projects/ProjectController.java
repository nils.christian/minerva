package lcsb.mapviewer.api.projects;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class ProjectController extends BaseController {
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ProjectController.class);

  @Autowired
  private ServletContext context;

  @Autowired
  private ProjectRestImpl projectController;

  @RequestMapping(value = "/projects/{projectId:.+}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getProject(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, ObjectNotFoundException {
    return projectController.getProject(projectId, token);
  }

  @RequestMapping(value = "/projects/{projectId:.+}", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> updateProject(//
      @RequestBody String body, //
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "project");
    return projectController.updateProject(token, projectId, data);

  }

  @RequestMapping(value = "/projects/{projectId:.+}", method = { RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addProject(//
      @RequestBody MultiValueMap<String, Object> formData, //
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, IOException, QueryException {
    return projectController.addProject(token, projectId, formData, context.getRealPath("/"));

  }

  @RequestMapping(value = "/projects/{projectId:.+}", method = { RequestMethod.DELETE }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> removeProject(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, IOException, QueryException {
    return projectController.removeProject(token, projectId, context.getRealPath("/"));

  }

  @RequestMapping(value = "/projects/", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getProjects(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, ObjectNotFoundException {
    return projectController.getProjects(token);
  }

  @RequestMapping(value = "/projects/{projectId}/statistics", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Object getStatistics(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, ObjectNotFoundException {
    return projectController.getStatistics(projectId, token);
  }

  @RequestMapping(value = "/projects/{projectId}:downloadSource", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public ResponseEntity<byte[]> getProjectSource(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId //
  ) throws SecurityException, QueryException {

    FileEntry file = projectController.getSource(token, projectId);
    MediaType type = MediaType.TEXT_PLAIN;
    if (file.getOriginalFileName().endsWith("xml")) {
      type = MediaType.APPLICATION_XML;
    } else if (file.getOriginalFileName().endsWith("zip")) {
      type = MediaType.APPLICATION_OCTET_STREAM;
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}:downloadImage", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public ResponseEntity<byte[]> getModelAsImage(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @RequestParam(value = "handlerClass") String handlerClass, //
      @RequestParam(value = "backgroundOverlayId", defaultValue = "") String backgroundOverlayId, //
      @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds, //
      @RequestParam(value = "zoomLevel", defaultValue = "") String zoomLevel, //
      @RequestParam(value = "polygonString", defaultValue = "") String polygonString//
  ) throws SecurityException, QueryException, IOException, InvalidColorSchemaException, CommandExecutionException,
      DrawingException {

    FileEntry file = projectController.getModelAsImage(token, projectId, modelId, handlerClass, backgroundOverlayId,
        overlayIds, zoomLevel, polygonString);
    MediaType type = MediaType.APPLICATION_OCTET_STREAM;
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}:downloadModel", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public ResponseEntity<byte[]> getModelAsModelFile(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @RequestParam(value = "handlerClass") String handlerClass, //
      @RequestParam(value = "backgroundOverlayId", defaultValue = "") String backgroundOverlayId, //
      @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds, //
      @RequestParam(value = "zoomLevel", defaultValue = "") String zoomLevel, //
      @RequestParam(value = "polygonString", defaultValue = "") String polygonString//
  ) throws SecurityException, QueryException, IOException, InvalidColorSchemaException, CommandExecutionException,
      ConverterException, InconsistentModelException {

    FileEntry file = projectController.getModelAsModelFile(token, projectId, modelId, handlerClass, backgroundOverlayId,
        overlayIds, zoomLevel, polygonString);
    MediaType type = MediaType.APPLICATION_OCTET_STREAM;
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  @RequestMapping(value = "/projects/{projectId}/logs/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getLogs(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @RequestParam(value = "level", defaultValue = "warning") String level, //
      @RequestParam(value = "start", defaultValue = "0") String start, //
      @RequestParam(value = "length", defaultValue = "10") Integer length, //
      @RequestParam(value = "sortColumn", defaultValue = "id") String sortColumn, //
      @RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder, //
      @RequestParam(value = "search", defaultValue = "") String search//
  ) throws QueryException, SecurityException {
    return projectController.getLogs(projectId, level, token, start, length, sortColumn, sortOrder, search);
  }

  /**
   * @return the context
   * @see #context
   */
  public ServletContext getContext() {
    return context;
  }

  /**
   * @param context
   *          the context to set
   * @see #context
   */
  public void setContext(ServletContext context) {
    this.context = context;
  }

  @RequestMapping(value = "/projects/{projectId}/submapConnections", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getSubmapConnections(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId //
  ) throws SecurityException, QueryException {

    return projectController.getSubmapConnections(token, projectId);
  }

}