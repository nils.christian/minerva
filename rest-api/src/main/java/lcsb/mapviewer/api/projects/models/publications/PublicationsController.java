package lcsb.mapviewer.api.projects.models.publications;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class PublicationsController extends BaseController {
	@Autowired
	private PublicationsRestImpl projectController;

	@RequestMapping(value = "/projects/{projectId}/models/{modelId}/publications/", method = { RequestMethod.GET },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public Map<String, Object> getPublications(//
			@PathVariable(value = "projectId") String projectId, //
			@PathVariable(value = "modelId") String modelId, //
			@CookieValue(value = Configuration.AUTH_TOKEN) String token, //
			@RequestParam(value = "start", defaultValue = "0") String start, //
			@RequestParam(value = "length", defaultValue = "10") Integer length, //
			@RequestParam(value = "sortColumn", defaultValue = "pubmedId") String sortColumn, //
			@RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder, //
			@RequestParam(value = "search", defaultValue = "") String search//
	) throws QueryException, SecurityException {
		return projectController.getPublications(projectId, modelId, token, start, length, sortColumn, sortOrder, search);
	}

}