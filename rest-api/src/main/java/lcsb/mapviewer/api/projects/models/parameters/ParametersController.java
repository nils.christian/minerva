package lcsb.mapviewer.api.projects.models.parameters;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class ParametersController extends BaseController {
  @Autowired
  private ParametersRestImpl parameterController;

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/parameters/{parameterId}", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getParameter(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "parameterId") String parameterId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws QueryException, SecurityException {
    return parameterController.getParameter(projectId, modelId, token, parameterId);
  }

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/parameters/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getParameters(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws QueryException, SecurityException {
    return parameterController.getParameters(projectId, modelId, token);
  }

}