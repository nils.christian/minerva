package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class ElementsController extends BaseController {
	@Autowired
	private ElementsRestImpl projectController;

	@RequestMapping(value = "/projects/{projectId}/models/{modelId}/bioEntities/elements/", method = { RequestMethod.GET, RequestMethod.POST },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Map<String, Object>> getElements(//
			@PathVariable(value = "projectId") String projectId, //
			@PathVariable(value = "modelId") String modelId, //
			@RequestParam(value = "id", defaultValue = "") String id, //
			@RequestParam(value = "type", defaultValue = "") String type, //
			@RequestParam(value = "columns", defaultValue = "") String columns, //
			@RequestParam(value = "includedCompartmentIds", defaultValue = "") String includedCompartmentIds, //
			@RequestParam(value = "excludedCompartmentIds", defaultValue = "") String excludedCompartmentIds, //
			@CookieValue(value = Configuration.AUTH_TOKEN) String token//
	) throws SecurityException, QueryException {
		return projectController.getElements(projectId, id, columns, modelId, token, type, includedCompartmentIds, excludedCompartmentIds);
	}

}