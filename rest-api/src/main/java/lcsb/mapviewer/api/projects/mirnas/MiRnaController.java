package lcsb.mapviewer.api.projects.mirnas;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.annotation.services.MiRNASearchException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class MiRnaController extends BaseController {

  @Autowired
  private MiRnaRestImpl miRnaController;

  @RequestMapping(value = "/projects/{projectId}/miRnas:search", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getMiRnasByQuery(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "columns", defaultValue = "") String columns, //
      @RequestParam(value = "query", defaultValue = "") String query, //
      @RequestParam(value = "target", defaultValue = "") String target //
  ) throws SecurityException, QueryException {
    if (!query.equals("")) {
      return miRnaController.getMiRnasByQuery(token, projectId, columns, query);
    } else if (target.indexOf(":") >= 0) {
      String targetType = target.split(":", -1)[0];
      String targetId = target.split(":", -1)[1];
      return miRnaController.getMiRnasByTarget(token, projectId, targetType, targetId, columns);
    } else {
      return new ArrayList<>();
    }
  }

  @RequestMapping(value = "/projects/{projectId}/miRnas/suggestedQueryList", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<String> getSuggestedQueryList(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token//
  ) throws SecurityException, MiRNASearchException {
    return miRnaController.getSuggestedQueryList(projectId, token);
  }

}