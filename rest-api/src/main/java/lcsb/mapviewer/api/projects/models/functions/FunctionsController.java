package lcsb.mapviewer.api.projects.models.functions;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class FunctionsController extends BaseController {
  @Autowired
  private FunctionsRestImpl functionController;

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/functions/{functionId}", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getFunction(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "functionId") String functionId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws QueryException, SecurityException {
    return functionController.getFunction(projectId, modelId, token, functionId);
  }

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/functions/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getFunctions(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws QueryException, SecurityException {
    return functionController.getFunctions(projectId, modelId, token);
  }

}