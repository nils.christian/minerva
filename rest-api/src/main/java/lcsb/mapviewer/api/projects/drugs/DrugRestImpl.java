package lcsb.mapviewer.api.projects.drugs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ElementIdentifierType;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.drug.IDrugService;

@Transactional(value = "txManager")
public class DrugRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(DrugRestImpl.class);

  @Autowired
  private IDrugService drugService;

  @Autowired
  private IUserService userService;

  public List<Map<String, Object>> getDrugsByQuery(String token, String projectId, String columns, String query)
      throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Project project = model.getProject();

    Set<String> columnSet = createDrugColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }
    Drug drug = drugService.getByName(query, new DbSearchCriteria().project(project).organisms(organism).colorSet(0));
    if (drug != null) {
      List<Model> models = getModels(projectId, "*", token);
      result.add(prepareDrug(drug, columnSet, models));
    }

    return result;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  private Map<String, Object> prepareDrug(Drug drug, Set<String> columnsSet, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = drug.getName();
      } else if (column.equals("name")) {
        value = drug.getName();
      } else if (column.equals("references")) {
        value = createAnnotations(drug.getSources());
      } else if (column.equals("description")) {
        value = drug.getDescription();
      } else if (column.equals("bloodbrainbarrier")) {
        value = drug.getBloodBrainBarrier();
      } else if (column.equals("brandnames")) {
        value = drug.getBrandNames();
      } else if (column.equals("synonyms")) {
        value = drug.getSynonyms();
      } else if (column.equals("targets")) {
        value = prepareTargets(drug.getTargets(), models);
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  private Set<String> createDrugColumnSet(String columns) {
    Set<String> columnsSet = new HashSet<>();
    if (columns.equals("")) {
      columnsSet.add("name");
      columnsSet.add("references");
      columnsSet.add("description");
      columnsSet.add("bloodBrainBarrier");
      columnsSet.add("brandNames");
      columnsSet.add("synonyms");
      columnsSet.add("id");
      columnsSet.add("targets");
    } else {
      for (String str : columns.split(",")) {
        columnsSet.add(str);
      }
    }
    return columnsSet;
  }

  /**
   * @return the drugService
   * @see #drugService
   */
  public IDrugService getDrugService() {
    return drugService;
  }

  /**
   * @param drugService
   *          the drugService to set
   * @see #drugService
   */
  public void setDrugService(IDrugService drugService) {
    this.drugService = drugService;
  }

  public List<Map<String, Object>> getDrugsByTarget(String token, String projectId, String targetType, String targetId,
      String columns) throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Project project = model.getProject();

    List<Model> models = getModels(projectId, "*", token);

    Integer dbId = Integer.valueOf(targetId);
    List<Element> targets = getTargets(targetType, models, dbId);
    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }

    Set<String> columnSet = createDrugColumnSet(columns);

    List<Drug> drugs = drugService.getForTargets(targets, new DbSearchCriteria().project(project).organisms(organism));

    List<Map<String, Object>> result = new ArrayList<>();

    for (Drug drug : drugs) {
      result.add(prepareDrug(drug, columnSet, models));
    }

    return result;
  }

  private List<Element> getTargets(String targetType, List<Model> models, Integer dbId) throws QueryException {
    List<Element> targets = new ArrayList<>();
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      Element element = null;
      for (Model m : models) {
        if (element == null) {
          element = m.getElementByDbId(dbId);
        }
      }
      if (element == null) {
        throw new QueryException("Invalid element identifier for given project");
      }
      targets.add(element);
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
    return targets;
  }

  public List<String> getSuggestedQueryList(String projectId, String token)
      throws SecurityException, DrugSearchException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    return drugService.getSuggestedQueryList(project, project.getOrganism());
  }

}
