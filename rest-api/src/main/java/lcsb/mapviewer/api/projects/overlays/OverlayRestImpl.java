package lcsb.mapviewer.api.projects.overlays;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ElementIdentifierType;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.GeneVariationColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.persist.dao.map.LayoutDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.ILayoutService.CreateLayoutParams;
import lcsb.mapviewer.services.utils.data.ColorSchemaType;

@Transactional(value = "txManager")
public class OverlayRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(OverlayRestImpl.class);

  @Autowired
  private ILayoutService layoutService;

  @Autowired
  private UploadedFileEntryDao uploadedFileEntryDao;

  @Autowired
  private LayoutDao layoutDao;

  ColorParser colorParser = new ColorParser();

  public List<Map<String, Object>> getOverlayList(String token, String projectId, String creatorLogin,
      String publicOverlay) throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      return new ArrayList<>();
    }
    User creator = null;
    if (creatorLogin != null && !creatorLogin.isEmpty()) {
      creator = getUserService().getUserByLogin(creatorLogin);
      if (creator == null) {
        throw new ObjectNotFoundException("User with given id doesn't exist: " + creatorLogin);
      }
    }
    Boolean publicData = null;
    if (publicOverlay != null && !publicOverlay.isEmpty()) {
      publicData = publicOverlay.equalsIgnoreCase("true");
    }
    return overlaysToMap(layoutService.getCustomLayouts(model, token, publicData, creator));
  }

  private List<Map<String, Object>> overlaysToMap(List<Layout> overlays) {
    overlays.sort(Layout.ID_COMPARATOR);
    List<Map<String, Object>> result = new ArrayList<>();
    for (Layout overlay : overlays) {
      result.add(overlayToMap(overlay));
    }
    return result;
  }

  private Map<String, Object> overlayToMap(Layout overlay) {

    Map<String, Object> result = new TreeMap<>();
    result.put("idObject", overlay.getId());
    result.put("name", overlay.getTitle());
    result.put("order", overlay.getOrderIndex());
    result.put("description", overlay.getDescription());
    result.put("publicOverlay", overlay.isPublicLayout());
    result.put("defaultOverlay", overlay.isDefaultOverlay());
    result.put("googleLicenseConsent", overlay.isGoogleLicenseConsent());
    List<Map<String, Object>> images = new ArrayList<>();
    List<Layout> childLayouts = new ArrayList<>();
    childLayouts.add(overlay);
    childLayouts.addAll(overlay.getLayouts());
    childLayouts.sort(Layout.ID_COMPARATOR);
    for (Layout child : childLayouts) {
      Map<String, Object> image = new TreeMap<>();
      image.put("path", child.getDirectory());
      image.put("modelId", child.getModel().getId());
      images.add(image);
    }
    result.put("images", images);
    if (overlay.getCreator() != null) {
      result.put("creator", overlay.getCreator().getLogin());
    }
    result.put("inputDataAvailable", overlay.getInputData() != null);
    return result;
  }

  /**
   * @return the layoutService
   * @see #layoutService
   */
  public ILayoutService getLayoutService() {
    return layoutService;
  }

  /**
   * @param layoutService
   *          the layoutService to set
   * @see #layoutService
   */
  public void setLayoutService(ILayoutService layoutService) {
    this.layoutService = layoutService;
  }

  public List<Map<String, Object>> getOverlayElements(String token, String projectId, int overlayId, String columns)
      throws QueryException, SecurityException {

    String columnSet[];
    if (columns != null && !columns.trim().isEmpty()) {
      columnSet = columns.split(",");
    } else {
      columnSet = new String[] { "modelId", "idObject", "value", "color", "uniqueId" };
    }
    List<Map<String, Object>> result = new ArrayList<>();

    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    List<Pair<Element, ColorSchema>> speciesList = layoutService.getAliasesForLayout(model, overlayId, token);
    for (Pair<Element, ColorSchema> elementDataOverlay : speciesList) {
      Map<String, Object> element = new TreeMap<>();
      element.put("type", ElementIdentifierType.ALIAS);
      element.put("overlayContent", overlayContentToMap(elementDataOverlay, columnSet));
      result.add(element);
    }

    List<Pair<Reaction, ColorSchema>> reactions = layoutService.getReactionsForLayout(model, overlayId, token);
    for (Pair<Reaction, ColorSchema> reactionDataOverlay : reactions) {
      Map<String, Object> element = new TreeMap<>();
      element.put("type", ElementIdentifierType.REACTION);
      element.put("overlayContent", overlayContentToMap(reactionDataOverlay, columnSet));
      result.add(element);
    }
    return result;
  }

  public Map<String, Object> getOverlayById(String token, String projectId, String overlayId)
      throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    return overlayToMap(layoutService.getLayoutById(Integer.valueOf(overlayId), token));
  }

  public FileEntry getOverlaySource(String token, String projectId, String overlayId)
      throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    try {
      Integer id = Integer.valueOf(overlayId);
      Layout layout = layoutService.getLayoutById(id, token);
      if (layout == null) {
        throw new QueryException("Invalid overlay id");
      }
      // lazy initialization issue
      layout.getInputData().getFileContent();
      return layout.getInputData();
    } catch (NumberFormatException e) {
      throw new QueryException("Invalid overlay id");
    }
  }

  public Map<String, Object> updateOverlay(String token, String projectId, String overlayId,
      Map<String, Object> overlayData) throws QueryException, SecurityException {
    if (overlayData == null) {
      throw new QueryException("overlay field cannot be undefined");
    }
    try {
      Integer id = Integer.valueOf(overlayId);
      Layout layout = layoutService.getLayoutById(id, token);
      if (layout == null) {
        throw new ObjectNotFoundException("overlay doesn't exist");
      }
      boolean isAdmin = getUserService().userHasPrivilege(token, PrivilegeType.LAYOUT_MANAGEMENT,
          layout.getModel().getProject());
      if (layout.isPublicLayout() && !isAdmin) {
        throw new SecurityException("You cannot modify given overlay");
      }
      for (String key : overlayData.keySet()) {
        Object value = overlayData.get(key);
        if (key.equalsIgnoreCase("description")) {
          layout.setDescription((String) value);
        } else if (key.equalsIgnoreCase("name")) {
          layout.setTitle((String) value);
        } else if (key.equalsIgnoreCase("order")) {
          layout.setOrderIndex(parseInteger(value));
        } else if (key.equalsIgnoreCase("publicOverlay")) {
          layout.setPublicLayout(parseBoolean(value));
        } else if (key.equalsIgnoreCase("defaultOverlay")) {
          layout.setDefaultOverlay(parseBoolean(value));
        } else if (key.equalsIgnoreCase("googleLicenseConsent")) {
          layout.setGoogleLicenseConsent((Boolean) overlayData.get("googleLicenseConsent"));
        } else if (key.equalsIgnoreCase("creator")) {
          if ("".equals(value)) {
            layout.setCreator(null);
          } else {
            layout.setCreator(getUserService().getUserByLogin((String) value));
          }
        } else {
          throw new QueryException("Unknown parameter: " + key);
        }
      }
      layoutDao.update(layout);
      return getOverlayById(token, layout.getModel().getProject().getProjectId(), overlayId);
    } catch (NumberFormatException e) {
      throw new ObjectNotFoundException("overlay doesn't exist");
    }
  }

  private boolean parseBoolean(Object value) {
    if (value instanceof Boolean) {
      return (Boolean) value;
    } else {
      return "true".equalsIgnoreCase((String) value);
    }
  }

  private int parseInteger(Object value) {
    if (value instanceof Integer) {
      return (int) value;
    } else if (value instanceof String) {
      return Integer.parseInt((String) value);
    } else {
      throw new InvalidArgumentException("Invalid class of object: " + value);
    }
  }

  public Map<String, Object> removeOverlay(String token, String projectId, String overlayId)
      throws QueryException, SecurityException, IOException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    try {
      Integer id = Integer.valueOf(overlayId);
      Layout layout = layoutService.getLayoutById(id, token);
      if (layout == null) {
        throw new ObjectNotFoundException("Overlay doesn't exist");
      }
      if (layoutService.userCanRemoveLayout(layout, token)) {
        layoutService.removeLayout(layout, null);
        List<Layout> overlays = layoutService.getCustomLayouts(model, token, false,
            getUserService().getUserByToken(token));
        overlays.sort(Layout.ORDER_COMPARATOR);
        for (int i = 0; i < overlays.size(); i++) {
          Layout overlay = overlays.get(i);
          if (overlay.getOrderIndex() != i + 1) {
            overlay.setOrderIndex(i + 1);
            layoutService.updateLayout(overlay);
          }
        }
        return okStatus();
      } else {
        throw new SecurityException("You cannot remove given overlay");
      }
    } catch (NumberFormatException e) {
      throw new QueryException("Invalid overlay id: " + overlayId);
    }
  }

  /**
   * @return the layoutDao
   * @see #layoutDao
   */
  public LayoutDao getLayoutDao() {
    return layoutDao;
  }

  /**
   * @param layoutDao
   *          the layoutDao to set
   * @see #layoutDao
   */
  public void setLayoutDao(LayoutDao layoutDao) {
    this.layoutDao = layoutDao;
  }

  public Map<String, Object> addOverlay(String token, String projectId, String name, String description, String content,
      String fileId, String filename, String type, String googleLicenseConsent) throws SecurityException, QueryException, IOException {
    User user = getUserService().getUserByToken(token);
    if (Configuration.ANONYMOUS_LOGIN.equals(user.getLogin())) {
      throw new SecurityException("You have no privileges to add overlay");
    }
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    ColorSchemaType colorSchemaType = ColorSchemaType.GENERIC;
    if (type != null && !type.equals("")) {
      try {
        colorSchemaType = ColorSchemaType.valueOf(type);
      } catch (IllegalArgumentException e) {
        throw new QueryException("Invalid type of overlay: " + type, e);
      }
    }
    if (content.isEmpty() && fileId.isEmpty()) {
      throw new QueryException("Either content or fileId must be provided");
    }

    try {
      InputStream stream = null;
      if (!content.isEmpty()) {
        stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
      } else {
        try {
          int id = Integer.valueOf(fileId);
          UploadedFileEntry file = uploadedFileEntryDao.getById(id);
          if (file == null) {
            throw new QueryException("Invalid file id: " + fileId);
          }
          if (file.getOwner() == null || !file.getOwner().getLogin().equals(user.getLogin())) {
            throw new SecurityException("Access denied to source file");
          }
          stream = new ByteArrayInputStream(file.getFileContent());
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid fileId: " + fileId);
        }
      }

      Layout layout = layoutService.createLayout(new CreateLayoutParams().async(false).colorInputStream(stream)
          .description(description).layoutFileName(filename).model(model).name(name).user(user)
          .colorSchemaType(colorSchemaType).directory(".").googleLicenseConsent(googleLicenseConsent.equalsIgnoreCase("true")));

      int count = layoutService.getCustomLayouts(model, token, false, user).size();
      layout.setOrderIndex(count);
      layoutService.updateLayout(layout);

      return getOverlayById(token, projectId, layout.getId() + "");
    } catch (InvalidColorSchemaException e) {
      throw new QueryException(e.getMessage(), e);
    }

  }

  public Map<String, Object> getOverlayElement(String token, String projectId, Integer modelId, Integer overlayId,
      Integer elementId, String elementType, String columns) throws QueryException, SecurityException {
    Model topModel = getModelService().getLastModelByProjectId(projectId, token);
    if (topModel == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Model model = topModel.getSubmodelById(modelId);

    String columnSet[];
    if (columns != null && !columns.trim().isEmpty()) {
      columnSet = columns.split(",");
    } else {
      columnSet = new String[] { "modelId", "idObject", "value", "color", "description", "type", "geneVariations",
          "uniqueId" };
    }
    Map<String, Object> result = new TreeMap<>();
    if (ElementIdentifierType.ALIAS.getJsName().equals(elementType)) {
      Pair<? extends BioEntity, ColorSchema> elementDataOverlay = layoutService.getFullAliasForLayout(model, elementId,
          overlayId, token);
      result.put("type", ElementIdentifierType.ALIAS);
      result.put("overlayContent", overlayContentToMap(elementDataOverlay, columnSet));
      return result;
    } else if (ElementIdentifierType.REACTION.getJsName().equals(elementType)) {
      Pair<? extends BioEntity, ColorSchema> reactionDataOverlay = layoutService.getFullReactionForLayout(model,
          elementId, overlayId, token);
      result.put("type", ElementIdentifierType.REACTION);
      result.put("overlayContent", overlayContentToMap(reactionDataOverlay, columnSet));
      return result;
    } else {
      throw new QueryException("Unknown element type: " + elementType);
    }
  }

  private Map<String, Object> overlayContentToMap(Pair<? extends BioEntity, ColorSchema> bioEntityDataOverlay,
      String[] columns) {
    Map<String, Object> result = new LinkedHashMap<>();
    BioEntity bioEntity = bioEntityDataOverlay.getLeft();
    ColorSchema colorSchema = bioEntityDataOverlay.getRight();
    for (String string : columns) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject") || column.equals("uniqueid")) {
        // casting to string is only to provide the same results as before refactoring
        value = bioEntity.getId() + "";
      } else if (column.equals("modelid")) {
        value = bioEntity.getModel().getId();
      } else if (column.equals("value")) {
        value = colorSchema.getValue();
      } else if (column.equals("color")) {
        if (colorSchema.getColor() != null) {
          value = colorParser.colorToMap(colorSchema.getColor());
        }
      } else if (column.equals("description")) {
        value = colorSchema.getDescription();
      } else if (column.equals("type")) {
        if (colorSchema instanceof GeneVariationColorSchema) {
          value = ColorSchemaType.GENETIC_VARIANT;
        } else {
          value = ColorSchemaType.GENERIC;
        }
      } else if (column.equals("genevariations")) {
        if (colorSchema instanceof GeneVariationColorSchema) {
          value = ((GeneVariationColorSchema) colorSchema).getGeneVariations();
        } else {
          value = new Object[] {};
        }
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

}
