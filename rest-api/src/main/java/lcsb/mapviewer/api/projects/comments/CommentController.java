package lcsb.mapviewer.api.projects.comments;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ElementIdentifierType;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class CommentController extends BaseController {

  @Autowired
  private CommentRestImpl commentController;

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getComments(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "columns", defaultValue = "") String columns, //
      @RequestParam(value = "removed", defaultValue = "") String removed //
  ) throws SecurityException, QueryException {
    return commentController.getCommentList(token, projectId, columns, "", "", removed);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/{commentId}/", method = { RequestMethod.DELETE }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> removeComment(//
      @RequestBody(required = false) String body, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "commentId") String commentId //
  ) throws SecurityException, QueryException, JsonParseException, JsonMappingException, IOException {
    Map<String, Object> node = parseBody(body);
    String reason = (String) node.get("reason");
    return commentController.removeComment(token, projectId, commentId, reason);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/bioEntities/reactions/{reactionId}", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getCommentsByReaction(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "columns", defaultValue = "") String columns, //
      @PathVariable(value = "reactionId") String reactionId, //
      @RequestParam(value = "removed", defaultValue = "") String removed //
  ) throws SecurityException, QueryException {
    return commentController.getCommentList(token, projectId, columns, reactionId,
        ElementIdentifierType.REACTION.getJsName(), removed);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/bioEntities/elements/{elementId}", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getCommentsByElement(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "columns", defaultValue = "") String columns, //
      @PathVariable(value = "elementId") String elementId, //
      @RequestParam(value = "removed", defaultValue = "") String removed //
  ) throws SecurityException, QueryException {
    return commentController.getCommentList(token, projectId, columns, elementId,
        ElementIdentifierType.ALIAS.getJsName(), removed);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/points/{coordinates:.+}", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getCommentsByPoint(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "columns", defaultValue = "") String columns, //
      @PathVariable(value = "coordinates") String coordinates, //
      @RequestParam(value = "removed", defaultValue = "") String removed //
  ) throws SecurityException, QueryException {
    return commentController.getCommentList(token, projectId, columns, coordinates,
        ElementIdentifierType.POINT.getJsName(), removed);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/bioEntities/elements/{elementId}", method = {
      RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addCommentForElement(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "elementId") String elementId, //
      @RequestParam(value = "name") String name, //
      @RequestParam(value = "email") String email, //
      @RequestParam(value = "content") String content, //
      @RequestParam(value = "pinned", defaultValue = "true") String pinned, //
      @RequestParam(value = "coordinates") String coordinates, //
      @PathVariable(value = "modelId") String modelId //
  ) throws SecurityException, QueryException {
    Point2D pointCoordinates = parseCoordinates(coordinates);
    return commentController.addComment(token, projectId, ElementIdentifierType.ALIAS.getJsName(), elementId, name,
        email, content, pinned.toLowerCase().equals("true"), pointCoordinates, modelId);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/bioEntities/reactions/{reactionId}", method = {
      RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addCommentForReaction(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "reactionId") String reactionId, //
      @RequestParam(value = "name") String name, //
      @RequestParam(value = "email") String email, //
      @RequestParam(value = "content") String content, //
      @RequestParam(value = "pinned", defaultValue = "true") String pinned, //
      @RequestParam(value = "coordinates") String coordinates, //
      @PathVariable(value = "modelId") String modelId //
  ) throws SecurityException, QueryException {
    Point2D pointCoordinates = parseCoordinates(coordinates);
    return commentController.addComment(token, projectId, ElementIdentifierType.REACTION.getJsName(), reactionId, name,
        email, content, pinned.toLowerCase().equals("true"), pointCoordinates, modelId);
  }

  @RequestMapping(value = "/projects/{projectId}/comments/models/{modelId}/points/{coordinates}", method = {
      RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addCommentForPoint(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "name") String name, //
      @RequestParam(value = "email") String email, //
      @RequestParam(value = "content") String content, //
      @RequestParam(value = "pinned", defaultValue = "true") String pinned, //
      @PathVariable(value = "coordinates") String coordinates, //
      @PathVariable(value = "modelId") String modelId //
  ) throws SecurityException, QueryException {
    Point2D pointCoordinates = parseCoordinates(coordinates);
    return commentController.addComment(token, projectId, ElementIdentifierType.POINT.getJsName(), coordinates, name,
        email, content, pinned.toLowerCase().equals("true"), pointCoordinates, modelId);
  }

  private Point2D parseCoordinates(String coordinates) throws QueryException {
    String[] tmp = coordinates.split(",");
    if (tmp.length != 2) {
      throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'");
    }
    Double x = null;
    Double y = null;
    try {
      x = Double.valueOf(tmp[0]);
      y = Double.valueOf(tmp[1]);
    } catch (NumberFormatException e) {
      throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'", e);
    }
    Point2D pointCoordinates = new Point2D.Double(x, y);
    return pointCoordinates;
  }

  /**
   * @return the overlayController
   * @see #commentController
   */
  public CommentRestImpl getOverlayController() {
    return commentController;
  }

  /**
   * @param overlayController
   *          the overlayController to set
   * @see #commentController
   */
  public void setOverlayController(CommentRestImpl overlayController) {
    this.commentController = overlayController;
  }

}