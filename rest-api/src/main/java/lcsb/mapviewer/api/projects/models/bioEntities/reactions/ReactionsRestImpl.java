package lcsb.mapviewer.api.projects.models.bioEntities.reactions;

import java.awt.geom.Line2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.UserAccessException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;

@Transactional(value = "txManager")
public class ReactionsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ReactionsRestImpl.class);

  @Autowired
  private IUserService userService;

  @Autowired
  private IModelService modelService;

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService
   *          the userService to set
   * @see #userService
   */
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  public List<Map<String, Object>> getReactions(String projectId, String id, String columns, String modelId,
      String token, String participantElementId) throws UserAccessException, SecurityException, QueryException {
    Set<Integer> ids = new HashSet<>();
    if (!id.equals("")) {
      for (String str : id.split(",")) {
        ids.add(Integer.valueOf(str));
      }
    }
    List<Model> models = getModels(projectId, modelId, token);

    Set<Element> elementSet = new HashSet<>();
    if (!participantElementId.equals("")) {
      for (String str : participantElementId.split(",")) {
        for (Model model : models) {
          elementSet.add(model.getElementByDbId(Integer.valueOf(str)));
        }
      }
    }
    Set<String> columnsSet = createReactionColumnSet(columns);

    List<Reaction> elements = new ArrayList<>();

    for (Model model2 : models) {
      for (Reaction reaction : model2.getReactions()) {
        if (ids.size() == 0 || ids.contains(reaction.getId())) {
          if (elementSet.size() == 0 || reactionContainsElement(reaction, elementSet)) {
            elements.add(reaction);
          }
        }
      }
    }
    elements.sort(BioEntity.ID_COMPARATOR);
    List<Map<String, Object>> result = new ArrayList<>();
    for (Reaction element : elements) {
      result.add(preparedReaction(element, columnsSet));

    }
    return result;
  }

  private boolean reactionContainsElement(Reaction reaction, Set<Element> elementSet) {
    for (Element element : elementSet) {
      if (reaction.containsElement(element)) {
        return true;
      }
    }
    return false;
  }

  private Map<String, Object> preparedReaction(Reaction reaction, Set<String> columnsSet) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = reaction.getId();
      } else if (column.equals("modelid")) {
        value = reaction.getModelData().getId();
      } else if (column.equals("reactionid")) {
        value = reaction.getIdReaction();
      } else if (column.equals("name")) {
        value = reaction.getName();
      } else if (column.equals("centerpoint")) {
        value = reaction.getCenterPoint();
      } else if (column.equals("products")) {
        List<Map<String, Object>> ids = new ArrayList<>();
        for (Product product : reaction.getProducts()) {
          ids.add(createReactionNode(product));
        }
        value = ids;
      } else if (column.equals("reactants")) {
        List<Map<String, Object>> ids = new ArrayList<>();
        for (Reactant reactant : reaction.getReactants()) {
          ids.add(createReactionNode(reactant));
        }
        value = ids;
      } else if (column.equals("modifiers")) {
        List<Map<String, Object>> ids = new ArrayList<>();
        for (Modifier modifier : reaction.getModifiers()) {
          ids.add(createReactionNode(modifier));
        }
        value = ids;
      } else if (column.equals("type")) {
        value = reaction.getStringType();
      } else if (column.equals("hierarchyvisibilitylevel")) {
        value = reaction.getVisibilityLevel();
      } else if (column.equals("lines")) {
        value = getLines(reaction);
      } else if (column.equals("notes")) {
        value = reaction.getNotes();
      } else if (column.equals("kineticlaw")) {
        value = kineticsToMap(reaction.getKinetics());
      } else if (column.equals("references")) {
        value = createAnnotations(reaction.getMiriamData());
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  private List<Map<String, Object>> getLines(Reaction reaction) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (Reactant reactant : reaction.getReactants()) {
      result.addAll(getLines(reactant));
    }
    for (Product product : reaction.getProducts()) {
      result.addAll(getLines(product));
    }
    for (Modifier modifier : reaction.getModifiers()) {
      result.addAll(getLines(modifier));
    }
    for (NodeOperator operator : reaction.getOperators()) {
      result.addAll(getLines(operator));
    }
    return result;
  }

  protected List<Map<String, Object>> getLines(AbstractNode reactant) {
    // order in this method is to keep the order from previous version, it can be
    // changed in the future, but I wanted to make that it's giving the same results
    // when refactoring
    List<Map<String, Object>> result = new ArrayList<>();
    List<Line2D> startLines = new ArrayList<>();
    List<Line2D> middleLines = new ArrayList<>();
    List<Line2D> endLines = new ArrayList<>();

    List<Line2D> lines = reactant.getLine().getLines();

    if (reactant instanceof Reactant) {
      startLines.add(lines.get(0));
      for (int i = 1; i < lines.size(); i++) {
        middleLines.add(lines.get(i));
      }
    } else if (reactant instanceof Product) {
      endLines.add(lines.get(lines.size() - 1));
      for (int i = 0; i < lines.size()-1; i++) {
        middleLines.add(lines.get(i));
      }
    } else if (reactant instanceof Modifier) {
      middleLines.add(lines.get(lines.size() - 1));
      for (int i = 0; i < lines.size()-1; i++) {
        middleLines.add(lines.get(i));
      }
    } else {
      middleLines.addAll(lines);
    }

    for (Line2D line2d : startLines) {
      result.add(lineToMap(line2d, "START"));
    }
    for (Line2D line2d : endLines) {
      result.add(lineToMap(line2d, "END"));
    }
    for (Line2D line2d : middleLines) {
      result.add(lineToMap(line2d, "MIDDLE"));
    }

    return result;
  }

  private Map<String, Object> lineToMap(Line2D line2d, String type) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("start", line2d.getP1());
    result.put("end", line2d.getP2());
    result.put("type", type);
    return result;
  }

  private Map<String, Object> createReactionNode(ReactionNode node) {
    Map<String, Object> result = new TreeMap<>();
    result.put("aliasId", node.getElement().getId());
    result.put("stoichiometry", node.getStoichiometry());
    return result;
  }

  private Map<String, Object> kineticsToMap(SbmlKinetics kinetics) {
    if (kinetics == null) {
      return null;
    }
    Map<String, Object> result = new TreeMap<>();
    result.put("definition", kinetics.getDefinition());
    try {
      result.put("mathMlPresentation", super.mathMLToPresentationML(kinetics.getDefinition()));
    } catch (IOException | InvalidXmlSchemaException | TransformerException e) {
      logger.error("Problems with transforming kinetics", e);
    }

    List<Integer> functionIds = new ArrayList<>();
    for (SbmlFunction function : kinetics.getFunctions()) {
      functionIds.add(function.getId());
    }
    result.put("functionIds", functionIds);
    List<Integer> parameterIds = new ArrayList<>();
    for (SbmlParameter parameter : kinetics.getParameters()) {
      parameterIds.add(parameter.getId());
    }
    result.put("parameterIds", parameterIds);
    return result;
  }

  private Set<String> createReactionColumnSet(String columns) {
    Set<String> columnsSet = new HashSet<>();
    if (columns.equals("")) {
      columnsSet.add("id");
      columnsSet.add("reactionId");
      columnsSet.add("modelId");
      columnsSet.add("type");
      columnsSet.add("lines");
      columnsSet.add("kineticLaw");
      columnsSet.add("centerPoint");
      columnsSet.add("products");
      columnsSet.add("reactants");
      columnsSet.add("modifiers");
      columnsSet.add("hierarchyVisibilityLevel");
      columnsSet.add("references");
      columnsSet.add("notes");
    } else {
      for (String str : columns.split(",")) {
        columnsSet.add(str);
      }
    }
    return columnsSet;
  }

  /**
   * @return the modelService
   * @see #modelService
   */
  public IModelService getModelService() {
    return modelService;
  }

  /**
   * @param modelService
   *          the modelService to set
   * @see #modelService
   */
  public void setModelService(IModelService modelService) {
    this.modelService = modelService;
  }

}
