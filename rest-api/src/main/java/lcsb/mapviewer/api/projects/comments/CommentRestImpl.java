package lcsb.mapviewer.api.projects.comments;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ElementIdentifierType;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.persist.dao.map.ReactionDao;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.overlay.IconManager;

@Transactional(value = "txManager")
public class CommentRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(CommentRestImpl.class);

  @Autowired
  private ICommentService commentService;

  @Autowired
  private ReactionDao reactionDao;

  @Autowired
  private ElementDao elementDao;

  public List<Map<String, Object>> getCommentList(String token, String projectId, String columns, String elementId,
      String elementType, String removed) throws SecurityException, QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    boolean isAdmin = getUserService().userHasPrivilege(token, PrivilegeType.EDIT_COMMENTS_PROJECT, project);
    Set<String> columnsSet = createCommentColumnSet(columns, isAdmin);

    List<Map<String, Object>> result = new ArrayList<>();

    List<Comment> comments = commentService.getCommentsByProject(project, token);
    for (Comment comment : comments) {
      boolean reject = false;
      if (!"".equals(elementType)) {
        if (elementType.equals(ElementIdentifierType.POINT.getJsName())) {
          reject = comment.getTableName() != null;
        } else if (elementType.equals(ElementIdentifierType.REACTION.getJsName())) {
          reject = comment.getTableName() == null || !comment.getTableName().getName().contains("Reaction");
        } else if (elementType.equals(ElementIdentifierType.ALIAS.getJsName())) {
          reject = comment.getTableName() == null || comment.getTableName().getName().contains("Reaction");
        } else {
          throw new QueryException("Unknown element type: " + elementType);
        }
      }
      if (!"".equals(elementId)) {
        Object id = getId(comment).toString();
        reject |= (!elementId.equals(id));
      }
      if (!"".equals(removed)) {
        boolean expectedRemoved = removed.equalsIgnoreCase("true");
        reject |= (comment.isDeleted() != expectedRemoved);
      }
      if (!isAdmin) {
        reject |= (!comment.isPinned());
      }
      if (!reject) {
        result.add(preparedComment(comment, columnsSet, isAdmin));
      }
    }

    return result;
  }

  /**
   * @return the commentService
   * @see #commentService
   */
  public ICommentService getCommentService() {
    return commentService;
  }

  /**
   * @param commentService
   *          the commentService to set
   * @see #commentService
   */
  public void setCommentService(ICommentService commentService) {
    this.commentService = commentService;
  }

  private Map<String, Object> preparedComment(Comment comment, Set<String> columnsSet, boolean admin) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = comment.getId();
      } else if (column.equals("elementid")) {
        value = getId(comment);
      } else if (column.equals("modelid")) {
        value = comment.getSubmodelData().getId();
      } else if (column.equals("title")) {
        value = getTitle(comment);
      } else if (column.equals("pinned")) {
        value = comment.isPinned();
      } else if (column.equals("content")) {
        value = comment.getContent();
      } else if (column.equals("removed")) {
        value = comment.isDeleted();
      } else if (column.equals("coord")) {
        value = getCoordinates(comment);
      } else if (column.equals("removereason")) {
        value = comment.getRemoveReason();
      } else if (column.equals("type")) {
        value = getType(comment);
      } else if (column.equals("icon")) {
        value = IconManager.getInstance().getCommentIcon();
      } else if (admin) {
        if (column.equals("author")) {
          value = comment.getName();
        } else if (column.equals("email")) {
          value = comment.getEmail();
        } else {
          value = "Unknown column";
        }
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  private Object getId(Comment comment) {
    if (comment.getTableName() == null) {
      return String.format("%.2f", comment.getCoordinates().getX()) + ","
          + String.format("%.2f", comment.getCoordinates().getY());
    } else {
      return comment.getTableId();
    }
  }

  private Object getType(Comment comment) {
    if (comment.getTableName() != null) {
      if (comment.getTableName().getName().contains("Reaction")) {
        return ElementIdentifierType.REACTION;
      } else {
        return ElementIdentifierType.ALIAS;
      }
    } else {
      return ElementIdentifierType.POINT;
    }
  }

  private String getTitle(Comment comment) {
    String title = "";
    if (comment.getCoordinates() != null) {
      title = "Comment (coord: " + String.format("%.2f", comment.getCoordinates().getX()) + ", "
          + String.format("%.2f", comment.getCoordinates().getY()) + ")";
    }
    if (comment.getTableName() != null) {
      if (comment.getTableName().getName().contains("Reaction")) {
        Reaction reaction = reactionDao.getById(comment.getTableId());
        if (reaction != null) {
          title = "Reaction " + reaction.getIdReaction();
        } else {
          logger.warn("Invalid reaction dbID: " + comment.getTableId());
        }

      } else {
        Element alias = elementDao.getById(comment.getTableId());
        if (alias != null) {
          title = alias.getName();
        } else {
          logger.warn("Invalid alias dbID: " + comment.getTableId());
        }
      }
    }
    return title;
  }

  private Point2D getCoordinates(Comment comment) {
    Point2D coordinates = comment.getCoordinates();
    if (comment.getCoordinates() != null) {
      coordinates = comment.getCoordinates();
    }
    if (comment.getTableName() != null) {
      if (comment.getTableName().getName().contains("Reaction")) {
        Reaction reaction = reactionDao.getById(comment.getTableId());
        if (reaction != null) {
          coordinates = reaction.getCenterPoint();
        } else {
          logger.warn("Invalid reaction dbID: " + comment.getTableId());
        }

      } else {
        Element alias = elementDao.getById(comment.getTableId());
        if (alias != null) {
          coordinates = alias.getCenter();
        } else {
          logger.warn("Invalid alias dbID: " + comment.getTableId());
        }
      }
    }
    return coordinates;
  }

  private Set<String> createCommentColumnSet(String columns, boolean admin) {
    Set<String> columnsSet = new HashSet<>();
    if (columns.equals("")) {
      columnsSet.add("title");
      columnsSet.add("icon");
      columnsSet.add("type");
      columnsSet.add("content");
      columnsSet.add("removed");
      columnsSet.add("coord");
      columnsSet.add("modelId");
      columnsSet.add("elementId");
      columnsSet.add("id");
      columnsSet.add("pinned");
      if (admin) {
        columnsSet.add("author");
        columnsSet.add("email");
        columnsSet.add("removeReason");
      }
    } else {
      for (String str : columns.split(",")) {
        columnsSet.add(str);
      }
    }
    return columnsSet;
  }

  /**
   * @return the reactionDao
   * @see #reactionDao
   */
  public ReactionDao getReactionDao() {
    return reactionDao;
  }

  /**
   * @param reactionDao
   *          the reactionDao to set
   * @see #reactionDao
   */
  public void setReactionDao(ReactionDao reactionDao) {
    this.reactionDao = reactionDao;
  }

  /**
   * @return the elementDao
   * @see #elementDao
   */
  public ElementDao getElementDao() {
    return elementDao;
  }

  /**
   * @param elementDao
   *          the elementDao to set
   * @see #elementDao
   */
  public void setElementDao(ElementDao elementDao) {
    this.elementDao = elementDao;
  }

  public Map<String, Object> addComment(String token, String projectId, String elementType, String elementId,
      String name, String email, String content, boolean pinned, Point2D pointCoordinates, String submodelId)
      throws QueryException, SecurityException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Integer modelId = null;
    try {
      modelId = Integer.valueOf(submodelId);
    } catch (NumberFormatException e) {
      throw new QueryException("Invalid model identifier for given project", e);
    }
    Model submodel = model.getSubmodelById(modelId);
    if (submodel == null) {
      throw new QueryException("Invalid model identifier for given project");
    }
    Object commentedObject;
    if (elementType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      commentedObject = submodel.getElementByDbId(Integer.valueOf(elementId));
      if (commentedObject == null) {
        throw new QueryException("Invalid commented element identifier for given project");
      }
    } else if (elementType.equals(ElementIdentifierType.REACTION.getJsName())) {
      commentedObject = submodel.getReactionByDbId(Integer.valueOf(elementId));
      if (commentedObject == null) {
        throw new QueryException("Invalid commented element identifier for given project");
      }
    } else if (elementType.equals(ElementIdentifierType.POINT.getJsName())) {
      commentedObject = null;
    } else {
      throw new QueryException("Unknown type of commented object: " + elementType);
    }

    Comment comment = commentService.addComment(name, email, content, model, pointCoordinates, commentedObject, pinned,
        submodel);

    Project project = model.getProject();
    boolean isAdmin = getUserService().userHasPrivilege(token, PrivilegeType.EDIT_COMMENTS_PROJECT, project);
    return preparedComment(comment, createCommentColumnSet("", isAdmin), isAdmin);
  }

  public Map<String, Object> removeComment(String token, String projectId, String commentId, String reason)
      throws SecurityException, QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    Comment comment = commentService.getCommentById(commentId);
    if (comment == null || comment.getModelData().getProject().getId() != project.getId()) {
      throw new ObjectNotFoundException("Comment with given id doesn't exist");
    }

    commentService.deleteComment(comment, token, reason);
    return okStatus();
  }

}
