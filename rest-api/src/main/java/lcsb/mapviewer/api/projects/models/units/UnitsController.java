package lcsb.mapviewer.api.projects.models.units;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class UnitsController extends BaseController {
  @Autowired
  private UnitsRestImpl unitController;

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/units/{unitId}", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getUnit(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "unitId") String unitId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws QueryException, SecurityException {
    return unitController.getUnit(projectId, modelId, token, unitId);
  }

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/units/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getUnits(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws QueryException, SecurityException {
    return unitController.getUnits(projectId, modelId, token);
  }

}