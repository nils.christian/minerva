package lcsb.mapviewer.api.projects.models.publications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.Article;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.ISearchService;

@Transactional(value = "txManager")
public class PublicationsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(PublicationsRestImpl.class);

  @Autowired
  private ISearchService searchService;

  @Autowired
  private PubmedParser pubmedParser;

  public SortedMap<MiriamData, List<BioEntity>> getPublications(Collection<Model> models) {
    SortedMap<MiriamData, List<BioEntity>> publications = new TreeMap<>();

    List<BioEntity> bioEntities = new ArrayList<>();
    for (Model modelData : models) {
      bioEntities.addAll(modelData.getBioEntities());
    }
    bioEntities.sort(BioEntity.ID_COMPARATOR);
    for (BioEntity bioEntity : bioEntities) {
      for (MiriamData md : bioEntity.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.PUBMED)) {
          List<BioEntity> list = publications.get(md);
          if (list == null) {
            list = new ArrayList<>();
            publications.put(md, list);
          }
          list.add(bioEntity);
        }
      }

    }
    return publications;
  }

  /**
   * @return the searchService
   * @see #searchService
   */
  public ISearchService getSearchService() {
    return searchService;
  }

  /**
   * @param searchService
   *          the searchService to set
   * @see #searchService
   */
  public void setSearchService(ISearchService searchService) {
    this.searchService = searchService;
  }

  private enum SortColumn {
    PUBMED_ID("pubmedId"), //
    YEAR("year"), //
    JOURNAL("journal"), //
    TITLE("title"), //
    AUTHORS("authors");

    private String commonName;

    SortColumn(String commonName) {
      this.commonName = commonName;
    }
  }

  public Map<String, Object> getPublications(String projectId, String modelId, String token, String startString,
      Integer length, String sortColumn, String sortOrder, String search) throws SecurityException, QueryException {
    List<Model> models = getModels(projectId, modelId, token);

    SortColumn sortColumnEnum = getSortOrderColumn(sortColumn);
    Comparator<Map.Entry<MiriamData, List<BioEntity>>> comparator = getComparatorForColumn(sortColumnEnum, sortOrder);

    Integer start = Math.max(0, Integer.valueOf(startString));
    List<Map<String, Object>> resultList = new ArrayList<>();

    SortedMap<MiriamData, List<BioEntity>> publications = getPublications(models);
    Set<String> columns = new HashSet<>();
    columns.add("id");
    columns.add("type");
    columns.add("modelid");

    List<Map.Entry<MiriamData, List<BioEntity>>> filteredList = new ArrayList<>();

    for (Map.Entry<MiriamData, List<BioEntity>> entry : publications.entrySet()) {
      if (isSearchResult(entry.getKey(), search)) {
        filteredList.add(entry);
      }
    }
    if (comparator != null) {
      filteredList.sort(comparator);
    }

    int index = 0;
    for (Map.Entry<MiriamData, List<BioEntity>> entry : filteredList) {
      if (index >= start && index < start + length) {
        List<Object> elements = new ArrayList<>();
        for (BioEntity object : entry.getValue()) {
          elements.add(createMinifiedSearchResult(object));
        }

        Map<String, Object> row = new TreeMap<>();
        row.put("elements", elements);
        row.put("publication", createAnnotation(entry.getKey()));
        resultList.add(row);
      }
      index++;
    }

    Map<String, Object> result = new TreeMap<>();
    result.put("data", resultList);
    result.put("totalSize", publications.size());
    result.put("filteredSize", filteredList.size());
    result.put("start", start);
    result.put("length", resultList.size());
    return result;
  }

  private Comparator<Entry<MiriamData, List<BioEntity>>> getComparatorForColumn(SortColumn sortColumnEnum,
      String sortOrder) {
    final int orderFactor;
    if (sortOrder.toLowerCase().equals("desc")) {
      orderFactor = -1;
    } else {
      orderFactor = 1;
    }
    if (sortColumnEnum == null) {
      return null;
    } else if (sortColumnEnum.equals(SortColumn.PUBMED_ID)) {
      return new Comparator<Map.Entry<MiriamData, List<BioEntity>>>() {
        @Override
        public int compare(Entry<MiriamData, List<BioEntity>> o1, Entry<MiriamData, List<BioEntity>> o2) {
          Integer id1 = Integer.valueOf(o1.getKey().getResource());
          Integer id2 = Integer.valueOf(o2.getKey().getResource());
          return id1.compareTo(id2) * orderFactor;

        }
      };
    } else if (sortColumnEnum.equals(SortColumn.YEAR)) {
      return new Comparator<Map.Entry<MiriamData, List<BioEntity>>>() {
        @Override
        public int compare(Entry<MiriamData, List<BioEntity>> o1, Entry<MiriamData, List<BioEntity>> o2) {
          try {
            Article article1 = getArticle(o1.getKey().getResource());
            Article article2 = getArticle(o2.getKey().getResource());
            return article1.getYear().compareTo(article2.getYear()) * orderFactor;
          } catch (Exception e) {
            logger.error("Problem with accessing article data ", e);
            return 0;
          }
        }
      };
    } else if (sortColumnEnum.equals(SortColumn.JOURNAL)) {
      return new Comparator<Map.Entry<MiriamData, List<BioEntity>>>() {
        @Override
        public int compare(Entry<MiriamData, List<BioEntity>> o1, Entry<MiriamData, List<BioEntity>> o2) {
          try {
            Article article1 = getArticle(o1.getKey().getResource());
            Article article2 = getArticle(o2.getKey().getResource());
            return article1.getJournal().compareTo(article2.getJournal()) * orderFactor;
          } catch (Exception e) {
            logger.error("Problem with accessing article data ", e);
            return 0;
          }
        }
      };
    } else if (sortColumnEnum.equals(SortColumn.TITLE)) {
      return new Comparator<Map.Entry<MiriamData, List<BioEntity>>>() {
        @Override
        public int compare(Entry<MiriamData, List<BioEntity>> o1, Entry<MiriamData, List<BioEntity>> o2) {
          try {
            Article article1 = getArticle(o1.getKey().getResource());
            Article article2 = getArticle(o2.getKey().getResource());
            return article1.getTitle().compareTo(article2.getTitle()) * orderFactor;
          } catch (Exception e) {
            logger.error("Problem with accessing article data ", e);
            return 0;
          }

        }
      };
    } else if (sortColumnEnum.equals(SortColumn.AUTHORS)) {
      return new Comparator<Map.Entry<MiriamData, List<BioEntity>>>() {
        @Override
        public int compare(Entry<MiriamData, List<BioEntity>> o1, Entry<MiriamData, List<BioEntity>> o2) {
          try {
            Article article1 = getArticle(o1.getKey().getResource());
            Article article2 = getArticle(o2.getKey().getResource());
            return article1.getStringAuthors().compareTo(article2.getStringAuthors()) * orderFactor;
          } catch (Exception e) {
            logger.error("Problem with accessing article data ", e);
            return 0;
          }
        }
      };
    } else {
      throw new InvalidArgumentException("Unknown column type: " + sortColumnEnum);
    }
  }

  private SortColumn getSortOrderColumn(String sortColumn) throws QueryException {
    if (!sortColumn.isEmpty()) {
      for (SortColumn type : SortColumn.values()) {
        if (type.commonName.toLowerCase().equals(sortColumn.toLowerCase())) {
          return type;
        }
      }
      throw new QueryException("Unknown sortColumn: " + sortColumn);
    }
    return null;
  }

  private boolean isSearchResult(MiriamData key, String search) {
    String lowerCaseSearch = search.toLowerCase();
    if (search == null || search.isEmpty()) {
      return true;
    }
    Article article = null;
    if (MiriamType.PUBMED.equals(key.getDataType())) {
      try {
        article = pubmedParser.getPubmedArticleById(Integer.valueOf(key.getResource()));
      } catch (PubmedSearchException e) {
        logger.error("Problem with accessing info about pubmed", e);
      }
    }
    if (article != null) {
      if (article.getId().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getJournal().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getStringAuthors().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getTitle().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      } else if (article.getYear().toString().toLowerCase().contains(lowerCaseSearch)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return the pubmedParser
   * @see #pubmedParser
   */
  public PubmedParser getPubmedParser() {
    return pubmedParser;
  }

  /**
   * @param pubmedParser
   *          the pubmedParser to set
   * @see #pubmedParser
   */
  public void setPubmedParser(PubmedParser pubmedParser) {
    this.pubmedParser = pubmedParser;
  }

  private Article getArticle(String resource) throws NumberFormatException, PubmedSearchException {
    return pubmedParser.getPubmedArticleById(Integer.valueOf(resource));
  }

}
