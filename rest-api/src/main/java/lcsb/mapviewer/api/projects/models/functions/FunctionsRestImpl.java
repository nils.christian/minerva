package lcsb.mapviewer.api.projects.models.functions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;

import org.apache.axis.utils.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.SecurityException;

@Transactional(value = "txManager")
public class FunctionsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(FunctionsRestImpl.class);

  public Map<String, Object> getFunction(String projectId, String modelId, String token, String functionId)
      throws SecurityException, QueryException {
    List<Model> models = getModels(projectId, modelId, token);
    int id = Integer.valueOf(functionId);
    for (Model model : models) {
      for (SbmlFunction function : model.getFunctions()) {
        if (function.getId() == id) {
          return functionToMap(function);
        }
      }
    }
    throw new ObjectNotFoundException("Function with given id doesn't exist");
  }

  private Map<String, Object> functionToMap(SbmlFunction function) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", function.getId());
    result.put("functionId", function.getFunctionId());
    result.put("name", function.getName());
    result.put("definition", function.getDefinition());
    try {
      String definition = extractLambda(function.getDefinition());
      result.put("mathMlPresentation", super.mathMLToPresentationML(definition));
    } catch (Exception e) {
      logger.error("Problems with transforming kinetics", e);
    }
    result.put("arguments", function.getArguments());
    return result;
  }

  protected String extractLambda(String definition)
      throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerException {
    String result = definition.replaceAll("<lambda>", "").replaceAll("</lambda>", "");

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    Document document = dbf.newDocumentBuilder().parse(new InputSource(new ByteArrayInputStream(result.getBytes())));

    NodeList nodes = document.getElementsByTagName("bvar");
    for (int i = nodes.getLength() - 1; i >= 0; i--) {
      Node e = nodes.item(i);
      e.getParentNode().removeChild(e);
    }

    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer t = tf.newTransformer();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    t.transform(new DOMSource(document), new StreamResult(baos));

    return baos.toString();
  }

  public List<Map<String, Object>> getFunctions(String projectId, String modelId, String token)
      throws SecurityException, QueryException {
    List<Map<String, Object>> result = new ArrayList<>();
    List<Model> models = getModels(projectId, modelId, token);
    for (Model model : models) {
      for (SbmlFunction function : model.getFunctions()) {
        result.add(functionToMap(function));
      }
    }
    return result;
  }
}
