package lcsb.mapviewer.api.projects.mirnas;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.MiRNA;
import lcsb.mapviewer.annotation.services.MiRNAParser;
import lcsb.mapviewer.annotation.services.MiRNASearchException;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ElementIdentifierType;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.mirna.IMiRNAService;

@Transactional(value = "txManager")
public class MiRnaRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(MiRnaRestImpl.class);

  @Autowired
  private IMiRNAService miRnaService;

  @Autowired
  private MiRNAParser miRNAParser;

  public List<Map<String, Object>> getMiRnasByQuery(String token, String projectId, String columns, String query)
      throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Project project = model.getProject();

    Set<String> columnSet = createMiRnaColumnSet(columns);

    List<Map<String, Object>> result = new ArrayList<>();

    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }
    MiRNA miRna = miRnaService.getByName(query,
        new DbSearchCriteria().project(project).organisms(organism).colorSet(0));
    if (miRna != null) {
      List<Model> models = getModels(projectId, "*", token);
      result.add(prepareMiRna(miRna, columnSet, models));
    }

    return result;
  }

  private Map<String, Object> prepareMiRna(MiRNA miRna, Set<String> columnsSet, List<Model> models) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = miRna.getName();
      } else if (column.equals("name")) {
        value = miRna.getName();
      } else if (column.equals("targets")) {
        value = prepareTargets(miRna.getTargets(), models);
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  private Set<String> createMiRnaColumnSet(String columns) {
    Set<String> columnsSet = new HashSet<>();
    if (columns.equals("")) {
      columnsSet.add("name");
      columnsSet.add("id");
      columnsSet.add("targets");
    } else {
      for (String str : columns.split(",")) {
        columnsSet.add(str);
      }
    }
    return columnsSet;
  }

  /**
   * @return the miRnaService
   * @see #miRnaService
   */
  public IMiRNAService getMiRnaService() {
    return miRnaService;
  }

  /**
   * @param miRnaService
   *          the miRnaService to set
   * @see #miRnaService
   */
  public void setMiRnaService(IMiRNAService miRnaService) {
    this.miRnaService = miRnaService;
  }

  public List<Map<String, Object>> getMiRnasByTarget(String token, String projectId, String targetType, String targetId,
      String columns) throws SecurityException, QueryException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    if (model == null) {
      throw new QueryException("Project with given id doesn't exist");
    }
    Project project = model.getProject();

    List<Model> models = getModels(projectId, "*", token);

    Integer dbId = Integer.valueOf(targetId);
    List<Element> targets = new ArrayList<>();
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      Element element = null;
      for (Model m : models) {
        if (element == null) {
          element = m.getElementByDbId(dbId);
        }
      }
      if (element == null) {
        throw new QueryException("Invalid element identifier for given project");
      }
      targets.add(element);
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
    MiriamData organism = project.getOrganism();
    if (organism == null) {
      organism = TaxonomyBackend.HUMAN_TAXONOMY;
    }

    Set<String> columnSet = createMiRnaColumnSet(columns);

    List<MiRNA> miRnas = miRnaService.getForTargets(targets,
        new DbSearchCriteria().project(project).organisms(organism));

    List<Map<String, Object>> result = new ArrayList<>();

    for (MiRNA miRna : miRnas) {
      result.add(prepareMiRna(miRna, columnSet, models));
    }

    return result;
  }

  public List<String> getSuggestedQueryList(String projectId, String token)
      throws SecurityException, MiRNASearchException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    return miRNAParser.getSuggestedQueryList(project);
  }

}
