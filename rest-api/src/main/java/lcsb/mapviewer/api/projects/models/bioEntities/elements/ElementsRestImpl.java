package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.services.SecurityException;

@Transactional(value = "txManager")
public class ElementsRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ElementsRestImpl.class);

  public List<Map<String, Object>> getElements(String projectId, String id, String columns, String modelId,
      String token, String type, String includedCompartmentIds, String excludedCompartmentIds)
      throws QueryException, SecurityException {
    Set<Integer> ids = new HashSet<>();
    if (!id.equals("")) {
      for (String str : id.split(",")) {
        ids.add(Integer.valueOf(str));
      }
    }
    Set<String> types = new HashSet<>();
    if (!type.isEmpty()) {
      for (String str : type.split(",")) {
        types.add(str.toLowerCase());
      }
    }
    List<Model> models = getModels(projectId, modelId, token);

    Set<Compartment> includedCompartments = getCompartments(includedCompartmentIds, models);
    Set<Compartment> excludedCompartments = getCompartments(excludedCompartmentIds, models);
    Set<String> columnsSet = createElementColumnSet(columns);

    List<Element> elements = new ArrayList<>();

    for (Model model2 : models) {
      for (Element element : model2.getElements()) {
        if (ids.size() == 0 || ids.contains(element.getId())) {
          if (types.size() == 0 || types.contains(element.getStringType().toLowerCase())) {
            if (matchIncludedExcludedCompartments(element, includedCompartments, excludedCompartments)) {
              elements.add(element);
            }
          }
        }
      }
    }
    elements.sort(BioEntity.ID_COMPARATOR);
    List<Map<String, Object>> result = new ArrayList<>();
    for (Element element : elements) {
      result.add(preparedElement(element, columnsSet));

    }
    return result;
  }

  private boolean matchIncludedExcludedCompartments(Element element, Set<Compartment> includedCompartments,
      Set<Compartment> excludedCompartments) {
    boolean matchIncluded = true;
    boolean matchExcluded = false;

    if (includedCompartments.size() > 0) {
      matchIncluded = matchCompartments(element, includedCompartments);
    }
    if (excludedCompartments.size() > 0) {
      matchExcluded = matchCompartments(element, excludedCompartments);
    }

    return matchIncluded && !matchExcluded;
  }

  private boolean matchCompartments(Element element, Set<Compartment> compartmentList) {
    boolean isInside = false;
    if (element != null) {
      for (Compartment compartment : compartmentList) {
        if (compartment.contains(element) && element.getModel().getId().equals(compartment.getModel().getId())) {
          isInside = true;
        }
      }
    }
    return isInside;
  }

  private Set<Compartment> getCompartments(String includedCompartmentIds, List<Model> models) {
    Set<Compartment> includedCompartments = new HashSet<>();
    int count = 0;
    if (!includedCompartmentIds.isEmpty()) {
      for (String compartmentId : includedCompartmentIds.split(",")) {
        Integer integerId = Integer.valueOf(compartmentId);
        for (Model model : models) {
          Compartment compartment = model.getElementByDbId(integerId);
          if (compartment != null) {
            includedCompartments.add(compartment);
          }
        }
        count++;
      }
      if (count > 0 && includedCompartments.size() == 0) {
        includedCompartments.add(new Compartment("empty_comp_to_reject_all_elements"));
      }
    }
    return includedCompartments;
  }

  private Map<String, Object> preparedElement(Element element, Set<String> columnsSet) {
    Map<String, Object> result = new TreeMap<>();
    for (String string : columnsSet) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = element.getId();
      } else if (column.equals("modelid")) {
        value = element.getModelData().getId();
      } else if (column.equals("elementid")) {
        value = element.getElementId();
      } else if (column.equals("name")) {
        value = element.getName();
      } else if (column.equals("type")) {
        value = element.getStringType();
      } else if (column.equals("symbol")) {
        value = element.getSymbol();
      } else if (column.equals("fullname")) {
        value = element.getFullName();
      } else if (column.equals("abbreviation")) {
        value = element.getAbbreviation();
      } else if (column.equals("compartmentid")) {
        if (element.getCompartment() != null) {
          value = element.getCompartment().getId();
        }
      } else if (column.equals("complexid")) {
        if (element instanceof Species) {
          if (((Species) element).getComplex() != null) {
            value = ((Species) element).getComplex().getId();
          }
        }
      } else if (column.equals("initialconcentration")) {
        if (element instanceof Species) {
          value = ((Species) element).getInitialConcentration();
        }
      } else if (column.equals("initialamount")) {
        if (element instanceof Species) {
          value = ((Species) element).getInitialAmount();
        }
      } else if (column.equals("boundarycondition")) {
        if (element instanceof Species) {
          value = ((Species) element).isBoundaryCondition();
        }
      } else if (column.equals("constant")) {
        if (element instanceof Species) {
          value = ((Species) element).isConstant();
        }
      } else if (column.equals("references")) {
        value = createAnnotations(element.getMiriamData());
      } else if (column.equals("synonyms")) {
        value = element.getSynonyms();
      } else if (column.equals("formula")) {
        value = element.getFormula();
      } else if (column.equals("notes")) {
        value = element.getNotes();
      } else if (column.equals("other")) {
        value = getOthersForElement(element);
      } else if (column.equals("formersymbols")) {
        value = element.getFormerSymbols();
      } else if (column.equals("hierarchyvisibilitylevel")) {
        value = element.getVisibilityLevel();
      } else if (column.equals("transparencylevel")) {
        value = element.getTransparencyLevel();
      } else if (column.equals("linkedsubmodel")) {
        if (element.getSubmodel() != null) {
          value = element.getSubmodel().getSubmodel().getId();
        }
      } else if (column.equals("bounds")) {
        value = createBounds(element.getX(), element.getY(), element.getWidth(), element.getHeight());
      } else {
        value = "Unknown column";
      }
      result.put(string, value);
    }
    return result;
  }

  protected Map<String, Object> getOthersForElement(Element element) {
    Map<String, Object> result = new TreeMap<>();
    List<Map<String, Object>> modifications = new ArrayList<>();
    String structuralState = null;
    Map<String, Object> structures = new TreeMap<>();
    if (element instanceof Protein) {
      Protein protein = ((Protein) element);
      modifications = getModifications(protein.getModificationResidues());
      structuralState = protein.getStructuralState();
    } else if (element instanceof Rna) {
      Rna rna = ((Rna) element);
      modifications = getModifications(rna.getRegions());
      structuralState = rna.getState();
    } else if (element instanceof AntisenseRna) {
      AntisenseRna antisenseRna = ((AntisenseRna) element);
      modifications = getModifications(antisenseRna.getRegions());
      structuralState = antisenseRna.getState();
    } else if (element instanceof Gene) {
      Gene gene = ((Gene) element);
      modifications = getModifications(gene.getModificationResidues());
      structuralState = gene.getState();
    }
    if (element instanceof Species) {
      structures = getStructures(((Species) element).getUniprots());
    }
    result.put("modifications", modifications);
    result.put("structuralState", structuralState);
    result.put("structures", structures);

    return result;
  }

  private List<Map<String, Object>> getModifications(List<? extends ModificationResidue> elements) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (ModificationResidue region : elements) {
      Map<String, Object> row = new TreeMap<>();
      row.put("name", region.getName());
      row.put("modificationId", region.getIdModificationResidue());
      if (region instanceof AbstractSiteModification) {
        AbstractSiteModification siteModification = ((AbstractSiteModification) region);
        if (siteModification.getState() != null) {
          row.put("state", siteModification.getState().name());
        }
      }
      String type = null;
      if (region instanceof Residue) {
        type = ModificationType.RESIDUE.name();
      } else if (region instanceof BindingRegion) {
        type = ModificationType.BINDING_REGION.name();
      } else if (region instanceof CodingRegion) {
        type = ModificationType.CODING_REGION.name();
      } else if (region instanceof ProteinBindingDomain) {
        type = ModificationType.PROTEIN_BINDING_DOMAIN.name();
      } else if (region instanceof RegulatoryRegion) {
        type = ModificationType.REGULATORY_REGION.name();
      } else if (region instanceof TranscriptionSite) {
        if (((TranscriptionSite) region).getDirection().equals("LEFT")) {
          type = ModificationType.TRANSCRIPTION_SITE_LEFT.name();
        } else {
          type = ModificationType.TRANSCRIPTION_SITE_RIGHT.name();
        }
      } else if (region instanceof ModificationSite) {
        type = ModificationType.MODIFICATION_SITE.name();
      } else {
        throw new InvalidArgumentException("Unknown class: " + region.getClass());
      }
      row.put("type", type);
      result.add(row);
    }
    return result;
  }

  private Map<String, Object> getStructures(Set<UniprotRecord> uniprots) {
    Map<String, Object> result = new TreeMap<>();
    for (UniprotRecord uniprotRec : uniprots) {
      Set<Object> structs = new HashSet<>();
      for (Structure struct : uniprotRec.getStructures()) {
        structs.add(struct.toMap());
      }
      result.put(uniprotRec.getUniprotId(), structs);
    }
    return result;
  }

  private Map<String, Object> createBounds(Double x, Double y, Double width, Double height) {
    Map<String, Object> result = new TreeMap<>();
    result.put("x", x);
    result.put("y", y);
    result.put("width", width);
    result.put("height", height);
    return result;
  }

  private Set<String> createElementColumnSet(String columns) {
    Set<String> columnsSet = new HashSet<>();
    if (columns.equals("")) {
      columnsSet.add("id");
      columnsSet.add("elementId");
      columnsSet.add("modelId");
      columnsSet.add("name");
      columnsSet.add("type");
      columnsSet.add("notes");
      columnsSet.add("type");
      columnsSet.add("symbol");
      columnsSet.add("complexId");
      columnsSet.add("compartmentId");
      columnsSet.add("fullName");
      columnsSet.add("abbreviation");
      columnsSet.add("formula");
      columnsSet.add("name");
      columnsSet.add("synonyms");
      columnsSet.add("formerSymbols");
      columnsSet.add("references");
      columnsSet.add("bounds");
      columnsSet.add("hierarchyVisibilityLevel");
      columnsSet.add("transparencyLevel");
      columnsSet.add("linkedSubmodel");
      columnsSet.add("other");
      columnsSet.add("initialConcentration");
      columnsSet.add("boundaryCondition");
      columnsSet.add("constant");
      columnsSet.add("initialAmount");
    } else {
      for (String str : columns.split(",")) {
        columnsSet.add(str);
      }
    }
    return columnsSet;
  }

}
