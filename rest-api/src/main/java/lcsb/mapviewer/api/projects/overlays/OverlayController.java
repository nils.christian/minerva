package lcsb.mapviewer.api.projects.overlays;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class OverlayController extends BaseController {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(OverlayController.class);

  @Autowired
  private OverlayRestImpl overlayRestImp;

  @RequestMapping(value = "/projects/{projectId}/overlays/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getOverlayList(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "creator", defaultValue = "") String creator, //
      @RequestParam(value = "publicOverlay", defaultValue = "") String publicOverlay //
  ) throws SecurityException, QueryException {
    return overlayRestImp.getOverlayList(token, projectId, creator, publicOverlay);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getOverlayById(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "overlayId") String overlayId//
  ) throws SecurityException, QueryException {
    return overlayRestImp.getOverlayById(token, projectId, overlayId);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}/models/{modelId}/bioEntities/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getOverlayElements(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "overlayId") String overlayId,
      @RequestParam(value = "columns", defaultValue = "") String columns) throws SecurityException, QueryException {
    return overlayRestImp.getOverlayElements(token, projectId, Integer.valueOf(overlayId), columns);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}/models/{modelId}/bioEntities/reactions/{reactionId}/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getFullReaction(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "overlayId") String overlayId, //
      @PathVariable(value = "reactionId") String reactionId, //
      @RequestParam(value = "columns", defaultValue = "") String columns //
  ) throws SecurityException, QueryException {
    return overlayRestImp.getOverlayElement(token, projectId, Integer.valueOf(modelId), Integer.valueOf(overlayId),
        Integer.valueOf(reactionId), "REACTION", columns);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}/models/{modelId}/bioEntities/elements/{elementId}/", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> getFullSpecies(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "overlayId") String overlayId, //
      @PathVariable(value = "elementId") String reactionId, //
      @RequestParam(value = "columns", defaultValue = "") String columns //
  ) throws SecurityException, QueryException {
    return overlayRestImp.getOverlayElement(token, projectId, Integer.valueOf(modelId), Integer.valueOf(overlayId),
        Integer.valueOf(reactionId), "ALIAS", columns);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/", method = { RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> addOverlay(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestParam(value = "name") String name, //
      @RequestParam(value = "description") String description, //
      @RequestParam(value = "content", defaultValue = "") String content, //
      @RequestParam(value = "fileId", defaultValue = "") String fileId, //
      @RequestParam(value = "filename") String filename, //
            @RequestParam(value = "googleLicenseConsent") String googleLicenseConsent, //
      @RequestParam(value = "type", defaultValue = "") String type //
  ) throws SecurityException, QueryException, IOException {
    return overlayRestImp.addOverlay(token, projectId, name, description, content, fileId, filename, type, googleLicenseConsent);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}", method = { RequestMethod.DELETE }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> removeOverlay(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "overlayId") String overlayId //
  ) throws SecurityException, QueryException, IOException {
    return overlayRestImp.removeOverlay(token, projectId, overlayId);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Map<String, Object> updateOverlay(//
      @RequestBody String body, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "overlayId") String overlayId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, QueryException, IOException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "overlay");
    return overlayRestImp.updateOverlay(token, projectId, overlayId, data);
  }

  @RequestMapping(value = "/projects/{projectId}/overlays/{overlayId}:downloadSource", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public ResponseEntity<byte[]> getOverlaySource(//
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "overlayId") String overlayId //
  ) throws SecurityException, QueryException, JsonParseException, JsonMappingException, IOException {

    FileEntry file = overlayRestImp.getOverlaySource(token, projectId, overlayId);
    MediaType type = MediaType.TEXT_PLAIN;
    String filename = file.getOriginalFileName();
    if (filename != null) {
      if (file.getOriginalFileName().endsWith("xml")) {
        type = MediaType.APPLICATION_XML;
      }
    } else {
      filename = overlayId + ".txt";
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + filename).body(file.getFileContent());
  }

  /**
   * @return the overlayRestImp
   * @see #overlayRestImp
   */
  public OverlayRestImpl getOverlayRestImp() {
    return overlayRestImp;
  }

  /**
   * @param overlayRestImp
   *          the overlayRestImp to set
   * @see #overlayRestImp
   */
  public void setOverlayRestImp(OverlayRestImpl overlayRestImp) {
    this.overlayRestImp = overlayRestImp;
  }

}