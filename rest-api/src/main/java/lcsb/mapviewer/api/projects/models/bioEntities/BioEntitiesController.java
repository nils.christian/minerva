package lcsb.mapviewer.api.projects.models.bioEntities;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class BioEntitiesController extends BaseController {
  @Autowired
  private BioEntitiesRestImpl bioEntitiesRestImpl;

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/bioEntities:search", method = {
      RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getClosestElementsByCoordinates(//
      @PathVariable(value = "projectId") String projectId, //
      @PathVariable(value = "modelId") String modelId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token, //
      @RequestParam(value = "coordinates", defaultValue = "") String coordinates, //
      @RequestParam(value = "query", defaultValue = "") String query, //
      @RequestParam(value = "count", defaultValue = "") String count, //
      @RequestParam(value = "type", defaultValue = "") String type, //
      @RequestParam(value = "perfectMatch", defaultValue = "false") String perfectMatch//
  ) throws QueryException, SecurityException {
    if (!coordinates.trim().isEmpty()) {
      if (modelId.equals("*")) {
        throw new QueryException("modelId must be defined when searching by coordinates");
      }
      if (count.trim().isEmpty()) {
        count = "5";
      }
      String[] tmp = coordinates.split(",");
      if (tmp.length != 2) {
        throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'");
      }
      Double x = null;
      Double y = null;
      try {
        x = Double.valueOf(tmp[0]);
        y = Double.valueOf(tmp[1]);
      } catch (NumberFormatException e) {
        throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'", e);
      }
      Point2D pointCoordinates = new Point2D.Double(x, y);
      return bioEntitiesRestImpl.getClosestElementsByCoordinates(projectId, modelId, token, pointCoordinates,
          Integer.valueOf(count), perfectMatch, type);
    } else if (!query.trim().isEmpty()) {
      if (count.trim().isEmpty()) {
        count = "100";
      }
      return bioEntitiesRestImpl.getElementsByQuery(projectId, token, modelId, query, Integer.valueOf(count),
          perfectMatch);
    } else {
      return new ArrayList<>();
    }
  }

  @RequestMapping(value = "/projects/{projectId}/models/{modelId}/bioEntities/suggestedQueryList", method = {
      RequestMethod.GET, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
  public String[] getSuggestedQueryList(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token//
  ) throws SecurityException {
    return bioEntitiesRestImpl.getSuggestedQueryList(projectId, token);
  }

}