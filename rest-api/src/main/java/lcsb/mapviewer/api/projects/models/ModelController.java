package lcsb.mapviewer.api.projects.models;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.services.SecurityException;

@RestController
public class ModelController extends BaseController {
  @Autowired
  private ModelRestImpl modelController;

  @RequestMapping(value = "/projects/{projectId:.+}/models/", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getModels(//
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, ObjectNotFoundException {
    return modelController.getModels(projectId, token);
  }

  @RequestMapping(value = "/projects/{projectId:.+}/models/{modelId:.+}", method = { RequestMethod.GET }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Object getModel(//
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "projectId") String projectId, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, ObjectNotFoundException {
    if (modelId.equals("*")) {
      return modelController.getModels(projectId, token);
    } else {
      return modelController.getModel(projectId, modelId, token);
    }
  }

  @RequestMapping(value = "/projects/{projectId:.+}/models/{modelId:.+}", method = { RequestMethod.PATCH }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Object updateModel(//
      @PathVariable(value = "modelId") String modelId, //
      @PathVariable(value = "projectId") String projectId, //
      @RequestBody String body, //
      @CookieValue(value = Configuration.AUTH_TOKEN) String token //
  ) throws SecurityException, JsonParseException, JsonMappingException, IOException, QueryException {
    Map<String, Object> node = parseBody(body);
    Map<String, Object> data = getData(node, "model");
    return modelController.updateModel(projectId, modelId, data, token);
  }

}