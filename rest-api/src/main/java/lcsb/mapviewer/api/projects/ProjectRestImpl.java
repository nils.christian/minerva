package lcsb.mapviewer.api.projects;

import java.awt.Color;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.map.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.api.ObjectExistsException;
import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.projects.models.publications.PublicationsRestImpl;
import lcsb.mapviewer.commands.ClearColorModelCommand;
import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.commands.SetFixedHierarchyLevelCommand;
import lcsb.mapviewer.commands.SubModelCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.IConverter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.OverviewSearchLink;
import lcsb.mapviewer.model.map.layout.ColorSchema;
import lcsb.mapviewer.model.map.layout.InvalidColorSchemaException;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.interfaces.ILayoutService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.utils.ColorSchemaReader;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import lcsb.mapviewer.services.utils.data.BuildInLayout;

@Transactional(value = "txManager")
public class ProjectRestImpl extends BaseRestImpl {

  /**
   * Constant defining size of the array returned by
   * {@link PathIterator#currentSegment(double[])} method. More information can be
   * found <a href=
   * "http://docs.oracle.com/javase/7/docs/api/java/awt/geom/PathIterator.html#currentSegment(double[])"
   * >here</a>
   */
  private static final int PATH_ITERATOR_SEGMENT_SIZE = 6;

  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ProjectRestImpl.class);

  @Autowired
  private PublicationsRestImpl publicationsRestImpl;

  @Autowired
  private ILayoutService layoutService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UploadedFileEntryDao uploadedFileEntryDao;

  public Map<String, Object> getProject(String projectId, String token)
      throws SecurityException, ObjectNotFoundException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return createData(project);
  }

  private Map<String, Object> createData(Project project) {
    Map<String, Object> result = new LinkedHashMap<>();

    result.put("version", project.getVersion());
    if (project.getDisease() != null) {
      result.put("disease", createAnnotation(project.getDisease()));
    } else {
      result.put("disease", null);
    }
    if (project.getOrganism() != null) {
      result.put("organism", createAnnotation(project.getOrganism()));
    } else {
      result.put("organism", null);
    }
    result.put("idObject", project.getId());
    if (project.getStatus() != null) {
      result.put("status", project.getStatus().toString());
    }
    result.put("directory", project.getDirectory());
    result.put("progress", project.getProgress());
    result.put("notifyEmail", project.getNotifyEmail());
    result.put("warnings", project.getWarnings().size() > 0);
    result.put("errors", project.getErrors() != null && !project.getErrors().isEmpty());
    result.put("name", project.getName());
    result.put("projectId", project.getProjectId());
    result.put("mapCanvasType", project.getMapCanvasType());

    List<Map<String, Object>> images = new ArrayList<>();
    for (OverviewImage image : project.getOverviewImages()) {
      images.add(imageToMap(image));
    }
    result.put("overviewImageViews", images);

    Set<OverviewImage> set = new HashSet<>();
    set.addAll(project.getOverviewImages());
    for (OverviewImage image : project.getOverviewImages()) {
      for (OverviewLink ol : image.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          set.remove(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    if (set.size() > 0) {
      result.put("topOverviewImage", imageToMap(set.iterator().next()));
    } else if (project.getOverviewImages().size() > 0) {
      logger.warn(
          "Cannot determine top level image. Taking first one. " + project.getOverviewImages().get(0).getFilename());
      result.put("topOverviewImage", imageToMap(project.getOverviewImages().get(0)));
    } else {
      result.put("topOverviewImage", null);
    }

    return result;
  }

  private Map<String, Object> imageToMap(OverviewImage image) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("idObject", image.getId());
    result.put("filename", image.getProject().getDirectory() + "/" + image.getFilename());
    result.put("width", image.getWidth());
    result.put("height", image.getHeight());
    List<Map<String, Object>> links = new ArrayList<>();
    for (OverviewLink link : image.getLinks()) {
      links.add(overviewLinkToMap(link));
    }
    result.put("links", links);
    return result;
  }

  private Map<String, Object> overviewLinkToMap(OverviewLink link) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("idObject", link.getId());
    result.put("polygon", polygonToMap(link.getPolygon()));
    if (link instanceof OverviewModelLink) {
      OverviewModelLink modelLink = (OverviewModelLink) link;
      result.put("zoomLevel", modelLink.getZoomLevel());
      result.put("modelPoint", new Point2D.Double(modelLink.getxCoord(), modelLink.getyCoord()));
      result.put("modelLinkId", modelLink.getLinkedModel().getId());

    } else if (link instanceof OverviewImageLink) {
      result.put("imageLinkId", ((OverviewImageLink) link).getLinkedOverviewImage().getId());
    } else if (link instanceof OverviewSearchLink) {
      result.put("query", ((OverviewSearchLink) link).getQuery());
    } else {
      throw new NotImplementedException("Not implemented behaviour for class: " + link.getClass());
    }
    result.put("type", link.getClass().getSimpleName());
    return result;
  }

  private List<Map<String, Double>> polygonToMap(String polygon) {
    List<Map<String, Double>> result = new ArrayList<>();
    for (String string : polygon.split(" ")) {
      String tmp[] = string.split(",");
      Map<String, Double> point = new TreeMap<>();
      point.put("x", Double.parseDouble(tmp[0]));
      point.put("y", Double.parseDouble(tmp[1]));
      result.add(point);
    }
    return result;
  }

  public FileEntry getSource(String token, String projectId) throws SecurityException, QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (project.getInputData() != null) {
      // fetch the data from db
      project.getInputData().getFileContent();
    }
    return project.getInputData();
  }

  public FileEntry getModelAsImage(String token, String projectId, String modelId, String handlerClass,
      String backgroundOverlayId, String overlayIds, String zoomLevel, String polygonString) throws SecurityException,
      QueryException, IOException, InvalidColorSchemaException, CommandExecutionException, DrawingException {
    User user = getUserService().getUserByToken(token);

    Model topModel = getModelService().getLastModelByProjectId(projectId, token);
    if (topModel == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Model originalModel = topModel.getSubmodelById(modelId);

    if (originalModel == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    Layout overlay = null;
    if (!backgroundOverlayId.equals("")) {
      overlay = topModel.getLayoutByIdentifier(Integer.valueOf(backgroundOverlayId));

      if (overlay == null) {
        throw new ObjectNotFoundException("Unknown overlay in model. Layout.id=" + backgroundOverlayId);
      }
    } else {
      if (topModel.getLayouts().size() > 0) {
        overlay = topModel.getLayouts().get(0);
      }
    }

    Model colorModel = new CopyCommand(originalModel).execute();
    if (overlay != null) {
      if (overlay.getInputData() != null) {
        ColorSchemaReader reader = new ColorSchemaReader();
        Collection<ColorSchema> schemas = reader.readColorSchema(overlay.getInputData().getFileContent());

        new ColorModelCommand(colorModel, schemas, getUserService().getColorExtractorForUser(user)).execute();
      } else if (overlay.getTitle().equals(BuildInLayout.CLEAN.getTitle())) {
        // this might not return true if we change CLEAN.title in future...

        // if it's clean then remove coloring
        new ClearColorModelCommand(colorModel).execute();
      } else if (overlay.isHierarchicalView()) {
        new SetFixedHierarchyLevelCommand(colorModel, overlay.getHierarchyViewLevel()).execute();
      }
    }

    Integer level = Configuration.MIN_ZOOM_LEVEL;
    if (!zoomLevel.equals("")) {
      level = Integer.valueOf(zoomLevel);
    }

    Path2D polygon = stringToPolygon(polygonString, colorModel);

    Double minX = originalModel.getWidth();
    Double minY = originalModel.getHeight();
    Double maxX = 0.0;
    Double maxY = 0.0;

    PathIterator pathIter = polygon.getPathIterator(null);
    while (!pathIter.isDone()) {
      final double[] segment = new double[PATH_ITERATOR_SEGMENT_SIZE];
      if (pathIter.currentSegment(segment) != PathIterator.SEG_CLOSE) {
        minX = Math.min(minX, segment[0]);
        maxX = Math.max(maxX, segment[0]);
        minY = Math.min(minY, segment[1]);
        maxY = Math.max(maxY, segment[1]);
      }
      pathIter.next();
    }

    maxX = Math.min(originalModel.getWidth(), maxX);
    maxY = Math.min(originalModel.getHeight(), maxY);
    minX = Math.max(0.0, minX);
    minY = Math.max(0.0, minY);

    Double scale = Math.max(originalModel.getHeight(), originalModel.getWidth()) / (originalModel.getTileSize());

    for (int i = level; i > Configuration.MIN_ZOOM_LEVEL; i--) {
      scale /= 2;
    }

    ColorExtractor colorExtractor = getUserService().getColorExtractorForUser(user);

    Params params = new Params().//
        x(minX).//
        y(minY).//
        height((maxY - minY) / scale).//
        width((maxX - minX) / scale).//
        level(level - Configuration.MIN_ZOOM_LEVEL).//
        nested(false).// automatically set nested view as invalid
        scale(scale).//
        colorExtractor(colorExtractor).//
        sbgn(topModel.getProject().isSbgnFormat()).//
        model(colorModel);
    if (overlay != null) {
      params.nested(overlay.isHierarchicalView());
    }
    List<Integer> visibleLayoutIds = deserializeIdList(overlayIds);
    for (Integer integer : visibleLayoutIds) {
      Map<Object, ColorSchema> map = layoutService.getElementsForLayout(colorModel, integer, token);
      params.addVisibleLayout(map);
    }

    ImageGenerators imageGenerator = new ImageGenerators();
    String extension = imageGenerator.getExtension(handlerClass);
    File file = File.createTempFile("map", "." + extension);

    imageGenerator.generate(handlerClass, params, file.getAbsolutePath());

    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName("map." + extension);
    entry.setFileContent(IOUtils.toByteArray(new FileInputStream(file)));
    entry.setLength(entry.getFileContent().length);
    file.delete();
    return entry;

  }

  private Path2D stringToPolygon(String polygonString, Model colorModel) {
    String[] stringPointArray = polygonString.split(";");

    List<Point2D> points = new ArrayList<>();
    for (String string : stringPointArray) {
        if (!string.trim().equals("")) {
            double x = Double.valueOf(string.split(",")[0]);
            double y= Double.valueOf(string.split(",")[1]);
            points.add(new Point2D.Double(x, y));
        }
    }

    if (points.size() <= 2) {
      points.clear();
      points.add(new Point2D.Double(0, 0));
      points.add(new Point2D.Double(colorModel.getWidth(), 0));
      points.add(new Point2D.Double(colorModel.getWidth(), colorModel.getHeight()));
      points.add(new Point2D.Double(0, colorModel.getHeight()));
  }

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(points.get(0).getX(), points.get(0).getY());
    for (int i = 1; i < points.size(); i++) {
        Point2D point = points.get(i);
        polygon.lineTo(point.getX(), point.getY());
    }
    polygon.closePath();
    return polygon;
  }

  private List<Integer> deserializeIdList(String overlayIds) {
    List<Integer> result = new ArrayList<>();
    String[] tmp = overlayIds.split(",");
    for (String string : tmp) {
      if (!string.equals("")) {
        result.add(Integer.valueOf(string));
      }
    }
    return result;
  }

  public FileEntry getModelAsModelFile(String token, String projectId, String modelId, String handlerClass,
      String backgroundOverlayId, String overlayIds, String zoomLevel, String polygonString)
      throws SecurityException, QueryException, IOException, InvalidColorSchemaException, CommandExecutionException,
      ConverterException, InconsistentModelException {
    User user = getUserService().getUserByToken(token);
    Model topModel = getModelService().getLastModelByProjectId(projectId, token);
    if (topModel == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    Model originalModel = topModel.getSubmodelById(modelId);

    if (originalModel == null) {
      throw new ObjectNotFoundException("Model with given id doesn't exist");
    }

    Path2D polygon = stringToPolygon(polygonString, originalModel);

    // create model bounded by the polygon
    SubModelCommand subModelCommand = new SubModelCommand(originalModel, polygon);
    Model part = subModelCommand.execute();

    // Get list of overlay ids
    String[] overlayIdsList;
    if (overlayIds == null || overlayIds.trim().isEmpty()) {
      overlayIdsList = new String[0];
    } else {
      overlayIdsList = overlayIds.split(",");
    }
    // Remove all colors
    if (overlayIdsList.length > 0) {

      for (Element element : part.getElements()) {
        element.setColor(Color.WHITE);
      }
    }
    // Color with overlays
    for (String overlayId : overlayIdsList) {
      Layout overlay = layoutService.getLayoutById(Integer.parseInt(overlayId.trim()), token);

      ColorSchemaReader reader = new ColorSchemaReader();
      Collection<ColorSchema> schemas = reader.readColorSchema(overlay.getInputData().getFileContent());

      new ColorModelCommand(part, schemas, getUserService().getColorExtractorForUser(user)).execute();
    }

    IConverter parser = getModelParser(handlerClass);
    InputStream is = parser.exportModelToInputStream(part);

    String fileExtension = parser.getFileExtension();

    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName("model." + fileExtension);
    entry.setFileContent(IOUtils.toByteArray(is));
    entry.setLength(entry.getFileContent().length);
    return entry;

  }

  public Map<String, Object> getStatistics(String projectId, String token)
      throws SecurityException, ObjectNotFoundException {
    Map<String, Object> result = new TreeMap<>();

    Map<MiriamType, Integer> elementAnnotations = new TreeMap<>();
    for (MiriamType mt : MiriamType.values()) {
      elementAnnotations.put(mt, 0);
    }
    List<Model> models = super.getModels(projectId, "*", token);

    for (Model model2 : models) {
      for (Element alias : model2.getElements()) {
        for (MiriamData md : alias.getMiriamData()) {
          Integer amount = elementAnnotations.get(md.getDataType());
          amount += 1;
          elementAnnotations.put(md.getDataType(), amount);
        }
      }
    }
    result.put("elementAnnotations", elementAnnotations);

    Map<MiriamType, Integer> reactionAnnotations = new TreeMap<>();
    for (MiriamType mt : MiriamType.values()) {
      reactionAnnotations.put(mt, 0);
    }
    for (Model model2 : models) {
      for (Reaction reaction : model2.getReactions()) {
        for (MiriamData md : reaction.getMiriamData()) {
          Integer amount = reactionAnnotations.get(md.getDataType());
          amount += 1;
          reactionAnnotations.put(md.getDataType(), amount);
        }
      }
    }

    result.put("reactionAnnotations", reactionAnnotations);

    result.put("publications", publicationsRestImpl.getPublications(models).size());

    return result;
  }

  public List<Map<String, Object>> getProjects(String token) throws SecurityException {
    List<Project> projects = getProjectService().getAllProjects(token);
    List<Map<String, Object>> result = new ArrayList<>();
    for (Project project : projects) {
      result.add(createData(project));
    }
    return result;
  }

  public Map<String, Object> updateProject(String token, String projectId, Map<String, Object> data)
      throws SecurityException, QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    boolean canModify = getUserService().userHasPrivilege(token, PrivilegeType.PROJECT_MANAGEMENT);
    if (!canModify) {
      throw new SecurityException("You cannot update projects");
    }
    Set<String> fields = data.keySet();
    for (String fieldName : fields) {
      Object value = data.get(fieldName);
      String stringValue = null;
      if (value instanceof String) {
        stringValue = (String) value;
      }
      if (fieldName.equalsIgnoreCase("version")) {
        project.setVersion((String) value);
      } else if (fieldName.equalsIgnoreCase("id")) {
        try {
          int id = Integer.parseInt(stringValue);
          if (id != project.getId()) {
            throw new QueryException("Invalid id: " + stringValue);
          }
        } catch (NumberFormatException e) {
          throw new QueryException("Invalid id: " + stringValue);
        }
      } else if (fieldName.equalsIgnoreCase("projectId")) {
        if (!project.getProjectId().equalsIgnoreCase(stringValue)) {
          throw new QueryException("You cannot modify projectId");
        }
      } else if (fieldName.equalsIgnoreCase("name")) {
        project.setName((String) value);
      } else if (fieldName.equalsIgnoreCase("notifyEmail")) {
        project.setNotifyEmail(stringValue);
      } else if (fieldName.equalsIgnoreCase("organism")) {
        MiriamData organism = updateMiriamData(project.getOrganism(), value);
        project.setOrganism(organism);
      } else if (fieldName.equalsIgnoreCase("disease")) {
        MiriamData disease = updateMiriamData(project.getDisease(), value);
        project.setDisease(disease);
      } else if (fieldName.equalsIgnoreCase("mapCanvasType")) {
        MapCanvasType mapCanvasType;
        try {
          mapCanvasType = MapCanvasType.valueOf(stringValue);
        } catch (Exception e) {
          throw new QueryException("Invalid mapCanvasType value");
        }
        project.setMapCanvasType(mapCanvasType);
      } else {
        throw new QueryException("Unknown field: " + fieldName);
      }
    }
    getProjectService().updateProject(project, token);
    return getProject(projectId, token);
  }

  private MiriamData updateMiriamData(MiriamData organism, Object res) {
    if (res == null) {
      return null;
    }
    if (organism == null) {
      organism = new MiriamData();
    }

    if (res instanceof Map) {
      @SuppressWarnings("unchecked")
      Map<String, Object> map = (Map<String, Object>) res;
      organism.setDataType(MiriamType.valueOf((String) map.get("type")));
      organism.setResource((String) map.get("resource"));
      return organism;
    } else {
      throw new InvalidArgumentException("invalid miriamdData: " + res);
    }
  }

  public Map<String, Object> addProject(String token, String projectId, MultiValueMap<String, Object> data, String path)
      throws SecurityException, QueryException, IOException {
    User user = getUserService().getUserByToken(token);
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project != null) {
      logger.debug(project.getProjectId());
      throw new ObjectExistsException("Project with given id already exists");
    }
    CreateProjectParams params = new CreateProjectParams();
    String directory = computePathForProject(projectId, path);
    String fileId = getFirstValue(data.get("file-id"));
    if (fileId == null) {
      throw new QueryException("file-id is obligatory");
    }
    UploadedFileEntry file = uploadedFileEntryDao.getById(Integer.valueOf(fileId));
    if (file == null) {
      throw new QueryException("Invalid file id: " + fileId);
    }
    if (file.getOwner() == null || !file.getOwner().getLogin().equals(user.getLogin())) {
      throw new SecurityException("Access denied to source file");
    }
    String parserClass = getFirstValue(data.get("parser"));
    if (parserClass == null) {
      throw new QueryException("parser is obligatory");
    }
    IConverter parser = getModelParser(parserClass);

    List<ZipEntryFile> zipEntries = extractZipEntries(data);
    params.complex(zipEntries.size() > 0);
    for (ZipEntryFile entry : zipEntries) {
      params.addZipEntry(entry);
    }

    params.addUser(user.getLogin(), null);
    params.async(true);
    params.parser(parser);
    params.authenticationToken(token);
    params.autoResize(getFirstValue(data.get("auto-resize")));
    params.cacheModel(getFirstValue(data.get("cache")));
    params.description(getFirstValue(data.get("description")));
    params.images(true);
    params.notifyEmail(getFirstValue(data.get("notify-email")));
    params.projectDir(directory);
    params.projectDisease(getFirstValue(data.get("disease")));
    params.projectFile(new ByteArrayInputStream(file.getFileContent()));
    params.projectId(projectId);
    params.projectName(getFirstValue(data.get("name")));
    params.projectOrganism(getFirstValue(data.get("organism")));
    params.sbgnFormat(getFirstValue(data.get("sbgn")));
    params.semanticZoom(getFirstValue(data.get("semantic-zoom")));
    params.version(getFirstValue(data.get("version")));
    params.annotations(getFirstValue(data.get("annotate")));
    MapCanvasType mapCanvasType;
    try {
      mapCanvasType = MapCanvasType.valueOf(getFirstValue(data.get("mapCanvasType")));
    } catch (Exception e) {
      throw new QueryException("Invalid mapCanvasType value");
    }
    params.mapCanvasType(mapCanvasType);

    if (params.isUpdateAnnotations()) {
      params.annotatorsMap(projectService.createClassAnnotatorTree(user));
    }
    params.analyzeAnnotations(getFirstValue(data.get("verify-annotations")));
    if (params.isAnalyzeAnnotations()) {
      params.requiredAnnotations(projectService.createClassAnnotatorTree(user));
      params.validAnnotations(projectService.createClassAnnotatorTree(user));
    }
    params.setAnnotatorParams(projectService.getAnnotatorsParams(user));

    getProjectService().createProject(params);
    return getProject(projectId, token);
  }

  protected String computePathForProject(String projectId, String path) {
    long id = projectDao.getNextId();
    return path + "/../map_images/" + md5(projectId + "-" + id) + "/";
  }

  protected List<ZipEntryFile> extractZipEntries(MultiValueMap<String, Object> data) {
    int fileIndex = 0;
    List<ZipEntryFile> result = new ArrayList<>();
    while (data.get("zip-entries[" + fileIndex + "][_filename]") != null) {
      ZipEntryFile entry = null;
      String entryType = (String) data.get("zip-entries[" + fileIndex + "][_type]").get(0);
      String filename = (String) data.get("zip-entries[" + fileIndex + "][_filename]").get(0);
      if ("MAP".equalsIgnoreCase(entryType)) {
        String submodelTypeKey = "zip-entries[" + fileIndex + "][_data][type][id]";
        String rootKey = "zip-entries[" + fileIndex + "][_data][root]";
        String mappingKey = "zip-entries[" + fileIndex + "][_data][mapping]";
        SubmodelType mapType = SubmodelType.valueOf((String) data.get(submodelTypeKey).get(0));
        String name = (String) data.get("zip-entries[" + fileIndex + "][_data][name]").get(0);
        Boolean root = getBoolValue(data.get(rootKey), false);
        Boolean mapping = getBoolValue(data.get(mappingKey), false);

        entry = new ModelZipEntryFile(filename, name, root, mapping, mapType);
      } else if ("OVERLAY".equalsIgnoreCase(entryType)) {
        String name = (String) data.get("zip-entries[" + fileIndex + "][_data][name]").get(0);
        String description = (String) data.get("zip-entries[" + fileIndex + "][_data][description]").get(0);
        entry = new LayoutZipEntryFile(filename, name, description);
      } else if ("IMAGE".equalsIgnoreCase(entryType)) {
        entry = new ImageZipEntryFile(filename);
      } else {
        throw new Error("Unknown entry type: " + entryType);
      }
      fileIndex++;
      result.add(entry);

    }
    return result;
  }

  private Boolean getBoolValue(List<Object> list, boolean defaultValue) {
    if (list == null) {
      return defaultValue;
    }
    Object obj = list.get(0);
    if (obj instanceof Boolean) {
      return (Boolean) list.get(0);
    } else {
      return "true".equalsIgnoreCase((String) obj);
    }
  }

  /**
   * Method that computes md5 hash for a given {@link String}.
   * 
   * @param data
   *          input string
   * @return md5 hash for input string
   */
  private String md5(String data) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] mdbytes = md.digest(data.getBytes());
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < mdbytes.length; i++) {
        // CHECKSTYLE:OFF
        // this magic formula transforms integer into hex value
        sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        // CHECKSTYLE:ON
      }
      return sb.toString();
    } catch (NoSuchAlgorithmException e) {
      logger.fatal("Problem with instance of MD5 encoder", e);
    }

    return null;
  }

  public Map<String, Object> removeProject(String token, String projectId, String path)
      throws ObjectNotFoundException, SecurityException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    getProjectService().removeProject(project, path, true, token);
    return getProject(projectId, token);
  }

  public UploadedFileEntryDao getUploadedFileEntryDao() {
    return uploadedFileEntryDao;
  }

  public void setUploadedFileEntryDao(UploadedFileEntryDao uploadedFileEntryDao) {
    this.uploadedFileEntryDao = uploadedFileEntryDao;
  }

  private enum LogSortColumn {
    ID("id"), //
    CONTENT("content"); //

    private String commonName;

    LogSortColumn(String commonName) {
      this.commonName = commonName;
    }
  }

  private class LogEntry implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public LogEntry(int id, String content, String level) {
      this.id = id;
      this.content = content;
      this.level = level;
    }

    public Integer id;
    public String content;
    public String level;
  }

  public Map<String, Object> getLogs(String projectId, String level, String token, String startString, Integer length,
      String sortColumn, String sortOrder, String search) throws SecurityException, QueryException {
    Project project = getProjectService().getProjectByProjectId(projectId, token);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    LogSortColumn sortColumnEnum = getSortOrderColumn(sortColumn);
    Comparator<LogEntry> comparator = getComparatorForColumn(sortColumnEnum, sortOrder);

    Integer start = Math.max(0, Integer.valueOf(startString));
    List<LogEntry> resultList = new ArrayList<>();

    List<LogEntry> logEntries = getEntries(project, level);

    List<LogEntry> filteredList = new ArrayList<>();

    search = search.toLowerCase();
    for (LogEntry entry : logEntries) {
      if (isSearchResult(entry, search)) {
        filteredList.add(entry);
      }
    }
    if (comparator != null) {
      filteredList.sort(comparator);
    }

    int index = 0;
    for (LogEntry entry : filteredList) {
      if (index >= start && index < start + length) {
        resultList.add(entry);
      }
      index++;
    }

    Map<String, Object> result = new TreeMap<>();
    result.put("data", resultList);
    result.put("totalSize", logEntries.size());
    result.put("filteredSize", filteredList.size());
    result.put("start", start);
    result.put("length", resultList.size());
    return result;

  }

  private boolean isSearchResult(LogEntry entry, String search) {
    if (search == null || search.isEmpty()) {
      return true;
    }
    if (entry.content.toLowerCase().contains(search) || entry.id.toString().contains(search)) {
      return true;
    }

    return false;

  }

  private List<LogEntry> getEntries(Project project, String level) {
    List<LogEntry> result = new ArrayList<>();
    int id = 0;
    for (String s : project.getWarnings()) {
      if (!s.isEmpty()) {
        if (level.equalsIgnoreCase("warning") || level.equals("")) {
          result.add(new LogEntry(id, s, "WARNING"));
        }
        id++;
      }
    }
    String errors = project.getErrors();
    if (errors == null) {
      errors = "";
    }
    for (String s : errors.split("\n")) {
      if (!s.isEmpty()) {
        if (level.equalsIgnoreCase("error") || level.equals("")) {
          result.add(new LogEntry(id, s, "ERROR"));
        }
        id++;
      }
    }
    return result;
  }

  private Comparator<LogEntry> getComparatorForColumn(LogSortColumn sortColumnEnum, String sortOrder)
      throws QueryException {
    final int orderFactor;
    if (sortOrder.toLowerCase().equals("desc")) {
      orderFactor = -1;
    } else {
      orderFactor = 1;
    }
    if (sortColumnEnum == null) {
      return null;
    } else if (sortColumnEnum.equals(LogSortColumn.ID)) {
      return new Comparator<LogEntry>() {
        @Override
        public int compare(LogEntry o1, LogEntry o2) {
          return o1.id.compareTo(o2.id) * orderFactor;

        }
      };
    } else if (sortColumnEnum.equals(LogSortColumn.CONTENT)) {
      return new Comparator<LogEntry>() {
        @Override
        public int compare(LogEntry o1, LogEntry o2) {
          return o1.content.compareTo(o2.content) * orderFactor;

        }
      };

    } else {
      throw new QueryException("Sort order not implemented for: " + sortColumnEnum);
    }
  }

  private LogSortColumn getSortOrderColumn(String sortColumn) throws QueryException {
    if (!sortColumn.isEmpty()) {
      for (LogSortColumn type : LogSortColumn.values()) {
        if (type.commonName.toLowerCase().equals(sortColumn.toLowerCase())) {
          return type;
        }
      }
      throw new QueryException("Unknown sortColumn: " + sortColumn);
    }
    return null;
  }

  public List<Map<String, Object>> getSubmapConnections(String token, String projectId)
      throws ObjectNotFoundException, SecurityException {
    List<Map<String, Object>> result = new ArrayList<>();
    List<Model> models = getModels(projectId, "*", token);
    List<Element> elements = new ArrayList<>();
    for (Model model : models) {
      elements.addAll(model.getElements());
    }
    elements.sort(BioEntity.ID_COMPARATOR);
    for (Element element : elements) {
      if (element.getSubmodel() != null) {
        result.add(submodelConnectionToMap(element));
      }
    }
    return result;
  }

  private Map<String, Object> submodelConnectionToMap(Element element) {
    Map<String, Object> result = new TreeMap<>();
    result.put("from", super.createMinifiedSearchResult(element));
    Map<String, Object> to = new TreeMap<>();
    to.put("modelId", element.getSubmodel().getSubmodel().getId());
    result.put("to", to);
    return result;
  }

}
