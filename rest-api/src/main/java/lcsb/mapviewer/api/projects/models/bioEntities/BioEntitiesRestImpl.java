package lcsb.mapviewer.api.projects.models.bioEntities;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.api.BaseRestImpl;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.SecurityException;
import lcsb.mapviewer.services.UserAccessException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.ISearchService;
import lcsb.mapviewer.services.interfaces.IUserService;

@Transactional(value = "txManager")
public class BioEntitiesRestImpl extends BaseRestImpl {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(BioEntitiesRestImpl.class);

  @Autowired
  private ISearchService searchService;

  public List<Map<String, Object>> getClosestElementsByCoordinates(String projectId, String modelId, String token,
      Point2D coordinates, Integer count, String perfectMatch, String type) throws UserAccessException, SecurityException {
    List<Map<String, Object>> resultMap = new ArrayList<>();

    Model model = getModelService().getLastModelByProjectId(projectId, token);

    Model submodel = model.getSubmodelById(modelId);

    Set<String> types = new HashSet<>();
    if (!type.isEmpty()) {
      for (String str : type.split(",")) {
        types.add(str.toLowerCase());
      }
    }

    List<BioEntity> elements = searchService.getClosestElements(submodel, coordinates, count,
        perfectMatch.equalsIgnoreCase("true"), types);
    for (BioEntity object : elements) {
      Map<String, Object> result = createMinifiedSearchResult(object);
      resultMap.add(result);
    }
    return resultMap;
  }

  /**
   * @return the searchService
   * @see #searchService
   */
  public ISearchService getSearchService() {
    return searchService;
  }

  /**
   * @param searchService
   *          the searchService to set
   * @see #searchService
   */
  public void setSearchService(ISearchService searchService) {
    this.searchService = searchService;
  }

  public List<Map<String, Object>> getElementsByQuery(String projectId, String token, String modelId, String query,
      Integer maxElements, String perfectMatch) throws SecurityException {
    List<Map<String, Object>> resultMap = new ArrayList<>();

    Model model = getModelService().getLastModelByProjectId(projectId, token);

    Integer limit = Integer.valueOf(maxElements);
    boolean match = perfectMatch.equals("true");
    List<BioEntity> elements = searchService.searchByQuery(model, query, limit, match);
    for (BioEntity object : elements) {
      Map<String, Object> result = createMinifiedSearchResult(object);
      resultMap.add(result);
    }
    return resultMap;
  }

  public String[] getSuggestedQueryList(String projectId, String token) throws SecurityException {
    Model model = getModelService().getLastModelByProjectId(projectId, token);
    return searchService.getSuggestedQueryList(model);
  }

}
