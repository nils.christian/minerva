package lcsb.mapviewer.api.configuration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ConfigurationRestImplTest extends RestTestFunctions {

  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(ConfigurationRestImplTest.class);

  @Autowired
  public ConfigurationRestImpl configurationRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAllParams() throws Exception {
    try {
      List<Map<String, Object>> list = configurationRestImpl.getAllValues(token);
      assertTrue(list.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testImageFormats() throws Exception {
    try {
      List<Map<String, Object>> list = configurationRestImpl.getImageFormats(token);
      assertTrue(list.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelFormats() throws Exception {
    try {
      List<Map<String, Object>> list = configurationRestImpl.getModelFormats(token);
      assertTrue(list.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetElementTypes() throws Exception {
    try {
      Set<Map<String, String>> list = configurationRestImpl.getElementTypes(token);
      GenericProtein protein = new GenericProtein("id");
      boolean contains = false;
      for (Map<String, String> object : list) {
        contains |= (object.get("name").equals(protein.getStringType()));
      }
      assertTrue(contains);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetReactionTypes() throws Exception {
    try {
      Set<Map<String, String>> list = configurationRestImpl.getReactionTypes(token);
      StateTransitionReaction reaction = new StateTransitionReaction();
      boolean contains = false;
      for (Map<String, String> object : list) {
        contains |= (object.get("name").equals(reaction.getStringType()));
      }
      assertTrue(contains);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetMiriamTypes() throws Exception {
    try {
      Map<?, ?> list = configurationRestImpl.getMiriamTypes(token);
      assertNotNull(list.get(MiriamType.PUBMED.name()));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
