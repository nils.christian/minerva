package lcsb.mapviewer.api.files;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FileRestImplTest.class })
public class AllFileTests {

}
