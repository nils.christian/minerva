package lcsb.mapviewer.api.files;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;

public class FileRestImplTest extends RestTestFunctions {

  @Autowired
  UploadedFileEntryDao uploadedFileEntryDao;

  @Autowired
  FileRestImpl fileRestImpl;

  @Test(expected = lcsb.mapviewer.services.SecurityException.class)
  public void testCreateFileWithoutPrivilege() throws Exception {
    fileRestImpl.createFile(token, "test.txt", "100");
  }

  @Test
  public void testCreateFile() throws Exception {
    Map<String, Object> result = fileRestImpl.createFile(adminToken, "test.txt", "100");
    assertNotNull(result);
    int id = (Integer) result.get("id");
    UploadedFileEntry file = uploadedFileEntryDao.getById(id);
    assertNotNull(file);
    assertEquals(100, file.getLength());
    assertEquals(0, file.getFileContent().length);
    uploadedFileEntryDao.delete(file);
  }

  @Test
  public void testUploadContent() throws Exception {
    byte[] dataChunk = new byte[] { 105, 103 };
    byte[] dataChunk2 = new byte[] { 32, 73 };
    byte[] dataChunkMerged = ArrayUtils.addAll(dataChunk, dataChunk2);
    Map<String, Object> result = fileRestImpl.createFile(adminToken, "test.txt", "100");
    int id = (Integer) result.get("id");
    fileRestImpl.uploadContent(adminToken, id + "", dataChunk);

    UploadedFileEntry file = uploadedFileEntryDao.getById(id);
    assertEquals(100, file.getLength());
    assertEquals(2, file.getFileContent().length);
    assertArrayEquals(dataChunk, file.getFileContent());

    fileRestImpl.uploadContent(adminToken, id + "", dataChunk2);

    assertEquals(100, file.getLength());
    assertEquals(4, file.getFileContent().length);
    assertArrayEquals(dataChunkMerged, file.getFileContent());

    uploadedFileEntryDao.delete(file);
  }

  @Test
  public void testUploadInvalidContent() throws Exception {
    byte[] dataChunk = new byte[100];
    Map<String, Object> result = fileRestImpl.createFile(adminToken, "test.txt", "100");
    int id = (Integer) result.get("id");

    try {
      fileRestImpl.uploadContent(adminToken, id + "", dataChunk);
    } finally {
      uploadedFileEntryDao.getById(id);
      UploadedFileEntry file = uploadedFileEntryDao.getById(id);
      uploadedFileEntryDao.delete(file);
    }
  }

}
