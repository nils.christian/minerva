package lcsb.mapviewer.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.mockito.Mockito;

public class BaseRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(BaseRestImplTest.class);

  @Test
  public void testMathMLToPresentationML() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    String content = super.readFile("testFiles/mathml/content.xml");
    String presentationXml = controller.mathMLToPresentationML(content);
    assertNotNull(presentationXml);
    assertTrue("Presentation math ml doesn't contain expected mrow tag", presentationXml.indexOf("mrow") >= 0);
  }

  @Test
  public void testMathMLToPresentationMLCalledTwice() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    String content = super.readFile("testFiles/mathml/content.xml");
    String presentationXml = controller.mathMLToPresentationML(content);
    String presentationXml2 = controller.mathMLToPresentationML(content);
    assertEquals(presentationXml, presentationXml2);
  }
  
  @Test
  public void testMathMLToPresentationMLCalledTwiceShouldntHaveXmlNode() throws Exception {
    BaseRestImpl controller = Mockito.mock(BaseRestImpl.class, CALLS_REAL_METHODS);
    String content = super.readFile("testFiles/mathml/content.xml");
    String presentationXml = controller.mathMLToPresentationML(content);
    assertFalse(presentationXml.startsWith("<?xml"));
  }
  
  

}
