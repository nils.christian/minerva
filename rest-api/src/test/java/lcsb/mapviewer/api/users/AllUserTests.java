package lcsb.mapviewer.api.users;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ UserRestImplTest.class })
public class AllUserTests {

}
