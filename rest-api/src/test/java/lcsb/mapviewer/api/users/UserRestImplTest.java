package lcsb.mapviewer.api.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.UserGuiPreference;

public class UserRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(UserRestImplTest.class);

  @Autowired
  UserRestImpl userRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetUser() throws Exception {
    try {
      Object response = userRestImpl.getUser(token, Configuration.ANONYMOUS_LOGIN, "");
      assertNotNull(response);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdatePrivileges() throws Exception {
    try {
      Map<String, Object> data = new HashMap<>();
      data.put(PrivilegeType.ADD_MAP.name(), 1);
      userRestImpl.updatePrivileges(adminToken, Configuration.ANONYMOUS_LOGIN, data);
      assertTrue(userService.userHasPrivilege(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN),
          PrivilegeType.ADD_MAP));

      data.put(PrivilegeType.ADD_MAP.name(), 0);
      userRestImpl.updatePrivileges(adminToken, Configuration.ANONYMOUS_LOGIN, data);
      assertFalse(userService.userHasPrivilege(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN),
          PrivilegeType.ADD_MAP));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdatePreferences() throws Exception {
    try {
      Map<String, Object> data = deserialize(
          userRestImpl.getUser(adminToken, Configuration.ANONYMOUS_LOGIN, "preferences"));
      userRestImpl.updatePreferences(adminToken, Configuration.ANONYMOUS_LOGIN,
          (Map<String, Object>) data.get("preferences"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Map<String, Object> deserialize(Map<String, Object> data)
      throws JsonParseException, JsonMappingException, IOException {
    String body = new Gson().toJson(data);
    ObjectMapper mapper = new ObjectMapper();
    ObjectNode result = mapper.readValue(body, ObjectNode.class);
    return mapper.convertValue(result, Map.class);
  }

  @Test
  public void testGetUsers() throws Exception {
    try {
      Object response = userRestImpl.getUsers(adminToken, "");
      assertNotNull(response);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPrepareGuiPreferencesSimple() throws Exception {
    try {
      Map<String, Object> response = userRestImpl.prepareGuiPreferences(new HashSet<>());
      assertNotNull(response);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPrepareGuiPreferencesWithOption() throws Exception {
    try {
      Set<UserGuiPreference> guiPreferences = new HashSet<>();
      UserGuiPreference option = new UserGuiPreference();
      option.setKey("TestKey");
      option.setValue("TestValue");
      guiPreferences.add(option);

      Map<String, Object> response = userRestImpl.prepareGuiPreferences(guiPreferences);
      assertNotNull(response);
      assertEquals("TestValue", response.get(option.getKey()));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
