package lcsb.mapviewer.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class BaseControllerTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParsePostBody() {
    BaseController controller = Mockito.mock(BaseController.class, CALLS_REAL_METHODS);
    Map<String, String> res = controller.parsePostBody("x=y&");
    assertEquals("y", res.get("x"));
  }

  @Test
  public void testParsePostBody2() {
    BaseController controller = Mockito.mock(BaseController.class, CALLS_REAL_METHODS);
    Map<String, String> res = controller.parsePostBody("x=");
    assertEquals("", res.get("x"));
  }

  @Test
  public void testParsePostBody3() {
    BaseController controller = Mockito.mock(BaseController.class, CALLS_REAL_METHODS);
    Map<String, String> res = controller.parsePostBody("x");
    assertTrue(res.keySet().contains("x"));
    assertEquals(null, res.get("x"));
  }

  @Test
  public void testParsePostBody4() {
    BaseController controller = Mockito.mock(BaseController.class, CALLS_REAL_METHODS);
    Map<String, String> res = controller.parsePostBody("a=b&c=test%20string%20%3D%20xyz");
    assertEquals("b", res.get("a"));
    assertEquals("test string = xyz", res.get("c"));
  }

}
