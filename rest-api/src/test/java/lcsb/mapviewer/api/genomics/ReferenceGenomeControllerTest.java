package lcsb.mapviewer.api.genomics;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;

public class ReferenceGenomeControllerTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(ReferenceGenomeControllerTest.class);

  @Autowired
  public ReferenceGenomeRestImpl referenceGenomeRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetForNonExistingType() throws Exception {
    try {
      referenceGenomeRestImpl.getReferenceGenome(token, "9606", "type", "ver");
      fail("Exception expected");
    } catch (QueryException e2) {
      assertTrue(e2.getMessage().contains("Cannot find type"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetForNonExistingOrganism() throws Exception {
    try {
      referenceGenomeRestImpl.getReferenceGenome(token, "960000", "UCSC", "ver");
      fail("Exception expected");
    } catch (ObjectNotFoundException e2) {
      assertTrue(e2.getMessage().contains("Cannot find requested reference genome"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetReferenceGenomeTaxonomies() throws Exception {
    try {
      List<Map<String, Object>> result = referenceGenomeRestImpl.getReferenceGenomeTaxonomies(token);
      assertNotNull(result);
      assertTrue(result.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetReferenceGenomeTypes() throws Exception {
    try {
      List<Map<String, Object>> result = referenceGenomeRestImpl.getReferenceGenomeTypes(token,"9606");
      assertNotNull(result);
      assertTrue(result.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetReferenceGenomeVersions() throws Exception {
    try {
      List<Map<String, Object>> result = referenceGenomeRestImpl.getReferenceGenomeVersions(token,"9606","UCSC");
      assertNotNull(result);
      assertTrue(result.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetRemoteUrls() throws Exception {
    try {
      List<Map<String, Object>> result = referenceGenomeRestImpl.getRemoteUrls(adminToken, "9606", "UCSC", "hg18");
      assertNotNull(result);
      assertTrue(result.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAllDowloadedReferenceGenomes() throws Exception {
    try {
      List<?> result = referenceGenomeRestImpl.getReferenceGenomes(token);
      assertNotNull(result);
      assertTrue(result.size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }


}
