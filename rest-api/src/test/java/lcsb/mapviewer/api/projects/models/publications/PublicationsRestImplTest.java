package lcsb.mapviewer.api.projects.models.publications;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class PublicationsRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(PublicationsRestImplTest.class);

  @Autowired
  PublicationsRestImpl _projectRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPublications() throws Exception {
    try {
      PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = projectRest.getPublications("sample", "*", token, "0", 20, "", "", "");
      assertEquals(1, result.get("totalSize"));
      assertEquals(0, result.get("start"));
      assertEquals(1, ((List<?>) result.get("data")).size());

      result = projectRest.getPublications("sample", "*", token, "1", 20, "", "", "");
      assertEquals(1, result.get("totalSize"));
      assertEquals(1, result.get("start"));
      assertEquals(0, ((List<?>) result.get("data")).size());

      result = projectRest.getPublications("sample", "*", token, "0", 00, "", "", "");
      assertEquals(1, result.get("totalSize"));
      assertEquals(0, result.get("start"));
      assertEquals(0, ((List<?>) result.get("data")).size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPublicationsFiltering() throws Exception {
    try {
      PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = projectRest.getPublications("sample", "*", token, "0", 20, "pubmedId", "asc", "123");
      assertEquals(1, result.get("totalSize"));
      assertEquals(0, result.get("start"));
      assertEquals(1, ((List<?>) result.get("data")).size());

      result = projectRest.getPublications("sample", "*", token, "0", 20, "pubmedId", "asc", "1234");
      assertEquals(1, result.get("totalSize"));
      assertEquals(0, result.get("start"));
      assertEquals(0, ((List<?>) result.get("data")).size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPublicationsInvalidSortingFiltering() throws Exception {
    try {
      PublicationsRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      projectRest.getPublications("sample", "*", token, "0", 20, "xxx", "asc", "123");
      fail("Exception expected");
    } catch (QueryException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private PublicationsRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _projectRestImpl.setModelService(mockModelService);
    return _projectRestImpl;
  }

}
