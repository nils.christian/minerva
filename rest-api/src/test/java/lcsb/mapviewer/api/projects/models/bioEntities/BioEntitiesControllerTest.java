package lcsb.mapviewer.api.projects.models.bioEntities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class BioEntitiesControllerTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(BioEntitiesControllerTest.class);

  @Autowired
  BioEntitiesRestImpl _bioEntitiesRestImpl;

  ObjectMapper mapper = new ObjectMapper();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetClosestElementsByCoordinates() throws Exception {
    try {
      BioEntitiesRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      int count = 3;
      List<Map<String, Object>> result = projectRest.getClosestElementsByCoordinates("sample", "0", token,
          new Point2D.Double(1, 2), count, "false", "");
      assertEquals(count, result.size());

      String json = mapper.writeValueAsString(result);
      assertNotNull(json);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private BioEntitiesRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _bioEntitiesRestImpl.setModelService(mockModelService);
    return _bioEntitiesRestImpl;
  }

}
