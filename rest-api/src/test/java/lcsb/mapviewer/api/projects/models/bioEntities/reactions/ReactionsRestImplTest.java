package lcsb.mapviewer.api.projects.models.bioEntities.reactions;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ReactionsRestImplTest extends RestTestFunctions {

  @Autowired
  ReactionsRestImpl reactionsRestImpl;

  @Test
  public void testLineCreationForReactant() {
    Reactant reactant = new Reactant();
    reactant.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(1, 0)));
    List<?> lines = reactionsRestImpl.getLines(reactant);
    assertEquals(1, lines.size());
  }

  @Test
  public void testLineCreationForProduct() {
    Product reactant = new Product();
    reactant.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(1, 0)));
    List<?> lines = reactionsRestImpl.getLines(reactant);
    assertEquals(1, lines.size());
  }

  @Test
  public void testLineCreationForModifier() {
    Modifier modifier = new Modifier(new GenericProtein("id"));
    modifier.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(1, 0)));
    List<?> lines = reactionsRestImpl.getLines(modifier);
    assertEquals(1, lines.size());
  }

}
