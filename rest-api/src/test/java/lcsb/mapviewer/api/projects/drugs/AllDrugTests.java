package lcsb.mapviewer.api.projects.drugs;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DrugRestImplTest.class })
public class AllDrugTests {

}
