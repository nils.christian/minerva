package lcsb.mapviewer.api.projects.drugs;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class DrugRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(DrugRestImplTest.class);

  @Autowired

  private DrugRestImpl _drugRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetDrugsByQuery() throws Exception {
    try {
      DrugRestImpl drugRestImpl = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = drugRestImpl.getDrugsByQuery(token, "sample", "", "nadh").get(0);
      List<Map<String, Object>> references = (List<Map<String, Object>>) result.get("references");
      Map<String, Object> reference = references.get(0);
      assertNotNull(reference.get("type"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private DrugRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _drugRestImpl.setModelService(mockModelService);
    return _drugRestImpl;
  }

}
