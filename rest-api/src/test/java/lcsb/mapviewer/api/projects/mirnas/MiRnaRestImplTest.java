package lcsb.mapviewer.api.projects.mirnas;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;

public class MiRnaRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(MiRnaRestImplTest.class);

  @Autowired
  MiRnaRestImpl _miRnaRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetMiRnasByTarget() throws Exception {
    try {
      MiRnaRestImpl miRnaRestImpl = createMockProjectRest("testFiles/model/mi_rna_target.xml");
      List<Map<String, Object>> result = miRnaRestImpl.getMiRnasByTarget(token, "mi_rna_target", "ALIAS", "0",
          "");
      Set<String> names = new HashSet<>();
      for (Map<String, Object> miRna : result) {
        assertFalse("Duplicate miRna found: " + miRna.get("name"), names.contains(miRna.get("name")));
        names.add((String) miRna.get("name"));
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private MiRnaRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _miRnaRestImpl.setModelService(mockModelService);
    return _miRnaRestImpl;
  }

}
