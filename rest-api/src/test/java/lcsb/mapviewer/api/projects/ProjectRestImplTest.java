package lcsb.mapviewer.api.projects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.Gson;

import lcsb.mapviewer.api.ObjectNotFoundException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.graphics.PdfImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class ProjectRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(ProjectRestImplTest.class);

  @Autowired
  ProjectRestImpl _projectRestImpl;

  @Autowired
  IModelService modelService;
  @Autowired
  IProjectService projectService;

  @Autowired
  ProjectDao projectDao;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetModelAsImageForInvalidConverter() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      projectRest.getModelAsImage(token, "sample", "0", "", "", "", "", "");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetModelAsImage() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      FileEntry result = projectRest.getModelAsImage(token, "sample", "0", PdfImageGenerator.class.getCanonicalName(),
          "", "", "", "");
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetModelDataDependencies() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = projectRest.getProject("sample", token);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));
      Mockito.verify(projectRest.getModelService(), times(0)).getLastModelByProjectId(anyString(), any());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test(expected = ObjectNotFoundException.class)
  public void testGetInvalidMetaData() throws Exception {
    ProjectRestImpl projectRest = createMockProjectRest(null);
    projectRest.getProject("unknown_model_id", token);
  }

  @Test
  public void testGetModelAsImage2() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      projectRest.getModelAsImage(token, "sample", "0", PdfImageGenerator.class.getCanonicalName(), "", "", "", "");
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetModelAsFileModel() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      projectRest.getModelAsModelFile(token, "sample", "0", "", "", "", "", "");
      fail("Exception expected");
    } catch (QueryException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetModelAsFileModel2() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      projectRest.getModelAsModelFile(token, "sample", "0", CellDesignerXmlParser.class.getCanonicalName(), "", "", "",
          "0,0;90,0;90,90;90,0");
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetModelAsFileModel3() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      projectRest.getModelAsModelFile(token, "sample", "0", "", "", "", "", "0,0;90,0;90,90;90,0");
      fail("Exception expected");
    } catch (QueryException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetProject() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = projectRest.getProject("sample", token);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdateProject() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, String> disease = new HashMap<>();
      disease.put("type", MiriamType.MESH_2012.name());
      disease.put("resource", "D010300");
      Map<String, Object> data = new HashMap<>();
      data.put("version", "1");
      data.put("name", "test");
      data.put("organism", null);
      data.put("disease", disease);
      data.put("projectId", "sample");
      data.put("id", "0");
      projectRest.updateProject(adminToken, "sample", data);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComputePathForProject() throws Exception {
    try {
      String project_id = "Some_id";
      String directory1 = _projectRestImpl.computePathForProject(project_id, ".");
      Project project = new Project();
      project.setProjectId(project_id);
      projectDao.add(project);
      projectDao.delete(project);
      String directory2 = _projectRestImpl.computePathForProject(project_id, ".");

      assertFalse(directory1.equals(directory2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetProjects() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      List<Map<String, Object>> result = projectRest.getProjects(token);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetStatistics() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = projectRest.getStatistics("sample", token);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));

      Map<?, ?> elementAnnotations = (Map<?, ?>) result.get("elementAnnotations");
      assertEquals(elementAnnotations.get(MiriamType.CAS), 0);
      assertTrue((Integer) elementAnnotations.get(MiriamType.ENTREZ) > 0);

      Map<?, ?> reactionAnnotations = (Map<?, ?>) result.get("reactionAnnotations");
      assertEquals(reactionAnnotations.get(MiriamType.ENTREZ), 0);
      assertEquals(reactionAnnotations.get(MiriamType.PUBMED), 1);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetMetaDataForComplexWithImages() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/complex_model_with_submaps.zip");
      Map<String, Object> result = projectRest.getProject("sample", token);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testExtractZipEntries() throws Exception {
    try {
      MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
      data.put("zip-entries[0][_type]", createLinkedList("MAP"));
      data.put("zip-entries[0][_filename]", createLinkedList("main.xml"));
      data.put("zip-entries[0][_data][root]", createLinkedList("true"));
      data.put("zip-entries[0][_data][name]", createLinkedList("main"));
      data.put("zip-entries[0][_data][type][id]", createLinkedList("UNKNOWN"));
      data.put("zip-entries[0][_data][type][name]", createLinkedList("Unknown"));
      data.put("zip-entries[1][_type]", createLinkedList("OVERLAY"));
      data.put("zip-entries[1][_filename]", createLinkedList("layouts/goodschema.txt"));
      data.put("zip-entries[1][_data][name]", createLinkedList("example name"));
      data.put("zip-entries[1][_data][description]", createLinkedList("layout description"));
      data.put("zip-entries[2][_type]", createLinkedList("IMAGE"));
      data.put("zip-entries[2][_filename]", createLinkedList("images/test.png"));
      List<ZipEntryFile> result = _projectRestImpl.extractZipEntries(data);
      assertNotNull(result);
      assertEquals(3, result.size());
      assertTrue(result.get(0) instanceof ModelZipEntryFile);
      assertTrue(result.get(1) instanceof LayoutZipEntryFile);
      assertTrue(result.get(2) instanceof ImageZipEntryFile);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private LinkedList<Object> createLinkedList(Object string) {
    LinkedList<Object> result = new LinkedList<>();
    result.add(string);
    return result;
  }

  private ProjectRestImpl createMockProjectRest(String string) throws Exception {
    Model model = null;
    Project project = null;
    if (string != null) {
      project = new Project();
      model = super.getModelForFile(string, true);
      project.addModel(model);
      project.setProjectId(model.getName());
      project.addWarning("some warning");
    }
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _projectRestImpl.setModelService(mockModelService);

    IProjectService projectServiceMock = Mockito.mock(IProjectService.class);
    Mockito.when(projectServiceMock.getProjectByProjectId(anyString(), any())).thenReturn(project);
    List<Project> projects = new ArrayList<>();
    projects.add(project);
    Mockito.when(projectServiceMock.getAllProjects(any())).thenReturn(projects);
    _projectRestImpl.setProjectService(projectServiceMock);

    return _projectRestImpl;
  }

  @Test
  public void testGetLogs() throws Exception {
    try {
      ProjectRestImpl projectRest = createMockProjectRest("testFiles/model/sample.xml");
      Map<String, Object> result = projectRest.getLogs("sample", "", token, "0", 10, "id", "asc", "");
      assertNotNull(result);
      Gson gson = new Gson();
      assertNotNull(gson.toJson(result));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
