package lcsb.mapviewer.api.projects.models;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.refEq;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class ModelRestImplTest extends RestTestFunctions {

  @Autowired
  ModelRestImpl _modelRestImpl;

  @Autowired
  IModelService modelService;

  @Autowired
  IProjectService projectService;

  @Before
  public void setUp() throws Exception {
    _modelRestImpl.setModelService(modelService);
    _modelRestImpl.setProjectService(projectService);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testModels() throws Exception {
    try {
      ModelRestImpl modelRest = createMockProjectRest("testFiles/model/sample.xml");
      List<Map<String, Object>> result = modelRest.getModels("sample", token);
      assertEquals(1, result.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetEmptyModels() throws Exception {
    try {
      ModelRestImpl modelRest = createEmptyProjectRest("test");
      List<Map<String, Object>> result = modelRest.getModels("test", token);
      assertEquals(0, result.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private ModelRestImpl createEmptyProjectRest(String string) throws Exception {
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(refEq(string), any())).thenReturn(null);

    IProjectService mockProjectService = Mockito.mock(IProjectService.class);
    Mockito.when(mockProjectService.getProjectByProjectId(refEq(string), any())).thenReturn(new Project());

    _modelRestImpl.setModelService(mockModelService);
    _modelRestImpl.setProjectService(mockProjectService);
    return _modelRestImpl;
  }

  private ModelRestImpl createMockProjectRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _modelRestImpl.setModelService(mockModelService);
    return _modelRestImpl;
  }

}
