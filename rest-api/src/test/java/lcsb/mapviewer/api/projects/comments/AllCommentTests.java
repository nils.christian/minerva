package lcsb.mapviewer.api.projects.comments;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CommentRestImplTest.class })
public class AllCommentTests {

}
