package lcsb.mapviewer.api.projects.models.bioEntities.elements;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.annotation.services.annotators.PdbAnnotator;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.services.interfaces.IModelService;

public class ElementRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(RestTestFunctions.class);

  @Autowired
  ElementsRestImpl _elementsRestImpl;

  @Autowired
  PdbAnnotator pdbAnnotator;

  ObjectMapper mapper = new ObjectMapper();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetElementsProcessAllColumns() throws Exception {
    try {
      ElementsRestImpl projectRest = createMockElementRest("testFiles/model/sample.xml", true);
      List<Map<String, Object>> result = projectRest.getElements("sample", "", "", "*", token, "", "", "");
      for (Map<String, Object> element : result) {
        for (String paramName : element.keySet()) {
          Object val = element.get(paramName);
          String paramValue = "";
          if (val != null) {
            paramValue = val.toString();
          }
          assertFalse("Improper handler for column name: " + paramName, paramValue.contains("Unknown column"));
        }
      }
      String json = mapper.writeValueAsString(result);
      assertNotNull(json);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetElementsByType() throws Exception {
    try {
      String proteinType = new GenericProtein("1").getStringType();
      ElementsRestImpl projectRest = createMockElementRest("testFiles/model/sample.xml", false);
      List<Map<String, Object>> result = projectRest.getElements("sample", "", "", "*", token, proteinType, "", "");
      assertEquals(12, result.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetElementsVisibility() throws Exception {
    try {
      ElementsRestImpl elementRest = createMockElementRest("testFiles/model/sample.xml");
      List<Map<String, Object>> result = elementRest.getElements("sample", "", "", "*", token, "", "", "");
      for (Map<String, Object> map : result) {
        assertTrue(map.get("hierarchyVisibilityLevel") instanceof String);
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetModificationsForProtein() throws Exception {
    try {
      String state = "asd";
      GenericProtein protein = new GenericProtein("s1");
      Residue mr = new Residue();
      mr.setState(ModificationState.ACETYLATED);
      mr.setName("S250");
      protein.addModificationResidue(mr);
      protein.setStructuralState(state);
      Map<String, Object> result = _elementsRestImpl.getOthersForElement(protein);
      assertNotNull(result.get("modifications"));
      assertEquals(state, result.get("structuralState"));
      List<Map<String, Object>> modifications = (List<Map<String, Object>>) result.get("modifications");
      assertEquals(1, modifications.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetModificationsForRna() throws Exception {
    try {
      String state = "asd";
      Rna rna = new Rna("s1");
      ModificationSite mr = new ModificationSite();
      mr.setState(ModificationState.ACETYLATED);
      mr.setName("S250");
      rna.addRegion(mr);
      rna.setState(state);
      Map<String, Object> result = _elementsRestImpl.getOthersForElement(rna);
      assertNotNull(result.get("modifications"));
      assertEquals(state, result.get("structuralState"));
      List<Map<String, Object>> modifications = (List<Map<String, Object>>) result.get("modifications");
      assertEquals(1, modifications.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetModificationsForAntisenseRna() throws Exception {
    try {
      String state = "asd";
      AntisenseRna antisenseRna = new AntisenseRna("s1");
      ModificationSite mr = new ModificationSite();
      mr.setName("S250");
      antisenseRna.addRegion(mr);
      antisenseRna.setState(state);
      Map<String, Object> result = _elementsRestImpl.getOthersForElement(antisenseRna);
      logger.debug(result);
      assertNotNull(result.get("modifications"));
      assertEquals(state, result.get("structuralState"));
      List<Map<String, Object>> modifications = (List<Map<String, Object>>) result.get("modifications");
      assertEquals(1, modifications.size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private ElementsRestImpl createMockElementRest(String string) throws Exception {
    return createMockElementRest(string, false);
  }

  private ElementsRestImpl createMockElementRest(String string, Boolean annotate) throws Exception {
    Model model = super.getModelForFile(string, true);
    if (annotate) {
      try {
        Protein protein = new GenericProtein("SNCA");
        protein.setElementId("SNCA");
        pdbAnnotator.annotateElement(protein);
        model.addElement(protein);
      } catch (Exception e) {
      }
    }
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _elementsRestImpl.setModelService(mockModelService);
    return _elementsRestImpl;
  }
}
