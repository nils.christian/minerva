package lcsb.mapviewer.api.projects.models.functions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FunctionsRestImplTest.class })
public class AllFunctionsTests {

}
