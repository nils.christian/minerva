package lcsb.mapviewer.api.projects.chemicals;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.chemical.IChemicalService;

public class ChemicalRestImplTest extends RestTestFunctions {
  Logger logger = Logger.getLogger(ChemicalRestImplTest.class);

  @Autowired

  private ChemicalRestImpl _drugRestImpl;

  @Autowired
  private IChemicalService chemicalService;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPrepareChemical() throws Exception {
    try {
      Chemical chemical = new Chemical();
      chemical.setChemicalId(new MiriamData(MiriamType.MESH_2012, "D010300"));
      Map<String, Object> result = _drugRestImpl.prepareChemical(chemical, _drugRestImpl.createChemicalColumnSet(""),
          new ArrayList<>());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPrepareChemicalWithProblematicMesh() throws Exception {
    try {
      Chemical chemical = chemicalService.getByName("Tetrachlorodibenzodioxin",
          new DbSearchCriteria().disease(new MiriamData(MiriamType.MESH_2012, "D010300")));
      Map<String, Object> result = _drugRestImpl.prepareChemical(chemical, _drugRestImpl.createChemicalColumnSet(""),
          new ArrayList<>());
      assertNotNull(result);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  protected ChemicalRestImpl createMockChemicalRest(String string) throws Exception {
    Model model = super.getModelForFile(string, true);
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(anyString(), any())).thenReturn(model);
    _drugRestImpl.setModelService(mockModelService);
    return _drugRestImpl;
  }

}
