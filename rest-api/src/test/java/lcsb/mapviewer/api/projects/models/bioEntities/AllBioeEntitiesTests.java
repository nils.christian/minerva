package lcsb.mapviewer.api.projects.models.bioEntities;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.api.projects.models.bioEntities.elements.AllElementsTests;

@RunWith(Suite.class)
@SuiteClasses({ //
		AllElementsTests.class, //
		BioEntitiesControllerTest.class //
})
public class AllBioeEntitiesTests {

}
