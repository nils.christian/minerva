package lcsb.mapviewer.api.projects.comments;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.RestTestFunctions;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;

public class CommentRestImplTest extends RestTestFunctions {

  @Autowired
  public CommentRestImpl _commentRestImpl;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetForNonExisting() throws Exception {
    try {
      _commentRestImpl.getCommentList(token, "unk", "", "", "", "");
      fail("Exception expected");
    } catch (QueryException e2) {
      assertTrue(e2.getMessage().contains("Project with given id doesn't exist"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetCommentsDependencies() throws Exception {
    try {
      CommentRestImpl commentRestImpl = createMockProjectRest("testFiles/model/sample.xml");
      commentRestImpl.getCommentList(adminToken, "sample", "", "", "", "");
      Mockito.verify(commentRestImpl.getModelService(), times(0)).getLastModelByProjectId(anyString(), any());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private CommentRestImpl createMockProjectRest(String string) throws Exception {
    Model model = null;
    Project project = null;
    String modelName = "";
    if (string != null) {
      project = new Project();
      model = super.getModelForFile(string, true);
      project.addModel(model);
      modelName = model.getName();
    }
    IModelService mockModelService = Mockito.mock(IModelService.class);
    Mockito.when(mockModelService.getLastModelByProjectId(eq(modelName), any())).thenReturn(model);
    _commentRestImpl.setModelService(mockModelService);

    IProjectService projectServiceMock = Mockito.mock(IProjectService.class);
    Mockito.when(projectServiceMock.getProjectByProjectId(eq(modelName), any())).thenReturn(project);
    List<Project> projects = new ArrayList<>();
    projects.add(project);
    Mockito.when(projectServiceMock.getAllProjects(any())).thenReturn(projects);
    _commentRestImpl.setProjectService(projectServiceMock);

    return _commentRestImpl;
  }

}
