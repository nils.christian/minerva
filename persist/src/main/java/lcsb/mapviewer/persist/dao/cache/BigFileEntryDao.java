package lcsb.mapviewer.persist.dao.cache;

import java.util.List;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
public class BigFileEntryDao extends BaseDao<BigFileEntry> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = Logger.getLogger(BigFileEntryDao.class);

  /**
   * Default constructor.
   */
  public BigFileEntryDao() {
    super(BigFileEntry.class, "removed");
  }

  /**
   * Return {@link BigFileEntry} identified by remote url.
   * 
   * @param url
   *          url of the file
   * @return {@link BigFileEntry} identified by remote url
   */
  public BigFileEntry getByUrl(String url) {
    List<?> list = getElementsByParameter("url", url);
    if (list.size() == 0) {
      return null;
    }
    return (BigFileEntry) list.get(0);
  }

  public List<BigFileEntry> getAllByUrl(String url) {
    return getElementsByParameter("url", url);
  }

  @Override
  public void delete(BigFileEntry entry) {
    entry.setRemoved(true);
    update(entry);
  }

}
