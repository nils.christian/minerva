package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link Reaction} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionDao extends BaseDao<Reaction> {

	/**
	 * Default constructor.
	 */
	public ReactionDao() {
		super(Reaction.class);
	}

}
