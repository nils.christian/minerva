package lcsb.mapviewer.persist.dao.map.layout;

import java.util.List;

import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link ReferenceGenome} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ReferenceGenomeDao extends BaseDao<ReferenceGenome> {

	/**
	 * Default constructor.
	 */
	public ReferenceGenomeDao() {
		super(ReferenceGenome.class);
	}

	/**
	 * List all reference genomes for specific reference genome type.
	 * 
	 * @param ucsc
	 *          reference genome type
	 * @return list of reference genomes for a given type
	 */
	public List<ReferenceGenome> getByType(ReferenceGenomeType ucsc) {
		return getElementsByParameter("type", ucsc);
	}

}
