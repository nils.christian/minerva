package lcsb.mapviewer.persist.dao.map.layout;

import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link ReferenceGenomeGeneMapping} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ReferenceGenomeGeneMappingDao extends BaseDao<ReferenceGenomeGeneMapping> {

	/**
	 * Default constructor.
	 */
	public ReferenceGenomeGeneMappingDao() {
		super(ReferenceGenomeGeneMapping.class);
	}

}
