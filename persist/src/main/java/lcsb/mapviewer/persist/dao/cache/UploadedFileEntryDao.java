package lcsb.mapviewer.persist.dao.cache;

import org.apache.log4j.Logger;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
public class UploadedFileEntryDao extends BaseDao<UploadedFileEntry> {
	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(UploadedFileEntryDao.class);

	/**
	 * Default constructor.
	 */
	public UploadedFileEntryDao() {
		super(UploadedFileEntry.class, "removed");
	}

	@Override
	public void delete(UploadedFileEntry entry) {
		entry.setRemoved(true);
		update(entry);
	}

}
