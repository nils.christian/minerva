package lcsb.mapviewer.persist.dao.user;

import lcsb.mapviewer.model.user.ObjectPrivilege;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object class for Privilege objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class PrivilegeDao extends BaseDao<ObjectPrivilege> {

	/**
	 * Default constructor.
	 */
	public PrivilegeDao() {
		super(ObjectPrivilege.class);
	}

}
