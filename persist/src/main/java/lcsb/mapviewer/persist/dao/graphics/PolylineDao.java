package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link PolylineDao} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class PolylineDao extends BaseDao<PolylineData> {

	/**
	 * Default constructor.
	 */
	public PolylineDao() {
		super(PolylineData.class);
	}

}
