package lcsb.mapviewer.persist.dao.plugin;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object class for {@link PluginDataEntry} objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class PluginDataEntryDao extends BaseDao<PluginDataEntry> {
  /**
   * Default constructor.
   */
  public PluginDataEntryDao() {
    super(PluginDataEntry.class);
  }

  public PluginDataEntry getByKey(Plugin plugin, String key, User user) {
    List<Pair<String, Object>> params = new ArrayList<>();
    params.add(new Pair<String, Object>("plugin_iddb", plugin.getId()));
    params.add(new Pair<String, Object>("entry_key", key));
    if (user == null) {
      params.add(new Pair<String, Object>("user_iddb", null));
    } else {
      params.add(new Pair<String, Object>("user_iddb", user.getId()));
    }
    List<PluginDataEntry> entries = getElementsByParameters(params);
    if (entries.size() > 0) {
      return entries.get(0);
    } else {
      return null;
    }
  }

}
