package lcsb.mapviewer.persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.dialect.Dialect;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.service.jdbc.dialect.internal.StandardDialectResolver;
import org.hibernate.service.jdbc.dialect.spi.DialectResolver;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Data access object class for Project objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class ProjectDao extends BaseDao<Project> {
  /**
   * Default constructor.
   */
  public ProjectDao() {
    super(Project.class);
  }

  /**
   * Returns project with the given {@link Project#projectId project identifier} .
   * 
   * @param projectId
   *          {@link Project#projectId project identifier}
   * @return project for the given name
   */
  public Project getProjectByProjectId(String projectId) {
    Project project = getByParameter("projectId", projectId);

    return project;
  }

  /**
   * Returns project that contains model with a given id.
   * 
   * @param id
   *          id of the model
   * @return project that contains model with a given id
   */
  public Project getProjectForModelId(Integer id) {
    List<?> list = getSession()
        .createQuery("select project from " + ModelData.class.getSimpleName() + " model_t where model_t.id = :id")
        .setParameter("id", id).list();
    if (list.size() == 0) {
      return null;
    }
    return (Project) list.get(0);
  }

  /**
   * Returns information if the project with given name exists.
   * 
   * @param projectName
   *          name of the project
   * @return <code>true</code> if project with given name exists,
   *         <code>false</code> otherwise
   */
  public boolean isProjectExistsByName(String projectName) {
    @SuppressWarnings("unchecked")
    List<Long> list = getSession()
        .createQuery("select count(*) from " + getClazz().getSimpleName() + " where projectId = :projectId")
        .setParameter("projectId", projectName).list();
    return list.get(0) > 0;
  }

  public long getNextId() {
    ReturningWork<Long> maxReturningWork = new ReturningWork<Long>() {
      @Override
      public Long execute(Connection connection) throws SQLException {
        DialectResolver dialectResolver = new StandardDialectResolver();
        Dialect dialect = dialectResolver.resolveDialect(connection.getMetaData());
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
          preparedStatement = connection.prepareStatement(dialect.getSequenceNextValString("project_table_iddb_seq"));
          resultSet = preparedStatement.executeQuery();
          resultSet.next();
          return resultSet.getLong(1);
        } catch (SQLException e) {
          throw e;
        } finally {
          if (preparedStatement != null) {
            preparedStatement.close();
          }
          if (resultSet != null) {
            resultSet.close();
          }
        }

      }
    };
    return getSession().doReturningWork(maxReturningWork);
  }

}
