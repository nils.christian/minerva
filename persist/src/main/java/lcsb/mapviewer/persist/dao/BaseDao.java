package lcsb.mapviewer.persist.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.persist.DbUtils;

/**
 * Abstract interface for data access object in the system. Containts the basic
 * generic functionality.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          type for which data access object is created
 */
public abstract class BaseDao<T> {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(BaseDao.class);

  /**
   * Sometimes objects have a flag that indicate that the object was removed from
   * the system (but is still there because of foreign keys etc). This column
   * determines the name of the column. If its set to null then such column
   * doesn't exist.
   */
  private String removableColumn = null;

  /**
   * Class of the object that DAO works on.
   */
  private Class<? extends T> clazz;

  /**
   * Default constructor.
   * 
   * @param theClass
   *          class of the object that DAO will work on
   */
  public BaseDao(Class<? extends T> theClass) {
    this.clazz = theClass;
  }

  /**
   * Default constructor.
   * 
   * @param theClass
   *          class of the object that DAO will work on
   * @param removableColumn
   *          determines the column in the object table that indicates if object
   *          should be considered as removed or not (see also:
   *          {@link #removableColumn}.
   * 
   */
  public BaseDao(Class<? extends T> theClass, String removableColumn) {
    this(theClass);
    this.removableColumn = removableColumn;
  }

  /**
   * Utils that help to manage the sessions in custom multithreaded
   * implementation.
   */
  @Autowired
  private DbUtils dbUtils;

  /**
   * Adds object to the database.
   * 
   * @param object
   *          object to add to database
   */
  public void add(T object) {
    getSession().save(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Flush connection with database.
   */
  public void flush() {
    getSession().flush();
  }

  /**
   * Commit current transaction.
   */
  public void commit() {
    if (!dbUtils.isCustomSessionForCurrentThread()) {
      logger.warn("Manual commit with spring managed session!", new Exception());
    }

    getSession().getTransaction().commit();
    getSession().beginTransaction();
  }

  /**
   * Returns current session.
   * 
   * @return session for current thread
   */
  protected Session getSession() {
    return dbUtils.getSessionForCurrentThread();
  }

  /**
   * Update object in the database.
   * 
   * @param object
   *          object to be updated
   */
  public void update(T object) {
    getSession().update(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Removes object from the database.
   * 
   * @param object
   *          object to be removed from database
   */
  public void delete(T object) {
    getSession().delete(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * "Disconnects" object with the element in database. From this point on we
   * cannot update/delete it in the database.
   * 
   * @param object
   *          object that should be evicted
   */
  public void evict(T object) {
    getSession().evict(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Returns number of elements in the table for this object.
   * 
   * @return number of all elements in database
   */
  public long getCount() {
    if (removableColumn == null) {
      return (Long) getSession().createCriteria(this.clazz).setProjection(Projections.rowCount()).uniqueResult();
    } else {
      return (Long) getSession().createCriteria(this.clazz).add(Restrictions.eq(removableColumn, false))
          .setProjection(Projections.rowCount()).uniqueResult();
    }
  }

  /**
   * Returns an element that has a key parameter equal to value. If there is more
   * than one element than one of them will be returned. If there is no such
   * element then <code>null</code> is returned.
   * 
   * @param key
   *          which parameter will filter the data
   * @param value
   *          what must be the value of parameter key
   * @return element that fulfill T.key=value criteria
   */
  @SuppressWarnings("unchecked")
  protected T getByParameter(String key, Object value) {
    List<?> list = getSession()
        .createQuery(
            " from " + this.clazz.getSimpleName() + " where " + key + " = :param_val " + removableAndStatemant())
        .setParameter("param_val", value).list();
    if (list.size() == 0) {
      return null;
    } else {
      return (T) list.get(0);
    }
  }

  /**
   * Returns the list of elements that have a key parameter equal to value.
   * 
   * @param key
   *          which parameter will filter the data
   * @param value
   *          what must be the value of parameter key
   * @return list of elements that fulfill T.key=value criteria
   */
  @SuppressWarnings("unchecked")
  protected List<T> getElementsByParameter(String key, Object value) {
    List<?> list = getSession()
        .createQuery(
            " from " + this.clazz.getSimpleName() + " where " + key + " = :param_val " + removableAndStatemant())
        .setParameter("param_val", value).list();
    return (List<T>) list;
  }

  @SuppressWarnings("unchecked")
  protected List<T> getElementsByParameters(List<Pair<String, Object>> params) {
    String queryString = " from " + this.clazz.getSimpleName() + " where ";
    boolean firstParam = true;
    for (Pair<String, Object> param : params) {
      String key = param.getLeft();
      if (!firstParam) {
        queryString += " AND ";
      }
      if (param.getRight() == null) {
        queryString += key + " is null ";
      } else {
        queryString += key + " = :param_val_" + key + " ";
      }

      firstParam = false;
    }
    queryString += removableAndStatemant();
    Query query = getSession().createQuery(queryString);
    for (Pair<String, Object> param : params) {
      String key = param.getLeft();
      Object value = param.getRight();
      if (value != null) {
        query = query.setParameter("param_val_" + key, value);
      }
    }
    List<?> list = query.list();
    return (List<T>) list;
  }

  /**
   * Returns element with the id given as parameter.
   * 
   * @param id
   *          database identifier
   * @return object width identifier given as parameter
   */
  @SuppressWarnings("unchecked")
  public T getById(int id) {
    List<?> list = getSession()
        .createQuery(" from " + this.clazz.getSimpleName() + " where id=? " + removableAndStatemant())
        .setParameter(0, id).list();
    if (list.size() == 0) {
      return null;
    } else {
      return (T) list.get(0);
    }
  }

  /**
   * Returns list of all object in db.
   * 
   * @return list of all object in db
   */
  @SuppressWarnings("unchecked")
  public List<T> getAll() {
    List<?> list = getSession().createQuery(" from " + this.clazz.getSimpleName() + " " + removableStatemant()).list();
    return (List<T>) list;
  }

  /**
   * Removes all elements from the database.
   */
  public void clearTable() {
    String stringQuery = "DELETE FROM " + this.clazz.getSimpleName();
    getSession().createQuery(stringQuery).executeUpdate();
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Refresh object with the new data taken from database.
   * 
   * @param object
   *          object to be refreshed
   */
  public void refresh(Object object) {
    getSession().refresh(object);
  }

  /**
   * @return the clazz
   */
  protected Class<? extends T> getClazz() {
    return clazz;
  }

  /**
   * Returns part of HQL statement responsible for {@link #removableColumn}.
   * 
   * @return part of HQL statement responsible for {@link #removableColumn}.
   */
  protected String removableAndStatemant() {
    if (removableColumn == null) {
      return "";
    } else {
      return " AND " + removableColumn + " = false ";
    }
  }

  /**
   * Returns part of HQL statement responsible for {@link #removableColumn}
   * (without sql AND).
   * 
   * @return part of HQL statement responsible for {@link #removableColumn}
   *         (without sql AND).
   */
  protected String removableStatemant() {
    if (removableColumn == null) {
      return "";
    } else {
      return " WHERE " + removableColumn + " = false ";
    }
  }

}
