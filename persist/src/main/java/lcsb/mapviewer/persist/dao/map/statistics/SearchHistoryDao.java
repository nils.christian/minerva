package lcsb.mapviewer.persist.dao.map.statistics;

import lcsb.mapviewer.model.map.statistics.SearchHistory;
import lcsb.mapviewer.persist.dao.BaseDao;

import org.apache.log4j.Logger;

/**
 * Data access object for {@link SearchHistory} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class SearchHistoryDao extends BaseDao<SearchHistory> {
	/**
	 * Default constructor.
	 */
	@SuppressWarnings("unused")
	private static Logger	logger	= Logger.getLogger(SearchHistoryDao.class);

	/**
	 * Default constructor.
	 */
	public SearchHistoryDao() {
		super(SearchHistory.class);
	}

}
