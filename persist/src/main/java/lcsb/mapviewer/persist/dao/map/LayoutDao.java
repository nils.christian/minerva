package lcsb.mapviewer.persist.dao.map;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for {@link Layout} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayoutDao extends BaseDao<Layout> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(LayoutDao.class);

  /**
   * Default constructor.
   */
  public LayoutDao() {
    super(Layout.class);
  }

  /**
   * Lists layouts for the model.
   *
   * @param model
   *          for this model layouts will be listed
   * @return list of layouts for the model
   */
  public List<Layout> getLayoutsByModel(Model model) {
    List<Layout> layouts = getElementsByParameter("model_iddb", model.getId());
    for (Layout layout : layouts) {
      refresh(layout);
    }
    return layouts;
  }

  public List<Layout> getLayoutsByModel(Model model, User creator, Boolean isPublic) {
    List<Pair<String, Object>> params = new ArrayList<>();
    params.add(new Pair<String, Object>("model_iddb", model.getId()));
    if (isPublic != null) {
      params.add(new Pair<String, Object>("publicLayout", isPublic));
    }
    if (creator != null) {
      params.add(new Pair<String, Object>("creator_iddb", creator.getId()));
    }
    List<Layout> layouts = getElementsByParameters(params);
    return layouts;
  }

  /**
   * Return number of layouts created by the user.
   *
   * @param user
   *          the user
   * @return number of layouts created by the user
   */
  public long getCountByUser(User user) {
    Criteria crit = getSession().createCriteria(this.getClazz());
    crit.setProjection(Projections.rowCount());
    crit.add(Restrictions.eq("creator", user));
    crit.createAlias("model", "m");
    crit.add(Restrictions.sizeLe("m.parentModels", 0));
    return (Long) crit.uniqueResult();
  }

  /**
   * Returns layout identified by name and model.
   *
   * @param model
   *          model where the layouts lay on
   * @param name
   *          name of the layout
   * @return layout
   */
  public Layout getLayoutByName(Model model, String name) {
    List<Layout> layouts = getElementsByParameter("model_iddb", model.getId());
    for (Layout layout : layouts) {
      refresh(layout);
      if (layout.getTitle().equals(name)) {
        return layout;
      }
    }
    return null;
  }
}
