package lcsb.mapviewer.persist.dao.cache;

import java.util.List;

import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
public class CacheQueryDao extends BaseDao<CacheQuery> {
	
	/**
	 * Default constructor.
	 */
	public CacheQueryDao() {
		super(CacheQuery.class);
	}

	/**
	 * Returns the cached value based on the type of resource and identifier in
	 * remote resource.
	 * 
	 * @param identifier
	 *          identifier in remote resource
	 * @param type
	 *          type of the resource
	 * @return cached value, if value wasn't cached then null is returned
	 */
	public CacheQuery getByQuery(String identifier, Integer type) {
		List<?> list = getSession()
				.createQuery(" from " + this.getClazz().getSimpleName() + " where query = :query and type = :type").setParameter("query", identifier)
				.setParameter("type", type).list();
		if (list.size() == 0) {
			return null;
		}
		return (CacheQuery) list.get(0);
	}

	/**
	 * Returns the cached value based on the type of resource and identifier in
	 * remote resource.
	 * 
	 * @param identifier
	 *          identifier in remote resource
	 * @param type
	 *          type of the resource
	 * @return cached value, if value wasn't cached then null is returned
	 */
	public CacheQuery getByQuery(String identifier, CacheType type) {
		return getByQuery(identifier, type.getId());
	}

}
