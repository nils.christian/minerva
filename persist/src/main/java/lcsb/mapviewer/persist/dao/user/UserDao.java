package lcsb.mapviewer.persist.dao.user;

import java.util.List;

import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object class for User objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class UserDao extends BaseDao<User> {

  /**
   * Default constructor.
   */
  public UserDao() {
    super(User.class, "removed");
  }

  /**
   * Returns user with a given login and password.
   * 
   * @param login
   *          user login
   * @param password
   *          - user password (encrypted)
   * @return user for given login and password
   */
  public User getUserByLoginAndCryptedPassword(String login, String password) {
    List<?> list = getSession()
        .createQuery(" from User where login=:login and cryptedPassword =:passwd " + removableAndStatemant())
        .setParameter("login", login).setParameter("passwd", password).list();
    if (list.size() == 0) {
      return null;
    } else {
      User user = (User) list.get(0);
      return user;
    }
  }

  /**
   * Returns user with a given login.
   * 
   * @param login
   *          user login
   * @return user for a given login
   */
  public User getUserByLogin(String login) {
    return getByParameter("login", login);
  }

  /**
   * Returns user with a given email.
   * 
   * @param email
   *          user email
   * @return user for a given email
   */
  public User getUserByEmail(String email) {
    return getByParameter("email", email);
  }

  @Override
  public void delete(User object) {
    object.setRemoved(true);
    object.setLogin("[REMOVED]_" + object.getId() + "_" + object.getLogin());
    update(object);
  }
}
