package lcsb.mapviewer.persist.dao.log;

import java.util.List;

import org.hibernate.Query;

import lcsb.mapviewer.model.log.GenericLog;
import lcsb.mapviewer.persist.dao.BaseDao;

/**
 * Data access object class for Log objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class LogDao extends BaseDao<GenericLog> {
	/**
	 * Default constructor.
	 */
	public LogDao() {
		super(GenericLog.class);
	}

	/**
	 * Returns last logged event.
	 * 
	 * @return last log event
	 */
	public GenericLog getLastLog() {
		String queryString = "from " + this.getClazz().getSimpleName() + " order by id desc";
		Query query = getSession().createQuery(queryString);
		List<?> list = query.list();
		if (list.size() > 0) {
			return (GenericLog) list.get(0);
		} else {
			return null;
		}
	}

	@Override
	protected String removableStatemant() {
		return super.removableStatemant();
	}

}
