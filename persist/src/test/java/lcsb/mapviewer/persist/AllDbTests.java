package lcsb.mapviewer.persist;

import lcsb.mapviewer.persist.dao.AllDaoTests;
import lcsb.mapviewer.persist.mapper.Point2DMapperTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ApplicationContextLoaderTest.class, //
		AllDaoTests.class, //
		DbUtilsTest.class, //
		Point2DMapperTest.class, //
		SpringApplicationContextTest.class,//

})
public class AllDbTests {

}
