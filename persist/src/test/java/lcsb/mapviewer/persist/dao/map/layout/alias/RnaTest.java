package lcsb.mapviewer.persist.dao.map.layout.alias;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class RnaTest extends PersistTestFunctions {
  Logger logger = Logger.getLogger(RnaTest.class);

  int identifierCounter = 0;

  String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
    Project project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testRnaRegionInDb() throws Exception {
    try {
      Project project = new Project();
      project.setProjectId(projectId);

      Model model = createModel();

      project.addModel(model);
      projectDao.add(project);
      projectDao.evict(project);

      Project project2 = projectDao.getProjectByProjectId(projectId);
      assertNotNull(project2);
      assertFalse(project2.equals(project));
      assertEquals(project.getId(), project2.getId());

      Model model2 = new ModelFullIndexed(project2.getModels().iterator().next());

      Element sp = model.getElements().iterator().next();
      Rna ar = (Rna) sp;

      Element sp2 = model2.getElements().iterator().next();
      Rna ar2 = (Rna) sp2;

      projectDao.delete(project2);

      assertEquals(ar.getRegions().size(), ar2.getRegions().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    Rna alias = new Rna("As");
    alias.addRegion(new CodingRegion());
    alias.setX(1);
    alias.setY(2);
    alias.setWidth(10);
    alias.setHeight(20);
    model.addElement(alias);

    return model;
  }

}
