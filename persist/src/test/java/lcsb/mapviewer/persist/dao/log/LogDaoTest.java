package lcsb.mapviewer.persist.dao.log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.log.SystemLog;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class LogDaoTest extends PersistTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetLastLog() {
		try {
			logDao.clearTable();
			assertNull(logDao.getLastLog());
			logDao.add(new SystemLog());
			assertNotNull(logDao.getLastLog());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRemovableStatement() {
		try {
			assertEquals("",logDao.removableStatemant());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
