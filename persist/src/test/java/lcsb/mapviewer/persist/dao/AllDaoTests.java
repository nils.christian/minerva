package lcsb.mapviewer.persist.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import lcsb.mapviewer.persist.dao.cache.AllCacheDbTests;
import lcsb.mapviewer.persist.dao.graphics.AllGraphicsDaoTests;
import lcsb.mapviewer.persist.dao.log.AllLogTests;
import lcsb.mapviewer.persist.dao.map.AllMapDaoTests;
import lcsb.mapviewer.persist.dao.plugin.AllPluginTests;
import lcsb.mapviewer.persist.dao.user.AllUserTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({ AllCacheDbTests.class, //
    AllGraphicsDaoTests.class, //
    AllLogTests.class, //
    AllMapDaoTests.class, //
    AllPluginTests.class, //
    AllUserTests.class, //
    BaseDaoTest.class, //
    ConfigurationDaoTest.class, //
    ProjectDaoTest.class, //

})
public class AllDaoTests {

}
