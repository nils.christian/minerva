package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.persist.PersistTestFunctions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FeedbackTest extends PersistTestFunctions {

	@Before
	public void setUp() throws Exception {
		createUser();
	}

	@After
	public void tearDown() throws Exception {
		userDao.delete(user);
	}

	@Test
	public void testFeedback() throws Exception {
		try {
			int counter = (int) commentDao.getCount();
			Comment feedback = new Comment();
			feedback.setUser(user);
			commentDao.add(feedback);
			int counter2 = (int) commentDao.getCount();
			assertEquals(counter + 1, counter2);
			Comment feedback2 = commentDao.getById(feedback.getId());
			assertNotNull(feedback2);
			commentDao.delete(feedback);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
