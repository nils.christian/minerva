package lcsb.mapviewer.persist.dao.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.BasicPrivilege;
import lcsb.mapviewer.model.user.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.UserClassValidAnnotations;
import lcsb.mapviewer.model.user.UserGuiPreference;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class UserDaoTest extends PersistTestFunctions {

  @Autowired
  protected PasswordEncoder passwordEncoder;

  protected Logger logger = Logger.getLogger(UserDaoTest.class.getName());
  String testLogin = "test_login123";
  String testEmail = "a@a.pl";
  String testPasswd = "pwd";
  String testLogin2 = "test_login_tmp";
  String testName = "John";
  String testSurname = "Doe";

  @Before
  public void setUp() throws Exception {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("login_param", testLogin);
    params.put("login_param2", testLogin2);
    dbUtils.executeSqlQuery("delete from user_table where login = :login_param or login = :login_param2", params);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddUpdateDelete() {
    try {
      User user = userDao.getUserByLogin(testLogin);
      assertNull(user);

      long counter = userDao.getCount();

      user = new User();
      user.setLogin(testLogin);
      user.addPrivilege(new BasicPrivilege(0, PrivilegeType.ADD_MAP, user));
      userDao.add(user);

      long counter2 = userDao.getCount();
      assertEquals(counter + 1, counter2);

      user.setLogin(testLogin2);
      userDao.update(user);

      User user2 = userDao.getUserByLogin(testLogin2);
      assertNotNull(user2);

      userDao.delete(user);

      user2 = userDao.getUserByLogin(testLogin2);
      assertNull(user2);
      counter2 = userDao.getCount();
      assertEquals(counter, counter2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddDeleteAdd() {
    try {
      User user = userDao.getUserByLogin(testLogin);
      assertNull(user);

      long counter = userDao.getCount();

      user = new User();
      user.setLogin(testLogin);
      userDao.add(user);

      long counter2 = userDao.getCount();
      assertEquals(counter + 1, counter2);

      userDao.delete(user);

      User user2 = userDao.getUserByLogin(testLogin);
      assertNull(user2);

      user2 = new User();
      user2.setLogin(testLogin);
      userDao.add(user2);

      assertNotNull(userDao.getUserByLogin(testLogin));

      userDao.delete(user2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUserByLogin() throws Exception {
    try {
      User user = new User();
      user.setCryptedPassword(passwordEncoder.encode(testPasswd));
      user.setLogin(testLogin);
      userDao.add(user);
      User user2 = userDao.getUserByLogin(testLogin);
      assertNotNull(user2);
      assertEquals(user2.getId(), user.getId());
      assertEquals(user2.getLogin(), user.getLogin());
      assertEquals(user2.getCryptedPassword(), user.getCryptedPassword());
      user2 = userDao.getUserByLogin(testLogin2);
      assertNull(user2);
      userDao.delete(user);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetUserByEmail() throws Exception {
    try {
      User user = new User();
      user.setCryptedPassword(passwordEncoder.encode(testPasswd));
      user.setLogin(testLogin);
      user.setEmail(testEmail);
      userDao.add(user);
      User user2 = userDao.getUserByEmail(testEmail);
      assertNotNull(user2);
      assertEquals(user2.getId(), user.getId());
      assertEquals(user2.getLogin(), user.getLogin());
      assertEquals(user2.getCryptedPassword(), user.getCryptedPassword());
      user2 = userDao.getUserByEmail(testEmail + "sadas");
      assertNull(user2);
      userDao.delete(user);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetAll() throws Exception {
    try {
      assertTrue(userDao.getAll().size() > 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUserWithAnnotatorSchema() throws Exception {
    try {
      User user = new User();
      UserAnnotationSchema uas = new UserAnnotationSchema();
      uas.setValidateMiriamTypes(true);
      UserClassAnnotators ca = new UserClassAnnotators();
      ca.setClassName(Species.class);
      ca.addAnnotator(String.class);
      ca.addAnnotator(Integer.class);
      uas.addClassAnnotator(ca);
      uas.addClassAnnotator(new UserClassAnnotators(String.class, new ArrayList<String>()));
      UserClassValidAnnotations cva = new UserClassValidAnnotations();
      cva.setClassName(Reaction.class);
      cva.addValidMiriamType(MiriamType.HGNC);
      cva.addValidMiriamType(MiriamType.HGNC_SYMBOL);
      uas.addClassValidAnnotations(cva);
      uas.addClassValidAnnotations(new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>()));
      uas.addClassValidAnnotations(new UserClassValidAnnotations(Integer.class, new ArrayList<MiriamType>()));
      user.setAnnotationSchema(uas);
      userDao.add(user);
      userDao.evict(user);
      User user2 = userDao.getById(user.getId());
      assertNotNull(user2);
      UserAnnotationSchema uas2 = user2.getAnnotationSchema();
      assertNotNull(uas2);
      assertEquals(2, uas2.getClassAnnotators().size());
      assertEquals(3, uas2.getClassValidAnnotators().size());
      assertEquals(Species.class.getCanonicalName(), uas.getClassAnnotators().get(0).getClassName());
      assertEquals(String.class.getCanonicalName(), uas.getClassAnnotators().get(0).getAnnotators().get(0));
      assertEquals(Integer.class.getCanonicalName(), uas.getClassAnnotators().get(0).getAnnotators().get(1));
      assertEquals(Reaction.class.getCanonicalName(), uas.getClassValidAnnotators().get(0).getClassName());
      assertEquals(MiriamType.HGNC, uas.getClassValidAnnotators().get(0).getValidMiriamTypes().get(0));
      assertEquals(MiriamType.HGNC_SYMBOL, uas.getClassValidAnnotators().get(0).getValidMiriamTypes().get(1));

      userDao.delete(user2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

   @Test
  public void testUserWithAnnotatorSchemaGuiPreferences() throws Exception {
    try {
      User user = new User();
      UserAnnotationSchema uas = new UserAnnotationSchema();
      user.setAnnotationSchema(uas);
      UserGuiPreference option = new UserGuiPreference();
      option.setKey("key");
      option.setValue("val");
      uas.addGuiPreference(option);
      userDao.add(user);
      userDao.evict(user);
      User user2 = userDao.getById(user.getId());
      assertNotNull(user2);
      UserAnnotationSchema uas2 = user2.getAnnotationSchema();
      assertNotNull(uas2);
      assertEquals(1, uas2.getGuiPreferences().size());

      userDao.delete(user2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
