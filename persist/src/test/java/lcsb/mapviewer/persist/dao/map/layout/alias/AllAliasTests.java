package lcsb.mapviewer.persist.dao.map.layout.alias;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AliasDaoTest.class, //
		AliasDaoTest2.class, //
		AntisenseRnaTest.class, //
		RnaTest.class, //
})
public class AllAliasTests {

}
