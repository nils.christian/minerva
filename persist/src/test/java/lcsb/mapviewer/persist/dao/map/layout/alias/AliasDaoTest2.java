package lcsb.mapviewer.persist.dao.map.layout.alias;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class AliasDaoTest2 extends PersistTestFunctions {
  static Logger logger = Logger.getLogger(AliasDaoTest.class);

  private Integer testChargeVal = 1;
  private String testIdAlias = "a";
  private Double testInitialAmount = 2.0;
  private Double testInitialConcentration = 3.0;
  private String testName = "d";
  private String testNotes = "e";
  private Boolean testOnlySubstanceunits = true;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAdd() throws Exception {
    try {

      GenericProtein sp = new GenericProtein(testIdAlias);
      sp.setCharge(testChargeVal);
      sp.setInitialAmount(testInitialAmount);
      sp.setInitialConcentration(testInitialConcentration);
      sp.setName(testName);
      sp.setNotes(testNotes);
      sp.setOnlySubstanceUnits(testOnlySubstanceunits);

      Compartment parent = new Compartment("comp id");
      sp.setCompartment(parent);

      MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.UNKNOWN, "c");
      sp.addMiriamData(md);

      elementDao.add(sp);

      Species sp2 = (Species) elementDao.getById(sp.getId());
      assertNotNull(sp2);
      assertEquals(sp.getCharge(), sp2.getCharge());
      assertEquals(sp.getElementId(), sp2.getElementId());
      assertEquals(sp.getInitialAmount(), sp2.getInitialAmount());
      assertEquals(sp.getInitialConcentration(), sp2.getInitialConcentration());
      assertEquals(sp.getName(), sp2.getName());
      assertEquals(sp.getNotes(), sp2.getNotes());
      assertEquals(sp.hasOnlySubstanceUnits(), sp2.hasOnlySubstanceUnits());

      Compartment parent2 = sp2.getCompartment();
      assertNotNull(parent2);
      assertEquals("comp id", parent2.getElementId());

      assertNotNull(sp2.getMiriamData());

      MiriamData md2 = sp2.getMiriamData().iterator().next();
      assertNotNull(md2);
      assertEquals(md.getDataType(), md2.getDataType());
      assertEquals(md.getRelationType(), md2.getRelationType());
      assertEquals(md.getResource(), md2.getResource());

      elementDao.delete(sp);
      sp2 = (Species) elementDao.getById(sp.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testProtein() throws Exception {
    try {

      Protein protein = new GenericProtein(testIdAlias);
      Residue mr = new Residue();
      mr.setPosition(new Point2D.Double(10, 20));
      mr.setName("name");
      mr.setState(ModificationState.GLYCOSYLATED);
      protein.addModificationResidue(mr);

      elementDao.add(protein);

      Protein sp2 = (Protein) elementDao.getById(protein.getId());
      assertNotNull(sp2);
      assertEquals(protein.getElementId(), sp2.getElementId());

      assertNotNull(sp2.getModificationResidues());
      assertEquals(1, sp2.getModificationResidues().size());

      Residue copy = (Residue) sp2.getModificationResidues().get(0);
      assertEquals(copy.getPosition(), mr.getPosition());
      assertEquals(copy.getIdModificationResidue(), mr.getIdModificationResidue());
      assertEquals(copy.getName(), mr.getName());
      assertEquals(copy.getState(), mr.getState());

      elementDao.delete(sp2);
      sp2 = (Protein) elementDao.getById(protein.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRna() throws Exception {
    try {

      Rna sp = new Rna(testIdAlias);
      ModificationSite mr = new ModificationSite();
      mr.setName("name");
      mr.setState(ModificationState.DONT_CARE);
      sp.addRegion(mr);

      elementDao.add(sp);
      elementDao.evict(sp);

      Rna sp2 = (Rna) elementDao.getById(sp.getId());
      assertNotNull(sp2);
      assertEquals(sp.getElementId(), sp2.getElementId());

      assertNotNull(sp2.getRegions());
      assertEquals(1, sp2.getRegions().size());

      ModificationSite copy = (ModificationSite) sp2.getRegions().get(0);
      assertEquals(copy.getId(), mr.getId());
      assertEquals(copy.getName(), mr.getName());
      assertEquals(copy.getState(), mr.getState());

      elementDao.delete(sp2);
      sp2 = (Rna) elementDao.getById(sp.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAntisenseRna() throws Exception {
    try {

      AntisenseRna sp = new AntisenseRna(testIdAlias);
      ModificationSite mr = new ModificationSite();
      mr.setName("name");
      sp.addRegion(mr);

      elementDao.add(sp);
      elementDao.evict(sp);

      AntisenseRna sp2 = (AntisenseRna) elementDao.getById(sp.getId());
      assertNotNull(sp2);
      assertEquals(sp.getElementId(), sp2.getElementId());

      assertNotNull(sp2.getRegions());
      assertEquals(1, sp2.getRegions().size());

      assertEquals(sp2.getRegions().get(0).getIdModificationResidue(), mr.getIdModificationResidue());
      assertEquals(sp2.getRegions().get(0).getName(), mr.getName());

      elementDao.delete(sp2);
      sp2 = (AntisenseRna) elementDao.getById(sp.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSynonymsInAlias() throws Exception {
    try {
      Protein protein = new GenericProtein(testIdAlias);
      protein.addSynonym("Synonym");
      protein.addSynonym("Synonym A");
      protein.addSynonym("A");

      protein.addFormerSymbol("Sym");
      protein.addFormerSymbol("Sym A");
      protein.addFormerSymbol("DD");

      elementDao.add(protein);
      elementDao.flush();

      elementDao.evict(protein);
      Protein sp2 = (Protein) elementDao.getById(protein.getId());

      assertNotNull(sp2.getSynonyms());
      assertEquals(protein.getSynonyms().size(), sp2.getSynonyms().size());

      for (int i = 0; i < protein.getSynonyms().size(); i++) {
        assertEquals(protein.getSynonyms().get(i), sp2.getSynonyms().get(i));
      }

      assertNotNull(sp2.getFormerSymbols());
      assertEquals(protein.getFormerSymbols().size(), sp2.getFormerSymbols().size());

      for (int i = 0; i < protein.getFormerSymbols().size(); i++) {
        assertEquals(protein.getFormerSymbols().get(i), sp2.getFormerSymbols().get(i));
      }

      elementDao.delete(sp2);
      sp2 = (Protein) elementDao.getById(protein.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testChemicals() throws Exception {
    try {
      Chemical ion = new Ion(testIdAlias);
      ion.setInChI("come inchi");
      ion.setInChIKey("keyyy");
      ion.setSmiles("smile");

      elementDao.add(ion);
      elementDao.flush();

      elementDao.evict(ion);
      Ion sp2 = (Ion) elementDao.getById(ion.getId());

      assertNotNull(sp2.getSynonyms());
      assertEquals(ion.getSynonyms().size(), sp2.getSynonyms().size());

      assertEquals(ion.getSmiles(), sp2.getSmiles());
      assertEquals(ion.getInChIKey(), sp2.getInChIKey());
      assertEquals(ion.getInChI(), sp2.getInChI());

      elementDao.delete(sp2);
      sp2 = (Ion) elementDao.getById(ion.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testPhenotype() throws Exception {
    try {
      Phenotype phenotype = new Phenotype(testIdAlias);

      elementDao.add(phenotype);
      elementDao.flush();

      elementDao.evict(phenotype);
      Phenotype sp2 = (Phenotype) elementDao.getById(phenotype.getId());

      assertNotNull(sp2.getSynonyms());
      assertEquals(phenotype.getSynonyms().size(), phenotype.getSynonyms().size());

      elementDao.delete(sp2);
      sp2 = (Phenotype) elementDao.getById(phenotype.getId());
      assertNull(sp2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
