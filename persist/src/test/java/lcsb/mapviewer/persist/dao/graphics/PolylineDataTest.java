package lcsb.mapviewer.persist.dao.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.awt.geom.Point2D;

import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.persist.PersistTestFunctions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PolylineDataTest extends PersistTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			PolylineData pd = new PolylineData();
			pd.getBeginAtd().setArrowType(ArrowType.BLANK_CROSSBAR);
			pd.getBeginAtd().setArrowLineType(LineType.DASHED);
			pd.getBeginAtd().setLen(102);
			pd.setColor(Color.CYAN);
			pd.setType(LineType.SOLID_BOLD);
			pd.setWidth(2);
			pd.getPoints().add(new Point2D.Double(1, 1));
			pd.getPoints().add(new Point2D.Double(2, 3));
			pd.getPoints().add(new Point2D.Double(10, 11));
			polylineDao.add(pd);
			assertNotNull(pd.getId());
			PolylineData pd2 = polylineDao.getById(pd.getId());
			assertNotNull(pd2);

			assertEquals(ArrowType.BLANK_CROSSBAR, pd2.getBeginAtd().getArrowType());
			assertEquals(LineType.DASHED, pd2.getBeginAtd().getArrowLineType());
			assertEquals(102, pd2.getBeginAtd().getLen(), EPSILON);
			assertEquals(Color.CYAN, pd2.getColor());
			assertEquals(LineType.SOLID_BOLD, pd2.getType());
			assertEquals(2, pd2.getWidth(), EPSILON);
			assertEquals(0, pd2.getPoints().get(0).distance(new Point2D.Double(1, 1)), EPSILON);
			assertEquals(0, pd2.getPoints().get(1).distance(new Point2D.Double(2, 3)), EPSILON);
			assertEquals(0, pd2.getPoints().get(2).distance(new Point2D.Double(10, 11)), EPSILON);

			polylineDao.delete(pd);
			pd2 = polylineDao.getById(pd.getId());
			assertNull(pd2);

		} catch (Exception e) {
			e.printStackTrace();

			fail("Unknowne exception occured");
		}
	}

}
