package lcsb.mapviewer.persist.dao.map.layout;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import lcsb.mapviewer.persist.dao.map.layout.alias.AllAliasTests;

@RunWith(Suite.class)
@SuiteClasses({ AllAliasTests.class, //
		ReferenceGenomeDaoTest.class, //
		ReferenceGenomeGeneMappingDaoTest.class,//
})
public class AllLayoutTests {

}
