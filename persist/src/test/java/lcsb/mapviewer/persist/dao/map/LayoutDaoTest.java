package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Color;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class LayoutDaoTest extends PersistTestFunctions {

  @Autowired
  private LayoutDao layoutDao;

  final static Logger logger = Logger.getLogger(LayoutDaoTest.class);

  int identifierCounter = 0;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  /**
   * After adding layouts to model.
   * 
   * @throws Exception
   */
  @Test
  public void testLayoutsWithData() throws Exception {
    try {
      Model model = createModel();

      Project project = new Project();
      project.addModel(model);
      projectDao.add(project);

      modelDao.evict(model);
      projectDao.evict(project);

      Layout layout = new Layout();
      layout.setDirectory("tmp");
      layout.setTitle("temporary name");

      byte[] data = "test".getBytes();
      UploadedFileEntry fileEntry = new UploadedFileEntry();
      fileEntry.setFileContent(data);

      layout.setInputData(fileEntry);
      model.addLayout(layout);
      layoutDao.add(layout);

      layoutDao.evict(layout);

      layout = layoutDao.getById(layout.getId());

      assertEquals("test", new String(layout.getInputData().getFileContent(), StandardCharsets.UTF_8));

      project = projectDao.getById(project.getId());
      projectDao.delete(project);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  /**
   * After adding layouts to model.
   * 
   * @throws Exception
   */
  @Test
  public void testLayoutsWithoutData() throws Exception {
    try {
      Model model = createModel();
      String tempName = "temporary name";

      createUser();

      Project project = new Project();
      project.addModel(model);
      projectDao.add(project);

      modelDao.evict(model);
      projectDao.evict(project);

      assertEquals(0, layoutDao.getCountByUser(user));
      assertEquals(0, layoutDao.getLayoutsByModel(model).size());
      assertNull(layoutDao.getLayoutByName(model, tempName));

      Layout layout = new Layout();
      layout.setDirectory("tmp");
      layout.setTitle(tempName);
      layout.setCreator(user);
      model.addLayout(layout);
      layoutDao.add(layout);

      layoutDao.evict(layout);

      layout = layoutDao.getById(layout.getId());

      assertEquals(1, layoutDao.getLayoutsByModel(model).size());
      assertNotNull(layoutDao.getLayoutByName(model, tempName));
      assertEquals(1, layoutDao.getCountByUser(user));

      assertNull(layout.getInputData());

      project = projectDao.getById(project.getId());
      projectDao.delete(project);

      userDao.delete(user);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    Species alias = createSpeciesAlias(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createSpeciesAlias(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createSpeciesAlias(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createSpeciesAlias(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createSpeciesAlias(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createSpeciesAlias(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplexAlias(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment cAlias = createCompartmentAlias(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(cAlias);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = new Layer();
    model.addLayer(layer);

    LayerRect lr = new LayerRect();
    lr.setColor(Color.YELLOW);
    layer.addLayerRect(lr);

    Reaction reaction = new TransportReaction();
    reaction.addProduct(new Product(alias));
    reaction.addReactant(new Reactant(alias2));
    reaction.setIdReaction("re" + identifierCounter++);
    model.addReaction(reaction);

    Protein protein = createSpeciesAlias(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(protein);

    Residue mr = new Residue();
    mr.setName("mr");
    protein.addModificationResidue(mr);

    protein.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "c"));
    return model;
  }

  private Compartment createCompartmentAlias(double x, double y, double width, double height, String aliasId) {
    Compartment alias = new Compartment(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private Protein createSpeciesAlias(double x, double y, double width, double height, String aliasId) {
    GenericProtein alias = new GenericProtein("s" + identifierCounter++);
    alias.setElementId(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private Complex createComplexAlias(double x, double y, double width, double height, String aliasId) {
    Complex alias = new Complex(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

}
