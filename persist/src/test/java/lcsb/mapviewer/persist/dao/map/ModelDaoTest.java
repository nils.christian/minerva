package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ModelDaoTest extends PersistTestFunctions {
  ModelComparator modelComparator = new ModelComparator();

  Logger logger = Logger.getLogger(ModelDaoTest.class);
  private Project project;
  String projectId = "Some_id";
  int identifierCounter = 0;

  @Before
  public void setUp() throws Exception {
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setProjectId(projectId);
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void testLoadFromDb() throws Exception {
    try {
      Model model = createModel();
      project.addModel(model);
      modelDao.add(model);
      projectDao.update(project);
      projectDao.evict(project);

      modelDao.evict(model);
      ModelData model2 = modelDao.getById(model.getId());
      assertNotNull(model2);
      assertFalse(model2 == model);

      assertEquals(model.getElements().size(), model2.getElements().size());
      assertEquals(model.getLayers().size(), model2.getLayers().size());
      assertEquals(model.getReactions().size(), model2.getReactions().size());

      ModelComparator comparator = new ModelComparator();
      assertEquals(0, comparator.compare(model, new ModelFullIndexed(model2)));

      modelDao.delete(model2);
      model2 = modelDao.getById(model.getId());
      assertNull(model2);
      project = projectDao.getById(project.getId());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testIndexesReload() throws Exception {
    try {
      Model model = createModel();
      project.addModel(model);
      modelDao.add(model);
      modelDao.evict(model);
      ModelData model2 = modelDao.getById(model.getId());
      Model fullModel = new ModelFullIndexed(model2);
      assertNotNull(model2);

      assertEquals(model.getElements().size(), model2.getElements().size());
      assertEquals(model.getLayers().size(), model2.getLayers().size());
      assertEquals(model.getReactions().size(), model2.getReactions().size());

      // check if we really performed a test
      boolean test = false;

      for (Element alias : model.getElements()) {
        assertNotNull(fullModel.getElementByElementId(alias.getElementId()));
        test = true;
      }
      assertTrue(test);

      test = false;
      for (Element alias : model.getElements()) {
        if (alias instanceof Compartment) {
          assertNotNull(fullModel.getElementByElementId(alias.getElementId()));
          test = true;
        }
      }
      assertTrue(test);

      model2.setHeight(32);
      modelDao.update(model2.getModel());

      modelDao.delete(model2);
      model2 = modelDao.getById(model.getId());
      assertNull(model2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReactionInModelAfterReload() throws Exception {
    try {
      Model model = createModel();
      Reaction reaction = model.getReactions().iterator().next();
      project.addModel(model);
      modelDao.add(model);
      projectDao.update(project);
      projectDao.evict(project);
      modelDao.evict(model);
      ModelData model2 = modelDao.getById(model.getId());

      Reaction reaction2 = null;
      for (Reaction r : model2.getReactions()) {
        if (r.getIdReaction().equals(reaction.getIdReaction())) {
          reaction2 = r;
        }
      }
      assertNotNull(reaction2);
      assertFalse(reaction.equals(reaction2));

      assertEquals(reaction.getNodes().size(), reaction2.getNodes().size());

      modelDao.delete(model2);
      model2 = modelDao.getById(model.getId());
      assertNull(model2);
      project = projectDao.getById(project.getId());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testReactionInWithKinetics() throws Exception {
    try {
      Model model = createModel();
      Reaction reaction = model.getReactions().iterator().next();
      SbmlKinetics kinetics = new SbmlKinetics();
      kinetics.addElement(reaction.getReactants().get(0).getElement());
      kinetics.addFunction(createFunction());
      model.addFunctions(kinetics.getFunctions());
      kinetics.addParameter(createParameter());
      model.addUnit(kinetics.getParameters().iterator().next().getUnits());
      reaction.setKinetics(kinetics);
      project.addModel(model);
      modelDao.add(model);
      projectDao.update(project);
      projectDao.evict(project);
      modelDao.evict(model);
      ModelData model2 = modelDao.getById(model.getId());

      assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

      project = projectDao.getById(project.getId());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testModelWithParameters() throws Exception {
    try {
      Model model = createModel();
      model.addParameter(createParameter());
      model.addUnit(model.getParameters().iterator().next().getUnits());
      project.addModel(model);
      modelDao.add(model);
      projectDao.update(project);
      projectDao.evict(project);
      modelDao.evict(model);
      ModelData model2 = modelDao.getById(model.getId());

      assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

      project = projectDao.getById(project.getId());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private SbmlParameter createParameter() {
    SbmlParameter parameter = new SbmlParameter("param_id");
    parameter.setName("X");
    parameter.setValue(4.7);
    parameter.setUnits(createUnits());
    return parameter;
  }

  private SbmlUnit createUnits() {
    SbmlUnit unit = new SbmlUnit("unit_id");
    unit.setName("u name");
    unit.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.AMPERE, 1, 2, 3));
    return unit;
  }

  private SbmlFunction createFunction() {
    SbmlFunction result = new SbmlFunction("fun_id");
    result.setDefinition("def(k1)");
    result.addArgument("k1");
    result.setName("fun name");
    return result;
  }

  @Test
  public void testGetLastModelForProjectName() throws Exception {
    try {
      ModelData model3 = modelDao.getLastModelForProjectIdentifier(projectId, false);
      assertNull(model3);

      Model model = createModel();
      project.addModel(model);
      modelDao.add(model);

      ModelData newModel = modelDao.getLastModelForProjectIdentifier(projectId, false);
      assertNotNull(newModel);
      assertEquals(model.getId(), newModel.getId());

      Model model2 = createModel();
      project.addModel(model2);
      modelDao.add(model2);

      newModel = modelDao.getLastModelForProjectIdentifier(projectId, false);
      assertNotNull(newModel);
      assertEquals(model2.getId(), newModel.getId());

      modelDao.delete(model2);
      modelDao.delete(model);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  /**
   * After adding model to db, modification residues disappear from the model...
   * 
   * @throws Exception
   */
  @Test
  public void testModificationsInProteins() throws Exception {
    try {
      Model model = createModel();
      Project project = new Project();
      project.addModel(model);
      projectDao.add(project);

      modelDao.evict(model);
      projectDao.evict(project);
      Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

      Protein originalSpecies = (Protein) model.getElementByElementId("pr1");
      Protein fromDbSpecies = (Protein) model2.getElementByElementId("pr1");

      assertFalse(originalSpecies.equals(fromDbSpecies));
      assertEquals(originalSpecies.getModificationResidues().size(), fromDbSpecies.getModificationResidues().size());

      project = projectDao.getById(project.getId());
      projectDao.delete(project);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  /**
   * After adding model to db, miriam annotations disappear...
   * 
   * @throws Exception
   */
  @Test
  public void testMiriamInSpecies() throws Exception {
    try {
      Model model = createModel();
      Project project = new Project();
      project.addModel(model);
      projectDao.add(project);

      modelDao.evict(model);
      projectDao.evict(project);
      Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

      Protein originalSpecies = (Protein) model.getElementByElementId("pr1");
      Protein fromDbSpecies = (Protein) model2.getElementByElementId("pr1");

      assertFalse(originalSpecies.equals(fromDbSpecies));
      assertEquals(originalSpecies.getMiriamData().size(), fromDbSpecies.getMiriamData().size());

      project = projectDao.getById(project.getId());
      projectDao.delete(project);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  /**
   * After adding layouts to model.
   * 
   * @throws Exception
   */
  @Test
  public void testLayoutsInModel() throws Exception {
    try {
      Model model = createModel();

      Layout layout = new Layout();
      layout.setDirectory("tmp");
      layout.setTitle("temporary name");
      model.addLayout(layout);
      Project project = new Project();
      project.addModel(model);
      projectDao.add(project);

      modelDao.evict(model);
      projectDao.evict(project);
      ModelData model2 = modelDao.getById(model.getId());

      assertEquals(1, model2.getLayouts().size());
      assertEquals("tmp", model2.getLayouts().get(0).getDirectory());
      assertEquals("temporary name", model2.getLayouts().get(0).getTitle());

      project = projectDao.getById(project.getId());
      projectDao.delete(project);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = createSpecies(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createSpecies(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createSpecies(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createSpecies(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createSpecies(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createSpecies(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment cAlias = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(cAlias);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = new Layer();
    model.addLayer(layer);

    LayerRect lr = new LayerRect();
    lr.setColor(Color.YELLOW);
    layer.addLayerRect(lr);

    Reaction reaction = new TransportReaction();
    reaction.addProduct(new Product(alias));
    reaction.addReactant(new Reactant(alias2));
    reaction.setIdReaction("re" + identifierCounter++);
    model.addReaction(reaction);

    alias = createSpecies(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(alias);

    Residue mr = new Residue();
    mr.setName("mr");
    mr.setPosition(new Point2D.Double(10, 20));
    alias.addModificationResidue(mr);

    alias.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "c"));
    return model;
  }

  private Compartment createCompartment(double x, double y, double width, double height, String aliasId) {
    Compartment alias = new Compartment(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private GenericProtein createSpecies(double x, double y, double width, double height, String aliasId) {
    GenericProtein alias = new GenericProtein(aliasId);
    alias.setName("SNCA");
    alias.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    alias.addMiriamData(new MiriamData(MiriamType.HGNC, "11138"));
    alias.setElementId(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private Complex createComplex(double x, double y, double width, double height, String aliasId) {
    Complex alias = new Complex(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

}
