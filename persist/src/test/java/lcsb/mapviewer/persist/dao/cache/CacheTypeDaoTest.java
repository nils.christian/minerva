package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.PersistTestFunctions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CacheTypeDaoTest extends PersistTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReactomeCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.reactome.utils.ReactomeConnector");
			assertNotNull(cacheType);
			assertEquals(0,cacheType.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEmpty() throws Exception {
		try {
			assertNull(cacheTypeDao.getByClassName("blablabla"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testBiocompendiumCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.annotators.BiocompendiumAnnotator");
			assertNotNull(cacheType);
			assertEquals(1,cacheType.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testChemblCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.ChEMBLParser");
			assertNotNull(cacheType);
			assertEquals(2,cacheType.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testArticleCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.PubmedParser");
			assertNotNull(cacheType);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDrugBankCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.DrugbankHTMLParser");
			assertNotNull(cacheType);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testMockCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.cache.MockCacheInterface");
			assertNotNull(cacheType);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testChebiCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator");
			assertNotNull(cacheType);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGoCacheData() throws Exception {
		try {
			CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.annotators.GoAnnotator");
			assertNotNull(cacheType);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
