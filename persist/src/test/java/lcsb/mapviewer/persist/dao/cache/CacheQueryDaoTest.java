package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class CacheQueryDaoTest extends PersistTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetByQuery() {
		try {
			CacheType type = cacheTypeDao.getByClassName("lcsb.mapviewer.reactome.utils.ReactomeConnector");
			CacheQuery query = new CacheQuery();
			query.setType(type);
			query.setQuery("test");
			cacheQueryDao.add(query);
			assertEquals(query, cacheQueryDao.getByQuery("test", type));
			assertNull(cacheQueryDao.getByQuery("another_test_bla", type));

			cacheQueryDao.delete(query);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
