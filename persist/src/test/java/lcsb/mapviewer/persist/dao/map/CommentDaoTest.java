package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class CommentDaoTest extends PersistTestFunctions {
  int identifierCounter = 1;

  private Project project;
  String projectId = "Some_id";

  @Before
  public void setUp() throws Exception {
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setProjectId(projectId);
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void testGetComments() {
    try {
      Model model = createModel();
      project.addModel(model);
      modelDao.add(model);
      projectDao.update(project);

      Comment comment = new Comment();
      comment.setDeleted(true);
      comment.setModel(model);
      commentDao.add(comment);

      Comment comment2 = new Comment();
      comment2.setPinned(true);
      comment2.setModel(model);
      commentDao.add(comment2);

      assertEquals(0, commentDao.getCommentByModel(model, true, true).size());
      assertEquals(1, commentDao.getCommentByModel(model, false, true).size());
      assertEquals(1, commentDao.getCommentByModel(model, null, true).size());
      assertEquals(1, commentDao.getCommentByModel(model, true, false).size());
      assertEquals(0, commentDao.getCommentByModel(model, false, false).size());
      assertEquals(1, commentDao.getCommentByModel(model, null, false).size());
      assertEquals(1, commentDao.getCommentByModel(model, true, null).size());
      assertEquals(1, commentDao.getCommentByModel(model, false, null).size());
      assertEquals(2, commentDao.getCommentByModel(model, null, null).size());

      assertEquals(0, commentDao.getCommentByModel(model.getModelData(), true, true).size());
      assertEquals(1, commentDao.getCommentByModel(model.getModelData(), false, true).size());
      assertEquals(1, commentDao.getCommentByModel(model.getModelData(), null, true).size());
      assertEquals(1, commentDao.getCommentByModel(model.getModelData(), true, false).size());
      assertEquals(0, commentDao.getCommentByModel(model.getModelData(), false, false).size());
      assertEquals(1, commentDao.getCommentByModel(model.getModelData(), null, false).size());
      assertEquals(1, commentDao.getCommentByModel(model.getModelData(), true, null).size());
      assertEquals(1, commentDao.getCommentByModel(model.getModelData(), false, null).size());
      assertEquals(2, commentDao.getCommentByModel(model.getModelData(), null, null).size());

      commentDao.delete(comment);
      commentDao.delete(comment2);
      modelDao.delete(model);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = createSpeciesAlias(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createSpeciesAlias(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createSpeciesAlias(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createSpeciesAlias(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createSpeciesAlias(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createSpeciesAlias(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplexAlias(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment cAlias = createCompartmentAlias(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(cAlias);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = new Layer();
    model.addLayer(layer);

    LayerRect lr = new LayerRect();
    lr.setColor(Color.YELLOW);
    layer.addLayerRect(lr);

    Reaction reaction = new TransportReaction();
    reaction.addProduct(new Product(alias));
    reaction.addReactant(new Reactant(alias2));
    reaction.setIdReaction("re" + identifierCounter++);
    model.addReaction(reaction);

    alias = createSpeciesAlias(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(alias);

    ModificationResidue mr = new Residue();
    mr.setName("mr");
    alias.addModificationResidue(mr);

    alias.addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.CHEBI, "c"));
    return model;
  }

  private Compartment createCompartmentAlias(double x, double y, double width, double height, String aliasId) {
    Compartment alias = new Compartment(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private GenericProtein createSpeciesAlias(double x, double y, double width, double height, String aliasId) {
    GenericProtein alias = new GenericProtein(aliasId);
    alias.setName("SNCA");
    alias.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    alias.addMiriamData(new MiriamData(MiriamType.HGNC, "11138"));
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private Complex createComplexAlias(double x, double y, double width, double height, String aliasId) {
    Complex alias = new Complex(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

}
