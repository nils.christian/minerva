package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ProjectDaoTest extends PersistTestFunctions {
	Logger logger						 = Logger.getLogger(ProjectDaoTest.class);
	int		 identifierCounter = 0;
	String projectId				 = "Some_id";

	@Before
	public void setUp() throws Exception {
		Project project = projectDao.getProjectByProjectId(projectId);
		if (project != null) {
			projectDao.delete(project);
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetProjectByName() throws Exception {
		try {
			Project project = new Project();
			project.setProjectId(projectId);
			projectDao.add(project);
			projectDao.evict(project);

			Project project2 = projectDao.getProjectByProjectId(projectId);
			assertNotNull(project2);
			assertFalse(project2.equals(project));
			assertEquals(project.getId(), project2.getId());

			projectDao.delete(project2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetProjectForModelId() throws Exception {
		try {
			String projectId = "Some name";
			Project project = new Project();
			project.setProjectId(projectId);
			projectDao.add(project);
			Model model = new ModelFullIndexed(null);
			project.addModel(model);
			modelDao.add(model);
			modelDao.evict(model);
			projectDao.evict(project);

			Project project2 = projectDao.getProjectForModelId(model.getId());
			assertNotNull(project2);
			assertFalse(project2.equals(project));
			assertEquals(project.getId(), project2.getId());

			modelDao.delete(modelDao.getById(model.getId()));
			projectDao.delete(project2);

			assertNull(projectDao.getProjectForModelId(model.getId()));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testProjectExists() throws Exception {
		try {
			String projectId = "Some_id";
			Project project = new Project();
			project.setProjectId(projectId);

			assertFalse(projectDao.isProjectExistsByName(projectId));

			projectDao.add(project);
			projectDao.evict(project);

			assertTrue(projectDao.isProjectExistsByName(projectId));

			Project project2 = projectDao.getProjectByProjectId(projectId);

			projectDao.delete(project2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCheckEqualityAfterReload() throws Exception {
		try {

			String projectId = "Some_id";
			Project project = new Project();
			project.setProjectId(projectId);
			projectDao.add(project);
			projectDao.flush();

			Model model = createModel();
			project.addModel(model);

			projectDao.update(project);
			projectDao.evict(project);

			ModelComparator comparator = new ModelComparator();

			Model model2 = new ModelFullIndexed(modelDao.getLastModelForProjectIdentifier(projectId, false));

			assertEquals(0, comparator.compare(model, model2));

			projectDao.delete(projectDao.getById(project.getId()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCheckEqualityAfterReload2() throws Exception {
		try {
			String project_id = "Some_id";
			Project project = new Project();
			project.setProjectId(project_id);
			Model model = createModel();
			project.addModel(model);

			projectDao.add(project);
			projectDao.evict(project);

			Project project2 = projectDao.getProjectByProjectId(project_id);

			ModelComparator comparator = new ModelComparator();

			Model fullModel1 = new ModelFullIndexed(project.getModels().iterator().next());
			Model fullModel2 = new ModelFullIndexed(project2.getModels().iterator().next());
			assertEquals(0, comparator.compare(fullModel1, fullModel2));

			projectDao.delete(project2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private Model createModel() {
		Model model = new ModelFullIndexed(null);

		model.addElement(createSpeciesAlias(264.8333333333335, 517.75, 86.0, 46.0, "sa2"));
		model.addElement(createSpeciesAlias(267.6666666666665, 438.75, 80.0, 40.0, "sa1117"));
		model.addElement(createSpeciesAlias(261.6666666666665, 600.75, 92.0, 52.0, "sa1119"));
		model.addElement(createSpeciesAlias(203.666666666667, 687.75, 98.0, 58.0, "sa1121"));

		Species alias = createSpeciesAlias(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
		Species alias2 = createSpeciesAlias(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
		Complex alias3 = createComplexAlias(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
		alias3.addSpecies(alias);
		alias3.addSpecies(alias2);
		alias.setComplex(alias3);
		alias2.setComplex(alias3);

		model.addElement(alias);
		model.addElement(alias2);
		model.addElement(alias3);

		model.addElement(createCompartmentAlias(380.0, 416.0, 1893.0, 1866.0, "ca1"));
		model.setWidth(2000);
		model.setHeight(2000);
		return model;
	}

	private Compartment createCompartmentAlias(double x, double y, double width, double height, String aliasId) {
		Compartment alias = new Compartment(aliasId);
		alias.setX(x);
		alias.setY(y);
		alias.setWidth(width);
		alias.setHeight(height);
		return alias;
	}

	private Species createSpeciesAlias(double x, double y, double width, double height, String aliasId) {
		Species alias = new SimpleMolecule(aliasId);
		alias.setX(x);
		alias.setY(y);
		alias.setWidth(width);
		alias.setHeight(height);
		return alias;
	}

	private Complex createComplexAlias(double x, double y, double width, double height, String aliasId) {
		Complex alias = new Complex(aliasId);
		alias.setX(x);
		alias.setY(y);
		alias.setWidth(width);
		alias.setHeight(height);
		return alias;
	}

	@Test
	public void testAddGetProjectWithOverviewImage() throws Exception {
		try {
			String projectId = "Some_id";
			Project project = new Project();
			project.setProjectId(projectId);
			Model model = new ModelFullIndexed(null);
			OverviewImage oi = new OverviewImage();
			oi.setFilename("test");
			OverviewModelLink oml = new OverviewModelLink();
			oml.setPolygon("10,10 20,20 20,100");
			oml.setxCoord(1);
			oml.setyCoord(2);
			oml.setZoomLevel(3);
			oi.addLink(oml);
			project.addOverviewImage(oi);
			project.addModel(model);

			projectDao.add(project);
			projectDao.evict(project);

			Project project2 = projectDao.getProjectByProjectId(projectId);
			assertNotNull(project2);

			OverviewImage oi2 = project2.getOverviewImages().get(0);
			OverviewModelLink oml2 = (OverviewModelLink) oi2.getLinks().get(0);

			assertEquals(oi.getFilename(), oi2.getFilename());
			assertEquals(oml.getPolygon(), oml2.getPolygon());
			assertEquals(oml.getxCoord(), oml2.getxCoord());
			assertEquals(oml.getyCoord(), oml2.getyCoord());
			assertEquals(oml.getZoomLevel(), oml2.getZoomLevel());
			assertNotNull(oml2.getPolygonCoordinates());

			projectDao.delete(project2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * After adding model to db with creation warnings...
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreationWarnings() throws Exception {
		try {
			Project project = new Project();
			project.addWarning("warning A");
			project.addWarning("warning B");
			assertEquals(2, project.getWarnings().size());

			projectDao.add(project);

			projectDao.evict(project);
			Project project2 = projectDao.getById(project.getId());

			assertEquals(2, project2.getWarnings().size());

			projectDao.delete(project2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAll() throws Exception {
		try {

			long startTime = System.currentTimeMillis();
			double max = 10;

			logger.debug("---");
			for (int i = 0; i < max; i++) {
				projectDao.getAll();
			}
			long estimatedTime = System.currentTimeMillis() - startTime;
			logger.debug(estimatedTime/max);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
