package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.persist.PersistTestFunctions;

@Rollback(false)
public class BaseDaoTest extends PersistTestFunctions{

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCommit() {
		try {

			dbUtils.createSessionForCurrentThread();
			Transaction tr = userDao.getSession().getTransaction();
			userDao.commit();
			assertTrue(tr.equals(tr));
			Transaction tr2 = userDao.getSession().getTransaction();
			
			assertFalse(tr.equals(tr2));

			dbUtils.closeSessionForCurrentThread();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
