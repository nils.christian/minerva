package lcsb.mapviewer.persist.dao.map.statistics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import lcsb.mapviewer.model.map.statistics.SearchHistory;
import lcsb.mapviewer.model.map.statistics.SearchType;
import lcsb.mapviewer.persist.PersistTestFunctions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SearchHistoryDaoTest extends PersistTestFunctions{

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			SearchHistory searchHistory = new SearchHistory();
			long count = searchHistoryDao.getCount();
			searchHistory.setIpAddress("0.0.0.0");
			searchHistory.setQuery("query ...");
			searchHistory.setType(SearchType.GENERAL);
			searchHistoryDao.add(searchHistory);
			long count1 = searchHistoryDao.getCount();
			assertEquals(count+1,count1);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception occurred");
		}
	}

}
