package lcsb.mapviewer.persist.dao.map.layout.alias;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.SearchIndex;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class AliasDaoTest extends PersistTestFunctions {
  Logger logger = Logger.getLogger(AliasDaoTest.class);

  private Project project;
  String projectId = "Some_id";

  int identifierCounter = 0;

  @Before
  public void setUp() throws Exception {
    project = projectDao.getProjectByProjectId(projectId);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setProjectId(projectId);
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void anotherSaveDaoTest() throws Exception {
    try {
      Model model = createModel();
      project.addModel(model);

      Species alias = (Species) model.getElementByElementId("sa2");

      alias.getSearchIndexes().add(new SearchIndex("blabla"));

      modelDao.add(model);

      Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

      Species alias2 = (Species) elementDao.getById(model2.getElementByElementId("sa2").getId());

      assertEquals(1, alias2.getSearchIndexes().size());

      modelDao.delete(model);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);
    model.addElement(createSpeciesAlias(264.8333333333335, 517.75, 86.0, 46.0, "sa2"));
    model.addElement(createSpeciesAlias(267.6666666666665, 438.75, 80.0, 40.0, "sa1117"));
    model.addElement(createSpeciesAlias(261.6666666666665, 600.75, 92.0, 52.0, "sa1119"));
    model.addElement(createSpeciesAlias(203.666666666667, 687.75, 98.0, 58.0, "sa1121"));

    Species alias = createSpeciesAlias(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createSpeciesAlias(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplexAlias(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    model.addElement(createCompartmentAlias(380.0, 416.0, 1893.0, 1866.0, "ca1"));
    model.setWidth(2000);
    model.setHeight(2000);
    return model;
  }

  private Compartment createCompartmentAlias(double x, double y, double width, double height, String aliasId) {
    Compartment alias = new Compartment(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private Species createSpeciesAlias(double x, double y, double width, double height, String aliasId) {
    SimpleMolecule alias = new SimpleMolecule(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  private Complex createComplexAlias(double x, double y, double width, double height, String aliasId) {
    Complex alias = new Complex(aliasId);
    alias.setX(x);
    alias.setY(y);
    alias.setWidth(width);
    alias.setHeight(height);
    return alias;
  }

  @Test
  public void saveAliasWithSubmodelTest() throws Exception {
    try {
      long count = modelDao.getCount();
      Model model = createModel();
      Model model1 = createModel();
      Element alias = model.getElementByElementId("sa2");
      ElementSubmodelConnection submodel = new ElementSubmodelConnection(model1, SubmodelType.UNKNOWN);
      alias.setSubmodel(submodel);
      project.addModel(model);

      projectDao.add(project);
      projectDao.flush();

      long count2 = modelDao.getCount();
      assertEquals(count + 2, count2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
