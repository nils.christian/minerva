package lcsb.mapviewer.persist.dao.cache;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BigFileEntryDaoTest.class, //
		CacheTypeDaoTest.class, //
		CacheQueryDaoTest.class, //
		UploadedFileEntryDaoTest.class,//
})
public class AllCacheDbTests {

}
