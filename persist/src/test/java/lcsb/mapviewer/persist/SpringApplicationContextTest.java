package lcsb.mapviewer.persist;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SpringApplicationContextTest extends PersistTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetter() {
		assertNotNull(SpringApplicationContext.getApplicationContext());
	}
	
	@Test
	public void testGetBean() {
		assertNotNull(SpringApplicationContext.getBean("DbUtils"));
	}

}
