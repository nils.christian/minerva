package lcsb.mapviewer.persist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;

public class ApplicationContextLoaderTest {
	Logger												 logger	= Logger.getLogger(ApplicationContextLoaderTest.class);

	ConfigurableApplicationContext originalContext;

	@Before
	public void setUp() throws Exception {
		originalContext = ApplicationContextLoader.getApplicationContext();
		ApplicationContextLoader.setApplicationContext(null);
	}

	@After
	public void tearDown() throws Exception {
		ApplicationContextLoader.setApplicationContext(originalContext);
	}

	@Test
	public void testGetters() {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("/test-applicationContext2.xml");
		ApplicationContextLoader.setApplicationContext(context);
		assertEquals(context, ApplicationContextLoader.getApplicationContext());
	}

	@Test
	public void testLoadApplicationContext() {
		ApplicationContextLoader.loadApplicationContext("/test-applicationContext2.xml");
		assertNotNull(ApplicationContextLoader.getApplicationContext());
	}

	@Test
	public void testInject() {
		try {
			class Tmp {
				@Autowired
				private PasswordEncoder encoder;

			}
			;
			Tmp obj = new Tmp();
			ApplicationContextLoader.loadApplicationContext("test-applicationContext2.xml");
			ApplicationContextLoader.injectDependencies(obj);
			assertNotNull(obj.encoder);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testPrivateConstructor() throws Exception {

		try {
			for (Constructor<?> constructor : ApplicationContextLoader.class.getDeclaredConstructors()) {
				logger.debug(constructor);
				constructor.setAccessible(true);
				ApplicationContextLoader obj = (ApplicationContextLoader) constructor.newInstance(new Object[] {});
				assertNotNull(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
