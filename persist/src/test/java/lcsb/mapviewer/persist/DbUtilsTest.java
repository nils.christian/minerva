package lcsb.mapviewer.persist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidStateException;

public class DbUtilsTest extends PersistTestFunctions {

	@Test
	public void testSchema() throws Exception {
		try {
			dbUtils.executeSqlQuery("update user_table set idDb = 1 where idDb =1");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testCreateSession() throws Exception {
		try {
			assertFalse(dbUtils.isCustomSessionForCurrentThread());
			dbUtils.createSessionForCurrentThread();
			assertTrue(dbUtils.isCustomSessionForCurrentThread());
			assertEquals(1, dbUtils.getSessionCounter());
			dbUtils.closeSessionForCurrentThread();
			assertFalse(dbUtils.isCustomSessionForCurrentThread());
			assertEquals(0, dbUtils.getSessionCounter());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void testCreateInvalidSession() throws Exception {
		try {
			dbUtils.createSessionForCurrentThread();
			dbUtils.createSessionForCurrentThread();
			fail("Exception expected");
		} catch (InvalidStateException e) {
			dbUtils.closeSessionForCurrentThread();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testAutoFlush() {
		try {
			boolean autoflush = dbUtils.isAutoFlush();
			dbUtils.setAutoFlush(false);
			assertFalse(dbUtils.isAutoFlush());
			dbUtils.setAutoFlush(autoflush);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	
}
