-- fix on comment visibility in new version
update feedback set tablename = 'lcsb.mapviewer.model.map.species.Species' where tablename='lcsb.mapviewer.model.map.layout.alias.SpeciesAlias';
update feedback set tablename = 'lcsb.mapviewer.model.map.species.Complex' where tablename='lcsb.mapviewer.model.map.layout.alias.ComplexAlias';