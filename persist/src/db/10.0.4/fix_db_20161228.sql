-- fix on drugbank parser
delete from cachequery where query like 'drug:%' and type in (select iddb from cache_type  where classname ='lcsb.mapviewer.annotation.services.DrugbankHTMLParser');
delete from cachequery where query like 'http:%' and type in (select iddb from cache_type  where classname ='lcsb.mapviewer.annotation.services.DrugbankHTMLParser');
