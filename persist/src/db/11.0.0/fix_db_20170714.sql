-- some sequences had too low values
DO ' 
DECLARE 
	rec 	RECORD;
	size 	INT;
BEGIN
	FOR rec IN SELECT * FROM information_schema.sequences LOOP
		EXECUTE format(''SELECT COUNT(1) FROM information_schema.tables WHERE table_name = ''''%s'''''', REPLACE(rec.sequence_name, ''_iddb_seq'', '''')) INTO size;
		IF  size = 1 THEN
			EXECUTE format(''SELECT setval(''''%s'''', (SELECT MAX(iddb)+1 FROM %I))'', rec.sequence_name, REPLACE(rec.sequence_name, ''_iddb_seq'', ''''));		
		END IF;
		EXECUTE format(''SELECT COUNT(1) FROM information_schema.tables WHERE table_name = ''''%s'''''', REPLACE(rec.sequence_name, ''_iddb_seq'', ''_table'')) INTO size;
		IF  size = 1 THEN
			EXECUTE format(''SELECT setval(''''%s'''', (SELECT MAX(iddb)+1 FROM %I))'', rec.sequence_name, REPLACE(rec.sequence_name, ''_iddb_seq'', ''_table''));
		END IF;
	END LOOP;
END ';