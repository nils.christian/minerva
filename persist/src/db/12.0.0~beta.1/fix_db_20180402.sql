CREATE SEQUENCE user_gui_preferences_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE user_gui_preferences
(
  iddb integer NOT NULL DEFAULT nextval('user_gui_preferences_iddb_seq'::regclass),
  key character varying(255) NOT NULL,
  value character varying(255) NOT NULL,
  annotation_schema_iddb integer NOT NULL,
  CONSTRAINT user_gui_preferences_fk FOREIGN KEY (annotation_schema_iddb)
      REFERENCES public.user_annotation_schema_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT user_gui_preferences_key_unique UNIQUE (key, annotation_schema_iddb)
);
