-- update annotator class information
update cache_type set classname = 'lcsb.mapviewer.annotation.services.annotators.BiocompendiumAnnotator' where classname = 'lcsb.mapviewer.annotation.services.AnnotationRestService';
update cache_type set classname = 'lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator' where classname = 'lcsb.mapviewer.annotation.services.ChebiBackend';
update cache_type set classname = 'lcsb.mapviewer.annotation.services.annotators.GoAnnotator' where classname = 'lcsb.mapviewer.annotation.services.GoBackend';

-- new annotator (uniprot)
insert into  cache_type (iddb, validity, classname) values (11, 28, 'lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator');
