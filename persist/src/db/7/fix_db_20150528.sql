-- searching by annotations
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (474, 'Session scope fixed (browsing many maps at the same time)', 'BUG_FIX', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));
--on client side maps are asynchronously added/removed, and info about map changes in project list is updated by polling
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (474, 'Asynchronous removing/adding maps', 'BUG_FIX', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));
