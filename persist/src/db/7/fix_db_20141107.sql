update species_table set species_type_db = 'GENERIC_PROTEIN' where species_type_db='Protein' and type='GENERIC';
update species_table set species_type_db = 'ION_CHANNEL_PROTEIN' where species_type_db='Protein' and type='ION_CHANNEL';
update species_table set species_type_db = 'TRUNCATED_PROTEIN' where species_type_db='Protein' and type='TRUNCATED';
update species_table set species_type_db = 'RECEPTOR_PROTEIN' where species_type_db='Protein' and type='RECEPTOR';
