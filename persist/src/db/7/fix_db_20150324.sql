-- table for external projects
CREATE TABLE external_project
(
  iddb serial NOT NULL,
  name varchar(255),
  CONSTRAINT external_project_pkey PRIMARY KEY (iddb)
);

-- table for external users
CREATE TABLE external_user
(
  iddb serial NOT NULL,
  name varchar(255) not null,
  surname varchar(255) not null,
  email varchar(255),
  CONSTRAINT external_user_pkey PRIMARY KEY (iddb)
);

-- framework version (past and current)
CREATE TABLE framework_version
(
  iddb serial NOT NULL,
  version integer not null unique,
  "time" timestamp without time zone not null,
  svnVersion integer not null,
  description character varying(255),
  CONSTRAINT framework_version_pkey PRIMARY KEY (iddb)
);

-- changelog information
CREATE TABLE changelog
(
  iddb serial NOT NULL,
  svnVersion integer not null,
  description character varying(255) not null,
  type character varying(255) not null,
  requestproject_iddb integer,
  requestuser_iddb integer,
  CONSTRAINT changelog_pkey PRIMARY KEY (iddb),
  CONSTRAINT changelog_requestuser_fk FOREIGN KEY (requestuser_iddb)
      REFERENCES external_user (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT changelog_requestproject_fk FOREIGN KEY (requestproject_iddb)
      REFERENCES external_project (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- add framework versions available until now
insert into framework_version(version,"time", svnversion) values (1, timestamp'2013-12-04',54);
insert into framework_version(version,"time", svnversion) values (2, timestamp'2014-02-02',66);
insert into framework_version(version,"time", svnversion) values (3, timestamp'2014-06-05',117);
insert into framework_version(version,"time", svnversion) values (4, timestamp'2014-09-11',186);
insert into framework_version(version,"time", svnversion) values (5, timestamp'2014-12-18',369);

-- add projects
insert into external_project (name) values ('Parkinson''s Disease map');
insert into external_project (name) values ('RECON');

-- add people
insert into external_user (name, surname, email) values ('Piotr', 'Gawron', 'piotr.gawron@uni.lu');
insert into external_user (name, surname, email) values ('Marek', 'Ostaszewski', 'marek.ostaszewski@uni.lu');
insert into external_user (name, surname, email) values ('Stephan', 'Gebel', 'stephan.gebel@uni.lu');
insert into external_user (name, surname, email) values ('Ronan', 'Fleming', 'ronan.mt.fleming@gmail.com');


-- changlog information
delete from changelog;
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (57, 'Problem with touch interface after update of browsers engine', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (59, 'Problem in Safari when searching for elements', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (66, 'Upload of the CellDesigner map', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (78, 'Drug targeting', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (93, 'Export to CellDesigner', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (109, 'Status page of all dependent services', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (117, 'Cache interface for data from external services', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (117, 'Configuration page', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (121, 'Custom layout upload', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (126, 'Additional structural information imported from annotation service', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (127, 'Fonts and lines in hierarchical view', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (131, 'Import data from GO', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (131, 'Export problem', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (134, 'Problem with users and session expire', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (134, 'Problem with special UTF-8 characters in CellDesigner file', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (134, 'Problem with touching bubbles on touch interface', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (135, 'Visualization of residues in rna, antisense rna, proteins', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (135, 'Export of the part of the map into CellDesigner file', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (155, 'Logo files managable via config webpage', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (160, 'Autoresfresh of cached data', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (216, 'Problem with touch interface on Windows 8.1', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (216, 'Google Maps API problem with floating bubbles', 'BUG_FIX', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (218, 'Parsing and annotation warnings for models in admin panel', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (222, 'Right click on Windows 8.1 touch interface', 'FUNCTIONALITY', null, null);
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (256, 'Information about build', 'FUNCTIONALITY', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (272, 'Error handling in admin panel', 'BUG_FIX', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (327, 'Popup window in admin panel that list warnings', 'FUNCTIONALITY', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (328, 'Additional checkboxes in upload model: annotate automatically, cache data, verify annotations', 'FUNCTIONALITY', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (329, 'Reaction color in CellDesigner file', 'BUG_FIX', (select iddb from external_project where name ='RECON'), (select iddb from external_user where email ='ronan.mt.fleming@gmail.com'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (330, 'Reaction line width', 'BUG_FIX', (select iddb from external_project where name ='RECON'), (select iddb from external_user where email ='ronan.mt.fleming@gmail.com'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (331, 'Custom reaction visualization in upload layout', 'FUNCTIONALITY', (select iddb from external_project where name ='RECON'), (select iddb from external_user where email ='ronan.mt.fleming@gmail.com'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (373, 'Errors from parsing data forwarded to client', 'FUNCTIONALITY', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (374, 'Notification email system added', 'FUNCTIONALITY', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (375, 'Warnings are downloadable', 'FUNCTIONALITY', null, (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (401, 'Drugbank problem after update of their interface', 'BUG_FIX', null, (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (402, 'Autorefresh functionality', 'BUG_FIX', null, (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (424, 'Comment in left panel (for touch interface)', 'FUNCTIONALITY', null, (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (432, 'Complex map (composed from many small maps)', 'FUNCTIONALITY', null, (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (434, 'Visualization of Recon specific annotations', 'FUNCTIONALITY', (select iddb from external_project where name ='RECON'), (select iddb from external_user where email ='ronan.mt.fleming@gmail.com'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (437, 'Visualization of posttranslationa modification', 'BUG_FIX', null, (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values (440, 'Reporting of drawing problems', 'BUG_FIX', null, (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
