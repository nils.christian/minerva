--entrez annotator (and cache store)
insert into cache_type (iddb,validity,classname) values (14,365,'lcsb.mapviewer.annotation.services.annotators.EntrezAnnotator');

--ensembl annotator (and cache store)
insert into cache_type (iddb,validity,classname) values (15,365,'lcsb.mapviewer.annotation.services.annotators.EnsemblAnnotator');

insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (535, 'Entrez annotator', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));

insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (535, 'Ensembl annotator', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));

