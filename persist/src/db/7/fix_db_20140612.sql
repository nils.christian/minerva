alter table species_table add column fullname varchar(255);
alter table species_table add column symbol varchar(255);

drop table if exists element_synonyms;
create table element_synonyms (
	iddb integer,
	idx integer,
	synonym varchar(255),

	CONSTRAINT element_synonyms_pkey PRIMARY KEY (iddb, idx),
	CONSTRAINT element_synonyms_fkey FOREIGN KEY (iddb)
      REFERENCES species_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

);

drop table if exists element_former_symbols;
create table element_former_symbols (
	iddb integer,
	idx integer,
	symbol varchar(255),

	CONSTRAINT element_former_symbols_pkey PRIMARY KEY (iddb, idx),
	CONSTRAINT element_former_symbols_fkey FOREIGN KEY (iddb)
      REFERENCES species_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

);

alter table species_table add column smiles text;
alter table species_table add column inchi text;
alter table species_table add column inchikey varchar(255);

CREATE SEQUENCE associated_phenotype_element_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
 CREATE TABLE associated_phenotype_element_table
(
  iddb integer NOT NULL DEFAULT nextval('associated_phenotype_element_table_iddb_seq'::regclass),
  source integer,
  data_iddb integer,
  phenotype_iddb integer,
  CONSTRAINT associated_phenotype_element_table_pkey PRIMARY KEY (iddb),
  CONSTRAINT associated_phenotype_element_table_fk FOREIGN KEY (data_iddb)
      REFERENCES miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT associated_phenotype_element_table_fk2 FOREIGN KEY (phenotype_iddb)
      REFERENCES species_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
