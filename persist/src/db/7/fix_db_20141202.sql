DROP INDEX query_index;
CREATE INDEX query_index
  ON cachequery
  USING hash
  (query COLLATE pg_catalog."default");
