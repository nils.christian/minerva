-- modification on model table
alter table model_table drop column shortdescription;

-- clearing data mining information (required for structure change in data mining)
delete from references_table;
delete from missingconnection;

-- modifing data_mining structure
ALTER TABLE references_table
  drop CONSTRAINT fkdfe14357f943f5d4;

ALTER TABLE references_table ADD COLUMN idx integer;
ALTER TABLE references_table ALTER COLUMN idx SET NOT NULL;

ALTER TABLE references_table DROP COLUMN reference;
ALTER TABLE references_table DROP COLUMN reference_id;

ALTER TABLE references_table ADD COLUMN missingconnection_iddb integer;
ALTER TABLE references_table ALTER COLUMN missingconnection_iddb SET NOT NULL;
ALTER TABLE references_table ADD COLUMN miriam_iddb integer;
ALTER TABLE references_table ALTER COLUMN miriam_iddb SET NOT NULL;

ALTER TABLE references_table
  ADD CONSTRAINT references_table_miriam_pkey PRIMARY KEY(missingconnection_iddb, idx);
ALTER TABLE references_table
  ADD CONSTRAINT references_table_foreign_key FOREIGN KEY (missingconnection_iddb)
      REFERENCES missingconnection (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE references_table
  ADD CONSTRAINT references_table_foreign_miriam_key FOREIGN KEY (miriam_iddb)
      REFERENCES miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE references_table
  ADD CONSTRAINT references_table_unique_miriam UNIQUE(miriam_iddb);

alter table references_table rename to missingconnection_miriam_data_table;
alter table missingconnection_miriam_data_table rename column miriam_iddb to  references_iddb;
