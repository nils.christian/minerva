alter table alias_table drop column maxvisibilitylevel;
alter table alias_table drop column minvisibilitylevel;

alter table alias_table add column transparencyLevel numeric;
update alias_table set transparencyLevel=0;