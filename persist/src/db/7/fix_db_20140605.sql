CREATE TABLE configuration_table (
    iddb integer NOT NULL,
    type integer,
    value character varying(255)
);

CREATE SEQUENCE configuration_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE configuration_table_iddb_seq OWNED BY configuration_table.iddb;

ALTER TABLE ONLY configuration_table ALTER COLUMN iddb SET DEFAULT nextval('configuration_table_iddb_seq'::regclass);

ALTER TABLE ONLY configuration_table
    ADD CONSTRAINT configuration_table_pkey PRIMARY KEY (iddb);
