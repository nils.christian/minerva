--Venkata changed response from the server
delete from cachequery where type = 1;

-- map path functionality removed
drop table mappathnode;
drop table mappath;

--taxonomy service
insert into cache_type (iddb,validity,classname) values (10,365,'lcsb.mapviewer.annotation.services.TaxonomyBackend');