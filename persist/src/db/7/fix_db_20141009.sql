--fix rna regions
CREATE SEQUENCE rna_region_table_idrnaregiondb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

  CREATE TABLE rna_region_table
(
  iddb integer NOT NULL,
  idrnaregion character varying(255),
  name character varying(255),
  pos double precision,
  size double precision,
  type character varying(255),
  state character varying(255),
  idspeciesdb integer,
  CONSTRAINT rna_region_table_pkey PRIMARY KEY (iddb),
  CONSTRAINT rna_region_species_iddb_fk FOREIGN KEY (idspeciesdb)
      REFERENCES species_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE rna_region_table
ALTER COLUMN iddb set DEFAULT nextval('rna_region_table_idrnaregiondb_seq'::regclass);

--and now antisense rna regions
ALTER TABLE antisense_rna_region_table
ALTER COLUMN idantisensernaregiondb set DEFAULT nextval('antisense_rna_region_table_idrnaregiondb_seq'::regclass);