DROP table IF EXISTS data_mining_set_table;
DROP sequence IF EXISTS data_mining_set_iddb_seq;

CREATE SEQUENCE data_mining_set_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE data_mining_set_iddb_seq
  OWNER TO map_viewer;

CREATE TABLE data_mining_set_table
(
  iddb integer NOT NULL DEFAULT nextval('data_mining_set_iddb_seq'::regclass),
  model_iddb integer,
  name character varying(255),
  description character varying(255),
  source character varying(255),
  inputdata bytea,
  type integer,

  CONSTRAINT data_mining_set_table_pkey PRIMARY KEY (iddb),
  CONSTRAINT data_mining_set_table_model_fk FOREIGN KEY (model_iddb)
      REFERENCES model_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE data_mining_set_table
  OWNER TO map_viewer;
