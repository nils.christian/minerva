alter table layout add column inputdata bytea;
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (533, 'Uploaded datasets are available for download', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));

