insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (463, 'User privileges for adding project', 'BUG_FIX', null, 
  (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (464, 'Data autofill problem in user management', 'BUG_FIX', null, 
  (select iddb from external_user where email ='stephan.gebel@uni.lu'));
