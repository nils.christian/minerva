-- uniprot annotator
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (462, 'Advanced annotation options', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (462, 'Uniprot annotation module', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));
