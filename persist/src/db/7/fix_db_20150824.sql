alter table user_table add column annotationschema_iddb integer;

alter table user_table drop CONSTRAINT if exists user_user_annotation_schema_fk;

DROP table IF EXISTS class_annotator_annotators_table;
DROP table IF EXISTS class_required_annotation_miriam_type_table;
DROP table IF EXISTS class_valid_annotation_miriam_type_table;
DROP table IF EXISTS class_annotator_table;
DROP table IF EXISTS class_required_annotation_table;
DROP table IF EXISTS class_valid_annotation_table;
DROP table IF EXISTS user_annotation_schema_table;
DROP sequence IF EXISTS user_annotation_schema_iddb_seq;
DROP sequence IF EXISTS class_valid_annotation_iddb_seq;
DROP sequence IF EXISTS class_required_annotation_iddb_seq;
DROP sequence IF EXISTS class_annotator_iddb_seq;

CREATE SEQUENCE user_annotation_schema_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE user_annotation_schema_iddb_seq
  OWNER TO map_viewer;

CREATE TABLE user_annotation_schema_table
(
  iddb integer NOT NULL DEFAULT nextval('user_annotation_schema_iddb_seq'::regclass),
  user_iddb integer,
  validatemiriamtypes boolean,

  CONSTRAINT user_annotation_schema_pkey PRIMARY KEY (iddb),
  CONSTRAINT user_annotation_schema_user_fk FOREIGN KEY (user_iddb)
      REFERENCES user_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_annotation_schema_table
  OWNER TO map_viewer;
  
alter table user_table add CONSTRAINT user_user_annotation_schema_fk FOREIGN KEY (annotationschema_iddb)
      REFERENCES user_annotation_schema_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
  

CREATE SEQUENCE class_valid_annotation_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE class_valid_annotation_iddb_seq
  OWNER TO map_viewer;

CREATE TABLE class_valid_annotation_table
(
  iddb integer NOT NULL DEFAULT nextval('class_valid_annotation_iddb_seq'::regclass),
  annotationschema_iddb integer,
  classname character varying(255),

  CONSTRAINT class_valid_annotation_pkey PRIMARY KEY (iddb),
  CONSTRAINT class_valid_annotation_user_annotation_schema_fk FOREIGN KEY (annotationschema_iddb)
      REFERENCES user_annotation_schema_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE class_valid_annotation_table
  OWNER TO map_viewer;


CREATE SEQUENCE class_annotator_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE class_annotator_iddb_seq
  OWNER TO map_viewer;

CREATE TABLE class_annotator_table
(
  iddb integer NOT NULL DEFAULT nextval('class_annotator_iddb_seq'::regclass),
  annotationschema_iddb integer,
  classname character varying(255),

  CONSTRAINT class_annotator_pkey PRIMARY KEY (iddb),
  CONSTRAINT class_annotator_user_annotation_schema_fk FOREIGN KEY (annotationschema_iddb)
      REFERENCES user_annotation_schema_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE class_annotator_table
  OWNER TO map_viewer;


CREATE TABLE class_annotator_annotators_table
(
  class_annotator_iddb integer NOT NULL,
  idx integer NOT NULL,
  annotator_name character varying(255),

  CONSTRAINT class_annotator_annotators_fk FOREIGN KEY (class_annotator_iddb)
      REFERENCES class_annotator_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE class_annotator_table
  OWNER TO map_viewer;


CREATE TABLE class_valid_annotation_miriam_type_table
(
  class_valid_annotation_iddb integer NOT NULL,
  idx integer NOT NULL,
  miriam_type_name character varying(255),

  CONSTRAINT class_valid_annotation_miriam_type_fk FOREIGN KEY (class_valid_annotation_iddb)
      REFERENCES class_valid_annotation_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE class_valid_annotation_miriam_type_table
  OWNER TO map_viewer;

CREATE SEQUENCE class_required_annotation_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE class_required_annotation_iddb_seq
  OWNER TO map_viewer;

CREATE TABLE class_required_annotation_table
(
  iddb integer NOT NULL DEFAULT nextval('class_required_annotation_iddb_seq'::regclass),
  annotationschema_iddb integer,
  requireatlestoneannotation boolean,
  classname character varying(255),

  CONSTRAINT class_required_annotation_pkey PRIMARY KEY (iddb),
  CONSTRAINT class_required_annotation_user_annotation_schema_fk FOREIGN KEY (annotationschema_iddb)
      REFERENCES user_annotation_schema_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE class_required_annotation_table
  OWNER TO map_viewer;

CREATE TABLE class_required_annotation_miriam_type_table
(
  class_required_annotation_iddb integer NOT NULL,
  idx integer NOT NULL,
  miriam_type_name character varying(255),

  CONSTRAINT class_required_annotation_miriam_type_fk FOREIGN KEY (class_required_annotation_iddb)
      REFERENCES class_required_annotation_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE class_required_annotation_miriam_type_table
  OWNER TO map_viewer;
