CREATE SEQUENCE plugin_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE plugin_table
(
  iddb integer NOT NULL DEFAULT nextval('plugin_table_iddb_seq'::regclass),
  hash character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  version character varying(255) NOT NULL,
  CONSTRAINT plugin_pkey PRIMARY KEY (iddb),
  CONSTRAINT plugin_hash_unique UNIQUE (hash)
);

CREATE TABLE plugin_urls
(
  plugin_iddb integer NOT NULL,
  url character varying(1024) NOT NULL,
  CONSTRAINT plugin_urls_pkey PRIMARY KEY (plugin_iddb, url),
  CONSTRAINT plugin_urls_fk FOREIGN KEY (plugin_iddb)
      REFERENCES public.plugin_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE SEQUENCE plugin_data_entry_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE plugin_data_entry
(
  iddb integer NOT NULL DEFAULT nextval('plugin_table_iddb_seq'::regclass),
  plugin_iddb integer NOT NULL,
  user_iddb integer,
  entry_key character varying(1024) NOT NULL,
  entry_value character varying(2048) NOT NULL,
  CONSTRAINT plugin_data_entry_pkey PRIMARY KEY (iddb),
  CONSTRAINT plugin_data_entry_plugin_fk FOREIGN KEY (plugin_iddb)
      REFERENCES public.plugin_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT plugin_data_entry_user_fk FOREIGN KEY (user_iddb)
      REFERENCES public.user_table (iddb) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);
