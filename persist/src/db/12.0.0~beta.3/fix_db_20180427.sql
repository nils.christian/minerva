-- alternative implementation of map canvas

alter table project_table add column google_map_license_accept_date timestamp without time zone;
alter table project_table add column map_canvas_type character varying default 'OPEN_LAYERS';
