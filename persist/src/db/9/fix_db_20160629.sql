insert into cache_type select 3,365,'lcsb.mapviewer.annotation.services.MiriamConnector' where (select count(*) from cache_type where iddb = 3) =0;

insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1022, 'Installation script fixed', 'BUG_FIX', null, null);

insert into framework_version(version,"time", svnversion) values (9, timestamp'2016-06-29',1023);
