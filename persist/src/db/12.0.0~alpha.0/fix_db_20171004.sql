--uploaded files that are uploaded in chunks should have info about how big should be the file
alter table file_entry add column length bigint;
update file_entry set length = octet_length(filecontent);

--add foreign key
alter table file_entry add column owner_iddb integer;
ALTER TABLE file_entry ADD CONSTRAINT file_entry_owner_constraint FOREIGN KEY (owner_iddb)
      REFERENCES public.user_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--add owners for layout files 
update file_entry SET owner_iddb = (select (max(creator_iddb)) from layout where file_entry_iddb = file_entry.iddb);      