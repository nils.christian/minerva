-- clear invalid urls from miriam connector
delete from cachequery where type = (select iddb from cache_type where classname='lcsb.mapviewer.annotation.services.MiriamConnector') and value = 'INVALID';

-- argument for neutral color scheme
alter table user_table add column neutralcolor bytea;
