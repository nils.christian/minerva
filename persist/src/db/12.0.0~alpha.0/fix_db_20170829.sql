--user prferences for uploading map
alter table user_annotation_schema_table add column annotatemodel boolean default false;
alter table user_annotation_schema_table add column autoresizemap boolean default true;
alter table user_annotation_schema_table add column cachedata boolean default true;
alter table user_annotation_schema_table add column semanticzooming boolean default false;
alter table class_required_annotation_table RENAME column requireatlestoneannotation to requireatleastoneannotation;

-- rename config annotation class name
update class_annotator_table set classname ='lcsb.mapviewer.model.map.BioEntity' where classname ='lcsb.mapviewer.model.map.AnnotatedObject';
update class_required_annotation_table set classname ='lcsb.mapviewer.model.map.BioEntity' where classname ='lcsb.mapviewer.model.map.AnnotatedObject';
update class_valid_annotation_table set classname ='lcsb.mapviewer.model.map.BioEntity' where classname ='lcsb.mapviewer.model.map.AnnotatedObject';


