DELETE FROM cache_type WHERE classname = 'lcsb.mapviewer.annotation.services.annotators.KeggAnnotator';
INSERT INTO cache_type(validity, classname) VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.KeggAnnotator');

UPDATE class_annotator_annotators_table  SET annotator_name = 'TAIR' WHERE annotator_name = 'TAIR Locus';

