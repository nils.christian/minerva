alter table submodel_connection_table rename fromalias_iddb to fromelement_iddb;
alter table submodel_connection_table rename toalias_iddb to toelement_iddb;

alter table alias_table rename to element_table;

update element_table set alias_type_db ='Bottom square Compartment' where alias_type_db = 'Bottom square Compartment Alias';
update element_table set alias_type_db ='Left square Compartment' where alias_type_db = 'Left square Compartment Alias';
update element_table set alias_type_db ='Oval Compartment' where alias_type_db = 'Oval Compartment Alias';
update element_table set alias_type_db ='Compartment' where alias_type_db = 'Compartment Alias';
update element_table set alias_type_db ='Pathway Compartment' where alias_type_db = 'Artifitial Compartment Alias';
update element_table set alias_type_db ='Right square Compartment' where alias_type_db = 'Right square Compartment Alias';
update element_table set alias_type_db ='Square Compartment' where alias_type_db = 'Square Compartment Alias';
update element_table set alias_type_db ='Top square Compartment' where alias_type_db = 'Top square Compartment Alias';

alter table node_table drop column if exists element_iddb;
alter table node_table rename alias_iddb to element_iddb;

update element_table set alias_type_db ='Complex' where alias_type_db = 'Complex Species Alias';
update element_table set alias_type_db ='Species' where alias_type_db = 'Species Alias';

alter table element_table rename alias_type_db  to element_type_db;
alter table element_table  rename idcomplexaliasdb to idcomplexdb;

alter table element_table  rename aliasstatelabel to statelabel;
alter table element_table  rename aliasstateprefix to stateprefix;
