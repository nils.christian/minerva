update element_table set idcomplexdb=parent_iddb where idcomplexdb is null and parent_iddb is not null and parent_iddb in (select iddb from element_table where element_type_db= 'Complex');

update element_table set compartment_iddb = parent_iddb where compartment_iddb is null and parent_iddb is not null and parent_iddb in (select iddb from element_table where element_type_db<> 'Complex');

alter table element_table  drop COLUMN parent_iddb ;

alter SEQUENCE alias_table_iddb_seq rename to element_table_iddb_seq;

drop sequence associated_phenotype_element_table_iddb_seq;

drop sequence layout_iddb_seq;

alter sequence layout_iddb_seq1 rename to layout_iddb_seq;

drop sequence search_index_table_iddb_seq;

alter sequence search_index_table_iddb_seq1 rename to search_index_table_iddb_seq;

alter table antisense_rna_region_table_new RENAME to antisense_rna_region_table;
alter table element_former_symbols_new rename to element_former_symbols;
alter table element_synonyms_new rename to element_synonyms;
alter table modification_residue_table_new rename to modification_residue_table;
alter table rna_region_table_new rename to rna_region_table;
