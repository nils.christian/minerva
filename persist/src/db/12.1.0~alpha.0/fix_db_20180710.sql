-- state of modification_residue is mapped via string

alter table modification_residue_table add column state_string varchar(255);
update modification_residue_table set state_string='PHOSPHORYLATED' where state=0;
update modification_residue_table set state_string='ACETYLATED' where state=1;
update modification_residue_table set state_string='UBIQUITINATED' where state=2;
update modification_residue_table set state_string='METHYLATED' where state=3;
update modification_residue_table set state_string='HYDROXYLATED' where state=4;
update modification_residue_table set state_string='MYRISTOYLATED' where state=5;
update modification_residue_table set state_string='SULFATED' where state=6;
update modification_residue_table set state_string='PRENYLATED' where state=7;
update modification_residue_table set state_string='GLYCOSYLATED' where state=8;
update modification_residue_table set state_string='PALMYTOYLATED' where state=9;
update modification_residue_table set state_string='UNKNOWN' where state=10;
update modification_residue_table set state_string='EMPTY' where state=11;
update modification_residue_table set state_string='PROTONATED' where state=12;
update modification_residue_table set state_string='DONT_CARE' where state=13;
alter table modification_residue_table drop column state;
alter table modification_residue_table rename column state_string to state;

-- coordinates for rna region changed into absolute x,y

alter table rna_region_table add column position varchar(255);
update rna_region_table set position=concat(element_table.x+element_table.width/4 +(element_table.width*3/4*rna_region_table.pos),',',element_table.y) from  element_table where element_table.iddb = rna_region_table.idspeciesdb and not pos is null;
update rna_region_table set position=concat(element_table.x,',',element_table.y) from  element_table where element_table.iddb = rna_region_table.idspeciesdb and pos is null;
alter table rna_region_table drop column pos;

