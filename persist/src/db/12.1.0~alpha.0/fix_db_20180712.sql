-- modification residue should have height 
alter table modification_residue_table add column height numeric(6,2);
update modification_residue_table set height = 10.0;

-- transcription site activity and direction
alter table modification_residue_table add column direction varchar(255);
