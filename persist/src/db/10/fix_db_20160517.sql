-- remove metaid columns (some useless identifier)
alter table species_table drop column metaid;
alter table reaction_table drop column metaid;
alter table model_table drop column metaid;
