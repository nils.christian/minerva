insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1024, 'Search by reaction id is not case sensitive', 'BUG_FIX', null, 
  (select iddb from external_user where email ='alberto.noronha@uni.lu'));
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1024, 'CellDesigner font size is processed properly', 'BUG_FIX', null, 
  (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));

--column with input file data for a project
alter table project_table add column inputdata bytea;
ALTER TABLE project_table ADD COLUMN inputfilename varchar;