insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1049, 'Out of memory when caching a lot of data', 'BUG_FIX', null, 
  (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
  
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1049, 'Management of overlays in admin panel', 'FUNCTIONALITY', null, 
  (select iddb from external_user where email ='marek.ostaszewski@uni.lu'));
  
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1049, 'Zooming issue when searching for results on submaps', 'BUG_FIX', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));
  
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (1049, 'Exception when accessing non-existing project', 'BUG_FIX', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));
  
