package lcsb.mapviewer.model;

import lcsb.mapviewer.model.cache.AllCacheTests;
import lcsb.mapviewer.model.graphics.AllGraphicsTests;
import lcsb.mapviewer.model.log.AllLogTests;
import lcsb.mapviewer.model.map.AllMapTests;
import lcsb.mapviewer.model.user.AllUserTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AllCacheTests.class, //
		AllGraphicsTests.class, //
		AllLogTests.class, //
		AllMapTests.class, //
		AllUserTests.class, //
		ProjectStatusTest.class, //
		ProjectTest.class,//
})
public class AllModelTests {

}
