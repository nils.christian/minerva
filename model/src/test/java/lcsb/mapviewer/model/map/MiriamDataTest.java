package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class MiriamDataTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new MiriamData());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidConstructor1() {
		try {
			new MiriamData(null, null);
			fail("Exception expection");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidConstructor2() {
		try {
			new MiriamData(null, MiriamType.CAS, null);
			fail("Exception expection");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetIdFromIdentifier() {
		try {
			assertEquals("id", MiriamData.getIdFromIdentifier("name:id"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			int id = 6;
			MiriamData md = new MiriamData();
			md.setResource(null);
			assertNull(md.getResource());

			md.setId(id);
			assertEquals(id, md.getId());
			
			md.setAnnotator(this.getClass());
			assertEquals(this.getClass(), md.getAnnotator());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToString() {
		try {
			assertNotNull(new MiriamData().toString());
			assertNotNull(new MiriamData(MiriamType.CAS, "a").toString());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEqual() {
		try {
			MiriamData md = new MiriamData();
			assertFalse(md.equals(new Object()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCompareTo() {
		try {
			MiriamData md = new MiriamData();
			assertTrue(md.compareTo(new MiriamData(MiriamType.CAS, "a")) != 0);
			
			MiriamData md1 = new MiriamData(MiriamType.CAS, "a");
			MiriamData md2 = new MiriamData(MiriamType.CAS, "a");
			assertTrue(md1.compareTo(md2) == 0);
			
			md1.setAnnotator(Integer.class);
			assertTrue(md1.compareTo(md2) != 0);
			
			md2.setAnnotator(Integer.class);
			assertTrue(md1.compareTo(md2) == 0);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
