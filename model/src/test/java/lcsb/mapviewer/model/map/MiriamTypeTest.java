package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class MiriamTypeTest {
	Logger logger = Logger.getLogger(MiriamDataTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIdUniqness() {
		try {
			Map<String, MiriamType> ids = new HashMap<>();
			for (MiriamType mt : MiriamType.values()) {
				assertNull(mt.getCommonName() + " doesn't have a unique key", ids.get(mt.getRegistryIdentifier()));
				ids.put(mt.getRegistryIdentifier(), mt);

				// coverage tests
				assertNotNull(MiriamType.valueOf(mt.name()));
				assertNotNull(mt.getUris());
				assertNotNull(mt.getValidClass());
				assertNotNull(mt.getRequiredClass());
				if (mt != MiriamType.UNKNOWN) {
					assertNotNull("No homepage for: " + mt, mt.getDbHomepage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetMiriamDataFromIdentifier() {
		MiriamData md = MiriamType.getMiriamDataFromIdentifier("HGNC Symbol:SNCA");
		assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"), md);
	}

	@Test
	public void testGetMiriamDataFromInvalidIdentifier() {
		try {
			MiriamType.getMiriamDataFromIdentifier("xyz:SNCA");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		}
	}

	@Test
	public void testGetMiriamDataFromInvalidIdentifier2() {
		try {
			MiriamType.getMiriamDataFromIdentifier("xyz");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		}
	}

	@Test
	public void testTypeByUri() {
		assertNotNull(MiriamType.getTypeByUri("urn:miriam:cas"));
		assertNull(MiriamType.getTypeByUri("xyz"));
	}

	@Test
	public void testTypeByCommonName() {
		assertNotNull(MiriamType.getTypeByCommonName("Consensus CDS"));
		assertNull(MiriamType.getTypeByCommonName("xyz"));
	}

	@Test
	public void testGetMiriamByUri1() throws Exception {
		try {
			MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family:PTHR19384:SF5");
			assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetMiriamByUri2() throws Exception {
		try {
			MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family:PTHR19384%3ASF5");
			assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetMiriamByUri3() throws Exception {
		try {
			MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family%3APTHR19384%3ASF5");
			assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetMiriamByInvalidUri() throws Exception {
		try {
			MiriamType.getMiriamByUri("invalid_uri");
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
