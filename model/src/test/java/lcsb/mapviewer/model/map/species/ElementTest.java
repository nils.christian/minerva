package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.SearchIndex;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class ElementTest extends ModelTestFunctions {

	private static double EPSILON = Configuration.EPSILON;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDistance1() {
		Species species = new GenericProtein();
		species.setWidth(10);
		species.setHeight(20);
		species.setX(100.0);
		species.setY(200.0);
		double distance = species.getDistanceFromPoint(species.getCenter());
		assertEquals(0, distance, EPSILON);
	}

	@Test
	public void testDistance2() {
		Species species = new GenericProtein();
		species.setWidth(10);
		species.setHeight(20);
		species.setX(100.0);
		species.setY(200.0);
		double distance = species.getDistanceFromPoint(new Point2D.Double(100, 205));
		assertEquals(0, distance, EPSILON);
	}

	@Test
	public void testDistance6() {
		Species species = new GenericProtein();
		species.setWidth(10);
		species.setHeight(20);
		species.setX(100.0);
		species.setY(200.0);
		double distance = species.getDistanceFromPoint(new Point2D.Double(105, 95));
		assertEquals(105, distance, EPSILON);
	}

	@Test
	public void testDistance3() {
		Species species = new GenericProtein();
		species.setWidth(10);
		species.setHeight(20);
		species.setX(100.0);
		species.setY(200.0);
		double distance = species.getDistanceFromPoint(new Point2D.Double(50, 205));
		assertEquals(50, distance, EPSILON);
	}

	@Test
	public void testDistance4() {
		Species species = new GenericProtein();
		species.setWidth(10);
		species.setHeight(20);
		species.setX(100.0);
		species.setY(200.0);
		double distance = species.getDistanceFromPoint(new Point2D.Double(120, 230));
		assertEquals(Math.sqrt(200), distance, EPSILON);
	}

	@Test
	public void testConstructor() {
		try {
			GenericProtein protein = new GenericProtein();
			protein.addSearchIndex(new SearchIndex());
			protein.setSubmodel(new ElementSubmodelConnection());
			Element copy = new GenericProtein(protein);
			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testIncreaseBorder() {
		try {
			Species protein = new GenericProtein();
			assertEquals(0.0, protein.getSize(), Configuration.EPSILON);
			protein.increaseBorder(2);
			assertEquals(16.0, protein.getSize(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			Element element = new GenericProtein();
			Double nullDouble = null;
			String xStr = "1.2";
			Double x = 1.2;
			String yStr = "1.3";
			Double y = 1.3;
			String fontSizeStr = "5.0";
			Double fontSize = 5.0;
			int fontSizeInt = 5;
			String widthStr = "3.0";
			Double width = 3.0;
			String heightStr = "4.0";
			Double height = 4.0;
			ModelData modelData = new ModelData();
			Set<MiriamData> miriamData = new HashSet<>();
			Compartment compartment = new Compartment("idd");
			int id = 72;
			String transparencyLevel = "2";
			Model model = new ModelFullIndexed(modelData);
			List<SearchIndex> indexes = new ArrayList<>();
			indexes.add(new SearchIndex());

			element.setMiriamData(miriamData);
			assertEquals(miriamData, element.getMiriamData());

			element.setX(xStr);
			assertEquals(x, element.getX(), Configuration.EPSILON);
			element.setX(nullDouble);
			assertNull(element.getX());
			element.setX(x);
			assertEquals(x, element.getX(), Configuration.EPSILON);

			element.setY(yStr);
			assertEquals(y, element.getY(), Configuration.EPSILON);
			element.setY(nullDouble);
			assertNull(element.getY());
			element.setY(y);
			assertEquals(y, element.getY(), Configuration.EPSILON);

			element.setFontSize(fontSizeStr);
			assertEquals(fontSize, element.getFontSize(), Configuration.EPSILON);
			element.setFontSize(nullDouble);
			assertNull(element.getFontSize());
			element.setFontSize(fontSize);
			assertEquals(fontSize, element.getFontSize(), Configuration.EPSILON);
			element.setFontSize(nullDouble);
			assertNull(element.getFontSize());
			element.setFontSize(fontSizeInt);
			assertEquals(fontSize, element.getFontSize(), Configuration.EPSILON);

			element.setWidth(widthStr);
			assertEquals(width, element.getWidth(), Configuration.EPSILON);
			element.setWidth(nullDouble);
			assertNull(element.getWidth());
			element.setWidth(width);
			assertEquals(width, element.getWidth(), Configuration.EPSILON);
			try {
				element.setWidth("as");
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}

			element.setHeight(heightStr);
			assertEquals(height, element.getHeight(), Configuration.EPSILON);
			element.setHeight(nullDouble);
			assertNull(element.getHeight());
			element.setHeight(height);
			assertEquals(height, element.getHeight(), Configuration.EPSILON);
			try {
				element.setHeight("as");
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}

			element.setCompartment(compartment);
			assertEquals(compartment, element.getCompartment());
			element.setId(id);
			assertEquals(id, element.getId());
			element.setTransparencyLevel(transparencyLevel);
			assertEquals(transparencyLevel, element.getTransparencyLevel());
			element.setModel(model);
			assertEquals(model, element.getModel());
			element.setModelData(modelData);
			assertEquals(modelData, element.getModelData());
			element.setSearchIndexes(indexes);
			assertEquals(indexes, element.getSearchIndexes());

			assertNotNull(element.getBorder());

			element = Mockito.spy(Element.class);
			List<String> synonyms = new ArrayList<>();
			List<String> formerSymbols = new ArrayList<>();
			String formula = "some_form";

			element.setSynonyms(synonyms);
			element.setFormerSymbols(formerSymbols);
			element.setModel(model);
			element.setFormula(formula);

			assertEquals(synonyms, element.getSynonyms());
			assertEquals(formerSymbols, element.getFormerSymbols());
			assertEquals(model.getModelData(), element.getModelData());
			assertEquals(formula, element.getFormula());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testContainsElement() {
		try {
			Species element = new GenericProtein();
			element.setWidth(100);
			element.setHeight(100);
			Species element2 = new GenericProtein();
			element2.setX(-2);
			element2.setY(-2);
			element2.setWidth(100);
			element2.setHeight(100);

			assertTrue(element.contains(element2));
			element2.setX(2);
			element2.setY(2);
			assertTrue(element.contains(element2));
			element2.setX(-2);
			element2.setY(2);
			assertTrue(element.contains(element2));
			element2.setX(2);
			element2.setY(-2);
			assertTrue(element.contains(element2));

			element2.setX(-2);
			element2.setY(-2);
			element2.setWidth(200);
			element2.setHeight(200);
			assertFalse(element.contains(element2));

			Compartment compartment = new Compartment("idd");
			compartment.setX(2);
			compartment.setY(2);
			compartment.setWidth(10);
			compartment.setHeight(100);
			assertFalse(element.contains(compartment));

			compartment.setWidth(100);
			compartment.setHeight(10);
			assertFalse(element.contains(compartment));

			compartment.setWidth(10);
			compartment.setHeight(10);
			assertTrue(element.contains(compartment));

			compartment.setWidth(100);
			compartment.setHeight(100);
			compartment.setX(-2);
			compartment.setY(2);
			assertFalse(element.contains(compartment));
			compartment.setX(2);
			compartment.setY(-2);
			assertFalse(element.contains(compartment));
			compartment.setX(-2);
			compartment.setY(-2);
			assertFalse(element.contains(compartment));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testContainsLayerText() {
		try {
			Species protein = new GenericProtein();
			protein.setWidth(100);
			protein.setHeight(100);
			LayerText layerText = new LayerText();
			layerText.setX(2.0);
			layerText.setY(2.0);
			layerText.setWidth(100.0);
			layerText.setHeight(100.0);

			assertFalse(protein.contains(layerText));
			layerText.setWidth(10.0);
			layerText.setHeight(10.0);
			assertTrue(protein.contains(layerText));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddMiriamCollection() {
		try {
			List<MiriamData> list = new ArrayList<>();
			list.add(new MiriamData(MiriamType.CAS, "1"));
			list.add(new MiriamData(MiriamType.CAS, "1"));
			Element element = Mockito.spy(Element.class);
			element.addMiriamData(list);
			assertEquals(1, element.getMiriamData().size());
			assertEquals(1, getWarnings().size());

			element.addMiriamData(list);
			assertEquals(3, getWarnings().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetNotes() {
		try {
			Element element = Mockito.spy(Element.class);
			element.setNotes("</html>");
			fail("Exception epxected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddSynonym() {
		try {
			Element element = Mockito.spy(Element.class);
			List<String> synonyms = new ArrayList<>();
			synonyms.add("syn");
			element.addSynonyms(synonyms);
			element.addSynonym("syn2");
			assertEquals("syn", element.getSynonyms().get(0));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 300; i++) {
				sb.append("a");
			}
			String syn = sb.toString();
			synonyms = new ArrayList<>();
			synonyms.add(syn);
			element.addSynonyms(synonyms);
			assertFalse(syn.equals(element.getSynonyms().get(1)));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddFormerSymbols() {
		try {
			Element element = Mockito.spy(Element.class);
			List<String> formerSymbols = new ArrayList<>();
			formerSymbols.add("symbol");
			element.addFormerSymbols(formerSymbols);
			element.addFormerSymbol("symbol2");
			assertEquals("symbol", element.getFormerSymbols().get(0));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 300; i++) {
				sb.append("a");
			}
			String syn = sb.toString();
			formerSymbols = new ArrayList<>();
			formerSymbols.add(syn);
			element.addFormerSymbols(formerSymbols);
			assertFalse(syn.equals(element.getFormerSymbols().get(1)));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
