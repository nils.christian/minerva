package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class RightSquareCompartmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new RightSquareCompartment());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			RightSquareCompartment compartment = new RightSquareCompartment(new RightSquareCompartment());
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor3() {
		try {
			RightSquareCompartment compartment = new RightSquareCompartment("id");
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			Model model = new ModelFullIndexed(null);
			model.setWidth(1);
			model.setHeight(1);
			RightSquareCompartment compartment = new RightSquareCompartment(new Compartment(), model);
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			RightSquareCompartment compartment = new RightSquareCompartment().copy();
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetPoint() {
		try {
			RightSquareCompartment compartment = new RightSquareCompartment();
			compartment.setPoint("10", "10");
			assertTrue(compartment.getWidth() < 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			RightSquareCompartment compartment = Mockito.spy(RightSquareCompartment.class);
			compartment.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
