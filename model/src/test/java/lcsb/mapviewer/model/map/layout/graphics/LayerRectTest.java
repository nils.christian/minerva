package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.awt.Color;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerRectTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new LayerRect());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			LayerRect layerRect = new LayerRect();

			LayerRect copy = new LayerRect(layerRect);

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			LayerRect copy = new LayerRect().copy();

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new LayerRect() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();

			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			LayerRect rect = new LayerRect();

			String yParam = "1.2";
			Double y = 1.2;

			String xParam = "2.2";
			Double x = 2.2;

			String widthParam = "10.2";
			Double width = 10.2;

			String heightParam = "72.2";
			Double height = 72.2;

			String invalidNumberStr = "xxd";

			Color color = Color.BLACK;

			rect.setY(yParam);
			assertEquals(y, rect.getY(), Configuration.EPSILON);
			try {
				rect.setY(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			rect.setY((Double) null);
			assertNull(rect.getY());
			rect.setY(y);
			assertEquals(y, rect.getY(), Configuration.EPSILON);

			rect.setX(xParam);
			assertEquals(x, rect.getX(), Configuration.EPSILON);
			try {
				rect.setX(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			rect.setX((Double) null);
			assertNull(rect.getX());
			rect.setX(x);
			assertEquals(x, rect.getX(), Configuration.EPSILON);

			rect.setWidth(widthParam);
			assertEquals(width, rect.getWidth(), Configuration.EPSILON);
			try {
				rect.setWidth(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			rect.setWidth((Double) null);
			assertNull(rect.getWidth());
			rect.setWidth(width);
			assertEquals(width, rect.getWidth(), Configuration.EPSILON);

			rect.setHeight(heightParam);
			assertEquals(height, rect.getHeight(), Configuration.EPSILON);
			try {
				rect.setHeight(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			rect.setHeight((Double) null);
			assertNull(rect.getHeight());
			rect.setHeight(height);
			assertEquals(height, rect.getHeight(), Configuration.EPSILON);

			rect.setColor(color);
			assertEquals(color, rect.getColor());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
