package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SbmlFunctionComparatorTest {
  SbmlFunctionComparator comparator = new SbmlFunctionComparator();

  @Test
  public void testCompareEqual() {
    SbmlFunction Function = createFunction();
    SbmlFunction Function2 = createFunction();
    assertEquals(0, comparator.compare(Function, Function2));
  }

  @Test
  public void testCompareDifferentFunctionId() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.setFunctionId("xxx");
    assertTrue("Function object have different id but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  @Test
  public void testCompareDifferentName() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.setName("xxx");
    assertTrue("Function object have different name but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  @Test
  public void testCompareDifferentDefinition() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.setDefinition("xxx");
    assertTrue("Function object have different name but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  @Test
  public void testCompareDifferentArguments() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.addArgument("x");
    assertTrue("Function object have different name but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  private SbmlFunction createFunction() {
    SbmlFunction result = new SbmlFunction("fun_id");
    return result;
  }

}
