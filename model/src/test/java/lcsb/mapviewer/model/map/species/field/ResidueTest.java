package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ResidueTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    Residue mr = new Residue();
    Species species = new GenericProtein("id");
    int id = 94;

    mr.setSpecies(species);
    mr.setId(id);

    assertEquals(species, mr.getSpecies());
    assertEquals(id, mr.getId());
  }

  @Test
  public void testCopy() {
    Residue mr = new Residue().copy();
    assertNotNull(mr);
  }

}
