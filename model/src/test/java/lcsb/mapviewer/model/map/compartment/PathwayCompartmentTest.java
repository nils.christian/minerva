package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.CompartmentComparator;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;

public class PathwayCompartmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		try {
			CompartmentComparator comparator = new CompartmentComparator();
			Compartment pathway = new PathwayCompartment("id");
			Compartment copy = pathway.copy();

			assertEquals(0, comparator.compare(pathway, copy));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new PathwayCompartment("id"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			PathwayCompartment pathway = new PathwayCompartment("id");
			String title = "tit27";
			pathway.setName(title);
			assertEquals(title, pathway.getName());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			PathwayCompartment comp = Mockito.spy(PathwayCompartment.class);
			comp.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			PathwayCompartment degraded = new PathwayCompartment("id").copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
