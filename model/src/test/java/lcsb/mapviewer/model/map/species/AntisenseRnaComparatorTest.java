package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;

public class AntisenseRnaComparatorTest {

  AntisenseRnaComparator comparator = new AntisenseRnaComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    try {
      AntisenseRna aRna1 = createAntisenseRna();
      AntisenseRna aRna2 = createAntisenseRna();

      assertEquals(0, comparator.compare(aRna1, aRna1));

      assertEquals(0, comparator.compare(aRna1, aRna2));
      assertEquals(0, comparator.compare(aRna2, aRna1));

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  @Test
  public void testDifferent() {
    try {
      AntisenseRna aRna1 = createAntisenseRna();
      AntisenseRna aRna2 = createAntisenseRna();
      aRna1.getRegions().get(0).setName("bla");
      assertTrue(comparator.compare(aRna1, aRna2) != 0);
      assertTrue(comparator.compare(aRna2, aRna1) != 0);

      aRna1 = createAntisenseRna();
      aRna2 = createAntisenseRna();
      aRna1.getRegions().clear();
      assertTrue(comparator.compare(aRna1, aRna2) != 0);
      assertTrue(comparator.compare(aRna2, aRna1) != 0);

      aRna1 = createAntisenseRna();
      aRna2 = createAntisenseRna();
      assertTrue(comparator.compare(null, aRna2) != 0);
      assertTrue(comparator.compare(aRna2, null) != 0);
      assertTrue(comparator.compare(null, null) == 0);

      AntisenseRna unknown = createAntisenseRna();
      unknown.setName("n");
      assertTrue(comparator.compare(unknown, aRna2) != 0);

      assertTrue(comparator.compare(unknown, Mockito.mock(AntisenseRna.class)) != 0);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  public AntisenseRna createAntisenseRna() {
    AntisenseRna result = new AntisenseRna();

    CodingRegion region1 = new CodingRegion();
    result.addRegion(region1);
    region1.setIdModificationResidue("a");
    region1.setName("name");
    region1.setPosition(new Point2D.Double(0, 1));
    region1.setWidth(2);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    AntisenseRna element = Mockito.mock(AntisenseRna.class);
    comparator.compare(element, element);
  }
}
