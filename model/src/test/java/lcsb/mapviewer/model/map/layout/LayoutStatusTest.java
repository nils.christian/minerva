package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LayoutStatusTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidValues() {
		for (LayoutStatus type : LayoutStatus.values()) {
			assertNotNull(type);

			// for coverage tests
			LayoutStatus.valueOf(type.toString());
			assertNotNull(type.getCommonName());
		}
	}

}
