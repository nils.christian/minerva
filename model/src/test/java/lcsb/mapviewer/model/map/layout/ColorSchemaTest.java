package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

public class ColorSchemaTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetGetters() throws Exception {
    try {
      ColorSchema cs = new GenericColorSchema();
      List<String> compartments = new ArrayList<>();
      List<Class<? extends Element>> types = new ArrayList<>();
      String name = "S";
      int matches = 79;
      Double lineWidth = 5.89;
      String reactionIdentifier = "re id";
      Boolean reverseReaction = true;

      cs.setName(name);
      assertEquals(name, cs.getName());

      cs.setCompartments(compartments);
      assertEquals(compartments, cs.getCompartments());

      cs.setTypes(types);
      assertEquals(types, cs.getTypes());

      cs.setMatches(matches);
      assertEquals(matches, cs.getMatches());

      cs.setLineWidth(lineWidth);
      assertEquals(lineWidth, cs.getLineWidth());

      cs.setElementId(reactionIdentifier);
      assertEquals(reactionIdentifier, cs.getElementId());

      cs.setReverseReaction(reverseReaction);
      assertEquals(reverseReaction, cs.getReverseReaction());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToString() throws Exception {
    try {
      ColorSchema cs = new GenericColorSchema();
      assertNotNull(cs.toString());
      cs.setName("gene name");
      cs.addCompartment("A");
      cs.addType(Species.class);
      cs.setValue(1.2);
      cs.setColor(Color.BLACK);
      cs.addMiriamData(new MiriamData(MiriamType.CAS, "X"));
      assertNotNull(cs.toString());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddCompartments() throws Exception {
    try {
      ColorSchema cs = new GenericColorSchema();
      cs.addCompartments(new String[] { "a", "b" });
      assertEquals(2, cs.getCompartments().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddCompartments2() throws Exception {
    try {
      ColorSchema cs = new GenericColorSchema();
      List<String> compartments = new ArrayList<>();
      compartments.add("a");
      compartments.add("b");
      cs.addCompartments(compartments);
      assertEquals(2, cs.getCompartments().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddIdentifgierColumns() throws Exception {
    try {
      MiriamData generalIdentifier = new MiriamData();
      ColorSchema cs = new GenericColorSchema();
      List<Pair<MiriamType, String>> compartments = new ArrayList<>();
      compartments.add(new Pair<MiriamType, String>(MiriamType.CAS, "x"));
      cs.addMiriamData(generalIdentifier);

      assertEquals(1, cs.getMiriamData().size());
      assertEquals(generalIdentifier, cs.getMiriamData().iterator().next());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddTypes() throws Exception {
    try {
      ColorSchema cs = new GenericColorSchema();
      List<Class<? extends Element>> compartments = new ArrayList<>();
      compartments.add(Species.class);
      compartments.add(Protein.class);
      cs.addTypes(compartments);
      assertEquals(2, cs.getTypes().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
