package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerRectComparatorTest {
	LayerRectComparator comparator = new LayerRectComparator();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals() {
		try {
			LayerRect layer1 = getLayerRect();
			LayerRect layer2 = getLayerRect();
			
			assertEquals(0, comparator.compare(new LayerRect(), new LayerRect()));
			assertEquals(0, comparator.compare(layer1, layer2));
			assertEquals(0, comparator.compare(layer1, layer1));
			assertEquals(0, comparator.compare(null,null));
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	private LayerRect getLayerRect() {
		
		LayerRect line = new LayerRect();
		line.setColor(Color.YELLOW);
		line.setWidth(4.3);
		
		
		return line;
	}

	@Test
	public void testDifferent() {
		try {
			LayerRect layer1 = getLayerRect();
			LayerRect layer2 = getLayerRect();
			
			assertTrue(comparator.compare(layer1, new LayerRect())!=0);

			assertTrue(comparator.compare(layer1, null)!=0);
			assertTrue(comparator.compare(null,layer1)!=0);
			
			layer1 = getLayerRect();
			layer2 = getLayerRect();
			
			layer1.setColor(Color.GREEN);
			
			assertTrue(comparator.compare(layer1, layer2)!=0);
			assertTrue(comparator.compare(layer2, layer1)!=0);
			
			layer1 = getLayerRect();
			layer2 = getLayerRect();
			
			layer1.setHeight("23");
			
			assertTrue(comparator.compare(layer1, layer2)!=0);
			assertTrue(comparator.compare(layer2, layer1)!=0);
			
			layer1 = getLayerRect();
			layer2 = getLayerRect();
			
			layer1.setHeight(1.2);
			
			assertTrue(comparator.compare(layer1, layer2)!=0);
			assertTrue(comparator.compare(layer2, layer1)!=0);
			
			layer1 = getLayerRect();
			layer2 = getLayerRect();
			
			layer1.setX(33.4);
			
			assertTrue(comparator.compare(layer1, layer2)!=0);
			assertTrue(comparator.compare(layer2, layer1)!=0);
			
			layer1 = getLayerRect();
			layer2 = getLayerRect();
			
			layer1.setY(33.4);
			
			assertTrue(comparator.compare(layer1, layer2)!=0);
			assertTrue(comparator.compare(layer2, layer1)!=0);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
	@Test
	public void testDifferent2() {
		try {
			LayerRect layer1 = getLayerRect();

			assertTrue(comparator.compare(layer1, new LayerRect() {
				private static final long serialVersionUID = 1L;
			}) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testInvalid() {
		try {
			class Tmp extends LayerRect {
				private static final long serialVersionUID = 1L;
			}
			Tmp layer1 = new Tmp();
			Tmp layer2 = new Tmp();

			comparator.compare(layer1, layer2);
			fail("Exception expected");

		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
