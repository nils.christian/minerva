package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class AndOperatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new AndOperator());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      AndOperator op = new AndOperator();
      op.setLine(new PolylineData());
      AndOperator operator = new AndOperator(op);
      assertNotNull(operator);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      AndOperator operator = new AndOperator();
      assertFalse("".equals(new AndOperator().getSBGNOperatorText()));
      assertFalse("".equals(operator.getOperatorText()));

      NodeOperator nodeOperatorForInput = new AndOperator();
      NodeOperator nodeOperatorForOutput = new AndOperator();

      Reaction reaction = new Reaction();

      operator.setReaction(reaction);
      operator.setNodeOperatorForInput(nodeOperatorForInput);
      operator.setNodeOperatorForOutput(nodeOperatorForOutput);

      assertEquals(reaction, operator.getReaction());
      assertEquals(nodeOperatorForInput, operator.getNodeOperatorForInput());
      assertEquals(nodeOperatorForOutput, operator.getNodeOperatorForOutput());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy1() {
    try {
      AndOperator op = new AndOperator();
      op.setLine(new PolylineData());
      AndOperator operator = op.copy();
      assertNotNull(operator);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy2() {
    try {
      Mockito.mock(AndOperator.class, Mockito.CALLS_REAL_METHODS).copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
