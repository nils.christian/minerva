package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ModelComparatorTest {
  Logger logger = Logger.getLogger(ModelComparatorTest.class);

  ModelComparator comparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      assertEquals(0, comparator.compare(new ModelFullIndexed(null), new ModelFullIndexed(null)));
      assertEquals(0, comparator.compare(model1, model2));
      assertEquals(0, comparator.compare(model1, model1));
      assertEquals(0, comparator.compare(null, null));

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknown exception");
    }
  }

  @Test
  public void testDifferent2() {
    try {
      Model model1 = getModel();
      Model model2 = Mockito.mock(Model.class);

      assertTrue(comparator.compare(model1, model2) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDifferent3() {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      Compartment compartment = new PathwayCompartment("1");
      Compartment compartment2 = new PathwayCompartment("12");
      model1.addElement(compartment);
      model1.addElement(compartment2);

      compartment = new PathwayCompartment("1");
      compartment2 = new PathwayCompartment("12");
      model2.addElement(compartment);
      model2.addElement(compartment2);
      compartment2.setElementId("1");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDifferentReactionsInReactionSet() {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      Reaction reaction1 = new Reaction();
      Reaction reaction2 = new Reaction();
      Reaction reaction3 = new Reaction();
      Reaction reaction4 = new Reaction();
      reaction1.setIdReaction("a");
      reaction2.setIdReaction("b");
      reaction3.setIdReaction("a");
      reaction4.setIdReaction("b");

      model1.addReaction(reaction1);
      model1.addReaction(reaction2);
      model2.addReaction(reaction3);
      model2.addReaction(reaction4);

      reaction2.setIdReaction("a");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDifferentReactionSet() {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      model1.addReaction(new TransportReaction("id"));

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalid() {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      Species mockSpecies = Mockito.mock(Species.class);
      when(mockSpecies.getElementId()).thenReturn("1");
      model1.addElement(mockSpecies);

      model2.addElement(mockSpecies);

      try {
        comparator.compare(model1, model2);
        fail("Exception expected");

      } catch (NotImplementedException e) {

      }
      try {
        comparator.compare(model2, model1);
        fail("Exception expected");

      } catch (NotImplementedException e) {

      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDifferent() throws Exception {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      assertTrue(comparator.compare(model1, new ModelFullIndexed(null)) != 0);
      assertTrue(comparator.compare(model1, null) != 0);
      assertTrue(comparator.compare(null, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.setNotes("ASDsaD");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      GenericProtein protein = new GenericProtein("SAd");

      model1.addElement(protein);

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.getElements().iterator().next().setElementId("sdfsd");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.addElement(new Compartment("unk_id"));

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.getElementByElementId("default").setName("tmpxx");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.addLayer(new Layer());

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.getLayers().iterator().next().setName("buu");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.addElement(new GenericProtein("ASdas"));

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.getElements().iterator().next()
          .addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_HAS_PART, MiriamType.CHEBI, "c"));

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.getReactions().iterator().next().setName("dsf");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.setIdModel("asdsdasd");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.setWidth(123);

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.setHeight(636);

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.setZoomLevels(1);

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.setTileSize(129);

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      model1 = getModel();
      model2 = getModel();

      model1.addLayout(new Layout());

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);
    model.setNotes("Some description");
    GenericProtein protein = new GenericProtein("a_id");
    protein.setName("ad");

    model.addElement(protein);

    model.addElement(new Compartment("default"));

    Layer layer = new Layer();
    layer.setName("layer name");
    model.addLayer(layer);

    model.addLayout(new Layout("1", "2", true));

    model.addReaction(new Reaction());
    return model;
  }

  @Test
  public void testCompareSubmodels() throws Exception {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      Model model3 = getModel();

      ModelSubmodelConnection submodelA = new ModelSubmodelConnection(model3, SubmodelType.DOWNSTREAM_TARGETS);

      model1.addSubmodelConnection(submodelA);

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      Model model4 = getModel();

      ModelSubmodelConnection submodelB = new ModelSubmodelConnection(model4, SubmodelType.DOWNSTREAM_TARGETS);

      model2.addSubmodelConnection(submodelB);

      assertTrue(comparator.compare(model1, model2) == 0);

      submodelB.setName("A");

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

      submodelB.setName(null);
      assertTrue(comparator.compare(model1, model2) == 0);

      model4.setNotes("ASdasdhsjkadhask");
      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCompareSubmodels2() throws Exception {
    try {
      Model model1 = getModel();
      Model model2 = getModel();

      model1.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS));
      model1.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS));

      model2.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS));
      model2.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.PATHWAY));

      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCompareName() throws Exception {
    try {
      Model model1 = getModel();
      Model model2 = getModel();
      model1.setName("ASD");
      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);
      model2.setName("A");
      assertTrue(comparator.compare(model1, model2) != 0);
      assertTrue(comparator.compare(model2, model1) != 0);
      model1.setName("A");
      assertEquals(0, comparator.compare(model1, model2));
      assertEquals(0, comparator.compare(model2, model1));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
