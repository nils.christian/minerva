package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class ModelSubmodelConnectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new ModelSubmodelConnection());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null), SubmodelType.DOWNSTREAM_TARGETS, "str");
			ModelSubmodelConnection connection2 = new ModelSubmodelConnection(connection);
			assertNotNull(connection2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null), SubmodelType.DOWNSTREAM_TARGETS, "str");

			assertNotNull(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null), SubmodelType.DOWNSTREAM_TARGETS, "str");
			ModelSubmodelConnection degraded = connection.copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			ModelSubmodelConnection connection = new ModelSubmodelConnection(new ModelFullIndexed(null), SubmodelType.DOWNSTREAM_TARGETS, "str");

			int id = 30;
			connection.setId(id);
			assertEquals(id, connection.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new ModelSubmodelConnection() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
