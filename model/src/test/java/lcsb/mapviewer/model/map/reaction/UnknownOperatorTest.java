package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class UnknownOperatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new UnknownOperator());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      UnknownOperator op = new UnknownOperator();
      op.setLine(new PolylineData());
      UnknownOperator operator = new UnknownOperator(op);
      assertNotNull(operator);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      assertFalse("".equals(new UnknownOperator().getSBGNOperatorText()));
      assertFalse("".equals(new UnknownOperator().getOperatorText()));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy1() {
    try {
      UnknownOperator op = new UnknownOperator();
      op.setLine(new PolylineData());
      UnknownOperator operator = op.copy();
      assertNotNull(operator);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy2() {
    try {
      Mockito.mock(UnknownOperator.class, Mockito.CALLS_REAL_METHODS).copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
