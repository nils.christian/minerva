package lcsb.mapviewer.model.map.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ElementSubmodelConnectionComparatorTest.class, //
		ElementSubmodelConnectionTest.class, //
		ModelComparatorTest.class, //
		ModelDataTest.class, //
		ModelFullIndexedTest.class, //
		ModelSubmodelConnectionComparatorTest.class, //
		ModelSubmodelConnectionTest.class, //
		ModelTest.class, //
		SubmodelConnectionComparatorTest.class,//
		SubmodelTypeTest.class,//
})
public class AllModelTests {

}
