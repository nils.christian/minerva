package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.LayoutComparator;

public class LayoutComparatorTest {
	LayoutComparator comparator = new LayoutComparator();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals() {
		try {
			Layout layout1 = getLayout();
			Layout layout2 = getLayout();

			assertEquals(0, comparator.compare(new Layout(), new Layout()));
			assertEquals(0, comparator.compare(layout1, layout2));
			assertEquals(0, comparator.compare(layout1, layout1));
			assertEquals(0, comparator.compare(null, null));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	private Layout getLayout() {
		Layout layout = new Layout();
		layout.setDirectory("dirrr");
		layout.setTitle("title");
		return layout;
	}

	@Test
	public void testDifferent() {
		try {
			Layout layout1 = getLayout();
			Layout layout2 = getLayout();

			assertTrue(comparator.compare(layout1, new Layout()) != 0);

			assertTrue(comparator.compare(layout1, null) != 0);
			assertTrue(comparator.compare(null, layout1) != 0);

			layout1 = getLayout();
			layout2 = getLayout();

			layout1.setDirectory("ASDsaD");

			assertTrue(comparator.compare(layout1, layout2) != 0);
			assertTrue(comparator.compare(layout2, layout1) != 0);

			layout1 = getLayout();
			layout2 = getLayout();

			layout1.setTitle("ASDsaD");

			assertTrue(comparator.compare(layout1, layout2) != 0);
			assertTrue(comparator.compare(layout2, layout1) != 0);

			layout1 = getLayout();
			layout2 = getLayout();

			layout1.setHierarchicalView(true);

			assertTrue(comparator.compare(layout1, layout2) != 0);
			assertTrue(comparator.compare(layout2, layout1) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testDifferent2() {
		try {
			class Tmp extends Layout {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}
			;

			Layout layout1 = new Tmp();
			Layout layout2 = new Layout();

			assertTrue(comparator.compare(layout1, layout2) != 0);
			assertTrue(comparator.compare(layout2, layout1) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testInvalid() {
		try {
			class Tmp extends Layout {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}
			;

			Layout layout1 = new Tmp();
			Layout layout2 = new Tmp();

			comparator.compare(layout1, layout2);
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

}
