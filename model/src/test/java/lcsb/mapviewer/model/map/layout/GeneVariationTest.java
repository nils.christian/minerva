package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;

public class GeneVariationTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetters() throws Exception {
		try {
			String modifiedDna = "str";

			int position = 12;

			GeneVariation gv = new GeneVariation();

			gv.setModifiedDna(modifiedDna);
			assertEquals(modifiedDna, gv.getModifiedDna());

			gv.setPosition(position);

			assertEquals((long) position, (long) gv.getPosition());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddReference() throws Exception {
		try {
			GeneVariation gv = new GeneVariation();
			gv.addReference(new MiriamData());
			assertEquals(1, gv.getReferences().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() throws Exception {
		try {
			String modifiedDna = "str";

			GeneVariation gv = new GeneVariation();

			gv.setModifiedDna(modifiedDna);
			gv.addReference(new MiriamData());

			GeneVariation copy = gv.copy();

			assertEquals(gv.getModifiedDna(), copy.getModifiedDna());
			assertEquals(1, copy.getReferences().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() throws Exception {
		try {
			GeneVariation gv = new GeneVariation() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			};

			gv.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
