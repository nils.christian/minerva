package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.User;

public class ModelFullIndexedTest {
  Logger logger = Logger.getLogger(ModelFullIndexedTest.class);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      Species protein = new GenericProtein("1a");
      Species protein2 = new GenericProtein("f");
      Species protein3 = new GenericProtein("fa");

      Element compartment = new Compartment("aaa");
      Element compartment3 = new Compartment("aaa3");
      Element compartment2 = new PathwayCompartment("aaa2");
      model.addElement(protein);
      model.addElement(protein2);
      model.addElement(protein3);
      model.addElement(compartment);
      model.addElement(compartment2);
      model.addElement(compartment3);

      Reaction reaction = new TransportReaction();
      reaction.addReactant(new Reactant(protein));
      reaction.addProduct(new Product(protein2));
      model.addReaction(reaction);
      model.setProject(new Project());

      ModelData submodel = new ModelData();
      model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.DOWNSTREAM_TARGETS));
      Model submodel2 = new ModelFullIndexed(null);
      model.addSubmodelConnection(new ModelSubmodelConnection(submodel2, SubmodelType.DOWNSTREAM_TARGETS));

      Layout layout = new Layout();
      layout.setCreator(new User());
      model.addLayout(layout);

      ModelFullIndexed model2 = new ModelFullIndexed(model.getModelData());

      assertNotNull(model2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetByName() {
    try {
      String name1 = "1a";
      String name2 = "2a";
      String unknownName = "unk";
      ModelFullIndexed model = new ModelFullIndexed(null);
      Species protein = new GenericProtein("id1");
      protein.setName(name1);
      Species protein2 = new GenericProtein("id2");
      protein2.setName(name2);

      model.addElement(protein);
      model.addElement(protein2);

      List<Element> elements = model.getElementsByName(name1);
      List<Element> unkElements = model.getElementsByName(unknownName);

      assertEquals(1, elements.size());
      assertEquals(0, unkElements.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSpeciesList() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      Species protein = new GenericProtein("id1");
      Species protein2 = new GenericProtein("id2");

      model.addElement(protein);
      model.addElement(protein2);
      model.addElement(new Compartment("compId"));

      List<Species> speciesList = model.getSpeciesList();

      assertEquals(2, speciesList.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor2() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      ModelFullIndexed model2 = new ModelFullIndexed(model.getModelData());

      assertNotNull(model2);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddInvalidElement6() {
    try {
      Element elementMock = Mockito.mock(Element.class);
      ModelFullIndexed model = new ModelFullIndexed(null);
      model.addElement(elementMock);

      new ModelFullIndexed(model.getModelData());

      fail("Exception expected");

    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddInvalidElement5() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Complex complex = new Complex("1");

      model.addElement(complex);
      try {
        model.addElement(complex);
        fail("Exception expected");
      } catch (InvalidArgumentException e) {
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddSpeciesWithParent() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      model.addElement(new Compartment("default"));

      Complex complex = new Complex("1");

      GenericProtein protein = new GenericProtein("asd");
      protein.setElementId("zz");
      protein.setComplex(complex);

      model.addElement(complex);

      model.addElement(protein);

      assertEquals(3, model.getElements().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddSpeciesWithParentWithCompartment() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Compartment compartment = new Compartment("default");
      model.addElement(compartment);

      Complex complex = new Complex("1");
      complex.setCompartment(compartment);

      GenericProtein protein = new GenericProtein("asd");
      protein.setElementId("zz");
      protein.setComplex(complex);

      model.addElement(complex);

      model.addElement(protein);

      assertEquals(3, model.getElements().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetElementByElementId() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      assertNull(model.getElementByElementId("id"));
      Species protein = new GenericProtein("asd");
      protein.setElementId("id");

      model.addElement(protein);

      assertNotNull(model.getElementByElementId("id"));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddCompartment2() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Compartment compartment = new Compartment("1");

      assertEquals(0, model.getCompartments().size());
      model.addElement(compartment);
      assertEquals(1, model.getCompartments().size());
      model.addElement(compartment);

      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetReactionByReactionId() {
    try {
      String reactionId = "id_r";
      ModelFullIndexed model = new ModelFullIndexed(null);
      Reaction reaction = new Reaction();
      reaction.setIdReaction(reactionId);
      assertNull(model.getReactionByReactionId(reactionId));
      model.addReaction(reaction);
      assertEquals(reaction, model.getReactionByReactionId(reactionId));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetReactionByDbId() {
    try {
      int reactionId = 12;
      ModelFullIndexed model = new ModelFullIndexed(null);
      Reaction reaction = new Reaction();
      reaction.setId(reactionId);
      assertNull(model.getReactionByDbId(reactionId));
      model.addReaction(reaction);
      assertEquals(reaction, model.getReactionByDbId(reactionId));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetElementByDbId() {
    try {
      int elementId = 12;
      ModelFullIndexed model = new ModelFullIndexed(null);
      Element protein = new GenericProtein("1");
      protein.setId(elementId);
      assertNull(model.getElementByDbId(elementId));
      model.addElement(protein);
      assertEquals(protein, model.getElementByDbId(elementId));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSortedCompartments() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      Compartment compartment = new Compartment("a1");
      compartment.setWidth(12);
      compartment.setHeight(12);
      model.addElement(compartment);
      compartment = new Compartment("a2");
      compartment.setWidth(14);
      compartment.setHeight(14);
      model.addElement(compartment);
      compartment = new Compartment("a3");
      compartment.setWidth(13);
      compartment.setHeight(13);
      model.addElement(compartment);

      List<Compartment> compartments = model.getSortedCompartments();
      assertEquals(196.0, compartments.get(0).getSize(), Configuration.EPSILON);
      assertEquals(169.0, compartments.get(1).getSize(), Configuration.EPSILON);
      assertEquals(144.0, compartments.get(2).getSize(), Configuration.EPSILON);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveCompartment2() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      Compartment compartment = new Compartment("a1");
      compartment.setWidth(12);
      compartment.setHeight(12);
      model.addElement(compartment);

      compartment = new Compartment("a2");
      compartment.setWidth(14);
      compartment.setHeight(14);
      model.addElement(compartment);
      assertEquals(2, model.getElements().size());

      model.removeElement(compartment);
      assertEquals(1, model.getElements().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddElements() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Compartment compartment = new Compartment("1");
      compartment.setWidth(100);
      compartment.setHeight(100);
      List<Element> elements = new ArrayList<>();
      elements.add(compartment);

      Complex complex = new Complex("ca1");
      elements.add(complex);
      complex.setWidth(10);
      complex.setHeight(10);

      Species species = new GenericProtein("a1");
      elements.add(species);
      species.setWidth(20);
      species.setHeight(20);

      assertEquals(0, model.getCompartments().size());
      model.addElements(elements);
      assertEquals(1, model.getCompartments().size());
      assertEquals(1, model.getComplexList().size());
      assertEquals(1, model.getNotComplexSpeciesList().size());

      List<Element> sortedSpecies = model.getSortedSpeciesList();

      assertEquals(compartment, sortedSpecies.get(0));
      assertEquals(species, sortedSpecies.get(1));
      assertEquals(complex, sortedSpecies.get(2));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddInvalidElement2() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      GenericProtein protein = new GenericProtein((String) null);
      model.addElement(protein);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveCompartment() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Compartment parent = new Compartment("parent_id");

      Compartment compartment = new Compartment("child_id");
      compartment.setCompartment(parent);

      model.addElement(compartment);

      assertEquals(1, model.getElements().size());

      model.removeElement(compartment);
      assertEquals(0, model.getElements().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveComplex() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      model.addElement(new Compartment("default"));

      Complex parentComplex = new Complex("a");

      Complex childComplex = new Complex("b");
      childComplex.setComplex(parentComplex);
      childComplex.setElementId("1");

      childComplex.setCompartment(new Compartment("comp_id"));
      childComplex.setComplex(new Complex("d"));

      model.addElement(childComplex);

      assertEquals(2, model.getElements().size());

      childComplex.setComplex(new Complex("xxx"));

      model.removeElement(childComplex);
      assertEquals(1, model.getElements().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddLayers() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Layer layer = new Layer();
      List<Layer> layers = new ArrayList<>();
      layers.add(layer);

      assertEquals(0, model.getLayers().size());
      model.addLayers(layers);
      assertEquals(1, model.getLayers().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetElementByAnnotation() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Reaction reaction = new Reaction();
      reaction.addMiriamData(new MiriamData(MiriamType.CAS, "C"));
      model.addReaction(reaction);

      Set<BioEntity> objects = model.getElementsByAnnotation(new MiriamData());
      assertEquals(0, objects.size());
      objects = model.getElementsByAnnotation(new MiriamData(MiriamType.CAS, "C"));
      assertTrue(objects.contains(reaction));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetLayoutByIdentifier() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Integer id = 903;
      Layout layout = new Layout();
      layout.setId(id);
      model.addLayout(layout);

      Layout result = model.getLayoutByIdentifier(id);
      assertEquals(layout, result);

      result = model.getLayoutByIdentifier(id + 1);

      assertNull(result);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetNotes() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      model.setNotes("<html/>");
      fail("Exception expected");
    } catch (InvalidArgumentException e) {

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddLayout() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      Layout layout = new Layout();

      model.addLayout(0, layout);

      assertEquals(1, model.getLayouts().size());

      model.removeLayout(layout);

      assertEquals(0, model.getLayouts().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testStubCoverageTests() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);

      model.addElementGroup(null);
      model.addBlockDiagream(null);

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSubmodelByName() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      ModelFullIndexed child = new ModelFullIndexed(null);
      String parentName = "PARENT M";
      String childName = "MY CHILD";
      model.setName(parentName);
      child.setName(childName);
      model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS));

      assertNull(model.getSubmodelByName("xyZ"));
      assertNull(model.getSubmodelByName(null));
      assertEquals(model, model.getSubmodelByName(parentName));
      assertEquals(child, model.getSubmodelByName(childName));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSubmodelById() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      ModelFullIndexed child = new ModelFullIndexed(null);
      Integer parentId = 73;
      Integer childId = 92;
      model.setId(parentId);
      child.setId(childId);
      model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS));

      assertNull(model.getSubmodelById(543543));
      assertNull(model.getSubmodelById((String) null));
      assertNull(model.getSubmodelById((Integer) null));
      assertEquals(model, model.getSubmodelById(parentId + ""));
      assertEquals(child, model.getSubmodelById(childId + ""));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetSubmodelByConnectionName() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      ModelFullIndexed child = new ModelFullIndexed(null);
      String name = "bla";
      model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS, name));

      assertNull(model.getSubmodelByConnectionName(null));
      assertNull(model.getSubmodelByConnectionName("basdkjas"));
      assertEquals(child, model.getSubmodelByConnectionName(name));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddReactions() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      Reaction reaction = new Reaction();
      reaction.setIdReaction("b");
      List<Reaction> reactions = new ArrayList<>();
      reactions.add(reaction);
      Reaction reaction2 = new Reaction();
      reaction2.setIdReaction("a");
      reactions.add(reaction2);

      model.addReactions(reactions);

      assertTrue(model.getReactions().contains(reaction));

      assertEquals(reaction2, model.getSortedReactions().get(0));
      assertEquals(reaction, model.getSortedReactions().get(1));

      model.removeReaction(reaction2);

      assertEquals(1, model.getReactions().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSubmodels() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      ModelFullIndexed child = new ModelFullIndexed(null);
      model.addSubmodelConnection(new ModelSubmodelConnection(child, SubmodelType.DOWNSTREAM_TARGETS));

      assertTrue(model.getSubmodels().contains(child));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      Model model = new ModelFullIndexed(null);

      Double width = 12.0;
      int widthInt = 12;
      String widthStr = "12.0";
      Double height = 13.0;
      String heightStr = "13.0";
      int heightInt = 13;
      Set<Element> elements = new HashSet<>();
      List<Layout> layouts = new ArrayList<>();
      int zoomLevels = 98;
      int tileSize = 1024;
      String idModel = "model_ID";
      Calendar creationDate = Calendar.getInstance();

      model.setWidth(widthStr);
      assertEquals(width, model.getWidth());
      model.setWidth((Double) null);
      assertNull(model.getWidth());
      model.setWidth(width);
      assertEquals(width, model.getWidth());
      model.setWidth((Double) null);
      assertNull(model.getWidth());
      model.setWidth(widthInt);
      assertEquals(width, model.getWidth());

      model.setHeight(heightStr);
      assertEquals(height, model.getHeight());
      model.setHeight((Double) null);
      assertNull(model.getHeight());
      model.setHeight(height);
      assertEquals(height, model.getHeight());
      model.setHeight((Double) null);
      assertNull(model.getHeight());
      model.setHeight(heightInt);
      assertEquals(height, model.getHeight());

      model.setElements(elements);
      assertEquals(elements, model.getElements());
      model.setLayouts(layouts);
      assertEquals(layouts, model.getLayouts());
      model.setZoomLevels(zoomLevels);
      assertEquals(zoomLevels, model.getZoomLevels());
      model.setTileSize(tileSize);
      assertEquals(tileSize, model.getTileSize());
      model.setIdModel(idModel);
      assertEquals(idModel, model.getIdModel());
      model.setCreationDate(creationDate);
      assertEquals(creationDate, model.getCreationDate());

      assertNotNull(model.getParentModels());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }

  }

  @Test
  public void testGetAnnotatedObjects() {
    try {
      String reactionId = "id_r";
      ModelFullIndexed model = new ModelFullIndexed(null);
      Reaction reaction = new Reaction();
      reaction.setIdReaction(reactionId);
      model.addReaction(reaction);
      Species protein = new GenericProtein("2");
      model.addElement(protein);

      Collection<BioEntity> obj = model.getBioEntities();
      assertEquals(2, obj.size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
