package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class BooleanLogicGateReactionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new BooleanLogicGateReaction());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			Reaction reaction = new BooleanLogicGateReaction();
			assertNotNull(reaction.getStringType());
			assertNull(reaction.getReactionRect());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructorWithInvalidArg() {
		try {
			Reaction reaction = new Reaction();
			new BooleanLogicGateReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructorWithInvalidArg2() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			new BooleanLogicGateReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			reaction.addReactant(new Reactant());
			reaction.addReactant(new Reactant());
			BooleanLogicGateReaction validReaction = new BooleanLogicGateReaction(reaction);
			assertNotNull(validReaction);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			BooleanLogicGateReaction original = new BooleanLogicGateReaction();
			original.addProduct(new Product());
			original.addReactant(new Reactant());
			original.addReactant(new Reactant());
			BooleanLogicGateReaction product = original.copy();
			assertNotNull(product);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new BooleanLogicGateReaction() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
