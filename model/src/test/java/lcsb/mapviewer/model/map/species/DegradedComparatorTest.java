package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class DegradedComparatorTest {

  DegradedComparator comparator = new DegradedComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    try {
      Degraded degraded1 = createDegraded();
      Degraded degraded2 = createDegraded();

      assertEquals(0, comparator.compare(degraded1, degraded1));

      assertEquals(0, comparator.compare(degraded1, degraded2));
      assertEquals(0, comparator.compare(degraded2, degraded1));

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  @Test
  public void testDifferent() {
    try {
      Degraded degraded1 = createDegraded();
      Degraded degraded2 = createDegraded();
      degraded1 = createDegraded();
      degraded2 = createDegraded();
      degraded1.setCharge(54);
      assertTrue(comparator.compare(degraded1, degraded2) != 0);
      assertTrue(comparator.compare(degraded2, degraded1) != 0);

      degraded1 = createDegraded();
      degraded2 = createDegraded();
      assertTrue(comparator.compare(null, degraded2) != 0);
      assertTrue(comparator.compare(degraded2, null) != 0);
      assertTrue(comparator.compare(null, null) == 0);

      Degraded degraded = createDegraded();
      degraded.setName("n");
      assertTrue(comparator.compare(degraded, degraded1) != 0);

      assertTrue(comparator.compare(degraded, Mockito.mock(Degraded.class)) != 0);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  public Degraded createDegraded() {
    Degraded result = new Degraded();
    result.setCharge(12);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Degraded object = Mockito.mock(Degraded.class);

    comparator.compare(object, object);
  }
}
