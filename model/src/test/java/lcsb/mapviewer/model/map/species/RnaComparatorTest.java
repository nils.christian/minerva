package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class RnaComparatorTest {

  RnaComparator comparator = new RnaComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    try {
      Rna aRna1 = createRna();
      Rna aRna2 = createRna();

      assertEquals(0, comparator.compare(aRna1, aRna1));

      assertEquals(0, comparator.compare(aRna1, aRna2));
      assertEquals(0, comparator.compare(aRna2, aRna1));

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  @Test
  public void testDifferent() {
    try {
      Rna aRna1 = createRna();
      Rna aRna2 = createRna();
      aRna1.getRegions().get(0).setName("X");
      assertTrue(comparator.compare(aRna1, aRna2) != 0);
      assertTrue(comparator.compare(aRna2, aRna1) != 0);

      aRna1 = createRna();
      aRna2 = createRna();
      aRna1.getRegions().clear();
      assertTrue(comparator.compare(aRna1, aRna2) != 0);
      assertTrue(comparator.compare(aRna2, aRna1) != 0);

      aRna1 = createRna();
      aRna2 = createRna();
      assertTrue(comparator.compare(null, aRna2) != 0);
      assertTrue(comparator.compare(aRna2, null) != 0);
      assertTrue(comparator.compare(null, null) == 0);

      Rna rna = createRna();
      rna.setName("n");
      assertTrue(comparator.compare(rna, aRna2) != 0);

      assertTrue(comparator.compare(rna, Mockito.mock(Rna.class)) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  public Rna createRna() {
    Rna result = new Rna();
    result.setHypothetical(true);

    CodingRegion region1 = new CodingRegion();
    result.addRegion(region1);
    region1.setIdModificationResidue("a");
    region1.setPosition(new Point2D.Double(0, 10));
    region1.setWidth(2.0);
    return result;
  }

  @Test
  public void testInvalid() {
    try {
      Rna object = Mockito.mock(Rna.class);

      comparator.compare(object, object);

      fail("Exception expected");
    } catch (NotImplementedException e) {

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

}
