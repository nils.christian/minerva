package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class OverviewModelLinkTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new OverviewModelLink());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			OverviewModelLink link = new OverviewModelLink(new OverviewModelLink());
			assertNotNull(link);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetters() {
		try {
			OverviewModelLink link = new OverviewModelLink();
			link.setLinkedModel(null);
			assertNull(link.getLinkedModel());

			link.setxCoord(2.0);
			link.setyCoord(3.0);

			assertEquals((Integer) 2, link.getxCoord());
			assertEquals((Integer) 3, link.getyCoord());

			link.setxCoord((Double) null);
			link.setyCoord((Double) null);
			assertNull(link.getxCoord());
			assertNull(link.getyCoord());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			OverviewModelLink degraded = new OverviewModelLink().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new OverviewModelLink() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
