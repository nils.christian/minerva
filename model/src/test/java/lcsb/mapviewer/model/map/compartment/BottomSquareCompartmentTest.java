package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class BottomSquareCompartmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new BottomSquareCompartment());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			BottomSquareCompartment compartment = new BottomSquareCompartment(new Compartment());
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor3() {
		try {
			BottomSquareCompartment compartment = new BottomSquareCompartment("id");
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			Model model = new ModelFullIndexed(null);
			model.setWidth(1);
			model.setHeight(1);
			BottomSquareCompartment compartment = new BottomSquareCompartment(new Compartment(), model);
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			BottomSquareCompartment compartment = new BottomSquareCompartment().copy();
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			BottomSquareCompartment compartment = Mockito.spy(BottomSquareCompartment.class);
			compartment.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
