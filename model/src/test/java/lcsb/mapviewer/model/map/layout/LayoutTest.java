package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class LayoutTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new Layout());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      String title = "TIT";
      String dir = "TIT";
      boolean publicL = true;
      Layout layout = new Layout(title, dir, publicL);
      assertEquals(title, layout.getTitle());
      assertEquals(dir, layout.getDirectory());
      assertEquals(publicL, layout.isPublicLayout());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor2() {
    try {
      String title = "TIT";
      String dir = "TIT";
      boolean publicL = true;
      Layout layout = new Layout(title, dir, publicL);
      Layout layout2 = new Layout(layout);
      assertNotNull(layout2);
      UploadedFileEntry fileEntry = new UploadedFileEntry();
      fileEntry.setFileContent(new byte[] {});

      layout.setInputData(fileEntry);
      Layout layout3 = new Layout(layout);
      assertNotNull(layout3);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructorWithOrder() {
    try {
      boolean publicL = true;
      Layout overlay1 = new Layout(null, null, publicL);
      overlay1.setOrderIndex(12);
      Layout overlay2 = new Layout(overlay1);
      assertEquals(overlay1.getOrderIndex(), overlay2.getOrderIndex());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      String title = "TIT";
      String dir = "TIT";
      boolean publicL = true;
      Layout layout = new Layout(title, dir, publicL);
      Layout layout2 = layout.copy();
      assertNotNull(layout2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidCopy() {
    try {
      Layout layout = new Layout() {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
      };
      layout.copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      ModelFullIndexed model = new ModelFullIndexed(null);
      Set<Layout> layouts = new HashSet<>();
      boolean hierarchicalView = false;
      boolean publicLayout = true;
      int id = 62;
      String description = "qds";
      String directory = "vcccv";
      String title = "tit";
      LayoutStatus status = LayoutStatus.FAILURE;
      double progress = 1.6;
      UploadedFileEntry inputData = new UploadedFileEntry();
      Layout parentLayout = new Layout();
      Layout layout = new Layout();
      layout.setHierarchicalView(hierarchicalView);
      assertEquals(hierarchicalView, layout.isHierarchicalView());
      layout.setId(id);
      assertEquals(id, layout.getId());
      layout.setDescription(description);
      assertEquals(description, layout.getDescription());
      layout.setDirectory(directory);
      assertEquals(directory, layout.getDirectory());
      layout.setTitle(title);
      assertEquals(title, layout.getTitle());
      layout.setModel((ModelData) null);
      assertNull(layout.getModel());
      layout.setCreator(null);
      assertNull(layout.getCreator());
      layout.setStatus(status);
      assertEquals(status, layout.getStatus());
      layout.setProgress(progress);
      assertEquals(progress, layout.getProgress(), Configuration.EPSILON);
      layout.setInputData(inputData);
      assertEquals(inputData, layout.getInputData());
      layout.setParentLayout(parentLayout);
      assertEquals(parentLayout, layout.getParentLayout());
      layout.setPublicLayout(publicLayout);
      assertEquals(publicLayout, layout.isPublicLayout());

      layout.setLayouts(layouts);
      assertEquals(layouts, layout.getLayouts());
      layout.addLayout(parentLayout);
      assertEquals(1, layouts.size());
      layout.setModel(model);
      assertEquals(model.getModelData(), layout.getModel());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
