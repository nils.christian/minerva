package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class AbstractNodeComparatorTest {

  AbstractNodeComparator comparator = new AbstractNodeComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = NotImplementedException.class)
  public void testCompareException() {
    comparator.compare(Mockito.mock(AbstractNode.class, Mockito.CALLS_REAL_METHODS),
        Mockito.mock(AbstractNode.class, Mockito.CALLS_REAL_METHODS));
  }

  @Test
  public void testCompareOk() {
    try {
      assertEquals(0, comparator.compare(new AndOperator(), new AndOperator()));
      assertEquals(0, comparator.compare(null, null));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testDifferent() {
    try {
      assertTrue(comparator.compare(new AndOperator(), null) != 0);
      assertTrue(comparator.compare(null, new AndOperator()) != 0);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
