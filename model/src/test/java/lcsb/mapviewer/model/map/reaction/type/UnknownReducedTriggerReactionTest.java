package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class UnknownReducedTriggerReactionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new UnknownReducedTriggerReaction());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void testConstructorWithInvalidArg() {
		try {
			Reaction reaction = new Reaction();
			new UnknownReducedTriggerReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructorWithInvalidArg2() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			new UnknownReducedTriggerReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			reaction.addReactant(new Reactant());
			UnknownReducedTriggerReaction validReaction = new UnknownReducedTriggerReaction(reaction);
			assertNotNull(validReaction);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			UnknownReducedTriggerReaction original = new UnknownReducedTriggerReaction();
			original.addProduct(new Product());
			original.addReactant(new Reactant());
			original.addReactant(new Reactant());
			UnknownReducedTriggerReaction product = original.copy();
			assertNotNull(product);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new UnknownReducedTriggerReaction() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	@Test
	public void testGetters() {
		try {
			UnknownReducedTriggerReaction original = new UnknownReducedTriggerReaction();
			assertNull(original.getReactionRect());
			assertNotNull(original.getStringType());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
