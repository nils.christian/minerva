package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class GeneVariationColorSchemaTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new GeneVariationColorSchema());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			GeneVariationColorSchema schema = new GeneVariationColorSchema();
			schema.setDescription("desc");
			GeneVariationColorSchema copy = new GeneVariationColorSchema(schema);
			assertEquals("desc", copy.getDescription());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			GeneVariationColorSchema schema = new GeneVariationColorSchema();
			schema.setDescription("desc");
			GeneVariationColorSchema copy = schema.copy();

			assertEquals("desc", copy.getDescription());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {

			List<GeneVariation> geneVariations = new ArrayList<>();

			GeneVariationColorSchema schema = new GeneVariationColorSchema();
			schema.setGeneVariations(geneVariations);

			assertEquals(geneVariations, schema.getGeneVariations());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddGeneVariations() {
		try {

			List<GeneVariation> geneVariations = new ArrayList<>();
			geneVariations.add(new GeneVariation());

			GeneVariationColorSchema schema = new GeneVariationColorSchema();
			schema.addGeneVariations(geneVariations);

			assertEquals(1, schema.getGeneVariations().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			GeneVariationColorSchema schema = new GeneVariationColorSchema() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			};
			schema.copy();
			fail("Exception expected");

		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
