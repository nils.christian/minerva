package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.species.field.ModificationState;

public class ModificationStateTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidValues() {
		for (ModificationState type : ModificationState.values()) {
			assertNotNull(type);

			// for coverage tests
			ModificationState.valueOf(type.toString());
			assertNotNull(type.getFullName());
			assertNotNull(type.getAbbreviation());
		}
	}

	@Test
	public void testGetByName() {
		assertNull(ModificationState.getByName("Adasd"));
		assertNotNull(ModificationState.getByName("protonated"));
	}

}
