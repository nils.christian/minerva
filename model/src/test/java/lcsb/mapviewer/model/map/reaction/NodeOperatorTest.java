package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class NodeOperatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			NodeOperator operator2 = new NodeOperatorMock();
			NodeOperator operator3 = new NodeOperatorMock();
			operator.addInput(operator2);
			operator.addOutput(operator3);
			operator.setLine(new PolylineData());

			NodeOperator result = new NodeOperatorMock(operator);
			assertEquals(1, result.getInputs().size());
			assertEquals(1, result.getOutputs().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testAddInput() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			NodeOperator operator2 = new NodeOperatorMock();
			operator.addInput(operator2);
			assertEquals(1, operator.getInputs().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testAddInputs() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			NodeOperator operator2 = new NodeOperatorMock();
			List<NodeOperator> list = new ArrayList<>();
			list.add(operator2);
			operator.addInputs(list);
			assertEquals(1, operator.getInputs().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testAddInput2() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			NodeOperator operator2 = new NodeOperatorMock();
			operator.addInput(operator2);
			operator.addInput(operator2);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testIsReactantOperator() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			assertFalse(operator.isReactantOperator());

			AndOperator op1 = new AndOperator();
			operator.addInput(op1);
			assertFalse(operator.isReactantOperator());

			op1.addInput(new Reactant());
			assertTrue(operator.isReactantOperator());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testAddOutputs() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			NodeOperator operator2 = new NodeOperatorMock();
			List<NodeOperator> list = new ArrayList<>();
			list.add(operator2);
			operator.addOutputs(list);
			assertEquals(1, operator.getOutputs().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testAddOutput2() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			NodeOperator operator2 = new NodeOperatorMock();
			operator.addOutput(operator2);
			operator.addOutput(operator2);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testIsProductOperator() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			assertFalse(operator.isReactantOperator());

			AndOperator op1 = new AndOperator();
			operator.addInput(op1);
			assertFalse(operator.isProductOperator());

			op1.addInput(new Reactant());
			assertFalse(operator.isProductOperator());

			SplitOperator op2 = new SplitOperator();
			operator.addOutput(op2);
			assertFalse(operator.isProductOperator());

			op2.addOutput(new Product());
			assertTrue(operator.isProductOperator());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testGetters() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			assertNotNull(operator.getSBGNOperatorText());
			List<AbstractNode> inputs = new ArrayList<>();
			List<AbstractNode> outputs = new ArrayList<>();
			operator.setInputs(inputs);
			operator.setOutputs(outputs);
			assertEquals(inputs, operator.getInputs());
			assertEquals(outputs, operator.getOutputs());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	@Test
	public void testIsModifierOperator() {
		try {
			NodeOperator operator = new NodeOperatorMock();
			assertFalse(operator.isReactantOperator());

			AndOperator op1 = new AndOperator();
			operator.addInput(op1);
			assertFalse(operator.isModifierOperator());

			op1.addInput(new Modifier());
			assertTrue(operator.isModifierOperator());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

}
