package lcsb.mapviewer.model.map.modifier;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CatalysisTest.class, //
		InhibitionTest.class, //
		ModulationTest.class, //
		PhysicalStimulationTest.class, //
		TriggerTest.class, //
		UnknownCatalysisTest.class,//
		UnknownInhibitionTest.class,//
})
public class AllModifierTests {

}
