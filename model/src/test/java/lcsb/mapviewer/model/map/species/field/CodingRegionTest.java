package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.AntisenseRna;

public class CodingRegionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new CodingRegion());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor1() {
    try {
      CodingRegion antisenseRna = new CodingRegion();
      CodingRegion antisenseRna2 = new CodingRegion(antisenseRna);
      assertNotNull(antisenseRna2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate() {
    try {
      CodingRegion antisenseRna = new CodingRegion();
      antisenseRna.setName("as");
      antisenseRna.setPosition(new Point2D.Double(10, 0));
      CodingRegion antisenseRna2 = new CodingRegion();
      antisenseRna2.update(antisenseRna);
      assertEquals(antisenseRna.getName(), antisenseRna2.getName());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidUpdate() {
    try {
      CodingRegion antisenseRna = new CodingRegion();
      CodingRegion antisenseRna2 = new CodingRegion();
      antisenseRna.setIdModificationResidue("@1");
      antisenseRna2.setIdModificationResidue("@");
      antisenseRna2.update(antisenseRna);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      CodingRegion region = new CodingRegion(new CodingRegion());
      int id = 91;
      AntisenseRna species = new AntisenseRna("id");
      Point2D position = new Point2D.Double(10, 20);
      double width = 5.3;
      String name = "nam";
      String idCodingRegion = "iddd";

      region.setId(id);
      region.setSpecies(species);
      region.setPosition(position);
      region.setWidth(width);
      region.setName(name);
      region.setIdModificationResidue(idCodingRegion);

      assertEquals(id, region.getId());
      assertEquals(species, region.getSpecies());
      assertEquals(position.distance(region.getPosition()), 0, Configuration.EPSILON);
      assertEquals(width, region.getWidth(), Configuration.EPSILON);
      assertEquals(name, region.getName());
      assertEquals(idCodingRegion, region.getIdModificationResidue());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testToString() {
    try {
      assertNotNull(new CodingRegion().toString());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      CodingRegion degraded = new CodingRegion().copy();
      assertNotNull(degraded);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidCopy() {
    try {
      Mockito.mock(CodingRegion.class, Mockito.CALLS_REAL_METHODS).copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
