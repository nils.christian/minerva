package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Rna;

public class ProteinBindingDomainTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new ProteinBindingDomain());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      ProteinBindingDomain region = new ProteinBindingDomain(new ProteinBindingDomain());
      assertNotNull(region);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate() {
    try {
      ProteinBindingDomain region = new ProteinBindingDomain();
      ProteinBindingDomain region2 = new ProteinBindingDomain();
      region2.setName("asd");
      region2.setPosition(new Point2D.Double(0, 10));
      region.update(region2);
      assertEquals(region2.getName(), region.getName());
      assertTrue(region2.getPosition().distance(region.getPosition()) <= Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUpdate2() {
    try {
      ProteinBindingDomain region = new ProteinBindingDomain();
      region.setIdModificationResidue("1");
      ProteinBindingDomain region2 = new ProteinBindingDomain();
      region2.setIdModificationResidue("2");
      region.update(region2);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      ProteinBindingDomain region = new ProteinBindingDomain();
      double size = 2.5;
      int id = 58;
      Rna species = new Rna("id");
      region.setWidth(size);
      region.setSpecies(species);
      region.setId(id);
      assertEquals(id, region.getId());
      assertEquals(size, region.getWidth(), Configuration.EPSILON);
      assertEquals(species, region.getSpecies());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
