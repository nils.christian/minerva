package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class LeftSquareCompartmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new LeftSquareCompartment());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			LeftSquareCompartment compartment = new LeftSquareCompartment(new LeftSquareCompartment());
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor3() {
		try {
			LeftSquareCompartment compartment = new LeftSquareCompartment("id");
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			Model model = new ModelFullIndexed(null);
			model.setWidth(1);
			model.setHeight(1);
			LeftSquareCompartment compartment = new LeftSquareCompartment(new Compartment(), model);
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			LeftSquareCompartment compartment = new LeftSquareCompartment().copy();
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			LeftSquareCompartment compartment = Mockito.spy(LeftSquareCompartment.class);
			compartment.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
