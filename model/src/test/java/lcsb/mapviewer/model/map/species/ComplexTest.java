package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class ComplexTest extends ModelTestFunctions {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Complex());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Complex complex = new Complex();
			complex.addSpecies(new GenericProtein());
			Complex copy = new Complex(complex);
			assertNotNull(copy);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddElement() {
		try {
			Complex complex = new Complex();
			Species species = new GenericProtein("id1");
			complex.addSpecies(species);
			assertEquals(1, complex.getAllChildren().size());
			complex.addSpecies(species);
			assertEquals(1, complex.getAllChildren().size());
			Species protein2 = new GenericProtein("s");
			complex.addSpecies(protein2);
			assertEquals(2, complex.getAllChildren().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAllChildren() {
		try {
			Complex complex = new Complex();

			complex.addSpecies(new GenericProtein("1"));
			Complex complex2 = new Complex("c3");
			complex2.addSpecies(new GenericProtein("2"));
			complex2.addSpecies(new GenericProtein("4"));

			complex.addSpecies(complex2);

			assertEquals(4, complex.getAllChildren().size());
			assertEquals(2, complex.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRemoveElement() {
		try {
			Complex complex = new Complex();

			Species protein = new GenericProtein("1");
			complex.addSpecies(protein);
			complex.addSpecies(new GenericProtein("2"));
			complex.addSpecies(new GenericProtein("4"));

			assertEquals(3, complex.getElements().size());
			complex.removeElement(protein);
			assertEquals(2, complex.getElements().size());
			complex.removeElement(protein);
			assertEquals(2, complex.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRemoveNonExistingElement() {
		try {
			Complex complex = new Complex();
			Complex complex2 = new Complex();

			Species protein = new GenericProtein("1");
			protein.setComplex(complex2);
			complex.removeElement(protein);

			assertEquals(1, super.getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			Complex original = new Complex();
			Complex copy = original.copy();
			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			Complex original = new Complex();
			assertNotNull(original.getStringType());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAllSimpleChildren() {
		try {
			Complex child = new Complex();
			child.addSpecies(new GenericProtein("id2"));
			Complex original = new Complex();
			original.addSpecies(child);
			original.addSpecies(new Complex());
			original.addSpecies(new GenericProtein("id"));
			assertEquals(2, original.getAllSimpleChildren().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			Complex object = Mockito.spy(Complex.class);
			object.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
