package lcsb.mapviewer.model.map.reaction;

import lcsb.mapviewer.model.map.reaction.type.AllReactionTypeTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AbstractNodeComparatorTest.class, //
		AllReactionTypeTests.class,//
		AndOperatorTest.class, //
		AssociationOperatorTest.class, //
		DissociationOperatorTest.class, //
		ModifierTest.class,//
		NandOperatorTest.class, //
		NodeOperatorComparatorTest.class, //
		NodeOperatorTest.class, //
		OrOperatorTest.class, //
		ProductTest.class, //
		ReactantTest.class,//
		ReactionComparatorTest.class, //
		ReactionNodeComparatorTest.class, //
		ReactionTest.class, //
		SplitOperatorTest.class,//
		TruncationOperatorTest.class, //
		UnknownOperatorTest.class,//
})
public class AllReactionTests {

}
