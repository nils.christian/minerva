package lcsb.mapviewer.model.map;

import static org.junit.Assert.*;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OverviewModelLinkComparatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEqual() throws Exception {
		try {
			OverviewModelLinkComparator oic = new OverviewModelLinkComparator();

			OverviewModelLink oi = new OverviewModelLink();
			OverviewModelLink oi2 = new OverviewModelLink();

			assertEquals(0, oic.compare(oi, oi2));
			assertEquals(0, oic.compare(null, null));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDifferent() throws Exception {
		try {
			OverviewModelLinkComparator oic = new OverviewModelLinkComparator();

			OverviewModelLink oi = new OverviewModelLink();

			OverviewModelLink oi2 = new OverviewModelLink();
			oi2.setLinkedModel(new ModelFullIndexed(null));

			assertTrue(0 != oic.compare(oi, oi2));
			assertTrue(0 != oic.compare(oi2, oi));
			assertTrue(0 != oic.compare(oi, null));
			assertTrue(0 != oic.compare(null, oi2));

			Model model = new ModelFullIndexed(null);
			model.setId(1);
			oi.setLinkedModel(model);
			assertTrue(0 != oic.compare(oi, oi2));

			oi2 = new OverviewModelLink();
			oi2.setPolygon("AS");

			assertTrue(0 != oic.compare(oi, oi2));

			oi2 = new OverviewModelLink();
			oi2.setxCoord(12);

			assertTrue(0 != oic.compare(oi, oi2));

			oi2 = new OverviewModelLink();
			oi2.setyCoord(13);

			assertTrue(0 != oic.compare(oi, oi2));

			oi2 = new OverviewModelLink();
			oi2.setZoomLevel(345);

			assertTrue(0 != oic.compare(oi, oi2));

			assertTrue(0 != oic.compare(oi, new OverviewModelLink() {
				private static final long serialVersionUID = 1L;
			}));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	  @Test
	  public void testDifferentPolygon() throws Exception {
	    try {
	      OverviewModelLinkComparator comparator = new OverviewModelLinkComparator();

	      OverviewModelLink iml1 = new OverviewModelLink();
	      OverviewModelLink iml2 = new OverviewModelLink();
	      iml2.setPolygon("1,2");

	      assertTrue(0 != comparator.compare(iml1, iml2));
	      assertTrue(0 != comparator.compare(iml2, iml1));

	    } catch (Exception e) {
	      e.printStackTrace();
	      throw e;
	    }
	  }

}
