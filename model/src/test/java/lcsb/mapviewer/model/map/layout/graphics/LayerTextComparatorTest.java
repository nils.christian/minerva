package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerTextComparatorTest {
	LayerTextComparator comparator = new LayerTextComparator();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals() {
		try {
			LayerText layer1 = getLayerText();
			LayerText layer2 = getLayerText();

			assertEquals(0, comparator.compare(new LayerText(), new LayerText()));
			assertEquals(0, comparator.compare(layer1, layer2));
			assertEquals(0, comparator.compare(layer1, layer1));
			assertEquals(0, comparator.compare(null, null));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testDifferent2() {
		try {
			LayerText layer1 = getLayerText();

			assertTrue(comparator.compare(layer1, new LayerText() {
				private static final long serialVersionUID = 1L;
			}) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	@Test
	public void testInvalid() {
		try {
			class Tmp extends LayerText {
				private static final long serialVersionUID = 1L;
			}
			Tmp layer1 = new Tmp();
			Tmp layer2 = new Tmp();

			comparator.compare(layer1, layer2);

			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}

	private LayerText getLayerText() {

		LayerText line = new LayerText();
		line.setColor(Color.YELLOW);
		line.setNotes(":asd");
		line.setFontSize(4.3);

		return line;
	}

	@Test
	public void testDifferent() {
		try {
			LayerText layer1 = getLayerText();
			LayerText layer2 = getLayerText();

			assertTrue(comparator.compare(layer1, new LayerText()) != 0);

			assertTrue(comparator.compare(layer1, null) != 0);
			assertTrue(comparator.compare(null, layer1) != 0);

			layer1 = getLayerText();
			layer2 = getLayerText();

			layer1.setColor(Color.GREEN);

			assertTrue(comparator.compare(layer1, layer2) != 0);
			assertTrue(comparator.compare(layer2, layer1) != 0);

			layer1 = getLayerText();
			layer2 = getLayerText();

			layer1.setNotes("asd");

			assertTrue(comparator.compare(layer1, layer2) != 0);
			assertTrue(comparator.compare(layer2, layer1) != 0);

			layer1 = getLayerText();
			layer2 = getLayerText();

			layer1.setHeight(1.2);

			assertTrue(comparator.compare(layer1, layer2) != 0);
			assertTrue(comparator.compare(layer2, layer1) != 0);

			layer1 = getLayerText();
			layer2 = getLayerText();

			layer1.setX(33.4);

			assertTrue(comparator.compare(layer1, layer2) != 0);
			assertTrue(comparator.compare(layer2, layer1) != 0);

			layer1 = getLayerText();
			layer2 = getLayerText();

			layer1.setY(33.4);

			assertTrue(comparator.compare(layer1, layer2) != 0);
			assertTrue(comparator.compare(layer2, layer1) != 0);

			layer1 = getLayerText();
			layer2 = getLayerText();

			layer1.setFontSize(64.1);

			assertTrue(comparator.compare(layer1, layer2) != 0);
			assertTrue(comparator.compare(layer2, layer1) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknown exception");
		}
	}
}
