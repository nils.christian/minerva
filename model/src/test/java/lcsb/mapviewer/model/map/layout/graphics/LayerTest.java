package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.ModelData;

public class LayerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Layer());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Layer layer = new Layer();
			layer.addLayerText(new LayerText());
			layer.addLayerLine(new PolylineData());
			layer.addLayerRect(new LayerRect());
			layer.addLayerOval(new LayerOval());
			Layer copy = new Layer(layer);

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			Layer degraded = new Layer().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetters() {
		try {
			Layer layer = new Layer().copy();

			ModelData model = new ModelData();
			List<LayerOval> ovals = new ArrayList<>();
			List<LayerRect> rectangles = new ArrayList<>();
			List<PolylineData> lines = new ArrayList<>();
			List<LayerText> texts= new ArrayList<>();

			boolean locked = true;
			boolean visible = true;
			String name = "sdtr";
			String layerId = "id";

			layer.setModel(model);
			layer.setOvals(ovals);
			layer.setTexts(texts);
			layer.setRectangles(rectangles);
			layer.setLines(lines);
			layer.setLocked(locked);
			layer.setVisible(visible);
			layer.setName(name);
			layer.setLayerId(layerId);

			assertEquals(model, layer.getModel());
			assertEquals(ovals, layer.getOvals());
			assertEquals(rectangles, layer.getRectangles());
			assertEquals(lines, layer.getLines());
			assertEquals(texts, layer.getTexts());
			assertEquals(locked, layer.isLocked());
			assertEquals(name, layer.getName());
			assertEquals(visible, layer.isVisible());
			assertEquals(layerId, layer.getLayerId());

			layer.setLocked("FALSE");
			layer.setVisible("FALSE");

			assertFalse(layer.isLocked());
			assertFalse(layer.isVisible());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new Layer() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRemoveLayerText() {
		try {
			Layer layer = new Layer();
			layer.removeLayerText(new LayerText());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddLines() {
		try {
			Layer layer = new Layer();
			List<PolylineData> lines = new ArrayList<>();
			lines.add(new PolylineData());
			layer.addLayerLines(lines);
			assertEquals(1, layer.getLines().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
