package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class ElementSubmodelConnectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new ElementSubmodelConnection());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			ElementSubmodelConnection connection = new ElementSubmodelConnection(new ModelData(), SubmodelType.DOWNSTREAM_TARGETS);
			assertNotNull(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			ElementSubmodelConnection connection = new ElementSubmodelConnection(new ElementSubmodelConnection(new ModelData(), SubmodelType.DOWNSTREAM_TARGETS));
			assertNotNull(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor3() {
		try {
			ElementSubmodelConnection connection = new ElementSubmodelConnection(new ModelFullIndexed(null), SubmodelType.DOWNSTREAM_TARGETS, "sd");
			assertNotNull(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			ElementSubmodelConnection connection = new ElementSubmodelConnection().copy();
			assertNotNull(connection);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new ElementSubmodelConnection() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
