package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;

public class ProteinTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new GenericProtein());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor1() {
    try {
      GenericProtein protein = new GenericProtein();
      protein.setStructuralState("srt");
      List<ModificationResidue> residues = new ArrayList<>();
      residues.add(new Residue());

      protein.setModificationResidues(residues);
      Protein protein2 = new GenericProtein(protein);
      assertNotNull(protein2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSetStructuralState() {
    try {
      GenericProtein protein = new GenericProtein();
      protein.setStructuralState("str");
      protein.setStructuralState("str1");

      assertEquals("str1", protein.getStructuralState());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      GenericProtein protein = new GenericProtein("id");
      assertNotNull(protein.getStringType());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      GenericProtein protein = new GenericProtein().copy();
      assertNotNull(protein);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidCopy() {
    try {
      GenericProtein mock = Mockito.spy(GenericProtein.class);
      mock.copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
