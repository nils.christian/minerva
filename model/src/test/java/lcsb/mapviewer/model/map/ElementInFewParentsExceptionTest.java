package lcsb.mapviewer.model.map;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElementInFewParentsExceptionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new ElementInFewParentsException(""));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
