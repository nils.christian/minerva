package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class TopSquareCompartmentTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new TopSquareCompartment());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	

	@Test
	public void testConstructor1() {
		try {
			TopSquareCompartment compartment = new TopSquareCompartment(new TopSquareCompartment());
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor3() {
		try {
			TopSquareCompartment compartment = new TopSquareCompartment("id");
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			Model model = new ModelFullIndexed(null);
			model.setWidth(1);
			model.setHeight(1);
			TopSquareCompartment compartment = new TopSquareCompartment(new Compartment(), model);
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			TopSquareCompartment compartment = new TopSquareCompartment().copy();
			assertNotNull(compartment);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			TopSquareCompartment compartment = Mockito.spy(TopSquareCompartment.class);
			compartment.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	
}
