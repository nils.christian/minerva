package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;

public class ProteinComparatorTest {
  Logger logger=  Logger.getLogger(ProteinComparatorTest.class);

  ProteinComparator comparator = new ProteinComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    try {
      GenericProtein protein1 = createProtein();
      GenericProtein protein2 = createProtein();

      assertEquals(0, comparator.compare(protein1, protein1));

      assertEquals(0, comparator.compare(protein1, protein2));
      assertEquals(0, comparator.compare(protein2, protein1));

      assertEquals(0, comparator.compare(null, null));

      assertEquals(0, comparator.compare(new TruncatedProtein(), new TruncatedProtein()));
      assertEquals(0, comparator.compare(new ReceptorProtein(), new ReceptorProtein()));
      assertEquals(0, comparator.compare(new IonChannelProtein(), new IonChannelProtein()));

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testUnknownProteinImplementations() {
    try {
      Protein protein1 = Mockito.mock(Protein.class);
      assertEquals(0, comparator.compare(protein1, protein1));

      fail("Exception expected");

    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testDifferent() {
    try {
      GenericProtein protein1 = createProtein();
      GenericProtein protein2 = createProtein();
      assertTrue(comparator.compare(null, protein2) != 0);
      assertTrue(comparator.compare(protein2, null) != 0);

      protein1.getModificationResidues().get(0).setName("bla");
      assertTrue(comparator.compare(protein1, protein2) != 0);
      assertTrue(comparator.compare(protein2, protein1) != 0);

      protein1 = createProtein();
      protein2 = createProtein();
      protein1.getModificationResidues().clear();
      assertTrue(comparator.compare(protein1, protein2) != 0);
      assertTrue(comparator.compare(protein2, protein1) != 0);

      protein1 = createProtein();
      protein2 = createProtein();
      assertTrue(comparator.compare(null, protein2) != 0);
      assertTrue(comparator.compare(protein2, null) != 0);
      assertTrue(comparator.compare(null, null) == 0);

      assertTrue(comparator.compare(protein2, new GenericProtein()) != 0);

      protein1 = createProtein();
      protein2 = createProtein();
      protein1.setName("a");
      assertTrue(comparator.compare(protein1, protein2) != 0);
      assertTrue(comparator.compare(protein2, protein1) != 0);

      protein1 = createProtein();
      protein2 = createProtein();
      protein1.setStructuralState("a");
      assertTrue(comparator.compare(protein1, protein2) != 0);
      assertTrue(comparator.compare(protein2, protein1) != 0);

      protein1 = createProtein();
      protein2 = createProtein();
      protein1.setHomodimer(1);
      assertTrue(comparator.compare(protein1, protein2) != 0);
      assertTrue(comparator.compare(protein2, protein1) != 0);

      assertTrue(comparator.compare(new GenericProtein(), new TruncatedProtein()) != 0);

    } catch (Exception e) {
      e.printStackTrace();
      fail("Unknowne exception occurred");
    }
  }

  public GenericProtein createProtein() {
    GenericProtein result = new GenericProtein();
    result.setHomodimer(12);
    result.setStructuralState("id1");
    result.setHypothetical(true);

    Residue residue = new Residue();
    result.addModificationResidue(residue);

    residue.setIdModificationResidue("a");
    residue.setName("name");
    residue.setPosition(new Point2D.Double(10, 20));
    residue.setState(ModificationState.DONT_CARE);
    return result;
  }

}
