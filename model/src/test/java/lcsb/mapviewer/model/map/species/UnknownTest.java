package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class UnknownTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Unknown());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			Unknown degraded = new Unknown(new Unknown());
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			Unknown degraded = new Unknown("id");
			assertNotNull(degraded.getStringType());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			Unknown degraded = new Unknown().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			Unknown object = Mockito.spy(Unknown.class);
			object.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
