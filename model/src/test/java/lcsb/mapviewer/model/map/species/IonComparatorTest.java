package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class IonComparatorTest {

	IonComparator comparator = new IonComparator();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals() {
		try {
			Ion ion1 = createIon();
			Ion ion2 = createIon();

			assertEquals(0, comparator.compare(ion1, ion1));

			assertEquals(0, comparator.compare(ion1, ion2));
			assertEquals(0, comparator.compare(ion2, ion1));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Ione exception occurred");
		}
	}

	@Test
	public void testDifferent() {
		try {
			Ion ion2 = createIon();
			assertTrue(comparator.compare(null, ion2) != 0);
			assertTrue(comparator.compare(ion2, null) != 0);
			assertTrue(comparator.compare(null, null) == 0);

			Ion ion = createIon();
			ion.setName("n");
			assertTrue(comparator.compare(ion, ion2) != 0);

			assertTrue(comparator.compare(ion, Mockito.mock(Ion.class)) != 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Ion createIon() {
		Ion result = new Ion();
		return result;
	}

	@Test
	public void testInvalid() {
		try {
			Ion object = Mockito.mock(Ion.class);

			comparator.compare(object, object);

			fail("Exception expected");
		} catch (NotImplementedException e) {

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknowne exception occurred");
		}
	}

}
