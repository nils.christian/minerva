package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ModelDataTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new ModelData());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddElements() {
    try {
      ModelData md = new ModelData();
      List<Element> elements = new ArrayList<>();
      elements.add(new GenericProtein("unk_id"));
      md.addElements(elements);
      assertEquals(1, md.getElements().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddReactions() {
    try {
      ModelData md = new ModelData();
      List<Reaction> reactions = new ArrayList<>();
      reactions.add(new Reaction());
      md.addReactions(reactions);
      assertEquals(1, md.getReactions().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddLayers() {
    try {
      ModelData md = new ModelData();
      List<Layer> layers = new ArrayList<>();
      layers.add(new Layer());
      md.addLayers(layers);
      assertEquals(1, md.getLayers().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddElementGroup() {
    try {
      ModelData md = new ModelData();
      md.addElementGroup(null);
      // not implemented for now
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddBlockDiagram() {
    try {
      ModelData md = new ModelData();
      md.addBlockDiagream(new BlockDiagram() {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
      });
      // not implemented for now
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddRemoveLayout() {
    try {
      ModelData md = new ModelData();
      Layout l = new Layout();
      l.setId(3);
      md.addLayout(l);
      assertEquals(1, md.getLayouts().size());
      md.removeLayout(new Layout());
      assertEquals(1, md.getLayouts().size());
      md.removeLayout(l);
      assertEquals(0, md.getLayouts().size());
      md.addLayout(0, l);
      assertEquals(1, md.getLayouts().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveElement1() {
    try {
      ModelData md = new ModelData();
      md.removeElement(null);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveElement2() {
    try {
      ModelData md = new ModelData();
      md.removeElement(new GenericProtein("unk_id"));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveElement3() {
    try {
      ModelData md = new ModelData();
      Element protein = new GenericProtein("unk_id");
      md.addElement(protein);
      assertEquals(1, md.getElements().size());
      md.removeElement(protein);
      assertEquals(0, md.getElements().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveReaction2() {
    try {
      ModelData md = new ModelData();
      md.removeReaction(new Reaction());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveReaction3() {
    try {
      ModelData md = new ModelData();
      Reaction reaction = new Reaction();
      md.addReaction(reaction);
      assertEquals(1, md.getReactions().size());
      md.removeReaction(reaction);
      assertEquals(0, md.getReactions().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddSubmodel() {
    try {
      ModelData md = new ModelData();
      md.addSubmodel(new ModelSubmodelConnection());
      assertEquals(1, md.getSubmodels().size());
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      ModelData md = new ModelData();
      Set<Element> elements = new HashSet<>();
      List<Layout> layouts = new ArrayList<>();
      Set<Layer> layers = new HashSet<>();
      Set<Reaction> reactions = new HashSet<>();
      Set<SubmodelConnection> parents = new HashSet<>();
      List<OverviewImage> overviewImages = new ArrayList<>();
      overviewImages.add(new OverviewImage());
      Integer id = 95;
      int width = 2;
      Double widthDouble = 2.0;
      int height = 29;
      int zoomLevels = 3;
      int tileSize = 512;
      String modelId = "modId";
      String notes = "nOT";
      String name = "n_ma";
      Double heightDouble = 29.0;
      Project project = new Project();
      Calendar creationDate = Calendar.getInstance();

      Model model = new ModelFullIndexed(null);

      md.setId(id);
      assertEquals(id, md.getId());

      md.setElements(elements);
      assertEquals(elements, md.getElements());

      md.setProject(project);
      assertEquals(project, md.getProject());

      md.setCreationDate(creationDate);
      assertEquals(creationDate, md.getCreationDate());

      md.setLayouts(layouts);
      assertEquals(layouts, md.getLayouts());

      md.setWidth(width);
      assertEquals(widthDouble, md.getWidth(), Configuration.EPSILON);

      md.setHeight(height);
      assertEquals(heightDouble, md.getHeight(), Configuration.EPSILON);

      md.setZoomLevels(zoomLevels);
      assertEquals(zoomLevels, md.getZoomLevels());

      md.setTileSize(tileSize);
      assertEquals(tileSize, md.getTileSize());

      md.setIdModel(modelId);
      assertEquals(modelId, md.getIdModel());

      md.setNotes(notes);
      assertEquals(notes, md.getNotes());

      md.setName(name);
      assertEquals(name, md.getName());

      md.setModel(null);
      assertNull(md.getModel());
      md.setModel(model);
      assertEquals(model, md.getModel());

      md.setLayers(layers);
      assertEquals(layers, md.getLayers());

      md.setReactions(reactions);
      assertEquals(reactions, md.getReactions());

      md.setParentModels(parents);
      assertEquals(parents, md.getParentModels());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
