package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class PositiveInfluenceReactionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new PositiveInfluenceReaction());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testConstructorWithInvalidArg() {
		try {
			Reaction reaction = new Reaction();
			new PositiveInfluenceReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructorWithInvalidArg2() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			new PositiveInfluenceReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			reaction.addReactant(new Reactant());
			PositiveInfluenceReaction validReaction = new PositiveInfluenceReaction(reaction);
			assertNotNull(validReaction);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			PositiveInfluenceReaction original = new PositiveInfluenceReaction();
			original.addProduct(new Product());
			original.addReactant(new Reactant());
			original.addReactant(new Reactant());
			PositiveInfluenceReaction product = original.copy();
			assertNotNull(product);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new PositiveInfluenceReaction() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			PositiveInfluenceReaction original = new PositiveInfluenceReaction();
			assertNull(original.getReactionRect());
			assertNotNull(original.getStringType());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
