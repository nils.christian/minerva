package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class GenericColorSchemaTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new GenericColorSchema());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			GenericColorSchema schema = new GenericColorSchema();
			schema.setDescription("desc");
			GenericColorSchema copy = new GenericColorSchema(schema);
			assertEquals("desc", copy.getDescription());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			GenericColorSchema schema = new GenericColorSchema();
			schema.setDescription("desc");
			GenericColorSchema copy = schema.copy();

			assertEquals("desc", copy.getDescription());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			GenericColorSchema schema = new GenericColorSchema() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			};
			schema.copy();
			fail("Exception expected");

		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
