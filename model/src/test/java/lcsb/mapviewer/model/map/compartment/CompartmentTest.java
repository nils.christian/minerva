package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

public class CompartmentTest {
	Logger logger						 = Logger.getLogger(CompartmentTest.class);

	int		 identifierCounter = 1;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCross() throws Exception {
		try {
			Model model = createCrossModel();
			Element ldhb1 = model.getElementByElementId("sa1115");
			Element ldhb2 = model.getElementByElementId("sa1117");
			Element ldhb3 = model.getElementByElementId("sa1119");
			Element ldhb4 = model.getElementByElementId("sa1121");

			Compartment cytosol = (Compartment) model.getElementByElementId("ca1");

			assertTrue(cytosol.cross(ldhb1));
			assertTrue(cytosol.cross(ldhb2));
			assertTrue(cytosol.cross(ldhb3));
			assertTrue(cytosol.cross(ldhb4));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCrossException() throws Exception {
		try {
			Compartment compartment = new Compartment();
			Complex complex = new Complex("idd");
			complex.setComplex(complex);
			compartment.cross(complex);

			fail("Exception epxected");
		} catch (InvalidStateException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCross2() throws Exception {
		try {
			Compartment compartment = new Compartment();
			compartment.setX(10.0);
			compartment.setY(10.0);
			compartment.setWidth(10.0);
			compartment.setHeight(10.0);
			Complex complex = new Complex("idd");
			complex.setWidth(10.0);
			complex.setHeight(10.0);
			complex.setX(5.0);
			complex.setY(5.0);
			assertTrue(compartment.cross(complex));

			complex.setX(15.0);
			complex.setY(5.0);
			assertTrue(compartment.cross(complex));

			complex.setX(5.0);
			complex.setY(15.0);
			assertTrue(compartment.cross(complex));

			complex.setX(15.0);
			complex.setY(15.0);
			assertTrue(compartment.cross(complex));

			complex.setWidth(100.0);
			complex.setHeight(100.0);
			complex.setX(-110.0);
			complex.setY(-10.0);
			assertFalse(compartment.cross(complex));

			complex.setX(-10.0);
			complex.setY(-110.0);
			assertFalse(compartment.cross(complex));

			complex.setX(40.0);
			complex.setY(-10.0);
			assertFalse(compartment.cross(complex));

			complex.setX(-10.0);
			complex.setY(40.0);
			assertFalse(compartment.cross(complex));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testContainsIdenticalSpecies() throws Exception {
		try {
			Compartment compartment = new Compartment();

			compartment.setName("AS");

			assertFalse(compartment.containsIdenticalSpecies());

			Species species = new GenericProtein("idd");
			species.setName("AS");

			compartment.addElement(species);

			assertTrue(compartment.containsIdenticalSpecies());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDoesntContainsIdenticalSpecies() throws Exception {
		try {
			Compartment compartment = new Compartment();

			compartment.setName("AS");

			Species species = new GenericProtein("idd");
			species.setName("not identical name");

			compartment.addElement(species);

			assertFalse(compartment.containsIdenticalSpecies());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private Model createCrossModel() {
		Model model = new ModelFullIndexed(null);
		model.addElement(new Compartment("default"));

		model.addElement(createSpecies(3264.8333333333335, 7517.75, 86.0, 46.0, "sa1115"));
		model.addElement(createSpecies(3267.6666666666665, 7438.75, 80.0, 40.0, "sa1117"));
		model.addElement(createSpecies(3261.6666666666665, 7600.75, 92.0, 52.0, "sa1119"));
		model.addElement(createSpecies(3203.666666666667, 7687.75, 98.0, 58.0, "sa1121"));

		Species species = createSpecies(7817.714285714286, 11287.642857142859, 80.0, 40.0, "sa1422");
		Species species2 = createSpecies(8224.964285714286, 11241.392857142859, 80.0, 40.0, "sa1419");
		Complex complex = createComplex(7804.714285714286, 11182.642857142859, 112.0, 172.0, "csa152");
		complex.addSpecies(species);
		complex.addSpecies(species2);
		species.setComplex(complex);
		species2.setComplex(complex);

		model.addElement(species);
		model.addElement(species2);
		model.addElement(complex);

		model.addElement(createCompartment(1380.0, 416.0, 15893.0, 10866.0, "ca1"));
		return model;
	}

	private Compartment createCompartment(double x, double y, double width, double height, String elementId) {
		Compartment compartment = new Compartment(elementId);
		compartment.setElementId(elementId);
		compartment.setX(x);
		compartment.setY(y);
		compartment.setWidth(width);
		compartment.setHeight(height);
		return compartment;
	}

	private Species createSpecies(double x, double y, double width, double height, String elementId) {
		SimpleMolecule simpleMolecule = new SimpleMolecule(elementId);
		simpleMolecule.setX(x);
		simpleMolecule.setY(y);
		simpleMolecule.setWidth(width);
		simpleMolecule.setHeight(height);
		return simpleMolecule;
	}

	private Complex createComplex(double x, double y, double width, double height, String elementId) {
		Complex complex = new Complex(elementId);
		complex.setX(x);
		complex.setY(y);
		complex.setWidth(width);
		complex.setHeight(height);
		return complex;
	}

	@Test
	public void testCrossWithComplexElements() throws Exception {
		try {
			Model model = createCrossModel();
			Element ldhb1 = model.getElementByElementId("sa1422");
			Element ldhb2 = model.getElementByElementId("sa1419");

			Compartment cytosol = (Compartment) model.getElementByElementId("ca1");

			assertTrue(cytosol.cross(ldhb1));
			assertTrue(cytosol.cross(ldhb2));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Compartment());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Compartment compartment = new Compartment();
			compartment.addElement(new GenericProtein("idd"));
			Compartment copy = new Compartment(compartment);
			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			Compartment compartment = new Compartment();
			String invalidVal = "a1.6";
			String val = "1.6";
			Double dVal = 1.6;

			compartment.setLineThickness(val);
			compartment.setLineOuterWidth(val);
			compartment.setLineInnerWidth(val);

			assertEquals(dVal, compartment.getThickness(), Configuration.EPSILON);
			assertEquals(dVal, compartment.getOuterWidth(), Configuration.EPSILON);
			assertEquals(dVal, compartment.getInnerWidth(), Configuration.EPSILON);

			try {
				compartment.setLineThickness(invalidVal);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			try {
				compartment.setLineOuterWidth(invalidVal);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			try {
				compartment.setLineInnerWidth(invalidVal);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}

			compartment.setNamePoint(2, 3);
			assertEquals(0, compartment.getNamePoint().distance(2, 3), Configuration.EPSILON);

			Set<Element> elements = new HashSet<>();

			compartment.setElements(elements);
			assertEquals(elements, compartment.getElements());

			assertNotNull(compartment.getStringType());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddElement() {
		try {
			Compartment compartment = new Compartment();

			Species child = new GenericProtein("id");

			compartment.addElement(child);
			compartment.addElement(child);

			assertEquals(1, compartment.getElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddInvalidElement() {
		try {
			Compartment compartment = new Compartment();

			Species child = new GenericProtein("id");
			Species child2 = new GenericProtein("id");

			compartment.addElement(child);
			compartment.addElement(child2);

			fail("Exception expected");

		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetAllElements() {
		try {
			Compartment compartment = new Compartment();

			Species child = new GenericProtein("id");
			Compartment child2 = new Compartment();
			child2.addElement(child);
			compartment.addElement(child2);

			assertEquals(2, compartment.getAllSubElements().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			Compartment degraded = new Compartment().copy();
			assertNotNull(degraded);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testRemoveElement() {
		try {
			Compartment comp = new Compartment();
			Species protein = new GenericProtein("idd");
			comp.addElement(protein);
			protein.setCompartment(new Compartment());

			comp.removeElement(protein);

			assertEquals(0, comp.getElements().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			Compartment compartment = Mockito.spy(Compartment.class);
			compartment.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
