package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OverviewLinkTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetPolygonCoord() {
		try {
			OverviewLink link = new OverviewLink() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public OverviewLink copy() {
					// TODO Auto-generated method stub
					return null;
				}
			};
			String polygon = "1,1 2,2 4,0";
			link.setPolygon(polygon);
			assertEquals(3, link.getPolygonCoordinates().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			OverviewLink link = new OverviewLink() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public OverviewLink copy() {
					// TODO Auto-generated method stub
					return null;
				}
			};
			OverviewImage image= new OverviewImage();
			int id = 309;
			link.setOverviewImage(image);
			link.setId(id);
			
			assertEquals(image, link.getOverviewImage());
			assertEquals(id, link.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
