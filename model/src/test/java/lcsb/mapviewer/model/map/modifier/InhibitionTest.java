package lcsb.mapviewer.model.map.modifier;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class InhibitionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new Inhibition());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      Inhibition modifier = new Inhibition(new GenericProtein("unk_id"));
      assertNotNull(modifier);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor2() {
    try {
      Inhibition modifier = new Inhibition(new GenericProtein("unk_id"));
      modifier.setLine(new PolylineData());
      Inhibition modifier2 = new Inhibition(modifier);
      assertNotNull(modifier2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      Inhibition modifier = new Inhibition(new GenericProtein("unk_id"));
      modifier.setLine(new PolylineData());
      Inhibition modifier2 = modifier.copy();
      assertNotNull(modifier2);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyInvalid() {
    try {
      Mockito.mock(Inhibition.class, Mockito.CALLS_REAL_METHODS).copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
