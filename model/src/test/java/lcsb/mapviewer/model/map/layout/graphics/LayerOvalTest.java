package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.awt.Color;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerOvalTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new LayerOval());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			LayerOval layerOval = new LayerOval();

			LayerOval copy = new LayerOval(layerOval);

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			LayerOval copy = new LayerOval().copy();

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new LayerOval() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();

			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			LayerOval oval = new LayerOval();

			String yParam = "1.2";
			Double y = 1.2;

			String xParam = "2.2";
			Double x = 2.2;

			String widthParam = "10.2";
			Double width = 10.2;

			String heightParam = "72.2";
			Double height = 72.2;

			String invalidNumberStr = "xxd";

			int id = 52;
			Color color = Color.BLACK;

			oval.setY(yParam);
			assertEquals(y, oval.getY(), Configuration.EPSILON);
			try {
				oval.setY(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			oval.setY((Double) null);
			assertNull(oval.getY());
			oval.setY(y);
			assertEquals(y, oval.getY(), Configuration.EPSILON);

			oval.setX(xParam);
			assertEquals(x, oval.getX(), Configuration.EPSILON);
			try {
				oval.setX(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			oval.setX((Double) null);
			assertNull(oval.getX());
			oval.setX(x);
			assertEquals(x, oval.getX(), Configuration.EPSILON);

			oval.setWidth(widthParam);
			assertEquals(width, oval.getWidth(), Configuration.EPSILON);
			try {
				oval.setWidth(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			oval.setWidth((Double) null);
			assertNull(oval.getWidth());
			oval.setWidth(width);
			assertEquals(width, oval.getWidth(), Configuration.EPSILON);

			oval.setHeight(heightParam);
			assertEquals(height, oval.getHeight(), Configuration.EPSILON);
			try {
				oval.setHeight(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			oval.setHeight((Double) null);
			assertNull(oval.getHeight());
			oval.setHeight(height);
			assertEquals(height, oval.getHeight(), Configuration.EPSILON);

			oval.setId(id);
			assertEquals(id, oval.getId());
			oval.setColor(color);
			assertEquals(color, oval.getColor());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
