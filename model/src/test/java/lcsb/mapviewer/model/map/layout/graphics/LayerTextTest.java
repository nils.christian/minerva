package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.awt.geom.Rectangle2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerTextTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new LayerText());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			LayerText layerText = new LayerText();

			LayerText copy = new LayerText(layerText);

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {

			LayerText copy = new LayerText(new Rectangle2D.Double(), "text");

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			LayerText copy = new LayerText().copy();

			assertNotNull(copy);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new LayerText() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();

			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetBorder() {
		try {
			LayerText copy = new LayerText().copy();
			assertNotNull(copy.getBorder());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			LayerText text = new LayerText();

			String yParam = "1.2";
			Double y = 1.2;

			String xParam = "2.2";
			Double x = 2.2;

			String widthParam = "10.2";
			Double width = 10.2;

			String heightParam = "72.2";
			Double height = 72.2;

			String fontSizeParam = "5.0";
			Double fontSize = 5.0;

			String invalidNumberStr = "xxd";

			text.setY(yParam);
			assertEquals(y, text.getY(), Configuration.EPSILON);
			try {
				text.setY(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			assertEquals(y, text.getY(), Configuration.EPSILON);
			text.setY((Double) null);
			assertNull(text.getY());
			text.setY(y);
			assertEquals(y, text.getY(), Configuration.EPSILON);

			text.setX(xParam);
			assertEquals(x, text.getX(), Configuration.EPSILON);
			try {
				text.setX(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			text.setX((Double) null);
			assertNull(text.getX());
			text.setX(x);
			assertEquals(x, text.getX(), Configuration.EPSILON);

			text.setFontSize(fontSizeParam);
			assertEquals(fontSize, text.getFontSize(), Configuration.EPSILON);
			try {
				text.setFontSize(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			text.setFontSize((Double) null);
			assertNull(text.getFontSize());
			text.setFontSize(fontSize);
			assertEquals(fontSize, text.getFontSize(), Configuration.EPSILON);

			text.setWidth(widthParam);
			assertEquals(width, text.getWidth(), Configuration.EPSILON);
			try {
				text.setWidth(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			text.setWidth((Double) null);
			assertNull(text.getWidth());
			text.setWidth(width);
			assertEquals(width, text.getWidth(), Configuration.EPSILON);

			text.setHeight(heightParam);
			assertEquals(height, text.getHeight(), Configuration.EPSILON);
			try {
				text.setHeight(invalidNumberStr);
				fail("Exception expected");
			} catch (InvalidArgumentException e) {
			}
			text.setHeight((Double) null);
			assertNull(text.getHeight());
			text.setHeight(height);
			assertEquals(height, text.getHeight(), Configuration.EPSILON);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
