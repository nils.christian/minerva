package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class StateTransitionReactionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new StateTransitionReaction());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void testConstructorWithInvalidArg() {
		try {
			Reaction reaction = new Reaction();
			new StateTransitionReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructorWithInvalidArg2() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			new StateTransitionReaction(reaction);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			Reaction reaction = new Reaction();
			reaction.addProduct(new Product());
			reaction.addReactant(new Reactant());
			StateTransitionReaction validReaction = new StateTransitionReaction(reaction);
			assertNotNull(validReaction);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCopy() {
		try {
			StateTransitionReaction original = new StateTransitionReaction();
			original.addProduct(new Product());
			original.addReactant(new Reactant());
			original.addReactant(new Reactant());
			StateTransitionReaction product = original.copy();
			assertNotNull(product);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() {
		try {
			new StateTransitionReaction() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			}.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	@Test
	public void testGetters() {
		try {
			StateTransitionReaction original = new StateTransitionReaction();
			assertNotNull(original.getReactionRect());
			assertNotNull(original.getStringType());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
