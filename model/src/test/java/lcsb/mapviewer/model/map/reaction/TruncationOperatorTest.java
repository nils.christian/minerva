package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class TruncationOperatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new TruncationOperator());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      TruncationOperator op = new TruncationOperator();
      op.setLine(new PolylineData());
      TruncationOperator operator = new TruncationOperator(op);
      assertNotNull(operator);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      assertNotNull(new TruncationOperator().getSBGNOperatorText());
      assertNotNull(new TruncationOperator().getOperatorText());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy1() {
    try {
      TruncationOperator op = new TruncationOperator();
      op.setLine(new PolylineData());
      TruncationOperator operator = op.copy();
      assertNotNull(operator);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy2() {
    try {
      Mockito.mock(TruncationOperator.class, Mockito.CALLS_REAL_METHODS).copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
