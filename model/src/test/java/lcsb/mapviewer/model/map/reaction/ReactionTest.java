package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlKineticsComparator;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionTest extends ModelTestFunctions {

  Logger logger = Logger.getLogger(ReactantTest.class);

  private Species species;

  @Before
  public void setUp() throws Exception {
    species = new GenericProtein("sa1");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testContainsElement() {
    try {
      Reaction reaction = new Reaction();
      assertFalse(reaction.containsElement(species));
      Modifier modifier = new Catalysis(species);
      reaction.addModifier(modifier);
      assertTrue(reaction.containsElement(species));
      assertFalse(reaction.containsElement(new GenericProtein("id")));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testComparator() {
    try {
      Reaction reaction1 = new Reaction();
      reaction1.setIdReaction("2");
      Reaction reaction2 = new Reaction();
      reaction2.setIdReaction("1");
      Reaction reaction3 = new Reaction();
      reaction3.setIdReaction("3");
      List<Reaction> reactions = new ArrayList<>();
      reactions.add(reaction1);
      reactions.add(reaction2);
      reactions.add(reaction3);
      Collections.sort(reactions, Reaction.ID_COMPARATOR);
      assertEquals(reaction2, reactions.get(0));
      assertEquals(reaction1, reactions.get(1));
      assertEquals(reaction3, reactions.get(2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructor() {
    try {
      Reaction reaction1 = new Reaction();
      reaction1.addMiriamData(new MiriamData());
      reaction1.addSynonym("s");
      Reaction reaction2 = new Reaction(reaction1);

      assertEquals(1, reaction2.getMiriamData().size());
      assertEquals(1, reaction2.getSynonyms().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetDistanceFromPoint() {
    try {
      Reaction reaction1 = new Reaction();
      Reactant reactant = new Reactant();
      PolylineData line = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(2.0, 0.0));
      reactant.setLine(line);
      reaction1.addReactant(reactant);

      assertEquals(0.0, reaction1.getDistanceFromPoint(new Point2D.Double(0.0, 0.0)), Configuration.EPSILON);
      assertEquals(0.0, reaction1.getDistanceFromPoint(new Point2D.Double(1.0, 0.0)), Configuration.EPSILON);
      assertEquals(1.0, reaction1.getDistanceFromPoint(new Point2D.Double(1.0, 1.0)), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetClosestPointTo() {
    try {
      Reaction reaction1 = new Reaction();
      Reactant reactant = new Reactant();
      PolylineData line = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(2.0, 0.0));
      reactant.setLine(line);
      reaction1.addReactant(reactant);

      assertEquals(0.0,
          reaction1.getClosestPointTo(new Point2D.Double(0.0, 0.0)).distance(new Point2D.Double(0.0, 0.0)),
          Configuration.EPSILON);
      assertEquals(0.0,
          reaction1.getClosestPointTo(new Point2D.Double(1.0, 0.0)).distance(new Point2D.Double(1.0, 0.0)),
          Configuration.EPSILON);
      assertEquals(0.0,
          reaction1.getClosestPointTo(new Point2D.Double(1.0, 1.0)).distance(new Point2D.Double(1.0, 0.0)),
          Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddMiriam() {
    try {
      Reaction reaction1 = new Reaction();

      MiriamData md = new MiriamData(MiriamType.CAS, "1");
      reaction1.addMiriamData(md);

      assertEquals(1, reaction1.getMiriamData().size());

      reaction1.addMiriamData(md);
      assertEquals(1, reaction1.getMiriamData().size());
      assertEquals(1, getWarnings().size());

      List<MiriamData> list = new ArrayList<>();
      list.add(md);

      reaction1.addMiriamData(list);
      assertEquals(2, getWarnings().size());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      Reaction reaction1 = new Reaction();

      assertNotNull(reaction1.getStringType());
      assertNull(reaction1.getReactionRect());

      int id = 61;
      List<AbstractNode> nodes = new ArrayList<>();
      String notes = "dfp";

      ModelData data = new ModelData();
      Model model = new ModelFullIndexed(data);
      String symbol = "sybm";
      String formula = "form";
      Integer mechanicalConfidenceScore = 9908;
      Double lowerBound = 9.2;
      Double upperBound = 9.2;
      String subsystem = "ssyst";
      String geneProteinReaction = "GPR";
      List<String> synonyms = new ArrayList<>();

      reaction1.setId(id);
      reaction1.setNodes(nodes);
      reaction1.setNotes(notes);
      reaction1.setModel(model);
      assertEquals(model, reaction1.getModel());
      reaction1.setModelData(data);
      assertEquals(data, reaction1.getModelData());
      reaction1.setSymbol(symbol);
      assertEquals(symbol, reaction1.getSymbol());
      reaction1.setFormula(formula);
      assertEquals(formula, reaction1.getFormula());
      reaction1.setMechanicalConfidenceScore(mechanicalConfidenceScore);
      assertEquals(mechanicalConfidenceScore, reaction1.getMechanicalConfidenceScore());
      reaction1.setLowerBound(lowerBound);
      assertEquals(lowerBound, reaction1.getLowerBound());
      reaction1.setUpperBound(upperBound);
      assertEquals(upperBound, reaction1.getUpperBound());
      reaction1.setSubsystem(subsystem);
      assertEquals(subsystem, reaction1.getSubsystem());
      reaction1.setGeneProteinReaction(geneProteinReaction);
      assertEquals(geneProteinReaction, reaction1.getGeneProteinReaction());
      reaction1.setSynonyms(synonyms);
      assertEquals(synonyms, reaction1.getSynonyms());

      assertEquals(id, reaction1.getId());
      assertEquals(nodes, reaction1.getNodes());
      assertEquals(notes, reaction1.getNotes());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testVisibilityLevel() {
    try {
      Reaction reaction1 = new Reaction();
      assertEquals("", reaction1.getVisibilityLevel());

      reaction1.setVisibilityLevel(1);
      assertEquals("1", reaction1.getVisibilityLevel());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetCenterLine() {
    try {
      Reaction reaction1 = new Reaction();

      Reactant reactant = new Reactant();
      PolylineData line1 = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 1.0));
      reactant.setLine(line1);
      reaction1.addReactant(reactant);

      Product product = new Product();
      PolylineData line2 = new PolylineData(new Point2D.Double(0.0, 2.0), new Point2D.Double(4.0, 0.0));
      product.setLine(line2);
      reaction1.addProduct(product);

      Line2D line = reaction1.getCenterLine();
      assertEquals(4.0, line.getP1().distance(line.getP2()), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetCenterPoint() {
    try {
      Reaction reaction1 = new Reaction();

      Reactant reactant = new Reactant();
      PolylineData line1 = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 1.0));
      reactant.setLine(line1);
      reaction1.addReactant(reactant);

      Product product = new Product();
      PolylineData line2 = new PolylineData(new Point2D.Double(0.0, 2.0), new Point2D.Double(4.0, 0.0));
      product.setLine(line2);
      reaction1.addProduct(product);

      assertEquals(0.0, reaction1.getCenterPoint().distance(new Point2D.Double(2.0, 0.0)), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetCenterLine2() {
    try {
      Reaction reaction1 = new HeterodimerAssociationReaction();

      Reactant reactant = new Reactant();
      PolylineData line1 = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 1.0));
      reactant.setLine(line1);
      reaction1.addReactant(reactant);

      Reactant reactant2 = new Reactant();
      PolylineData line3 = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 1.0));
      reactant2.setLine(line3);
      reaction1.addReactant(reactant2);
      AndOperator operator = new AndOperator();
      operator.addInput(reactant);
      operator.addInput(reactant2);
      PolylineData line4 = new PolylineData(new Point2D.Double(10.0, 0.0), new Point2D.Double(11.0, 11.0));
      operator.setLine(line4);
      reaction1.addNode(operator);

      Product product = new Product();
      PolylineData line2 = new PolylineData(new Point2D.Double(0.0, 2.0), new Point2D.Double(4.0, 0.0));
      product.setLine(line2);
      reaction1.addProduct(product);

      Line2D line = reaction1.getCenterLine();
      assertEquals(6.0, line.getP1().distance(line.getP2()), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetCenterLine3() {
    try {
      Reaction reaction1 = new DissociationReaction();

      Reactant reactant = new Reactant();
      PolylineData line1 = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 1.0));
      reactant.setLine(line1);
      reaction1.addReactant(reactant);

      Product product = new Product();
      PolylineData line2 = new PolylineData(new Point2D.Double(0.0, 2.0), new Point2D.Double(4.0, 0.0));
      product.setLine(line2);
      reaction1.addProduct(product);

      Product product2 = new Product();
      PolylineData line3 = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 1.0));
      product2.setLine(line3);
      reaction1.addProduct(product2);
      SplitOperator operator = new SplitOperator();
      operator.addOutput(product);
      operator.addOutput(product2);
      PolylineData line4 = new PolylineData(new Point2D.Double(10.0, 0.0), new Point2D.Double(11.0, 0.0));
      operator.setLine(line4);
      reaction1.addNode(operator);

      Line2D line = reaction1.getCenterLine();
      assertEquals(10.0, line.getP1().distance(line.getP2()), Configuration.EPSILON);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new Reaction());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testRemoveModifier() {
    try {
      Reaction reaction = new Reaction();
      Modifier modifier = new Modifier();
      reaction.addModifier(modifier);
      assertEquals(1, reaction.getModifiers().size());
      reaction.removeModifier(modifier);
      assertEquals(0, reaction.getModifiers().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopy() {
    try {
      Reaction original = new Reaction();
      Reaction copy = original.copy();
      assertNotNull(copy);
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testCopyWithKinetics() {
    try {
      Reaction original = new Reaction();
      original.setKinetics(new SbmlKinetics());
      Reaction copy = original.copy();
      SbmlKineticsComparator kineticsComparator = new SbmlKineticsComparator();
      assertEquals(0, kineticsComparator.compare(original.getKinetics(), copy.getKinetics()));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidCopy() {
    try {
      new Reaction() {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
      }.copy();
      fail("Exception expected");
    } catch (NotImplementedException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
