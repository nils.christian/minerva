package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.*;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class InvalidColorSchemaExceptionTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new InvalidColorSchemaException("x"));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor1() {
		try {
			Exception e = new InvalidColorSchemaException(new Exception());
			assertNotNull(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() {
		try {
			Exception e = new InvalidColorSchemaException("message", new Exception());
			assertNotNull(e);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
