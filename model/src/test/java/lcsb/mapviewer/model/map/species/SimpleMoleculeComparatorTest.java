package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class SimpleMoleculeComparatorTest {

	SimpleMoleculeComparator comparator = new SimpleMoleculeComparator();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEquals() {
		try {
			SimpleMolecule simpleMolecule1 = createSimpleMolecule();
			SimpleMolecule simpleMolecule2 = createSimpleMolecule();

			assertEquals(0, comparator.compare(simpleMolecule1, simpleMolecule1));

			assertEquals(0, comparator.compare(simpleMolecule1, simpleMolecule2));
			assertEquals(0, comparator.compare(simpleMolecule2, simpleMolecule1));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Unknowne exception occurred");
		}
	}

	@Test
	public void testDifferent() {
		try {
			SimpleMolecule simpleMolecule1 = createSimpleMolecule();
			SimpleMolecule simpleMolecule2 = createSimpleMolecule();
			simpleMolecule1.setHomodimer(3);
			assertTrue(comparator.compare(simpleMolecule1, simpleMolecule2) != 0);
			assertTrue(comparator.compare(simpleMolecule2, simpleMolecule1) != 0);

			simpleMolecule1 = createSimpleMolecule();
			simpleMolecule2 = createSimpleMolecule();
			simpleMolecule1.setName("as");
			assertTrue(comparator.compare(simpleMolecule1, simpleMolecule2) != 0);
			assertTrue(comparator.compare(simpleMolecule2, simpleMolecule1) != 0);

			simpleMolecule1 = createSimpleMolecule();
			simpleMolecule2 = createSimpleMolecule();
			simpleMolecule1.setInChIKey("as");
			assertTrue(comparator.compare(simpleMolecule1, simpleMolecule2) != 0);
			assertTrue(comparator.compare(simpleMolecule2, simpleMolecule1) != 0);

			simpleMolecule1 = createSimpleMolecule();
			simpleMolecule2 = createSimpleMolecule();
			assertTrue(comparator.compare(null, simpleMolecule2) != 0);
			assertTrue(comparator.compare(simpleMolecule2, null) != 0);
			assertTrue(comparator.compare(null, null) == 0);

			assertTrue(comparator.compare(simpleMolecule1, Mockito.mock(SimpleMolecule.class)) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalid() {
		try {
			SimpleMolecule simpleMolecule1 = Mockito.mock(SimpleMolecule.class);
			comparator.compare(simpleMolecule1, simpleMolecule1);
			fail("Exception expected");
		} catch (NotImplementedException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testDifferentNewFields() throws Exception {
		try {
			SimpleMolecule species1 = createSimpleMolecule();
			SimpleMolecule species2 = createSimpleMolecule();

			species1.setSmiles("some symbol");
			assertTrue(comparator.compare(species1, species2) != 0);
			assertTrue(comparator.compare(species2, species1) != 0);

			species1 = createSimpleMolecule();
			species2 = createSimpleMolecule();

			species1.setInChI("some inchi");
			assertTrue(comparator.compare(species1, species2) != 0);
			assertTrue(comparator.compare(species2, species1) != 0);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public SimpleMolecule createSimpleMolecule() {
		SimpleMolecule result = new SimpleMolecule();
		result.setHomodimer(12);

		return result;
	}

}
