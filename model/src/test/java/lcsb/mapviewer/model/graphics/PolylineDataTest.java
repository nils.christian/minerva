package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class PolylineDataTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConstructor() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			assertNotNull(pd);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor2() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			PolylineData pd2 = new PolylineData(pd);
			assertNotNull(pd2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor3() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(5, 1));
			points.add(new Point2D.Double(5, 5));
			PolylineData pd2 = new PolylineData(points);
			assertNotNull(pd2);
			assertEquals(points.size(), pd2.getPoints().size());
			assertEquals(8, pd2.length(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.addPoint(0, new Point2D.Double(-10, -10));
			assertEquals(3, pd.getPoints().size());
			assertTrue(pd.getPoints().get(0).getX() < 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddInvalidPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.addPoint(0, new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.setPoint(0, new Point2D.Double(-10, -10));
			assertEquals(2, pd.getPoints().size());
			assertTrue(pd.getPoints().get(0).getX() < 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetInvalidPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.setPoint(0, new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetLines() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(5, 1));
			points.add(new Point2D.Double(5, 5));
			PolylineData pd2 = new PolylineData(points);
			assertEquals(2, pd2.getLines().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvaliArgument() throws Exception {
		try {
			new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, Double.NaN));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new PolylineData());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetSubline() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(5, 1));
			points.add(new Point2D.Double(5, 5));
			PolylineData pd = new PolylineData(points);
			PolylineData pd2 = pd.getSubline(0, 2);
			assertNotNull(pd2);
			assertEquals(2, pd2.getPoints().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetEndPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.setEndPoint(new Point2D.Double(-10, -10));
			assertEquals(2, pd.getPoints().size());
			assertTrue(pd.getEndPoint().getX() < 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetInvalidEndPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.setEndPoint(new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetStartPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.setStartPoint(new Point2D.Double(-10, -10));
			assertEquals(2, pd.getPoints().size());
			assertTrue(pd.getBeginPoint().getX() < 0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetInvalidStartPoint() throws Exception {
		try {
			PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
			pd.setStartPoint(new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToGeneralPath() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(5, 1));
			points.add(new Point2D.Double(5, 5));
			PolylineData pd = new PolylineData(points);
			GeneralPath gp = pd.toGeneralPath();
			PathIterator pi = gp.getPathIterator(new AffineTransform());
			int count = 0;
			while (!pi.isDone()) {
				count++;
				pi.next();
			}
			assertEquals(3, count);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testTrimEnd() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(11, 1));
			PolylineData pd = new PolylineData(points);
			pd.trimEnd(3);

			assertEquals(8, pd.getEndPoint().getX(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testTrimBegin() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(11, 1));
			PolylineData pd = new PolylineData(points);
			pd.trimBegin(3);

			assertEquals(4, pd.getPoints().get(0).getX(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testTrimEnd2() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(11, 1));
			PolylineData pd = new PolylineData(points);
			pd.trimEnd(30);

			assertEquals(1, pd.getEndPoint().getX(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testTrimBegin2() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(11, 1));
			PolylineData pd = new PolylineData(points);
			pd.trimBegin(30);

			assertEquals(11, pd.getPoints().get(0).getX(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testReverse() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(1, 1));
			points.add(new Point2D.Double(11, 1));
			PolylineData pd = new PolylineData(points);
			PolylineData pd2 = pd.reverse();
			assertTrue(pd.getBeginPoint().distance(pd2.getEndPoint()) <= Configuration.EPSILON);
			assertTrue(pd2.getBeginPoint().distance(pd.getEndPoint()) <= Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEmptyLength() throws Exception {
		try {
			PolylineData pd = new PolylineData();
			pd.addPoint(new Point2D.Double(0, 0));
			assertEquals(0, pd.length(), Configuration.EPSILON);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSetInvalidPoints() throws Exception {
		try {
			List<Point2D> points = new ArrayList<>();
			points.add(new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
			PolylineData pd = new PolylineData();
			pd.setPoints(points);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() throws Exception {
		try {
			PolylineData pd = new PolylineData();
			Color color = Color.BLACK;
			LineType type = LineType.DASH_DOT;
			String strWidth = "12";
			double width = 12.0;
			int id = 3;
			pd.setColor(color);
			pd.setId(id);
			pd.setType(type);
			pd.setWidth(strWidth);

			assertEquals(id, pd.getId());
			assertEquals(color, pd.getColor());
			assertEquals(width, pd.getWidth(), Configuration.EPSILON);
			assertEquals(type, pd.getType());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testSimpleCopy() throws Exception {
		try {
			PolylineData pd = new PolylineData();
			PolylineData pd2 = pd.copy();

			assertNotNull(pd2);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testInvalidCopy() throws Exception {
		try {
			PolylineData pd = new PolylineData() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
			};
			pd.copy();
			fail("Exception expected");
		} catch (NotImplementedException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
