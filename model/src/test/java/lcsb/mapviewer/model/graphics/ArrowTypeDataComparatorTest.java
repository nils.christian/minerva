package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArrowTypeDataComparatorTest {
	ArrowTypeDataComparator comparator = new ArrowTypeDataComparator();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCompareEquals() {
		try {
			assertEquals(0, comparator.compare(null, null));
			assertEquals(0, comparator.compare(new ArrowTypeData(), new ArrowTypeData()));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testCompareDifferent() {
		try {
			assertTrue(comparator.compare(new ArrowTypeData(), null) != 0);
			assertTrue(comparator.compare(null, new ArrowTypeData()) != 0);
			assertTrue(comparator.compare(new ArrowTypeData() {
				private static final long serialVersionUID = 1L;
			}, new ArrowTypeData()) != 0);
			
			ArrowTypeData atd1 = new ArrowTypeData();
			ArrowTypeData atd2 = new ArrowTypeData();

			atd1.setArrowType(ArrowType.CROSSBAR);
			assertTrue(comparator.compare(atd1, atd2)!=0);
			
			atd1 = new ArrowTypeData();
			atd2 = new ArrowTypeData();

			atd1.setArrowLineType(LineType.DOUBLE);
			assertTrue(comparator.compare(atd1, atd2)!=0);
			
			atd1 = new ArrowTypeData();
			atd2 = new ArrowTypeData();

			atd1.setLen(2.0);
			assertTrue(comparator.compare(atd1, atd2)!=0);
			
			
			atd1 = new ArrowTypeData();
			atd2 = new ArrowTypeData();

			atd1.setAngle(3.4);
			assertTrue(comparator.compare(atd1, atd2)!=0);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
