package lcsb.mapviewer.model.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CacheTypeTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new CacheType());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			int id = 4;
			int validity = 12;
			String className = "cname";
			CacheType ct = new CacheType();
			ct.setId(id);
			ct.setClassName(className);
			ct.setValidity(validity);
			assertEquals(id, ct.getId());
			assertEquals(className, ct.getClassName());
			assertEquals(validity, ct.getValidity());
			assertNotNull(ct.toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
