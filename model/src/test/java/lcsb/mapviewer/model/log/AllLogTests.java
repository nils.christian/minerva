package lcsb.mapviewer.model.log;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LogTypeTest.class, //
		ObjectLogTest.class, //
		SystemLogTest.class //
})
public class AllLogTests {

}
