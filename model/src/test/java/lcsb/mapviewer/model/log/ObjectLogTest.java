package lcsb.mapviewer.model.log;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.user.User;

public class ObjectLogTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new ObjectLog());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			ObjectLog log = new ObjectLog();
			String description = "qwe";
			int id = 20;
			int objectId = 21;
			Calendar time = Calendar.getInstance();
			Class<?> tableName = Object.class;
			LogType type = LogType.ADD_MISSING_CONNECTION;
			User user = new User();
			log.setDescription(description);
			log.setId(id);
			log.setObjectId(objectId);
			log.setTime(time);
			log.setTable(tableName);
			log.setType(type);
			log.setUser(user);
			
			assertEquals(description,log.getDescription());
			assertEquals(id,log.getId());
			assertEquals((Integer)objectId,log.getObjectId());
			assertEquals(time,log.getTime());
			assertEquals(tableName,log.getTable());
			assertEquals(type,log.getType());
			assertEquals(user,log.getUser());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
