package lcsb.mapviewer.model.log;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LogTypeTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidValues() {
		for (LogType type : LogType.values()) {
			assertNotNull(type);

			// for coverage tests
			LogType.valueOf(type.toString());
		}
	}

}
