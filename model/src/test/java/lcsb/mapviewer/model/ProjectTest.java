package lcsb.mapviewer.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class ProjectTest {
	Logger logger = Logger.getLogger(ProjectTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new Project());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testProject() {
		try {
			Project project = new Project("str");
			assertNotNull(project);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddModel() {
		try {
			Model model = new ModelFullIndexed(null);
			Model model2 = new ModelFullIndexed(null);
			Project project = new Project();
			assertEquals(0, project.getModels().size());
			project.addModel(model);
			assertEquals(1, project.getModels().size());
			project.addModel(model);
			assertEquals(1, project.getModels().size());
			project.addModel(model2.getModelData());
			assertEquals(2, project.getModels().size());
			project.removeModel(model2.getModelData());
			assertEquals(1, project.getModels().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			MiriamData organism = new MiriamData();
			int id = 5;
			String projectId = "str";
			ProjectStatus status = ProjectStatus.ANNOTATING;
			double progress = 4.5;
			Set<ModelData> models = new HashSet<>();
			String errors = "eerStr";
			String directory = "dir";
			String name = "name3";
			MiriamData disease = new MiriamData();
			Project project = new Project("str");
			boolean sbgn = true;

			project.setSbgnFormat(sbgn);
			assertEquals(sbgn, project.isSbgnFormat());
			
			project.setId(id);
			assertEquals(id, project.getId());

			project.setProjectId(projectId);
			assertEquals(projectId, project.getProjectId());

			project.setStatus(status);
			assertEquals(status, project.getStatus());

			project.setProgress(progress);
			assertEquals(progress, project.getProgress(), Configuration.EPSILON);

			project.setModels(models);
			assertEquals(models, project.getModels());

			project.setErrors(errors);
			assertEquals(errors, project.getErrors());

			project.setDirectory(directory);
			assertEquals(directory, project.getDirectory());

			project.setName(name);
			assertEquals(name, project.getName());

			project.setDisease(disease);
			assertEquals(disease, project.getDisease());

			UploadedFileEntry entry = new UploadedFileEntry() ;
			project.setInputData(entry );
			assertEquals(entry , project.getInputData());

			project.setOrganism(organism);
			assertEquals(organism, project.getOrganism());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddCreationWarning() {
		try {
			Project md = new Project();
			md.addWarning("text\ntext");

			fail("Exception expected");
		} catch (InvalidArgumentException e) {

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddCreationWarning2() {
		try {
			Project md = new Project();
			md.addWarning("");
			List<String> warnings = md.getWarnings();
			assertEquals(0, warnings.size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddCreationWarning3() {
		try {
			Project md = new Project();
			md.addWarning("xqw");
			List<String> warnings = md.getWarnings();
			assertEquals(1, warnings.size());
			assertEquals("xqw", warnings.get(0));

			md.addWarning("WER");
			warnings = md.getWarnings();
			assertEquals(2, warnings.size());
			assertEquals("WER", warnings.get(1));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddCreationWarnings() {
		try {
			List<String> warnings = new ArrayList<>();
			warnings.add("xqw");
			warnings.add("aso");
			warnings.add("");
			Project project = new Project();
			project.addWarnings(warnings);

			project.addWarning("WER");
			warnings = project.getWarnings();
			assertEquals(3, warnings.size());
			assertEquals("aso", warnings.get(1));

			project.addWarnings(warnings);
			warnings = project.getWarnings();
			assertEquals(6, warnings.size());
			assertEquals("aso", warnings.get(1));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddCreationWarnings2() {
		try {
			List<String> warnings = new ArrayList<>();
			warnings.add("xqw\nwer\nt");
			Project md = new Project();
			md.addWarnings(warnings);
			fail("Exception expected");
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddCreationWarnings3() {
		try {
			List<String> warnings = new ArrayList<>();
			Project md = new Project();
			md.addWarnings(warnings);
			assertEquals(0, md.getWarnings().size());
		} catch (InvalidArgumentException e) {
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddLoggingInfo() {
		try {
			Project project = new Project();
			EventStorageLoggerAppender appender = new EventStorageLoggerAppender();
			logger.addAppender(appender);
			logger.warn("test");
			logger.warn("test2");
			LoggingEvent event = appender.getWarnings().get(0);
			appender.getWarnings().add(new LoggingEvent("St", event.getLogger(), null, new Object(), null));
			logger.removeAppender(appender);

			project.addLoggingInfo(appender);
			assertEquals(2, project.getWarnings().size());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
