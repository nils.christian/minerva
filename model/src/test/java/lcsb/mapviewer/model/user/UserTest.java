package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new User());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testToString() {
		try {
			assertNotNull(new User().toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddPrivilege() {
		try {
			User user = new User();
			user.addPrivilege(new BasicPrivilege());
			assertEquals(1, user.getPrivileges().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			User user = new User();
			int id = 737;
			String login = "log 54";
			String cryptedPassword = "pa";
			String name = "ccr";
			String email= "a.a@pl.pl";
			String surname = "cccv";
			Set<BasicPrivilege> privileges = new HashSet<>();
			boolean removed = true;
			UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
			Color minColor = Color.BLACK;
			Color maxColor = Color.BLUE;

			user.setMaxColor(maxColor);
			assertEquals(maxColor, user.getMaxColor());
			
			user.setMinColor(minColor);
			assertEquals(minColor, user.getMinColor());
			
			user.setId(id);
			assertEquals((Integer)id, user.getId());
			
			user.setLogin(login);
			assertEquals(login, user.getLogin());
			
			user.setCryptedPassword(cryptedPassword);
			assertEquals(cryptedPassword, user.getCryptedPassword());
			
			user.setName(name);
			assertEquals(name, user.getName());
			
			user.setSurname(surname);
			assertEquals(surname, user.getSurname());
			
			user.setPrivileges(privileges);
			assertEquals(privileges, user.getPrivileges());
			
			user.setRemoved(removed);
			assertEquals(removed, user.isRemoved());
			
			user.setEmail(email);
			assertEquals(email, user.getEmail());
			
			user.setAnnotationSchema(annotationSchema);
			assertEquals(annotationSchema, user.getAnnotationSchema());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
