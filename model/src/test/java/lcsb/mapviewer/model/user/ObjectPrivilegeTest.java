package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.Project;

public class ObjectPrivilegeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new ObjectPrivilege());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testConstructror() {
    try {
      Integer id = 12;
      Project project = new Project();
      project.setId(id);
      ObjectPrivilege privilege = new ObjectPrivilege(project, 1, PrivilegeType.VIEW_PROJECT, new User());
      assertEquals(id, privilege.getIdObject());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      String idStr = "12";
      Integer id = 12;
      ObjectPrivilege privilege = new ObjectPrivilege();
      privilege.setIdObject(idStr);
      assertEquals(id, privilege.getIdObject());
      privilege.setIdObject((Integer) null);
      assertNull(privilege.getIdObject());
      privilege.setIdObject(id);
      assertEquals(id, privilege.getIdObject());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testEqualsPrivilege() {
    try {
      ObjectPrivilege privilege = new ObjectPrivilege();
      privilege.setType(PrivilegeType.ADD_MAP);
      assertFalse(privilege.equalsPrivilege(null));
      assertFalse(privilege.equalsPrivilege(new BasicPrivilege()));

      privilege.setIdObject(2);
      ObjectPrivilege privilege2 = new ObjectPrivilege();
      privilege2.setIdObject(3);
      privilege2.setType(PrivilegeType.ADD_MAP);
      assertFalse(privilege.equalsPrivilege(privilege2));

      privilege2.setIdObject(3);
      privilege2.setType(PrivilegeType.CONFIGURATION_MANAGE);
      assertFalse(privilege.equalsPrivilege(privilege2));

      privilege2.setIdObject(2);
      privilege2.setType(PrivilegeType.ADD_MAP);
      assertTrue(privilege.equalsPrivilege(privilege2));

      privilege.setIdObject((Integer) null);
      assertFalse(privilege.equalsPrivilege(privilege2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testEqualsWithEmptyObjectId() {
    try {
      ObjectPrivilege privilege = new ObjectPrivilege();
      privilege.setType(PrivilegeType.ADD_MAP);
      privilege.setIdObject((Integer) null);

      ObjectPrivilege privilege2 = new ObjectPrivilege();
      privilege2.setType(PrivilegeType.ADD_MAP);
      privilege2.setIdObject((Integer) null);
      assertTrue(privilege.equalsPrivilege(privilege2));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
