package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.MiriamType;

public class UserClassValidAnnotationsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new UserClassValidAnnotations());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testConstructor() {
		try {
			UserClassValidAnnotations usva = new UserClassValidAnnotations(Object.class, new MiriamType[] { MiriamType.CAS });
			assertNotNull(usva);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			UserClassValidAnnotations usva = new UserClassValidAnnotations();
			List<MiriamType> validMiriamTypes = new ArrayList<>();
			UserAnnotationSchema schema = new UserAnnotationSchema();
			Integer id = 69;
			usva.setValidMiriamTypes(validMiriamTypes);
			assertEquals(validMiriamTypes, usva.getValidMiriamTypes());
			usva.setId(id);
			assertEquals(id, usva.getId());
			usva.setAnnotationSchema(schema);
			assertEquals(schema, usva.getAnnotationSchema());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testAddValidMiriamType() {
		try {
			UserClassValidAnnotations usva = new UserClassValidAnnotations();
			usva.addValidMiriamType(MiriamType.CAS);
			assertTrue(usva.getValidMiriamTypes().contains(MiriamType.CAS));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
