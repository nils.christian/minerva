package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.MiriamType;

public class UserClassRequiredAnnotationsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new UserClassRequiredAnnotations());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testGetters() {
		try {
			UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations();
			Integer id= 24;
			List<MiriamType> requiredMiriamTypes = new ArrayList<>();
			Boolean requireAtLestOneAnnotation = true;
			UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
			
			obj.setId(id);
			obj.setRequiredMiriamTypes(requiredMiriamTypes);
			obj.setRequireAtLeastOneAnnotation(requireAtLestOneAnnotation);
			obj.setAnnotationSchema(annotationSchema);
			
			assertEquals(id, obj.getId());
			assertEquals(requiredMiriamTypes, obj.getRequiredMiriamTypes());
			assertEquals(requireAtLestOneAnnotation, obj.getRequireAtLeastOneAnnotation());
			
			assertEquals(annotationSchema, obj.getAnnotationSchema());
			
			obj.addRequiredMiriamType(MiriamType.CCDS);
			assertEquals(1,obj.getRequiredMiriamTypes().size());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testConstructor() {
		try {
			UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations(String.class,(Collection<MiriamType>)null);
			assertFalse(obj.getRequireAtLeastOneAnnotation());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testConstructor2() {
		try {
			UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations(String.class,new MiriamType[]{MiriamType.CAS});
			assertTrue(obj.getRequireAtLeastOneAnnotation());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void testConstructor3() {
		try {
			UserClassRequiredAnnotations obj = new UserClassRequiredAnnotations(String.class,new MiriamType[]{});
			assertFalse(obj.getRequireAtLeastOneAnnotation());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
