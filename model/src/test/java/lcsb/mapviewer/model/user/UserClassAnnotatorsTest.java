package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserClassAnnotatorsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new UserClassAnnotators());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testConstructor() {
		try {
			List<String> list = new ArrayList<>();
			list.add("test");
			UserClassAnnotators uca = new UserClassAnnotators(String.class,list);
			
			assertEquals(1,uca.getAnnotators().size());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	@Test
	public void testGetters() {
		try {
			UserClassAnnotators uca = new UserClassAnnotators();
			Integer id = 26;
			UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
			List<String> annotators = new ArrayList<>();
			Class<?> clazz = String.class;
			
			uca.setId(id);
			uca.setAnnotationSchema(annotationSchema);
			uca.setAnnotators(annotators);
			uca.addAnnotator(clazz);
			
			assertEquals(id, uca.getId());
			assertEquals(annotationSchema, uca.getAnnotationSchema());
			assertEquals(annotators, uca.getAnnotators());
			assertEquals(clazz.getCanonicalName(), uca.getAnnotators().get(0));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
