package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamType;

public class UserAnnotationSchemaTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddClassAnnotator() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserClassAnnotators ca = new UserClassAnnotators(String.class, new ArrayList<String>());
      UserClassAnnotators ca2 = new UserClassAnnotators(String.class, new ArrayList<String>());
      annotationSchema.addClassAnnotator(ca);
      annotationSchema.addClassAnnotator(ca2);
      assertEquals(1, annotationSchema.getClassAnnotators().size());
      assertEquals(ca2.getAnnotators(), annotationSchema.getAnnotatorsForClass(String.class));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testInvalidAddClassAnnotator() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserClassAnnotators ca = new UserClassAnnotators(String.class, new ArrayList<String>());
      ca.setClassName((String) null);
      annotationSchema.addClassAnnotator(ca);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddClassRequiredAnnotations() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserClassRequiredAnnotations cra = new UserClassRequiredAnnotations(String.class, new ArrayList<MiriamType>());
      UserClassRequiredAnnotations cra2 = new UserClassRequiredAnnotations(String.class, new ArrayList<MiriamType>());
      annotationSchema.addClassRequiredAnnotations(cra);
      annotationSchema.addClassRequiredAnnotations(cra2);
      assertEquals(1, annotationSchema.getClassRequiredAnnotators().size());
      assertEquals(cra2, annotationSchema.getClassRequiredAnnotators().get(0));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddInvalidClassRequiredAnnotations() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserClassRequiredAnnotations cra = new UserClassRequiredAnnotations(String.class, new ArrayList<MiriamType>());
      cra.setClassName((String) null);

      annotationSchema.addClassRequiredAnnotations(cra);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddClassValidAnnotations() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserClassValidAnnotations cva = new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>());
      UserClassValidAnnotations cva2 = new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>());
      annotationSchema.addClassValidAnnotations(cva);
      annotationSchema.addClassValidAnnotations(cva2);
      assertEquals(1, annotationSchema.getClassValidAnnotators().size());
      assertEquals(cva2, annotationSchema.getClassValidAnnotators().get(0));
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetValidAnnotations() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      List<MiriamType> list = new ArrayList<>();
      list.add(MiriamType.CAS);
      UserClassValidAnnotations cva = new UserClassValidAnnotations(String.class, list);
      annotationSchema.addClassValidAnnotations(cva);
      assertEquals(1, annotationSchema.getValidAnnotations(String.class).size());
      assertEquals(0, annotationSchema.getValidAnnotations(Integer.class).size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetRequiredAnnotations() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      List<MiriamType> list = new ArrayList<>();
      list.add(MiriamType.CAS);
      UserClassRequiredAnnotations cva = new UserClassRequiredAnnotations(String.class, list);
      annotationSchema.addClassRequiredAnnotations(cva);
      assertEquals(1, annotationSchema.getRequiredAnnotations(String.class).size());
      assertEquals(0, annotationSchema.getRequiredAnnotations(Integer.class).size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddInvalidClassValidAnnotations() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserClassValidAnnotations cva = new UserClassValidAnnotations(String.class, new ArrayList<MiriamType>());
      cva.setClassName((String) null);

      annotationSchema.addClassValidAnnotations(cva);
      fail("Exception expected");
    } catch (InvalidArgumentException e) {
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddGuiPreference() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();

      annotationSchema.addGuiPreference(new UserGuiPreference());
      assertEquals(1, annotationSchema.getGuiPreferences().size());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testAddGuiPreferenceWithDuplicateKey() throws Exception {
    try {
      UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
      UserGuiPreference preference1 = new UserGuiPreference();
      preference1.setKey("test");
      preference1.setValue("value");
      annotationSchema.addGuiPreference(preference1);
      UserGuiPreference preference2 = new UserGuiPreference();
      preference2.setKey("test");
      preference2.setValue("new value");
      annotationSchema.addGuiPreference(preference2);
      assertEquals(1, annotationSchema.getGuiPreferences().size());
      assertEquals("new value", annotationSchema.getGuiPreferences().iterator().next().getValue());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testSerialization() {
    try {
      SerializationUtils.serialize(new UserAnnotationSchema());
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void testGetters() {
    try {
      UserAnnotationSchema uas = new UserAnnotationSchema();
      Boolean validateMiriamTypes = true;
      List<UserClassAnnotators> classAnnotators = new ArrayList<>();
      List<UserClassValidAnnotations> classValidAnnotators = new ArrayList<>();
      List<UserClassRequiredAnnotations> classRequiredAnnotators = new ArrayList<>();
      User user = new User();

      Boolean sbgnFormat = true;
      Boolean networkLayoutAsDefault = true;

      uas.setValidateMiriamTypes(validateMiriamTypes);
      uas.setClassAnnotators(classAnnotators);
      uas.setClassValidAnnotators(classValidAnnotators);
      uas.setClassRequiredAnnotators(classRequiredAnnotators);
      uas.setSbgnFormat(sbgnFormat);
      uas.setNetworkLayoutAsDefault(networkLayoutAsDefault);
      uas.setUser(user);

      assertEquals(validateMiriamTypes, uas.getValidateMiriamTypes());
      assertEquals(classAnnotators, uas.getClassAnnotators());
      assertEquals(classValidAnnotators, uas.getClassValidAnnotators());
      assertEquals(classRequiredAnnotators, uas.getClassRequiredAnnotators());
      assertEquals(sbgnFormat, uas.getSbgnFormat());
      assertEquals(networkLayoutAsDefault, uas.getNetworkLayoutAsDefault());
      assertEquals(user, uas.getUser());

    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

}
