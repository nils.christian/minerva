package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new ConfigurationOption());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testGetters() {
		try {
			ConfigurationOption conf = new ConfigurationOption();
			int id = 81;
			ConfigurationElementType type = ConfigurationElementType.DEFAULT_MAP;
			String value = "a.val";
			conf.setId(id);
			assertEquals(id, conf.getId());
			conf.setType(type);
			assertEquals(type, conf.getType());
			conf.setValue(value);
			assertEquals(value, conf.getValue());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
