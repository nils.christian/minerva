package lcsb.mapviewer.model.user;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BasicPrivilegeTest.class, //
	ConfigurationElementTypeTest.class, //
	ConfigurationElementEditTypeTest.class, //
		ConfigurationTest.class, //
		ObjectPrivilegeTest.class, //
		PrivilegeTypeTest.class, //
		UserAnnotationSchemaTest.class, //
		UserClassAnnotatorsTest.class, //
		UserClassRequiredAnnotationsTest.class, //
		UserClassValidAnnotationsTest.class, //
		UserTest.class,//
})
public class AllUserTests {

}
