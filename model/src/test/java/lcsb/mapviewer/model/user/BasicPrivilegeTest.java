package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BasicPrivilegeTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSerialization() {
		try {
			SerializationUtils.serialize(new BasicPrivilege());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetters() {
		try {
			BasicPrivilege privilege = new BasicPrivilege();
			int id = 25;
			int level = 26;
			PrivilegeType type = PrivilegeType.ADD_MAP;
			User user = new User();

			privilege.setId(id);
			privilege.setLevel(level);
			privilege.setType(type);
			privilege.setUser(user);

			assertEquals(id, privilege.getId());
			assertEquals(level, privilege.getLevel());
			assertEquals(type, privilege.getType());
			assertEquals(user, privilege.getUser());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testEqualsPrivilege() {
		try {
			BasicPrivilege privilege = new BasicPrivilege();
			assertFalse(privilege.equalsPrivilege(null));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
