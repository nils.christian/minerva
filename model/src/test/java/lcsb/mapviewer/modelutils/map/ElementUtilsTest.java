package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Protein;

public class ElementUtilsTest {
	Logger logger = Logger.getLogger(ElementUtilsTest.class);

	@Before
	public void setUp() throws Exception {
		ElementUtils.setElementClasses(null);
		ElementUtils.setReactionClasses(null);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testClassTree() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();

			ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();
			elementUtils.getAnnotatedElementClassTree();
			elementUtils.getAnnotatedElementClassTree();
			elementUtils.getAnnotatedElementClassTree();
			elementUtils.getAnnotatedElementClassTree();

			assertNotNull(top);

			assertTrue(top.getChildren().size() > 0);

			// print(top, 0);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetTag() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();

			assertNotNull(elementUtils.getElementTag(new Reaction() {
				private static final long serialVersionUID = 1L;
			}));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetTag2() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();
			
			GenericProtein protein = new GenericProtein("id");

			assertNotNull(elementUtils.getElementTag(protein, new Object()));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testGetTagForInvalidElement() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();
			
			BioEntity element = Mockito.mock(BioEntity.class);
			assertNotNull(elementUtils.getElementTag(element));
			fail("Exception expected");
		} catch (NotImplementedException e) {
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected void print(ClassTreeNode top, int indent) {
		if (indent > 10) {
			throw new InvalidArgumentException();
		}
		String tmp = "";
		for (int i = 0; i < indent; i++)
			tmp += "  ";
		logger.debug(tmp + top.getCommonName());
		for (ClassTreeNode node : top.getChildren()) {
			print(node, indent + 1);
		}

	}

	@Test
	public void tesGetAvailableElementSubclasses() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();

			List<Class<? extends Element>> list = elementUtils.getAvailableElementSubclasses();
			assertTrue(list.contains(IonChannelProtein.class));
			assertFalse(list.contains(Protein.class));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void tesClassByName() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();
			assertEquals(Ion.class, elementUtils.getClassByName(Ion.class.getSimpleName()));
			assertEquals(Ion.class, elementUtils.getClassByName("Ion"));
			assertNull(elementUtils.getClassByName("unknown class name"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void tesGetAvailableReactionSubclasses() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();

			List<Class<? extends Reaction>> list = elementUtils.getAvailableReactionSubclasses();
			assertTrue(list.contains(UnknownNegativeInfluenceReaction.class));
			assertFalse(list.contains(Reaction.class));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void tesClassByName2() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();
			assertEquals(UnknownNegativeInfluenceReaction.class, elementUtils.getClassByName("UnknownNegativeInfluence"));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void tesAnnotatedElementClassTree() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();
			ClassTreeNode tree = elementUtils.getAnnotatedElementClassTree();
			Queue<ClassTreeNode> queue = new LinkedList<ClassTreeNode>();
			queue.add(tree);
			while (!queue.isEmpty()) {
				ClassTreeNode node = queue.poll();
				for (ClassTreeNode child : node.getChildren()) {
					queue.add(child);
				}
				assertNotNull("Data for class " + node.getClazz() + " is null", node.getData());
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void tesAnnotatedElementClassTreeSorted() throws Exception {
		try {
			ElementUtils elementUtils = new ElementUtils();
			ClassTreeNode tree = elementUtils.getAnnotatedElementClassTree();
			Queue<ClassTreeNode> queue = new LinkedList<ClassTreeNode>();
			queue.add(tree);
			while (!queue.isEmpty()) {
				ClassTreeNode node = queue.poll();
				String lastChild = "";
				for (ClassTreeNode child : node.getChildren()) {
					queue.add(child);
					assertTrue(lastChild.compareTo(child.getCommonName()) <= 0);
					lastChild = child.getCommonName();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
