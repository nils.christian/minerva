package lcsb.mapviewer.modelutils.map;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ClassTreeNodeTest.class, //
		ElementUtilsTest.class, //
		RequireAnnotationMapTest.class, //
})
public class AllMapUtilTests {

}
