package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClassTreeNodeTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetters() {
		try {
			ClassTreeNode node = new ClassTreeNode(Object.class);
			Class<?> clazz = String.class;
			List<ClassTreeNode> children = new ArrayList<>();
			String commonName= "test";
			node.setClazz(clazz);
			node.setChildren(children);
			node.setCommonName(commonName);
			
			assertEquals(clazz, node.getClazz());
			assertEquals(children, node.getChildren());
			assertEquals(commonName, node.getCommonName());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
