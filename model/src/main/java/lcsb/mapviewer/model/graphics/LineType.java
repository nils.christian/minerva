package lcsb.mapviewer.model.graphics;

import java.awt.BasicStroke;
import java.awt.Stroke;

import lcsb.mapviewer.common.geometry.CompositeStroke;

/**
 * Available types of lines in the system.
 * 
 * @author Piotr Gawron
 * 
 */
public enum LineType {
	/**
	 * Solid line.
	 */
	SOLID(1),

	/**
	 * Solid bold line.
	 */

	SOLID_BOLD(3),
	/**
	 * Dash-dot-dot line:
	 * 
	 * <pre>
	 *  - . . - . . - . . - . .
	 * </pre>
	 */

	DASH_DOT_DOT(1, new float[] { 11.0f, 3.0f, 1.0f, 3.0f, 1.0f, 3.0f }),
	/**
	 * Dash-dot line:
	 * 
	 * <pre>
	 *  - . - . - . - . - . - . - .
	 * </pre>
	 */
	DASH_DOT(1, new float[] { 11.0f, 3.0f, 1.0f, 3.0f }),

	/**
	 * Dashed line:
	 * 
	 * <pre>
	 *  - - - - - - -
	 * </pre>
	 * 
	 * .
	 */
	DASHED(1, new float[] { 8.0f, 8.0f }),

	/**
	 * Dotted line:
	 * 
	 * <pre>
	 *  . . . . . . . .
	 * </pre>
	 * 
	 * .
	 */
	DOTTED(1, new float[] { 2.0f, 5.0f }),

	/**
	 * Dashed and bold line:
	 * 
	 * <pre>
	 *  - - - - - - -
	 * </pre>
	 * 
	 * .
	 */
	DASHED_BOLD(3, new float[] { 8.0f, 8.0f }),

	/**
	 * Double line.
	 */
	DOUBLE(new CompositeStroke(new BasicStroke(10f), new BasicStroke(0.5f)));

	/**
	 * Default miterlimit: "the limit to trim the miter join. The miterlimit must
	 * be greater than or equal to 1.0f
	 * " taken from <a href="http://docs.oracle.com
	 * /javase/7/docs/api/java/awt/BasicStroke.html" >oracle webpage</a>
	 */
	private static final float	DEFAULT_MITERLIMIT	= 10.0f;

	/**
	 * {@link BasicStroke} defined for the line type.
	 */
	private Stroke							stroke;

	/**
	 * Constructor with line width as a parameter.
	 * 
	 * @param width
	 *          width of the line
	 */
	LineType(float width) {
		this.stroke = new BasicStroke(width);
	}

	/**
	 * Constructor for complex line types.
	 * 
	 * @param stroke specific {@link Stroke} used for this line type 
	 */
	LineType(Stroke stroke) {
		this.stroke = stroke;
	}

	/**
	 * Constructor with line width and pattern as a parameter.
	 * 
	 * @param width
	 *          width of the line
	 * @param pattern
	 *          pattern of the line
	 */
	LineType(float width, float[] pattern) {
		this.stroke = new BasicStroke(width, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, DEFAULT_MITERLIMIT, pattern, 0.0f);
	}

	/**
	 * Return the {@link BasicStroke} for this line type.
	 * 
	 * @return the {@link BasicStroke} for this line type
	 */
	public Stroke getStroke() {
		return stroke;
	}
}
