package lcsb.mapviewer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lcsb.mapviewer.common.EventStorageLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.MapCanvasType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Top level structure in the system. It describes single project and contains
 * all data in it.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "project_table")
public class Project implements Serializable {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(Project.class);

  /**
  	* 
    */
  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * Project identifier defined by the user (some meaningful string).
   */
  @Column(name = "project_id", unique = true, nullable = false)
  private String projectId;

  /**
   * Project disease.
   */
  @Cascade({ CascadeType.ALL })
  @OneToOne
  private MiriamData disease;

  /**
   * Name of the project.
   */
  private String name;

  /**
   * Version of the project.
   */
  private String version;

  /**
   * Email of the user who should be notified when some serious modification to
   * the map were performed.
   */
  private String notifyEmail;

  /**
   * Directory where the images on the server will be stored for this project.
   */
  private String directory;

  /**
   * Status of the project used during uploading the project to the system.
   */
  @Enumerated(EnumType.STRING)
  private ProjectStatus status = ProjectStatus.UNKNOWN;

  /**
   * Progress of single step of project uploading.
   */
  private double progress = 0;

  /**
   * Errors connected with the project.
   */
  private String errors = null;

  /**
   * Short description of the map.
   */
  @Column(columnDefinition = "TEXT")
  private String warnings;

  /**
   * Which "maps" implementation should be used for frontend visualization.
   */
  @Enumerated(EnumType.STRING)
  @Column(name = "map_canvas_type")
  private MapCanvasType mapCanvasType = MapCanvasType.OPEN_LAYERS;

  @Column(name = "google_map_license_accept_date")
  private Calendar googleMapLicenseAcceptDate;

  /**
   * Map models in the project.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  private Set<ModelData> models = new HashSet<>();

  /**
   * Here we store input file.
   */
  @Cascade({ CascadeType.SAVE_UPDATE })
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "file_entry_iddb")
  private UploadedFileEntry inputData;

  /**
   * Organism associated with the project.
   */
  @Cascade({ CascadeType.ALL })
  @OneToOne
  private MiriamData organism;

  /**
   * SBGN format selection. Set, if map is to be displayed in SBGN format
   */
  private boolean sbgnFormat = false;

  /**
   * List of overview images describing this {@link Project}. The elements on the
   * list can create a hierarchy that describe dependencies, however now matter if
   * the image is a top-level object or a leaf it should be placed on this list.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("id")
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<OverviewImage> overviewImages = new ArrayList<>();

  /**
   * Default constructor.
   */
  public Project() {
  }

  /**
   * Constructor with the project name in the parameter.
   * 
   * @param projectName
   *          name of the project
   */
  public Project(String projectName) {
    this.projectId = projectName;
  }

  /**
   * Adds map model to the project.
   * 
   * @param model
   *          map to be added
   */
  public void addModel(ModelData model) {
    models.add(model);
    model.setProject(this);
  }

  /**
   * Adds map model to the project.
   * 
   * @param model
   *          map to be added
   */
  public void addModel(Model model) {
    models.add(model.getModelData());
    model.setProject(this);
  }

  /**
   * Removes map model from the project.
   * 
   * @param model
   *          map model to remove
   */
  public void removeModel(ModelData model) {
    models.remove(model);
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return user defined project identifier
   * @see #projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @param projectId
   *          user defined project identifier
   * @see #projectId
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the status
   * @see #status
   */
  public ProjectStatus getStatus() {
    return status;
  }

  /**
   * @param status
   *          the status to set
   * @see #status
   */
  public void setStatus(ProjectStatus status) {
    this.status = status;
  }

  /**
   * @return the progress
   * @see #progress
   */
  public double getProgress() {
    return progress;
  }

  /**
   * @param progress
   *          the progress to set
   * @see #progress
   */
  public void setProgress(double progress) {
    this.progress = progress;
  }

  /**
   * @return the models
   * @see #models
   */
  public Set<ModelData> getModels() {
    return models;
  }

  /**
   * @param models
   *          the models to set
   * @see #models
   */
  public void setModels(Set<ModelData> models) {
    this.models = models;
  }

  /**
   * @return the errors
   * @see #errors
   */
  public String getErrors() {
    return errors;
  }

  /**
   * @param errors
   *          the errors to set
   * @see #errors
   */
  public void setErrors(String errors) {
    this.errors = errors;
  }

  /**
   * @return the directory
   * @see #directory
   */
  public String getDirectory() {
    return directory;
  }

  /**
   * @param directory
   *          the directory to set
   * @see #directory
   */
  public void setDirectory(String directory) {
    this.directory = directory;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return disease
   */
  public MiriamData getDisease() {
    return disease;
  }

  /**
   * @param disease
   *          the disease
   */
  public void setDisease(MiriamData disease) {
    this.disease = disease;
  }

  /**
   * @param organism
   *          the organism to set
   * @see #organism
   */
  public void setOrganism(MiriamData organism) {
    this.organism = organism;
  }

  /**
   * @return the organism
   * @see #organism
   */
  public MiriamData getOrganism() {
    return organism;
  }

  /**
   * Adds a warning to a list of warnings generated during model creation.
   * 
   * @param warning
   *          warning to add
   */
  public void addWarning(String warning) {
    if (warning.trim().contains("\n")) {
      throw new InvalidArgumentException("Warning must be one line of text");
    }
    if (warning.trim().equals("")) {
      return;
    }
    if (warnings == null) {
      warnings = warning.trim();
    } else {
      warnings = warnings + "\n" + warning.trim();
    }
  }

  /**
   * Returns list of warnings set during creation of mode.
   * 
   * @return list of warnings set during creation of mode
   */
  public List<String> getWarnings() {
    List<String> result = new ArrayList<>();
    if (warnings == null) {
      return result;
    }
    String[] warningsArray = warnings.split("\n");
    for (String string : warningsArray) {
      result.add(string);
    }
    return result;
  }

  /**
   * Adds warnings to a list of warnings generated during model creation.
   * 
   * @param warnings
   *          list of warnings to add
   */
  public void addWarnings(List<String> warnings) {
    StringBuilder sb = new StringBuilder("");
    if (this.warnings != null) {
      sb.append(this.warnings);
    }
    for (String string : warnings) {
      if (string.trim().equals("")) {
        continue;
      }
      if (string.contains("\n")) {
        throw new InvalidArgumentException("Warning must be one line of text, but found:\n" + string);
      }
      if (sb.length() > 0) {
        sb.append("\n");
      }
      sb.append(string.trim());
    }
    if (sb.length() != 0) {
      this.warnings = sb.toString();
    }
  }

  /**
   * Adds warning log information to a project.
   * 
   * @param appender
   *          {@link Appender log4j appender} that gathered logs
   */
  public void addLoggingInfo(EventStorageLoggerAppender appender) {
    List<String> warnings = new ArrayList<>();
    for (LoggingEvent event : appender.getWarnings()) {
      if (event.getMessage() instanceof String) {
        warnings.add(((String) event.getMessage()).replaceAll("\n", "_NEW_LINE_"));
      } else {
        logger.warn("Unknown message class: " + event.getClass());
      }
    }
    addWarnings(warnings);
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat
   *          the sbgnFormat to set
   * @see #sbgnFormat
   */
  public void setSbgnFormat(boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

  /**
   * @return the version
   * @see #version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @param version
   *          the version to set
   * @see #version
   */
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * @return the overviewImages
   * @see #overviewImages
   */
  public List<OverviewImage> getOverviewImages() {
    return overviewImages;
  }

  /**
   * @param overviewImages
   *          the overviewImages to set
   * @see #overviewImages
   */
  public void setOverviewImages(List<OverviewImage> overviewImages) {
    this.overviewImages = overviewImages;
    for (OverviewImage overviewImage : overviewImages) {
      overviewImage.setProject(this);
    }
  }

  /**
   * Returns top level {@link OverviewImage} that should be present as a first one
   * when switching to view data by the overview images.
   * 
   * @return top level {@link OverviewImage}
   */
  public OverviewImage getTopOverviewImage() {
    Set<OverviewImage> set = new HashSet<OverviewImage>();
    set.addAll(getOverviewImages());
    for (OverviewImage image : getOverviewImages()) {
      for (OverviewLink ol : image.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          set.remove(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    if (set.size() > 0) {
      return set.iterator().next();
    } else if (getOverviewImages().size() > 0) {
      logger.warn("Cannot determine top level image. Taking first one: " + getOverviewImages().get(0).getFilename());
      return getOverviewImages().get(0);
    } else {
      return null;
    }
  }

  public void addOverviewImage(OverviewImage oi) {
    this.overviewImages.add(oi);
    oi.setProject(this);
  }

  public void addOverviewImages(List<OverviewImage> parseOverviewLinks) {
    for (OverviewImage overviewImage : parseOverviewLinks) {
      this.addOverviewImage(overviewImage);
    }

  }

  /**
   * @return the inputData
   * @see #inputData
   */
  public UploadedFileEntry getInputData() {
    return inputData;
  }

  /**
   * @param inputData
   *          the inputData to set
   * @see #inputData
   */
  public void setInputData(UploadedFileEntry inputData) {
    this.inputData = inputData;
  }

  /**
   * @return the notifyEmail
   * @see #notifyEmail
   */
  public String getNotifyEmail() {
    return notifyEmail;
  }

  /**
   * @param notifyEmail
   *          the notifyEmail to set
   * @see #notifyEmail
   */
  public void setNotifyEmail(String notifyEmail) {
    this.notifyEmail = notifyEmail;
  }

  public MapCanvasType getMapCanvasType() {
    return mapCanvasType;
  }

  public void setMapCanvasType(MapCanvasType mapCanvasType) {
    MapCanvasType oldMapCanvasType = this.mapCanvasType;
    this.mapCanvasType = mapCanvasType;
    if (!mapCanvasType.equals(oldMapCanvasType)) {
      if (mapCanvasType.equals(MapCanvasType.GOOGLE_MAPS_API)) {
        this.setGoogleMapLicenseAcceptDate(Calendar.getInstance());
      } else {
        this.setGoogleMapLicenseAcceptDate(null);
      }
    }
  }

  public Calendar getGoogleMapLicenseAcceptDate() {
    return googleMapLicenseAcceptDate;
  }

  public void setGoogleMapLicenseAcceptDate(Calendar googleMapLicenseAcceptDate) {
    this.googleMapLicenseAcceptDate = googleMapLicenseAcceptDate;
  }

}
