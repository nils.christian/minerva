package lcsb.mapviewer.model.log;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * General system log entry.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("SYSTEM_LOG")
public class SystemLog extends GenericLog {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
}
