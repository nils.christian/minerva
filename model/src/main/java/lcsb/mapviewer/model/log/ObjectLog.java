package lcsb.mapviewer.model.log;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Log entry that has refernce to an object in the database.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("OBJECT_LOG")
public class ObjectLog extends GenericLog {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Identifier of the referenced object in database.
	 */
	private Integer		objectId;

	/**
	 * Class of the object that is referenced by this log entry.
	 */
	private Class<?>	tableName;

	/**
	 * @return the objectId
	 * @see #objectId
	 */
	public Integer getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId
	 *          the objectId to set
	 * @see #objectId
	 */
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	/**
	 * @return the tableName
	 * @see #tableName
	 */
	public Class<?> getTable() {
		return tableName;
	}

	/**
	 * @param tableName
	 *          the tableName to set
	 * @see #tableName
	 */
	public void setTable(Class<?> tableName) {
		this.tableName = tableName;
	}
}
