package lcsb.mapviewer.model.log;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lcsb.mapviewer.model.user.User;

/**
 * Generic object representing log entry.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "log_table")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "log_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_LOG")
public abstract class GenericLog implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Unique database identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDb", unique = true, nullable = false)
	private int				id;

	/**
	 * Logged user responsible for the log event.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	private User			user;

	/**
	 * Type of the logged event.
	 */
	private LogType		type;

	/**
	 * Description with additional information.
	 */
	private String		description;

	/**
	 * When the event took place.
	 */
	private Calendar	time;

	/**
	 * @return the id
	 * @see #id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the user
	 * @see #user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *          the user to set
	 * @see #user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public LogType getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(LogType type) {
		this.type = type;
	}

	/**
	 * @return the description
	 * @see #description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 * @see #description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the time
	 * @see #time
	 */
	public Calendar getTime() {
		return time;
	}

	/**
	 * @param time
	 *          the time to set
	 * @see #time
	 */
	public void setTime(Calendar time) {
		this.time = time;
	}

}
