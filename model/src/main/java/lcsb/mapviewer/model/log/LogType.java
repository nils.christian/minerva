package lcsb.mapviewer.model.log;

/**
 * Types of log events.
 * 
 * @author Piotr Gawron
 * 
 */
public enum LogType {
	/**
	 * New data mining object was added to the map.
	 */
	ADD_MISSING_CONNECTION,

	/**
	 * Data mining object was removed from the map.
	 */
	REMOVE_MISSING_CONNECTION,

	/**
	 * User logged in.
	 */
	USER_LOGIN,

	/**
	 * User logged out.
	 */
	USER_LOGOUT,

	/**
	 * User was created.
	 */
	USER_CREATED,

	/**
	 * User was removed.
	 */
	USER_REMOVED,

	/**
	 * Map was created.
	 */
	MAP_CREATED,

	/**
	 * Map was removed.
	 */
	MAP_REMOVED,

	/**
	 * Custom layout was added.
	 */
	LAYOUT_CREATED,

	/**
	 * Custom layout was removed.
	 */
	LAYOUT_REMOVED

}
