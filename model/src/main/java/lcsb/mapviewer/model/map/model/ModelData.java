package lcsb.mapviewer.model.map.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Representation of the model data. It contains all information about single
 * map:
 * <ul>
 * <li>species and compartments ({@link #elements} field)</li>
 * <li>list of reactions ({@link #reactions})</li>
 * <li>layers with additional graphical objects ({@link #layers})</li>
 * <li>different graphical visualizations of the whole map ({@link #layouts})
 * </li>
 * <li>some other meta data (like: creation date, version, etc)</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "model_table")
public class ModelData implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ModelData.class);

  /**
   * Set of all elements in the map.
   * 
   * @see Element
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<Element> elements = new HashSet<>();

  /**
   * Set of all layers in the map.
   * 
   * @see Layer
   * 
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<Layer> layers = new HashSet<>();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * Collection of all reactions in the map.
   * 
   * @see Reaction
   * 
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<Reaction> reactions = new HashSet<>();

  /**
   * Collection of SBML functions in the map.
   * 
   * @see SbmlFunction
   * 
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<SbmlFunction> functions = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "model_parameters", joinColumns = {
      @JoinColumn(name = "model_id", referencedColumnName = "idDb", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "parameter_id", referencedColumnName = "idDb", nullable = true, updatable = true) })
  private Set<SbmlParameter> parameters = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  private Set<SbmlUnit> units = new HashSet<>();

  /**
   * When the map was created.
   */
  private Calendar creationDate = Calendar.getInstance();

  /**
   * Width of the map.
   */
  private Double width;

  /**
   * Height of the map.
   */
  private Double height;

  /**
   * X coordinate that should be used when initially showing map.
   */
  private Double defaultCenterX;

  /**
   * Y coordinate that should be used when initially showing map.
   */
  private Double defaultCenterY;

  /**
   * Description of the map.
   */
  @Column(name = "notes", columnDefinition = "TEXT")
  private String notes;

  /**
   * Name of the map.
   */
  private String name;

  /**
   * Another CellDesigner identifier.
   */
  private String idModel;

  /**
   * How many hierarchical levels are in this map.
   */
  private int zoomLevels;

  /**
   * Zoom level that should be used when initially showing map.
   */
  private Integer defaultZoomLevel;

  /**
   * Size of the image tile that are used in this model.
   */
  private int tileSize;

  /**
   * List of layouts.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "model", orphanRemoval = true)
  @OrderBy("id")
  private List<Layout> layouts = new ArrayList<>();

  /**
   * Project to which this model belong to.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private Project project;

  // This field should be transient in hibernate and during the transformation
  // to xml
  /**
   * {@link Model} object that is a container where data is being placed.
   */
  @Transient
  @XmlTransient
  private Model model;

  /**
   * List of submodels.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentModel", orphanRemoval = true)
  private Set<ModelSubmodelConnection> submodels = new HashSet<>();

  /**
   * List of connections with parent model (by definition one map can be a
   * submodel of few maps).
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "submodel")
  private Set<SubmodelConnection> parentModels = new HashSet<>();

  /**
   * Default constructor.
   */
  public ModelData() {
  }

  /**
   * Adds {@link Element} to model data.
   * 
   * @param element
   *          element to add
   */
  public void addElement(Element element) {
    element.setModelData(this);
    elements.add(element);
  }

  /**
   * Adds {@link Reaction} to model data.
   * 
   * @param reaction
   *          reaction to add
   */
  public void addReaction(Reaction reaction) {
    reaction.setModelData(this);
    reactions.add(reaction);
  }

  /**
   * Adds {@link Layer} to model data.
   * 
   * @param layer
   *          layer to add
   */
  public void addLayer(Layer layer) {
    layer.setModel(this);
    layers.add(layer);
  }

  /**
   * Adds collection of {@link Element elements} to model data.
   * 
   * @param elements
   *          elements to add
   */
  public void addElements(List<? extends Element> elements) {
    for (Element element : elements) {
      addElement(element);
    }
  }

  /**
   * 
   * @return {@link #id}
   */
  public Integer getId() {
    return id;
  }

  /**
   * 
   * @param id
   *          new {@link #id}
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * 
   * @param elements
   *          new {@link #elements} collection
   */
  public void setElements(Set<Element> elements) {
    this.elements = elements;
  }

  /**
   * 
   * @return {@link #project}
   */
  public Project getProject() {
    return project;
  }

  /**
   * 
   * @param project
   *          new {@link #project}
   */
  public void setProject(Project project) {
    this.project = project;
  }

  /**
   * 
   * @return {@link #creationDate}
   */
  public Calendar getCreationDate() {
    return creationDate;
  }

  /**
   * 
   * @param creationDate
   *          new {@link #creationDate}
   */
  public void setCreationDate(Calendar creationDate) {
    this.creationDate = creationDate;
  }

  /**
   * 
   * @return {@link #layouts}
   */
  public List<Layout> getLayouts() {
    return layouts;
  }

  /**
   * 
   * @param layouts
   *          new {@link #layouts}
   */
  public void setLayouts(List<Layout> layouts) {
    this.layouts = layouts;
  }

  /**
   * Adds layout to model.
   * 
   * @param layout
   *          layout to add
   */
  public void addLayout(Layout layout) {
    layouts.add(layout);
    layout.setModel(this);
  }

  /**
   * 
   * @param width
   *          new {@link #width}
   */
  public void setWidth(int width) {
    setWidth(Double.valueOf(width));
  }

  /**
   * 
   * @param height
   *          new {@link #height}
   */
  public void setHeight(int height) {
    setHeight(Double.valueOf(height));
  }

  /**
   * Adds reactions to model.
   * 
   * @param reactions2
   *          list of reaction to add
   */
  public void addReactions(List<Reaction> reactions2) {
    for (Reaction reaction : reactions2) {
      addReaction(reaction);
    }
  }

  /**
   * Adds collection of {@link Layer layers} to the model data.
   * 
   * @param layers
   *          objets to add
   */
  public void addLayers(Collection<Layer> layers) {
    for (Layer layer : layers) {
      addLayer(layer);
    }
  }

  /**
   * Remove layout from model.
   * 
   * @param dbLayout
   *          layout to remove
   */
  public void removeLayout(Layout dbLayout) {
    List<Layout> toRemove = new ArrayList<Layout>();
    for (Layout layout : layouts) {
      if (layout.getId() == dbLayout.getId()) {
        toRemove.add(layout);
      }
    }

    if (toRemove.size() == 0) {
      logger.warn("Cannot remove layout: " + dbLayout.getId());
    } else {
      layouts.removeAll(toRemove);
    }
  }

  /**
   * Adds {@link ElementGroup} to the model data.
   * 
   * @param elementGroup
   *          object to add
   */
  public void addElementGroup(ElementGroup elementGroup) {
    // for now we ignore this information
  }

  /**
   * Adds {@link BlockDiagram} to the model data.
   * 
   * @param blockDiagram
   *          object to add
   */
  public void addBlockDiagream(BlockDiagram blockDiagram) {
    // for now we ignore this information
  }

  /**
   * Removes {@link Element} from the model.
   * 
   * @param element
   *          element to remove
   */
  public void removeElement(Element element) {
    if (element == null) {
      throw new InvalidArgumentException("Cannot remove null");
    }
    if (!elements.contains(element)) {
      logger.warn("Element doesn't exist in the map: " + element.getElementId());
      return;
    }

    element.setModelData(null);
    elements.remove(element);
  }

  /**
   * Removes reaction from model.
   * 
   * @param reaction
   *          reaction to remove
   */
  public void removeReaction(Reaction reaction) {
    if (!reactions.contains(reaction)) {
      logger.warn("Reaction doesn't exist in the model: " + reaction.getIdReaction());
      return;
    }
    reaction.setModelData(null);
    reactions.remove(reaction);
  }

  /**
   * @return the zoomLevels
   * @see #zoomLevels
   */
  public int getZoomLevels() {
    return zoomLevels;
  }

  /**
   * @param zoomLevels
   *          the zoomLevels to set
   * @see #zoomLevels
   */
  public void setZoomLevels(int zoomLevels) {
    this.zoomLevels = zoomLevels;
  }

  /**
   * @return the tileSize
   * @see #tileSize
   */
  public int getTileSize() {
    return tileSize;
  }

  /**
   * @param tileSize
   *          the tileSize to set
   * @see #tileSize
   */
  public void setTileSize(int tileSize) {
    this.tileSize = tileSize;
  }

  /**
   * @return the idModel
   * @see #idModel
   */
  public String getIdModel() {
    return idModel;
  }

  /**
   * @param idModel
   *          the idModel to set
   * @see #idModel
   */
  public void setIdModel(String idModel) {
    this.idModel = idModel;
  }

  /**
   * @return the model
   * @see #model
   */
  @XmlTransient
  public Model getModel() {
    if (model == null) {
      logger.warn("Model not set in model data.");
    }
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModel(Model model) {
    this.model = model;
  }

  /**
   * @return the elements
   * @see #elements
   */
  public Set<Element> getElements() {
    return elements;
  }

  /**
   * @return the layers
   * @see #layers
   */
  public Set<Layer> getLayers() {
    return layers;
  }

  /**
   * @param layers
   *          the layers to set
   * @see #layers
   */
  public void setLayers(Set<Layer> layers) {
    this.layers = layers;
  }

  /**
   * @return the reactions
   * @see #reactions
   */
  public Set<Reaction> getReactions() {
    return reactions;
  }

  /**
   * @param reactions
   *          the reactions to set
   * @see #reactions
   */
  public void setReactions(Set<Reaction> reactions) {
    this.reactions = reactions;
  }

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * @param height
   *          the height to set
   * @see #height
   */
  public void setHeight(Double height) {
    this.height = height;
  }

  /**
   * @return the notes
   * @see #notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes
   *          the notes to set
   * @see #notes
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /**
   * Adds submodel.
   * 
   * @param submodel
   *          object to add
   */
  public void addSubmodel(ModelSubmodelConnection submodel) {
    this.submodels.add(submodel);
    submodel.setParentModel(this);
  }

  /**
   * Returns collection of submodels.
   * 
   * @return collection of submodels.
   */
  public Collection<ModelSubmodelConnection> getSubmodels() {
    return this.submodels;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the parentModels
   * @see #parentModels
   */
  public Set<SubmodelConnection> getParentModels() {
    return parentModels;
  }

  /**
   * @param parentModels
   *          the parentModels to set
   * @see #parentModels
   */
  public void setParentModels(Set<SubmodelConnection> parentModels) {
    this.parentModels = parentModels;
  }

  /**
   * Adds layout to the list of layouts at a given position.
   * 
   * @param index
   *          position at which new object will be inserted
   * @param layout
   *          object to add
   */
  public void addLayout(int index, Layout layout) {
    layouts.add(index, layout);
    layout.setModel(this);
  }

  public Double getDefaultCenterX() {
    return defaultCenterX;
  }

  public void setDefaultCenterX(Double defaultCenterX) {
    this.defaultCenterX = defaultCenterX;
  }

  public Double getDefaultCenterY() {
    return defaultCenterY;
  }

  public void setDefaultCenterY(Double defaultCenterY) {
    this.defaultCenterY = defaultCenterY;
  }

  public Integer getDefaultZoomLevel() {
    return defaultZoomLevel;
  }

  public void setDefaultZoomLevel(Integer defaultZoomLevel) {
    this.defaultZoomLevel = defaultZoomLevel;
  }

  public Set<SbmlFunction> getFunctions() {
    return functions;
  }

  public Set<SbmlParameter> getParameters() {
    return parameters;
  }

  public void addUnits(Collection<SbmlUnit> units) {
    for (SbmlUnit sbmlUnit : units) {
      addUnit(sbmlUnit);
    }
  }

  public Set<SbmlUnit> getUnits() {
    return units;
  }

  public void addUnit(SbmlUnit unit) {
    this.units.add(unit);
    unit.setModel(this);
  }

  public void addParameters(Collection<SbmlParameter> sbmlParameters) {
    this.parameters.addAll(sbmlParameters);
  }

  public void addParameter(SbmlParameter sbmlParameter) {
    this.parameters.add(sbmlParameter);
  }

  public void addFunction(SbmlFunction sbmlFunction) {
    this.functions.add(sbmlFunction);
    sbmlFunction.setModel(this);
  }

  public void addFunctions(Collection<SbmlFunction> functions) {
    for (SbmlFunction sbmlFunction : functions) {
      addFunction(sbmlFunction);
    }
  }
}