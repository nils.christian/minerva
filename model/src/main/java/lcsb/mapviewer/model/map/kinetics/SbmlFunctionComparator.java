package lcsb.mapviewer.model.map.kinetics;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.comparator.StringListComparator;

public class SbmlFunctionComparator extends Comparator<SbmlFunction> {
  Logger logger = Logger.getLogger(SbmlFunctionComparator.class);

  public SbmlFunctionComparator() {
    super(SbmlFunction.class);
  }

  @Override
  protected int internalCompare(SbmlFunction arg0, SbmlFunction arg1) {
    StringComparator stringComparator = new StringComparator();
    StringListComparator stringListComparator = new StringListComparator();
    if (stringComparator.compare(arg0.getFunctionId(), arg1.getFunctionId()) != 0) {
      logger.debug("functionId different");
      return stringComparator.compare(arg0.getFunctionId(), arg1.getFunctionId());
    }
    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("name different");
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (stringComparator.compare(arg0.getDefinition(), arg1.getDefinition(), true) != 0) {
      logger.debug("definition different");
      logger.debug(arg0.getDefinition());
      logger.debug(arg1.getDefinition());
      return stringComparator.compare(arg0.getDefinition(), arg1.getDefinition());
    }

    if (stringListComparator.compare(arg0.getArguments(), arg1.getArguments()) != 0) {
      logger.debug("arguments different");
      return stringListComparator.compare(arg0.getArguments(), arg1.getArguments());
    }

    return 0;
  }

}
