package lcsb.mapviewer.model.map.species;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link Degraded} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class DegradedComparator extends Comparator<Degraded> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(DegradedComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public DegradedComparator(double epsilon) {
    super(Degraded.class, true);
    this.epsilon = epsilon;
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  /**
   * Default constructor.
   */
  public DegradedComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(Degraded arg0, Degraded arg1) {
    return 0;
  }
}
