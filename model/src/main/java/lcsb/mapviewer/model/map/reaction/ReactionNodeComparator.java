package lcsb.mapviewer.model.map.reaction;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.model.map.species.ElementComparator;

/**
 * This class implements comparator interface for {@link ReactionNode}. It also
 * handles comparison of subclasses of {@link ReactionNode} class.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionNodeComparator extends Comparator<ReactionNode> {
  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ReactionNodeComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  private DoubleComparator doubleComparator;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ReactionNodeComparator(double epsilon) {
    super(ReactionNode.class);
    this.epsilon = epsilon;
    this.doubleComparator = new DoubleComparator(epsilon);
  }

  protected Comparator<?> getParentComparator() {
    return new AbstractNodeComparator(epsilon);
  }

  /**
   * Default constructor.
   */
  public ReactionNodeComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(ReactionNode arg0, ReactionNode arg1) {
    AbstractNodeComparator anComparator = new AbstractNodeComparator(epsilon);
    ElementComparator elementComparator = new ElementComparator(epsilon);

    int result = anComparator.internalCompare(arg0, arg1);
    if (result != 0) {
      return result;
    }

    if (elementComparator.compare(arg0.getElement(), arg1.getElement()) != 0) {
      logger.debug("Element different");
      return elementComparator.compare(arg0.getElement(), arg1.getElement());
    }

    if (doubleComparator.compare(arg0.getStoichiometry(), arg1.getStoichiometry()) != 0) {
      logger.debug("Stoichiometry different");
      return doubleComparator.compare(arg0.getStoichiometry(), arg1.getStoichiometry());
    }

    return 0;
  }
}
