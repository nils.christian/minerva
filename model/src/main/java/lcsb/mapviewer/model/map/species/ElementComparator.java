package lcsb.mapviewer.model.map.species;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.comparator.StringListComparator;
import lcsb.mapviewer.common.comparator.StringSetComparator;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.CompartmentComparator;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnectionComparator;

/**
 * Comparator class used for comparing {@link Element} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ElementComparator extends Comparator<Element> {
  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ElementComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ElementComparator(double epsilon) {
    super(Element.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new ComplexComparator(epsilon));
    addSubClassComparator(new CompartmentComparator(epsilon));
    addSubClassComparator(new SpeciesComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public ElementComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(Element arg0, Element arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    StringComparator stringComparator = new StringComparator();
    ColorComparator colorComparator = new ColorComparator();
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);

    if (stringComparator.compare(arg0.getElementId(), arg1.getElementId()) != 0) {
      logger.debug("ElementId different: " + arg0.getElementId() + ", " + arg1.getElementId());
      return stringComparator.compare(arg0.getElementId(), arg1.getElementId());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      logger.debug("X different: " + arg0.getX() + ", " + arg1.getX());
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      logger.debug("Y different: " + arg0.getY() + ", " + arg1.getY());
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      logger.debug("Width different: " + arg0.getWidth() + ", " + arg1.getWidth());
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      logger.debug("Height different: " + arg0.getHeight() + ", " + arg1.getHeight());
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize()) != 0) {
      logger.debug("Font size different: " + arg0.getFontSize() + ", " + arg1.getFontSize());
      return doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize());
    }

    if (stringComparator.compare(arg0.getVisibilityLevel(), arg1.getVisibilityLevel()) != 0) {
      logger.debug("Visibility level different: " + arg0.getVisibilityLevel() + ", " + arg1.getVisibilityLevel());
      return stringComparator.compare(arg0.getVisibilityLevel(), arg1.getVisibilityLevel());
    }

    if (colorComparator.compare(arg0.getColor(), arg1.getColor()) != 0) {
      logger.debug("Color different: " + arg0.getColor() + ", " + arg1.getColor());
      return colorComparator.compare(arg0.getColor(), arg1.getColor());
    }

    // this should be somehow modified, because it can create a situation where
    // comparison will fall into infinite loop (in cyclic submodels)
    ElementSubmodelConnectionComparator ascc = new ElementSubmodelConnectionComparator(epsilon);
    int status = ascc.compare(arg0.getSubmodel(), arg1.getSubmodel());
    if (status != 0) {
      logger.debug("Element submodel different: " + arg0.getSubmodel() + ", " + arg1.getSubmodel());
      return status;
    }

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("Name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (stringComparator.compare(arg0.getNotes(), arg1.getNotes(), true) != 0) {
      logger.debug("notes different: \n\"" + arg0.getNotes() + "\"\n\"" + arg1.getNotes() + "\"");
      return stringComparator.compare(arg0.getNotes(), arg1.getNotes());
    }

    if (stringComparator.compare(arg0.getSymbol(), arg1.getSymbol()) != 0) {
      logger.debug("symbol different: \"" + arg0.getSymbol() + "\", \"" + arg1.getSymbol() + "\"");
      return stringComparator.compare(arg0.getSymbol(), arg1.getSymbol());
    }

    if (stringComparator.compare(arg0.getFullName(), arg1.getFullName()) != 0) {
      logger.debug("full name different: \"" + arg0.getFullName() + "\", \"" + arg1.getFullName() + "\"");
      return stringComparator.compare(arg0.getFullName(), arg1.getFullName());
    }

    if (stringComparator.compare(arg0.getAbbreviation(), arg1.getAbbreviation()) != 0) {
      logger.debug("Abbreviation different: \"" + arg0.getAbbreviation() + "\", \"" + arg1.getAbbreviation() + "\"");
      return stringComparator.compare(arg0.getAbbreviation(), arg1.getAbbreviation());
    }

    if (stringComparator.compare(arg0.getFormula(), arg1.getFormula()) != 0) {
      logger.debug("formula different: \"" + arg0.getFormula() + "\", \"" + arg1.getFormula() + "\"");
      return stringComparator.compare(arg0.getFormula(), arg1.getFormula());
    }

    StringSetComparator stringSetComparator = new StringSetComparator();
    StringListComparator stringListComparator = new StringListComparator();

    if (stringListComparator.compare(arg0.getSynonyms(), arg1.getSynonyms()) != 0) {
      logger.debug("List of synonyms different");
      return stringListComparator.compare(arg0.getSynonyms(), arg1.getSynonyms());
    }

    if (stringListComparator.compare(arg0.getFormerSymbols(), arg1.getFormerSymbols()) != 0) {
      logger.debug("List of former symbols different");
      return stringListComparator.compare(arg0.getFormerSymbols(), arg1.getFormerSymbols());
    }

    Set<String> hashCode1 = new HashSet<>();
    Set<String> hashCode2 = new HashSet<>();

    if (arg0.getMiriamData().size() != arg1.getMiriamData().size()) {
      logger.debug(
          "different number of annotations: " + arg0.getMiriamData().size() + ", " + arg1.getMiriamData().size());
      return ((Integer) arg0.getMiriamData().size()).compareTo(arg1.getMiriamData().size());
    }

    for (MiriamData md : arg0.getMiriamData()) {
      String hash = md.getRelationType() + "  " + md.getDataType() + "  " + md.getResource();
      hashCode1.add(hash);
    }

    for (MiriamData md : arg1.getMiriamData()) {
      String hash = md.getRelationType() + "  " + md.getDataType() + "  " + md.getResource();
      hashCode2.add(hash);
    }

    if (stringSetComparator.compare(hashCode1, hashCode2) != 0) {
      logger.debug("different annotations: ");
      logger.debug("A:");
      for (String string : hashCode1) {
        logger.debug("|" + string + "|");
      }
      logger.debug("B:");
      for (String string : hashCode2) {
        logger.debug("|" + string + "|");
      }
      logger.debug("--");
      return stringSetComparator.compare(hashCode1, hashCode2);

    }

    return 0;
  }

}
