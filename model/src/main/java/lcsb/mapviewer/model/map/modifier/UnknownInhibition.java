package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines unknown inhibition modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("UNKNOWN_INHIBITION_MODIFIER")
public class UnknownInhibition extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public UnknownInhibition() {
    super();
  }

  /**
   * Constructor that creates unknown inhibition modifier for given and element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public UnknownInhibition(Element element) {
    super(element);
  }

  /**
   * Constructor that creates object with data taken from parameter unknown
   * inhibition.
   * 
   * @param unknownInhibition
   *          object from which data are initialized
   */
  public UnknownInhibition(UnknownInhibition unknownInhibition) {
    super(unknownInhibition);
  }

  @Override
  public UnknownInhibition copy() {
    if (this.getClass() == UnknownInhibition.class) {
      return new UnknownInhibition(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
