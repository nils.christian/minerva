package lcsb.mapviewer.model.map.species.field;

import java.text.DecimalFormat;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("ABSTRACT_SITE_MODIFICATION")
public abstract class AbstractSiteModification extends ModificationResidue {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  /**
   * State in which this modification is.
   */
  @Enumerated(EnumType.STRING)
  protected ModificationState state = null;

  public AbstractSiteModification() {
    super();
  }

  public AbstractSiteModification(AbstractSiteModification mr) {
    super(mr);
    this.state = mr.getState();
  }

  public ModificationState getState() {
    return state;
  }

  public void setState(ModificationState state) {
    this.state = state;
  }

  @Override
  public String toString() {
    DecimalFormat format = new DecimalFormat("#.##");
    String result = getIdModificationResidue() + "," + getName() + "," + getState() + ",Point2D["
        + format.format(getPosition().getX()) + "," + format.format(getPosition().getY()) + "]";
    return result;
  }


}