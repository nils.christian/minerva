package lcsb.mapviewer.model.map.reaction;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.BooleanComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.comparator.StringListComparator;
import lcsb.mapviewer.common.comparator.StringSetComparator;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlKineticsComparator;

/**
 * This class implements comparator interface for {@link Reaction}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionComparator extends Comparator<Reaction> {
  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(ReactionComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ReactionComparator(double epsilon) {
    super(Reaction.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public ReactionComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(Reaction arg0, Reaction arg1) {
    StringComparator stringComparator = new StringComparator();
    BooleanComparator booleanComparator = new BooleanComparator();
    AbstractNodeComparator aNodeComparator = new AbstractNodeComparator(epsilon);

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("Name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (stringComparator.compare(arg0.getNotes().trim(), arg1.getNotes().trim()) != 0) {
      logger.debug("Notes different: " + arg0.getNotes() + ", " + arg1.getNotes());
      return stringComparator.compare(arg0.getNotes(), arg1.getNotes());
    }

    if (stringComparator.compare(arg0.getIdReaction(), arg1.getIdReaction()) != 0) {
      logger.debug("IdReaction different: " + arg0.getIdReaction() + ", " + arg1.getIdReaction());
      return stringComparator.compare(arg0.getIdReaction(), arg1.getIdReaction());
    }

    if (booleanComparator.compare(arg0.isReversible(), arg1.isReversible()) != 0) {
      logger.debug("Reversible different: " + arg0.isReversible() + ", " + arg1.isReversible());
      return booleanComparator.compare(arg0.isReversible(), arg1.isReversible());
    }

    if (stringComparator.compare(arg0.getSymbol(), arg1.getSymbol()) != 0) {
      logger.debug("Symbol different: " + arg0.getSymbol() + ", " + arg1.getSymbol());
      return stringComparator.compare(arg0.getSymbol(), arg1.getSymbol());
    }
    if (stringComparator.compare(arg0.getAbbreviation(), arg1.getAbbreviation()) != 0) {
      logger.debug("abbreviation different: " + arg0.getAbbreviation() + ", " + arg1.getAbbreviation());
      return stringComparator.compare(arg0.getAbbreviation(), arg1.getAbbreviation());
    }
    if (stringComparator.compare(arg0.getFormula(), arg1.getFormula()) != 0) {
      logger.debug("Formula different: " + arg0.getFormula() + ", " + arg1.getFormula());
      return stringComparator.compare(arg0.getFormula(), arg1.getFormula());
    }
    if (stringComparator.compare(arg0.getSubsystem(), arg1.getSubsystem()) != 0) {
      logger.debug("Subsystem different: " + arg0.getSubsystem() + ", " + arg1.getSubsystem());
      return stringComparator.compare(arg0.getSubsystem(), arg1.getSubsystem());
    }
    if (stringComparator.compare(arg0.getGeneProteinReaction(), arg1.getGeneProteinReaction()) != 0) {
      logger.debug(
          "GeneProteinReaction different: " + arg0.getGeneProteinReaction() + ", " + arg1.getGeneProteinReaction());
      return stringComparator.compare(arg0.getGeneProteinReaction(), arg1.getGeneProteinReaction());
    }
    if (stringComparator.compare(arg0.getVisibilityLevel(), arg1.getVisibilityLevel()) != 0) {
      logger.debug("SemanticZoomLevelVisibility different: \"" + arg0.getVisibilityLevel() + "\", \""
          + arg1.getVisibilityLevel() + "\"");
      return stringComparator.compare(arg0.getVisibilityLevel(), arg1.getVisibilityLevel());
    }

    IntegerComparator integerComparator = new IntegerComparator();

    if (integerComparator.compare(arg0.getMechanicalConfidenceScore(), arg1.getMechanicalConfidenceScore()) != 0) {
      logger.debug("MechanicalConfidenceScore different: " + arg0.getMechanicalConfidenceScore() + ", "
          + arg1.getMechanicalConfidenceScore());
      return integerComparator.compare(arg0.getMechanicalConfidenceScore(), arg1.getMechanicalConfidenceScore());
    }

    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    if (doubleComparator.compare(arg0.getLowerBound(), arg1.getLowerBound()) != 0) {
      logger.debug("LowerBound different: " + arg0.getLowerBound() + ", " + arg1.getLowerBound());
      return doubleComparator.compare(arg0.getLowerBound(), arg1.getLowerBound());
    }
    if (doubleComparator.compare(arg0.getUpperBound(), arg1.getUpperBound()) != 0) {
      logger.debug("UpperBound different: " + arg0.getUpperBound() + ", " + arg1.getUpperBound());
      return doubleComparator.compare(arg0.getUpperBound(), arg1.getUpperBound());
    }

    StringListComparator stringListComparator = new StringListComparator();

    if (stringListComparator.compare(arg0.getSynonyms(), arg1.getSynonyms()) != 0) {
      logger.debug("Synonyms dataset different");
      return stringListComparator.compare(arg0.getSynonyms(), arg1.getSynonyms());
    }

    Set<String> hashCode1 = new HashSet<String>();
    Set<String> hashCode2 = new HashSet<String>();

    for (MiriamData md : arg0.getMiriamData()) {
      String hash = md.getRelationType() + "  " + md.getDataType() + "  " + md.getResource();
      hashCode1.add(hash);
    }

    for (MiriamData md : arg1.getMiriamData()) {
      String hash = md.getRelationType() + "  " + md.getDataType() + "  " + md.getResource();
      hashCode2.add(hash);
    }

    StringSetComparator stringSetComparator = new StringSetComparator();
    if (stringSetComparator.compare(hashCode1, hashCode2) != 0) {
      logger.debug("Miriam dataset different");
      return stringSetComparator.compare(hashCode1, hashCode2);
    }

    if (arg0.getNodes().size() != arg1.getNodes().size()) {
      logger.debug("Different number of nodes: " + arg0.getNodes().size() + ", " + arg1.getNodes().size());
      return ((Integer) arg0.getNodes().size()).compareTo(arg1.getNodes().size());
    }

    for (int i = 0; i < arg0.getNodes().size(); i++) {
      AbstractNode node0 = arg0.getNodes().get(i);
      int status = -1;
      for (int j = 0; j < arg1.getNodes().size(); j++) {
        int tmpStatus = aNodeComparator.compare(node0, arg1.getNodes().get(j));
        if (tmpStatus == 0) {
          status = 0;
          break;
        }
      }
      if (status != 0) {
        String debugMessage = "Can't find matching node for: " + node0.getClass().getSimpleName();
        if (node0 instanceof ReactionNode) {
          debugMessage += " [" + ((ReactionNode) node0).getElement().getElementId() + "]";
        }

        logger.debug(debugMessage);
        return status;
      }
    }

    SbmlKineticsComparator kineticsComparator = new SbmlKineticsComparator();
    if (kineticsComparator.compare(arg0.getKinetics(), arg1.getKinetics()) != 0) {
      logger.debug("Kinetics different");
      logger.debug(arg0.getKinetics());
      logger.debug(arg1.getKinetics());
      return kineticsComparator.compare(arg0.getKinetics(), arg1.getKinetics());
    }
    return 0;
  }

}
