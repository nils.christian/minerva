package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner transcription reaction. It must
 * have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("TRANSCRIPTION_REACTION")
public class TranscriptionReaction extends Reaction implements SimpleReactionInterface {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public TranscriptionReaction() {
		super();
	}

	/**
	 * Constructor that copies data from the parameter given in the argument.
	 * 
	 * @param result
	 *          parent reaction from which we copy data
	 */
	public TranscriptionReaction(Reaction result) {
		super(result);
		if (result.getProducts().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to " + getStringType() + ": number of products must be greater than 0");
		}
		if (result.getReactants().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to " + getStringType() + ": number of reactants must be greater than 0");
		}
	}

	@Override
	public String getStringType() {
		return "Transcription";
	}

	@Override
	public ReactionRect getReactionRect() {
		return ReactionRect.RECT_EMPTY;
	}

	@Override
	public TranscriptionReaction copy() {
		if (this.getClass() == TranscriptionReaction.class) {
			return new TranscriptionReaction(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

}
