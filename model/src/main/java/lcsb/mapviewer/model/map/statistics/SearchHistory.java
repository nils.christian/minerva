package lcsb.mapviewer.model.map.statistics;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This object describe single search event entry. It's used for gathering some
 * statistics about system usage.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "history_table")
public class SearchHistory implements Serializable {
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Unique database identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDb", unique = true, nullable = false)
	private int					id;

	/**
	 * Query entered by the user.
	 */
	@Column(nullable = false)
	private String			query;

	/**
	 * Name of the project where user performed search. (It cannot be id, because
	 * event after map is removed we want to store statistics).
	 */
	private String			map;

	/**
	 * When the event occurred.
	 */
	@Column(nullable = false)
	private Calendar		timestamp	= Calendar.getInstance();

	/**
	 * Type of the search event.
	 */
	@Column(nullable = false)
	private SearchType	type;

	/**
	 * Client ip address.
	 */
	@Column(nullable = false)
	private String			ipAddress;

	/**
	 * @return the id
	 * @see #id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the query
	 * @see #query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @param query
	 *          the query to set
	 * @see #query
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * @return the map
	 * @see #map
	 */
	public String getMap() {
		return map;
	}

	/**
	 * @param map
	 *          the map to set
	 * @see #map
	 */
	public void setMap(String map) {
		this.map = map;
	}

	/**
	 * @return the timestamp
	 * @see #timestamp
	 */
	public Calendar getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *          the timestamp to set
	 * @see #timestamp
	 */
	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the type
	 * @see #type
	 */
	public SearchType getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 * @see #type
	 */
	public void setType(SearchType type) {
		this.type = type;
	}

	/**
	 * @return the ipAddress
	 * @see #ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *          the ipAddress to set
	 * @see #ipAddress
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
