package lcsb.mapviewer.model.map.species;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;

/**
 * Structure used for representing information about single element.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("Species")
public abstract class Species extends Element {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(Species.class);

  /**
   * Is the element active.
   */
  private Boolean activity;

  /**
   * Width of the element border.
   */
  private Double lineWidth;

  /**
   * State of the element visualization (should the inside be presented in details
   * or not). TODO this should be transformed into enum
   */
  private String state;

  /**
   * State (free text describing element in some way) of the element is split into
   * {@link #statePrefix prefix} and {@link #stateLabel label}. This value
   * represent the first part of the state.
   */
  private String statePrefix;

  /**
   * State (free text describing element in some way) of the element is split into
   * {@link #statePrefix prefix} and {@link #stateLabel label}. This value
   * represent the second part of the state.
   */
  private String stateLabel;

  /**
   * Complex to which this element belongs to. Null if such complex doesn't exist.
   */
  @ManyToOne
  @Cascade({ CascadeType.ALL })
  @JoinColumn(name = "idcomplexdb")
  private Complex complex;

  /**
   * Initial amount of species.
   */
  private Double initialAmount = null;

  /**
   * Charge of the species.
   */
  private Integer charge = null;

  /**
   * Initial concentration of species.
   */
  private Double initialConcentration = null;

  /**
   * Is only substance units allowed.
   */
  private Boolean onlySubstanceUnits = null;

  /**
   * How many dimers are in this species.
   */
  private int homodimer = 1;

  /**
   * Position on the compartment.
   */
  @Enumerated(EnumType.STRING)
  private PositionToCompartment positionToCompartment = null;

  /**
   * Is species hypothetical.
   */
  private Boolean hypothetical = null;

  /**
   * List of uniprot records which are associated with this species.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "species", orphanRemoval = true)
  private Set<UniprotRecord> uniprots = new HashSet<>();

  private Boolean boundaryCondition;

  private Boolean constant;

  @Enumerated(EnumType.STRING)
  private SbmlUnitType substanceUnits;

  /**
   * Constructor that set element identifier.
   * 
   * @param elementId
   *          {@link Element#elementId}
   */
  public Species(String elementId) {
    this();
    setElementId(elementId);
    setName(elementId);
  }

  /**
   * Empty constructor required by hibernate.
   */
  Species() {
    super();
    activity = false;
    lineWidth = 1.0;
    state = "usual";
  }

  /**
   * Constructor that initializes data with the information given in the
   * parameter.
   * 
   * @param original
   *          object from which data will be initialized
   */
  protected Species(Species original) {
    super(original);
    activity = original.getActivity();
    lineWidth = original.getLineWidth();
    state = original.getState();

    complex = original.getComplex();
    stateLabel = original.getStateLabel();
    statePrefix = original.getStatePrefix();

    initialAmount = original.getInitialAmount();
    charge = original.getCharge();
    initialConcentration = original.getInitialConcentration();
    onlySubstanceUnits = original.getOnlySubstanceUnits();
    homodimer = original.getHomodimer();
    positionToCompartment = original.getPositionToCompartment();
    hypothetical = original.getHypothetical();
    boundaryCondition = original.getBoundaryCondition();
    constant = original.getConstant();
    substanceUnits = original.getSubstanceUnits();

    uniprots = original.getUniprots();

    // don't copy reaction nodes
  }

  /**
   * @return the activity
   * @see #activity
   */
  public Boolean getActivity() {
    return activity;
  }

  /**
   * @param activity
   *          the activity to set
   * @see #activity
   */
  public void setActivity(Boolean activity) {
    this.activity = activity;
  }

  /**
   * @return the lineWidth
   * @see #lineWidth
   */
  public Double getLineWidth() {
    return lineWidth;
  }

  /**
   * @param lineWidth
   *          the lineWidth to set
   * @see #lineWidth
   */
  public void setLineWidth(Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  /**
   * @return the state
   * @see #state
   */
  public String getState() {
    return state;
  }

  /**
   * @param state
   *          the state to set
   * @see #state
   */
  public void setState(String state) {
    this.state = state;
  }

  /**
   * @return the complex
   * @see #complex
   */
  public Complex getComplex() {
    return complex;
  }

  /**
   * @param complex
   *          the complex to set
   * @see #complex
   */
  public void setComplex(Complex complex) {
    this.complex = complex;
  }

  /**
   * @return the statePrefix
   * @see #statePrefix
   */
  public String getStatePrefix() {
    return statePrefix;
  }

  /**
   * @param statePrefix
   *          the statePrefix to set
   * @see #statePrefix
   */
  public void setStatePrefix(String statePrefix) {
    this.statePrefix = statePrefix;
  }

  /**
   * @return the stateLabel
   * @see #stateLabel
   */
  public String getStateLabel() {
    return stateLabel;
  }

  /**
   * @param stateLabel
   *          the stateLabel to set
   * @see #stateLabel
   */
  public void setStateLabel(String stateLabel) {
    this.stateLabel = stateLabel;
  }

  @Override
  public abstract Species copy();

  /**
   * @return the initialAmount
   * @see #initialAmount
   */
  public Double getInitialAmount() {
    return initialAmount;
  }

  /**
   * @param initialAmount
   *          the initialAmount to set
   * @see #initialAmount
   */
  public void setInitialAmount(Double initialAmount) {
    this.initialAmount = initialAmount;
  }

  /**
   * @return the charge
   * @see #charge
   */
  public Integer getCharge() {
    return charge;
  }

  /**
   * @param charge
   *          the charge to set
   * @see #charge
   */
  public void setCharge(Integer charge) {
    this.charge = charge;
  }

  /**
   * @return the initialConcentration
   * @see #initialConcentration
   */
  public Double getInitialConcentration() {
    return initialConcentration;
  }

  /**
   * @param initialConcentration
   *          the initialConcentration to set
   * @see #initialConcentration
   */
  public void setInitialConcentration(Double initialConcentration) {
    this.initialConcentration = initialConcentration;
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean getOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * @param onlySubstanceUnits
   *          the onlySubstanceUnits to set
   * @see #onlySubstanceUnits
   */
  public void setOnlySubstanceUnits(Boolean onlySubstanceUnits) {
    this.onlySubstanceUnits = onlySubstanceUnits;
  }

  /**
   * @return the homodimer
   * @see #homodimer
   */
  public int getHomodimer() {
    return homodimer;
  }

  /**
   * @param homodimer
   *          the homodimer to set
   * @see #homodimer
   */
  public void setHomodimer(int homodimer) {
    this.homodimer = homodimer;
  }

  /**
   * @return the positionToCompartment
   * @see #positionToCompartment
   */
  public PositionToCompartment getPositionToCompartment() {
    return positionToCompartment;
  }

  /**
   * @param positionToCompartment
   *          the positionToCompartment to set
   * @see #positionToCompartment
   */
  public void setPositionToCompartment(PositionToCompartment positionToCompartment) {
    this.positionToCompartment = positionToCompartment;
  }

  /**
   * @return the hypothetical
   * @see #hypothetical
   */
  public Boolean getHypothetical() {
    return hypothetical;
  }

  /**
   * @param uniprots
   *          set of uniprot records for this species
   * @see #uniprots
   */
  public void setUniprots(Set<UniprotRecord> uniprots) {
    this.uniprots = uniprots;
  }

  /**
   * @return the uniprot
   * @see #uniprots
   */
  public Set<UniprotRecord> getUniprots() {
    return uniprots;
  }

  /**
   * @param hypothetical
   *          the hypothetical to set
   * @see #hypothetical
   */
  public void setHypothetical(Boolean hypothetical) {
    this.hypothetical = hypothetical;
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean hasOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * Is species hypothetical or not.
   * 
   * @return <code>true</code> if species is hypothetical, <code>false</code>
   *         otherwise
   */
  public boolean isHypothetical() {
    if (hypothetical == null) {
      return false;
    }
    return hypothetical;
  }

  public boolean isBoundaryCondition() {
    if (boundaryCondition == null) {
      return false;
    }
    return boundaryCondition;
  }

  public Boolean getBoundaryCondition() {
    return boundaryCondition;
  }

  public void setBoundaryCondition(Boolean boundaryCondition) {
    this.boundaryCondition = boundaryCondition;
  }

  public boolean isConstant() {
    if (constant == null) {
      return false;
    }
    return constant;
  }

  public Boolean getConstant() {
    return constant;
  }

  public SbmlUnitType getSubstanceUnits() {
    return substanceUnits;
  }

  public void setConstant(Boolean constant) {
    this.constant = constant;
  }

  public void setSubstanceUnits(SbmlUnitType substanceUnits) {
    this.substanceUnits = substanceUnits;
  }

}
