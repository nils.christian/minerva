package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Compparator of {@link LayerText} class.
 * 
 * @author Piotr Gawron
 * 
 * 
 */
public class LayerTextComparator extends Comparator<LayerText> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayerTextComparator(double epsilon) {
    super(LayerText.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerTextComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(LayerText arg0, LayerText arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    StringComparator stringComparator = new StringComparator();
    ColorComparator colorComparator = new ColorComparator();

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    if (colorComparator.compare(arg0.getColor(), arg1.getColor()) != 0) {
      return colorComparator.compare(arg0.getColor(), arg1.getColor());
    }

    if (stringComparator.compare(arg0.getNotes(), arg1.getNotes()) != 0) {
      return stringComparator.compare(arg0.getNotes(), arg1.getNotes());
    }

    if (doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize()) != 0) {
      return doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize());
    }

    return 0;
  }

}
