package lcsb.mapviewer.model.map.reaction;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;

/**
 * This class implements comparator interface for AbstractNode. It also handles
 * comparison of subclasses of AbstractNode class.
 * 
 * @author Piotr Gawron
 * 
 */
public class AbstractNodeComparator extends Comparator<AbstractNode> {
  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(AbstractNodeComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public AbstractNodeComparator(double epsilon) {
    super(AbstractNode.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new NodeOperatorComparator(epsilon));
    addSubClassComparator(new ReactionNodeComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public AbstractNodeComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(AbstractNode arg0, AbstractNode arg1) {
    PolylineDataComparator pdComparator = new PolylineDataComparator(epsilon);
    if (pdComparator.compare(arg0.getLine(), arg1.getLine()) != 0) {
      logger.debug("Different lines: " + arg0.getLine() + ", " + arg1.getLine());
      return pdComparator.compare(arg0.getLine(), arg1.getLine());
    }
    return 0;
  }

}
