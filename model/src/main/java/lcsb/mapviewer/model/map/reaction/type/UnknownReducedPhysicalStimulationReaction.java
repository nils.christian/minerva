package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner unknown reduced physical
 * stimulation reaction. It must have at least one reactant and one product.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("U_REDUCED_PHYSICAL_STIM")
public class UnknownReducedPhysicalStimulationReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public UnknownReducedPhysicalStimulationReaction() {
		super();
	}

	/**
	 * Constructor that copies data from the parameter given in the argument.
	 * 
	 * @param result
	 *          parent reaction from which we copy data
	 */
	public UnknownReducedPhysicalStimulationReaction(Reaction result) {
		super(result);
		if (result.getProducts().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to " + getStringType() + ": number of products must be greater than 0");
		}
		if (result.getReactants().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to " + getStringType() + ": number of reactants must be greater than 0");
		}
	}

	@Override
	public String getStringType() {
		return "Unknown reduced physical stimulation";
	}

	@Override
	public ReactionRect getReactionRect() {
		return null;
	}

	@Override
	public UnknownReducedPhysicalStimulationReaction copy() {
		if (this.getClass() == UnknownReducedPhysicalStimulationReaction.class) {
			return new UnknownReducedPhysicalStimulationReaction(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

}
