package lcsb.mapviewer.model.map.species;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link Ion} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class IonComparator extends Comparator<Ion> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(IonComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public IonComparator(double epsilon) {
    super(Ion.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public IonComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new ChemicalComparator(epsilon);
  }

  @Override
  protected int internalCompare(Ion arg0, Ion arg1) {
    return 0;
  }
}
