package lcsb.mapviewer.model.map.reaction;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.IndexColumn;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class describes reaction in the {@link Model}. Every reaction consists
 * of set of participants (nodes):
 * <ul>
 * <li>{@link Reactant reactants},</li>
 * <li>{@link Product products},</li>
 * <li>{@link Modifier modifiers}.</li>
 * </ul>
 * These nodes are connected using {@link NodeOperator operators} that define
 * relation among them.<br/>
 * <br/>
 * There are also other fields that describes reaction (like {@link #reversible}
 * , {@link #kineticLaw}, etc.).<br/>
 * <br/>
 * This class is general and shouldn't be used (except during initialization).
 * There are few extensions of this class (in sub-package:
 * {@link lcsb.mapviewer.model.map.reaction.type type}) that defines specific
 * reaction type.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "reaction_table")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "reaction_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_REACTION")
public class Reaction implements BioEntity {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Comparator of reactions that compare them using {@link #idReaction
   * identifier} as a key.
   */
  public static final Comparator<Reaction> ID_COMPARATOR = new Comparator<Reaction>() {
    @Override
    public int compare(Reaction reaction1, Reaction reaction2) {
      return reaction1.getIdReaction().compareTo(reaction2.getIdReaction());
    }
  };

  /**
   * Default class logger.
   */

  private static Logger logger = Logger.getLogger(Reaction.class.getName());

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * List of {@link AbstractNode nodes} in the reaction.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "reaction")
  @OrderBy("id")
  private List<AbstractNode> nodes = new ArrayList<>();

  /**
   * Description of the reaction.
   */
  @Column(name = "notes", columnDefinition = "TEXT")
  private String notes = "";

  /**
   * Identifier of the reaction. Unique in the {@link Model}.
   */
  private String idReaction = "";

  /**
   * Name of the reaction.
   */
  private String name = "";

  /**
   * Is the reaction reversible.
   */
  private boolean reversible = false;

  /**
   * Symbol of the reaction (RECON).
   */
  private String symbol = null;

  /**
   * Abbreviation of the reaction (RECON).
   */
  private String abbreviation = null;

  /**
   * Formula (RECON).
   */
  private String formula = null;

  /**
   * Mechanical confidence score (RECON).
   */
  private Integer mechanicalConfidenceScore = null;

  /**
   * Lower bound (RECON).
   */
  private Double lowerBound = null;

  /**
   * Upper bound (RECON).
   */
  private Double upperBound = null;

  /**
   * Subsystem (RECON).
   */
  private String subsystem = null;

  /**
   * Gene protein reaction (RECON).
   */
  private String geneProteinReaction = null;

  /**
   * Zoom level visibility for semantic zooming.
   */
  private String visibilityLevel = "";

  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection
  @CollectionTable(name = "reaction_synonyms", joinColumns = @JoinColumn(name = "idDb"))
  @Column(name = "synonym")
  @IndexColumn(name = "idx")
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private List<String> synonyms = new ArrayList<>();

  /**
   * Set of annotations (references to external resources) that describe this
   * reaction.
   */
  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "reaction_miriam", joinColumns = {
      @JoinColumn(name = "reaction_id", referencedColumnName = "idDb", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "miriam_id", referencedColumnName = "idDb", nullable = true, updatable = true) })
  private Set<MiriamData> miriamDataSet = new HashSet<>();

  /**
   * ModelData where reaction is placed.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  @Cascade({ CascadeType.ALL })
  @OneToOne
  private SbmlKinetics kinetics;

  /**
   * Default constructor.
   */
  public Reaction() {
  }

  /**
   * Creates reaction based on the information from the reaction passed as
   * parameter.
   * 
   * @param original
   *          original reaction
   */
  public Reaction(Reaction original) {
    // don't copy nodes - reactions refers to this objects (maybe it should be
    // refactored)
    for (AbstractNode node : original.getNodes()) {
      addNode(node);
    }

    notes = original.getNotes();
    idReaction = original.getIdReaction();
    name = original.getName();
    reversible = original.reversible;
    miriamDataSet = new HashSet<>();
    for (MiriamData md : original.getMiriamData()) {
      miriamDataSet.add(new MiriamData(md));
    }

    symbol = original.getSymbol();
    abbreviation = original.getAbbreviation();
    formula = original.getFormula();
    mechanicalConfidenceScore = original.getMechanicalConfidenceScore();
    lowerBound = original.getLowerBound();
    upperBound = original.getUpperBound();
    subsystem = original.getSubsystem();
    geneProteinReaction = original.getGeneProteinReaction();
    synonyms = new ArrayList<>();
    for (String synonym : original.getSynonyms()) {
      synonyms.add(synonym);
    }
    setVisibilityLevel(original.getVisibilityLevel());
    if (original.getKinetics() != null) {
      setKinetics(original.getKinetics().copy());
    }

  }

  public Reaction(String reactionId) {
    setIdReaction(reactionId);
  }

  /**
   * Adds node to the reaction.
   * 
   * @param node
   *          node to add
   */
  public void addNode(AbstractNode node) {
    node.setReaction(this);
    nodes.add(node);
  }

  /**
   * Adds reactant.
   * 
   * @param reactant
   *          reactant to add
   */
  public void addReactant(Reactant reactant) {
    addNode(reactant);
  }

  /**
   * Adds product.
   * 
   * @param product
   *          product to add
   */
  public void addProduct(Product product) {
    addNode(product);
  }

  /**
   * Adds modifier.
   * 
   * @param modifier
   *          modifier to add
   */
  public void addModifier(Modifier modifier) {
    addNode(modifier);
  }

  /**
   * Returns list of reaction products.
   * 
   * @return list of products
   */
  public List<Product> getProducts() {
    List<Product> result = new ArrayList<>();
    for (AbstractNode node : nodes) {
      if (node instanceof Product) {
        result.add((Product) node);
      }
    }
    return result;
  }

  /**
   * Returns list of reaction reactants.
   * 
   * @return list of reactants
   */
  public List<Reactant> getReactants() {
    List<Reactant> result = new ArrayList<>();
    for (AbstractNode node : nodes) {
      if (node instanceof Reactant) {
        result.add((Reactant) node);
      }
    }
    return result;
  }

  /**
   * Returns list of reaction modifiers.
   * 
   * @return list of modifiers
   */
  public List<Modifier> getModifiers() {
    List<Modifier> result = new ArrayList<Modifier>();
    for (AbstractNode node : nodes) {
      if (node instanceof Modifier) {
        result.add((Modifier) node);
      }
    }
    return result;
  }

  /**
   * This method return the minimum distance from the reaction representation (all
   * lines which describe reaction) and point given as a parameter.
   * 
   * @param point
   *          from where we look for the shortest possible distance
   * 
   * @return the shortest possible distance from this reaction and point
   */
  public double getDistanceFromPoint(Point2D point) {
    LineTransformation lt = new LineTransformation();
    double dist = Double.MAX_VALUE;
    List<Line2D> lines = getLines();
    for (Line2D line : lines) {
      double d2 = lt.distBetweenPointAndLineSegment(line, point);
      if (d2 < dist) {
        dist = d2;
      }
    }
    return dist;
  }

  /**
   * Return list of all lines in the reaction that describe it.
   * 
   * @return list of all lines that describes every connection in the rection
   */
  public List<Line2D> getLines() {
    List<Line2D> result = new ArrayList<Line2D>();
    for (AbstractNode node : nodes) {
      result.addAll(node.getLine().getLines());
    }
    return result;
  }

  /**
   * Returns point on the reaction that is as close as possible to the given
   * parameter point.
   * 
   * @param point
   *          parameter point to which results will be as close as possible
   * @return the closest point to the parameter point that is placed on the
   *         reaction
   */
  public Point2D getClosestPointTo(Point2D point) {
    LineTransformation lt = new LineTransformation();
    double dist = Double.MAX_VALUE;
    List<Line2D> lines = getLines();
    Point2D result = null;
    for (Line2D line : lines) {
      double d2 = lt.distBetweenPointAndLineSegment(line, point);
      if (d2 < dist) {
        dist = d2;
        result = lt.closestPointOnSegmentLineToPoint(line, point);
      }
    }
    return result;
  }

  /**
   * Returns short string describing type of the reaction.
   * 
   * @return short string describing type of the reaction
   */
  public String getStringType() {
    return "Generic Reaction";
  }

  @Override
  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  @Override
  public void setVisibilityLevel(String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  /**
   * Returns list of nodes (producst+rectants+modifiers) in the reaction.
   * 
   * @return list of nodes (producst+rectants+modifiers) in the reaction
   */
  public List<ReactionNode> getReactionNodes() {
    List<ReactionNode> result = new ArrayList<ReactionNode>();
    for (AbstractNode node : nodes) {
      if (node instanceof ReactionNode) {
        result.add((ReactionNode) node);
      }
    }
    return result;
  }

  /**
   * Returns central line of the reaction. Central line is the line that separates
   * inputs from outputs.
   * 
   * @return central line of the reaction
   */
  public Line2D getCenterLine() {
    Product product = getProducts().get(0);
    Reactant reactant = getReactants().get(0);

    Point2D startPoint = reactant.getLine().getPoints().get(reactant.getLine().getPoints().size() - 2);
    Point2D endPoint = product.getLine().getPoints().get(1);

    if (this instanceof TwoReactantReactionInterface) {

      NodeOperator firstOperator = null;

      for (NodeOperator operator : getOperators()) {
        if (operator.isReactantOperator() && firstOperator == null) {
          firstOperator = operator;
        }
      }
      if (firstOperator != null) {
        startPoint = firstOperator.getLine().getPoints().get(firstOperator.getLine().getPoints().size() - 2);
      }
    }

    if (this instanceof TwoProductReactionInterface) {

      NodeOperator firstOperator = null;

      for (NodeOperator operator : getOperators()) {
        if (operator.isProductOperator() && firstOperator == null) {
          firstOperator = operator;
        }
      }
      if (firstOperator != null) {
        endPoint = firstOperator.getLine().getPoints().get(firstOperator.getLine().getPoints().size() - 2);
      }
    }

    return new Line2D.Double(startPoint, endPoint);
  }

  /**
   * Removes modifier.
   * 
   * @param modifier
   *          modifier to remove
   */
  public void removeModifier(Modifier modifier) {
    nodes.remove(modifier);
  }

  /**
   * Removes {@link AbstractNode}.
   * 
   * @param node
   *          node to remove
   */
  public void removeNode(AbstractNode node) {
    nodes.remove(node);
  }

  /**
   * Returns {@link ReactionRect} object that defines a small object that should
   * be drawn on the central line of the reaction.
   * 
   * @return {@link ReactionRect} object that defines a small object that should
   *         be drawn on the central line of the reaction
   */
  public ReactionRect getReactionRect() {
    return null;
  }

  /**
   * Returns central point of the reaction - point on central line (see:
   * {@link #getCenterLine()}). This point determines one of the points where
   * modifiers are attached.
   * 
   * @return central point of the reaction
   */
  public Point2D getCenterPoint() {
    Line2D line2D = getCenterLine();
    return new Point2D.Double((line2D.getX1() + line2D.getX2()) / 2, (line2D.getY1() + line2D.getY2()) / 2);
  }

  /**
   * Check if one of the nodes reference to the element.
   * 
   * @param element
   *          element to be checked
   * @return <code>true</code> if element is part of the reaction,
   *         <code>false</code> otherwise
   */
  public boolean containsElement(Element element) {
    for (ReactionNode node : getReactionNodes()) {
      if (node.getElement().equals(element)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Creates copy of the reaction.
   * 
   * @return copy of the reaction
   */
  public Reaction copy() {
    if (this.getClass() == Reaction.class) {
      return new Reaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * Returns list of all {@link NodeOperator operators}.
   * 
   * @return list of all {@link NodeOperator operators}
   */
  public List<NodeOperator> getOperators() {
    List<NodeOperator> result = new ArrayList<NodeOperator>();
    for (AbstractNode node : getNodes()) {
      if (node instanceof NodeOperator) {
        result.add((NodeOperator) node);
      }
    }
    return result;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the nodes
   * @see #nodes
   */
  public List<AbstractNode> getNodes() {
    return nodes;
  }

  /**
   * @param nodes
   *          the nodes to set
   * @see #nodes
   */
  public void setNodes(List<AbstractNode> nodes) {
    this.nodes = nodes;
  }

  @Override
  public String getNotes() {
    return notes;
  }

  @Override
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /**
   * @return the idReaction
   * @see #idReaction
   */
  public String getIdReaction() {
    return idReaction;
  }

  @Override
  public String getElementId() {
    return getIdReaction();
  }

  /**
   * @param idReaction
   *          the idReaction to set
   * @see #idReaction
   */
  public void setIdReaction(String idReaction) {
    this.idReaction = idReaction;
  }

  @Override
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the reversible
   * @see #reversible
   */
  public boolean isReversible() {
    return reversible;
  }

  /**
   * @param reversible
   *          the reversible to set
   * @see #reversible
   */
  public void setReversible(boolean reversible) {
    this.reversible = reversible;
  }

  @Override
  public Set<MiriamData> getMiriamData() {
    return miriamDataSet;
  }

  @Override
  public void addMiriamData(Collection<MiriamData> miriamData) {
    for (MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  /**
   * @return the model
   * @see #model
   */
  public ModelData getModelData() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModelData(ModelData model) {
    this.model = model;
  }

  @Override
  public void addMiriamData(MiriamData md) {
    if (this.miriamDataSet.contains(md)) {
      logger.warn("Miriam data (" + md.getDataType() + ": " + md.getResource() + ") for " + getIdReaction()
          + " already exists. Ignoring...");
    } else {
      this.miriamDataSet.add(md);
    }
  }

  /**
   * Sets model where the reaction is located.
   * 
   * @param model2
   *          model where the reaction is located
   */
  public void setModel(Model model2) {
    this.model = model2.getModelData();
  }

  @XmlTransient
  @Override
  public Model getModel() {
    return model.getModel();
  }

  @Override
  public String getSymbol() {
    return symbol;
  }

  @Override
  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  @Override
  public String getAbbreviation() {
    return abbreviation;
  }

  @Override
  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  @Override
  public String getFormula() {
    return formula;
  }

  @Override
  public void setFormula(String formula) {
    this.formula = formula;
  }

  /**
   * @return the mechanicalConfidenceScore
   * @see #mechanicalConfidenceScore
   */
  public Integer getMechanicalConfidenceScore() {
    return mechanicalConfidenceScore;
  }

  /**
   * @param mechanicalConfidenceScore
   *          the mechanicalConfidenceScore to set
   * @see #mechanicalConfidenceScore
   */
  public void setMechanicalConfidenceScore(Integer mechanicalConfidenceScore) {
    this.mechanicalConfidenceScore = mechanicalConfidenceScore;
  }

  /**
   * @return the lowerBound
   * @see #lowerBound
   */
  public Double getLowerBound() {
    return lowerBound;
  }

  /**
   * @param lowerBound
   *          the lowerBound to set
   * @see #lowerBound
   */
  public void setLowerBound(Double lowerBound) {
    this.lowerBound = lowerBound;
  }

  /**
   * @return the upperBound
   * @see #upperBound
   */
  public Double getUpperBound() {
    return upperBound;
  }

  /**
   * @param upperBound
   *          the upperBound to set
   * @see #upperBound
   */
  public void setUpperBound(Double upperBound) {
    this.upperBound = upperBound;
  }

  /**
   * @return the subsystem
   * @see #subsystem
   */
  public String getSubsystem() {
    return subsystem;
  }

  /**
   * @param subsystem
   *          the subsystem to set
   * @see #subsystem
   */
  public void setSubsystem(String subsystem) {
    this.subsystem = subsystem;
  }

  /**
   * @return the geneProteinReaction
   * @see #geneProteinReaction
   */
  public String getGeneProteinReaction() {
    return geneProteinReaction;
  }

  /**
   * @param geneProteinReaction
   *          the geneProteinReaction to set
   * @see #geneProteinReaction
   */
  public void setGeneProteinReaction(String geneProteinReaction) {
    this.geneProteinReaction = geneProteinReaction;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * Adds synonym to the {@link #synonyms}.
   * 
   * @param synonym
   *          new synonym to add
   */
  public void addSynonym(String synonym) {
    this.synonyms.add(synonym);
  }

  @Override
  public void setVisibilityLevel(Integer zoomLevelVisibility) {
    if (zoomLevelVisibility == null) {
      this.visibilityLevel = null;
    } else {
      this.visibilityLevel = zoomLevelVisibility + "";
    }

  }

  public SbmlKinetics getKinetics() {
    return kinetics;
  }

  public void setKinetics(SbmlKinetics kinetics) {
    this.kinetics = kinetics;
  }
}
