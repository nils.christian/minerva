package lcsb.mapviewer.model.map;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;

/**
 * Type of known miriam annotation.
 * 
 * @author Piotr Gawron
 * 
 */
@SuppressWarnings("unchecked")
public enum MiriamType {

  BiGG_COMPARTMENT("BiGG Compartment", //
      "http://bigg.ucsd.edu/compartments/", //
      new String[] { "urn:miriam:bigg.compartment" }, //
      new Class<?>[] {}, "MIR:00000555"), //

  BiGG_METABOLITE("BiGG Metabolite", //
      "http://bigg.ucsd.edu/universal/metabolites", //
      new String[] { "urn:miriam:bigg.metabolite" }, //
      new Class<?>[] {}, "MIR:00000556"), //

  BiGG_REACTIONS("BiGG Reaction", //
      "http://bigg.ucsd.edu/universal/reactions", //
      new String[] { "urn:miriam:bigg.reaction" }, //
      new Class<?>[] {}, "MIR:00000557"), //

  /**
   * Brenda enzyme database: http://www.brenda-enzymes.org.
   */
  BRENDA("BRENDA", //
      "http://www.brenda-enzymes.org", //
      new String[] { "urn:miriam:brenda" }, //
      new Class<?>[] {}, "MIR:00100101"), //

  /**
   * Chemical Abstracts Service database: http://commonchemistry.org.
   */
  CAS("Chemical Abstracts Service", //
      "http://commonchemistry.org", //
      new String[] { "urn:miriam:cas" }, //
      new Class<?>[] {}, "MIR:00000237"), //

  /**
   * The Carbohydrate-Active Enzyme (CAZy) database: http://www.cazy.org/.
   */
  CAZY("CAZy", //
      "http://commonchemistry.org", //
      new String[] { "urn:miriam:cazy" }, //
      new Class<?>[] {}, "MIR:00000195"), //

  /**
   * Consensus CDS: http://identifiers.org/ccds/.
   */
  CCDS("Consensus CDS", //
      "http://www.ncbi.nlm.nih.gov/CCDS/", //
      new String[] { "urn:miriam:ccds" }, //
      new Class<?>[] {}, "MIR:00000375"), //

  /**
   * Chebi database:
   * <a href = "http://www.ebi.ac.uk/chebi/">http://www.ebi.ac.uk/chebi/</a>.
   */
  CHEBI("Chebi", //
      "http://www.ebi.ac.uk/chebi/", //
      new String[] { "urn:miriam:obo.chebi", "urn:miriam:chebi" }, //
      new Class<?>[] { Chemical.class, Drug.class, }, "MIR:00000002", //
      new Class<?>[] { Chemical.class }), //

  /**
   * ChemSpider database:
   * <a href = "http://www.chemspider.com/">http://www.chemspider.com/</a>.
   */
  CHEMSPIDER("ChemSpider", //
      "http://www.chemspider.com//", //
      new String[] { "urn:miriam:chemspider" }, //
      new Class<?>[] {}, "MIR:00000138"), //

  /**
   * Chembl database: https://www.ebi.ac.uk/chembldb/.
   */
  CHEMBL_COMPOUND("ChEMBL", //
      "https://www.ebi.ac.uk/chembldb/", //
      new String[] { "urn:miriam:chembl.compound" }, //
      new Class<?>[] { Drug.class }, "MIR:00000084"), //

  /**
   * Target in chembl database: https://www.ebi.ac.uk/chembldb/.
   */
  CHEMBL_TARGET("ChEMBL target", //
      "https://www.ebi.ac.uk/chembldb/", //
      new String[] { "urn:miriam:chembl.target" }, //
      new Class<?>[] { Protein.class, Complex.class }, "MIR:00000085"), //

  /**
   * Clusters of Orthologous Groups: https://www.ncbi.nlm.nih.gov/COG/.
   */
  COG("Clusters of Orthologous Groups", //
      "https://www.ncbi.nlm.nih.gov/COG/", //
      new String[] { "urn:miriam:cogs" }, //
      new Class<?>[] { Reaction.class }, "MIR:00000296"), //

  /**
   * Digital Object Identifier: http://www.doi.org/.
   */
  DOI("Digital Object Identifier", //
      "http://www.doi.org/", //
      new String[] { "urn:miriam:doi" }, //
      new Class<?>[] { Reaction.class }, "MIR:00000019"), //

  /**
   * Drugbank database: http://www.drugbank.ca/.
   */
  DRUGBANK("DrugBank", //
      "http://www.drugbank.ca/", //
      new String[] { "urn:miriam:drugbank" }, //
      new Class<?>[] { Drug.class }, "MIR:00000102"), //
  /**
   * Drugbank targets: http://www.drugbank.ca/targets.
   */
  DRUGBANK_TARGET_V4("DrugBank Target v4", //
      "http://www.drugbank.ca/targets", //
      new String[] { "urn:miriam:drugbankv4.target" }, //
      new Class<?>[] {}, "MIR:00000528"), //

  /**
   * Enzyme Nomenclature: http://www.enzyme-database.org/.
   */
  EC("Enzyme Nomenclature", //
      "http://www.enzyme-database.org/", //
      new String[] { "urn:miriam:ec-code" }, //
      new Class<?>[] { Protein.class, Complex.class }, "MIR:00000004"), //

  /**
   * Ensembl: www.ensembl.org.
   */
  ENSEMBL("Ensembl", //
      "www.ensembl.org", //
      new String[] { "urn:miriam:ensembl" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000003"), //

  /**
   * Ensembl Plants: http://plants.ensembl.org/.
   */
  ENSEMBL_PLANTS("Ensembl Plants", //
      "http://plants.ensembl.org/", //
      new String[] { "urn:miriam:ensembl.plant" }, //
      new Class<?>[] {}, "MIR:00000205"), //

  /**
   * Entrez Gene: http://www.ncbi.nlm.nih.gov/gene.
   */
  ENTREZ("Entrez Gene", //
      "http://www.ncbi.nlm.nih.gov/gene", //
      new String[] { "urn:miriam:ncbigene", "urn:miriam:entrez.gene" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000069"), //

  /**
   * Gene Ontology: http://amigo.geneontology.org/amigo.
   */
  GO("Gene Ontology", //
      "http://amigo.geneontology.org/amigo", //
      new String[] { "urn:miriam:obo.go", "urn:miriam:go" }, //
      new Class<?>[] { Phenotype.class, Compartment.class, Complex.class }, "MIR:00000022"), //

  /**
   * HGNC: http://www.genenames.org.
   */
  HGNC("HGNC", //
      "http://www.genenames.org", //
      new String[] { "urn:miriam:hgnc" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000080", //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }), //

  /**
   * HGNC symbol: http://www.genenames.org.
   */
  HGNC_SYMBOL("HGNC Symbol", //
      "http://www.genenames.org", //
      new String[] { "urn:miriam:hgnc.symbol" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000362", //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }), //

  /**
   * HMDB: http://www.hmdb.ca/.
   */
  HMDB("HMDB", //
      "http://www.hmdb.ca/", //
      "urn:miriam:hmdb", //
      new Class<?>[] { Chemical.class, Drug.class, }, "MIR:00000051"), //

  /**
   * InterPro: http://www.ebi.ac.uk/interpro/.
   */
  INTERPRO("InterPro", //
      "http://www.ebi.ac.uk/interpro/", //
      new String[] { "urn:miriam:interpro" }, //
      new Class<?>[] { Protein.class, Complex.class }, "MIR:00000011"), //

  /**
   * KEGG Compound: http://www.genome.jp/kegg/ligand.html.
   */
  KEGG_COMPOUND("Kegg Compound", //
      "http://www.genome.jp/kegg/ligand.html", //
      "urn:miriam:kegg.compound", //
      new Class<?>[] { Chemical.class }, "MIR:00000013"), //

  /**
   * KEGG Genes: http://www.genome.jp/kegg/genes.html.
   */
  KEGG_GENES("Kegg Genes", //
      "http://www.genome.jp/kegg/genes.html", //
      new String[] { "urn:miriam:kegg.genes", "urn:miriam:kegg.genes:hsa" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000070"), //

  /**
   * KEGG Orthology: http://www.genome.jp/kegg/ko.html.
   */
  KEGG_ORTHOLOGY("KEGG Orthology", //
      "http://www.genome.jp/kegg/ko.html", //
      new String[] { "urn:miriam:kegg.orthology" }, //
      new Class<?>[] {}, "MIR:00000116"), //

  /**
   * KEGG Pathway: http://www.genome.jp/kegg/pathway.html.
   */
  KEGG_PATHWAY("Kegg Pathway", //
      "http://www.genome.jp/kegg/pathway.html", //
      "urn:miriam:kegg.pathway", //
      new Class<?>[] { Reaction.class }, "MIR:00000012"), //

  /**
   * KEGG Reaction: http://www.genome.jp/kegg/reaction/.
   */
  KEGG_REACTION("Kegg Reaction", //
      "http://www.genome.jp/kegg/reaction/", //
      "urn:miriam:kegg.reaction", //
      new Class<?>[] { Reaction.class }, "MIR:00000014"), //

  /**
   * MeSH 2012: http://www.nlm.nih.gov/mesh/.
   */
  MESH_2012("MeSH 2012", //
      "http://www.nlm.nih.gov/mesh/", //
      new String[] { "urn:miriam:mesh.2012", "urn:miriam:mesh" }, //
      new Class<?>[] { Phenotype.class, Compartment.class, Complex.class }, "MIR:00000270"), //

  /**
   * miRBase Sequence: http://www.mirbase.org/.
   */
  MI_R_BASE_SEQUENCE("miRBase Sequence Database", //
      "http://www.mirbase.org/", //
      new String[] { "urn:miriam:mirbase" }, //
      new Class<?>[] {}, "MIR:00000078"), //

  /**
   * miRBase Mature Sequence: http://www.mirbase.org/.
   */
  MI_R_BASE_MATURE_SEQUENCE("miRBase Mature Sequence Database", //
      "http://www.mirbase.org/", //
      new String[] { "urn:miriam:mirbase.mature" }, //
      new Class<?>[] {}, "MIR:00000235"), //

  /**
   * miRTaRBase Mature Sequence: http://mirtarbase.mbc.nctu.edu.tw/.
   */
  MIR_TAR_BASE_MATURE_SEQUENCE("miRTarBase Mature Sequence Database", //
      "http://mirtarbase.mbc.nctu.edu.tw/", //
      new String[] { "urn:miriam:mirtarbase" }, //
      new Class<?>[] {}, "MIR:00100739"), //

  /**
   * Mouse Genome Database: http://www.informatics.jax.org/.
   */
  MGD("Mouse Genome Database", //
      "http://www.informatics.jax.org/", //
      new String[] { "urn:miriam:mgd" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000037"), //

  /**
   * Online Mendelian Inheritance in Man: http://omim.org/.
   */
  OMIM("Online Mendelian Inheritance in Man", //
      "http://omim.org/", //
      new String[] { "urn:miriam:omim" }, //
      new Class<?>[] { Phenotype.class }, "MIR:00000016"), //

  /**
   * PANTHER Family: http://www.pantherdb.org/.
   */
  PANTHER("PANTHER Family", //
      "http://www.pantherdb.org/", //
      new String[] { "urn:miriam:panther.family", "urn:miriam:panther" }, //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000060"), //

  /**
   * PDB: http://www.pdbe.org/.
   */
  PDB("Protein Data Bank", //
      "http://www.pdbe.org/", //
      "urn:miriam:pdb", //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000020"),

  /**
   * Protein Family Database: http://pfam.xfam.org/.
   */
  PFAM("Protein Family Database", //
      "http://pfam.xfam.org//", //
      "urn:miriam:pfam", //
      new Class<?>[] {}, "MIR:00000028"), //

  /**
   * PharmGKB Pathways: http://www.pharmgkb.org/.
   */
  PHARM("PharmGKB Pathways", //
      "http://www.pharmgkb.org/", //
      "urn:miriam:pharmgkb.pathways", //
      new Class<?>[] {}, "MIR:00000089"), //

  /**
   * PubChem-compound: http://pubchem.ncbi.nlm.nih.gov/.
   */
  PUBCHEM("PubChem-compound", //
      "http://pubchem.ncbi.nlm.nih.gov/", //
      new String[] { "urn:miriam:pubchem.compound" }, //
      new Class<?>[] { Chemical.class }, "MIR:00000034", //
      new Class<?>[] { Chemical.class }), //

  /**
   * PubChem-substance: http://pubchem.ncbi.nlm.nih.gov/.
   */
  PUBCHEM_SUBSTANCE("PubChem-substance", //
      "http://pubchem.ncbi.nlm.nih.gov/", //
      new String[] { "urn:miriam:pubchem.substance" }, //
      new Class<?>[] { Chemical.class }, "MIR:00000033", //
      new Class<?>[] { Chemical.class }), //

  /**
   * PubMed: http://www.ncbi.nlm.nih.gov/PubMed/.
   */
  PUBMED("PubMed", //
      "http://www.ncbi.nlm.nih.gov/PubMed/", //
      new String[] { "urn:miriam:pubmed" }, //
      new Class<?>[] { BioEntity.class }, "MIR:00000015", //
      new Class<?>[] { Reaction.class }), //

  /**
   * Reactome: http://www.reactome.org/.
   */
  REACTOME("Reactome", //
      "http://www.reactome.org/", //
      "urn:miriam:reactome", //
      new Class<?>[] { Reaction.class }, "MIR:00000018"), //

  /**
   * RefSeq: http://www.ncbi.nlm.nih.gov/projects/RefSeq/.
   */
  REFSEQ("RefSeq", //
      "http://www.ncbi.nlm.nih.gov/projects/RefSeq/", //
      "urn:miriam:refseq", //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000039"),

  /**
   * Rhea: http://www.rhea-db.org/.
   */
  RHEA("Rhea", //
      "http://www.rhea-db.org/", //
      "urn:miriam:rhea", //
      new Class<?>[] { Reaction.class }, "MIR:00000082"),

  /**
   * SGD: http://www.yeastgenome.org/.
   */
  SGD("Saccharomyces Genome Database", //
      "http://www.yeastgenome.org/", //
      "urn:miriam:sgd", //
      new Class<?>[] {}, "MIR:00000023"),

  /**
   * STITCH: http://stitch.embl.de/.
   */
  STITCH("STITCH", //
      "http://stitch.embl.de/", //
      "urn:miriam:stitch", //
      new Class<?>[] {}, "MIR:00100343"),

  /**
   * STRING: http://string-db.org/.
   */
  STRING("STRING", //
      "http://string-db.org/", //
      "urn:miriam:string", //
      new Class<?>[] {}, "MIR:00000265"),

  /**
   * The Arabidopsis Information Resource (TAIR) maintains a database of genetic
   * and molecular biology data for the model higher plant Arabidopsis thaliana.
   * The name of a Locus is unique and used by TAIR, TIGR, and MIPS:
   * http://arabidopsis.org/index.jsp.
   */
  TAIR_LOCUS("TAIR Locus", //
      "http://arabidopsis.org/index.jsp", //
      "urn:miriam:tair.locus", //
      new Class<?>[] {}, "MIR:00000050"),

  /**
   * Taxonomy: http://www.ncbi.nlm.nih.gov/taxonomy/.
   */
  TAXONOMY("Taxonomy", //
      "http://www.ncbi.nlm.nih.gov/taxonomy/", //
      "urn:miriam:taxonomy", //
      new Class<?>[] {}, "MIR:00000006"),
  /**
   * Toxicogenomic: Chemical: http://ctdbase.org/detail.go.
   * http://ctdbase.org/detail.go
   */
  TOXICOGENOMIC_CHEMICAL("Toxicogenomic Chemical", //
      "http://ctdbase.org/", //
      "urn:miriam:ctd.chemical", //
      new Class<?>[] {}, "MIR:00000098"), //

  /**
   * UniGene: http://www.ncbi.nlm.nih.gov/unigene.
   */
  UNIGENE("UniGene", //
      "http://www.ncbi.nlm.nih.gov/unigene", //
      "urn:miriam:unigene", //
      new Class<?>[] {}, "MIR:00000346"),

  /**
   * Uniprot: http://www.uniprot.org/.
   */
  UNIPROT("Uniprot", //
      "http://www.uniprot.org/", //
      "urn:miriam:uniprot", //
      new Class<?>[] { Protein.class, Gene.class, Rna.class }, "MIR:00000005"),

  /**
   * UniProt Isoform: http://www.uniprot.org/.
   */
  UNIPROT_ISOFORM("UniProt Isoform", //
      "http://www.uniprot.org/", //
      "urn:miriam:uniprot.isoform", //
      new Class<?>[] { Protein.class }, "MIR:00000388"),

  /**
   * Unknown reference type...
   */
  UNKNOWN("Unknown", //
      null, //
      new String[] {}, //
      new Class<?>[] {}, null),

  /**
   * Wikidata: https://www.wikidata.org/.
   */
  WIKIDATA("Wikidata", //
      "https://www.wikidata.org/", //
      new String[] { "urn:miriam:wikidata" }, //
      new Class<?>[] {}, "MIR:00000549"), //

  /**
   * WikiPathways: http://www.wikipathways.org/.
   */
  WIKIPATHWAYS("WikiPathways", //
      "http://www.wikipathways.org/", //
      new String[] { "urn:miriam:wikipathways" }, //
      new Class<?>[] {}, "MIR:00000076"), //

  /**
   * Wikipedia: http://en.wikipedia.org/wiki/Main_Page.
   */
  WIKIPEDIA("Wikipedia (English)", //
      "http://en.wikipedia.org/wiki/Main_Page", // /
      new String[] { "urn:miriam:wikipedia.en" }, //
      new Class<?>[] {}, "MIR:00000384"),

  /**
   * WormBase: http://wormbase.bio2rdf.org/fct.
   */
  WORM_BASE("WormBase", //
      "http://wormbase.bio2rdf.org/fct", // /
      new String[] { "urn:miriam:wormbase" }, //
      new Class<?>[] {}, "MIR:00000027");

  /**
   * User friendly name.
   */
  private String commonName;

  /**
   * url to home page of given resource type.
   */
  private String dbHomepage;

  /**
   * Identifier of the database in miriam registry.
   */
  private String registryIdentifier;

  /**
   * Valid URIs to this resource.
   */
  private List<String> uris = new ArrayList<>();

  /**
   * Classes that can be annotated by this resource.
   */
  private List<Class<? extends BioEntity>> validClass = new ArrayList<>();

  /**
   * When class from this list is marked as "require at least one annotation" then
   * annotation of this type is valid.
   */
  private List<Class<? extends BioEntity>> requiredClass = new ArrayList<>();

  /**
   * Constructor that initialize enum object.
   * 
   * @param dbHomePage
   *          home page of the resource {@link #dbHomepage}
   * @param commonName
   *          {@link #commonName}
   * @param uris
   *          {@link #uris}
   * @param classes
   *          {@link #validClass}
   * @param registryIdentifier
   *          {@link #registryIdentifier}
   */
  MiriamType(String commonName, String dbHomePage, String[] uris, Class<?>[] classes, String registryIdentifier) {
    this(commonName, dbHomePage, uris, classes, registryIdentifier, new Class<?>[] {});
  }

  /**
   * Constructor that initialize enum object.
   * 
   * @param dbHomePage
   *          home page of the resource {@link #dbHomepage}
   * @param commonName
   *          {@link #commonName}
   * @param uris
   *          {@link #uris}
   * @param classes
   *          {@link #validClass}
   * @param registryIdentifier
   *          {@link #registryIdentifier}
   * @param requiredClasses
   *          {@link #requiredClasses}
   */
  MiriamType(String commonName, String dbHomePage, String[] uris, Class<?>[] classes, String registryIdentifier,
      Class<?>[] requiredClasses) {
    this.commonName = commonName;
    this.dbHomepage = dbHomePage;
    for (String string : uris) {
      this.uris.add(string);
    }
    for (Class<?> clazz : classes) {
      this.validClass.add((Class<? extends BioEntity>) clazz);
    }
    for (Class<?> clazz : requiredClasses) {
      this.requiredClass.add((Class<? extends BioEntity>) clazz);
    }
    this.registryIdentifier = registryIdentifier;
  }

  /**
   * Constructor that initialize enum object.
   * 
   * @param dbHomePage
   *          home page of the resource {@link #dbHomepage}
   * @param commonName
   *          {@link #commonName}
   * @param uri
   *          one of {@link #uris}
   * @param registryIdentifier
   *          {@link #registryIdentifier}
   * @param classes
   *          {@link #validClass}
   */
  MiriamType(String commonName, String dbHomePage, String uri, Class<?>[] classes, String registryIdentifier) {
    this(commonName, dbHomePage, new String[] { uri }, classes, registryIdentifier);
  }

  /**
   * 
   * @return {@link #commonName}
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * 
   * @return {@link #uris}
   */
  public List<String> getUris() {
    return uris;
  }

  /**
   * 
   * @return {@link #validClass}
   */
  public List<Class<? extends BioEntity>> getValidClass() {
    return validClass;
  }

  /**
   * Returns {@link MiriamType} associated with parameter uri address.
   * 
   * @param uri
   *          uri to check
   * @return {@link MiriamType} for given uri
   */
  public static MiriamType getTypeByUri(String uri) {
    for (MiriamType mt : MiriamType.values()) {
      for (String string : mt.getUris()) {
        if (string.equalsIgnoreCase(uri)) {
          return mt;
        }
      }
    }
    return null;
  }

  /**
   * @return the dbHomepage
   * @see #dbHomepage
   */
  public String getDbHomepage() {
    return dbHomepage;
  }

  /**
   * @return the registryIdentifier
   * @see #registryIdentifier
   */
  public String getRegistryIdentifier() {
    return registryIdentifier;
  }

  /**
   * @return the requiredClass
   * @see #requiredClass
   */
  public List<Class<? extends BioEntity>> getRequiredClass() {
    return requiredClass;
  }

  /**
   * Returns {@link MiriamType} associated with {@link #commonName}.
   * 
   * @param string
   *          {@link #commonName}
   * @return {@link MiriamType} for given name
   */
  public static MiriamType getTypeByCommonName(String string) {
    for (MiriamType mt : MiriamType.values()) {
      if (string.equalsIgnoreCase(mt.getCommonName())) {
        return mt;
      }
    }
    return null;
  }

  /**
   * Transforms identifier into {@link MiriamData}.
   * 
   * @param generalIdentifier
   *          identifier in the format NAME:IDENTIFIER. Where NAME is the name
   *          from {@link MiriamType#commonName} and IDENTIFIER is resource
   *          identifier.
   * @return {@link MiriamData} representing generalIdentifier, when identifier is
   *         invalid InvalidArgumentException is thrown
   */
  public static MiriamData getMiriamDataFromIdentifier(String generalIdentifier) {
    int index = generalIdentifier.indexOf(":");
    if (index < 0) {
      throw new InvalidArgumentException("Identifier doesn't contain type");
    }
    String type = generalIdentifier.substring(0, index);
    String id = generalIdentifier.substring(index + 1);
    for (MiriamType mt : MiriamType.values()) {
      if (mt.getCommonName().equalsIgnoreCase(type)) {
        return new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, mt, id);
      }
    }
    throw new InvalidArgumentException("Unknown miriam type: " + type + " (id: " + id + ")");
  }

  /**
   * Creates {@link MiriamData} from miriam uri.
   * 
   * @param miriamUri
   *          miriam uri defining {@link MiriamData}
   * @return {@link MiriamData} from miriam uri
   */
  public static MiriamData getMiriamByUri(String miriamUri) {
    // this hack is due to CellDesigner issue (CellDesigner incorectly handle
    // with identifiers that have ":" inside resource ":" inside resource with
    // "%3A" and also the last ":"
    miriamUri = miriamUri.replace("%3A", ":");

    String foundUri = "";
    MiriamType foundType = null;

    for (MiriamType type : MiriamType.values()) {
      for (String uri : type.getUris()) {
        if (miriamUri.startsWith(uri + ":")) {
          if (uri.length() > foundUri.length()) {
            foundType = type;
            foundUri = uri;
          }
        }
      }
    }
    if (foundType != null) {
      return new MiriamData(foundType, miriamUri.substring(foundUri.length() + 1));
    }
    throw new InvalidArgumentException("Invalid miriam uri: " + miriamUri);
  }
}
