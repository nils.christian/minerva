package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;


/**
 * This model element is used by cell designer but don't know how... We ignore it
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class BlockDiagram implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	
}
