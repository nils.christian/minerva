package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This structure contains information about Transcription Site for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Gene}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("TRANSCRIPTION_SITE")
public class TranscriptionSite extends AbstractRegionModification implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(TranscriptionSite.class);

  private String direction = null;

  private Boolean active = false;

  /**
   * Default constructor.
   */
  public TranscriptionSite() {

  }

  /**
   * Constructor that initialize object with the data from the parameter.
   * 
   * @param original
   *          object from which we initialize data
   */
  public TranscriptionSite(TranscriptionSite original) {
    super(original);
    this.direction = original.getDirection();
    this.active = original.getActive();
  }

  @Override
  public void update(AbstractRegionModification mr) {
    TranscriptionSite original = (TranscriptionSite) mr;
    if (original.getDirection() != null) {
      this.direction = original.getDirection();
    }
    if (original.getActive() != null) {
      this.active = original.getActive();
    }
  }

  /**
   * Creates a copy of current object.
   * 
   * @return copy of the object
   */
  public TranscriptionSite copy() {
    if (this.getClass() == TranscriptionSite.class) {
      return new TranscriptionSite(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  @Override
  public String toString() {
    return super.toString() + "," + getActive() + "," + getDirection();
  }
}
