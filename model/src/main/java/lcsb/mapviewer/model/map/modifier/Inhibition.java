package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines inhibition modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("INHIBITION_MODIFIER")
public class Inhibition extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public Inhibition() {
    super();
  }

  /**
   * Constructor that creates inhibition modifier for given element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public Inhibition(Element element) {
    super(element);
  }

  /**
   * Constructor that creates object with data taken from parameter inhibition.
   * 
   * @param inhibition
   *          object from which data are initialized
   */
  public Inhibition(Inhibition inhibition) {
    super(inhibition);
  }

  @Override
  public Inhibition copy() {
    if (this.getClass() == Inhibition.class) {
      return new Inhibition(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
