package lcsb.mapviewer.model.map.species;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.IndexColumn;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.SearchIndex;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Abstract class representing objects in the model. Elements are objects that
 * contains graphical representation data for element and metainformation about
 * element (like symbol, name, etc.).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "element_table")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "element_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_ALIAS")
public abstract class Element implements BioEntity, Serializable, SbmlArgument {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default font size for element description.
   */
  private static final double DEFAULT_FONT_SIZE = 12.0;

  /**
   * Maximum length of the valid synonym name.
   */
  private static final int MAX_SYNONYM_LENGTH = 255;

  /**
   * Comparator of elements that takes into consideration size (width*height) of
   * elements.
   */
  public static final Comparator<Element> SIZE_COMPARATOR = new Comparator<Element>() {
    @Override
    public int compare(Element element1, Element element2) {

      double size = element1.getWidth() * element1.getHeight();
      double size2 = element2.getWidth() * element2.getHeight();
      if (size < size2) {
        return 1;
      } else if (size > size2) {
        return -1;
      } else {
        return 0;
      }
    }
  };

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(Element.class);

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * Map model object to which element belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * Submodel that is extension of these element.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
  private ElementSubmodelConnection submodel;

  /**
   * {@link Compartment} where element is located. When element lies outside of
   * every compartment then null value is assigned.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @Cascade({ CascadeType.SAVE_UPDATE })
  private Compartment compartment;

  /**
   * Unique string identifier within one model object (usually imported from
   * external source from which map was imported).
   */
  private String elementId;

  /**
   * X coordinate on the map where element is located (top left corner).
   */
  private Double x;

  /**
   * Y coordinate on the map where element is located (top left corner).
   */
  private Double y;

  /**
   * Width of the element.
   */
  private Double width;

  /**
   * Height of the element.
   */
  private Double height;

  /**
   * Size of the font used for description.
   */
  private Double fontSize;

  /**
   * Color of filling.
   */
  private Color color;

  /**
   * From which level element should be visible.
   *
   * @see #transparencyLevel
   * @see lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params#level
   *      AbstractImageGenerator.Params#level
   */
  private String visibilityLevel = "";

  /**
   * From which level element should be transparent.
   *
   * @see #visibilityLevel
   * @see AbstractImageGenerator.Params#level
   */
  private String transparencyLevel = "";

  /**
   * List of search indexes that describe this element.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "source", orphanRemoval = true)
  private List<SearchIndex> searchIndexes = new ArrayList<>();

  /**
   * Notes describing this element.
   */
  @Column(name = "notes", columnDefinition = "TEXT")
  private String notes = "";

  /**
   * Symbol of the element.
   */
  private String symbol;

  /**
   * Full name of the element.
   */
  private String fullName;

  /**
   * Abbreviation associated with the element.
   */
  private String abbreviation;

  /**
   * Formula associated with the element.
   */
  private String formula;

  /**
   * Short name of the element.
   */
  private String name = "";

  /**
   * Lists of all synonyms used for describing this element.
   */
  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "element_synonyms", joinColumns = @JoinColumn(name = "idDb"))
  @Column(name = "synonym")
  @IndexColumn(name = "idx")
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private List<String> synonyms = new ArrayList<>();

  /**
   * List of former symbols used to describe this element.
   */
  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "element_former_symbols", joinColumns = @JoinColumn(name = "idDb"))
  @Column(name = "symbol")
  @IndexColumn(name = "idx")
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private List<String> formerSymbols = new ArrayList<>();

  /**
   * Set of miriam annotations for this element.
   */
  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "element_miriam", joinColumns = {
      @JoinColumn(name = "element_id", referencedColumnName = "idDb", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "miriam_id", referencedColumnName = "idDb", nullable = true, updatable = true) })
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * Constructor that creates a copy of the element given in the parameter.
   *
   * @param original
   *          original object that will be used for creating copy
   */
  protected Element(Element original) {
    elementId = original.getElementId();
    x = original.getX();
    y = original.getY();
    width = original.getWidth();
    height = original.getHeight();
    fontSize = original.getFontSize();
    color = original.getColor();
    for (SearchIndex searchIndex : original.getSearchIndexes()) {
      searchIndexes.add(searchIndex.copy());
    }
    if (original.getSubmodel() != null) {
      setSubmodel(original.getSubmodel().copy());
    }

    this.notes = original.notes;
    this.symbol = original.symbol;
    this.fullName = original.fullName;
    this.name = original.getName();
    addSynonyms(original.getSynonyms());
    addFormerSymbols(original.getFormerSymbols());

    addMiriamData(original.getMiriamData());
    this.abbreviation = original.getAbbreviation();
    this.formula = original.getFormula();
    setVisibilityLevel(original.getVisibilityLevel());
    setTransparencyLevel(original.getTransparencyLevel());
  }

  /**
   * Adds list of former symbol to the object.
   *
   * @param formerSymbols
   *          list of former symbols to add
   */
  public void addFormerSymbols(List<String> formerSymbols) {
    this.formerSymbols.addAll(formerSymbols);
  }

  /**
   * Empty constructor required by hibernate.
   */
  protected Element() {
    super();
    elementId = "";
    x = 0.0;
    y = 0.0;
    width = 0.0;
    height = 0.0;
    fontSize = DEFAULT_FONT_SIZE;
    color = Color.white;
  }

  /**
   * Parse and set x coordinate from string.
   *
   * @param string
   *          text representing x coordinate
   * @see x
   */
  public void setX(String string) {
    this.x = Double.parseDouble(string);
  }

  /**
   * Parse and set y coordinate from string.
   *
   * @param string
   *          text representing y coordinate
   * @see y
   */
  public void setY(String string) {
    this.y = Double.parseDouble(string);
  }

  /**
   * Parse and set font size.
   *
   * @param string
   *          text representing font size
   * @see fontSize
   */
  public void setFontSize(String string) {
    this.fontSize = Double.parseDouble(string);
  }

  /**
   * Returns x coordinate of the element center.
   *
   * @return x coordinate of the element center
   */
  public double getCenterX() {
    return getX() + getWidth() / 2;
  }

  /**
   * Returns y coordinate of the element center.
   *
   * @return y coordinate of the element center
   */
  public double getCenterY() {
    return getY() + getHeight() / 2;
  }

  /**
   * Returns coordinates of the element center point.
   *
   * @return coordinates of the element center point
   */
  public Point2D getCenter() {
    return new Point2D.Double(getCenterX(), getCenterY());
  }

  /**
   * Methods that increase size of the element by increaseSize. It influence x, y,
   * width and height fields of the element.
   *
   * @param increaseSize
   *          by how many units we want to increase size of the element
   */
  public void increaseBorder(double increaseSize) {
    x -= increaseSize;
    y -= increaseSize;
    this.width += increaseSize * 2;
    this.height += increaseSize * 2;
  }

  /**
   * Parse and set width.
   *
   * @param string
   *          text representing width
   * @see width
   */
  public void setWidth(String string) {
    try {
      width = Double.parseDouble(string);
    } catch (NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width format: " + string, e);
    }
  }

  /**
   * Parse and set height.
   *
   * @param string
   *          text representing height
   * @see height
   */
  public void setHeight(String string) {
    try {
      height = Double.parseDouble(string);
    } catch (Exception e) {
      throw new InvalidArgumentException("Invalid height format: " + string, e);
    }
  }

  /**
   *
   * @param width
   *          the width value to set
   * @see #width
   */
  public void setWidth(int width) {
    this.width = (double) width;
  }

  /**
   *
   * @param height
   *          the height value to set
   * @see #height
   */
  public void setHeight(int height) {
    this.height = (double) height;
  }

  /**
   * This method computes the distance between point and the element. It assumes
   * that element is a rectangle.
   *
   * @param point
   *          point from which we are looking for a distance
   * @return distance between point and this element
   */
  public double getDistanceFromPoint(Point2D point) {
    if (contains(point)) {
      return 0;
    } else if (getX() <= point.getX() && getX() + getWidth() >= point.getX()) {
      return Math.min(Math.abs(point.getY() - getY()), Math.abs(point.getY() - (getY() + getHeight())));
    } else if (getY() <= point.getY() && getY() + getHeight() >= point.getY()) {
      return Math.min(Math.abs(point.getX() - getX()), Math.abs(point.getX() - (getY() + getWidth())));
    } else {
      double distance1 = point.distance(getX(), getY());
      double distance2 = point.distance(getX() + getWidth(), getY());
      double distance3 = point.distance(getX(), getY() + getHeight());
      double distance4 = point.distance(getX() + getWidth(), getY() + getHeight());
      return Math.min(Math.min(distance1, distance2), Math.min(distance3, distance4));
    }
  }

  /**
   * Checks if element contains a point. It assumes that element is a rectangle.
   *
   * @param point
   *          point to check
   * @return <i>true</i> if point belongs to the element, <i>false</i> otherwise
   */
  public boolean contains(Point2D point) {
    if (getX() <= point.getX() && getY() <= point.getY() && getX() + getWidth() >= point.getX()
        && getY() + getHeight() >= point.getY()) {
      return true;
    }
    return false;
  }

  /**
   * Returns a rectangle that determines a rectangle border.
   *
   * @return rectangle border
   */
  public Rectangle2D getBorder() {
    return new Rectangle2D.Double(x, y, width, height);
  }

  /**
   * Returns size of the element in square units.
   *
   * @return size of the element
   */
  public double getSize() {
    return getWidth() * getHeight();
  }

  /**
   * Checks if the element2 (given as parameter) is placed inside this element.
   * This method should be overridden by all inheriting classes - it should
   * properly handle other shapes than rectangle.
   *
   * @param element2
   *          object to be checked
   * @return true if element2 lies in this object, false otherwise
   */
  public boolean contains(Element element2) {
    if (element2 instanceof Species) {
      Point2D p1 = new Point2D.Double(element2.getX(), element2.getY());
      Point2D p2 = new Point2D.Double(element2.getX(), element2.getY() + element2.getHeight());
      Point2D p3 = new Point2D.Double(element2.getX() + element2.getWidth(), element2.getY());
      Point2D p4 = new Point2D.Double(element2.getX() + element2.getWidth(), element2.getY() + element2.getHeight());
      return contains(p1) || contains(p2) || contains(p3) || contains(p4);
    } else if (getX() < element2.getX() && getY() < element2.getY()
        && getX() + getWidth() > element2.getX() + element2.getWidth()
        && getY() + getHeight() > element2.getY() + element2.getHeight()) {
      return true;
    }
    return false;
  }

  /**
   * Makes a copy of the element.
   *
   * @return copy of the element
   */
  public abstract Element copy();

  /**
   *
   * @param x
   *          the x value to set
   * @see x
   */
  public void setX(int x) {
    setX((double) x);
  }

  /**
   *
   * @param y
   *          the y value to set
   * @see y
   */
  public void setY(int y) {
    setY((double) y);
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  /**
   * @param x
   *          the x to set
   * @see #x
   */
  public void setX(Double x) {
    this.x = x;
  }

  /**
   * @return the y
   * @see #y
   */
  public Double getY() {
    return y;
  }

  /**
   * @param y
   *          the y to set
   * @see #y
   */
  public void setY(Double y) {
    this.y = y;
  }

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * @param height
   *          the height to set
   * @see #height
   */
  public void setHeight(Double height) {
    this.height = height;
  }

  /**
   * @return the fontSize
   * @see #fontSize
   */
  public Double getFontSize() {
    return fontSize;
  }

  /**
   * @param fontSize
   *          the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(Double fontSize) {
    this.fontSize = fontSize;
  }

  /**
   * @return the elementId
   * @see #elementId
   */
  public String getElementId() {
    return elementId;
  }

  /**
   * @param elementId
   *          the elementId to set
   * @see #elementId
   */
  public void setElementId(String elementId) {
    this.elementId = elementId;
  }

  @Override
  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  @Override
  public void setVisibilityLevel(String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  @Override
  public void setVisibilityLevel(Integer visibilityLevel) {
    if (visibilityLevel == null) {
      this.visibilityLevel = null;
    } else {
      this.visibilityLevel = visibilityLevel + "";
    }
  }

  /**
   * @return the model
   * @see #model
   */
  public ModelData getModelData() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModelData(ModelData model) {
    this.model = model;
  }

  /**
   * @return the compartment
   * @see #compartment
   */
  public Compartment getCompartment() {
    return compartment;
  }

  /**
   * @param compartment
   *          the compartment to set
   * @see #compartment
   */
  public void setCompartment(Compartment compartment) {
    if (this.compartment != null && compartment != this.compartment) {
      Compartment formerCompartment = this.compartment;
      this.compartment = null;
      formerCompartment.removeElement(this);
    }
    this.compartment = compartment;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * @return the transparencyLevel
   * @see #transparencyLevel
   */
  public String getTransparencyLevel() {
    return transparencyLevel;
  }

  /**
   * @param transparencyLevel
   *          the transparencyLevel to set
   * @see #transparencyLevel
   */
  public void setTransparencyLevel(String transparencyLevel) {
    this.transparencyLevel = transparencyLevel;
  }

  @XmlTransient
  @Override
  public Model getModel() {
    return model.getModel();
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModel(Model model) {
    this.model = model.getModelData();
  }

  /**
   *
   * @param searchIndexes
   *          new {@link #searchIndexes} list
   */
  public void setSearchIndexes(List<SearchIndex> searchIndexes) {
    this.searchIndexes.clear();
    for (SearchIndex searchIndex : searchIndexes) {
      searchIndex.setSource(this);
    }
    this.searchIndexes.addAll(searchIndexes);
  }

  /**
   * @return the searchIndexes
   * @see #searchIndexes
   */
  public List<SearchIndex> getSearchIndexes() {
    return searchIndexes;
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public ElementSubmodelConnection getSubmodel() {
    return submodel;
  }

  /**
   * @param submodel
   *          the submodel to set
   * @see #submodel
   */
  public void setSubmodel(ElementSubmodelConnection submodel) {
    this.submodel = submodel;
    if (submodel != null) {
      this.submodel.setFromElement(this);
    }
  }

  /**
   * @param fontSize
   *          the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(int fontSize) {
    setFontSize((double) fontSize);
  }

  /**
   * Checks if element contains a {@link LayerText}.
   *
   * @param lt
   *          text to check
   * @return <i>true</i> if {@link LayerText} belongs to the element, <i>false</i>
   *         otherwise
   */
  public boolean contains(LayerText lt) {
    return getX() < lt.getX() && getY() < lt.getY() && getX() + getWidth() > lt.getX() + lt.getWidth()
        && getY() + getHeight() > lt.getY() + lt.getHeight();
  }

  /**
   * Adds {@link SearchIndex} to the element.
   *
   * @param searchIndex
   *          search index to add
   * @see #searchIndexes
   */
  public void addSearchIndex(SearchIndex searchIndex) {
    searchIndexes.add(searchIndex);
  }

  /**
   * @return the symbol
   * @see #symbol
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * @param symbol
   *          the symbol to set
   * @see #symbol
   */
  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  /**
   * @return the fullName
   * @see #fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @param fullName
   *          the fullName to set
   * @see #fullName
   */
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  /**
   * @return the abbreviation
   * @see #abbreviation
   */
  public String getAbbreviation() {
    return abbreviation;
  }

  /**
   * @param abbreviation
   *          the abbreviation to set
   * @see #abbreviation
   */
  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  /**
   * @return the formula
   * @see #formula
   */
  public String getFormula() {
    return formula;
  }

  /**
   * @param formula
   *          the formula to set
   * @see #formula
   */
  public void setFormula(String formula) {
    this.formula = formula;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return the formerSymbols
   * @see #formerSymbols
   */
  public List<String> getFormerSymbols() {
    return formerSymbols;
  }

  /**
   * @param formerSymbols
   *          the formerSymbols to set
   * @see #formerSymbols
   */
  public void setFormerSymbols(List<String> formerSymbols) {
    this.formerSymbols = formerSymbols;
  }

  /**
   * @return the miriamData
   * @see #miriamData
   */
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * @param miriamData
   *          the miriamData to set
   * @see #miriamData
   */
  public void setMiriamData(Set<MiriamData> miriamData) {
    this.miriamData = miriamData;
  }

  /**
   * Adds synonyms to the element.
   *
   * @param synonyms
   *          list of synonyms to be added
   */
  public void addSynonyms(List<String> synonyms) {
    for (String string : synonyms) {
      if (string.length() > MAX_SYNONYM_LENGTH) {
        String message = " <Synonym too long. Only " + MAX_SYNONYM_LENGTH + " characters are allowed>";
        this.getSynonyms().add(string.substring(0, MAX_SYNONYM_LENGTH - message.length()) + message);
      } else {
        this.getSynonyms().add(string);
      }
    }

  }

  @Override
  public void addMiriamData(Collection<MiriamData> miriamData) {
    for (MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  @Override
  public void addMiriamData(MiriamData md) {
    if (this.miriamData.contains(md)) {
      logger.warn("Miriam data (" + md.getDataType() + ": " + md.getResource() + ") for " + getElementId()
          + " already exists. Ignoring...");
    } else {
      this.miriamData.add(md);
    }
  }

  @Override
  public String getNotes() {
    return notes;
  }

  @Override
  public void setNotes(String notes) {
    if (notes != null) {
      if (notes.contains("</html>")) {
        throw new InvalidArgumentException("Notes cannot contain html tags...");
      }
    }
    this.notes = notes;
  }

  /**
   * Adds former symbol to the object.
   *
   * @param formerSymbol
   *          former symbol to add
   */
  public void addFormerSymbol(String formerSymbol) {
    formerSymbols.add(formerSymbol);
  }

  /**
   * Adds synonym to object.
   *
   * @param synonym
   *          synonym to be added
   */
  public void addSynonym(String synonym) {
    synonyms.add(synonym);
  }

}