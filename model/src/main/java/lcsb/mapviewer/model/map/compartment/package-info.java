/**
 * Contains structures used for compartment modeling.
 */
package lcsb.mapviewer.model.map.compartment;