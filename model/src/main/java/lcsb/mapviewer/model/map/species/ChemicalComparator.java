package lcsb.mapviewer.model.map.species;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Comparator class used for comparing {@link Chemical} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ChemicalComparator extends Comparator<Chemical> {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ChemicalComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ChemicalComparator(double epsilon) {
    super(Chemical.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new IonComparator(epsilon));
    addSubClassComparator(new SimpleMoleculeComparator(epsilon));
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  /**
   * Default constructor.
   */
  public ChemicalComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(Chemical arg0, Chemical arg1) {
    StringComparator stringComparator = new StringComparator();

    if (stringComparator.compare(arg0.getSmiles(), arg1.getSmiles()) != 0) {
      logger.debug("Smiles different: " + arg0.getSmiles() + ", " + arg1.getSmiles());
      return stringComparator.compare(arg0.getSmiles(), arg1.getSmiles());
    }

    if (stringComparator.compare(arg0.getInChIKey(), arg1.getInChIKey()) != 0) {
      logger.debug("InChIKey different: " + arg0.getInChIKey() + ", " + arg1.getInChIKey());
      return stringComparator.compare(arg0.getInChIKey(), arg1.getInChIKey());
    }

    if (stringComparator.compare(arg0.getInChI(), arg1.getInChI()) != 0) {
      logger.debug("InChI different: " + arg0.getInChI() + ", " + arg1.getInChI());
      return stringComparator.compare(arg0.getInChI(), arg1.getInChI());
    }

    return 0;
  }
}
