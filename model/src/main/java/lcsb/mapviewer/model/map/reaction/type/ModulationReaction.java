package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner modulation reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODULATION_REACTION")
public class ModulationReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Default constructor.
	 */
	public ModulationReaction() {
		super();
	}

	/**
	 * Constructor that copies data from the parameter given in the argument.
	 * 
	 * @param result
	 *          parent reaction from which we copy data
	 */
	public ModulationReaction(Reaction result) {
		super(result);
		if (result.getProducts().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to " + getStringType() + ": number of products must be greater than 0");
		}
		if (result.getReactants().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to " + getStringType() + ": number of reactants must be greater than 0");
		}
	}

	@Override
	public String getStringType() {
		return "Modulation";
	}

	@Override
	public ReactionRect getReactionRect() {
		return null;
	}

	@Override
	public ModulationReaction copy() {
		if (this.getClass() == ModulationReaction.class) {
			return new ModulationReaction(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

}
