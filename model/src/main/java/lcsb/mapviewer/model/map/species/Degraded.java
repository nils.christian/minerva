package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Entity representing degraded element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("DEGRADED_ALIAS")
public class Degraded extends Species {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Empty constructor required by hibernate.
	 */
	Degraded() {
	}

	/**
	 * Constructor that creates a copy of the element given in the parameter.
	 * 
	 * @param original
	 *          original object that will be used for creating copy
	 */
	public Degraded(Degraded original) {
		super(original);
	}

	/**
	 * Default constructor.
	 * 
	 * @param elementId
	 *          uniqe (within model) element identifier
	 */
	public Degraded(String elementId) {
		super.setElementId(elementId);
	}

	@Override
	public Degraded copy() {
		if (this.getClass() == Degraded.class) {
			return new Degraded(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

	@Override
	public String getStringType() {
		return "Degraded";
	}

}
