package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.hibernate.annotations.IndexColumn;

import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Representation of a single SBML function
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "sbml_function")
@org.hibernate.annotations.GenericGenerator(name = "test-increment-strategy", strategy = "increment")
@XmlRootElement
public class SbmlFunction implements Serializable, SbmlArgument {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(SbmlFunction.class);

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  private String functionId;

  private String name;

  @Column(columnDefinition = "TEXT")
  private String definition;

  @ElementCollection
  @CollectionTable(name = "sbml_function_arguments", joinColumns = @JoinColumn(name = "sbml_function_iddb"))
  @IndexColumn(name = "idx")
  @Column(name = "argument_name")
  private List<String> arguments = new ArrayList<>();

  /**
   * Map model object to which function belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * Constructor required by hibernate.
   */
  SbmlFunction() {
    super();
  }

  public SbmlFunction(String functionId) {
    this.functionId = functionId;
  }

  public SbmlFunction(SbmlFunction original) {
    this.functionId = original.getFunctionId();
    this.definition = original.getDefinition();
    this.name = original.getName();
    for (String argument : original.getArguments()) {
      this.addArgument(argument);
    }
  }

  public String getFunctionId() {
    return functionId;
  }

  public void setFunctionId(String functionId) {
    this.functionId = functionId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getArguments() {
    return arguments;
  }

  public void setArguments(List<String> arguments) {
    this.arguments = arguments;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public void addArgument(String argument) {
    arguments.add(argument);
  }

  @Override
  public SbmlFunction copy() {
    return new SbmlFunction(this);
  }

  @Override
  public String getElementId() {
    return getFunctionId();
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(ModelData model) {
    this.model = model;
  }

  public int getId() {
    return id;
  }

}
