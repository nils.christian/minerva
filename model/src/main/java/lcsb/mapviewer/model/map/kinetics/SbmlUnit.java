package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.model.ModelData;

/**
 * Representation of a single SBML unit
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "sbml_unit")
@org.hibernate.annotations.GenericGenerator(name = "test-increment-strategy", strategy = "increment")
@XmlRootElement
public class SbmlUnit implements Serializable {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(SbmlUnit.class);

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  private String unitId;
  private String name;

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "unit", orphanRemoval = true)
  private Set<SbmlUnitTypeFactor> unitTypeFactors = new HashSet<>();

  /**
   * Map model object to which unit belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * Constructor required by hibernate.
   */
  SbmlUnit() {
    super();
  }

  public SbmlUnit(String unitId) {
    this.unitId = unitId;
  }

  public SbmlUnit(SbmlUnit sbmlUnit) {
    this(sbmlUnit.getUnitId());
    this.setName(sbmlUnit.getName());
    for (SbmlUnitTypeFactor factor : sbmlUnit.getUnitTypeFactors()) {
      this.addUnitTypeFactor(factor.copy());
    }
  }

  public String getUnitId() {
    return unitId;
  }

  public void setUnitId(String unitId) {
    this.unitId = unitId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void addUnitTypeFactor(SbmlUnitTypeFactor factor) {
    unitTypeFactors.add(factor);
    factor.setUnit(this);
  }

  public Set<SbmlUnitTypeFactor> getUnitTypeFactors() {
    return unitTypeFactors;
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(ModelData model) {
    this.model = model;
  }

  public SbmlUnit copy() {
    return new SbmlUnit(this);
  }

  public int getId() {
    return id;
  }

}
