package lcsb.mapviewer.model.map.kinetics;

public interface SbmlArgument {

  SbmlArgument copy();

  String getElementId();

}
