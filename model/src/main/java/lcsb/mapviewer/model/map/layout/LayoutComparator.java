package lcsb.mapviewer.model.map.layout;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.BooleanComparator;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * This class implements comparator interface for Layout.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayoutComparator extends Comparator<Layout> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  @SuppressWarnings("unused")
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayoutComparator(double epsilon) {
    super(Layout.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayoutComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(Layout arg0, Layout arg1) {
    StringComparator stringComparator = new StringComparator();
    BooleanComparator booleanComparator = new BooleanComparator();
    if (stringComparator.compare(arg0.getDirectory(), arg1.getDirectory()) != 0) {
      return stringComparator.compare(arg0.getDirectory(), arg1.getDirectory());
    }
    if (stringComparator.compare(arg0.getTitle(), arg1.getTitle()) != 0) {
      return stringComparator.compare(arg0.getTitle(), arg1.getTitle());
    }

    if (booleanComparator.compare(arg0.isHierarchicalView(), arg1.isHierarchicalView()) != 0) {
      return booleanComparator.compare(arg0.isHierarchicalView(), arg1.isHierarchicalView());
    }

    return 0;
  }

}
