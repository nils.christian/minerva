package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This structure contains information about Protein Binding Domain for one of
 * the following {@link Species}:
 * <ul>
 * <li>{@link Rna}</li>
 * <li>{@link AntisenseRna}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("PROTEIN_BINDING_DOMAIN")
public class ProteinBindingDomain extends AbstractRegionModification implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(ProteinBindingDomain.class);

  /**
   * Default constructor.
   */
  public ProteinBindingDomain() {

  }

  /**
   * Constructor that initialize object with the data from the parameter.
   * 
   * @param original
   *          object from which we initialize data
   */
  public ProteinBindingDomain(ProteinBindingDomain original) {
    super(original);
  }

  /**
   * Creates a copy of current object.
   * 
   * @return copy of the object
   */
  public ProteinBindingDomain copy() {
    if (this.getClass() == ProteinBindingDomain.class) {
      return new ProteinBindingDomain(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }
}
