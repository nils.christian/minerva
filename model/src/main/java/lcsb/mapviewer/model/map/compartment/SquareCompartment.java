package lcsb.mapviewer.model.map.compartment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class defines compartment with oval shape.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("Square Compartment")
public class SquareCompartment extends Compartment {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  SquareCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   * 
   * @param original
   *          original compartment where the data was kept
   */
  public SquareCompartment(Compartment original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          identifier of the compartment
   */
  public SquareCompartment(String elementId) {
    super();
    setElementId(elementId);
  }

  @Override
  public SquareCompartment copy() {
    if (this.getClass() == SquareCompartment.class) {
      return new SquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
