package lcsb.mapviewer.model.map.species.field;

import java.awt.geom.Point2D;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class represent modification residue in a {@link Species}.
 * 
 * @author Piotr Gawron
 * 
 */

@Entity
@Table(name = "modification_residue_table")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "modification_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_MODIFICATION_RESIDUE")
@org.hibernate.annotations.GenericGenerator(name = "test-increment-strategy", strategy = "increment")
public class ModificationResidue implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(ModificationResidue.class.getName());

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "iddb", unique = true, nullable = false)
  private int id;

  /**
   * Identifier of the modification. Must be unique in single map model.
   */
  private String idModificationResidue = "";

  /**
   * Name of the modification.
   */
  private String name = "";

  @Column(name = "position")
  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  private Point2D position = null;

  /**
   * Species to which this modification belong to.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "idSpeciesDb", nullable = false)
  private Species species;

  /**
   * Default constructor.
   */
  public ModificationResidue() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   * 
   * @param mr
   *          original object from which data is taken
   */
  public ModificationResidue(ModificationResidue mr) {
    this.idModificationResidue = mr.idModificationResidue;
    this.name = mr.name;
    this.position = mr.position;
  }

  /**
   * @return the idModificationResidue
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the idModificationResidue to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the id
   * @see #idModificationResidue
   */
  public String getIdModificationResidue() {
    return idModificationResidue;
  }

  /**
   * @param idModificationResidue
   *          the id to set
   * @see #idModificationResidue
   */
  public void setIdModificationResidue(String idModificationResidue) {
    this.idModificationResidue = idModificationResidue;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the species
   * @see #species
   */
  public Species getSpecies() {
    return species;
  }

  /**
   * @param species
   *          the species to set
   * @see #species
   */
  public void setSpecies(Species species) {
    this.species = species;
  }

  public Point2D getPosition() {
    return position;
  }

  public void setPosition(Point2D position) {
    this.position = position;
  }

  public ModificationResidue copy() {
    throw new NotImplementedException();
  }

}
