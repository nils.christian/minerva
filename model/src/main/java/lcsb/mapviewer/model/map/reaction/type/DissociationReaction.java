package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a standard CellDesigner dissociation. It must have at
 * least one reactant and two products.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("DISSOCIATION_REACTION")
public class DissociationReaction extends Reaction implements TwoProductReactionInterface {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Default constructor.
	 */
	public DissociationReaction() {
		super();
	}

	/**
	 * Constructor that copies data from the parameter given in the argument.
	 * 
	 * @param result
	 *          parent reaction from which we copy data
	 */
	public DissociationReaction(Reaction result) {
		super(result);
		if (result.getProducts().size() < 2) {
			throw new InvalidArgumentException("Reaction cannot be transformed to dissociation: number of products must be greater than 1");
		}
		if (result.getReactants().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to dissociation: number of reactants must be greater than 0");
		}
	}

	@Override
	public String getStringType() {
		return "Dissociation";
	}

	@Override
	public ReactionRect getReactionRect() {
		return ReactionRect.RECT_EMPTY;
	}

	@Override
	public DissociationReaction copy() {
		if (this.getClass() == DissociationReaction.class) {
			return new DissociationReaction(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

}
