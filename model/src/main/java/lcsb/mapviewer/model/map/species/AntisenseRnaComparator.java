package lcsb.mapviewer.model.map.species;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.StringSetComparator;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Comparator class used for comparing {@link AntisenseRna} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class AntisenseRnaComparator extends Comparator<AntisenseRna> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(AntisenseRnaComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public AntisenseRnaComparator(double epsilon) {
    super(AntisenseRna.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public AntisenseRnaComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(AntisenseRna arg0, AntisenseRna arg1) {
    StringSetComparator stringSetComparator = new StringSetComparator();

    Set<String> set1 = new HashSet<>();
    Set<String> set2 = new HashSet<>();

    for (ModificationResidue region : arg0.getRegions()) {
      set1.add(region.toString());
    }

    for (ModificationResidue region : arg1.getRegions()) {
      set2.add(region.toString());
    }

    if (stringSetComparator.compare(set1, set2) != 0) {
      logger.debug(set1);
      logger.debug(set2);
      logger.debug("Regions mismatch");
      return stringSetComparator.compare(set1, set2);
    }

    return 0;
  }
}
