package lcsb.mapviewer.model.map;

import java.awt.geom.Point2D;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.user.User;

/**
 * Class representing comments on the map.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "feedback")
public class Comment implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Is the feedback removed.
   */
  private boolean deleted = false;

  /**
   * What was the reason of removal.
   */
  private String removeReason = "";

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * Name of the user that insert this feedback.
   */
  private String name;

  /**
   * The model that was commented.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData model;

  /**
   * The model that was commented.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData submodel;

  /**
   * User who gave the feedback (if logged in).
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  /**
   * Email of the user who gave feedback.
   */
  private String email;

  /**
   * Content of the feedback.
   */
  @Column(columnDefinition = "TEXT")
  private String content;

  /**
   * Where on map the feedback is located.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  private Point2D coordinates;

  /**
   * If feedback is located on the element, what is the type of the element.
   */
  private Class<?> tableName;

  /**
   * If feedback is located on the element, what is the identifier of the element.
   */
  private Integer tableId;

  /**
   * @return the tableId
   * @see #tableId
   */
  public Integer getTableId() {
    return tableId;
  }

  /**
   * @param tableId
   *          the tableId to set
   * @see #tableId
   */
  public void setTableId(Integer tableId) {
    this.tableId = tableId;
  }

  /**
   * @return the tableName
   * @see #tableName
   */
  public Class<?> getTableName() {
    return tableName;
  }

  /**
   * @param tableName
   *          the tableName to set
   * @see #tableName
   */
  public void setTableName(Class<?> tableName) {
    this.tableName = tableName;
  }

  /**
   * @return the coordinates
   * @see #coordinates
   */
  public Point2D getCoordinates() {
    return coordinates;
  }

  /**
   * @param coordinates
   *          the coordinates to set
   * @see #coordinates
   */
  public void setCoordinates(Point2D coordinates) {
    this.coordinates = coordinates;
  }

  /**
   * @return the content
   * @see #content
   */
  public String getContent() {
    return content;
  }

  /**
   * @param content
   *          the content to set
   * @see #content
   */
  public void setContent(String content) {
    this.content = content;
  }

  /**
   * @return the email
   * @see #email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email
   *          the email to set
   * @see #email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the user
   * @see #user
   */
  public User getUser() {
    return user;
  }

  /**
   * @param user
   *          the user to set
   * @see #user
   */
  public void setUser(User user) {
    this.user = user;
  }

  /**
   * @return the model
   * @see #model
   */
  public ModelData getModelData() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  public void setModelData(ModelData model) {
    this.model = model;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the removeReason
   * @see #removeReason
   */
  public String getRemoveReason() {
    return removeReason;
  }

  /**
   * @param removeReason
   *          the removeReason to set
   * @see #removeReason
   */
  public void setRemoveReason(String removeReason) {
    this.removeReason = removeReason;
  }

  /**
   * @return the deleted
   * @see #deleted
   */
  public boolean isDeleted() {
    return deleted;
  }

  /**
   * @param deleted
   *          the deleted to set
   * @see #deleted
   */
  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  /**
   * Determines if comment should be visible on the map.
   */
  private boolean pinned = false;

  /**
   * @return the pinned
   * @see #pinned
   */
  public boolean isPinned() {
    return pinned;
  }

  /**
   * @param pinned
   *          the pinned to set
   * @see #pinned
   */
  public void setPinned(boolean pinned) {
    this.pinned = pinned;
  }

  /**
   * Sets model.
   * 
   * @param model2
   *          model to set
   */
  public void setModel(Model model2) {
    this.model = model2.getModelData();
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public ModelData getSubmodelData() {
    return submodel;
  }

  /**
   * @param submodel
   *          the submodel to set
   * @see #submodel
   */
  public void setSubmodelData(ModelData submodel) {
    this.submodel = submodel;
  }

  /**
   * @param submodel
   *          the submodel to set
   * @see #submodel
   */
  public void setSubmodel(Model submodel) {
    this.submodel = submodel.getModelData();
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public Model getSubmodel() {
    return this.submodel.getModel();
  }

}
