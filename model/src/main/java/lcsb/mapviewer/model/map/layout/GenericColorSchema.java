package lcsb.mapviewer.model.map.layout;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Generic color schema used for visualization any data type.
 * 
 * @author Piotr Gawron
 *
 */
public class GenericColorSchema extends ColorSchema {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public GenericColorSchema() {
	}

	/**
	 * Default constructor that copies data from parameter.
	 * 
	 * @param original
	 *          origanl object from which data is copied
	 */
	public GenericColorSchema(GenericColorSchema original) {
		super(original);
	}

	@Override
	public GenericColorSchema copy() {
		if (this.getClass().equals(GenericColorSchema.class)) {
			return new GenericColorSchema(this);
		} else {
			throw new NotImplementedException("Copy mechanism of class " + this.getClass() + " is not implemented");
		}

	}

}
