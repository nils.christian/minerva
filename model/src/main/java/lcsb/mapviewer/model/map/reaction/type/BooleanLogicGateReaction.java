package lcsb.mapviewer.model.map.reaction.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class defines a reaction that as an input contains at least one boolean
 * gate. This type was defined by CellDesigner.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("BOOLEAN_LOGIC_GATE")
public class BooleanLogicGateReaction extends Reaction implements TwoReactantReactionInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public BooleanLogicGateReaction() {
		super();
	}

	/**
	 * Constructor that makes new reaction based on the data from reaction in the
	 * parameter.
	 * 
	 * @param result
	 *          original reaction
	 */
	public BooleanLogicGateReaction(Reaction result) {
		super(result);
		if (result.getProducts().size() < 1) {
			throw new InvalidArgumentException("Reaction cannot be transformed to Boolean logic gate: number of products must be greater than 0");
		}
		if (result.getReactants().size() < 2) {
			throw new InvalidArgumentException("Reaction cannot be transformed to Boolean logic gate: number of reactants must be greater than 1");
		}
	}

	@Override
	public String getStringType() {
		return "Boolean logic gate";
	}

	@Override
	public ReactionRect getReactionRect() {
		return null;
	}

	/**
	 * Prepares a copy of the reaction.
	 * 
	 * @return copy of the reaction
	 */
	public BooleanLogicGateReaction copy() {
		if (this.getClass() == BooleanLogicGateReaction.class) {
			return new BooleanLogicGateReaction(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

}
