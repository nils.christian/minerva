package lcsb.mapviewer.model.map.layout.graphics;

import java.awt.Color;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class describes rectangle in the layer.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class LayerRect implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID = 1L;

	/**
	 * Default class logger.
	 */
	@SuppressWarnings("unused")
	private static Logger			logger					 = Logger.getLogger(LayerRect.class);

	/**
	 * Unique database identifier.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDb", unique = true, nullable = false)
	private int								id;

	/**
	 * Color of the rectangle.
	 */
	private Color							color;

	/**
	 * X coordinate of top left corner.
	 */
	private Double						x								 = 0.0;

	/**
	 * Y coordinate of top left corner.
	 */
	private Double						y								 = 0.0;

	/**
	 * Width of the rectangle.
	 */
	private Double						width						 = 0.0;

	/**
	 * Height of the rectangle.
	 */
	private Double						height					 = 0.0;

	/**
	 * Default constructor.
	 */
	public LayerRect() {

	}

	/**
	 * Constructor that copies data from the parameter.
	 * 
	 * @param layerRect
	 *          from this paramter line data will be copied
	 */
	public LayerRect(LayerRect layerRect) {
		color = layerRect.getColor();
		x = layerRect.getX();
		y = layerRect.getY();
		width = layerRect.getWidth();
		height = layerRect.getHeight();
	}

	/**
	 * Set x from string containing double value.
	 * 
	 * @param param
	 *          x of the line in text format
	 */
	public void setX(String param) {
		try {
			x = Double.parseDouble(param);
		} catch (NumberFormatException e) {
			throw new InvalidArgumentException("Invalid x value: " + param, e);
		}
	}

	/**
	 * Set y from string containing double value.
	 * 
	 * @param param
	 *          y of the line in text format
	 */
	public void setY(String param) {
		try {
			y = Double.parseDouble(param);
		} catch (NumberFormatException e) {
			throw new InvalidArgumentException("Invalid y value: " + param, e);
		}
	}

	/**
	 * Set width from string containing double value.
	 * 
	 * @param param
	 *          width of the line in text format
	 */
	public void setWidth(String param) {
		try {
			width = Double.parseDouble(param);
		} catch (NumberFormatException e) {
			throw new InvalidArgumentException("Invalid width value: " + param, e);
		}
	}

	/**
	 * Set height from string containing double value.
	 * 
	 * @param param
	 *          height of the line in text format
	 */
	public void setHeight(String param) {
		try {
			height = Double.parseDouble(param);
		} catch (NumberFormatException e) {
			throw new InvalidArgumentException("Invalid height value: " + param, e);
		}
	}

	/**
	 * Prepares a copy of the object.
	 * 
	 * @return copy of LayerRect
	 */
	public LayerRect copy() {
		if (this.getClass() == LayerRect.class) {
			return new LayerRect(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

	/**
	 * @return the color
	 * @see #color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *          the color to set
	 * @see #color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the x
	 * @see #x
	 */
	public Double getX() {
		return x;
	}

	/**
	 * @param x
	 *          the x to set
	 * @see #x
	 */
	public void setX(Double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 * @see #y
	 */
	public Double getY() {
		return y;
	}

	/**
	 * @param y
	 *          the y to set
	 * @see #y
	 */
	public void setY(Double y) {
		this.y = y;
	}

	/**
	 * @return the width
	 * @see #width
	 */
	public Double getWidth() {
		return width;
	}

	/**
	 * @param width
	 *          the width to set
	 * @see #width
	 */
	public void setWidth(Double width) {
		this.width = width;
	}

	/**
	 * @return the height
	 * @see #height
	 */
	public Double getHeight() {
		return height;
	}

	/**
	 * @param height
	 *          the height to set
	 * @see #height
	 */
	public void setHeight(Double height) {
		this.height = height;
	}

}
