package lcsb.mapviewer.model.map;

/**
 * Exception thrown when one element has more then one parent.
 * 
 * @author Piotr Gawron
 * 
 */
public class ElementInFewParentsException extends Exception {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Default constructor with a message passed in the argument.
	 * 
	 * @param message
	 *          text message of this exception
	 */
	public ElementInFewParentsException(String message) {
		super(message);
	}
}
