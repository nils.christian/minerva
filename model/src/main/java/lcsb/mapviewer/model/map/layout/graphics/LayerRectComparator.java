package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;

/**
 * Compparator of {@link LayerRect} class.
 * 
 * @author Piotr Gawron
 * 
 * 
 */
public class LayerRectComparator extends Comparator<LayerRect> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayerRectComparator(double epsilon) {
    super(LayerRect.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerRectComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(LayerRect arg0, LayerRect arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    ColorComparator colorComparator = new ColorComparator();

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    if (colorComparator.compare(arg0.getColor(), arg1.getColor()) != 0) {
      return colorComparator.compare(arg0.getColor(), arg1.getColor());
    }

    return 0;
  }

}
