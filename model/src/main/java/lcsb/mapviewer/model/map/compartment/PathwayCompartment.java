package lcsb.mapviewer.model.map.compartment;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Compartment that represents a pathway.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("Pathway Compartment")
public class PathwayCompartment extends Compartment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Empty constructor required by hibernate.
	 */
	PathwayCompartment() {
		super();
	}

	/**
	 * Default constructor.
	 * 
	 * @param elementId
	 *          identifier of the compartment
	 */
	public PathwayCompartment(String elementId) {
		super();
		setElementId(elementId);
	}

	/**
	 * Constructor that intialize object with the data given in the parameter.
	 * 
	 * @param original
	 *          data for initialization
	 */
	public PathwayCompartment(PathwayCompartment original) {
		super(original);
	}

	@Override
	public PathwayCompartment copy() {
		if (this.getClass() == PathwayCompartment.class) {
			return new PathwayCompartment(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}
	}

}
