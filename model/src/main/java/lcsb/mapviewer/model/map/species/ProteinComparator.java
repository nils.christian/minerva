package lcsb.mapviewer.model.map.species;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.comparator.StringSetComparator;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Comparator class used for comparing {@link Protein} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ProteinComparator extends Comparator<Protein> {

  /**
   * Default class logger.
   */
  private static Logger logger = Logger.getLogger(ProteinComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ProteinComparator(double epsilon) {
    super(Protein.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new GenericProteinComparator(epsilon));
    addSubClassComparator(new IonChannelProteinComparator(epsilon));
    addSubClassComparator(new ReceptorProteinComparator(epsilon));
    addSubClassComparator(new TruncatedProteinComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public ProteinComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(Protein arg0, Protein arg1) {
    StringComparator stringComparator = new StringComparator();
    StringSetComparator stringSetComparator = new StringSetComparator();

    if (stringComparator.compare(arg0.getStructuralState(), arg1.getStructuralState()) != 0) {
      logger.debug("structural state different: " + arg0.getStructuralState() + ", " + arg1.getStructuralState());
      return stringComparator.compare(arg0.getStructuralState(), arg1.getStructuralState());
    }

    Set<String> set1 = new HashSet<>();
    Set<String> set2 = new HashSet<>();

    for (ModificationResidue region : arg0.getModificationResidues()) {
      set1.add(region.toString());
    }

    for (ModificationResidue region : arg1.getModificationResidues()) {
      set2.add(region.toString());
    }

    if (stringSetComparator.compare(set1, set2) != 0) {
      logger.debug(set1);
      logger.debug(set2);
      logger.debug("modification residues different");
      return stringSetComparator.compare(set1, set2);
    }

    return 0;
  }
}
