package lcsb.mapviewer.model.map.reaction;

import org.apache.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * This class implements comparator interface for {@link NodeOperator}. It also
 * handles comparison of subclasses of {@link NodeOperator} class.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public class NodeOperatorComparator extends Comparator<NodeOperator> {
  /**
   * Default class logger.
   */
  private Logger logger = Logger.getLogger(NodeOperatorComparator.class);

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public NodeOperatorComparator(double epsilon) {
    super(NodeOperator.class);
    this.epsilon = epsilon;
  }

  protected Comparator<?> getParentComparator() {
    return new AbstractNodeComparator(epsilon);
  }

  /**
   * Default constructor.
   */
  public NodeOperatorComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(NodeOperator arg0, NodeOperator arg1) {
    AbstractNodeComparator anComparator = new AbstractNodeComparator(epsilon);

    IntegerComparator integerComparator = new IntegerComparator();

    if (integerComparator.compare(arg0.getInputs().size(), arg1.getInputs().size()) != 0) {
      logger.debug("Different input size: " + arg0.getInputs().size() + ", " + arg1.getInputs().size());
      return integerComparator.compare(arg0.getInputs().size(), arg1.getInputs().size());
    }
    if (integerComparator.compare(arg0.getOutputs().size(), arg1.getOutputs().size()) != 0) {
      logger.debug("Different output size: " + arg0.getOutputs().size() + ", " + arg1.getOutputs().size());
      return integerComparator.compare(arg0.getOutputs().size(), arg1.getOutputs().size());
    }

    for (int i = 0; i < arg0.getInputs().size(); i++) {
      AbstractNode node1 = arg0.getInputs().get(i);
      int status = -1;
      for (int j = 0; j < arg1.getInputs().size(); j++) {
        AbstractNode node2 = arg1.getInputs().get(j);
        int tmpStatus = anComparator.compare(node1, node2);
        if (tmpStatus == 0) {
          status = 0;
          break;
        }
      }
      if (status != 0) {
        logger.debug("Can't find match in inputs.");
        return status;
      }
    }
    for (int i = 0; i < arg0.getOutputs().size(); i++) {
      AbstractNode node1 = arg0.getOutputs().get(i);
      int status = -1;
      for (int j = 0; j < arg1.getOutputs().size(); j++) {
        AbstractNode node2 = arg1.getOutputs().get(j);
        int tmpStatus = anComparator.compare(node1, node2);
        if (tmpStatus == 0) {
          status = 0;
          break;
        }
      }
      if (status != 0) {
        logger.debug("Can't find match in outputs.");
        return status;
      }
    }

    return 0;
  }

}
