package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class describing single gene variation.
 * 
 * @author Piotr Gawron
 *
 */
public class GeneVariation implements Serializable {

	/**
	 * 
	 */
	private static final long		serialVersionUID = 1L;

	/**
	 * Variation position in the genome.
	 */
	private Long								position;

	/**
	 * Original dna (from the reference genome).
	 */
	private String							originalDna;

	/**
	 * Alternative dna (suggested by the variant).
	 */
	private String							modifiedDna;

	/**
	 * Contig where variant was observed.
	 */
	private String							contig;

	private String							allelFrequency;
	private String							variantIdentifier;

	/**
	 * Reference genome type.
	 */
	private ReferenceGenomeType	referenceGenomeType;

	/**
	 * Version of the reference genome.
	 */
	private String							referenceGenomeVersion;

	/**
	 * List of references connected to this variant.
	 */
	private List<MiriamData>		references			 = new ArrayList<>();

	/**
	 * Constructor that creates copy of the {@link GeneVariation}.
	 * 
	 * @param original
	 *          original {@link GeneVariation}
	 */
	protected GeneVariation(GeneVariation original) {
		this.setPosition(original.getPosition());
		this.setOriginalDna(original.getOriginalDna());
		this.setModifiedDna(original.getModifiedDna());
		this.setReferenceGenomeType(original.getReferenceGenomeType());
		this.setReferenceGenomeVersion(original.getReferenceGenomeVersion());
		this.addReferences(original.getReferences());
		this.setContig(original.getContig());
		this.setAllelFrequency(original.getAllelFrequency());
		this.setVariantIdentifier(original.getVariantIdentifier());
	}

	/**
	 * Default constructor.
	 */
	public GeneVariation() {
	}

	/**
	 * Adds references.
	 * 
	 * @param references
	 *          references to add
	 * @see #references
	 */
	private void addReferences(Collection<MiriamData> references) {
		for (MiriamData reference : references) {
			addReference(reference);
		}
	}

	/**
	 * @return the position
	 * @see #position
	 */
	public Long getPosition() {
		return position;
	}

	/**
	 * @param position
	 *          the position to set
	 * @see #position
	 */
	public void setPosition(Long position) {
		this.position = position;
	}

	/**
	 * @return the originalDna
	 * @see #originalDna
	 */
	public String getOriginalDna() {
		return originalDna;
	}

	/**
	 * @param originalDna
	 *          the originalDna to set
	 * @see #originalDna
	 */
	public void setOriginalDna(String originalDna) {
		this.originalDna = originalDna;
	}

	/**
	 * @return the modifiedDna
	 * @see #modifiedDna
	 */
	public String getModifiedDna() {
		return modifiedDna;
	}

	/**
	 * @param modifiedDna
	 *          the modifiedDna to set
	 * @see #modifiedDna
	 */
	public void setModifiedDna(String modifiedDna) {
		this.modifiedDna = modifiedDna;
	}

	/**
	 * @return the referenceGenomeType
	 * @see #referenceGenomeType
	 */
	public ReferenceGenomeType getReferenceGenomeType() {
		return referenceGenomeType;
	}

	/**
	 * @param referenceGenomeType
	 *          the referenceGenomeType to set
	 * @see #referenceGenomeType
	 */
	public void setReferenceGenomeType(ReferenceGenomeType referenceGenomeType) {
		this.referenceGenomeType = referenceGenomeType;
	}

	/**
	 * @return the references
	 * @see #references
	 */
	public List<MiriamData> getReferences() {
		return references;
	}

	/**
	 * @return the referenceGenomeVersion
	 * @see #referenceGenomeVersion
	 */
	public String getReferenceGenomeVersion() {
		return referenceGenomeVersion;
	}

	/**
	 * @param referenceGenomeVersion
	 *          the referenceGenomeVersion to set
	 * @see #referenceGenomeVersion
	 */
	public void setReferenceGenomeVersion(String referenceGenomeVersion) {
		this.referenceGenomeVersion = referenceGenomeVersion;
	}

	/**
	 * Adds reference.
	 * 
	 * @param reference
	 *          reference to add
	 * @see #references
	 */
	public void addReference(MiriamData reference) {
		this.references.add(reference);
	}

	/**
	 * Creates copy of the object.
	 * 
	 * @return copy of the object
	 */
	public GeneVariation copy() {
		if (this.getClass().equals(GeneVariation.class)) {
			return new GeneVariation(this);
		} else {
			throw new NotImplementedException("Copy method not implemented for class: " + this.getClass());
		}
	}

	/**
	 * @return the contig
	 * @see #contig
	 */
	public String getContig() {
		return contig;
	}

	/**
	 * @param contig
	 *          the contig to set
	 * @see #contig
	 */
	public void setContig(String contig) {
		this.contig = contig;
	}

	/**
	 * Sets {@link #position}.
	 * 
	 * @param position
	 *          new position value
	 */
	public void setPosition(int position) {
		this.position = (long) position;
	}

	/**
	 * @return the allelFrequency
	 * @see #allelFrequency
	 */
	public String getAllelFrequency() {
		return allelFrequency;
	}

	/**
	 * @param allelFrequency
	 *          the allelFrequency to set
	 * @see #allelFrequency
	 */
	public void setAllelFrequency(String allelFrequency) {
		this.allelFrequency = allelFrequency;
	}

	/**
	 * @return the variantIdentifier
	 * @see #variantIdentifier
	 */
	public String getVariantIdentifier() {
		return variantIdentifier;
	}

	/**
	 * @param variantIdentifier
	 *          the variantIdentifier to set
	 * @see #variantIdentifier
	 */
	public void setVariantIdentifier(String variantIdentifier) {
		this.variantIdentifier = variantIdentifier;
	}

}
