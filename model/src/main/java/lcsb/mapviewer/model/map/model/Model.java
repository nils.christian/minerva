package lcsb.mapviewer.model.map.model;

import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.Layout;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This interface defines functionality that the model container class should
 * implement to access data efficiently. It shouldn't be implemented by data
 * model.
 * 
 * @author Piotr Gawron
 * 
 */
public interface Model {

  Comparator<? super Model> ID_COMPARATOR = new Comparator<Model>() {

    @Override
    public int compare(Model o1, Model o2) {
      return o1.getId() - o2.getId();
    }
  };

  /**
   * Adds element to the model.
   * 
   * @param element
   *          element to add
   */
  void addElement(Element element);

  /**
   * 
   * @return model width
   */
  Double getWidth();

  /**
   * Sets model width.
   * 
   * @param width
   *          new model width
   */
  void setWidth(Double width);

  /**
   * 
   * @return model height
   */
  Double getHeight();

  /**
   * Sets model height.
   * 
   * @param height
   *          new model height
   */
  void setHeight(Double height);

  /**
   * Sets model width.
   * 
   * @param text
   *          new model width
   */
  void setWidth(String text);

  /**
   * Sets model height.
   * 
   * @param text
   *          new model height
   */
  void setHeight(String text);

  /**
   * Returns set of all elements.
   * 
   * @return set of all elements
   */
  Set<Element> getElements();

  /**
   * Returns element with the given element identifier ({@link Element#elementId}
   * ).
   * 
   * @param idElement
   *          element identifier
   * @param <T>
   *          type of the object to be returned
   * @return {@link Element} with the given id
   */
  <T extends Element> T getElementByElementId(String idElement);

  /**
   * Adds reaction to the model.
   * 
   * @param reaction
   *          reaction to add
   */
  void addReaction(Reaction reaction);

  /**
   * Returns set of reactions.
   * 
   * @return set of reaction in the model
   */
  Set<Reaction> getReactions();

  /**
   * @return list of compartments
   */
  List<Compartment> getCompartments();

  /**
   * Adds layer to the model.
   * 
   * @param layer
   *          object to add
   */
  void addLayer(Layer layer);

  /**
   * 
   * @return set of layers
   */
  Set<Layer> getLayers();

  /**
   * Adds list of elements into model.
   * 
   * @param elements
   *          list of elements
   */
  void addElements(List<? extends Element> elements);

  /**
   * Sets new short description of the model.
   * 
   * @param notes
   *          new short description
   */
  void setNotes(String notes);

  /**
   * 
   * @return short description of the model
   */
  String getNotes();

  /**
   * Returns reaction with the id given in the parameter.
   * 
   * @param idReaction
   *          reaction identifier ({@link Reaction#idReaction})
   * @return reaction with the id given in the parameter
   */
  Reaction getReactionByReactionId(String idReaction);

  /**
   * Adds set of layers to the model.
   * 
   * @param layers
   *          object to add
   */
  void addLayers(Collection<Layer> layers);

  /**
   * Adds {@link ElementGroup} to the model.
   * 
   * @param elementGroup
   *          object to add
   */
  void addElementGroup(ElementGroup elementGroup);

  /**
   * Adds {@link BlockDiagram} to the model.
   * 
   * @param blockDiagram
   *          object to add
   */
  void addBlockDiagream(BlockDiagram blockDiagram);

  /**
   * @param idModel
   *          the idModel to set
   * @see Model#idModel
   */
  void setIdModel(String idModel);

  /**
   * @return the idModel
   * @see Model#idModel
   */
  String getIdModel();

  /**
   * @param tileSize
   *          the tileSize to set
   * @see ModelData#tileSize
   */
  void setTileSize(int tileSize);

  /**
   * @return the tileSize
   * @see ModelData#tileSize
   */
  int getTileSize();

  /**
   * @param zoomLevels
   *          the zoomLevels to set
   * @see ModelData#zoomLevels
   */
  void setZoomLevels(int zoomLevels);

  /**
   * @return the zoomLevels
   * @see ModelData#zoomLevels
   */
  int getZoomLevels();

  /**
   * Removes reaction from model.
   * 
   * @param reaction
   *          reaction to remove
   */
  void removeReaction(Reaction reaction);

  /**
   * Removes {@link Element} from the model.
   * 
   * @param element
   *          element to remove
   */
  void removeElement(Element element);

  /**
   * Returns list of reactions sorted by reaction id.
   * 
   * @return list of reactions sorted by reaction id
   */
  List<Reaction> getSortedReactions();

  /**
   * Return list of elements sorted by the size.
   * 
   * @return list of elements sorted by the size
   */
  List<Element> getSortedSpeciesList();

  /**
   * Returns collection of all {@link Species} excluding {@link Complex}.
   * 
   * @return collection of all {@link Species} excluding {@link Complex}.
   */
  Collection<Species> getNotComplexSpeciesList();

  /**
   * Returns list of all {@link Species} in the model.
   * 
   * @return list of all {@link Species} in the model
   */
  List<Species> getSpeciesList();

  /**
   * Returns collection of {@link Complex}.
   * 
   * @return collection of {@link Complex}
   */
  Collection<Complex> getComplexList();

  /**
   * Adds reactions to model.
   * 
   * @param reactions2
   *          list of reaction to add
   */
  void addReactions(List<Reaction> reactions2);

  /**
   * Returns list of elements annotated by the {@link MiriamData}.
   * 
   * @param miriamData
   *          {@link MiriamData}
   * @return list of elements
   */
  Set<BioEntity> getElementsByAnnotation(MiriamData miriamData);

  /**
   * Returns list of elements with given name.
   * 
   * @param name
   *          name of the element
   * @return list of elements with given name
   */
  List<Element> getElementsByName(String name);

  /**
   * 
   * @param height
   *          new {@link ModelData#height}
   */
  void setHeight(int height);

  /**
   * 
   * @param width
   *          new {@link ModelData#width}
   */
  void setWidth(int width);

  /**
   * Adds layout to model.
   * 
   * @param layout
   *          layout to add
   */
  void addLayout(Layout layout);

  /**
   * 
   * @param layouts
   *          new {@link ModelData#layouts}
   */
  void setLayouts(List<Layout> layouts);

  /**
   * 
   * @return {@link ModelData#layouts}
   */
  List<Layout> getLayouts();

  /**
   * 
   * @param creationDate
   *          new {@link ModelData#creationDate}
   */
  void setCreationDate(Calendar creationDate);

  /**
   * 
   * @return {@link ModelData#creationDate}
   */
  Calendar getCreationDate();

  /**
   * Returns {@link Element} for given database identifier.
   * 
   * @param dbId
   *          element database identifier ({@link Element#id})
   * @return {@link Element} for a given id
   */
  <T extends Element> T getElementByDbId(Integer dbId);

  /**
   * Returns {@link Reaction} for given database identifier.
   * 
   * @param dbId
   *          reaction database identifier ({@link Reaction#id})
   * @return {@link Reaction} for a given id
   */
  Reaction getReactionByDbId(Integer dbId);

  /**
   * Returns sorted by size list of compartments.
   * 
   * @return list of compartment sorted by size
   */
  List<Compartment> getSortedCompartments();

  /**
   * Returns list of elements sorted by the size.
   * 
   * @return list of elements sorted by the size
   */
  List<Element> getElementsSortedBySize();

  /**
   * 
   * @param project
   *          new {@link ModelData#project}
   */
  void setProject(Project project);

  /**
   * 
   * @return {@link ModelData#project}
   */
  Project getProject();

  /**
   * 
   * @param elements
   *          new {@link ModelData#elements} collection
   */
  void setElements(Set<Element> elements);

  /**
   * @return the modelData
   */
  ModelData getModelData();

  /**
   * 
   * @return {@link ModelData#id}
   */
  Integer getId();

  /**
   * Adds submodel connection.
   * 
   * @param submodel
   *          submodel to add
   */
  void addSubmodelConnection(ModelSubmodelConnection submodel);

  /**
   * Returns set of submodel connections.
   * 
   * @return collection of submodels
   */
  Collection<ModelSubmodelConnection> getSubmodelConnections();

  /**
   * Returns name of the model.
   * 
   * @return name of the model
   */
  String getName();

  /**
   * Sets name of the model.
   * 
   * @param name
   *          name of the model
   */
  void setName(String name);

  /**
   * Returns {@link Model submodel} by the {@link ModelData#id database
   * identifier} given in the parameter.
   * 
   * @param idObject
   *          the {@link ModelData#id database identifier} that identifies
   *          submodel
   * @return {@link Model submodel} by the {@link ModelData#id database
   *         identifier} given in the parameter
   */
  Model getSubmodelById(Integer idObject);

  /**
   * Returns set of connections that point to this model. Be very carefoul with
   * using this function as the implementation forces lazy loading of the maps.
   * 
   * @return set of connections that point to this model
   */
  Collection<SubmodelConnection> getParentModels();

  /**
   * Returns connection to a submodel identified by connection name.
   * 
   * @param name
   *          name of the connection
   * @return connection to a submodel identified by connection name
   */
  Model getSubmodelByConnectionName(String name);

  /**
   * Returns connection to a submodel identified by connection identifier.
   * 
   * @param id
   *          id of the connection
   * @return connection to a submodel identified by connection identifier
   */
  SubmodelConnection getSubmodelConnectionById(Integer id);

  /**
   * Returns submodel identified by submodel identifier.
   * 
   * @param identifier
   *          identifier of the model
   * @return submodel identified by identifier
   */
  Model getSubmodelById(String identifier);

  /**
   * Returns collection of {@link Model submodels}.
   * 
   * @return collection of {@link Model submodels}
   */
  Collection<Model> getSubmodels();

  /**
   * Returns {@link Model submodel} identified by the {@link ModelData#name model
   * name}. It returns this 'parent' object when the names matches.
   * 
   * @param name
   *          name of the submodel that should be returned
   * @return {@link Model submodel} identified by the {@link ModelData#name model
   *         name}
   */
  Model getSubmodelByName(String name);

  /**
   * Adds layout to list of layouts on a given position.
   * 
   * @param index
   *          where the new layout should be added
   * @param layout
   *          object to add
   */
  void addLayout(int index, Layout layout);

  /**
   * Returns layout identified by {@link Layout#id database identifier}.
   * 
   * @param layoutIdentfier
   *          layout {@link Layout#id database identifier}
   * @return layout identified by {@link Layout#id database identifier}
   */
  Layout getLayoutByIdentifier(Integer layoutIdentfier);

  /**
   * Sets database identifier of the model.
   * 
   * @param id
   *          database identifier
   */
  void setId(int id);

  /**
   * Return list of all {@link BioEntity} in the map. This includes all
   * {@link Reaction reactions} and {@link Element elements}.
   * 
   * @return list of all {@link BioEntity} in the map
   */
  List<BioEntity> getBioEntities();

  Double getDefaultCenterX();

  void setDefaultCenterX(Double defaultCenterX);

  Double getDefaultCenterY();

  void setDefaultCenterY(Double defaultCenterY);

  Integer getDefaultZoomLevel();

  void setDefaultZoomLevel(Integer defaultZoomLevel);

  /**
   * Remove layout from model.
   * 
   * @param dbLayout
   *          layout to remove
   */
  void removeLayout(Layout dbLayout);

  Set<SbmlFunction> getFunctions();

  void addFunctions(Collection<SbmlFunction> sbmlFunctions);

  Set<SbmlParameter> getParameters();

  void addUnits(Collection<SbmlUnit> units);

  Set<SbmlUnit> getUnits();

  void addUnit(SbmlUnit volume);

  SbmlUnit getUnitsByUnitId(String unitId);

  void addParameters(Collection<SbmlParameter> sbmlParamters);

  void addParameter(SbmlParameter sbmlParameter);

  SbmlParameter getParameterById(String parameterId);

  void addFunction(SbmlFunction sbmlFunction);

  SbmlArgument getFunctionById(String id);

  void removeReactions(Collection<Reaction> reactions);
}
