package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class that represents complex in the model. Complex elements contain
 * subelements.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("Complex")
public class Complex extends Species {

	/**
	 * 
	 */
	private static final long	serialVersionUID = 1L;

	/**
	 * Default class logger.
	 */
	private static Logger			logger					 = Logger.getLogger(Complex.class);

	/**
	 * List of elements that are in this complex (only elements that lies there
	 * directly).
	 */
	@Cascade({ CascadeType.ALL })
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "complex")
	private List<Species>			elements				 = new ArrayList<>();

	/**
	 * State of the complex species.
	 */
	private String						structuralState	 = null;

	/**
	 * Empty constructor required by hibernate.
	 */
	Complex() {
		super();
	}

	/**
	 * Constructor that creates object initialized by the object given in the
	 * parameter.
	 * 
	 * @param original
	 *          original complex used for initialization
	 */
	public Complex(Complex original) {
		super(original);

		for (Species element : original.getElements()) {
			addSpecies(element.copy());
		}
		this.structuralState = original.getStructuralState();
	}

	/**
	 * Default constructor.
	 * 
	 * @param elementId
	 *          uniqe (within model) element identifier
	 */
	public Complex(String elementId) {
		super(elementId);
	}

	/**
	 * Adds {@link Species} to the complex.
	 * 
	 * @param species
	 *          object to add
	 */
	public void addSpecies(Species species) {
		boolean contains = false;
		for (Species element2 : elements) {
			if (species.getElementId().equals(element2.getElementId())) {
				contains = true;
			}

		}
		if (!contains) {
			elements.add(species);
			species.setComplex(this);
		}
	}

	/**
	 * Returns list of elements.
	 * 
	 * @return list of element inside complex
	 */
	public List<Species> getElements() {
		return elements;
	}

	/**
	 * Returns list of all {@link Species} that lies inside complex (direct and
	 * undirect).
	 * 
	 * @return list of all {@link Species} inside {@link Complex}
	 */
	public List<Species> getAllChildren() {
		List<Species> result = new ArrayList<>();
		result.addAll(elements);
		for (Species species : elements) {
			if (species instanceof Complex) {
				result.addAll(((Complex) species).getAllChildren());
			}
		}
		return result;
	}

	@Override
	public Complex copy() {
		if (this.getClass() == Complex.class) {
			return new Complex(this);
		} else {
			throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
		}

	}

	/**
	 * Removes {@link Species} from the complex.
	 * 
	 * @param element
	 *          object to remove
	 */
	public void removeElement(Species element) {
		elements.remove(element);
		if (element.getComplex() != null) {
			if (element.getComplex() == this) {
				element.setComplex(null);
			} else {
				logger.warn("Removing element from complex that probably doesn't belong there");
			}
		}
	}

	/**
	 * @return the structuralState
	 * @see #structuralState
	 */
	public String getStructuralState() {
		return structuralState;
	}

	/**
	 * @param structuralState
	 *          the structuralState to set
	 * @see #structuralState
	 */
	public void setStructuralState(String structuralState) {
		this.structuralState = structuralState;
	}

	/**
	 * Returns all children and subchildren elements for this element. Only non
	 * {@link Complex} children will be returned.
	 * 
	 * @return all children and subchildren elements for this element
	 */
	public Set<Species> getAllSimpleChildren() {
		Set<Species> result = new HashSet<>();
		for (Species element : getElements()) {
			if (element instanceof Complex) {
				result.addAll(((Complex) element).getAllSimpleChildren());
			} else {
				result.add(element);
			}
		}
		return result;
	}

	@Override
	public String getStringType() {
		return "Complex";
	}

}
