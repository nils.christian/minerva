package lcsb.mapviewer.model.map.layout;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Entry of coloring schema used for changing colors in the map. It allows to
 * identify some elements by use of the filters (by name, type, etc.) and
 * contains information about the color that should be assigned to the filter
 * elements.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class ColorSchema implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Name of the {@link Element Element}. If null then this field will be skipped.
   */
  private String name = null;

  /**
   * Name of the {@link Model} to which this schema should be limited. If null
   * then this field will be skipped.
   */
  private String modelName = null;

  /**
   * Original identifier of the {@link BioEntity} to change the color.
   */
  private String elementId = null;

  /**
   * Should the direction of highlighted reaction be reversed.
   */
  private Boolean reverseReaction = null;

  /**
   * Width of the line in the reaction.
   */
  private Double lineWidth = null;

  /**
   * In which compartments (identified by name) the element can occur.
   */
  private List<String> compartments = new ArrayList<String>();

  /**
   * What types of element should be identified by this entry.
   */
  private List<Class<? extends Element>> types = new ArrayList<>();

  /**
   * Value (-1..1 range) that is assigned to filtered elements (it will be
   * transformed into color later on). Only one of the {@link #value} and
   * {@link #color} can be set.
   */
  private Double value = null;

  /**
   * Color that is assigned to filtered elements. Only one of the {@link #value}
   * and {@link #color} can be set.
   */
  private Color color = null;

  /**
   * Set of identifiers that filter the elements.
   */
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * Number of elements matched by this entry.
   */
  private int matches = 0;

  /**
   * Short description of the entry.
   */
  private String description;

  /**
   * Default constructor.
   */
  protected ColorSchema() {
  }

  /**
   * Initializes object by copying data from the parameter.
   * 
   * @param original
   *          original object used for initialization
   */
  protected ColorSchema(ColorSchema original) {
    this.setName(original.getName());
    this.setElementId(original.getElementId());
    this.setReverseReaction(original.getReverseReaction());
    this.setLineWidth(original.getLineWidth());
    this.addCompartments(original.getCompartments());
    this.addTypes(original.getTypes());
    this.setValue(original.getValue());
    this.setColor(original.getColor());
    this.miriamData.addAll(original.getMiriamData());
    this.setMatches(original.getMatches());
    this.setDescription(original.getDescription());
  }

  /**
   * Adds class types to {@link #types} list.
   * 
   * @param types2
   *          list of classes to add
   */
  public void addTypes(List<Class<? extends Element>> types2) {
    for (Class<? extends Element> clazz : types2) {
      addType(clazz);
    }
  }

  /**
   * Adds compartment names to {@link #compartments} list.
   * 
   * @param compartments2
   *          elements to add
   */
  public void addCompartments(String[] compartments2) {
    for (String string : compartments2) {
      compartments.add(string);
    }
  }

  /**
   * Adds compartment names to {@link #compartments} list.
   * 
   * @param compartments2
   *          elements to add
   */
  public void addCompartments(Collection<String> compartments2) {
    for (String string : compartments2) {
      compartments.add(string);
    }
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    result.append("[");
    if (name != null) {
      result.append(name + ",");
    }
    if (compartments.size() > 0) {
      result.append("(");
      for (String comp : compartments) {
        result.append(comp + ",");
      }
      result.append("),");
    }
    if (types.size() > 0) {
      result.append("(");
      for (Class<?> clazz : types) {
        result.append(clazz.getSimpleName() + ",");
      }
      result.append("),");
    }
    if (value != null) {
      result.append(value + ",");
    }
    if (color != null) {
      result.append(color + ",");
    }
    result.append(miriamData + ",");

    result.append(matches + "]");
    return result.toString();
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    if (name == null) {
      this.name = null;
    } else {
      this.name = name.trim();
    }
  }

  /**
   * @return the compartments
   * @see #compartments
   */
  public List<String> getCompartments() {
    return compartments;
  }

  /**
   * @param compartments
   *          the compartments to set
   * @see #compartments
   */
  public void setCompartments(List<String> compartments) {
    this.compartments = compartments;
  }

  /**
   * @return the types
   * @see #types
   */
  public List<Class<? extends Element>> getTypes() {
    return types;
  }

  /**
   * @param types
   *          the types to set
   * @see #types
   */
  public void setTypes(List<Class<? extends Element>> types) {
    this.types = types;
  }

  /**
   * @return the value
   * @see #value
   */
  public Double getValue() {
    return value;
  }

  /**
   * @param value
   *          the value to set
   * @see #value
   */
  public void setValue(Double value) {
    this.value = value;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * @return the matches
   * @see #matches
   */
  public int getMatches() {
    return matches;
  }

  /**
   * @param matches
   *          the matches to set
   * @see #matches
   */
  public void setMatches(int matches) {
    this.matches = matches;
  }

  /**
   * Adds identifier to {@link #miriamData} set.
   * 
   */
  public void addMiriamData(MiriamData md) {
    miriamData.add(md);
  }

  /**
   * @return the lineWidth
   * @see #lineWidth
   */
  public Double getLineWidth() {
    return lineWidth;
  }

  /**
   * @param lineWidth
   *          the lineWidth to set
   * @see #lineWidth
   */
  public void setLineWidth(Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  /**
   * @return the reverseReaction
   * @see #reverseReaction
   */
  public Boolean getReverseReaction() {
    return reverseReaction;
  }

  /**
   * @param reverseReaction
   *          the reverseReaction to set
   * @see #reverseReaction
   */
  public void setReverseReaction(Boolean reverseReaction) {
    this.reverseReaction = reverseReaction;
  }

  /**
   * Adds compartment name to {@link #compartments}.
   * 
   * @param name
   *          compartment name
   */
  public void addCompartment(String name) {
    compartments.add(name);
  }

  /**
   * Adds class type to {@link #types} list.
   * 
   * @param clazz
   *          class to add
   */
  public void addType(Class<? extends Element> clazz) {
    this.types.add(clazz);
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Creates a copy of this object.
   * 
   * @return copy of the object
   */
  public abstract ColorSchema copy();

  public String getModelName() {
    return modelName;
  }

  public void setModelName(String modelName) {
    this.modelName = modelName;
  }

  public String getElementId() {
    return elementId;
  }

  public void setElementId(String elementId) {
    this.elementId = elementId;
  }

  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

}
