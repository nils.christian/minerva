package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

/**
 * Representation of a single SBML unit factor. For example unit for velocity is
 * m/s. It means that we have two unit types here:
 * <ul>
 * <li>metre^1</li>
 * <li>second^-1</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "sbml_unit_factor")
@org.hibernate.annotations.GenericGenerator(name = "test-increment-strategy", strategy = "increment")
@XmlRootElement
public class SbmlUnitTypeFactor implements Serializable {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = Logger.getLogger(SbmlUnitTypeFactor.class);

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  @Enumerated(EnumType.STRING)
  private SbmlUnitType unitType;

  private int exponent = 1;
  private int scale = 0;
  private double multiplier = 1.0;

  @ManyToOne(fetch = FetchType.LAZY)
  private SbmlUnit unit;

  /**
   * Constructor required by hibernate.
   */
  SbmlUnitTypeFactor() {
    super();
  }

  public SbmlUnitTypeFactor(SbmlUnitType unitType, int exponent, int scale, double multiplier) {
    this.unitType = unitType;
    this.exponent = exponent;
    this.scale = scale;
    this.multiplier = multiplier;
  }

  public SbmlUnitTypeFactor(SbmlUnitTypeFactor original) {
    this(original.getUnitType(), original.getExponent(), original.getScale(), original.getMultiplier());
  }

  public SbmlUnitType getUnitType() {
    return unitType;
  }

  public void setUnitType(SbmlUnitType unitType) {
    this.unitType = unitType;
  }

  public int getExponent() {
    return exponent;
  }

  public void setExponent(int exponent) {
    this.exponent = exponent;
  }

  public int getScale() {
    return scale;
  }

  public void setScale(int scale) {
    this.scale = scale;
  }

  public double getMultiplier() {
    return multiplier;
  }

  public void setMultiplier(double multiplier) {
    this.multiplier = multiplier;
  }

  public SbmlUnit getUnit() {
    return unit;
  }

  public void setUnit(SbmlUnit unit) {
    this.unit = unit;
  }

  public SbmlUnitTypeFactor copy() {
    return new SbmlUnitTypeFactor(this);
  }

  public int getId() {
    return id;
  }

}
