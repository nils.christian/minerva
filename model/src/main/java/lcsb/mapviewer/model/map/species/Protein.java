package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Entity representing protein element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("PROTEIN_ALIAS")
public abstract class Protein extends Species {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * State of the protein.
   */
  private String structuralState = null;

  /**
   * List of modifications for the Protein.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Protein() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  protected Protein(Protein original) {
    super(original);
    this.structuralState = original.getStructuralState();
    for (ModificationResidue mr : original.getModificationResidues()) {
      addModificationResidue(mr.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  protected Protein(String elementId) {
    super(elementId);
  }

  /**
   * Adds modification to the protein.
   * 
   * @param modificationResidue
   *          modification to add
   */
  public void addModificationResidue(ModificationResidue modificationResidue) {
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(List<ModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  /**
   * @return the structuralState
   * @see #structuralState
   */
  public String getStructuralState() {
    return structuralState;
  }

  /**
   * @param structuralState
   *          the structuralState to set
   * @see #structuralState
   */
  public void setStructuralState(String structuralState) {
    this.structuralState = structuralState;
  }

  @Override
  public String getStringType() {
    return "Protein";
  }

}
