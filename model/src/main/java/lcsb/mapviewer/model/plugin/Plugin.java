package lcsb.mapviewer.model.plugin;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * Meta data of the plugin used in the system.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "plugin_table")
public class Plugin implements Serializable {

  /**
  	* 
    */
  private static final long serialVersionUID = 1L;

  public static final Comparator<? super Plugin> ID_COMPARATOR = new Comparator<Plugin>() {

    @Override
    public int compare(Plugin o1, Plugin o2) {
      return new IntegerComparator().compare(o1.getId(), o2.getId());
    }
  };

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * Hash of the plugin code used as identifier to distinguish between plugins.
   */
  private String hash;

  /**
   * Name of the plugin.
   */
  private String name;

  /**
   * Version of the plugin.
   */
  private String version;

  /**
   * List of urls from which plugin can be downloaded.
   */
  @ElementCollection
  @CollectionTable(name = "plugin_urls", joinColumns = @JoinColumn(name = "plugin_iddb"))
  @Column(name = "url")
  private Set<String> urls = new HashSet<>();

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public Set<String> getUrls() {
    return urls;
  }

  public void setUrls(Set<String> urls) {
    this.urls = urls;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
