package lcsb.mapviewer.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class defines GUI preference for the {@link User}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "user_gui_preferences")
public class UserGuiPreference implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private Integer id;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this parameter.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="annotation_schema_iddb")
  private UserAnnotationSchema annotationSchema;

  /**
   * GUI parameter name.
   */
  @Column(name = "key", nullable = false)
  private String key;

  /**
   * GUI parameter value.
   */
  @Column(name = "value", nullable = false)
  private String value;

  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
