package lcsb.mapviewer.model.user;

import java.awt.Color;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Class representing user.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "user_table")
public class User implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public static final Comparator<? super User> LOGIN_COMPARATOR = new Comparator<User>() {

    @Override
    public int compare(User o1, User o2) {
      String login1 = null;
      String login2 = null;
      if (o1 != null) {
        login1 = o1.getLogin();
      }
      if (o2 != null) {
        login2 = o2.getLogin();
      }
      return new StringComparator().compare(login1, login2);
    }
  };

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private Integer id;

  /**
   * User login.
   */
  private String login;

  /**
   * Password in encrypted form. For the encryption PasswordEncoder spring bean is
   * used.
   */
  private String cryptedPassword;

  /**
   * Name of the user.
   */
  private String name;

  /**
   * Family name of the user.
   */
  private String surname;

  /**
   * Email address of the user.
   */
  private String email;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#MIN_COLOR_VAL}. Used for coloring minimum
   * values in overlays.
   */
  private Color minColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#MAX_COLOR_VAL}. Used for coloring maximum
   * values in overlays.
   */
  private Color maxColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#NEUTRAL_COLOR_VAL}. Used for coloring neutral
   * values (0) in overlays.
   */
  private Color neutralColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#SIMPLE_COLOR_VAL}. Used for coloring overlays
   * without values and colors.
   */
  private Color simpleColor;

  /**
   * Is the user removed.
   */
  private boolean removed = false;

  /**
   * User is connected to LDAP directory.
   */
  private boolean connectedToLdap = false;

  @Column(name = "terms_of_use_consent")
  private boolean termsOfUseConsent = false;

  /**
   * Set of user privileges.
   */
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true, cascade = CascadeType.ALL)
  private Set<BasicPrivilege> privileges = new HashSet<>();

  /**
   * Default annotations schema used by this user.
   */
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(nullable = true)
  private UserAnnotationSchema annotationSchema;

  /**
   * Default constructor.
   */
  public User() {
  }

  /**
   * Adds privilege to the user.
   *
   * @param privilege
   *          privilege to add
   */
  public void addPrivilege(BasicPrivilege privilege) {
    privileges.add(privilege);
  }

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the login
   * @see #login
   */
  public String getLogin() {
    return login;
  }

  /**
   * @param login
   *          the login to set
   * @see #login
   */
  public void setLogin(String login) {
    this.login = login;
  }

  /**
   * @return the cryptedPassword
   * @see #cryptedPassword
   */
  public String getCryptedPassword() {
    return cryptedPassword;
  }

  /**
   * @param cryptedPassword
   *          the cryptedPassword to set
   * @see #cryptedPassword
   */
  public void setCryptedPassword(String cryptedPassword) {
    this.cryptedPassword = cryptedPassword;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the surname
   * @see #surname
   */
  public String getSurname() {
    return surname;
  }

  /**
   * @param surname
   *          the surname to set
   * @see #surname
   */
  public void setSurname(String surname) {
    this.surname = surname;
  }

  /**
   * @return the email
   * @see #email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email
   *          the email to set
   * @see #email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the privileges
   * @see #privileges
   */
  public Set<BasicPrivilege> getPrivileges() {
    return privileges;
  }

  /**
   * @param privileges
   *          the privileges to set
   * @see #privileges
   */
  public void setPrivileges(Set<BasicPrivilege> privileges) {
    this.privileges = privileges;
  }

  /**
   * @return the removed
   * @see #removed
   */
  public boolean isRemoved() {
    return removed;
  }

  /**
   * @param removed
   *          the removed to set
   * @see #removed
   */
  public void setRemoved(boolean removed) {
    this.removed = removed;
  }

  /**
   * @return the annotationSchema
   * @see #annotationSchema
   */
  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  /**
   * @param annotationSchema
   *          the annotationSchema to set
   * @see #annotationSchema
   */
  public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
    if (!this.equals(annotationSchema.getUser())) {
      annotationSchema.setUser(this);
    }
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + "] " + getName() + " " + getSurname();
  }

  /**
   * @return the minColor
   * @see #minColor
   */
  public Color getMinColor() {
    return minColor;
  }

  /**
   * @param minColor
   *          the minColor to set
   * @see #minColor
   */
  public void setMinColor(Color minColor) {
    this.minColor = minColor;
  }

  /**
   * @return the maxColor
   * @see #maxColor
   */
  public Color getMaxColor() {
    return maxColor;
  }

  /**
   * @param maxColor
   *          the maxColor to set
   * @see #maxColor
   */
  public void setMaxColor(Color maxColor) {
    this.maxColor = maxColor;
  }

  /**
   * @return the simpleColor
   * @see #simpleColor
   */
  public Color getSimpleColor() {
    return simpleColor;
  }

  /**
   * @param simpleColor
   *          the simpleColor to set
   * @see #simpleColor
   */
  public void setSimpleColor(Color simpleColor) {
    this.simpleColor = simpleColor;
  }

  public Color getNeutralColor() {
    return neutralColor;
  }

  public void setNeutralColor(Color neutralColor) {
    this.neutralColor = neutralColor;
  }

  public boolean isTermsOfUseConsent() {
    return termsOfUseConsent;
  }

  public void setTermsOfUseConsent(boolean termsOfUseConsent) {
    this.termsOfUseConsent = termsOfUseConsent;
  }

  public boolean isConnectedToLdap() {
    return connectedToLdap;
  }

  public void setConnectedToLdap(boolean connectedToLdap) {
    this.connectedToLdap = connectedToLdap;
  }

}
