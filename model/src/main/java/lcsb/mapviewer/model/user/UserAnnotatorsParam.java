package lcsb.mapviewer.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * This class defines set of annotators parameters that are used by a user.
 * 
 * @author David Hoksza
 * 
 */
@Entity
@Table(name = "annotators_params_table")
public class UserAnnotatorsParam implements Serializable {

	/**
	 * 
	 */
	private static final long		 serialVersionUID	= 1L;

	/**
	 * Unique identifier in the database.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDb", unique = true, nullable = false)
	private Integer							 id;

	/**
	 * {@link UserAnnotationSchema} that defines which user is using this set of
	 * annotators.
	 */
	@ManyToOne
	private UserAnnotationSchema annotationSchema;

	
	/**
	 * Classname of the annotator {@link #paramName parameter} of which is being set to the {@link #paramValue value}.  
	 */
	@Column(name = "annotator_classname", nullable = false)
	private Class<?>						 annotatorClassName;
	
	/**
	 * Parameter name to be set.  
	 */
	@Column(name = "name", nullable = false)
	private String							 paramName;

	/**
	 * Parameter value to be set.  
	 */
	@Column(name = "value", nullable = false)
	private String							 paramValue;

	/**
	 * Default constructor.
	 */
	public UserAnnotatorsParam() {

	}

	/**
	 * Default constructor.
	 * 
	 * @param objectClass
	 *          {@link #className}
	 * @param annotators
	 *          {@link #annotators}
	 */
	public UserAnnotatorsParam(Class<?> annotatorClassName, String paramName, String paramValue) {
		setAnnotatorClassName(annotatorClassName);
		setParamName(paramName);
		setParamValue(paramValue);
	}

	/**
	 * @return the id
	 * @see #id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the annotationSchema
	 * @see #annotationSchema
	 */
	public UserAnnotationSchema getAnnotationSchema() {
		return annotationSchema;
	}

	/**
	 * @param annotationSchema
	 *          the annotationSchema to set
	 * @see #annotationSchema
	 */
	public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
		this.annotationSchema = annotationSchema;
	}

	/**
	 * @return the className
	 * @see #className
	 */
	public Class<?> getAnnotatorClassName() {
		return annotatorClassName;
	}

	/**
	 * @param className
	 *          the className to set
	 * @see #className
	 */
	public void setAnnotatorClassName(Class<?> annotatorClassName) {
		this.annotatorClassName = annotatorClassName;
	}
	
	/**
	 * @return the parameter name
	 * @see #paramName
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * @param paramName
	 *          the {@link UserAnnotatorsParam#paramName} to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
	/**
	 * @return the parameter value
	 * @see #paramValue
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * @param parameter value
	 *          the {@link UserAnnotatorsParam#paramValue} to set	  
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
}
