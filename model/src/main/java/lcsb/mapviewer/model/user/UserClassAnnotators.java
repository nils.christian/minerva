package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

/**
 * This class defines set of default class annotatators for a single class type
 * that are used by a user.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "class_annotator_table")
public class UserClassAnnotators implements Serializable {

	/**
	 * 
	 */
	private static final long		 serialVersionUID	= 1L;

	/**
	 * Unique identifier in the database.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDb", unique = true, nullable = false)
	private Integer							 id;

	/**
	 * {@link UserAnnotationSchema} that defines which user is using this set of
	 * annotators.
	 */
	@ManyToOne
	private UserAnnotationSchema annotationSchema;

	/**
	 * Class for which this set of annotators is defined.
	 */
	private String							 className;

	/**
	 * List of strings defining set of annotators (canonical class names).
	 */
	@ElementCollection
	@CollectionTable(name = "class_annotator_annotators_table", joinColumns = @JoinColumn(name = "class_annotator_iddb"))
	@IndexColumn(name = "idx")
	@Column(name = "annotator_name")
	private List<String>				 annotators				= new ArrayList<>();

	/**
	 * Default constructor.
	 */
	public UserClassAnnotators() {

	}

	/**
	 * Default constructor.
	 * 
	 * @param objectClass
	 *          {@link #className}
	 * @param annotators
	 *          {@link #annotators}
	 */
	public UserClassAnnotators(Class<?> objectClass, Collection<String> annotators) {
		setClassName(objectClass);
		for (String className : annotators) {
			this.annotators.add(className);
		}
	}

	/**
	 * @return the id
	 * @see #id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the annotationSchema
	 * @see #annotationSchema
	 */
	public UserAnnotationSchema getAnnotationSchema() {
		return annotationSchema;
	}

	/**
	 * @param annotationSchema
	 *          the annotationSchema to set
	 * @see #annotationSchema
	 */
	public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
		this.annotationSchema = annotationSchema;
	}

	/**
	 * @return the className
	 * @see #className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className
	 *          the className to set
	 * @see #className
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the annotators
	 * @see #annotators
	 */
	public List<String> getAnnotators() {
		return annotators;
	}

	/**
	 * @param annotators
	 *          the annotators to set
	 * @see #annotators
	 */
	public void setAnnotators(List<String> annotators) {
		this.annotators = annotators;
	}

	/**
	 * Sets {@link #className}.
	 * 
	 * @param clazz
	 *          new {@link #className} value
	 */
	public void setClassName(Class<?> clazz) {
		setClassName(clazz.getCanonicalName());
	}

	/**
	 * Adds annotator to {@link #annotators list of annotators}.
	 * 
	 * @param clazz
	 *          object to add
	 */
	public void addAnnotator(Class<?> clazz) {
		annotators.add(clazz.getCanonicalName());
	}
}
