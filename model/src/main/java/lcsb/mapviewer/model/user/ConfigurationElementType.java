package lcsb.mapviewer.model.user;

/**
 * This enumerate defines all possible configuration parameter that are
 * configurable by the user.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ConfigurationElementType {

  /**
   * Email address used for sending email from the system.
   */
  EMAIL_ADDRESS("E-mail address", "your.account@domain.com", ConfigurationElementEditType.EMAIL, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION), //

  /**
   * Login for the email account.
   */
  EMAIL_LOGIN("E-mail server login", "your@login", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION), //

  /**
   * Password for the email account.
   */
  EMAIL_PASSWORD("E-mail server password", "email.secret.password", ConfigurationElementEditType.PASSWORD, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION), //

  /**
   * Address of the IMAP server.
   */
  EMAIL_IMAP_SERVER("IMAP server", "your.imap.domain.com", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION), //

  /**
   * Address of the SMTP server.
   */
  EMAIL_SMTP_SERVER("SMTP server", "your.smtp.domain.com", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION), //

  /**
   * Port used for SMTP connection (sending e-mails).
   */
  EMAIL_SMTP_PORT("SMTP port", "25", ConfigurationElementEditType.INTEGER, true,
      ConfigurationElementTypeGroup.EMAIL_NOTIFICATION), //

  /**
   * Default map that should be presented if no map is selected by user side.
   */
  DEFAULT_MAP("Default Project Id", "empty", ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION), //

  /**
   * Logo presented in the system.
   */
  LOGO_IMG("Logo icon", "udl.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO), //

  /**
   * Address connected to the logo.
   */
  LOGO_LINK("Logo link (after click)", "http://wwwen.uni.lu/", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO), //

  /**
   * Maximum distance (in pixels) that is allowed during finding closest element
   * on the map.
   */
  SEARCH_DISTANCE("Max distance for clicking on element (px)", "10", ConfigurationElementEditType.DOUBLE, false,
      ConfigurationElementTypeGroup.POINT_AND_CLICK),

  /**
   * Email used for requesting an account (in client side).
   */
  REQUEST_ACCOUNT_EMAIL("Email used for requesting an account", "your.email@domain.com",
      ConfigurationElementEditType.EMAIL, false, ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  /**
   * Max number of results in search box.
   */
  SEARCH_RESULT_NUMBER(
      "Max number of results (this value indicates the max number of elements that will be returned from search - not the number of aggregated elements in the search box).",
      "100", ConfigurationElementEditType.INTEGER, false, ConfigurationElementTypeGroup.POINT_AND_CLICK),

  /**
   * Google Analytics tracking ID used for statistics. This tracking ID should
   * look like "UA-000000-01". More information about tracking ID can be found
   * <a href="https://support.google.com/analytics/answer/1032385?hl=en"> here
   * </a>.
   */
  GOOGLE_ANALYTICS_IDENTIFIER("Google Analytics tracking ID used for statistics", "",
      ConfigurationElementEditType.STRING, false, ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Description of the logo presented in the system.
   */
  LOGO_TEXT("Logo description", "University of Luxembourg", ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Domain allowed to connect via x-frame technology.
   */
  X_FRAME_DOMAIN("Domain allowed to connect via x-frame technology", "", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Relative directory (in webapps folder) where big files will be stored.
   */
  BIG_FILE_STORAGE_DIR("Path to store big files", "minerva-big/", ConfigurationElementEditType.STRING, false,
      ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * File where legend 1/4 is stored.
   */
  LEGEND_FILE_1("Legend 1 image file", "resources/images/legend_a.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 2/4 is stored.
   */
  LEGEND_FILE_2("Legend 2 image file", "resources/images/legend_b.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 3/4 is stored.
   */
  LEGEND_FILE_3("Legend 3 image file", "resources/images/legend_c.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 4/4 is stored.
   */
  LEGEND_FILE_4("Legend 4 image file", "resources/images/legend_d.png", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * File where legend 4/4 is stored.
   */
  USER_MANUAL_FILE("User manual file", "resources/other/user_guide.pdf", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  /**
   * Color used for negative overlay values.
   */
  MIN_COLOR_VAL("Overlay color for negative values", "FF0000", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Color used for positive overlay values.
   */
  MAX_COLOR_VAL("Overlay color for postive values", "0000FF", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Color used for undefined overlay values.
   */
  SIMPLE_COLOR_VAL("Overlay color when no values are defined", "00FF00", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Color used for 0 overlay value.
   */
  NEUTRAL_COLOR_VAL("Overlay color for value=0", "FFFFFF", ConfigurationElementEditType.COLOR, false,
      ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Opacity of data overlay objects in the frontend.
   */
  OVERLAY_OPACITY("Opacity used when drwaing data overlays (value between 0.0-1.0)", "0.8",
      ConfigurationElementEditType.DOUBLE, false, ConfigurationElementTypeGroup.OVERLAYS),

  /**
   * Default content of the email when requesting for an account in the system.
   */
  REQUEST_ACCOUNT_DEFAULT_CONTENT("Email content used for requesting an account",
      "Dear Disease map team,\nI would like to request an account in the system.\nKind regards",
      ConfigurationElementEditType.TEXT, false, ConfigurationElementTypeGroup.EMAIL_NOTIFICATION),

  DEFAULT_VIEW_PROJECT("Default user privilege for: " + PrivilegeType.VIEW_PROJECT.getCommonName(), "true",
      ConfigurationElementEditType.BOOLEAN, true, ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES),

  DEFAULT_EDIT_COMMENTS_PROJECT("Default user privilege for: " + PrivilegeType.EDIT_COMMENTS_PROJECT.getCommonName(),
      "false", ConfigurationElementEditType.BOOLEAN, true, ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES),

  DEFAULT_LAYOUT_MANAGEMENT("Default user privilege for: " + PrivilegeType.LAYOUT_MANAGEMENT.getCommonName(), "false",
      ConfigurationElementEditType.BOOLEAN, true, ConfigurationElementTypeGroup.DEFAULT_USER_PRIVILEGES),

  SHOW_REACTION_TYPE("Show reaction type when browsing map", "true", ConfigurationElementEditType.BOOLEAN, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  GOOGLE_MAPS_API_KEY("By providing this Google Maps Platform API key I declare that I am aware that "
      + "I am a Customer of the Google Maps Platform and I agree to the terms of the <a href=\"https://cloud.google.com/maps-platform/terms/\"  target='_blank'>license of Google Maps Platform</a>."
      + "In particular, I warrant that neither any of the maps nor publicly available data overlays "
      + "(\"General overlays\") on this MINERVA server contain Protected Health Information (as defined in and subject to HIPAA).",
      "", ConfigurationElementEditType.STRING, false, ConfigurationElementTypeGroup.SERVER_CONFIGURATION),

  /**
   * Terms of use.
   */
  TERMS_OF_USE("URL of platform Terms of Use file", "", ConfigurationElementEditType.URL, false,
      ConfigurationElementTypeGroup.LEGEND_AND_LOGO),

  LDAP_ADDRESS("LDAP address", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_PORT("LDAP port", "389", ConfigurationElementEditType.INTEGER, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_SSL("LDAP uses SSL", "false", ConfigurationElementEditType.BOOLEAN, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_BIND_DN("LDAP bind DN", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_PASSWORD("LDAP password", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_BASE_DN("LDAP base DN", "", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_OBJECT_CLASS("LDAP filter objectClass", "*", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_FIRST_NAME_ATTRIBUTE("LDAP first name attribute", "givenName", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_LAST_NAME_ATTRIBUTE("LDAP last name attribute", "sn", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //
  LDAP_EMAIL_ATTRIBUTE("LDAP email attribute", "mail", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION),//
  LDAP_FILTER("LDAP filter ", "(memberof=cn=minerva,cn=groups,cn=accounts,dc=uni,dc=lu)", ConfigurationElementEditType.STRING, true,
      ConfigurationElementTypeGroup.LDAP_CONFIGURATION), //

  ;

  /**
   * Default value of the configuration parameter (it will be used only when value
   * doesn't exist in the DAO).
   */
  private String defaultValue = "";

  /**
   * Common name used for visualization (query user).
   */
  private String commonName = "";

  /**
   * How we want to edit specific parameter.
   */
  private ConfigurationElementEditType editType = null;

  private boolean serverSide = true;
  private ConfigurationElementTypeGroup group = null;

  /**
   * Default constructor.
   *
   * @param commonName
   *          common name used for this parameter
   * @param editType
   *          type defining how we want to edit this configuration parameter
   * @param defaultVal
   *          default value assigned to this parameter
   */
  ConfigurationElementType(String commonName, String defaultVal, ConfigurationElementEditType editType,
      boolean serverSide, ConfigurationElementTypeGroup group) {
    this.defaultValue = defaultVal;
    this.commonName = commonName;
    this.editType = editType;
    this.serverSide = serverSide;
    this.group = group;
  }

  /**
   * @return the defaultValue
   * @see #defaultValue
   */
  public String getDefaultValue() {
    return defaultValue;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * @return the editType
   * @see #editType
   */
  public ConfigurationElementEditType getEditType() {
    return editType;
  }

  /**
   * @return the serverSide
   * @see #serverSide
   */
  public boolean isServerSide() {
    return serverSide;
  }

  public ConfigurationElementTypeGroup getGroup() {
    return group;
  }
}
