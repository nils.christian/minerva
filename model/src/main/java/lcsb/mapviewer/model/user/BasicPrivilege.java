package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * Class that defines user privilege. Privilege has an access level (typically 0,
 * 1 value), type of the privilege and user reference.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "privilege_table")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "privilege_class_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("BASIC_PRIVILEGE")
public class BasicPrivilege implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public static final Comparator<? super BasicPrivilege> ID_COMPARATOR = new Comparator<BasicPrivilege>() {

    @Override
    public int compare(BasicPrivilege o1, BasicPrivilege o2) {
      return new IntegerComparator().compare(o1.getId(), o2.getId());
    }
  };

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private int id;

  /**
   * Which user this privilege concerns.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  /**
   * Type of the privilege.
   */
  @Enumerated(EnumType.STRING)
  private PrivilegeType type;

  /**
   * Access level of the privilege. By default 0 means that user doesn't have
   * privilege and value greater than 0 means that user has privilege. It's
   * possible to implement different behaviour for different level values.
   */
  private int level;

  /**
   * Constructor that initialize the privilege with given data.
   * 
   * @param level
   *          level on which the privilege is set
   * @param type
   *          type of the privilege
   * @param user
   *          user for which this privilege is set
   */
  public BasicPrivilege(int level, PrivilegeType type, User user) {
    this.level = level;
    this.type = type;
    this.user = user;
  }

  /**
   * Default constructor.
   */
  public BasicPrivilege() {
  }

  /**
   * Checks if the privilege is with the same type as privilege given in the
   * parameter.
   * 
   * @param privilege
   *          other privilege to compare
   * @return <code>true</code> if privilege is of the same type as an argument,
   *         <code>false</code> otherwise
   */
  public boolean equalsPrivilege(BasicPrivilege privilege) {
    if (privilege == null) {
      return false;
    }
    return type.equals(privilege.getType());
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the user
   * @see #user
   */
  public User getUser() {
    return user;
  }

  /**
   * @param user
   *          the user to set
   * @see #user
   */
  public void setUser(User user) {
    this.user = user;
  }

  /**
   * @return the type
   * @see #type
   */
  public PrivilegeType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(PrivilegeType type) {
    this.type = type;
  }

  /**
   * @return the level
   * @see #level
   */
  public int getLevel() {
    return level;
  }

  /**
   * @param level
   *          the level to set
   * @see #level
   */
  public void setLevel(int level) {
    this.level = level;
  }
}
