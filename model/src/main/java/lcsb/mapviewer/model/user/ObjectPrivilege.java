package lcsb.mapviewer.model.user;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.ObjectUtils;
import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * This class extends {@link BasicPrivilege} class which define typical user
 * privilege. The extension introduces specific object in the model to which
 * privilege is given.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("OBJECT_PRIVILEGE")
public class ObjectPrivilege extends BasicPrivilege implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Identifier of the object connected to this privilege.
   */
  private Integer idObject;

  /**
   * Constructor that initialize data with the information given in the
   * parameters.
   * 
   * @param object
   *          object connected to this privilege
   * @param level
   *          access level
   * @param type
   *          type of the privilege
   * @param user
   *          user who has this privilege
   */
  public ObjectPrivilege(Object object, int level, PrivilegeType type, User user) {
    super(level, type, user);
    if (object != null) {
      idObject = ObjectUtils.getIdOfObject(object);
    }
  }

  /**
   * Default constructor.
   */
  public ObjectPrivilege() {
  }

  @Override
  public boolean equalsPrivilege(BasicPrivilege privilege) {
    if (privilege == null) {
      return false;
    }
    if (privilege instanceof ObjectPrivilege) {
      IntegerComparator integerComparator = new IntegerComparator();
      return super.equalsPrivilege(privilege)
          && integerComparator.compare(idObject, ((ObjectPrivilege) privilege).getIdObject()) == 0;
    } else {
      return false;
    }
  }

  /**
   * @return the idObject
   * @see #idObject
   */
  public Integer getIdObject() {
    return idObject;
  }

  /**
   * @param idObject
   *          the idObject to set
   * @see #idObject
   */
  public void setIdObject(Integer idObject) {
    this.idObject = idObject;
  }

  /**
   * @param idObject
   *          the idObject to set
   * @see #idObject
   */
  public void setIdObject(String idObject) {
    this.idObject = Integer.valueOf(idObject);
  }

}
