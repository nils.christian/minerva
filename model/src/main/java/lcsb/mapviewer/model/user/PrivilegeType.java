package lcsb.mapviewer.model.user;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.Layout;

/**
 * Defines types of privileges in the system.
 * 
 * @author Piotr Gawron
 * 
 */
public enum PrivilegeType {

  /**
   * User can browse project.
   */
  VIEW_PROJECT(ObjectPrivilege.class, Project.class, "View project"),

  /**
   * User can add project.
   */
  ADD_MAP(BasicPrivilege.class, null, "Add project"),

  /**
   * User can edit comments in the project.
   */
  EDIT_COMMENTS_PROJECT(ObjectPrivilege.class, Project.class, "Manage comments"),

  /**
   * User can manage projects.
   */
  PROJECT_MANAGEMENT(BasicPrivilege.class, null, "Map management"),

  /**
   * User can manage users.
   */
  USER_MANAGEMENT(BasicPrivilege.class, null, "User management"),

  /**
   * User can have custom layouts (access level defines how many).
   */
  CUSTOM_LAYOUTS(BasicPrivilege.class, null, "Custom overlays", true),

  /**
   * User can view non-public layout.
   */
  LAYOUT_VIEW(ObjectPrivilege.class, Layout.class, "View overlay"),

  /**
   * User can manage configuration.
   */
  CONFIGURATION_MANAGE(BasicPrivilege.class, null, "Manage configuration"),

  /**
   * User can manage layouts of all users in the project.
   */
  LAYOUT_MANAGEMENT(ObjectPrivilege.class, Project.class, "Manage overlays"), //

  /**
   * User can manage reference genomes.
   */
  MANAGE_GENOMES(BasicPrivilege.class, null, "Manage genomes");

  /**
   * Type of privilege (basic or privilege to the object).
   */
  private Class<? extends BasicPrivilege> privilegeClassType;

  /**
   * Type of the object to which privilege refers.
   */
  private Class<?> privilegeObjectType;

  /**
   * Name of the privilege.
   */
  private String commonName;

  /**
   * Is the privilege numeric.
   */
  private boolean numeric = false;

  /**
   * Constructor that initialize enum with data.
   * 
   * @param privilegeClazz
   *          {@link #privilegeClassType}
   * @param objectClazz
   *          {@link #privilegeObjectType}
   * @param commonName
   *          {@link #commonName}
   */
  PrivilegeType(Class<? extends BasicPrivilege> privilegeClazz, Class<?> objectClazz, String commonName) {
    this.privilegeClassType = privilegeClazz;
    this.privilegeObjectType = objectClazz;
    this.commonName = commonName;
  }

  /**
   * Constructor that initialize enum with data.
   * 
   * @param privilegeClazz
   *          {@link #privilegeClassType}
   * @param objectClazz
   *          {@link #privilegeObjectType}
   * @param commonName
   *          {@link #commonName}
   * @param numeric
   *          {@link #numeric}
   */
  PrivilegeType(Class<? extends BasicPrivilege> privilegeClazz, Class<?> objectClazz, String commonName,
      boolean numeric) {
    this.privilegeClassType = privilegeClazz;
    this.privilegeObjectType = objectClazz;
    this.commonName = commonName;
    this.numeric = true;
  }

  /**
   * 
   * @return {@link #privilegeClassType}
   */
  public Class<? extends BasicPrivilege> getPrivilegeClassType() {
    return privilegeClassType;
  }

  /**
   * 
   * @return {@link #privilegeObjectType}
   */
  public Class<?> getPrivilegeObjectType() {
    return privilegeObjectType;
  }

  /**
   * 
   * @return {@link #commonName}
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * 
   * @return {@link #numeric}
   */
  public boolean isNumeric() {
    return numeric;
  }
}
