package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * Annotation schema used by the user. It contains information about annotators
 * used by the user. Which annotations are valid/required for given object
 * types.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "user_annotation_schema_table")
public class UserAnnotationSchema implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @Transient
  private final transient Logger logger = Logger.getLogger(UserAnnotationSchema.class);

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idDb", unique = true, nullable = false)
  private Integer id;

  /**
   * {@link User} for which annotation schema is defined.
   */
  @OneToOne
  private User user;

  /**
   * Should the miriam types be validated?
   */
  private Boolean validateMiriamTypes = false;

  private Boolean annotateModel = false;

  private Boolean cacheData = false;

  private Boolean autoResizeMap = true;

  private Boolean semanticZooming = false;

  /**
   * Should map be visualized as sbgn?
   */
  private Boolean sbgnFormat = false;

  /**
   * Should the default view be network (if not then it will be pathways and
   * compartments)?
   */
  private Boolean networkLayoutAsDefault = false;

  /**
   * List of class annotators for specific object types.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotationSchema")
  @OrderBy("id")
  private List<UserClassAnnotators> classAnnotators = new ArrayList<>();

  /**
   * List of class annotators params.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotationSchema")
  @OrderBy("id")
  private List<UserAnnotatorsParam> annotatorsParams = new ArrayList<>();

  /**
   * List of valid annotations for given object type.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotationSchema")
  @OrderBy("id")
  private List<UserClassValidAnnotations> classValidAnnotators = new ArrayList<>();

  /**
   * List of required annotations for given object type.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotationSchema")
  @OrderBy("id")
  private List<UserClassRequiredAnnotations> classRequiredAnnotators = new ArrayList<>();

  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotationSchema")
  private Set<UserGuiPreference> guiPreferences = new HashSet<>();

  /**
   * @return the user
   * @see #user
   */
  public User getUser() {
    return user;
  }

  /**
   * @param user
   *          the user to set
   * @see #user
   */
  public void setUser(User user) {
    this.user = user;
  }

  /**
   * @return the validateMiriamTypes
   * @see #validateMiriamTypes
   */
  public Boolean getValidateMiriamTypes() {
    return validateMiriamTypes;
  }

  /**
   * @param validateMiriamTypes
   *          the validateMiriamTypes to set
   * @see #validateMiriamTypes
   */
  public void setValidateMiriamTypes(Boolean validateMiriamTypes) {
    this.validateMiriamTypes = validateMiriamTypes;
  }

  /**
   * @return the classAnnotators
   * @see #classAnnotators
   */
  public List<UserClassAnnotators> getClassAnnotators() {
    return classAnnotators;
  }

  /**
   * @param classAnnotators
   *          the classAnnotators to set
   * @see #classAnnotators
   */
  public void setClassAnnotators(List<UserClassAnnotators> classAnnotators) {
    this.classAnnotators = classAnnotators;
  }

  /**
   * @return the annotatorsParams
   */
  public List<UserAnnotatorsParam> getAnnotatorsParams() {
    return annotatorsParams;
  }

  /**
   * @param annotatorsParams
   *          the annotatorsParams to set
   */
  public void setAnnotatorsParams(List<UserAnnotatorsParam> annotatorsParams) {
    this.annotatorsParams = annotatorsParams;
  }

  /**
   * @return the classValidAnnotators
   * @see #classValidAnnotators
   */
  public List<UserClassValidAnnotations> getClassValidAnnotators() {
    return classValidAnnotators;
  }

  /**
   * @param classValidAnnotators
   *          the classValidAnnotators to set
   * @see #classValidAnnotators
   */
  public void setClassValidAnnotators(List<UserClassValidAnnotations> classValidAnnotators) {
    this.classValidAnnotators = classValidAnnotators;
  }

  /**
   * Adds (or updates) {@link UserClassAnnotators class annotator} information to
   * {@link #classAnnotators}.
   * 
   * @param ca
   *          object to add/update
   */
  public void addClassAnnotator(UserClassAnnotators ca) {
    if (ca.getClassName() == null) {
      throw new InvalidArgumentException("Class name cannot be null");
    }
    boolean replaced = false;
    for (int i = 0; i < this.classAnnotators.size(); i++) {
      UserClassAnnotators annotators = this.classAnnotators.get(i);
      if (annotators.getClassName().equals(ca.getClassName())) {
        annotators.setAnnotationSchema(null);
        this.classAnnotators.set(i, ca);
        replaced = true;
      }
    }
    if (!replaced) {
      classAnnotators.add(ca);
    }
    ca.setAnnotationSchema(this);
  }

  /**
   * Adds (or updates) {@link UserAnnotatorsParam} information to
   * {@link #annotatorsParams}.
   * 
   * @param ap
   *          object to add/update
   */
  public void addAnnotatorParam(UserAnnotatorsParam ap) {
    if (ap.getAnnotatorClassName() == null) {
      throw new InvalidArgumentException("Class name cannot be null");
    }
    if (ap.getParamName() == null) {
      throw new InvalidArgumentException("Parameter name cannot be null");
    }
    if (ap.getParamValue() == null) {
      throw new InvalidArgumentException("Parameter value cannot be null");
    }

    for (int i = 0; i < this.annotatorsParams.size(); i++) {
      UserAnnotatorsParam params = this.annotatorsParams.get(i);
      if (params.getAnnotatorClassName().equals(ap.getAnnotatorClassName())
          && params.getParamName().equals(ap.getParamName())) {
        this.annotatorsParams.get(i).setParamValue(ap.getParamValue());
        return;
      }
    }

    ap.setAnnotationSchema(this);
    this.annotatorsParams.add(ap);
  }

  /**
   * Adds (or updates) {@link UserClassValidAnnotations valid annotation}
   * information to {@link #classValidAnnotators}.
   * 
   * @param cva
   *          object to add/update
   */
  public void addClassValidAnnotations(UserClassValidAnnotations cva) {
    if (cva.getClassName() == null) {
      throw new InvalidArgumentException("Class name cannot be null");
    }
    boolean replaced = false;
    for (int i = 0; i < this.classValidAnnotators.size(); i++) {
      UserClassValidAnnotations annotators = this.classValidAnnotators.get(i);
      if (annotators.getClassName().equals(cva.getClassName())) {
        annotators.setAnnotationSchema(null);
        this.classValidAnnotators.set(i, cva);
        replaced = true;
      }
    }
    if (!replaced) {
      classValidAnnotators.add(cva);
    }
    cva.setAnnotationSchema(this);
  }

  /**
   * Returns list of annotators for given object type.
   * 
   * @param clazz
   *          type of object
   * @return list of annotators names
   */
  public List<String> getAnnotatorsForClass(Class<?> clazz) {
    List<String> result = new ArrayList<String>();
    for (UserClassAnnotators ca : classAnnotators) {
      if (ca.getClassName().equals(clazz.getCanonicalName())) {
        result = ca.getAnnotators();
      }
    }
    return result;
  }

  /**
   * Return list of valid annotation types for given class.
   * 
   * @param clazz
   *          type of the object for which result is returned
   * @return list of valid annotation types
   */
  public Collection<MiriamType> getValidAnnotations(Class<?> clazz) {
    for (UserClassValidAnnotations cva : classValidAnnotators) {
      if (cva.getClassName().equals(clazz.getCanonicalName())) {
        return cva.getValidMiriamTypes();
      }
    }
    return new ArrayList<MiriamType>();
  }

  /**
   * Return list of required annotation types for given class.
   * 
   * @param clazz
   *          type of the object for which result is returned
   * @return list of requried annotation types
   */
  public Collection<MiriamType> getRequiredAnnotations(Class<?> clazz) {
    for (UserClassRequiredAnnotations cva : classRequiredAnnotators) {
      if (cva.getClassName().equals(clazz.getCanonicalName())) {
        return cva.getRequiredMiriamTypes();
      }
    }
    return new ArrayList<MiriamType>();
  }

  /**
   * @return the classRequiredAnnotators
   * @see #classRequiredAnnotators
   */
  public List<UserClassRequiredAnnotations> getClassRequiredAnnotators() {
    return classRequiredAnnotators;
  }

  /**
   * @param classRequiredAnnotators
   *          the classRequiredAnnotators to set
   * @see #classRequiredAnnotators
   */
  public void setClassRequiredAnnotators(List<UserClassRequiredAnnotations> classRequiredAnnotators) {
    this.classRequiredAnnotators = classRequiredAnnotators;
  }

  /**
   * Adds (or updates) {@link UserClassRequiredAnnotations required annotation}
   * information to {@link #classRequiredAnnotators}.
   * 
   * @param cra
   *          object to add/update
   */
  public void addClassRequiredAnnotations(UserClassRequiredAnnotations cra) {
    if (cra.getClassName() == null) {
      throw new InvalidArgumentException("Class name cannot be null");
    }
    boolean replaced = false;
    for (int i = 0; i < this.classRequiredAnnotators.size(); i++) {
      UserClassRequiredAnnotations annotators = this.classRequiredAnnotators.get(i);
      if (annotators.getClassName().equals(cra.getClassName())) {
        annotators.setAnnotationSchema(null);
        this.classRequiredAnnotators.set(i, cra);
        replaced = true;
      }
    }
    if (!replaced) {
      this.classRequiredAnnotators.add(cra);
    }
    cra.setAnnotationSchema(this);
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public Boolean getSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat
   *          the sbgnFormat to set
   * @see #sbgnFormat
   */
  public void setSbgnFormat(Boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

  /**
   * @return the networkLayoutAsDefault
   * @see #networkLayoutAsDefault
   */
  public Boolean getNetworkLayoutAsDefault() {
    return networkLayoutAsDefault;
  }

  /**
   * @param networkLayoutAsDefault
   *          the networkLayoutAsDefault to set
   * @see #networkLayoutAsDefault
   */
  public void setNetworkLayoutAsDefault(Boolean networkLayoutAsDefault) {
    this.networkLayoutAsDefault = networkLayoutAsDefault;
  }

  /**
   * @return the annotateModel
   * @see #annotateModel
   */
  public Boolean getAnnotateModel() {
    return annotateModel;
  }

  /**
   * @param annotateModel
   *          the annotateModel to set
   * @see #annotateModel
   */
  public void setAnnotateModel(Boolean annotateModel) {
    this.annotateModel = annotateModel;
  }

  /**
   * @return the cacheData
   * @see #cacheData
   */
  public Boolean getCacheData() {
    return cacheData;
  }

  /**
   * @param cacheData
   *          the cacheData to set
   * @see #cacheData
   */
  public void setCacheData(Boolean cacheData) {
    this.cacheData = cacheData;
  }

  /**
   * @return the autoResizeMap
   * @see #autoResizeMap
   */
  public Boolean getAutoResizeMap() {
    return autoResizeMap;
  }

  /**
   * @param autoResizeMap
   *          the autoResizeMap to set
   * @see #autoResizeMap
   */
  public void setAutoResizeMap(Boolean autoResizeMap) {
    this.autoResizeMap = autoResizeMap;
  }

  /**
   * @return the semanticZooming
   * @see #semanticZooming
   */
  public Boolean getSemanticZooming() {
    return semanticZooming;
  }

  /**
   * @param semanticZooming
   *          the semanticZooming to set
   * @see #semanticZooming
   */
  public void setSemanticZooming(Boolean semanticZooming) {
    this.semanticZooming = semanticZooming;
  }

  public Set<UserGuiPreference> getGuiPreferences() {
    return guiPreferences;
  }

  public void addGuiPreference(UserGuiPreference option) {
    boolean updated = false;
    for (UserGuiPreference userOption : guiPreferences) {
      if (userOption.getKey().equals(option.getKey())) {
        userOption.setValue(option.getValue());
        updated = true;
      }
    }
    if (!updated) {
      guiPreferences.add(option);
      option.setAnnotationSchema(this);
    }
  }

  public UserGuiPreference getGuiPreference(String key) {
    for (UserGuiPreference userOption : guiPreferences) {
      if (userOption.getKey().equals(key)) {
        return userOption;
      }
    }
    return null;
  }

  public void setGuiPreference(String key, String value) {
    UserGuiPreference option = getGuiPreference(key);
    if (option == null) {
      option = new UserGuiPreference();
      option.setKey(key);
      addGuiPreference(option);
    }
    option.setValue(value);
  }

}
