package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.hibernate.annotations.IndexColumn;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * Defines set of required {@link MiriamType annotations} for a given object
 * type.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Table(name = "class_required_annotation_table")
public class UserClassRequiredAnnotations implements Serializable {

	/**
	 * 
	 */
	private static final long			 serialVersionUID		 = 1L;

	/**
	 * Defdault class logger.
	 */
	@Transient
	private final transient Logger logger							 = Logger.getLogger(UserClassRequiredAnnotations.class);

	/**
	 * Unique identifier in the database.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDb", unique = true, nullable = false)
	private Integer								 id;

	/**
	 * {@link UserAnnotationSchema} in which this set of required
	 * {@link MiriamType annotations} is used.
	 */
	@ManyToOne
	private UserAnnotationSchema	 annotationSchema;

	/**
	 * Name of the class for which this set is defined.
	 */
	private String								 className;

	/**
	 * Are the annotations required?
	 */
	private Boolean								 requireAtLeastOneAnnotation;

	/**
	 * One of this annotations will be required if
	 * {@link #requireAtLeastOneAnnotation} is set.
	 */
	@ElementCollection
	@JoinTable(name = "class_required_annotation_miriam_type_table", joinColumns = @JoinColumn(name = "class_required_annotation_iddb"))
	@Column(name = "miriam_type_name", nullable = false)
	@IndexColumn(name = "idx")
	@Enumerated(EnumType.STRING)
	private List<MiriamType>			 requiredMiriamTypes = new ArrayList<>();

	/**
	 * Default constructor.
	 */
	public UserClassRequiredAnnotations() {

	}

	/**
	 * Default constructor.
	 * 
	 * @param clazz
	 *          {@link #className}
	 * @param miriamTypes
	 *          {@link #requiredMiriamTypes}
	 */
	public UserClassRequiredAnnotations(Class<?> clazz, Collection<MiriamType> miriamTypes) {
		setClassName(clazz);
		if (miriamTypes != null) {
			setRequireAtLeastOneAnnotation(true);
			this.requiredMiriamTypes.addAll(miriamTypes);
		} else {
			setRequireAtLeastOneAnnotation(false);
		}
	}

	/**
	 * Default constructor.
	 * 
	 * @param clazz
	 *          {@link #className}
	 * @param miriamTypes
	 *          {@link #requiredMiriamTypes}
	 */
	public UserClassRequiredAnnotations(Class<?> clazz, MiriamType[] miriamTypes) {
		setClassName(clazz);
		for (MiriamType miriamType : miriamTypes) {
			this.requiredMiriamTypes.add(miriamType);
		}
		setRequireAtLeastOneAnnotation(miriamTypes.length > 0);
	}

	/**
	 * @return the className
	 * @see #className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className
	 *          the className to set
	 * @see #className
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the id
	 * @see #id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 * @see #id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets {@link #className}.
	 * 
	 * @param clazz
	 *          new {@link #className} value
	 */
	public void setClassName(Class<?> clazz) {
		setClassName(clazz.getCanonicalName());
	}

	/**
	 * Adds a type into {@link #requiredMiriamTypes list of required annotations}.
	 * 
	 * @param miriamType
	 *          object to add
	 */
	public void addRequiredMiriamType(MiriamType miriamType) {
		requiredMiriamTypes.add(miriamType);
	}

	/**
	 * @return the annotationSchema
	 * @see #annotationSchema
	 */
	public UserAnnotationSchema getAnnotationSchema() {
		return annotationSchema;
	}

	/**
	 * @param annotationSchema
	 *          the annotationSchema to set
	 * @see #annotationSchema
	 */
	public void setAnnotationSchema(UserAnnotationSchema annotationSchema) {
		this.annotationSchema = annotationSchema;
	}

	/**
	 * @return the requiredMiriamTypes
	 * @see #requiredMiriamTypes
	 */
	public List<MiriamType> getRequiredMiriamTypes() {
		return requiredMiriamTypes;
	}

	/**
	 * @param requiredMiriamTypes
	 *          the requiredMiriamTypes to set
	 * @see #requiredMiriamTypes
	 */
	public void setRequiredMiriamTypes(List<MiriamType> requiredMiriamTypes) {
		this.requiredMiriamTypes = requiredMiriamTypes;
	}

	/**
	 * @return the requireAtLestOneAnnotation
	 * @see #requireAtLeastOneAnnotation
	 */
	public Boolean getRequireAtLeastOneAnnotation() {
		return requireAtLeastOneAnnotation;
	}

	/**
	 * @param requireAtLeastOneAnnotation
	 *          the requireAtLestOneAnnotation to set
	 * @see #requireAtLeastOneAnnotation
	 */
	public void setRequireAtLeastOneAnnotation(Boolean requireAtLeastOneAnnotation) {
		this.requireAtLeastOneAnnotation = requireAtLeastOneAnnotation;
	}
}
