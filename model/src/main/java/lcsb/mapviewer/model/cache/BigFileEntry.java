package lcsb.mapviewer.model.cache;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.IProgressUpdater;

/**
 * Database object representing big file cached in filesystem.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("BIG_FILE_ENTRY")
public class BigFileEntry extends FileEntry implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID = 1L;

	/**
	 * Url from which file was obtained.
	 */
	@Column(columnDefinition = "TEXT")
	private String						url;

	/**
	 * When file was downloaded.
	 */
	private Calendar					downloadDate;

	/**
	 * What is the download progress (value between 0 and
	 * {@link IProgressUpdater#MAX_PROGRESS 100}).
	 */
	private Double						downloadProgress;

	/**
	 * {@link Thread} identifier that is downloading the file.
	 */
	private Long							downloadThreadId;

	/**
	 * @return the url
	 * @see #url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *          the url to set
	 * @see #url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the downloadDate
	 * @see #downloadDate
	 */
	public Calendar getDownloadDate() {
		return downloadDate;
	}

	/**
	 * @param downloadDate
	 *          the downloadDate to set
	 * @see #downloadDate
	 */
	public void setDownloadDate(Calendar downloadDate) {
		this.downloadDate = downloadDate;
	}

	/**
	 * @return the downloadProgress
	 * @see #downloadProgress
	 */
	public Double getDownloadProgress() {
		return downloadProgress;
	}

	/**
	 * @param downloadProgress
	 *          the downloadProgress to set
	 * @see #downloadProgress
	 */
	public void setDownloadProgress(Double downloadProgress) {
		this.downloadProgress = downloadProgress;
	}

	/**
	 * @return the downloadThreadId
	 * @see #downloadThreadId
	 */
	public Long getDownloadThreadId() {
		return downloadThreadId;
	}

	/**
	 * @param downloadThreadId
	 *          the downloadThreadId to set
	 * @see #downloadThreadId
	 */
	public void setDownloadThreadId(Long downloadThreadId) {
		this.downloadThreadId = downloadThreadId;
	}

}
